<?php
$getadmindetails    = $this->front_model->getadmindetails();  
$unlog_menu_status  = $getadmindetails[0]->unlog_menu_status;
$log_menu_status    = $getadmindetails[0]->log_menu_status;
$log_content        = $getadmindetails[0]->log_content;
$unlog_content      = $getadmindetails[0]->unlog_content;
$log_status         = $getadmindetails[0]->log_status;
$unlog_status       = $getadmindetails[0]->unlog_status;
?>
<!--Bootstrap core JavaScript -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>front/js/jquery.1.11.1.min.js"></script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
<!--<script src="<?php echo base_url(); ?>front/new/js/bootstrap.min.js"></script>-->
<!--<script src="<?php echo base_url(); ?>front/new/js/wow.min.js"></script>-->


<!-- Data table related scripts Start -->

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="<?php echo base_url(); ?>front/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>front/js/owl-carousel.js"></script>
<script src="<?php echo base_url();?>front/js/jquery-ui.js"></script>

<script src="<?php echo base_url();?>front/js/jquery.nicescroll.min.js"></script>
<!-- Scripts queries -->
<script>
$(document).ready(function() {

  $("#RefFrndScroll").niceScroll(); 

$('.owl-carousel').owlCarousel({
loop: true,
margin: 1,
responsiveClass: true,
responsive: {
0: {
items: 1,
nav: true
},
600: {
items: 2,
nav: true
},
1150: {
items: 4,
nav: true,
loop: false,
margin:0
}
}
})
})
</script> 
<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){


$(function () { 
    $('#startdatepicker, #enddatepicker').datepicker({
        beforeShow: customRange,
        dateFormat: "dd-mm-yy",
        firstDay: 1,
        changeFirstDay: false
    });
});

function customRange(input) {
    var min = null, // Set this to your absolute minimum date
        dateMin = min,
        dateMax = null,
        dayRange = 30; // Restrict the number of days for the date range
   
    if ($('#select1').val() === '2') {
        if (input.id === "startdatepicker") { 
            if ($("#enddatepicker").datepicker("getDate") != null) {
                dateMax = $("#enddatepicker").datepicker("getDate");
                dateMin = $("#enddatepicker").datepicker("getDate");
                dateMin.setDate(dateMin.getDate() - dayRange);
                if (dateMin < min) { dateMin = min; }
            } else {  }
        } else if (input.id === "enddatepicker") {
            dateMin = $("#startdatepicker").datepicker('getDate');
            dateMax = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() + 30);
            if ($('#startdatepicker').datepicker('getDate') != null) {
                var rangeMax = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() + dayRange);
                if (rangeMax < dateMax) { dateMax = rangeMax; }
            }
        }
    } else if ($('#select1').val() != '2') {
        if (input.id === "startdatepicker") {
            if ($('#enddatepicker').datepicker('getDate') != null) {
                dateMin = null;
            } else {  }
        } else if (input.id === "enddatepicker") {
            dateMin = $('#startdatepicker').datepicker('getDate');
            dateMax = null;
            if ($('#startdatepicker').datepicker('getDate') != null) { dateMax = null; }
        }
    }
    return {
        minDate: dateMin,
        maxDate: dateMax
    };
}

$('.datepicker').datepicker('widget').delegate('.ui-datepicker-close', 'mouseup', function() {
    var inputToBeCleared = $('.datepicker').filter(function() { 
      return $(this).data('pickerVisible') == true;
    });    
    $(inputToBeCleared).val('');
});
});//]]>  

</script>
<style type="text/css">
    /*.ui-datepicker { font-size: 9.7pt !important; }*/
</style>
<script type="text/javascript">
  $(function () { $("[data-toggle='tooltip']").tooltip(); });
  </script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/new/js/dataTables.bootstrap.css">
      <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>      
      <!--<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.js"></script>-->
      <script src="<?php echo base_url();?>front/js/dataTables.bootstrap.js"></script>
      <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
          $('#example').dataTable();
        } );
</script>
<script type="text/javascript">
var table = $("#example").dataTable({
    language : {
        sLengthMenu: "_MENU_",
        searchPlaceholder: "Buscar por loja ou valor por loja"
    }

}); 
</script>
 
<script>
/*$('ul.nav li.dropdown.top-login').hover(function() {
$(this).addClass('open');
$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});*/
</script>


<script>
/*new WOW().init();*/
</script>

<script>
   $(function(){
        var x = 0;
        setInterval(function(){
            x-=1;
            $('.moving').css('background-position', x + 'px 0');
        }, 50);
    })
</script>

<script>

  $(function() {
      
    $('a[href*="#"]:not([href="#"])').click(function() {
    if ( $( this ).hasClass( "check" ) ){
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
        scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
    });
  });
</script>


<!-- LOGIN POPUP PAGE VALIDATION --> 

<script type="text/javascript">
  function setupajax_login()
  {   
    //var datas = $('#login_form').serialize();
    var emails     = $('#emails').val();
    var passwords  = $('#passwrd').val();
    var signins    = $('#signin').val(); 

    /*new code for session values in cashback_exclusive concept 18-7-16*/
    var new_cash_ex_id   = $('#new_cash_ex_id').val();
    var cashback_details = $('#cashback_details').val();
    var expiry_dates     = $('#expiry_dates').val();
    var cashback_web     = $('#cashback_web').val();
    /*end*/
      
      $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>cashback/logincheck",
      data: {'email':emails,'password':passwords,'signin':signins,'cashback_id':new_cash_ex_id,'cashback_details':cashback_details,'expirydate':expiry_dates,'cashbackweb':cashback_web},
      cache: false,
      success: function(result)
      {
        if(result!=1)
        {
          $('#newdis').html(result);
          return false;
        }
        else
        {
          <?php $redirect_urlset =  base_url(uri_string());?>
          window.location.href = '<?php echo $redirect_urlset; ?>';
          return false;
        }             
      }
    });
    return false;
  }

  //New code for New Login page popup 27-4-16//
  function ssetupajax_login()
  { 
     
    //var datas = $('#login_form').serialize();
    var emails     = $('#semails').val();
    var passwords  = $('#spasswrd').val();
    var signins    = $('#ssignin').val(); 
    //alert(emails); die;
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>cashback/logincheck",
      data: {'email':emails,'password':passwords,'signin':signins},
      cache: false,
      success: function(result)
      {

        if(result!=1)
        {
          $('#snewdis').html(result);
          return false;
        }
        else
        {
          <?php $redirect_urlset =  base_url(uri_string());?>
          window.location.href = '<?php echo $redirect_urlset; ?>';
          return false;
        }             
      }
    });
    return false;
  }
  //End//

</script>  

<!--<script src="<?php echo base_url();?>front/js/jquery.min.js"></script>-->
<script src="<?php echo base_url();?>front/js/jquery.validate.min.js"></script> 
<script type="text/javascript">

/* form validation*/
 $(document).ready(function() {

         $("#regform").validate({

            rules: {

        user_email: {

                    required: true,

          email :true

                },

        user_pwd: {

                    required: true,

          minlength: 6

                },    

            },

            messages: {

        
        user_email: {

                      required: "Please enter your valid Emailid."
                    },

        user_pwd: {

                    required: "Please enter the password.",

          minlength: "Passwords must be minimum 6 characters."    

                },

        pwd_confirm: {

                    required: "Please confirm your password.",

          minlength: "Passwords must be minimum 6 characters.",

                },
            }
        });

    $("#rregform").validate({

    rules: {

       
    user_email: {

  required: true,

    email :true

    },

    user_pwd: {

                    required: true,

          minlength: 6

                },    

            },

            messages: {

        
        user_email: {

                      required: "Please enter  your valid Emailid."
                    },

        user_pwd: {

                    required: "Please enter the password.",

          minlength: "Passwords must be minimum 6 characters."    

                },

        pwd_confirm: {

                    required: "Please confirm your password.",

          minlength: "Passwords must be minimum 6 characters.",

          //equalTo : "Please enter the same password."

                    

                },
  
            }

         
        });

}); 
</script>

<script type="text/javascript">
//check email for  registration
function check_email()
{
  var email = $('#user_email').val();

  //alert(email);
  if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
  {
      $.ajax({

        type: 'POST',

        url: '<?php echo base_url();?>cashback/check_email',

        data:{'email':email},

         success:function(result){

          if(result.trim()==1)

          {

            $("#unique_name_error").css('color','#29BAB0');
            
             $("#unique_name_error").html('available.');

             $("input[name='register']").attr("disabled", false); 
          }

          else

          {

            $("#unique_name_error").css('color','#ff0000');
            $("#unique_name_error").css('font-size','14px');
            $("#unique_name_error").css('font-weight','bold');

            $("#unique_name_error").html('This email is already exists.'); 

            $("input[name='register']").attr("disabled", true); 

          }

        }

      });
  }
  return false;
}

function rcheck_email()
{

  var email = $('#ruser_email').val();
    //alert(email);
  if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
  {
      $.ajax({

        type: 'POST',

        url: '<?php echo base_url();?>cashback/check_email',

        data:{'email':email},

         success:function(result){

          if(result.trim()==1)

          {

            $("#runique_name_error").css('color','#29BAB0');
            

             $("#runique_name_error").html('available.');

             $("input[name='register']").attr("disabled", false);

          }

          else

          {

            $("#runique_name_error").css('color','#ff0000');
            $("#runique_name_error").css('font-weight','bold');

            $("#runique_name_error").html('This email is already exists.'); 

            $("input[name='register']").attr("disabled", true);  

          }

        }

      });
  }
    return false;
}

function valid_email()
{
  var emailid = $('#emailid').val();

  if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailid))
  {
      $.ajax({

        type: 'POST',

        url: '<?php echo base_url();?>cashback/check_vaild_email',

        data:{'email':emailid},

         success:function(result){

          if(result.trim()==1)

          {

            $("#unique_email_error").css('color','#29BAB0');
            

             $("#unique_email_error").html('available.');

          }

          else

          {

            $("#unique_email_error").css('color','#ff0000');

            $("#unique_email_error").html('This email is already exists.');  

          }

        }

      });
  }
  return false;
}
</script>

<!-- All stores page (desconto) modal popp files starts -->
    <!-- classie.js by @desandro: https://github.com/desandro/classie -->
    <script src="<?php echo base_url();?>front/new/js/classie.js"></script>
    <script src="<?php echo base_url();?>front/new/js/modalEffects.js"></script>

    <!-- for the blur effect -->
    <!-- by @derSchepp https://github.com/Schepp/CSS-Filters-Polyfill -->
    <script>
      // this is important for IEs
      var polyfilter_scriptpath = '/js/';
    </script>
    <!--<script src="<?php echo base_url();?>front/new/js/cssParser.js"></script>-->
    <script src="<?php echo base_url();?>front/new/js/css-filters-polyfill.js"></script>
<!-- modal popp files ends (Desconto page)-->


<script type="text/javascript"> 
 $(document).on('click','#signupclk',function()
  { 
    $('#login').modal('hide');
    $("#login").on('hide', function () {
    $('#register').modal('show');
    });
  });

  $(document).on('click','#signinclk',function()
  {
     $('#register').modal('hide');
    $("#register").on('hide', function () {
    $('#login').modal('show'); 
    });
  });

  $(document).on('click','#signupclks',function()
  {  
    $('#pingou').modal('hide');
    $("#pingou").on('hide', function () {
    $('#entrars').modal('show'); 
    });
  });

  $(document).on('click','#signinclks',function()
  { 
    $('#entrars').modal('hide');
    $("#entrars").on('hide', function () {
    $('#pingou').modal('show');
    });
  });

  $(document).on('click','#forgotclk',function()
  { 
    $('#login').modal('hide');
    $("#login").on('hide', function () {
    $('#forgot').modal('show');
    });
  });

  $(document).on('click','#forgotclick',function()
  { 
    $('#forgot').modal('hide');
    $("#forgot").on('hide', function () {
    $('#login').modal('show');
    });
  });
  $(document).on('click','#store_forgotclk',function()
  { 
    $('#pingou').modal('hide');
    $("#pingou").on('hide', function () {
    $('#forgot').modal('show');
    });
  });

    
  </script>  
  <script type="text/javascript">
  /* form validation*/
  $(document).ready(function() {
         $("#forget_password").validate({
            rules: {
        email: {
                     required: true,
            email :true
                }     
            },
            messages: {
        email: {
                   required: "Please enter  your valid Emailid."                  
                }
      }
        
        });
});
</script>


  <!--bootstrap date picker starts -->
<script type="text/javascript">
  $(function () {
  $('#datetimepicker1').datetimepicker();
  });
</script>

<script src="<?php echo base_url();?>front/new/js/moment-with-locales.js"></script>

<!--bootstrap date picker ends -->

<!-- select box file starts -->
<script type="text/javascript" src="<?php echo base_url();?>front/new/js/jquery.dropdown.js"></script>
<script type="text/javascript">
      
      $( function() {
        
        $( '#cd-dropdown' ).dropdown( {
          gutter : 2,
          stack : false,
          slidingIn : 100
        } );
        
        $( '#cd-dropdown1' ).dropdown( {
          gutter : 2,
          stack : false,
          slidingIn : 100
        } );
        
        $( '#cd-dropdown2' ).dropdown( {
          gutter : 2,
          stack : false,
          slidingIn : 100
        } );

      });

</script>
<!--select box file ends -->
<!--datasort table section starts-->
<script src="<?php echo base_url();?>front/new/js/jquery-ui.js"></script>
<script src="<?php echo base_url();?>front/new/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
         $(document).ready(function() {
             $('#sourceTable').dataTable();
         });
</script>
<script type="text/javascript">
         function changeState(el) {
             if (el.readOnly) el.checked=el.readOnly=false;
             else if (!el.checked) el.readOnly=el.indeterminate=true;
         }
         var selectIds = $('#panel1,#panel2,#panel3');
         $(function ($) {
         selectIds.on('show.bs.collapse hidden.bs.collapse', function () {
             $(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
         })
         });
</script>
<!--datasort table section ends-->
<!--show hide sub menu onmouseover -->
<script type="text/javascript">
/*function show_datasub()
{
 document.getElementById('dataSubMain').style.display="block";
}

$("#hide_fn").mouseleave(function(){
console.log('Pila');
$("#dataSubMain").css("display", "none");
});*/

</script>
<!--show hide sub menu onmouseover -->
<script>
 
   /* $(".dropdown-toggle").click(function(){
        $(".dropdown-menu").slideToggle("slow");
    });*/
</script>
<!--<script src="<?php echo base_url(); ?>front/js/jquery.1.11.1.min.js"></script>
<script src="<?php echo base_url(); ?>front/js/bootstrap.min.js"></script>-->
<script src="<?php echo base_url();?>front/js/bootstrap-typeahead.js"></script>

<script type="text/javascript">
/*Search form content 6-8-16*/
 
    var xhr = null;
      $('input.typeahead').typeahead({
    source: function (query, process)
    {
      if( xhr != null ) 
      {
              xhr.abort();
              xhr = null;
          }
      map = {};
      objects = [];
      xhr = $.ajax({
        url: '<?php echo base_url();?>cashback/getstores_listjson/'+query,
        type: 'POST',
        dataType: 'JSON',
        data: 'query=' + query,
        success: function(data) 
        {
          alert(data);
          $.each(data, function (i, object) 
          {
            map[object.affiliate_name] = object;
            objects.push(object.affiliate_name);
          });
          process(objects);
          //alert(objects);
        }
      });
    }
    });
    
    var xhr;
    $('input#Location').typeahead({
        source: function (query, process) 
        {
      if(xhr && xhr.readyState != 4 && xhr.readyState != 0)
      {
        xhr.abort();
      }
      map = {};
      objects = [];
      xhr = $.ajax({
          url: '<?php echo base_url();?>cashback/getcitys_listjson/'+query,
          type: 'POST',
          dataType: 'JSON',
          data: 'query=' + query, 
          success: function(data) 
          {
        $.each(data, function (i, object)
        {
          map[object.city_name] = object;
          objects.push(object.city_name);
        });
            process(objects);
          }
          });
    }
        });
 
/*end*/

</script>

<!-- Cupom desconto page popup an redirect contents start -->
  
<script>
//before login sections
 $('.dsf').click(function(){
   //alert('seetha');
  url = ($(this).attr('data-id'));
  var showcoupon_id = ($(this).attr('showcoupon_id'));
  sessionStorage["showcoupon_id"]=showcoupon_id;
  var win=window.open(url, '_self');
  win.focus();
  sessionStorage["PopupShown"] = 'yes';
  $('#without_shopping').show();
})
$().ready(function(){
  if(sessionStorage["PopupShown"] == 'yes') 
  { 
     $("div#LoginModal").modal({backdrop: 'static',keyboard: false}); 
     $('#without_shopping').show();

    //setTimeout(function(){ location.reload(); }, 2000); 
    sessionStorage["PopupShown"] = 'no';
  } else {
    sessionStorage["PopupShown"] = 'no';
  }
  $('.without_viji').click(function(){
    //alert(sessionStorage["showcoupon_id"]);
    $("div#LoginModal").modal('hide');
    $('.btn1_'+sessionStorage["showcoupon_id"]).hide();
    $('.btn2_'+sessionStorage["showcoupon_id"]).show();
  });
})

//after login sections
$('.after_login').click(function(){ 
  var url = ($(this).attr('data-id'));
  var show_id = ($(this).attr('show_id'));
  //alert(show_id);
  var win=window.open(url, '_self');
  win.focus();
  sessionStorage["PopupShown1"] = 'yes';
  sessionStorage["show_id"] = show_id;
  
})
$().ready(function(){
  if(sessionStorage["PopupShown1"] == 'yes') 
  { 
     
    $('.btn1_'+sessionStorage["showcoupon_id"]).hide();
    $('.btn2_'+sessionStorage["showcoupon_id"]).show();
    $("div#myModal_visit_store"+sessionStorage["show_id"]).modal({backdrop: 'static',keyboard: false});     
    sessionStorage["PopupShown1"] = 'no';
    sessionStorage["show_id"] = '';
  } else {
    sessionStorage["show_id"] = '';
  } 
})
</script>
 <script>
 $(".top-search .module-title ").click(function(){
    $(".module-ct").toggle();
});
 </script>

 <!-- new code for store popup shows 28-7-16 -->
<?php

$cashback_id =  $_REQUEST['oid'];

$store_name  =  $this->uri->segment(2);
$kt = 1;
$mi = 1;
$selqry   = $this->db->query("SELECT * from coupons where coupon_id='$cashback_id'")->row(); 
$selqry1  = $this->db->query("SELECT * from affiliates where affiliate_url='$store_name'")->row();

//print_r($selqry);
$aff_name = $selqry->affiliate_url;
if($cashback_id!='')
{
  ?>
  <div class='modal fade cus_modal' id='myModal_visit_store' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
    <div class='modal-dialog' style="width:900px !important;">
      <div class='modal-content' id='newcontent<?php echo $kt;?>' style=''>
        <div class="modal-header">
          <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
          <div class="modal-header-default">
            <div style="background: url("<?php echo base_url()."uploads/adminpro/".$logo;?>") no-repeat scroll 0px 0px transparent; height: 69px; padding: 0px 0px 0px 271px;">
              <center>
                <p class="lead3 m-warning display-none" style="display: block;">You are about to visit</p>
                <h3><?php echo $selqry->offer_name;?></h3>
              </center>
            </div>
          </div>
        </div>
        <div class='modal-body-default'>
          <span class='alert alert-block alert-info alert-icon' style='display: block; font-size: 16px;line-height: 25px;'>
            <span>
              <center>Your visit has been recorded. The cashback from any purchase(s) will soon show in your account.</center>
            </span>
          </span>
          <?php
          if($selqry->type!='Promotion')
          {
            ?>
            <input id='copyTarget<?php echo $mi;?>' class='txtclss' readonly value='<?php echo $selqry->code;?>'>
            <button class='btn btn-primary newclls' onclick='copy(<?php echo $mi; ?>);' data-id='<?php echo $mi;?>' id='copyButton<?php echo $mi;?>'>COPY</button>
            <p style='text-align:center; margin-right: 10%;' id='msg<?php echo $mi;?>'></p><br>
            <?php
          }
          ?>
        </div>
        <div class='modal-footer' style='display: block;'>
          <div class='continue-hide m-non-warning display-none' style='display: block;margin-right: 29px;'>
            <p class='copy-medium'>
              <?php
              if($selqry->type!='Promotion')
              {
                ?>
                  <a target="_blank" class='btn btn-primary' href='<?php echo base_url().'ir-loja/'.$selqry1->affiliate_id.'/'.$cashback_id;?>'> Visit <?php echo $store_name;?> and redeem code</a>
                  <?php
              }
              else
              {
                ?>
                  <a class='btn btn-primary' href='<?php echo base_url();?>'> Continue shopping at <?php $admindetails = $this->front_model->getadmindetails_main(); echo $admindetails->site_name; ?> for more great offers </a>
                  <?php
              }
              ?>
              <br>
            </p>
          </div>
        </div>    
      </div>
    </div>
  </div>
  <div class="modal fade cus_modal" id="myModal_redam" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:900px !important;">
      <div class="modal-content" id="newcontent<?php echo $kt;?>" style="">
        <div class="modal-header">
          <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
          <div class="modal-header-default">
            <div style="background: url('<?php echo base_url()."uploads/adminpro/".$logo;?>') no-repeat scroll 0px 0px transparent; height: 69px; padding: 0px 0px 0px 271px;">
              <p class="lead3 m-warning display-none" style="display: block;">Indo para</p>
              <h3><?php echo $selqry->offer_name;?></h3>
            </div>
          </div>
        </div>
        <div class="modal-body-default">
          <span class="alert alert-block alert-info alert-icon" style="display: block; font-size: 16px;line-height: 25px;">
            <span>
              <center>
              Sua visita foi Registrada. O dinheiro de volta de sua(s) compra(s) estará disponível no seu extato em até 48h.
              </center>
            </span>
          </span>
          <?php
          if($selqry->type!='Promotion')
          {
            ?>
            <div style="display: block;" class="voucher-code display-none">
              <p>Copie e cole o código no carrinho de compras</p>
              <span> <?php echo $selqry->code;?></span>
            </div>
            <?php
          }
          ?>
        </div>
        <div class="modal-footer" style="display: block;">
          <div class="continue-hide m-non-warning display-none" style="display: block;margin-right: 29px;">
            <p class="copy-medium">
              <?php
              if($selqry->type!='Promotion')
              {
                ?>
                <a target="_blank" class="btn btn-primary" href="<?php echo base_url().'ir-loja/'.$selqry1->affiliate_id.'/'.$cashback_id;?>"> Ir para <?php echo $store_name;?></a>
                <?php
              }
              else
              {
                ?>
                <a class="btn btn-primary" href="<?php echo base_url();?>"> Continue shopping at <?php $admindetails = $this->front_model->getadmindetails_main(); echo $admindetails->site_name; ?> for more great offers </a>
                <?php
              }
              ?>
              <br>
            </p>
          </div>
        </div> 
      </div>
    </div>
  </div>

  <script type="text/javascript">
        $('#myModal_visit_store').modal('show');
    //$("#myModal_visit_store").modal(); 
    /*$('.btn2_'+sessionStorage["showcoupon_id"]).show();*/
   /* $('.btn1_'+<?php echo $cashback_id;?>).hide();
    $('.btn2_'+<?php echo $cashback_id;?>).show();*/
  </script>
  <?php
}
?>  
<!-- End 28-7-16 -->

<!-- new code for star rating 1-8-16 -->
<script type="text/javascript" src="<?php echo base_url();?>front/js/rating.js"></script>
<script language="javascript" type="text/javascript">

    $("#rating_star").codexworld_rating_widget({
        starLength: '5',
        initialValue: '',
        callbackFunctionName: 'processRating',
        imageDirectory: 'images/',
        inputAttr: 'postID',
    });


function processRating(val, attrVal){
  //alert(attrVal);
    $.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>cashback/rating',
        data: 'postID='+attrVal+'&ratingPoints='+val,
        dataType: 'json',
        success : function(data) {
            if (data) {
                //alert('You have rated '+val+' to CodexWorld');
                $('#avgrat').text(data.average_rating);
                $('#totalrat').text(data.rating_number);
            }else{
                alert('Some problem occured, please try again.');
            }
        }
    });
}
</script>
<!-- end star rating -->
<!-- Cupom-desconto End -->


<!-- Productos page scripts -->
<script type="text/javascript">
function proRangeSlider(sliderid, outputid, colorclass) {
        var x = document.getElementById(sliderid).value;  
        document.getElementById(outputid).innerHTML = x;
        document.getElementById(sliderid).setAttribute('class', colorclass);
        
        updateTotal();
}

var total = 0;
function updateTotal() {
        var list= document.getElementsByClassName("range");
        [].forEach.call(list, function(el) {
            console.log(el.value);
            total += parseInt(el.value);
        }); 
        document.getElementById("n_total").innerHTML = total;
      
}
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(function() {
    $('form').submit(function() {
        $('#result').text(JSON.stringify($('form').serializeObject()));
        return false;
    });
});
</script>
<!-- Productos page End-->
<?php 
$storename = $this->uri->segment(2); 
  $selqry    = $this->db->query("SELECT * from affiliates where affiliate_url='$storename'")->row(); 
  $aff_id    = $selqry->affiliate_id;
  $aff_name  = $selqry->affiliate_name;
  $aff_url   = $selqry->affiliate_url;
  $types     = $selqry->affiliate_cashback_type;
?>
<script type="text/javascript">

$(document).ready(function()
{
  $('.newnew1').click(function()
  {

    var coupontypes    = $(this).attr('cuptype'); 
    var couponid       = $(this).attr('cupid');
    
    $('.cupid').val(couponid);
    $('.cuptype').val(coupontypes);
  });



  $("#data_temp_val").click(function()
  {

    var data_temp_val = $('.cupid').val();
    var coupon_type   = $('.cuptype').val();
    //alert(coupon_type); 
    
    if(coupon_type == 'Promotion')
    {
      /*New changes 24-8-16*/
      $("#data_temp_val").attr("href", "<?php echo base_url();?>ir-loja/<?php echo $aff_id;?>/"+data_temp_val);
      /*End 24-8-16*/
      
      /*$("#data_temp_val").attr("data-id", "<?php echo base_url();?>cupom-desconto/<?php echo $aff_url; ?>?oid="+data_temp_val);*/
      $("#data_temp_val").attr("show_id", data_temp_val);
      /*$("#data_temp_val").attr("href", "<?php echo base_url();?>ir-loja/<?php echo $aff_id;?>/"+data_temp_val);*/
    }
    else
    {
      $("#data_temp_val").attr("data-id", "<?php echo base_url();?>ir-loja/<?php echo $aff_id;?>/"+data_temp_val);
      $("#data_temp_val").attr("show_id", data_temp_val);
      $("#data_temp_val").attr("href", "<?php echo base_url();?>cupom-desconto/<?php echo $aff_url; ?>?oid="+data_temp_val);
    }

    var url = ($(this).attr('data-id'));
    var show_id = ($(this).attr('show_id'));
    //alert(show_id);
    var win=window.open(url, '_self');
    win.focus();
    sessionStorage["PopupShown1"] = 'yes';
    sessionStorage["show_id"] = show_id;

   });

  $("#data_temp_vals").click(function()
  {

    var data_temp_vals = $('.cupid').val();
    
    var coupon_type   = $('.cuptype').val();
    //alert(coupon_type); 
    
    if(coupon_type == 'Promotion')
    {
     /*New changes 24-8-16*/
      $("#data_temp_vals").attr("href", "<?php echo base_url();?>ir-loja/<?php echo $aff_id;?>/"+data_temp_vals);
      /*End 24-8-16*/
      
      /*$("#data_temp_val").attr("data-id", "<?php echo base_url();?>cupom-desconto/<?php echo $aff_url; ?>?oid="+data_temp_val);*/
      $("#data_temp_vals").attr("show_id", data_temp_vals);
      /*$("#data_temp_val").attr("href", "<?php echo base_url();?>ir-loja/<?php echo $aff_id;?>/"+data_temp_val);*/
    }
    else
    {
      $("#data_temp_vals").attr("data-id", "<?php echo base_url();?>ir-loja/<?php echo $aff_id;?>/"+data_temp_vals);
      $("#data_temp_vals").attr("show_id", data_temp_vals);
      $("#data_temp_vals").attr("href", "<?php echo base_url();?>cupom-desconto/<?php echo $aff_url; ?>?oid="+data_temp_vals);
    }
    
    var url = ($(this).attr('data-id'));
    var show_id = ($(this).attr('show_id'));
    //alert(show_id);
    var win=window.open(url, '_self');
    win.focus();
    sessionStorage["PopupShown1"] = 'yes';
    sessionStorage["show_id"] = show_id;

  });
});
</script>

<script type="text/javascript">
  function email_news()
  {
    var email = $("#news_email").val();
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,3})?$/;
    if(!email || !emailReg.test(email))
    {
      $('#news_email').css('border', '2px solid red');
    } 
    else
    {
      $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>cashback/email_subscribe/",
      data: {'email':email},
      success: function(msg)
      {
        if(msg==1)
        {
          $('#new_msg').text('Activated Successfully');
          $('#news_email').css('border', '');
        }
        else
        {
          $('#new_msg').text('Already Activated');
          $('#news_email').css('border', '');
        } 
      }
      });
    } 
  }
</script>

<!-- New code for search content in top of the page 6-8-16 -->
<script type="text/javascript">
  $('#search_content').hide();
  $('#selsearch').each(function() {
    $(this).show(0).on('click', function(e) {
      //alert("hai");
        // This is only needed if your using an anchor to target the "box" elements
        e.preventDefault();
        
        // Find the next "box" element in the DOM
        $('#search_content').slideToggle('fast');
        $('#search').focus();
    });
});
</script>
<script type="text/javascript">
   $('#search_content1').hide();
  $('#selsearch1').each(function() {
    $(this).show(0).on('click', function(e) {
      
        e.preventDefault();
        
        $('#search_content1').slideToggle('fast');
        $('.searchs').focus();
    });
});
</script>
<!-- End -->
<script src="<?php echo base_url(); ?>front/js/jquery-ui.js"></script>
<?php $search_name =$this->front_model->search_name(); ?>
<!-- New code fo search store and submit action -->
  <script type="text/javascript">
  function submit_form()
  {
    var storeid_enc = $('#search').val();
    //alert(storeid);
    var storeid = storeid_enc.replace(/ /g,"-");
    var storeid = storeid.toLowerCase();
    var redirect_url = '<?php echo base_url();?>cupom-desconto/'+storeid;
    $('#storehead').val(storeid);     
    $('#dummyform').attr('action', redirect_url).submit();
    //window.location.href=redirect_url;
    return false;
  }
  </script>
  <!-- End -->
<script type="text/javascript">
  //New code for search//
var avail = <?php echo $search_name; ?>;
jQuery(".search_header").autocomplete({

   source: avail,        
   select: function (event, ui) 
   {
          autoFocus: true;
          location.href = ui.item.the_link;

    $("#product_id").val(ui.item.product_id); 

   },

}).autocomplete( "instance" )._renderItem = function( ul, item ) {
var msg = '<a href="'+item.the_link+'"><div style="margin:8px;" class="search_list"><span><div class="search_icon"><img width="150" height="70" class="img-response" src="<?php echo base_url(); ?>uploads/affiliates/'+item.logo+'" /></div><strong style="margin-left:10px; text-transform: capitalize;">'+item.label+'</strong></span></div></a>';
return $( "<li>" )
.append( msg )
.appendTo( ul );

};
//End//
</script>

<?php 
//echo $unlog_status; echo "<br>"; echo $unlog_menu_status; 
if(($unlog_status == 1 && $unlog_menu_status == 1) || ($unlog_status == 0 && $unlog_menu_status == 1))
{
  ?>
  <script>
  $(window).scroll(function(){
      'use strict';
      if ($(this).scrollTop() > 50){  
          $('#cbp-af-header').addClass("sticky");
      }
      else{
          $('#cbp-af-header').removeClass("sticky");
      }
  });
  </script>
<?php
}

if(($log_status == 1 && $log_menu_status == 1) || ($log_status == 0 && $log_menu_status == 1))
{
  ?>
  <script>
  $(window).scroll(function(){
      'use strict';
      if ($(this).scrollTop() > 50){  
          $('#cbp-af-header1').addClass("sticky");
      }
      else{
          $('#cbp-af-header1').removeClass("sticky");
      }
  });
  </script>
<?php
}
?>
 
