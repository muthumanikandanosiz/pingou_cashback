Array
(
    [0] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Viagens
                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Array
                        (
                            [0] => Promoções
                            [1] => Desconto em Porcentagem
                        )

                )

            [offerdescription] => CUPOM DE DESCONTO 5% PARA TODO O SITE
            [offerstartdate] => 2016-08-03
            [offerenddate] => 2017-12-31
            [couponcode] => D7KNA5GBYV
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=451626.54
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=451626.54
&type=3&subid=0
            [advertiserid] => 40161
            [advertisername] => Descubra o Mundo
            [network] => Brazil Network
        )

    [1] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Array
                        (
                            [0] => Namorados
                            [1] => Presentes
                        )

                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Desconto em Porcentagem
                )

            [offerdescription] => R$ 10 OFF em todo o site!
            [offerstartdate] => 2016-04-26
            [offerenddate] => 2022-12-30
            [couponcode] => RKT10DESOFF
            [couponrestriction] => R$ 10 OFF em todo o site!
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=379075.30
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=379075.30
&type=3&subid=0
            [advertiserid] => 40272
            [advertisername] => Giuliana Flores
            [network] => Brazil Network
        )

    [2] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Array
                        (
                            [0] => Jardinagem
                            [1] => Presentes
                        )

                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Array
                        (
                            [0] => Promoções
                            [1] => Desconto em Porcentagem
                        )

                )

            [offerdescription] => R$ 15 OFF em todo o site!
            [offerstartdate] => 2016-04-15
            [offerenddate] => 2022-12-30
            [couponcode] => RKT15ROFF
            [couponrestriction] => R$ 15 OFF em todo o site!
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=379075.31
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=379075.31
&type=3&subid=0
            [advertiserid] => 40272
            [advertisername] => Giuliana Flores
            [network] => Brazil Network
        )

    [3] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Array
                        (
                            [0] => Beleza e Saúde
                            [1] => Bem Estar
                        )

                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Array
                        (
                            [0] => Promoções
                            [1] => Frete Grátis
                        )

                )

            [offerdescription] => Cupom de desconto para Cabelos:- 10% para compras acima de R$199
            [offerstartdate] => 2016-03-30
            [offerenddate] => 2017-02-26
            [couponcode] => C4B301PCNT
            [couponrestriction] => Cupom de desconto para Cabelos
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=429923.11
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=429923.11
&type=3&subid=0
            [advertiserid] => 41184
            [advertisername] => Sepha
            [network] => Brazil Network
        )

    [4] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Array
                        (
                            [0] => Beleza e Saúde
                            [1] => Bem Estar
                        )

                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Array
                        (
                            [0] => Promoções
                            [1] => Frete Grátis
                        )

                )

            [offerdescription] => Cupom de desconto para Cabelos: C4B351P6NT - 15% para compras acima
 de R$399
            [offerstartdate] => 2016-03-30
            [offerenddate] => 2017-02-26
            [couponcode] => C4B351P6NT
            [couponrestriction] => Cupom de desconto para Cabelos
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=429923.12
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=429923.12
&type=3&subid=0
            [advertiserid] => 41184
            [advertisername] => Sepha
            [network] => Brazil Network
        )

    [5] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Array
                        (
                            [0] => Beleza e Saúde
                            [1] => Bem Estar
                        )

                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Array
                        (
                            [0] => Promoções
                            [1] => Frete Grátis
                        )

                )

            [offerdescription] => Cupom de desconto para Cabelos: - 20% para compras acima de R$499
            [offerstartdate] => 2016-03-30
            [offerenddate] => 2017-02-26
            [couponcode] => C4B302PC3T
            [couponrestriction] => Cupom de desconto para Cabelos
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=429923.13
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=429923.13
&type=3&subid=0
            [advertiserid] => 41184
            [advertisername] => Sepha
            [network] => Brazil Network
        )

    [6] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Array
                        (
                            [0] => Flores
                            [1] => Presentes
                        )

                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Desconto em Porcentagem
                )

            [offerdescription] => 20% de desconto em todo o site nas compras a partir de R$100 (CUPOM
: RK20OFF)
            [offerstartdate] => 2015-11-26
            [offerenddate] => 2021-12-31
            [couponcode] => RK20OFF
            [couponrestriction] => Desconto em todo o site nas compras a partir de R$100.
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=379075.9&type
=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=379075.9&type
=3&subid=0
            [advertiserid] => 40272
            [advertisername] => Giuliana Flores
            [network] => Brazil Network
        )

    [7] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Array
                        (
                            [0] => Moda
                            [1] => Calçados
                        )

                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Array
                        (
                            [0] => Promoções
                            [1] => De - Por
                        )

                )

            [offerdescription] => Calçados com 5% de Desconto! (cupom: FA93-4431-3AD2-B1E5)
            [offerstartdate] => 2015-11-25
            [offerenddate] => 2021-12-30
            [couponcode] => FA93-4431-3AD2-B1E5
            [couponrestriction] => Calçados.
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=414984.11
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=414984.11
&type=3&subid=0
            [advertiserid] => 39922
            [advertisername] => Ricardo Eletro
            [network] => Brazil Network
        )

    [8] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Casa e Decoração
                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Array
                        (
                            [0] => Promoções
                            [1] => De - Por
                        )

                )

            [offerdescription] => Cama, Mesa e Banho com 5% de Desconto! (cupom: 44D8-625D-46AF-A1E9
)
            [offerstartdate] => 2015-11-25
            [offerenddate] => 2021-12-30
            [couponcode] => 44D8-625D-46AF-A1E9
            [couponrestriction] => Cama, Mesa e Banho.
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=414984.12
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=414984.12
&type=3&subid=0
            [advertiserid] => 39922
            [advertisername] => Ricardo Eletro
            [network] => Brazil Network
        )

    [9] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Casa e Decoração
                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Array
                        (
                            [0] => Promoções
                            [1] => De - Por
                        )

                )

            [offerdescription] => Casa e Ferramentas com 5% de Desconto! (cupom: 3AD9-4CA3-2D43-F699
)
            [offerstartdate] => 2015-11-25
            [offerenddate] => 2021-12-30
            [couponcode] => 3AD9-4CA3-2D43-F699
            [couponrestriction] => Casa e Ferramentas.
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=414984.14
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=414984.14
&type=3&subid=0
            [advertiserid] => 39922
            [advertisername] => Ricardo Eletro
            [network] => Brazil Network
        )

    [10] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Eletrônico
                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Array
                        (
                            [0] => Promoções
                            [1] => De - Por
                        )

                )

            [offerdescription] => Celulares e Telefones com 5% de Desconto! (cupom: 4208-D7C2-D908-3DC9
)
            [offerstartdate] => 2015-11-25
            [offerenddate] => 2021-12-30
            [couponcode] => 4208-D7C2-D908-3DC9
            [couponrestriction] => Celulares e Telefones.
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=414984.15
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=414984.15
&type=3&subid=0
            [advertiserid] => 39922
            [advertisername] => Ricardo Eletro
            [network] => Brazil Network
        )

    [11] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Casa e Decoração
                )

            [promotiontypes] => Array
                (
                    [promotiontype] => De - Por
                )

            [offerdescription] => Eletrodomésticos com 5% de Desconto! (cupom: E198-E863-BA87-DC80)
            [offerstartdate] => 2015-11-25
            [offerenddate] => 2021-12-30
            [couponcode] => E198-E863-BA87-DC80
            [couponrestriction] => Eletrodomésticos.
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=414984.17
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=414984.17
&type=3&subid=0
            [advertiserid] => 39922
            [advertisername] => Ricardo Eletro
            [network] => Brazil Network
        )

    [12] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Array
                        (
                            [0] => Eletrônico
                            [1] => Games
                        )

                )

            [promotiontypes] => Array
                (
                    [promotiontype] => De - Por
                )

            [offerdescription] => Games com 5% de Desconto! (cupom: C90D-2155-B128-4AF6)
            [offerstartdate] => 2015-11-25
            [offerenddate] => 2021-12-30
            [couponcode] => C90D-2155-B128-4AF6
            [couponrestriction] => Games.
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=414984.20
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=414984.20
&type=3&subid=0
            [advertiserid] => 39922
            [advertisername] => Ricardo Eletro
            [network] => Brazil Network
        )

    [13] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Instrumentos Musicais
                )

            [promotiontypes] => Array
                (
                    [promotiontype] => De - Por
                )

            [offerdescription] => Instrumentos Musicais com 5% de Desconto! (cupom: 7261-0ABB-407A-5634
)
            [offerstartdate] => 2015-11-25
            [offerenddate] => 2021-12-30
            [couponcode] => 7261-0ABB-407A-5634
            [couponrestriction] => Instrumentos Musicais.
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=414984.22
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=414984.22
&type=3&subid=0
            [advertiserid] => 39922
            [advertisername] => Ricardo Eletro
            [network] => Brazil Network
        )

    [14] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Casa e Decoração
                )

            [promotiontypes] => Array
                (
                    [promotiontype] => De - Por
                )

            [offerdescription] => Móveis com 5% de Desconto! (cupom: FA6D-0558-1972-8F14)
            [offerstartdate] => 2015-11-24
            [offerenddate] => 2021-12-30
            [couponcode] => FA6D-0558-1972-8F14
            [couponrestriction] => Móveis.
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=414984.25
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=414984.25
&type=3&subid=0
            [advertiserid] => 39922
            [advertisername] => Ricardo Eletro
            [network] => Brazil Network
        )

    [15] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Esporte e Lazer
                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Array
                        (
                            [0] => Desconto em Porcentagem
                            [1] => De - Por
                        )

                )

            [offerdescription] => Pescaria com 5% de Desconto! (cupom: 932B-C599-485F-7CCF)
            [offerstartdate] => 2015-11-24
            [offerenddate] => 2021-12-30
            [couponcode] => 932B-C599-485F-7CCF
            [couponrestriction] => Pescaria.
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=414984.26
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=414984.26
&type=3&subid=0
            [advertiserid] => 39922
            [advertisername] => Ricardo Eletro
            [network] => Brazil Network
        )

    [16] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Array
                        (
                            [0] => Moda
                            [1] => Presentes
                            [2] => Joias e Acessórios
                        )

                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Array
                        (
                            [0] => Promoções
                            [1] => Desconto em Porcentagem
                        )

                )

            [offerdescription] => Relógios com 5% de Desconto! (cupom: FDA5-FBA4-582B-19D3)
            [offerstartdate] => 2015-11-24
            [offerenddate] => 2021-12-30
            [couponcode] => FDA5-FBA4-582B-19D3
            [couponrestriction] => Relógios.
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=414984.27
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=414984.27
&type=3&subid=0
            [advertiserid] => 39922
            [advertisername] => Ricardo Eletro
            [network] => Brazil Network
        )

    [17] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Array
                        (
                            [0] => Beleza e Saúde
                            [1] => Alimentos e Bebidas
                        )

                )

            [promotiontypes] => Array
                (
                    [promotiontype] => De - Por
                )

            [offerdescription] => Suplementos e Vitaminas com 5% de Desconto! (cupom: 1EF1-9CC7-5578-A910
)
            [offerstartdate] => 2015-11-24
            [offerenddate] => 2021-12-30
            [couponcode] => 1EF1-9CC7-5578-A910
            [couponrestriction] => Suplementos e Vitaminas.
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=414984.28
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=414984.28
&type=3&subid=0
            [advertiserid] => 39922
            [advertisername] => Ricardo Eletro
            [network] => Brazil Network
        )

    [18] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Array
                        (
                            [0] => Lojas de Departamento
                            [1] => Presentes
                            [2] => Casa e Decoração
                        )

                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Array
                        (
                            [0] => Desconto em Porcentagem
                            [1] => De - Por
                        )

                )

            [offerdescription] => Utilidade Domésticas com 5% de Desconto! (cupom: 7048-EEC7-7447-00AD
)
            [offerstartdate] => 2015-11-24
            [offerenddate] => 2021-12-30
            [couponcode] => 7048-EEC7-7447-00AD
            [couponrestriction] => Utilidade Domésticas.
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=414984.29
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=414984.29
&type=3&subid=0
            [advertiserid] => 39922
            [advertisername] => Ricardo Eletro
            [network] => Brazil Network
        )

    [19] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Array
                        (
                            [0] => Eletrônico
                            [1] => Presentes
                        )

                )

            [promotiontypes] => Array
                (
                    [promotiontype] => De - Por
                )

            [offerdescription] => Tablets com 5% de Desconto! (cupom: 5E64-39E2-8A9E-957E)
            [offerstartdate] => 2015-11-24
            [offerenddate] => 2021-12-30
            [couponcode] => 5E64-39E2-8A9E-957E
            [couponrestriction] => Tablets.
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=414984.30
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=414984.30
&type=3&subid=0
            [advertiserid] => 39922
            [advertisername] => Ricardo Eletro
            [network] => Brazil Network
        )

    [20] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Array
                        (
                            [0] => Moda Feminina
                            [1] => Beleza e Saúde
                            [2] => Serviços
                        )

                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Black Friday
                )

            [offerdescription] => CUPOM R$ 33off código GLAMBOXTOP  para novas assinaturas mensais
            [offerstartdate] => 2015-11-19
            [offerenddate] => 2017-10-21
            [couponcode] => GLAMBOXTOP
            [couponrestriction] => Para novas assinaturas mensais
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=412179.6&type
=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=412179.6&type
=3&subid=0
            [advertiserid] => 39970
            [advertisername] => Glambox
            [network] => Brazil Network
        )

    [21] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Array
                        (
                            [0] => Moda Masculina
                            [1] => Moda Feminina
                            [2] => Calçados
                        )

                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Desconto em Porcentagem
                )

            [offerdescription] => 10% de desconto em todo o site: (Cupom: rak10desco)
            [offerstartdate] => 2015-11-18
            [offerenddate] => 2021-12-30
            [couponcode] => rak10desco
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=386671.32
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=386671.32
&type=3&subid=0
            [advertiserid] => 40052
            [advertisername] => Lojas Pompéia
            [network] => Brazil Network
        )

    [22] => Array
        (
            [@attributes] => Array
                (
                    [type] => TEXT
                )

            [categories] => Array
                (
                    [category] => Viagens
                )

            [promotiontypes] => Array
                (
                    [promotiontype] => Desconto em Porcentagem
                )

            [offerdescription] => Cupom com 8% de desconto em todo o site: RBUS8
            [offerstartdate] => 2015-11-05
            [offerenddate] => 2021-12-30
            [couponcode] => RBUS8
            [couponrestriction] => Sem restrições
            [clickurl] => http://click.linksynergy.com/fs-bin/click?id=Ef7P5x/tx5E&offerid=453319.3474
&type=3&subid=0
            [impressionpixel] => http://ad.linksynergy.com/fs-bin/show?id=Ef7P5x/tx5E&bids=453319.3474
&type=3&subid=0
            [advertiserid] => 40388
            [advertisername] => Click Bus
            [network] => Brazil Network
        )

)