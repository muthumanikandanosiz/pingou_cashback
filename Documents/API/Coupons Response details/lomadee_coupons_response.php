Array
(
    [0] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2501
                )

            [couponName] => 5% OFF nas compras acima de R$250* (exceto Adidas Boost e Asics)
            [couponCode] => CENTAURO5OFF
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmb3JpZ2VtPWxvbWFkZWU-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 5.0
                )

        )

    [1] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2502
                )

            [couponName] => 15% OFF em todo site* (aplicavel em produtos com "tag")
            [couponCode] => 15OFF2017
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmb3JpZ2VtPWxvbWFkZWU-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 15.0
                )

        )

    [2] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2503
                )

            [couponName] => 10% em Monitores e Relógios * (somente nesta lista)
            [couponCode] => 10MONITORES
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmbmV4dHVybD1odHRwOi8vZXNwb3J0ZXMuY2VudGF1cm8uY29tLmJyL3BwYy9wYXJjZXJpYV9tb25pdG9yZXNfcmVsb2dpb3Mmb3JpZ2VtPWxvbWFkZWU-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 10.0
                )

        )

    [3] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2505
                )

            [couponName] => 10% em Óculos* (somente nesta lista)
            [couponCode] => 10OCULOS
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmbmV4dHVybD1odHRwOi8vZXNwb3J0ZXMuY2VudGF1cm8uY29tLmJyL3BwYy9wYXJjZXJpYV9vY3Vsb3Mmb3JpZ2VtPWxvbWFkZWU-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 10.0
                )

        )

    [4] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2507
                )

            [couponName] => 15% em Calçados* (somente nesta lista)
            [couponCode] => 15NIKEADIDAS
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmbmV4dHVybD1odHRwOi8vZXNwb3J0ZXMuY2VudGF1cm8uY29tLmJyL3BwYy8xMG9mZi1uaWtlLWFkaWRhcy1wYXJjZXJpYXMmb3JpZ2VtPWxvbWFkZWU-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 15.0
                )

        )

    [5] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2508
                )

            [couponName] => 10% em Mochilas* (somente nesta lista)
            [couponCode] => 10MOCHILAS
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmbmV4dHVybD1odHRwOi8vZXNwb3J0ZXMuY2VudGF1cm8uY29tLmJyL3BwYy9tb2NoaWxhcy0xMG9mZi1wYXJjZXJpYXMmb3JpZ2VtPWxvbWFkZWU-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 10.0
                )

        )

    [6] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2510
                )

            [couponName] => 4 Camisetas Por R$99,99
            [couponCode] => 4CAM99
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmbmV4dFVybD1odHRwOi8vZXNwb3J0ZXMuY2VudGF1cm8uY29tLmJyL3BwYy80Q0FNOTkmb3JpZ2VtPWxvbWFkZWU-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 0.0
                )

        )

    [7] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2511
                )

            [couponName] => 6 Camisetas Por R$139,99
            [couponCode] => 6CAM139
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmbmV4dFVybD1odHRwOi8vZXNwb3J0ZXMuY2VudGF1cm8uY29tLmJyL3BwYy80Q0FNOTkmb3JpZ2VtPWxvbWFkZWU-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 0.0
                )

        )

    [8] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2512
                )

            [couponName] => 2 Camisetas Grande Marcas Por R$99,99
            [couponCode] => 2CAM99
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmbmV4dFVybD1odHRwOi8vZXNwb3J0ZXMuY2VudGF1cm8uY29tLmJyL3BwYy9sc3QtMkNBTTk5Jm9yaWdlbT1sb21hZGVl/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 0.0
                )

        )

    [9] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2515
                )

            [couponName] => 3 Peças Femininas Por R$99,99
            [couponCode] => OLHAELA99
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmbmV4dFVybD1odHRwOi8vZXNwb3J0ZXMuY2VudGF1cm8uY29tLmJyL3BwYy9vbGhhLWVsYSZvcmlnZW09bG9tYWRlZQ--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 0.0
                )

        )

    [10] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2516
                )

            [couponName] => 5 Peças Femininas Por R$149,99
            [couponCode] => OLHAELA149
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmbmV4dFVybD1odHRwOi8vZXNwb3J0ZXMuY2VudGF1cm8uY29tLmJyL3BwYy9vbGhhLWVsYSZvcmlnZW09bG9tYWRlZQ--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 0.0
                )

        )

    [11] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2517
                )

            [couponName] => 2 Bermudas Por R$99,99
            [couponCode] => 2BERMA99
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmbmV4dFVybD1odHRwOi8vZXNwb3J0ZXMuY2VudGF1cm8uY29tLmJyL3BwYy8yQkVSTUE5OSZvcmlnZW09bG9tYWRlZQ--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 0.0
                )

        )

    [12] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2518
                )

            [couponName] => 4 Bermudas Por R$179,99
            [couponCode] => 4BERMA179
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmbmV4dFVybD1odHRwOi8vZXNwb3J0ZXMuY2VudGF1cm8uY29tLmJyL3BwYy8yQkVSTUE5OSZvcmlnZW09bG9tYWRlZQ--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 0.0
                )

        )

    [13] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2519
                )

            [couponName] => 2 Polos Por R$79,99
            [couponCode] => 2POLO79
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmbmV4dFVybD1odHRwOi8vZXNwb3J0ZXMuY2VudGF1cm8uY29tLmJyL3BwYy8yUE9MTzc5Jm9yaWdlbT1sb21hZGVl/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 0.0
                )

        )

    [14] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2520
                )

            [couponName] => 4 Polos Por R$149,99
            [couponCode] => 4POLO149
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmbmV4dFVybD1odHRwOi8vZXNwb3J0ZXMuY2VudGF1cm8uY29tLmJyL3BwYy8yUE9MTzc5Jm9yaWdlbT1sb21hZGVl/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 0.0
                )

        )

    [15] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2521
                )

            [couponName] => Ganhe 50% OFF em Vestuário* - *Exceção Camisas de futebol e Nike
            [couponCode] => GANHEI15
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5jZW50YXVyby5jb20uYnIvcHJvbW9jYW8_dXRtX3NvdXJjZT1wYXJjZXJpYXNfbG9tYWRlZSZ1dG1fbWVkaXVtPXB1Ymxpc2hlciZ1dG1fY2FtcGFpZ249cGFyY2VyaWFzX2xvbWFkZWUmbmV4dHVybD1odHRwOi8vZXNwb3J0ZXMuY2VudGF1cm8uY29tLmJyL3BwYy9wcGMvZ2FuaGVpLTE1Jm9yaWdlbT1sb21hZGVl/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-05T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5714
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5714
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Centauro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo52674.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 15.0
                )

        )

    [16] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2525
                )

            [couponName] => Cupom de R$ 10,00 de desconto na primeira compra
            [couponCode] => bemvindogp
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cuZ2VyYWNhb3BldC5jb20uYnI-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-09T00:00:00.000-03:00
            [endOffer] => 2017-06-30T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 6052
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 6052
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Geração Pet
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://wwws.lomadee.com/programas/BR/6052/logo_185x140.png
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99012
                        )

                    [name] => Mais Ofertas
                )

            [price] => Array
                (
                    [discountPercentage] => 10.0
                )

        )

    [17] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2555
                )

            [couponName] => 5% OFF Áudio - Ricardo Eletro ( não é válido para produtos de marketplace)
            [couponCode] => 74F1-2DED-5101-2677
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5yaWNhcmRvZWxldHJvLmNvbS5ici9Mb2phL0F1ZGlvLzU2Njk-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-19T23:59:00.000-03:00
            [endOffer] => 2017-11-17T10:00:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5860
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5860
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Ricardo Eletro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo62687.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99012
                        )

                    [name] => Mais Ofertas
                )

            [price] => Array
                (
                    [discountPercentage] => 5.0
                )

        )

    [18] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2556
                )

            [couponName] => 5% OFF Bebês - Ricardo Eletro ( não é válido para produtos de marketplace)
            [couponCode] => 1944-7969-9271-99A5
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5yaWNhcmRvZWxldHJvLmNvbS5ici9Mb2phL0JlYmVzLzIzMjI-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-19T10:00:00.000-03:00
            [endOffer] => 2017-11-17T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5860
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5860
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Ricardo Eletro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo62687.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99012
                        )

                    [name] => Mais Ofertas
                )

            [price] => Array
                (
                    [discountPercentage] => 5.0
                )

        )

    [19] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2557
                )

            [couponName] => 5% OFF Beleza e Saúde - Ricardo Eletro ( não é válido para produtos de marketplace)
            [couponCode] => 2D8C-CB4D-FF12-0FFA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5yaWNhcmRvZWxldHJvLmNvbS5ici9Mb2phL0JlbGV6YS1lLVNhdWRlLzE-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-19T10:00:00.000-03:00
            [endOffer] => 2017-12-17T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5860
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5860
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Ricardo Eletro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo62687.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99008
                        )

                    [name] => Saúde e Beleza
                )

            [price] => Array
                (
                    [discountPercentage] => 5.0
                )

        )

    [20] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2558
                )

            [couponName] => 5% OFF Brinquedos - Ricardo Eletro ( não é válido para produtos de marketplace)
            [couponCode] => BC66-827B-201E-FB31
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5yaWNhcmRvZWxldHJvLmNvbS5ici9Mb2phL0JyaW5xdWVkb3MvNDM-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-19T10:00:00.000-03:00
            [endOffer] => 2017-11-17T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5860
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5860
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Ricardo Eletro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo62687.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99011
                        )

                    [name] => Brinquedos
                )

            [price] => Array
                (
                    [discountPercentage] => 5.0
                )

        )

    [21] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2559
                )

            [couponName] => 10% OFF Cama. Mesa e Banho - Ricardo Eletro ( não é válido para produtos de marketplace)
            [couponCode] => 44D8-625D-46AF-A1E9
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5yaWNhcmRvZWxldHJvLmNvbS5ici9Mb2phL0NhbWEtTWVzYS1lLUJhbmhvLzI0Mzg-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-19T10:00:00.000-03:00
            [endOffer] => 2017-12-17T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5860
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5860
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Ricardo Eletro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo62687.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99012
                        )

                    [name] => Mais Ofertas
                )

            [price] => Array
                (
                    [discountPercentage] => 10.0
                )

        )

    [22] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2560
                )

            [couponName] => 5% OFF Colchões - Ricardo Eletro ( não é válido para produtos de marketplace)
            [couponCode] => 832E-7F7F-3C18-3591
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5yaWNhcmRvZWxldHJvLmNvbS5ici9Mb2phL0NvbGNob2VzLzI3NzI-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-01-19T10:00:00.000-03:00
            [endOffer] => 2017-11-27T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5860
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5860
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Ricardo Eletro
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo62687.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99012
                        )

                    [name] => Mais Ofertas
                )

            [price] => Array
                (
                    [discountPercentage] => 5.0
                )

        )

    [23] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2583
                )

            [couponName] => Desconto de 5%. Exclusivo loja online PANDORA
            [couponCode] => PDRFEV175
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5wYW5kb3Jham9pYXMuY29tLmJyLw--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-01T00:00:00.000-03:00
            [endOffer] => 2017-02-28T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 6015
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 6015
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Pandora Joias
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://wwws.lomadee.com/programas/BR/6015/logo_185x140.png
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99004
                        )

                    [name] => Moda e Acessórios
                )

            [price] => Array
                (
                    [discountPercentage] => 5.0
                )

        )

    [24] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2591
                )

            [couponName] => Ganhe mais 10% OFF no carrinho de compras
            [couponCode] => FOLIA10
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5mdXRmYW5hdGljcy5jb20uYnIv/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-09T00:00:00.000-03:00
            [endOffer] => 2017-02-28T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5966
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5966
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => FutFanatics
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://wwws.lomadee.com/programas/BR/5966/logo_185x140.png
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 10.0
                )

        )

    [25] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2592
                )

            [couponName] => Cupom 5% OFF MOTO G4 16GB BRANCO
            [couponCode] => URLCUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cuY2FycmVmb3VyLmNvbS5ici9TbWFydHBob25lLU1vdG9yb2xhLU1vdG8tRy00LUdlcmFjYW8tWFQxNjAzLVBsYXktRFRWLUVFLTE2R0ItQnJhbmNvLTRHLVRlbGEtNS1DYW1lcmEtOE1QLUFuZHJvaWQtNi0wL3AvOTk3ODIxNg--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-01T00:00:00.000-03:00
            [endOffer] => 2017-02-28T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5630
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5630
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Carrefour
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo51010.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 77
                        )

                    [name] => Celular/Smartphone
                )

            [price] => Array
                (
                    [discountPercentage] => 5.0
                )

        )

    [26] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2593
                )

            [couponName] => Cupom 5% OFF MOTO G4 16GB PRETO
            [couponCode] => URLCUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cuY2FycmVmb3VyLmNvbS5ici9Nb3RvLUctNC1HZXJhY2FvLVhUMTYwMy1QbGF5LURUVi1Db2xvcnMtMTZHQi1QcmV0by9wLzk5NzgyMjQ-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-01T00:00:00.000-03:00
            [endOffer] => 2017-02-28T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5630
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5630
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Carrefour
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo51010.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 77
                        )

                    [name] => Celular/Smartphone
                )

            [price] => Array
                (
                    [discountPercentage] => 5.0
                )

        )

    [27] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2595
                )

            [couponName] => 10% em Toda Loja
            [couponCode] => LOMA10
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5vbGhvZmFzaGlvbi5jb20uYnIvbG9qYS1kZS1vY3Vsb3MtbWVsaG9yZXMtbWFyY2Fz/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-01T00:00:00.000-03:00
            [endOffer] => 2017-03-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 6012
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 6012
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Olho Fashion
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://wwws.lomadee.com/programas/BR/6012/logo_185x140.png
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99004
                        )

                    [name] => Moda e Acessórios
                )

            [price] => Array
                (
                    [discountPercentage] => 10.0
                )

        )

    [28] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2603
                )

            [couponName] => Até 50% de desconto em camisetas selecionadas
            [couponCode] => URL CUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cuY2FtaXNldGVyaWEuY29tL2NhdGFsb2cuYXNweD91dG1fc291cmNlPWFmaWxpYWRvcyZ1dG1fbWVkaXVtPWFmaWxpYWRvcyZ1dG1fY29udGVudD1jYXRhbG9nbGluayZ1dG1fY2FtcGFpZ249Y2FybmF2YWwyMDE3JnZjb2Q9YWZpbGlhZG9zJnBvcj1wcmVjbyZvcmRlbT1hc2M-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-08T00:00:00.000-03:00
            [endOffer] => 2017-02-28T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5736
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5736
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Camiseteria
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://wwws.lomadee.com/programas/BR/5736/logo_185x140.png
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99004
                        )

                    [name] => Moda e Acessórios
                )

            [price] => Array
                (
                    [discountPercentage] => 50.0
                )

        )

    [29] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2613
                )

            [couponName] => 15% 0FF Netshoes    (PROMOÇÃO NÃO CUMULATIVA. VÁLIDA SOMENTE PARA PRODUTOS VENDIDOS E ENTREGUES PELA NETSHOES. EXCETO COMBOS, BICICLETAS, EQUIP. FITNESS, MONITORES CARDÍACOS, MONITORAMENTO ESPORTIVO, ELETRÔNICOS, JOGOS, GAMES,  SUPLEMENTOS, PRODUTOS COM SELOS COLLECTION , LANÇAMENTOS, CUPOM GANHE 100*, Selo Natal e Selo Zattini)
            [couponCode] => NETSUPER
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5uZXRzaG9lcy5jb20uYnIvbHN0L2NhbmFpcz9Ocj1OT1Qoc2t1LnNwZWNpYWxFdmVudDpuYXRhbCksTk9UKHNrdS5leGNsdXNpdmU6MSksTk9UKHNrdS5sYW5jYW1lbnRvOjEpJmNhbXBhaWduPW1lX2FmaWwtMjAmdXRtX3NvdXJjZT1tZS1zX2xtZGVfXyZ1dG1fbWVkaXVtPXBvc3QmdXRtX2NhbXBhaWduPW1lLXNfbG1kZS1fLXRuZmlsZF9fdGNhX19fLV8tcG9zdC1fLV92YXJfbWVfX2xtZGVfXzE1b2ZmLUZldg--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-13T10:00:00.000-03:00
            [endOffer] => 2017-02-28T23:00:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5783
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5783
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Netshoes
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo54189.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 15.0
                )

        )

    [30] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2618
                )

            [couponName] => Cupom 15% off - Suplementos Netshoes                              (EXCETO KITS e as seguintes marcas : OPTIMUM /ATLHETICA/ INTEGRALMEDICA/ BLACK SKULL/ MAX TITANIUM)
            [couponCode] => SUPLE15
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5uZXRzaG9lcy5jb20uYnIvc3VwbGVtZW50b3M_TnI9Tk9UKHByb2R1Y3QucHJvZHVjdFR5cGUuZGlzcGxheU5hbWU6S2l0cykmY2FtcGFpZ249bWVfc3VwbGUmdXRtX3NvdXJjZT1tZS1zX2xtZGVfXyZ1dG1fbWVkaXVtPXBvc3QmdXRtX2NhbXBhaWduPW1lLXNfbG1kZS1fLXRuZmlsZF9fdGNhX19fLV8tcG9zdC1fLV92YXJfbWVfX2xtZGVfX3N1cGxlMTU-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-14T00:01:00.000-03:00
            [endOffer] => 2017-02-28T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5783
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5783
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Netshoes
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo54189.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 15.0
                )

        )

    [31] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2619
                )

            [couponName] => Suplementos - Midway 35% off Netshoes   (
            [couponCode] => MDW35
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5uZXRzaG9lcy5jb20uYnIvc3VwbGVtZW50b3MvbWlkd2F5LWxhYnM_Y2FtcGFpZ249bWVfbWlkLTMwJnV0bV9zb3VyY2U9bWUtc19sbWRlX18mdXRtX21lZGl1bT1wb3N0JnV0bV9jYW1wYWlnbj1tZS1zX2xtZGUtXy10bmZpbGRfX3RjYV9fXy1fLXBvc3QtXy1fdmFyX21lX19sbWRlX19taWR3YXkzNQ--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-14T00:01:00.000-03:00
            [endOffer] => 2017-02-28T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5783
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5783
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Netshoes
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo54189.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 35.0
                )

        )

    [32] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2621
                )

            [couponName] => Outlet Beleza Até 50%OFF
            [couponCode] => URLCUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL2J1c2NhLm9ub2ZyZS5jb20uYnIvcHBjL291dGxldG9ub2ZyZT91dG1fc291cmNlPUxvbWFkZWVfRGVlcGxpbmsmdXRtX21lZGl1bT1BZmlsaWFkb3MmdXRtX2NhbXBhaWduPU91dGxldF9Pbm9mcmUmYWNhbz0zJnBhcnRuZXI9NjA4/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-14T00:00:00.000-03:00
            [endOffer] => 2017-03-01T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 6009
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 6009
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Onofre
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo1152706.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99008
                        )

                    [name] => Saúde e Beleza
                )

            [price] => Array
                (
                    [discountPercentage] => 50.0
                )

        )

    [33] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2622
                )

            [couponName] => Seleção de desconto da Drogaria Onofre para você! Confira!
            [couponCode] => URLCUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL2J1c2NhLm9ub2ZyZS5jb20uYnIvcHBjL0JhaXhhcmFtX2RlX1ByZWNvP3V0bV9zb3VyY2U9TG9tYWRlZV9EZWVwbGluayZ1dG1fbWVkaXVtPUFmaWxpYWRvcyZ1dG1fY2FtcGFpZ249QmFpeGFyYW1fUHJlY28mYWNhbz0zJnBhcnRuZXI9NjA4/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-14T00:00:00.000-03:00
            [endOffer] => 2017-03-01T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 6009
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 6009
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Onofre
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo1152706.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99008
                        )

                    [name] => Saúde e Beleza
                )

            [price] => Array
                (
                    [discountPercentage] => 50.0
                )

        )

    [34] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2623
                )

            [couponName] => Compre e Ganhe! Brindes especiais!
            [couponCode] => URLCUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL2J1c2NhLm9ub2ZyZS5jb20uYnIvcHBjL0NvbXByZV9HYW5oZT91dG1fc291cmNlPUxvbWFkZWVfRGVlcGxpbmsmdXRtX21lZGl1bT1BZmlsaWFkb3MmdXRtX2NhbXBhaWduPUNvbXByZV9HYW5oZSZhY2FvPTMmcGFydG5lcj02MDg-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-14T00:00:00.000-03:00
            [endOffer] => 2017-03-01T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 6009
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 6009
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Onofre
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo1152706.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99008
                        )

                    [name] => Saúde e Beleza
                )

            [price] => Array
                (
                    [discountPercentage] => 30.0
                )

        )

    [35] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2624
                )

            [couponName] => Kits com Desconto! Confira nossas ofertas!
            [couponCode] => URLCUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL2J1c2NhLm9ub2ZyZS5jb20uYnIvcHBjL0tpdHNfQ29tX0Rlc2NvbnRvcz91dG1fc291cmNlPUxvbWFkZWVfRGVlcGxpbmsmdXRtX21lZGl1bT1BZmlsaWFkb3MmdXRtX2NhbXBhaWduPUtpdHNfRGVzY29udG8mYWNhbz0zJnBhcnRuZXI9NjA4/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-14T00:00:00.000-03:00
            [endOffer] => 2017-03-01T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 6009
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 6009
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Onofre
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo1152706.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99008
                        )

                    [name] => Saúde e Beleza
                )

            [price] => Array
                (
                    [discountPercentage] => 50.0
                )

        )

    [36] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2625
                )

            [couponName] => Cupom 15% OFF - VOLTA ÀS AULAS
            [couponCode] => 15OFFVOLTAASAULAS
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cuY2FycmVmb3VyLmNvbS5ici9tYWxhcy1lLW1vY2hpbGFzP3Rlcm1vPSUzQXByaWNlLWFzYyUzQW5hdmVnYWNhbyUzQW1vY2hpbGFz/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-14T00:00:00.000-03:00
            [endOffer] => 2017-02-28T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5630
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5630
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Carrefour
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo51010.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99012
                        )

                    [name] => Mais Ofertas
                )

            [price] => Array
                (
                    [discountPercentage] => 15.0
                )

        )

    [37] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2626
                )

            [couponName] => Cupom 15% OFF - VOLTA ÀS AULAS
            [couponCode] => 15OFFVOLTAASAULAS
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cuY2FycmVmb3VyLmNvbS5ici9tYWxhcy1lLW1vY2hpbGFzP3Rlcm1vPSUzQXByaWNlLWFzYyUzQW5hdmVnYWNhbyUzQWxhbmNoZWlyYQ--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-14T00:00:00.000-03:00
            [endOffer] => 2017-02-28T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5630
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5630
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Carrefour
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo51010.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99012
                        )

                    [name] => Mais Ofertas
                )

            [price] => Array
                (
                    [discountPercentage] => 15.0
                )

        )

    [38] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2627
                )

            [couponName] => Cupom 15% OFF - VOLTA ÀS AULAS
            [couponCode] => 15OFFVOLTAASAULAS
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cuY2FycmVmb3VyLmNvbS5ici9tYWxhcy1lLW1vY2hpbGFzP3Rlcm1vPSUzQXByaWNlLWFzYyUzQW5hdmVnYWNhbyUzQWxhbmNoZWlyYQ--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-14T00:00:00.000-03:00
            [endOffer] => 2017-02-28T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5630
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5630
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Carrefour
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo51010.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99012
                        )

                    [name] => Mais Ofertas
                )

            [price] => Array
                (
                    [discountPercentage] => 15.0
                )

        )

    [39] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2628
                )

            [couponName] => 10% de desconto em Lazer e Jogos, Fitness e Bicicletas
            [couponCode] => 10OFFESPORTES
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cuY2FycmVmb3VyLmNvbS5ici9kaWNhcy9zYXVkZS1lLWJlbS1lc3Rhci9jaWNsaXNtbw--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-15T00:00:00.000-03:00
            [endOffer] => 2017-02-28T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5630
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5630
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Carrefour
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo51010.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 10.0
                )

        )

    [40] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2629
                )

            [couponName] => 5% de desconto na categoria de Pneus
            [couponCode] => 5OFFPNEUS
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cuY2FycmVmb3VyLmNvbS5ici9kaWNhcy9hdXRvLWUtZmVycmFtZW50YXMvcG5ldXM-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-15T00:00:00.000-03:00
            [endOffer] => 2017-02-28T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5630
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5630
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Carrefour
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo51010.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99009
                        )

                    [name] => Automotivo
                )

            [price] => Array
                (
                    [discountPercentage] => 5.0
                )

        )

    [41] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2630
                )

            [couponName] => 10% de desconto para Som Automotivo e GPS
            [couponCode] => 10OFFSOMAUTOMOTIVO
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cuY2FycmVmb3VyLmNvbS5ici9zb20tYXV0b21vdGl2by1lLWdwcw--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-15T00:00:00.000-03:00
            [endOffer] => 2017-02-28T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5630
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5630
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Carrefour
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo51010.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99009
                        )

                    [name] => Automotivo
                )

            [price] => Array
                (
                    [discountPercentage] => 10.0
                )

        )

    [42] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2631
                )

            [couponName] => 20% de desconto em Produtos Gilette
            [couponCode] => 20OFFGILLETTE
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cuY2FycmVmb3VyLmNvbS5ici9zYXVkZS1lLWJlbGV6YT90ZXJtbz0lM0FyZWxldmFuY2UlM0FuYXZlZ2FjYW8lM0FhcGFyZWxoby1kZS1iYXJiZWFy/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-15T00:00:00.000-03:00
            [endOffer] => 2017-02-28T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5630
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5630
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Carrefour
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo51010.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99008
                        )

                    [name] => Saúde e Beleza
                )

            [price] => Array
                (
                    [discountPercentage] => 20.0
                )

        )

    [43] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2633
                )

            [couponName] => 5% off com pagamento paypal
            [couponCode] => 5offpaypalnomamute
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5tZWdhbWFtdXRlLmNvbS5ici8-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-15T00:00:00.000-03:00
            [endOffer] => 2017-02-22T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5800
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5800
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Mega Mamute
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo590367.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99001
                        )

                    [name] => Informática
                )

            [price] => Array
                (
                    [discountPercentage] => 5.0
                )

        )

    [44] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2635
                )

            [couponName] => Tapetes com 10% de desconto
            [couponCode] => URL CUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cud2FsbWFydC5jb20uYnIvY2F0ZWdvcmlhL2NhbWEtbWVzYS1lLWJhbmhvL3RhcGV0ZXMvP2ZxPUM6NDY3OC81MjIzLzUyMjcvJlBTPTIwJnV0bV9tZWRpdW09YWZpbGlhZG9zJnV0bV9zb3VyY2U9bG9tYWRlZSZ1dG1fY2FtcGFpZ249b2ZmZXJzXzE2MDImdXRtX3Rlcm09b2ZmZXJzXzE2MDI-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-20T00:00:00.000-03:00
            [endOffer] => 2017-02-23T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5576
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5576
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Walmart
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo255256.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99007
                        )

                    [name] => Casa e Decoração
                )

            [price] => Array
                (
                    [discountPercentage] => 10.0
                )

        )

    [45] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2639
                )

            [couponName] => Aquecedores de água a gás com 5% de desconto
            [couponCode] => URL CUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cud2FsbWFydC5jb20uYnIvbG5kL21lbnVidXNjYT8_ZnE9QzoxMjQ5LzE0MDgvMTQwOS8mUFM9MTAwJnJlZnM9NjA0MDY4OzEwNTg5Mzs2MDMyMTYmTz1PcmRlckJ5VG9wU2FsZURFU0MmdXRtX21lZGl1bT1hZmlsaWFkb3MmdXRtX3NvdXJjZT1sb21hZGVlJnV0bV9jYW1wYWlnbj1vZmZlcnNfMTYwMiZ1dG1fdGVybT1vZmZlcnNfMTYwMg--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-16T00:00:00.000-03:00
            [endOffer] => 2017-02-22T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5576
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5576
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Walmart
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo255256.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99007
                        )

                    [name] => Casa e Decoração
                )

            [price] => Array
                (
                    [discountPercentage] => 5.0
                )

        )

    [46] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2640
                )

            [couponName] => Toda a linha de Cofres com 5% de desconto
            [couponCode] => URL CUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cud2FsbWFydC5jb20uYnIvbG5kL21lbnVidXNjYT9mcT1DOjEyNDkvMTI2MC8mUFM9MTAwJnJlZnM9NDAyMTQ1OzQxMDg3OTs0MDIxNDEmTz1PcmRlckJ5VG9wU2FsZURFU0MmdXRtX21lZGl1bT1hZmlsaWFkb3MmdXRtX3NvdXJjZT1sb21hZGVlJnV0bV9jYW1wYWlnbj1vZmZlcnNfMTYwMiZ1dG1fdGVybT1vZmZlcnNfMTYwMg--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-16T00:00:00.000-03:00
            [endOffer] => 2017-02-22T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5576
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5576
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Walmart
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo255256.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99007
                        )

                    [name] => Casa e Decoração
                )

            [price] => Array
                (
                    [discountPercentage] => 5.0
                )

        )

    [47] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2641
                )

            [couponName] => Iluminação com 3% de desconto
            [couponCode] => URL CUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cud2FsbWFydC5jb20uYnIvbG5kL21lbnVidXNjYT9mcT1DOjEyNDkvMTI3MC8mUFM9MTAwJnJlZnNta3Q9MTE2MDIyMDszNDkzMDUxOzI4MTQwMTkmTz1PcmRlckJ5VG9wU2FsZURFU0MmdXRtX21lZGl1bT1hZmlsaWFkb3MmdXRtX3NvdXJjZT1sb21hZGVlJnV0bV9jYW1wYWlnbj1vZmZlcnNfMTYwMiZ1dG1fdGVybT1vZmZlcnNfMTYwMg--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-16T00:00:00.000-03:00
            [endOffer] => 2017-02-22T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5576
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5576
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Walmart
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo255256.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99007
                        )

                    [name] => Casa e Decoração
                )

            [price] => Array
                (
                    [discountPercentage] => 3.0
                )

        )

    [48] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2642
                )

            [couponName] => Festival de Inox a partir de R$ 29,90
            [couponCode] => URL CUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cud2FsbWFydC5jb20uYnIvbG5kL21lbnVidXNjYT9mcT1IOjMyNTAwJk89T3JkZXJCeVRvcFNhbGVERVNDJnJlZnM9NDgzMDg3OzI0MjE3ODs4OTM1NyZ1dG1fbWVkaXVtPWFmaWxpYWRvcyZ1dG1fc291cmNlPWxvbWFkZWUmdXRtX2NhbXBhaWduPW9mZmVyc18xNjAyJnV0bV90ZXJtPW9mZmVyc18xNjAy/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-16T00:00:00.000-03:00
            [endOffer] => 2017-02-22T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5576
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5576
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Walmart
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo255256.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99007
                        )

                    [name] => Casa e Decoração
                )

            [price] => Array
                (
                    [discountPercentage] => 30.0
                )

        )

    [49] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2643
                )

            [couponName] => Panelas de Pressão com até 40% de desconto
            [couponCode] => URL CUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cud2FsbWFydC5jb20uYnIvbG5kL21lbnVidXNjYT9mcT1IOjU4NTEmTz1PcmRlckJ5VG9wU2FsZURFU0MmcmVmcz0zNTY3MDszODAyODY7NDEzNzcxJnV0bV9tZWRpdW09YWZpbGlhZG9zJnV0bV9zb3VyY2U9bG9tYWRlZSZ1dG1fY2FtcGFpZ249b2ZmZXJzXzE2MDImdXRtX3Rlcm09b2ZmZXJzXzE2MDI-/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-16T00:00:00.000-03:00
            [endOffer] => 2017-02-22T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5576
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5576
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Walmart
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo255256.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99007
                        )

                    [name] => Casa e Decoração
                )

            [price] => Array
                (
                    [discountPercentage] => 40.0
                )

        )

    [50] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2644
                )

            [couponName] => 10% de desconto em todo o site (limite de R$ 17,00)
            [couponCode] => MERCADOPAGO
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cuY2xpY2tidXMuY29tLmJyLw--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-20T00:01:00.000-03:00
            [endOffer] => 2017-02-22T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5986
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5986
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Click Bus
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://wwws.lomadee.com/programas/BR/5986/logo_185x140.png
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99006
                        )

                    [name] => Turismo
                )

            [price] => Array
                (
                    [discountPercentage] => 10.0
                )

        )

    [51] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2646
                )

            [couponName] => Ganhe 1 miniatura para cabelos nas compras acima de 179,00
            [couponCode] => MEUMINI
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL3d3dy5lcG9jYWNvc21ldGljb3MuY29tLmJy/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-20T00:00:00.000-03:00
            [endOffer] => 2017-03-14T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5761
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5761
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Época Cosméticos Perfumaria
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo130016.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99008
                        )

                    [name] => Saúde e Beleza
                )

            [price] => Array
                (
                    [discountPercentage] => 10.0
                )

        )

    [52] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2647
                )

            [couponName] => Linha de Esporte e Lazer com Frete Grátis Brasil - exceto 3P e tiro esportivo
            [couponCode] => URL CUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cud2FsbWFydC5jb20uYnIvbG5kL21lbnVidXNjYT9mcT1IOjMyMTY4JnJlZnM9NDEzNzc1LDYwMjM3Niw1MzY5MzMmdXRtX21lZGl1bT1hZmlsaWFkb3MmdXRtX3NvdXJjZT1sb21hZGVlJnV0bV9jYW1wYWlnbj1vZmZlcnNfMjAwMiZ1dG1fdGVybT1vZmZlcnNfMjAwMg--/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-20T00:00:00.000-03:00
            [endOffer] => 2017-02-22T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5576
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5576
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Walmart
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo255256.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99005
                        )

                    [name] => Esportes e Lazer
                )

            [price] => Array
                (
                    [discountPercentage] => 0.0
                )

        )

    [53] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2648
                )

            [couponName] => Waldays - Os melhores produtos com até 30% de desconto
            [couponCode] => URL CUPONADA
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cHM6Ly93d3cud2FsbWFydC5jb20uYnIvZXNwZWNpYWwvd2FsbWFydC1kYXk_ZnE9SDozMjU4MyZ1dG1fbWVkaXVtPWFmaWxpYWRvcyZ1dG1fc291cmNlPWxvbWFkZWUmdXRtX2NhbXBhaWduPW9mZmVyc18yMDAyJnV0bV90ZXJtPW9mZmVyc18yMDAy/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-20T00:00:00.000-03:00
            [endOffer] => 2017-02-24T23:50:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 5576
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 5576
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Walmart
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => http://imagem.buscape.com.br/vitrine/logo255256.gif
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99012
                        )

                    [name] => Mais Ofertas
                )

            [price] => Array
                (
                    [discountPercentage] => 30.0
                )

        )

    [54] => Array
        (
            [@attributes] => Array
                (
                    [id] => 2649
                )

            [couponName] => Imaginarium com 10% OFF - É só aqui!
            [couponCode] => presenteimg
            [links] => Array
                (
                    [link] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://redir.lomadee.com/v2/direct/aHR0cDovL2xvamEuaW1hZ2luYXJpdW0uY29tLmJyLz91dG1fY2FtcGFpZ249bG9tYWRlZSZ1dG1fbWVkaXVtPWxvbWFkZWUmdXRtX3NvdXJjZT1sb21hZGVl/35754790/6202
                                    [type] => offer
                                )

                        )

                )

            [startOffer] => 2017-02-21T00:00:00.000-03:00
            [endOffer] => 2017-12-31T23:59:00.000-03:00
            [seller] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 6016
                            [isTrustedStore] => false
                            [pagamentoDigital] => false
                            [advertiserId] => 6016
                            [oneClickBuy] => false
                            [oneClickBuyValue] => 0
                            [cpcDifferentiated] => false
                        )

                    [sellerName] => Imaginarium
                    [thumbnail] => Array
                        (
                            [@attributes] => Array
                                (
                                    [url] => https://wwws.lomadee.com/programas/BR/6016/logo_185x140.png
                                )

                        )

                )

            [category] => Array
                (
                    [@attributes] => Array
                        (
                            [id] => 99012
                        )

                    [name] => Mais Ofertas
                )

            [price] => Array
                (
                    [discountPercentage] => 10.0
                )

        )

)
