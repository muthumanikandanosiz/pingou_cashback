<?php
global $vbegy_sidebar,$post_style,$vbegy_sidebar_all,$posts_meta,$post_type_option,$post_author,$post_excerpt_title,$post_excerpt,$post_share,$page_tamplate,$post;
if ($page_tamplate != true) {
	$posts_meta = vpanel_options("post_meta");
	$post_type_option = vpanel_options("post_type");
	$post_author = vpanel_options("post_author");
	$post_excerpt_title = vpanel_options("post_excerpt_title");
	$post_excerpt = vpanel_options("post_excerpt");
	$post_share = vpanel_options("post_share");
}
$post_excerpt_title = (isset($post_excerpt_title) && $post_excerpt_title != ""?$post_excerpt_title:5);
$post_excerpt = (isset($post_excerpt) && $post_excerpt != ""?$post_excerpt:40);

$page_id = $post->ID;
if ($page_tamplate == true || is_page() || is_category()) {
	$page_category = "yes";
}else {
	$page_category = "";
}
$vbegy_sidebar = $vbegy_sidebar_all;
$author_by = vpanel_options("author_by");
if ($post_style == "style_4" || $post_style == "style_5") {
	if ( have_posts() ) : ?>
		<div class="block-box<?php echo ($vbegy_sidebar == "full"?" block-box-full":"")?>">
			<?php if ($page_category == "yes") {?>
				<div class="block-box-title"><?php echo ($page_tamplate == true || is_page()?get_the_title($page_id):single_cat_title( '', false ));?></div>
			<?php }?>
			<div class="block-box-1 <?php echo ($post_style == "style_4"?"block-recent-1":"").($post_style == "style_5"?" block-box-2 block-box-4":"")?>">
				<ul>
					<?php while ( have_posts() ) : the_post();
						$vbegy_what_post = rwmb_meta('vbegy_what_post','select',$post->ID);
						$video_type = rwmb_meta('vbegy_video_post_type',"select",$post->ID);
						$post_username = get_post_meta($post->ID, 'post_username',true);
						$post_email = get_post_meta($post->ID, 'post_email',true);?>
						<li<?php echo ($post_style == "style_5"?" class='block-box-first'":"")?>>
							<?php if (has_post_thumbnail()) {?>
								<div class="block-box-img">
									<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark">
										<i class="fa fa-<?php if (is_sticky()) {?>thumb-tack<?php }else if ($vbegy_what_post == "google") {?>map-marker<?php }else if ($vbegy_what_post == "audio") {?>volume-up<?php }else if ($vbegy_what_post == "video") {if ($video_type == 'youtube') {?>youtube-play<?php }else if ($video_type == 'vimeo') {?>vimeo-square<?php }else if ($video_type == 'daily') {?>video-camera<?php }?><?php }else if ($vbegy_what_post == "slideshow") {?>film<?php }else if ($vbegy_what_post == "quote") {?>quote-left<?php }else if ($vbegy_what_post == "link") {?>link<?php }else if ($vbegy_what_post == "soundcloud") {?>soundcloud<?php }else if ($vbegy_what_post == "twitter") {?>twitter<?php }else if ($vbegy_what_post == "facebook") {?>facebook<?php }else {if (has_post_thumbnail()) {?>image<?php }else {?>file-text<?php }}?>"></i>
										<?php if ($post_style == "style_5") {
											echo get_aq_resize_img('full',345,240);
										}else {
											echo get_aq_resize_img('full',130,130);
										}
										?>
									</a>
								</div>
							<?php }?>
							<div class="block-box-content">
								<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php the_title();?></a>
								<?php if ($posts_meta == "on") {
									if ($author_by == 'on') {?>
										<span><i class="fa fa-user"></i><?php _e("by","vbegy")?> : <?php echo ($post->post_author > 0?the_author_posts_link():$post_username);?></span>
									<?php }?>
									<span><i class="fa fa-clock-o"></i><?php the_time('F j, Y');?></span>
								<?php }?>
								<div class="clearfix"></div>
								<p><?php excerpt($post_excerpt);if ($post_style == "style_4") {?> <a class="color" href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php _e("Continue Reading","vbegy")?></a><?php }?></p>
								<?php if ($post_style == "style_5") {?>
									<a class="button post-more" href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php _e("Continue Reading","vbegy")?></a>
								<?php }?>
							</div>
						</li>
					<?php endwhile;?>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div><!-- End block-box -->
		<div class="clearfix"></div>
	<?php else :?>
		<div class="post">
			<div class="post-wrap">
				<div class="post-inner">
					<div class="page-404">
						<h3><?php _e("Sorry, no posts yet .","vbegy")?></h3><a class="button" href="<?php echo esc_url(home_url('/'));?>"><?php _e("Back To Homepage","vbegy")?></a>
					</div>
					<div class="clearfix"></div>
				</div><!-- End post-inner -->
			</div><!-- End post-wrap -->
		</div><!-- End post -->
		<div class="clearfix"></div>
	<?php endif;
}else if ($post_style == "style_6") {
	if ( have_posts() ) :?>
		<div class="block-box<?php echo ($vbegy_sidebar == "full"?" block-box-full block-box-full-5":"")?>">
			<?php if ($page_category == "yes") {?>
				<div class="block-box-title"><?php echo ($page_tamplate == true || is_page()?get_the_title($page_id):single_cat_title( '', false ));?></div>
			<?php }?>
			<div class="block-box-1 block-box-5">
				<ul>
					<?php while ( have_posts() ) : the_post();
						$vbegy_what_post = rwmb_meta('vbegy_what_post','select',$post->ID);
						$video_type = rwmb_meta('vbegy_video_post_type',"select",$post->ID);
						$post_username = get_post_meta($post->ID, 'post_username',true);
						$post_email = get_post_meta($post->ID, 'post_email',true);?>
						<li class="block-box-first">
							<?php if (has_post_thumbnail()) {?>
								<div class="block-box-img">
									<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark">
										<i class="fa fa-<?php if (is_sticky()) {?>thumb-tack<?php }else if ($vbegy_what_post == "google") {?>map-marker<?php }else if ($vbegy_what_post == "audio") {?>volume-up<?php }else if ($vbegy_what_post == "video") {if ($video_type == 'youtube') {?>youtube-play<?php }else if ($video_type == 'vimeo') {?>vimeo-square<?php }else if ($video_type == 'daily') {?>video-camera<?php }?><?php }else if ($vbegy_what_post == "slideshow") {?>film<?php }else if ($vbegy_what_post == "quote") {?>quote-left<?php }else if ($vbegy_what_post == "link") {?>link<?php }else if ($vbegy_what_post == "soundcloud") {?>soundcloud<?php }else if ($vbegy_what_post == "twitter") {?>twitter<?php }else if ($vbegy_what_post == "facebook") {?>facebook<?php }else {if (has_post_thumbnail()) {?>image<?php }else {?>file-text<?php }}?>"></i>
										<?php echo get_aq_resize_img('full',345,165);?>
									</a>
								</div>
							<?php }?>
							<div class="block-box-content">
								<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php excerpt_title($post_excerpt_title);?></a>
								<?php if ($author_by == 'on') {?>
										<span><i class="fa fa-user"></i><?php _e("by","vbegy")?> : <?php echo ($post->post_author > 0?the_author_posts_link():$post_username);?></span>
									<?php }?>
								<span><i class="fa fa-clock-o"></i><?php the_time('F j, Y');?></span>
								<div class="clearfix"></div>
								<p><?php excerpt($post_excerpt);?></p>
								<a class="button post-more" href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php _e("Continue Reading","vbegy")?></a>
							</div>
						</li>
					<?php endwhile;?>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div><!-- End block-box -->
		<div class="clearfix"></div>
	<?php else :?>
		<div class="post">
			<div class="post-wrap">
				<div class="post-inner">
					<div class="page-404">
						<h3><?php _e("Sorry, no posts yet .","vbegy")?></h3><a class="button" href="<?php echo esc_url(home_url('/'));?>"><?php _e("Back To Homepage","vbegy")?></a>
					</div>
					<div class="clearfix"></div>
				</div><!-- End post-inner -->
			</div><!-- End post-wrap -->
		</div><!-- End post -->
	<?php endif;
}else {
	if (have_posts() ) : while (have_posts() ) : the_post();
		$vbegy_what_post = rwmb_meta('vbegy_what_post','select',$post->ID);
		$vbegy_google = rwmb_meta('vbegy_google',"textarea",$post->ID);
		$vbegy_quote_author = rwmb_meta('vbegy_quote_author',"text",$post->ID);
		$vbegy_quote_background = rwmb_meta('vbegy_quote_background',"color",$post->ID);
		$quote_background = (isset($vbegy_quote_background) && $vbegy_quote_background != ""?"style='background-color:".$vbegy_quote_background.";border-color:".$vbegy_quote_background."'":"");
		$vbegy_quote_icon_color = rwmb_meta('vbegy_quote_icon_color',"color",$post->ID);
		$quote_icon_color = (isset($vbegy_quote_icon_color) && $vbegy_quote_icon_color != ""?"style='color:".$vbegy_quote_icon_color.";'":(isset($vbegy_quote_background) && $vbegy_quote_background != ""?"style='color:#FFF;'":""));
		$vbegy_twitter_background = rwmb_meta('vbegy_twitter_background',"color",$post->ID);
		$twitter_background = (isset($vbegy_twitter_background) && $vbegy_twitter_background != ""?"style='background-color:".$vbegy_twitter_background."'":"");
		$vbegy_facebook_background = rwmb_meta('vbegy_facebook_background',"color",$post->ID);
		$facebook_background = (isset($vbegy_facebook_background) && $vbegy_facebook_background != ""?"style='background-color:".$vbegy_facebook_background."'":"");
		$vbegy_soundcloud_background = rwmb_meta('vbegy_soundcloud_background',"color",$post->ID);
		$soundcloud_background = (isset($vbegy_soundcloud_background) && $vbegy_soundcloud_background != ""?"style='background-color:".$vbegy_soundcloud_background."'":"");
		$vbegy_link_target = rwmb_meta('vbegy_link_target',"select",$post->ID);
		$vbegy_link = rwmb_meta('vbegy_link',"text",$post->ID);
		$vbegy_link_title = rwmb_meta('vbegy_link_title',"text",$post->ID);
		$vbegy_link_background = rwmb_meta('vbegy_link_background',"color",$post->ID);
		$link_background = (isset($vbegy_link_background) && $vbegy_link_background != ""?"style='background-color:".$vbegy_link_background."'":"");
		$vbegy_link_icon_color = rwmb_meta('vbegy_link_icon_color',"color",$post->ID);
		$link_icon_color = (isset($vbegy_link_icon_color) && $vbegy_link_icon_color != ""?"style='color:".$vbegy_link_icon_color.";'":(isset($vbegy_link_background) && $vbegy_link_background != ""?"style='color:#FFF;'":""));
		$vbegy_slideshow_type = rwmb_meta('vbegy_slideshow_type','select',$post->ID);
		$video_id = rwmb_meta('vbegy_video_post_id',"text",$post->ID);
		$video_type = rwmb_meta('vbegy_video_post_type',"select",$post->ID);
		if ($video_type == 'youtube') {
			$type = "http://www.youtube.com/embed/".$video_id;
		}else if ($video_type == 'vimeo') {
			$type = "http://player.vimeo.com/video/".$video_id;
		}else if ($video_type == 'daily') {
			$type = "http://www.dailymotion.com/swf/video/".$video_id;
		}
		
	    if (is_sticky()) {
	    	$post_type = " sticky_post";
	    }else if ($vbegy_what_post == "google") {
	    	$post_type = " google_post";
	    }else if ($vbegy_what_post == "image_lightbox") {
	    	$post_type = " post-lightbox";
	    }else if ($vbegy_what_post == "audio") {
	    	$post_type = " post-audio";
	    }else if ($vbegy_what_post == "video") {
	    	if ($video_type == 'youtube') {
	        	$post_type = " video_y_post";
	    	}else if ($video_type == 'vimeo') {
	        	$post_type = " video_v_post";
	    	}else if ($video_type == 'daily') {
	        	$post_type = " video_d_post";
	    	}
	    }else if ($vbegy_what_post == "slideshow") {
	    	$post_type = " post-gallery";
	    }else if ($vbegy_what_post == "quote") {
	    	$post_type = " post-quote";
	    }else if ($vbegy_what_post == "link") {
	    	$post_type = " post-link";
	    }else if ($vbegy_what_post == "soundcloud") {
			$post_type = " post-soundcloud";
	    }else if ($vbegy_what_post == "twitter") {
	    	$post_type = " post-twitter";
	    }else if ($vbegy_what_post == "facebook") {
	    	$post_type = " post-facebook";
	    }else {
	    	if (has_post_thumbnail()) {
	        	$post_type = " image_post";
	    	}else {
	        	$post_type = " no_image_post";
	    	}
	    }
	    if ($post->post_content == "") {
	    	$post_type = " post-no-content".$post_type;
	    }else {
	    	$post_type = " post--content".$post_type;
	    }
	    if ($post_type_option == 'on' && $post_author == 'on' && $post_style == 'style_1') {
	    	$post_type = " post-2".$post_type;
	    }
	    if ($post_style == "style_2" || ($post_style == "style_3" && $vbegy_sidebar != "full")) {
	    	$post_type = " post-3 col-md-6".$post_type;
	    }
	    if ($post_style == "style_3" && $vbegy_sidebar == "full") {
	    	$post_type = " post-3 col-md-4".$post_type;
	    }
	    if ($post_style != "style_2" && $post_style != "style_3") {
	    	$post_type = " animation".$post_type;
	    }
	    
	    $post_username = get_post_meta($post->ID, 'post_username',true);
	    $post_email = get_post_meta($post->ID, 'post_email',true);?>
	    <article <?php post_class('post clearfix '.$post_type);?> data-animate="fadeInUp" role="article" itemscope="" itemtype="http://schema.org/Article">
	    	<?php if ($post_type_option == 'on') {?>
		    	<div class="post-type"><i class="fa fa-<?php if (is_sticky()) {?>thumb-tack<?php }else if ($vbegy_what_post == "google") {?>map-marker<?php }else if ($vbegy_what_post == "audio") {?>volume-up<?php }else if ($vbegy_what_post == "video") {if ($video_type == 'youtube') {?>youtube-play<?php }else if ($video_type == 'vimeo') {?>vimeo-square<?php }else if ($video_type == 'daily') {?>video-camera<?php }?><?php }else if ($vbegy_what_post == "slideshow") {?>film<?php }else if ($vbegy_what_post == "quote") {?>quote-left<?php }else if ($vbegy_what_post == "link") {?>link<?php }else if ($vbegy_what_post == "soundcloud") {?>soundcloud<?php }else if ($vbegy_what_post == "twitter") {?>twitter<?php }else if ($vbegy_what_post == "facebook") {?>facebook<?php }else {if (has_post_thumbnail()) {?>image<?php }else {?>file-text<?php }}?>"></i></div>
	    	<?php }
	    	if ($post_author == 'on') {
	    		$user_info = get_userdata($post->post_author);?>
	    		<div class="post-author">
	    			<?php if (get_the_author_meta('you_avatar', $post->post_author)) {
	    				$you_avatar_img = get_aq_resize_url(esc_attr(get_the_author_meta('you_avatar', $post->post_author)),"full",70,70);
	    				echo "<img alt='".$user_info->display_name."' src='".$you_avatar_img."'>";
	    			}else {
	    				if ($post->post_author != 0) {
	    					echo get_avatar($user_info->user_email,'70','');
	    				}else {
	    					echo get_avatar($post_email,'70','');
	    				}
	    			}?>
	    		</div>
	    	<?php }?>
	    	<div class="post-head"><h3><a itemprop="url" href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php the_title();?></a></h3>
	    		<?php if ($posts_meta == 'on') {
	    			$category_post = vpanel_options("category_post");?>
		    		<div class="post-meta">
		    			<?php if ($author_by == 'on') {?>
		    				<div><i class="fa fa-user"></i><?php _e("by","vbegy")?> : <?php echo ($post->post_author > 0?the_author_posts_link():$post_username);?></div>
		    			<?php }?>
		    			<div><i class="fa fa-clock-o"></i><?php the_time('F j , Y');?></div>
		    			<?php if ((($post_style == "style_2" && $vbegy_sidebar == "full") && $post_style != "style_3") || $post_style == "style_1") {?>
			    			<div><i class="fa fa-comments"></i><?php comments_popup_link(__('0 Comments', 'vbegy'), __('1 Comment', 'vbegy'), '% '.__('Comments', 'vbegy'));?></div>
			    			<meta itemprop="interactionCount" content="<?php comments_number( 'UserComments: 0', 'UserComments: 1', 'UserComments: %' ); ?>">
		    			<?php }
		    			if ($post_style != "style_2" && $post_style != "style_3") {
		    				if ($category_post == 'on') {?>
			    				<div><i class="fa fa-folder-open"></i><?php _e("in","vbegy")?> : <?php the_category(' , ');?></div>
		    				<?php }
		    			}
		    			if ((($post_style == "style_2" && $vbegy_sidebar == "full") && $post_style != "style_3") || $post_style == "style_1") {
			    			$post_like = get_post_meta($post->ID,"post_like",true);
			    			$like_yes = "";
			    			if (isset($_COOKIE['logger_post_like'.$post->ID]) && $_COOKIE['logger_post_like'.$post->ID] == "logger_like_yes") {
			    				$like_yes = "logger_like_yes";
			    			}
			    			?>
			    			<div><a class="post-like <?php echo ($like_yes == "logger_like_yes"?"post-like-done":"")?>" title="<?php echo ($like_yes == "logger_like_yes"?__("You already like this","vbegy"):__("Love","vbegy"))?>" id="post-like-<?php the_ID();?>"><i class="fa <?php echo ($like_yes == "logger_like_yes"?"fa-heart":"fa-heart-o")?>"></i><span><?php echo (isset($post_like) && $post_like != ""?$post_like:0)?></span></a></div>
			    		<?php }?>
		    		</div><!-- End post-meta -->
	    		<?php }?>
	    		<div class="clearfix"></div>
	    	</div><!-- End post-head -->
	    	<div class="post-wrap">
	    		<?php if ($vbegy_what_post != "link" && $vbegy_what_post != "quote") {?>
		    		<div <?php echo ($vbegy_what_post == "soundcloud"?$soundcloud_background:"").($vbegy_what_post == "twitter"?$twitter_background:"").($vbegy_what_post == "facebook"?$facebook_background:"")?> class="post-img<?php if ((!isset($vbegy_what_post) || $vbegy_what_post == "none" || $vbegy_what_post == "image" || $vbegy_what_post == "soundcloud" || $vbegy_what_post == "twitter" || $vbegy_what_post == "facebook" || $vbegy_what_post == "audio") && !has_post_thumbnail()) {echo " post-img-0";}else if ($vbegy_what_post == "video" || $vbegy_what_post == "soundcloud" || $vbegy_what_post == "twitter" || $vbegy_what_post == "facebook") {echo " post-iframe";}if ($vbegy_sidebar == "full") {echo " post-img-12";}else {echo " post-img-8";}?>">
		    			<?php if ((has_post_thumbnail() && $vbegy_what_post != "image_lightbox") && $vbegy_what_post != "audio") {?><a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php }
		    				get_template_part('includes/head');
		    			if ((has_post_thumbnail() && $vbegy_what_post != "image_lightbox") && $vbegy_what_post != "audio") {?></a><?php }?>
		    		</div>
		    		<div class="post-inner">
	    		<?php }
	    			if ($vbegy_what_post == "quote" || $vbegy_what_post == "link") {
	    				if ($vbegy_what_post == "quote") {?>
	    					<div class="post-inner" <?php echo esc_attr($quote_background)?>>
		    					<div class="post-quote-top"><i <?php echo esc_attr($quote_icon_color)?> class="fa fa-quote-left"></i></div>
		    					<?php the_content();?>
		    					<?php if ($vbegy_quote_author != "") {?>
		    						<span class="author">― <?php echo esc_attr($vbegy_quote_author)?></span>
		    					<?php }?>
		    					<div class="post-quote-bottom"><i <?php echo esc_attr($quote_icon_color)?> class="fa fa-quote-right"></i></div>
		    					<div class="clearfix"></div>
	    					</div><!-- End post-inner -->
	    				<?php }else if ($vbegy_what_post == "link") {?>
	    					<a <?php echo esc_attr($link_background)?> href="<?php echo esc_url($vbegy_link)?>" <?php echo ($vbegy_link_target == "style_2"?"target='_blank'":"")?> class="post-inner link">
	    						<i <?php echo esc_attr($link_icon_color)?> class="fa fa-link"></i>
	    						<div>
	    							<?php echo esc_attr($vbegy_link_title)?>
	    							<span><?php echo esc_attr($vbegy_link)?></span>
	    						</div>
	    					</a><!-- End post-inner -->
	    				<?php }
	    			}else {
	    				if ($vbegy_what_post != "link" && $vbegy_what_post != "quote") {?>
	    			    	<p><?php excerpt($post_excerpt);?></p>
	    				<?php }
	    			}
	    		if ($vbegy_what_post != "link" && $vbegy_what_post != "quote") {?>
		    			<div class="clearfix"></div>
		    			<div class="post-share-view">
		    				<div class="post-meta">
		    					<?php $post_stats = get_post_meta($post->ID, 'post_stats', true)?>
		    					<div><i class="fa fa-eye"></i><span><?php echo (isset($post_stats) && $post_stats != ""?(int)$post_stats:0)?> </span><?php _e("Views","vbegy")?></div>
		    					<?php if ((($post_style == "style_2" && $vbegy_sidebar == "full") && $post_style != "style_3") || $post_style == "style_1") {
		    						if ($post_share == "on") {?>
				    					<div class="post-meta-share">
				    						<i class="fa fa-share-alt"></i>
				    						<a href="#"><?php _e("Share This","vbegy")?></a>
				    						<div class="share-social">
				    							<ul>
				    								<li class="social-facebook"><a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink());?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
				    								<li class="social-twitter"><a href="http://twitter.com/home?status=<?php echo urlencode(get_permalink());?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
				    								<li class="social-google"><a href="http://plus.google.com/share?url=<?php echo urlencode(get_permalink());?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
				    							</ul>
				    							<i class="fa fa-caret-down"></i>
				    						</div><!-- End follow-social -->
				    					</div><!-- End post-meta-share -->
			    					<?php }
		    					}?>
		    					<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark" class="button post-more"><?php _e("Continue Reading","vbegy");?></a>
		    				</div><!-- End post-meta -->
		    			</div><!-- End post-share-view -->
		    			<div class="clearfix"></div>
		    		</div><!-- End post-inner -->
	    		<?php }?>
	    	</div><!-- End post-wrap -->
	    </article><!-- End post -->
	<?php endwhile;else :?>
		<div class="post">
			<div class="post-wrap">
				<div class="post-inner">
					<div class="page-404">
						<h3><?php _e("Sorry, no posts yet .","vbegy")?></h3><a class="button" href="<?php echo esc_url(home_url('/'));?>"><?php _e("Back To Homepage","vbegy")?></a>
					</div>
					<div class="clearfix"></div>
				</div><!-- End post-inner -->
			</div><!-- End post-wrap -->
		</div><!-- End post -->
	<?php endif;
}?>