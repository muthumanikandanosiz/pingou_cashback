<?php
$implode_c = "";
if (isset($slideshow_categories) && is_array($slideshow_categories)) {
	foreach ($slideshow_categories as $key => $value) {
		if ($value == "on") {
			$implode_key_c[] = $key;
		}
		if ($value == 1) {
			$implode_key_c[] = $key;
		}
	}
	if (isset($implode_key_c) && is_array($implode_key_c)) {
		$implode_c = implode(",",$implode_key_c);
	}
}

$news_implode_c = "";
if (isset($news_categories) && is_array($news_categories)) {
	foreach ($news_categories as $news_key => $news_value) {
		if ($news_value == "on") {
			$news_implode_key_c[] = $news_key;
		}
		if ($news_value == 1) {
			$news_implode_key_c[] = $news_key;
		}
	}
	if (isset($news_implode_key_c) && is_array($news_implode_key_c)) {
		$news_implode_c = implode(",",$news_implode_key_c);
	}
}
?>
<div class="head-slide<?php echo ($head_slide == "header"?" head-slide-header":"").($head_slide == "footer"?" head-slide-footer":"").($head_slide_background == "transparent"?" head-slide-transparent":"")?>" <?php if ($head_slide_background == "custom") {?>style='background:<?php if (!empty($head_slide_custom_background)) {?><?php echo esc_attr($head_slide_background_color)?> url(<?php echo esc_attr($head_slide_background_img)?>) <?php echo esc_attr($head_slide_background_repeat)?> <?php echo esc_attr($head_slide_background_position)?> <?php echo esc_attr($head_slide_background_fixed)?>;<?php if ($head_slide_full_screen_background == 'on') {echo "-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;";} }?>'<?php }?>>
	<div class="container">
		<div class="row">
			<?php if ($news_ticker == "on") {?>
				<div class="col-md-12">
					<div class="news-ticker">
						<div class="news-ticker-title"><i class="fa fa-bell"></i><?php _e("Breaking News","vbegy")?></div>
						<div class="news-ticker-content">
							<ul>
								<?php query_posts(array('cat' => $news_implode_c,'ignore_sticky_posts' => 1,'posts_per_page' => $news_number));
								if ( have_posts() ) :
								while ( have_posts() ) : the_post();?>
									<li>
										<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php excerpt_title($news_excerpt_title);?></a>
									</li>
								<?php endwhile;endif;wp_reset_query();?>
							</ul>
						</div><!-- End news-ticker-content -->
					</div><!-- End news-ticker -->
				</div><!-- End col-md-12 -->
			<?php }?>
			<div class="<?php echo ($head_slide_style == "slideshow_thumbnail"?"col-md-8":"col-md-12")?>">
				<?php if ($head_slide != "none") {
					if ($head_slide_style == "slideshow_thumbnail" || $head_slide_style == "slideshow") {?>
						<div class="box-slideshow<?php echo ($head_slide_style == "slideshow"?" block-box-full":"")?>">
							<ul>
								<?php query_posts(array('cat' => $implode_c,'ignore_sticky_posts' => 1,'posts_per_page' => $slides_number));
								if ( have_posts() ) :
								while ( have_posts() ) : the_post();
									$post_username = get_post_meta($post->ID, 'post_username',true);
									$post_email = get_post_meta($post->ID, 'post_email',true);
									if (has_post_thumbnail()) {?>
										<li>
											<div class="box-slideshow-main">
												<div class="box-slideshow-img">
													<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark">
														<?php if ($head_slide_style == "slideshow") {
															echo get_aq_resize_img('full',1140,641);
														}else {
															echo get_aq_resize_img('full',750,422);
														}?>
													</a>
												</div>
												<?php if ($slide_overlay == "enable") {
													$author_by = vpanel_options("author_by");?>
													<div class="box-slideshow-content">
														<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php excerpt_title($excerpt_title);?></a>
														<div class="clearfix"></div>
														<p><?php excerpt($excerpt);?></p>
														<?php if ($author_by == 'on') {?>
															<span><i class="fa fa-user"></i><?php _e("by","vbegy")?> : <?php echo ($post->post_author > 0?the_author_posts_link():$post_username);?></span>
														<?php }?>
														<span><i class="fa fa-clock-o"></i><?php the_time('F j, Y');?></span>
														<div class="clearfix"></div>
													</div>
												<?php }?>
											</div>
										</li>
									<?php }
								endwhile;endif;wp_reset_query();?>
							</ul>
						</div><!-- End box-slideshow -->
					<?php }else if ($head_slide_style == "thumbnail") {?>
						<div class="row">
							<ul>
								<?php query_posts(array('cat' => $implode_c,'ignore_sticky_posts' => 1,'posts_per_page' => 6));
								if ( have_posts() ) :
								$post_width = 360;
								$post_height = 196;
								while ( have_posts() ) : the_post();
									$vbegy_what_post = rwmb_meta('vbegy_what_post','select',$post->ID);
									$video_id = rwmb_meta('vbegy_video_post_id',"select",$post->ID);
									$video_type = rwmb_meta('vbegy_video_post_type',"text",$post->ID);
									$post_username = get_post_meta($post->ID, 'post_username',true);
									$post_email = get_post_meta($post->ID, 'post_email',true);
									if ($video_type == 'youtube') {
										$type = "http://www.youtube.com/embed/".$video_id;
									}else if ($video_type == 'vimeo') {
										$type = "http://player.vimeo.com/video/".$video_id;
									}else if ($video_type == 'daily') {
										$type = "http://www.dailymotion.com/swf/video/".$video_id;
									}
									$vbegy_slideshow_type = rwmb_meta('vbegy_slideshow_type','select',$post->ID);
									if (has_post_thumbnail() || $vbegy_what_post == "video") {?>
										<li class="col-md-4">
											<div class="related-post-item">
												<div class="related-post-one">
													<div class="related-post-img">
														<a itemprop="url" href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark">
															<?php
															if ($vbegy_what_post == "image" || $vbegy_what_post == "slideshow") {
																if (has_post_thumbnail() && $vbegy_what_post == "image") {
																	echo get_aq_resize_img('full',$post_width,$post_height,$img_lightbox = "lightbox");
																}else if (has_post_thumbnail() && $vbegy_what_post == "slideshow") {
																	echo get_aq_resize_img('full',$post_width,$post_height);
																}
															}else {
																if (has_post_thumbnail()) {
																	echo get_aq_resize_img('full',$post_width,$post_height,$img_lightbox = "lightbox");
																}
															}
															?>
														</a>
														<div class="related-post-type">
															<i class="fa fa-<?php if (is_sticky()) {?>thumb-tack<?php }else if ($vbegy_what_post == "google") {?>map-marker<?php }else if ($vbegy_what_post == "video") {if ($video_type == 'youtube') {?>youtube-play<?php }else if ($video_type == 'vimeo') {?>vimeo-square<?php }else if ($video_type == 'daily') {?>video-camera<?php }?><?php }else if ($vbegy_what_post == "slideshow") {?>film<?php }else if ($vbegy_what_post == "quote") {?>quote-left<?php }else if ($vbegy_what_post == "link") {?>link<?php }else if ($vbegy_what_post == "soundcloud") {?>soundcloud<?php }else if ($vbegy_what_post == "twitter") {?>twitter<?php }else if ($vbegy_what_post == "facebook") {?>facebook<?php }else {if (has_post_thumbnail()) {?>image<?php }else {?>file-text<?php }}?>"></i>
														</div>
													</div>
													<div class="related-post-head">
														<a itemprop="url" href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php excerpt_title($excerpt_title)?></a>
														<span><i class="fa fa-clock-o"></i><?php the_time('F j , Y');?></span>
														<span><i class="fa fa-comments"></i><?php comments_popup_link(__('0 Comments', 'vbegy'), __('1 Comment', 'vbegy'), '% '.__('Comments', 'vbegy'));?></span>
													</div>
												</div>
											</div>
										</li>
									<?php }
								endwhile;endif;wp_reset_query();?>
							</ul>
						</div>
					<?php }
				}?>
			</div>
			<?php if ($head_slide_style == "slideshow_thumbnail") {?>
				<div class="col-md-4">
					<ul class="head-posts">
						<?php query_posts(array('cat' => $implode_c,'ignore_sticky_posts' => 1,'posts_per_page' => 2));
						if ( have_posts() ) :
						$post_width = 360;
						$post_height = 196;
						while ( have_posts() ) : the_post();
							$vbegy_what_post = rwmb_meta('vbegy_what_post','select',$post->ID);
							$video_id = rwmb_meta('vbegy_video_post_id',"select",$post->ID);
							$video_type = rwmb_meta('vbegy_video_post_type',"text",$post->ID);
							$post_username = get_post_meta($post->ID, 'post_username',true);
							$post_email = get_post_meta($post->ID, 'post_email',true);
							if ($video_type == 'youtube') {
								$type = "http://www.youtube.com/embed/".$video_id;
							}else if ($video_type == 'vimeo') {
								$type = "http://player.vimeo.com/video/".$video_id;
							}else if ($video_type == 'daily') {
								$type = "http://www.dailymotion.com/swf/video/".$video_id;
							}
							$vbegy_slideshow_type = rwmb_meta('vbegy_slideshow_type','select',$post->ID);
							if (has_post_thumbnail() || $vbegy_what_post == "video") {?>
								<li>
									<div class="related-post-item">
										<div class="related-post-one">
											<div class="related-post-img">
												<a itemprop="url" href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark">
													<?php
													if ($vbegy_what_post == "image" || $vbegy_what_post == "slideshow") {
														if (has_post_thumbnail() && $vbegy_what_post == "image") {
															echo get_aq_resize_img('full',$post_width,$post_height,$img_lightbox = "lightbox");
														}else if (has_post_thumbnail() && $vbegy_what_post == "slideshow") {
															echo get_aq_resize_img('full',$post_width,$post_height);
														}
													}else if ($vbegy_what_post == "video") {
														echo '<iframe height="'.$post_height.'" src="'.$type.'"></iframe>';
													}else {
														if (has_post_thumbnail()) {
															echo get_aq_resize_img('full',$post_width,$post_height,$img_lightbox = "lightbox");
														}
													}
													?>
												</a>
												<div class="related-post-type">
													<i class="fa fa-<?php if (is_sticky()) {?>thumb-tack<?php }else if ($vbegy_what_post == "google") {?>map-marker<?php }else if ($vbegy_what_post == "video") {if ($video_type == 'youtube') {?>youtube-play<?php }else if ($video_type == 'vimeo') {?>vimeo-square<?php }else if ($video_type == 'daily') {?>video-camera<?php }?><?php }else if ($vbegy_what_post == "slideshow") {?>film<?php }else if ($vbegy_what_post == "quote") {?>quote-left<?php }else if ($vbegy_what_post == "link") {?>link<?php }else if ($vbegy_what_post == "soundcloud") {?>soundcloud<?php }else if ($vbegy_what_post == "twitter") {?>twitter<?php }else if ($vbegy_what_post == "facebook") {?>facebook<?php }else {if (has_post_thumbnail()) {?>image<?php }else {?>file-text<?php }}?>"></i>
												</div>
											</div>
											<div class="related-post-head">
												<a itemprop="url" href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php excerpt_title($excerpt_title)?></a>
												<span><i class="fa fa-clock-o"></i><?php the_time('F j , Y');?></span>
												<span><i class="fa fa-comments"></i><?php comments_popup_link(__('0 Comments', 'vbegy'), __('1 Comment', 'vbegy'), '% '.__('Comments', 'vbegy'));?></span>
											</div>
										</div>
									</div>
								</li>
							<?php }
						endwhile;endif;wp_reset_query();?>
					</ul>
				</div>
			<?php }?>
		</div><!-- End row -->
	</div><!-- End container -->
</div>