					<?php if (is_single() || is_page()) {
						$vbegy_content_adv_type = rwmb_meta('vbegy_content_adv_type','radio',$post->ID);
						$vbegy_content_adv_code = rwmb_meta('vbegy_content_adv_code','textarea',$post->ID);
						$vbegy_content_adv_href = rwmb_meta('vbegy_content_adv_href','text',$post->ID);
						$vbegy_content_adv_img = rwmb_meta('vbegy_content_adv_img','upload',$post->ID);
					}
					
					if ((is_single() || is_page()) && (($vbegy_content_adv_type == "display_code" && $vbegy_content_adv_code != "") || ($vbegy_content_adv_type == "custom_image" && $vbegy_content_adv_img != ""))) {
						$content_adv_type = $vbegy_content_adv_type;
						$content_adv_code = $vbegy_content_adv_code;
						$content_adv_href = $vbegy_content_adv_href;
						$content_adv_img = $vbegy_content_adv_img;
					}else {
						$content_adv_type = vpanel_options("content_adv_type");
						$content_adv_code = vpanel_options("content_adv_code");
						$content_adv_href = vpanel_options("content_adv_href");
						$content_adv_img = vpanel_options("content_adv_img");
					}
					if (($content_adv_type == "display_code" && $content_adv_code != "") || ($content_adv_type == "custom_image" && $content_adv_img != "")) {
						echo '<div class="clearfix"></div>
						<div class="advertising">';
						if ($content_adv_type == "display_code") {
							echo stripcslashes($content_adv_code);
						}else {
							if ($content_adv_href != "") {
								echo '<a href="'.$content_adv_href.'">';
							}
							echo '<img alt="" src="'.$content_adv_img.'">';
							if ($content_adv_href != "") {
								echo '</a>';
							}
						}
						echo '</div><!-- End advertising -->
						<div class="clearfix"></div>';
					}?>
				</div><!-- End main-content -->
				<div class="col-md-4 sidebar">
					<?php get_sidebar();?>
				</div><!-- End sidebar -->
			</div><!-- End row -->
		</div><!-- End container -->
	</div><!-- End sections -->
	
	<div class="clearfix"></div>
	
	<?php
	wp_reset_query();
	$vbegy_custom_header = rwmb_meta('vbegy_custom_header','checkbox',$post->ID);
	if ((is_single() || is_page()) && isset($vbegy_custom_header) && $vbegy_custom_header == 1) {
		$head_slide = rwmb_meta('vbegy_head_slide','select',$post->ID);
		$head_slide_style = rwmb_meta('vbegy_head_slide_style','select',$post->ID);
		$slide_overlay = rwmb_meta('vbegy_slide_overlay','select',$post->ID);
		$excerpt_title = rwmb_meta('vbegy_excerpt_title','text',$post->ID);
		$excerpt = rwmb_meta('vbegy_excerpt','text',$post->ID);
		$slides_number = rwmb_meta('vbegy_slides_number','text',$post->ID);
		$slideshow_categories = rwmb_meta('vbegy_slideshow_categories','multicheck',$post->ID);
		$head_slide_background = rwmb_meta('vbegy_head_slide_background','select',$post->ID);
		$head_slide_background_color = rwmb_meta('vbegy_head_slide_background_color','color',$post->ID);
		$news_ticker = rwmb_meta('vbegy_news_ticker','checkbox',$post->ID);
		if ($news_ticker == 1) {
			$news_ticker = "on";
		}
		$news_excerpt_title = rwmb_meta('vbegy_news_excerpt_title','text',$post->ID);
		$news_number = rwmb_meta('vbegy_news_number','text',$post->ID);
		$news_categories = rwmb_meta('vbegy_news_categories','multicheck',$post->ID);
	}else {
		$head_slide = vpanel_options('head_slide');
		$head_slide_style = vpanel_options('head_slide_style');
		$slide_overlay = vpanel_options('slide_overlay');
		$excerpt_title = vpanel_options('excerpt_title');
		$excerpt = vpanel_options('excerpt');
		$slides_number = vpanel_options('slides_number');
		$slideshow_categories = vpanel_options('slideshow_categories');
		$head_slide_background = vpanel_options('head_slide_background');
		$head_slide_background_color = vpanel_options('head_slide_background_color');
		$news_ticker = vpanel_options('news_ticker');
		$news_excerpt_title = vpanel_options('news_excerpt_title');
		$news_number = vpanel_options('news_number');
		$news_categories = vpanel_options('news_categories');
	}
	if ($head_slide == "footer") {
		include(locate_template("includes/head_slide.php"));
	}
	
	$footer_top = vpanel_options("footer_top");
	if ($footer_top == "on") {
		$subscribe_f = vpanel_options("subscribe_f");
		$footer_layout_1 = vpanel_options("footer_layout_1");
		$footer_layout_2 = vpanel_options("footer_layout_2");?>
		<footer id="footer-top">
			<div class="container">
				<div class="row">
					<?php if ($subscribe_f == "on") {
						$feedburner_h3 = vpanel_options("feedburner_h3");
						$feedburner_p = vpanel_options("feedburner_p");
						$feedburner_id = vpanel_options("feedburner_id");
						?>
						<div class="col-md-12">
							<div class="footer-subscribe">
								<div class="row">
									<form action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=<?php echo esc_attr($feedburner_id); ?>', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
										<div class="col-md-8">
											<?php if ($feedburner_h3 != "") {?>
												<h3><?php echo esc_attr($feedburner_h3)?></h3>
											<?php }
											if ($feedburner_p != "") {?>
												<p><?php echo esc_attr($feedburner_p)?></p>
											<?php }?>
										</div>
										<div class="col-md-4">
										    <input name="email" type="text" value="<?php _e("Email","vbegy");?>" onfocus="if(this.value=='<?php _e("Email","vbegy");?>')this.value='';" onblur="if(this.value=='')this.value='<?php _e("Email","vbegy");?>';">
											<input type="hidden" value="<?php echo esc_attr($feedburner_id); ?>" name="uri">
											<input type="hidden" name="loc" value="en_US">	
											<input name="submit" type="submit" id="submit" class="button color small sidebar_submit" value="<?php _e('Subscribe','vbegy')?>">
										</div>
									</form>
								</div>
							</div>
						</div>
					<?php }
					if ($footer_layout_1 != "footer_no") {
						if ($footer_layout_1 == "footer_1c") {?>
							<div class="col-md-12">
								<?php dynamic_sidebar('footer_1c_sidebar_1');?>
							</div>
						<?php }else if ($footer_layout_1 == "footer_2c") {?>
							<div class="col-md-6">
								<?php dynamic_sidebar('footer_1c_sidebar_1');?>
							</div>
							<div class="col-md-6">
								<?php dynamic_sidebar('footer_2c_sidebar_1');?>
							</div>
						<?php }else if ($footer_layout_1 == "footer_3c") {?>
							<div class="col-md-4">
								<?php dynamic_sidebar('footer_1c_sidebar_1');?>
							</div>
							<div class="col-md-4">
								<?php dynamic_sidebar('footer_2c_sidebar_1');?>
							</div>
							<div class="col-md-4">
								<?php dynamic_sidebar('footer_3c_sidebar_1');?>
							</div>
						<?php }else if ($footer_layout_1 == "footer_4c") {?>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_1c_sidebar_1');?>
							</div>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_2c_sidebar_1');?>
							</div>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_3c_sidebar_1');?>
							</div>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_4c_sidebar_1');?>
							</div>
						<?php }
					}?>
					<div class="clearfix"></div>
					<?php if ($footer_layout_2 != "footer_no") {
						if ($footer_layout_2 == "footer_1c") {?>
							<div class="col-md-12">
								<?php dynamic_sidebar('footer_1c_sidebar_2');?>
							</div>
						<?php }else if ($footer_layout_2 == "footer_2c") {?>
							<div class="col-md-6">
								<?php dynamic_sidebar('footer_1c_sidebar_2');?>
							</div>
							<div class="col-md-6">
								<?php dynamic_sidebar('footer_2c_sidebar_2');?>
							</div>
						<?php }else if ($footer_layout_2 == "footer_3c") {?>
							<div class="col-md-4">
								<?php dynamic_sidebar('footer_1c_sidebar_2');?>
							</div>
							<div class="col-md-4">
								<?php dynamic_sidebar('footer_2c_sidebar_2');?>
							</div>
							<div class="col-md-4">
								<?php dynamic_sidebar('footer_3c_sidebar_2');?>
							</div>
						<?php }else if ($footer_layout_2 == "footer_4c") {?>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_1c_sidebar_2');?>
							</div>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_2c_sidebar_2');?>
							</div>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_3c_sidebar_2');?>
							</div>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_4c_sidebar_2');?>
							</div>
						<?php }
					}?>
				</div><!-- End row -->
			</div><!-- End container -->
		</footer>
	<?php }?>
	
	<footer id="footer"<?php echo ($footer_top == "on"?"class='footer-no-top'":"")?>>
		<div class="container">
			<div class="copyrights"><?php echo vpanel_options("footer_copyrights")?></div>
			<div class="social-ul">
				<ul>
					<?php
					$social_icon_f = vpanel_options("social_icon_f");
					if ($social_icon_f == "on") {
						$facebook_icon_f = vpanel_options("facebook_icon_f");
						$twitter_icon_f = vpanel_options("twitter_icon_f");
						$gplus_icon_f = vpanel_options("gplus_icon_f");
						$linkedin_icon_f = vpanel_options("linkedin_icon_f");
						$dribbble_icon_f = vpanel_options("dribbble_icon_f");
						$youtube_icon_f = vpanel_options("youtube_icon_f");
						$skype_icon_f = vpanel_options("skype_icon_f");
						$vimeo_icon_f = vpanel_options("vimeo_icon_f");
						$flickr_icon_f = vpanel_options("flickr_icon_f");
						$rss_icon_f = vpanel_options("rss_icon_f");
						$rss_icon_f_other = vpanel_options("rss_icon_f_other");
						if ($facebook_icon_f) {?>
							<li class="social-facebook"><a href="<?php echo esc_url($facebook_icon_f)?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<?php }
						if ($twitter_icon_f) {?>
							<li class="social-twitter"><a href="<?php echo esc_url($twitter_icon_f)?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
						<?php }
						if ($gplus_icon_f) {?>
							<li class="social-google"><a href="<?php echo esc_url($gplus_icon_f)?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
						<?php }
						if ($linkedin_icon_f) {?>
							<li class="social-linkedin"><a href="<?php echo esc_url($linkedin_icon_f)?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
						<?php }
						if ($dribbble_icon_f) {?>
							<li class="social-dribbble"><a href="<?php echo esc_url($dribbble_icon_f)?>" target="_blank"><i class="fa fa-dribbble"></i></a></li>
						<?php }
						if ($youtube_icon_f) {?>
							<li class="social-youtube"><a href="<?php echo esc_url($youtube_icon_f)?>" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
						<?php }
						if ($vimeo_icon_f) {?>
							<li class="social-vimeo"><a href="<?php echo esc_url($vimeo_icon_f)?>" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>
						<?php }
						if ($skype_icon_f) {?>
							<li class="social-skype"><a href="<?php echo esc_url($skype_icon_f)?>" target="_blank"><i class="fa fa-skype"></i></a></li>
						<?php }
						if ($flickr_icon_f) {?>
							<li class="social-flickr"><a href="<?php echo esc_url($flickr_icon_f)?>" target="_blank"><i class="fa fa-flickr"></i></a></li>
						<?php }
						if ($rss_icon_f == "on") {?>
							<li class="social-rss"><a href="<?php echo ($rss_icon_f_other != ""?esc_url($rss_icon_f_other):esc_url(bloginfo('rss2_url')));?>" target="_blank"><i class="fa fa-rss"></i></a></li>
						<?php }
					}?>
				</ul>
			</div><!-- End social-ul -->
		</div><!-- End container -->
	</footer><!-- End footer -->
	
</div><!-- End wrap -->

<div class="go-up"><i class="fa fa-chevron-up"></i></div>

<?php wp_footer(); ?>
</body>
</html>