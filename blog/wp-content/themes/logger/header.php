<?php ob_start();?>
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> xmlns:fb="http://ogp.me/ns/fb#">
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=9">
<title><?php wp_title( '|', true, 'right' );?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<?php wp_head();?>
</head>
<body <?php body_class();?>>
	<div class="background-cover"></div>
	<?php
	$vbegy_layout = "";
	if (is_single() || is_page()) {
		$vbegy_layout = rwmb_meta('vbegy_layout','radio',$post->ID);
	}
	
	$cat_layout = "";
	if (is_category()) {
		$category_id = get_query_var('cat');
		$categories = get_option("categories_$category_id");
		$cat_layout = (isset($categories["cat_layout"])?$categories["cat_layout"]:"default");
	}
	$home_layout = vpanel_options("home_layout");
	$author_layout = vpanel_options("author_layout");
	if (is_author() && $author_layout != "default") {
		$home_layout = $author_layout;
	}
	$loader_option = vpanel_options("loader");
	if ($loader_option == "on") {?>
		<div class="loader"><div class="loader_html"></div></div>
	<?php }
	
	if (is_author()) {
		$grid_template = vpanel_options("author_template");
	}else if (is_single() || is_page()) {
		$grid_template = rwmb_meta('vbegy_home_template','radio',$post->ID);
	}else if (is_category()) {
		$grid_template = (isset($categories["grid_template"])?$categories["grid_template"]:"default");
	}else {
		$grid_template = vpanel_options("home_template");
	}
	
	if ((is_author() && $grid_template == "default") || ((is_single() || is_page()) && $grid_template == "default") || (is_category() && $grid_template == "default")) {
		$grid_template = vpanel_options("home_template");
	}
	
	if (is_single() || is_page()) {
		$vbegy_custom_header = rwmb_meta('vbegy_custom_header','checkbox',$post->ID);
	}
	if ((is_single() || is_page()) && isset($vbegy_custom_header) && $vbegy_custom_header == 1) {
		$vbegy_header_style = rwmb_meta('vbegy_header_style','select',$post->ID);
		$vbegy_header_menu = rwmb_meta('vbegy_header_menu','checkbox',$post->ID);
		$vbegy_header_menu_style = rwmb_meta('vbegy_header_menu_style','radio',$post->ID);
		$vbegy_header_fixed = rwmb_meta('vbegy_header_fixed','checkbox',$post->ID);
		$vbegy_logo_display = rwmb_meta('vbegy_logo_display','radio',$post->ID);
		$vbegy_logo_img = rwmb_meta('vbegy_logo_img','upload',$post->ID);
		$vbegy_header_search = rwmb_meta('vbegy_header_search','checkbox',$post->ID);
		$vbegy_header_follow = rwmb_meta('vbegy_header_follow','checkbox',$post->ID);
		$vbegy_header_follow_style = rwmb_meta('vbegy_header_follow_style','radio',$post->ID);
		$vbegy_facebook_icon_h = rwmb_meta('vbegy_facebook_icon_h','text',$post->ID);
		$vbegy_twitter_icon_h = rwmb_meta('vbegy_twitter_icon_h','text',$post->ID);
		$vbegy_gplus_icon_h = rwmb_meta('vbegy_gplus_icon_h','text',$post->ID);
		$vbegy_linkedin_icon_h = rwmb_meta('vbegy_linkedin_icon_h','text',$post->ID);
		$vbegy_dribbble_icon_h = rwmb_meta('vbegy_dribbble_icon_h','text',$post->ID);
		$vbegy_youtube_icon_h = rwmb_meta('vbegy_youtube_icon_h','text',$post->ID);
		$vbegy_vimeo_icon_h = rwmb_meta('vbegy_vimeo_icon_h','text',$post->ID);
		$vbegy_skype_icon_h = rwmb_meta('vbegy_skype_icon_h','text',$post->ID);
		$vbegy_flickr_icon_h = rwmb_meta('vbegy_flickr_icon_h','text',$post->ID);
		$header_style = $vbegy_header_style;
		$header_menu = $vbegy_header_menu;
		if ($vbegy_header_menu == 1) {
			$header_menu = "on";
		}
		$header_menu_style = $vbegy_header_menu_style;
		$header_fixed = $vbegy_header_fixed;
		if ($vbegy_header_fixed == 1) {
			$header_fixed = "on";
		}
		$logo_display = $vbegy_logo_display;
		$logo_img = $vbegy_logo_img;
		$header_search = $vbegy_header_search;
		if ($vbegy_header_search == 1) {
			$header_search = "on";
		}
		$header_follow = $vbegy_header_follow;
		if ($vbegy_header_follow == 1) {
			$header_follow = "on";
		}
		$header_follow_style = $vbegy_header_follow_style;
		$facebook_icon_h = $vbegy_facebook_icon_h;
		$twitter_icon_h = $vbegy_twitter_icon_h;
		$gplus_icon_h = $vbegy_gplus_icon_h;
		$linkedin_icon_h = $vbegy_linkedin_icon_h;
		$dribbble_icon_h = $vbegy_dribbble_icon_h;
		$youtube_icon_h = $vbegy_youtube_icon_h;
		$vimeo_icon_h = $vbegy_vimeo_icon_h;
		$skype_icon_h = $vbegy_skype_icon_h;
		$flickr_icon_h = $vbegy_flickr_icon_h;
		$head_slide = rwmb_meta('vbegy_head_slide','select',$post->ID);
		$head_slide_style = rwmb_meta('vbegy_head_slide_style','select',$post->ID);
		$slide_overlay = rwmb_meta('vbegy_slide_overlay','select',$post->ID);
		$excerpt_title = rwmb_meta('vbegy_excerpt_title','text',$post->ID);
		$excerpt = rwmb_meta('vbegy_excerpt','text',$post->ID);
		$slides_number = rwmb_meta('vbegy_slides_number','text',$post->ID);
		$slideshow_categories = rwmb_meta('vbegy_slideshow_categories','multicheck',$post->ID);
		$head_slide_background = rwmb_meta('vbegy_head_slide_background','select',$post->ID);
		$head_slide_background_color = rwmb_meta('vbegy_head_slide_background_color','color',$post->ID);
		$head_slide_background_img = rwmb_meta('vbegy_head_slide_background_img','upload',$post->ID);
		$head_slide_background_repeat = rwmb_meta('vbegy_head_slide_background_repeat','select',$post->ID);
		$head_slide_background_fixed = rwmb_meta('vbegy_head_slide_background_fixed','select',$post->ID);
		$head_slide_background_position_x = rwmb_meta('vbegy_head_slide_background_position_x','select',$post->ID);
		$head_slide_background_position_y = rwmb_meta('vbegy_head_slide_background_position_y','select',$post->ID);
		$head_slide_background_position = $head_slide_background_position_x." ".$head_slide_background_position_y;
		$head_slide_full_screen_background = rwmb_meta('vbegy_head_slide_background_full','checkbox',$post->ID);
		if ($head_slide_full_screen_background == 1) {
			$head_slide_full_screen_background = "on";
		}
		$head_slide_custom_background = "on";
		$news_ticker = rwmb_meta('vbegy_news_ticker','checkbox',$post->ID);
		if ($news_ticker == 1) {
			$news_ticker = "on";
		}
		$news_excerpt_title = rwmb_meta('vbegy_news_excerpt_title','text',$post->ID);
		$news_number = rwmb_meta('vbegy_news_number','text',$post->ID);
		$news_categories = rwmb_meta('vbegy_news_categories','multicheck',$post->ID);
	}else {
		$header_style = vpanel_options("header_style");
		$header_menu = vpanel_options("header_menu");
		$header_menu_style = vpanel_options("header_menu_style");
		$header_fixed = vpanel_options("header_fixed");
		$logo_display = vpanel_options("logo_display");
		$logo_img = vpanel_options("logo_img");
		$header_search = vpanel_options("header_search");
		$header_follow = vpanel_options("header_follow");
		$header_follow_style = vpanel_options("header_follow_style");
		$facebook_icon_h = vpanel_options("facebook_icon_h");
		$twitter_icon_h = vpanel_options("twitter_icon_h");
		$gplus_icon_h = vpanel_options("gplus_icon_h");
		$linkedin_icon_h = vpanel_options("linkedin_icon_h");
		$dribbble_icon_h = vpanel_options("dribbble_icon_h");
		$youtube_icon_h = vpanel_options("youtube_icon_h");
		$skype_icon_h = vpanel_options("skype_icon_h");
		$vimeo_icon_h = vpanel_options("vimeo_icon_h");
		$flickr_icon_h = vpanel_options("flickr_icon_h");
		$head_slide = vpanel_options('head_slide');
		$head_slide_style = vpanel_options('head_slide_style');
		$slide_overlay = vpanel_options('slide_overlay');
		$excerpt_title = vpanel_options('excerpt_title');
		$excerpt = vpanel_options('excerpt');
		$slides_number = vpanel_options('slides_number');
		$slideshow_categories = vpanel_options('slideshow_categories');
		$head_slide_background = vpanel_options('head_slide_background');
		$head_slide_custom_background = vpanel_options('head_slide_custom_background');
		$head_slide_background_color = $head_slide_custom_background["color"];
		$head_slide_background_img = $head_slide_custom_background["image"];
		$head_slide_background_repeat = $head_slide_custom_background["repeat"];
		$head_slide_background_position = $head_slide_custom_background["position"];
		$head_slide_background_fixed = $head_slide_custom_background["attachment"];
		$head_slide_full_screen_background = vpanel_options('head_slide_full_screen_background');
		$news_ticker = vpanel_options('news_ticker');
		$news_excerpt_title = vpanel_options('news_excerpt_title');
		$news_number = vpanel_options('news_number');
		$news_categories = vpanel_options('news_categories');
	}
	?>
	<div id="wrap" class="<?php echo esc_attr($grid_template)." ";if ($header_fixed == 'on') {echo "fixed-enabled ";}if (is_category() && $cat_layout != "default") {if ($cat_layout == "fixed") {echo "boxed ";}else if ($cat_layout == "fixed_2") {echo "boxed-2 ";}}else if ((is_single() || is_page()) && $vbegy_layout != "default") {if ($vbegy_layout == "fixed") {echo "boxed ";}else if ($vbegy_layout == "fixed_2") {echo "boxed-2 ";}}else {if ($home_layout == "fixed") {echo "boxed ";}else if ($home_layout == "fixed_2") {echo "boxed-2 ";}}?>">
		<?php if ($header_search == 'on') {?>
			<div class="wrap-search">
				<form role="search" method="get" class="searchform" action="<?php echo home_url('/'); ?>">
					<input type="search" name="s" value="<?php if (get_search_query() != "") {echo the_search_query();}else {_e("Search here ...","vbegy");}?>" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
				</form>
			</div><!-- End wrap-search -->
		<?php }?>
		<header id="header" class="<?php echo ($header_style == 2?'header-2':'').($header_style == 3?'header-3':'').($head_slide == "header"?" header-slide-header":"")?>">
			<?php if ($header_style == 1 || $header_style == 3) {?>
				<div class="container clearfix">
			<?php }?>
				<div class="logo">
					<?php
					if ($logo_display == "custom_image") {?>
					    <a class="logo-img" href="<?php echo esc_url(home_url('/'));?>" itemprop="url" title="<?php echo esc_attr(get_bloginfo('name','display'))?>"><img itemprop="logo" alt="<?php echo esc_attr(get_bloginfo('name','display'))?>" src="<?php echo esc_attr($logo_img)?>"></a>
					<?php }else {?>
						<a href="<?php echo esc_url(home_url('/'));?>" itemprop="url" title="<?php echo esc_attr(get_bloginfo('name','display'))?>"<?php echo ($header_style == 3?" class='logo-name'":"");?>><?php bloginfo('name');?></a>
						<?php if ($header_style == 3) {
							echo "<span>";bloginfo('description');echo "</span>";
						}
					}?>
				</div><!-- End logo -->
				
				<?php 
				if ($header_style == 3) {
					if (is_single() || is_page()) {
						$vbegy_header_adv_type_3 = rwmb_meta('vbegy_header_adv_type_3','radio',$post->ID);
						$vbegy_header_adv_code_3 = rwmb_meta('vbegy_header_adv_code_3','textarea',$post->ID);
						$vbegy_header_adv_href_3 = rwmb_meta('vbegy_header_adv_href_3','text',$post->ID);
						$vbegy_header_adv_img_3 = rwmb_meta('vbegy_header_adv_img_3','upload',$post->ID);
					}
					
					if ((is_single() || is_page()) && (($vbegy_header_adv_type_3 == "display_code" && $vbegy_header_adv_code_3 != "") || ($vbegy_header_adv_type_3 == "custom_image" && $vbegy_header_adv_img_3 != ""))) {
						$header_adv_type_3 = $vbegy_header_adv_type_3;
						$header_adv_code_3 = $vbegy_header_adv_code_3;
						$header_adv_href_3 = $vbegy_header_adv_href_3;
						$header_adv_img_3 = $vbegy_header_adv_img_3;
					}else {
						$header_adv_type_3 = vpanel_options("header_adv_type_3");
						$header_adv_code_3 = vpanel_options("header_adv_code_3");
						$header_adv_href_3 = vpanel_options("header_adv_href_3");
						$header_adv_img_3 = vpanel_options("header_adv_img_3");
					}
					if (($header_adv_type_3 == "display_code" && $header_adv_code_3 != "") || ($header_adv_type_3 == "custom_image" && $header_adv_img_3 != "")) {
						echo '
						<div class="advertising">';
						if ($header_adv_type_3 == "display_code") {
							echo stripcslashes($header_adv_code_3);
						}else {
							if ($header_adv_href_3 != "") {
								echo '<a href="'.esc_url($header_adv_href_3).'">';
							}
							echo '<img alt="" src="'.$header_adv_img_3.'">';
							if ($header_adv_href_3 != "") {
								echo '</a>';
							}
						}
						echo '</div><!-- End advertising -->
						<div class="clearfix"></div>';
					}
				}
				
				if ($header_follow == 'on') {?>
					<div class="header-follow">
						<div class="header-follow-a"><?php _e("Follow Me","vbegy")?></div>
						<div class="follow-social<?php echo ($header_follow_style == 2?' follow-social-2':'')?>">
							<i class="fa fa-caret-up"></i>
							<ul>
								<?php
								if ($facebook_icon_h) {?>
									<li class="social-facebook"><a href="<?php echo esc_url($facebook_icon_h)?>" target="_blank"><i class="fa fa-facebook"></i><?php _e("Facebook","vbegy")?></a></li>
								<?php }
								if ($twitter_icon_h) {?>
									<li class="social-twitter"><a href="<?php echo esc_url($twitter_icon_h)?>" target="_blank"><i class="fa fa-twitter"></i><?php _e("Twitter","vbegy")?></a></li>
								<?php }
								if ($gplus_icon_h) {?>
									<li class="social-google"><a href="<?php echo esc_url($gplus_icon_h)?>" target="_blank"><i class="fa fa-google-plus"></i><?php _e("Google Plus","vbegy")?></a></li>
								<?php }
								if ($linkedin_icon_h) {?>
									<li class="social-linkedin"><a href="<?php echo esc_url($linkedin_icon_h)?>" target="_blank"><i class="fa fa-linkedin"></i><?php _e("Linkedin","vbegy")?></a></li>
								<?php }
								if ($dribbble_icon_h) {?>
									<li class="social-dribbble"><a href="<?php echo esc_url($dribbble_icon_h)?>" target="_blank"><i class="fa fa-dribbble"></i><?php _e("Dribbble","vbegy")?></a></li>
								<?php }
								if ($youtube_icon_h) {?>
									<li class="social-youtube"><a href="<?php echo esc_url($youtube_icon_h)?>" target="_blank"><i class="fa fa-youtube-play"></i><?php _e("Youtube","vbegy")?></a></li>
								<?php }
								if ($vimeo_icon_h) {?>
									<li class="social-vimeo"><a href="<?php echo esc_url($vimeo_icon_h)?>" target="_blank"><i class="fa fa-vimeo-square"></i><?php _e("Vimeo","vbegy")?></a></li>
								<?php }
								if ($skype_icon_h) {?>
									<li class="social-skype"><a href="<?php echo esc_url($skype_icon_h)?>" target="_blank"><i class="fa fa-skype"></i><?php _e("Skype","vbegy")?></a></li>
								<?php }
								if ($flickr_icon_h) {?>
									<li class="social-flickr"><a href="<?php echo esc_url($flickr_icon_h)?>" target="_blank"><i class="fa fa-flickr"></i><?php _e("Flickr","vbegy")?></a></li>
								<?php }?>
							</ul>
						</div><!-- End follow-social -->
					</div><!-- End header-follow -->
				<?php }
				if ($header_search == 'on') {?>
					<div class="header-search">
						<div class="header-search-a"><i class="fa fa-search"></i></div>
					</div><!-- End header-search -->
				<?php }
				if ($header_menu == 'on') {?>
					<nav class="navigation<?php echo ($header_menu_style == 2?' navigation-2':'')?>">
						<?php wp_nav_menu(array('container_class' => 'header-menu','menu_class' => '','theme_location' => 'header_menu','fallback_cb' => 'vpanel_nav_fallback'));?>
					</nav><!-- End navigation -->
					<nav class="navigation_mobile navigation_mobile_main">
						<div class="navigation_mobile_click"><?php _e("Go to...","vbegy")?></div>
						<ul></ul>
					</nav><!-- End navigation_mobile -->
				<?php }
			if ($header_style == 1 || $header_style == 3) {?>
				</div><!-- End container -->
			<?php }?>
			<div class="clearfix"></div>
		</header><!-- End header -->
		
		<?php
		if ($head_slide == "header") {
			include(locate_template("includes/head_slide.php"));
		}
		
		$sidebar_layout = "";
		$container_span = "col-md-8";
		$full_span = "col-md-12";
		$page_right = "page-right-sidebar";
		$page_left = "sections-left-sidebar";
		$page_full_width = "sections-full-width";
		
		if (is_author()) {
			$author_sidebar_layout = vpanel_options('author_sidebar_layout');
			if ($author_sidebar_layout == 'centered') {
				$sidebar_dir = 'sections-centered';
				$homepage_content_span = $container_span;
			}elseif ($author_sidebar_layout == 'left') {
				$sidebar_dir = $page_left;
				$homepage_content_span = $container_span;
			}elseif ($author_sidebar_layout == 'full') {
				$sidebar_dir = $page_full_width;
				$homepage_content_span = $full_span;
			}else {
				$sidebar_dir = $page_right;
				$homepage_content_span = $container_span;
			}
		}else if (is_single() || is_page()) {
			$sidebar_post = rwmb_meta('vbegy_sidebar','radio',$post->ID);
			$sidebar_dir = '';
			if (isset($sidebar_post) && $sidebar_post != "default" && $sidebar_post != "") {
				if ($sidebar_post == 'centered') {
					$sidebar_dir = 'sections-centered';
					$sidebar_dir = 'sections-centered';
					$homepage_content_span = $container_span;
				}elseif ($sidebar_post == 'left') {
					$sidebar_dir = $page_left;
					$homepage_content_span = $container_span;
				}elseif ($sidebar_post == 'full') {
					$sidebar_dir = $page_full_width;
					$homepage_content_span = $full_span;
				}else {
					$sidebar_dir = $page_right;
					$homepage_content_span = $container_span;
				}
			}else {
				$sidebar_dir = $page_right;
				$homepage_content_span = $container_span;
			}
		}else if (is_category()) {
			$cat_sidebar_layout = (isset($categories["cat_sidebar_layout"])?$categories["cat_sidebar_layout"]:"default");
			if ($cat_sidebar_layout == 'centered') {
				$sidebar_dir = 'sections-centered';
				$homepage_content_span = $container_span;
			}elseif ($cat_sidebar_layout == 'left') {
				$sidebar_dir = $page_left;
				$homepage_content_span = $container_span;
			}elseif ($cat_sidebar_layout == 'full') {
				$sidebar_dir = $page_full_width;
				$homepage_content_span = $full_span;
			}else {
				$sidebar_dir = $page_right;
				$homepage_content_span = $container_span;
			}
		}else {
			$sidebar_layout = vpanel_options('sidebar_layout');
			if ($sidebar_layout == 'centered') {
				$sidebar_dir = 'sections-centered';
				$homepage_content_span = $container_span;
			}elseif ($sidebar_layout == 'left') {
				$sidebar_dir = $page_left;
				$homepage_content_span = $container_span;
			}elseif ($sidebar_layout == 'full') {
				$sidebar_dir = $page_full_width;
				$homepage_content_span = $full_span;
			}else {
				$sidebar_dir = $page_right;
				$homepage_content_span = $container_span;
			}
		}
		
		if ((is_author() && $author_sidebar_layout == "default") || ((is_single() || is_page()) && $sidebar_post == "default") || (is_category() && $cat_sidebar_layout == "default")) {
			$sidebar_layout = vpanel_options('sidebar_layout');
			if ($sidebar_layout == 'centered') {
				$sidebar_dir = 'sections-centered';
				$homepage_content_span = $container_span;
			}elseif ($sidebar_layout == 'left') {
				$sidebar_dir = $page_left;
				$homepage_content_span = $container_span;
			}elseif ($sidebar_layout == 'full') {
				$sidebar_dir = $page_full_width;
				$homepage_content_span = $full_span;
			}else {
				$sidebar_dir = $page_right;
				$homepage_content_span = $container_span;
			}
		}
		
		if (is_single() || is_page()) {
			$vbegy_header_adv_type_1 = rwmb_meta('vbegy_header_adv_type_1','radio',$post->ID);
			$vbegy_header_adv_code_1 = rwmb_meta('vbegy_header_adv_code_1','textarea',$post->ID);
			$vbegy_header_adv_href_1 = rwmb_meta('vbegy_header_adv_href_1','text',$post->ID);
			$vbegy_header_adv_img_1 = rwmb_meta('vbegy_header_adv_img_1','upload',$post->ID);
		}
		
		if ((is_single() || is_page()) && (($vbegy_header_adv_type_1 == "display_code" && $vbegy_header_adv_code_1 != "") || ($vbegy_header_adv_type_1 == "custom_image" && $vbegy_header_adv_img_1 != ""))) {
			$header_adv_type_1 = $vbegy_header_adv_type_1;
			$header_adv_code_1 = $vbegy_header_adv_code_1;
			$header_adv_href_1 = $vbegy_header_adv_href_1;
			$header_adv_img_1 = $vbegy_header_adv_img_1;
		}else {
			$header_adv_type_1 = vpanel_options("header_adv_type_1");
			$header_adv_code_1 = vpanel_options("header_adv_code_1");
			$header_adv_href_1 = vpanel_options("header_adv_href_1");
			$header_adv_img_1 = vpanel_options("header_adv_img_1");
		}
		if (($header_adv_type_1 == "display_code" && $header_adv_code_1 != "") || ($header_adv_type_1 == "custom_image" && $header_adv_img_1 != "")) {
			echo '<div class="clearfix"></div>
			<div class="advertising">';
			if ($header_adv_type_1 == "display_code") {
				echo stripcslashes($header_adv_code_1);
			}else {
				if ($header_adv_href_1 != "") {
					echo '<a href="'.esc_url($header_adv_href_1).'">';
				}
				echo '<img alt="" src="'.$header_adv_img_1.'">';
				if ($header_adv_href_1 != "") {
					echo '</a>';
				}
			}
			echo '</div><!-- End advertising -->
			<div class="clearfix"></div>';
		}
		?>
		<div class="clearfix"></div>
		<div class="sections <?php echo esc_attr($sidebar_dir);?>">
			<div class="container">
				<div class="row">
					<div class="<?php echo esc_attr($homepage_content_span).($sidebar_dir == 'sections-centered'?" col-md-offset-2":"");?> main-content">
					
					<?php $post_publish = vpanel_options("post_publish");
					if ($post_publish == "draft") {
						vbegy_session();
					}
					vbegy_session_edit();
					
					if (is_single() || is_page()) {
						$vbegy_header_adv_type = rwmb_meta('vbegy_header_adv_type','radio',$post->ID);
						$vbegy_header_adv_code = rwmb_meta('vbegy_header_adv_code','textarea',$post->ID);
						$vbegy_header_adv_href = rwmb_meta('vbegy_header_adv_href','text',$post->ID);
						$vbegy_header_adv_img = rwmb_meta('vbegy_header_adv_img','upload',$post->ID);
					}
					
					if ((is_single() || is_page()) && (($vbegy_header_adv_type == "display_code" && $vbegy_header_adv_code != "") || ($vbegy_header_adv_type == "custom_image" && $vbegy_header_adv_img != ""))) {
						$header_adv_type = $vbegy_header_adv_type;
						$header_adv_code = $vbegy_header_adv_code;
						$header_adv_href = $vbegy_header_adv_href;
						$header_adv_img = $vbegy_header_adv_img;
					}else {
						$header_adv_type = vpanel_options("header_adv_type");
						$header_adv_code = vpanel_options("header_adv_code");
						$header_adv_href = vpanel_options("header_adv_href");
						$header_adv_img = vpanel_options("header_adv_img");
					}
					if (($header_adv_type == "display_code" && $header_adv_code != "") || ($header_adv_type == "custom_image" && $header_adv_img != "")) {
						echo '<div class="clearfix"></div>
						<div class="advertising">';
						if ($header_adv_type == "display_code") {
							echo stripcslashes($header_adv_code);
						}else {
							if ($header_adv_href != "") {
								echo '<a href="'.esc_url($header_adv_href).'">';
							}
							echo '<img alt="" src="'.$header_adv_img.'">';
							if ($header_adv_href != "") {
								echo '</a>';
							}
						}
						echo '</div><!-- End advertising -->
						<div class="clearfix"></div>';
					}
					?>