<?php
if (is_admin() and isset($_GET['activated']) and $pagenow == "themes.php")
wp_redirect('admin.php?page=options');
define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/admin/' );

/* Load lang languages */
load_theme_textdomain('vbegy',dirname(__FILE__).'/languages');

/* require files */
require_once get_template_directory() . '/admin/options-framework.php';
require_once get_template_directory() . '/admin/meta-box/meta-box.php';
require_once get_template_directory() . '/admin/meta-box/meta_box.php';
require_once get_template_directory() . '/admin/functions/aq_resizer.php';
require_once get_template_directory() . '/admin/functions/main_functions.php';
require_once get_template_directory() . '/admin/functions/widget_functions.php';
require_once get_template_directory() . '/admin/functions/nav_menu.php';
require_once get_template_directory() . '/admin/functions/register_post.php';
require_once get_template_directory() . '/admin/functions/page_builder.php';
require_once get_template_directory() . '/functions/shortcode_logger.php';
require_once get_template_directory() . '/functions/functions_logger.php';
if (!class_exists('TwitterOAuth',false)) {
	require_once (get_template_directory() . '/includes/twitteroauth/twitteroauth.php');
}

$themename = wp_get_theme();
$themename = preg_replace("/\W/", "_", strtolower($themename) );
define("vpanel_name",$themename);

/* Builder */
include get_template_directory() . '/admin/page-builder/builder_page.php';
include get_template_directory() . '/admin/page-builder/slideshow.php';
include get_template_directory() . '/admin/page-builder/box_news.php';
include get_template_directory() . '/admin/page-builder/tabs_news.php';
include get_template_directory() . '/admin/page-builder/pictures_news.php';
include get_template_directory() . '/admin/page-builder/recent_posts.php';
include get_template_directory() . '/admin/page-builder/scroll_news.php';

/* Widgets */
include get_template_directory() . '/admin/widgets/counter.php';
include get_template_directory() . '/admin/widgets/login.php';
include get_template_directory() . '/admin/widgets/signup.php';
include get_template_directory() . '/admin/widgets/posts.php';
include get_template_directory() . '/admin/widgets/posts_images.php';
include get_template_directory() . '/admin/widgets/posts_big_images.php';
include get_template_directory() . '/admin/widgets/posts_slideshow.php';
include get_template_directory() . '/admin/widgets/twitter.php';
include get_template_directory() . '/admin/widgets/flickr.php';
include get_template_directory() . '/admin/widgets/dribbble.php';
include get_template_directory() . '/admin/widgets/youtube.php';
include get_template_directory() . '/admin/widgets/google.php';
include get_template_directory() . '/admin/widgets/facebook.php';
include get_template_directory() . '/admin/widgets/soundcloud.php';
include get_template_directory() . '/admin/widgets/video.php';
include get_template_directory() . '/admin/widgets/subscribe.php';
include get_template_directory() . '/admin/widgets/comments.php';
include get_template_directory() . '/admin/widgets/tabs.php';
include get_template_directory() . '/admin/widgets/about-me.php';
include get_template_directory() . '/admin/widgets/adv-120x600.php';
include get_template_directory() . '/admin/widgets/adv-234x60.php';
include get_template_directory() . '/admin/widgets/adv-250x250.php';
include get_template_directory() . '/admin/widgets/adv-120x240.php';
include get_template_directory() . '/admin/widgets/adv-125x125.php';
include get_template_directory() . '/admin/widgets/adv-300x250.php';
include get_template_directory() . '/admin/widgets/adv-300x600.php';
include get_template_directory() . '/admin/widgets/adv-336x280.php';

/* vbegy_scripts_styles */
function vbegy_scripts_styles() {
	global $post;
	wp_enqueue_style('v_css', get_stylesheet_uri() .  '', '', null, 'all');
	wp_enqueue_style('v_responsive', get_template_directory_uri( __FILE__ )."/css/responsive.css");
	if (is_category()) {
		$category_id = get_query_var('cat');
		$categories = get_option("categories_$category_id");
	}
	$site_skin_all = vpanel_options("site_skin_l");
	if (is_author()) {
		$author_skin_l = vpanel_options("author_skin_l");
		if ($author_skin_l == "site_dark") {
			wp_enqueue_style('v_dark', get_template_directory_uri( __FILE__ )."/css/dark.css");
			add_filter('body_class', 'dark_skin_body_classes');
			function dark_skin_body_classes($classes) {
				$classes[] = 'dark_skin';
				return $classes;
			}
		}
	}else if (is_category()) {
		$cat_skin_l = (isset($categories["cat_skin_l"])?$categories["cat_skin_l"]:"default");
		if ($cat_skin_l == "site_dark") {
			wp_enqueue_style('v_dark', get_template_directory_uri( __FILE__ )."/css/dark.css");
			add_filter('body_class', 'dark_skin_body_classes');
			function dark_skin_body_classes($classes) {
				$classes[] = 'dark_skin';
				return $classes;
			}
		}
	}else if (is_single() || is_page()) {
		$vbegy_site_skin_l = rwmb_meta('vbegy_site_skin_l','radio',$post->ID);
		if ($vbegy_site_skin_l == "site_dark") {
			wp_enqueue_style('v_dark', get_template_directory_uri( __FILE__ )."/css/dark.css");
			add_filter('body_class', 'dark_skin_body_classes');
			function dark_skin_body_classes($classes) {
				$classes[] = 'dark_skin';
				return $classes;
			}
		}
	}else {
		if ($site_skin_all == "site_dark") {
			wp_enqueue_style('v_dark', get_template_directory_uri( __FILE__ )."/css/dark.css");
			add_filter('body_class', 'dark_skin_body_classes');
			function dark_skin_body_classes($classes) {
				$classes[] = 'dark_skin';
				return $classes;
			}
		}
	}
	
	if ((is_author() && $author_skin_l == "default") || ((is_single() || is_page()) && $vbegy_site_skin_l == "default") || (is_category() && $cat_skin_l == "default")) {
		if ($site_skin_all == "site_dark") {
			wp_enqueue_style('v_dark', get_template_directory_uri( __FILE__ )."/css/dark.css");
			add_filter('body_class', 'dark_skin_body_classes');
			function dark_skin_body_classes($classes) {
				$classes[] = 'dark_skin';
				return $classes;
			}
		}
	}
	
	$site_skin = vpanel_options('site_skin');
	if ($site_skin != "default") {
		wp_enqueue_style('skin_'.$site_skin, get_template_directory_uri( __FILE__ )."/css/skins/".$site_skin.".css");
	}else {
		wp_enqueue_style('skin_default', get_template_directory_uri( __FILE__ )."/css/skins/skins.css");
	}
	wp_enqueue_script("v_html5", get_template_directory_uri( __FILE__ )."/js/html5.js",array("jquery"),'1.0.0',true);
	wp_enqueue_script("v_nicescroll", get_template_directory_uri( __FILE__ )."/js/jquery.nicescroll.min.js",array("jquery"),'1.0.0',true);
	wp_enqueue_script("v_modernizr", get_template_directory_uri( __FILE__ )."/js/modernizr.js",array("jquery"),'1.0.0',true);
	wp_enqueue_script("v_isotope", get_template_directory_uri( __FILE__ )."/js/jquery.isotope.min.js",array("jquery"),'1.0.0',true);
	wp_enqueue_script("v_tabs", get_template_directory_uri( __FILE__ )."/js/tabs.js",array("jquery"),'1.0.0',true);
	wp_enqueue_script("v_prettyphoto", get_template_directory_uri( __FILE__ )."/js/jquery.prettyPhoto.js",array("jquery"),'1.0.0',true);
	wp_enqueue_script("v_bxslider", get_template_directory_uri( __FILE__ )."/js/jquery.bxslider.min.js",array("jquery"));
	wp_enqueue_script("v_twitter", get_template_directory_uri( __FILE__ )."/js/twitter/jquery.tweet.js",array("jquery"),'1.0.0',true);
	wp_enqueue_script("v_jflickrfeed", get_template_directory_uri( __FILE__ )."/js/jflickrfeed.min.js",array("jquery"),'1.0.0',true);
	wp_enqueue_script("v_jribbble", get_template_directory_uri( __FILE__ )."/js/jquery.jribbble-1.0.1.ugly.js",array("jquery"),'1.0.0',true);
	wp_enqueue_script("v_inview", get_template_directory_uri( __FILE__ )."/js/jquery.inview.min.js",array("jquery"),'1.0.0',true);
	wp_enqueue_script("v_flexslider", get_template_directory_uri( __FILE__ )."/js/jquery.flexslider.js",array("jquery"),'1.0.0',true);
	wp_enqueue_script("v_tags", get_template_directory_uri( __FILE__ )."/js/tags.js",array("jquery"),'1.0.0',true);
	wp_enqueue_script("v_custom", get_template_directory_uri( __FILE__ )."/js/custom.js",array("jquery"),'1.0.0',true);
	wp_localize_script("v_custom","template_url",get_template_directory_uri( __FILE__ ));
	wp_localize_script("v_custom","go_to",__("Go to...","vbegy"));
	wp_localize_script("v_custom","logger_error_text",__("Please fill the required field.","vbegy"));
	wp_localize_script("v_custom","logger_error_captcha",__("The captcha is incorrect, please try again.","vbegy"));
	wp_localize_script("v_custom","logger_error_empty",__("Fill out all the required fields.","vbegy"));
	wp_localize_script("v_custom","sure_delete",__("Are you sure you want to delete the post?","vbegy"));
	wp_localize_script("v_custom","v_get_template_directory_uri",get_template_directory_uri());
	wp_localize_script("v_custom","admin_url",admin_url("admin-ajax.php"));
	if (is_singular() && comments_open() && get_option('thread_comments'))
		wp_enqueue_script( 'comment-reply');
}
add_action('wp_enqueue_scripts','vbegy_scripts_styles');
/* vbegy_load_theme */
function vbegy_load_theme() {
	/* Default RSS feed links */
	add_theme_support('automatic-feed-links');

	/* Post Thumbnails */
	if ( function_exists( 'add_theme_support' ) ){
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 50, 50, true );
		set_post_thumbnail_size( 60, 60, true );
		set_post_thumbnail_size( 80, 80, true );
	}
	if ( function_exists( 'add_image_size' ) ){
		add_image_size( 'vbegy_img_1', 50, 50, true );
		add_image_size( 'vbegy_img_2', 60, 60, true );
		add_image_size( 'vbegy_img_3', 80, 80, true );
	}
	/* Valid HTML5 */
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
	/* This theme uses its own gallery styles */
	add_filter( 'use_default_gallery_style', '__return_false' );
}
add_action( 'after_setup_theme', 'vbegy_load_theme' );

/* wp head */
function vbegy_head() {
	global $post;
	$default_favicon = get_template_directory_uri()."/images/favicon.png";
	echo '<link rel="shortcut icon" href="'.vpanel_options("favicon").'" type="image/x-icon">' ."\n";

	/* Favicon iPhone */
	if (vpanel_options("iphone_icon")) {
		echo '<link rel="apple-touch-icon-precomposed" href="'.vpanel_options("iphone_icon").'">' ."\n";
	}

	/* Favicon iPhone 4 Retina display */
	if (vpanel_options("iphone_icon_retina")) {
		echo '<link rel="apple-touch-icon-precomposed" sizes="114x114" href="'.vpanel_options("iphone_icon_retina").'">' ."\n";
	}

	/* Favicon iPad */
	if (vpanel_options("ipad_icon")) {
		echo '<link rel="apple-touch-icon-precomposed" sizes="72x72" href="'.vpanel_options("ipad_icon").'">' ."\n";
	}

	/* Favicon iPad Retina display */
	if (vpanel_options("ipad_icon_retina")) {
		echo '<link rel="apple-touch-icon-precomposed" sizes="144x144" href="'.vpanel_options("ipad_icon_retina").'">' ."\n";
	}

	/* Seo */
	$the_seo = stripslashes(vpanel_options("the_keywords"));

	if(vpanel_options("seo_active") == 'on'){
		if( is_home() || is_front_page() ){
			echo '<meta name="description" content="'. get_bloginfo('description') .'">' ."\n";
			echo "<meta name='keywords' content='".$the_seo."'>" ."\n";
		}else if (is_single() || is_page()) {
			if ( have_posts() ) : while ( have_posts() ) : the_post();?>
				<meta name="description" content="<?php the_excerpt_rss(); ?>">
				<?php $posttags = get_the_tags();
					if ($posttags) {
						$the_tags_post = '';
						foreach ($posttags as $tag) {
							$the_tags_post .= $tag->name . ',';
						}
						echo '<meta name="keywords" content="' . trim($the_tags_post, ',') . '">' ."\n";
					}
			endwhile;endif;wp_reset_query();
		}
	}
	
	?>
	<style type="text/css"> 
	<?php echo "\n";
	$vbegy_layout = "";
	if (is_single() || is_page()) {
		global $post;
		$vbegy_layout = rwmb_meta('vbegy_layout','radio',$post->ID);
		$primary_color_p = rwmb_meta('vbegy_primary_color','color',$post->ID);
		$secondary_color_p = rwmb_meta('vbegy_secondary_color','color',$post->ID);
		$vbegy_skin = rwmb_meta('vbegy_skin','radio',$post->ID);
	}
	$cat_layout = "";
	if (is_category()) {
		$cat_layout = (isset($categories["cat_layout"])?$categories["cat_layout"]:"default");
		$background_img = (isset($categories["background_img"])?$categories["background_img"]:"");
		$background_color = (isset($categories["background_color"])?$categories["background_color"]:"");
		$background_repeat = (isset($categories["background_repeat"])?$categories["background_repeat"]:"");
		$background_fixed = (isset($categories["background_fixed"])?$categories["background_fixed"]:"");
		$background_position_x = (isset($categories["background_position_x"])?$categories["background_position_x"]:"");
		$background_position_y = (isset($categories["background_position_y"])?$categories["background_position_y"]:"");
		$cat_full_screen_background = (isset($categories["background_full"])?$categories["background_full"]:"");
		$cat_skin = (isset($categories["cat_skin"])?$categories["cat_skin"]:"default_color");
		$primary_color_c = (isset($categories["primary_color"])?$categories["primary_color"]:"");
		$secondary_color_c = (isset($categories["secondary_color"])?$categories["secondary_color"]:"");
	}
	$author_layout = "";
	if (is_author()) {
		$author_layout = vpanel_options('author_layout');
		$author_background_type = vpanel_options('author_background_type');
		$author_background_color = vpanel_options('author_background_color');
		$author_background_pattern = vpanel_options('author_background_pattern');
		$author_custom_background = vpanel_options('author_custom_background');
		$author_full_screen_background = vpanel_options('author_full_screen_background');
		$vbegy_skin = vpanel_options('author_skin');
		$primary_color_a = vpanel_options('author_primary_color');
		$secondary_color_a = vpanel_options('author_secondary_color');
	}
	
	if (is_category() && $cat_layout != "default") {
		if ($cat_layout != "full") {
			if ($cat_full_screen_background == "on") {?>
				.background-cover {
					<?php if (!empty($background_color)) {?>
					background-color:<?php echo esc_attr($background_color) ?>;
					<?php }?>
					background-image : url('<?php echo esc_attr($background_img)?>') ;<?php echo "\n"; ?>
					filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo esc_attr($background_img)?>',sizingMethod='scale');<?php echo "\n"; ?>
					-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo esc_attr($background_img)?>',sizingMethod='scale')";<?php echo "\n";?>
				}
			<?php }else {?>
				body {
					background:
					<?php 
					if ($cat_full_screen_background != "on") {
						echo esc_attr($background_color)?> url(<?php echo esc_attr($background_img)?>) <?php echo esc_attr($background_repeat)?> <?php echo esc_attr($background_position_x)?> <?php echo esc_attr($background_position_y)?> <?php echo esc_attr($background_fixed)?>;
					<?php }
					?>
				}
			<?php }
		}
	}else if (is_author() && $author_layout != "default") {
		if ($author_layout != "full") {
			$custom_background = $author_custom_background;
			if ($author_full_screen_background == 'on' && $author_background_type != "patterns") {?>
				.background-cover {
					<?php if (!empty($author_background_color)) {?>
					background-color:<?php echo esc_attr($author_background_color) ?>;
					<?php }?>
					background-image : url('<?php echo esc_attr($custom_background["image"])?>') ;<?php echo "\n"; ?>
					filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo esc_attr($custom_background["image"])?>',sizingMethod='scale');<?php echo "\n"; ?>
					-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo ($custom_background["image"])?>',sizingMethod='scale')";<?php echo "\n";?>
				}
			<?php }else {?>
				body {
					background:
					<?php if ($author_background_type == "patterns") {
						if ($author_background_pattern != "default") {?>
						<?php echo esc_attr($author_background_color)?> url(<?php echo esc_attr(get_template_directory_uri())?>/images/patterns/<?php echo esc_attr($author_background_pattern)?>.png) repeat;
						<?php }?>
					<?php }else {
					}
					if (!empty($custom_background)) {
					if ($author_full_screen_background != 'on') {?>
					<?php echo esc_attr($custom_background["color"])?> url(<?php echo esc_attr($custom_background["image"])?>) <?php echo esc_attr($custom_background["repeat"])?> <?php echo esc_attr($custom_background["position"])?> <?php echo esc_attr($custom_background["attachment"])?>;
					<?php }}?>
				}
			<?php }
		}
	}else if ((is_single() || is_page()) && $vbegy_layout != "default"):
		if ($vbegy_layout == "fixed" || $vbegy_layout == "fixed_2"):
			$background_img = rwmb_meta('vbegy_background_img','upload',$post->ID);
			$background_color = rwmb_meta('vbegy_background_color','color',$post->ID);
			$background_repeat = rwmb_meta('vbegy_background_repeat','select',$post->ID);
			$background_fixed = rwmb_meta('vbegy_background_fixed','select',$post->ID);
			$background_position_x = rwmb_meta('vbegy_background_position_x','select',$post->ID);
			$background_position_y = rwmb_meta('vbegy_background_position_y','select',$post->ID);
			if ($background_color || $background_img):
				if (rwmb_meta('vbegy_background_full','checkbox',$post->ID) == 1): ?>
					.background-cover{<?php echo "\n"; ?>
						<?php if (!empty($background_color)) {?>
						background-color:<?php echo esc_attr($background_color) ?>;
						<?php }?>
						background-image : url('<?php echo esc_attr($background_img) ?>') ;<?php echo "\n"; ?>
						filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo esc_attr($background_img) ?>',sizingMethod='scale');<?php echo "\n"; ?>
						-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo esc_attr($background_img) ?>',sizingMethod='scale')";<?php echo "\n"; ?>
					}
				<?php else: ?>
					body{background:<?php echo esc_attr($background_color) ?> url('<?php echo esc_attr($background_img) ?>') <?php echo esc_attr($background_repeat) ?> <?php echo esc_attr($background_fixed) ?> <?php echo esc_attr($background_position_x) ?> <?php echo esc_attr($background_position_y) ?>;}<?php echo "\n"; ?>
				<?php endif;
			endif;
		endif;
	else:
		if (vpanel_options("home_layout") != "full") {
			$custom_background = vpanel_options("custom_background");
			if (vpanel_options("full_screen_background") == 'on' && vpanel_options("background_type") != "patterns") {?>
				.background-cover {
					<?php $background_color_s = vpanel_options("background_color");
					if (!empty($background_color_s)) {?>
					background-color:<?php echo esc_attr($background_color_s) ?>;
					<?php }?>
					background-image : url('<?php echo esc_attr($custom_background["image"])?>') ;<?php echo "\n"; ?>
					filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo esc_attr($custom_background["image"])?>',sizingMethod='scale');<?php echo "\n"; ?>
					-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo esc_attr($custom_background["image"])?>',sizingMethod='scale')";<?php echo "\n";?>
				}
			<?php }else {?>
				body {
					background:
					<?php if (vpanel_options("background_type") == "patterns") {
						if (vpanel_options("background_pattern") != "default") {?>
						<?php echo vpanel_options("background_color")?> url(<?php echo get_template_directory_uri()?>/images/patterns/<?php echo vpanel_options("background_pattern")?>.png) repeat;
						<?php }?>
					<?php }else {
					}
					if (!empty($custom_background)) {
					if (vpanel_options("full_screen_background") != 1) {?>
					<?php echo esc_attr($custom_background["color"])?> url(<?php echo esc_attr($custom_background["image"])?>) <?php echo esc_attr($custom_background["repeat"])?> <?php echo esc_attr($custom_background["position"])?> <?php echo esc_attr($custom_background["attachment"])?>;
					<?php }}?>
				}
			<?php }
		}
	endif;
	if ((is_single() || is_page()) && ($primary_color_p == "" && $secondary_color_p == "")) {
		if ($vbegy_skin != "default_color") {
			if ($vbegy_skin == "default") {
				wp_enqueue_style('skin_skins', get_template_directory_uri( __FILE__ )."/css/skins/skins.css");
			}else if (!empty($vbegy_skin)) {
				wp_enqueue_style('skin_'.$vbegy_skin, get_template_directory_uri( __FILE__ )."/css/skins/".$vbegy_skin.".css");
			}
		}else {
			$primary_color = vpanel_options("primary_color");
			$secondary_color = vpanel_options("secondary_color");
			if ($primary_color != "" && $secondary_color != "") :
				all_css_color($primary_color,$secondary_color);
			endif;
		}
	}else if ((is_single() || is_page()) && ($primary_color_p != "" && $secondary_color_p != "")) {
		all_css_color($primary_color_p,$secondary_color_p);
	}else if (is_category() && ($primary_color_c == "" && $secondary_color_c == "")) {
		if ($cat_skin != "default_color") {
			if ($cat_skin == "default") {
				wp_enqueue_style('skin_skins', get_template_directory_uri( __FILE__ )."/css/skins/skins.css");
			}else if (!empty($cat_skin)) {
				wp_enqueue_style('skin_'.$cat_skin, get_template_directory_uri( __FILE__ )."/css/skins/".$cat_skin.".css");
			}
		}else {
			$primary_color = vpanel_options("primary_color");
			$secondary_color = vpanel_options("secondary_color");
			if ($primary_color != "" && $secondary_color != "") :
				all_css_color($primary_color,$secondary_color);
			endif;
		}
	}else if (is_category() && ($primary_color_c != "" && $secondary_color_c != "")) {
		all_css_color($primary_color_c,$secondary_color_c);
	}else if (is_author() && ($primary_color_a == "" && $secondary_color_a == "")) {
		if ($vbegy_skin != "default_color") {
			if ($vbegy_skin == "default") {
				wp_enqueue_style('skin_skins', get_template_directory_uri( __FILE__ )."/css/skins/skins.css");
			}else if (!empty($vbegy_skin)) {
				wp_enqueue_style('skin_'.$vbegy_skin, get_template_directory_uri( __FILE__ )."/css/skins/".$vbegy_skin.".css");
			}
		}else {
			$primary_color = vpanel_options("primary_color");
			$secondary_color = vpanel_options("secondary_color");
			if ($primary_color != "" && $secondary_color != "") :
				all_css_color($primary_color,$secondary_color);
			endif;
		}
	}else if (is_author() && ($primary_color_a != "" && $secondary_color_a != "")) {
		all_css_color($primary_color_a,$secondary_color_a);
	}else {
		$primary_color = vpanel_options("primary_color");
		$secondary_color = vpanel_options("secondary_color");
		if ($primary_color != "" && $secondary_color != "") :
			all_css_color($primary_color,$secondary_color);
		endif;
	}
	if (is_single() || is_page()) {
		$header_fixed_responsive = rwmb_meta('vbegy_header_fixed_responsive','radio',$post->ID);
		if ($header_fixed_responsive == 1) {?>
			@media only screen and (max-width: 479px) {
				#header.fixed-nav {
					position: relative !important;
				}
			}
		<?php }
	}else {
		if (vpanel_options("header_fixed_responsive") == 'on') {?>
			@media only screen and (max-width: 479px) {
				#header.fixed-nav {
					position: relative !important;
				}
			}
		<?php }
	}
	if (vpanel_options("stripe") == 'on') {?>
		#header:before,.post-head:before,.post-wrap:before,.block-box:before,.widget:before,.page-navigation div:before {
			content: "";
			height: 1px;
			width: 100%;
			background-color: #FFF;
			border-bottom: 1px solid #d3d5d7;
			-webkit-box-shadow: 0 0 5px 0 #e2e3e4;
			-moz-box-shadow: 0 0 5px 0 #e2e3e4;
			box-shadow: 0 0 5px 0 #e2e3e4;
			bottom: 2px;
			left: 0;
			position: absolute;
		}
		#header:after,.widget:after,.post-wrap:after,.block-box:after {
			content: "";
			height: 1px;
			width: 100%;
			background-color: #FFF;
			border-bottom: 1px solid #d3d5d7;
			-webkit-box-shadow: 0 0 5px 0 #e2e3e4;
			-moz-box-shadow: 0 0 5px 0 #e2e3e4;
			box-shadow: 0 0 5px 0 #e2e3e4;
			bottom: 5px;
			left: 0;
			position: absolute;
		}
		.widget:before,.widget:after,.post-head:before,.post-wrap:before,.block-box:before,.post-wrap:after,.block-box:after,.page-navigation div:before {
			-moz-border-radius: 0 0 2px 2px;
			-webkit-border-radius: 0 0 2px 2px;
			border-radius: 0 0 2px 2px;
		}
		.post-quote .post-wrap:before,.post-quote .post-wrap:after,.post-link .post-wrap:before,.post-link .post-wrap:after,.widget-about:before,.widget-about:after,#footer-top .widget:before,#footer-top .widget:after {
			height: 0;
			width: 0;
		}
		.navigation > div > ul > li:hover > ul {
			top: 92px;
		}
	<?php }
	
	/* custom_css */
	if(vpanel_options("custom_css")) {
		echo stripslashes(vpanel_options("custom_css"));
	}
	if (is_single() || is_page()) {
		echo esc_attr(rwmb_meta('vbegy_footer_css','textarea',$post->ID));
	}
	?>
	</style>
	<?php
	
	/* head_code */
	if(vpanel_options("head_code")) {
		echo stripslashes(vpanel_options("head_code"));
	}

}
add_action('wp_head', 'vbegy_head');

/* wp login head */
function vbegy_login_logo() {
	if (vpanel_options("login_logo") != "") {
		echo '<style type="text/css"> h1 a {  background-image:url('.vpanel_options("login_logo").')  !important; background-size: auto !important; } </style>';
	}
}
add_action('login_head',  'vbegy_login_logo');

/* all_css_color */
function all_css_color($color_1,$color_2) {
	?>
	::-moz-selection {
		background: <?php echo esc_attr($color_1);?>;
	}
	::selection {
		background: <?php echo esc_attr($color_1);?>;
	}
	.social-ul li a:hover,.go-up,.widget-about,.post-type,.button,.comment-edit-link,.comment-reply-link,.post-quote .post-wrap,.post-link .post-inner.link,.post-gallery .post-img .bx-controls-direction a:hover,.widget-dribbble .bx-controls-direction a:hover,.related-posts .bx-controls-direction a:hover,.post-soundcloud .post-img,.post-twitter .post-img,.post-facebook .post-img,.widget-posts-img a:before,.widget_tag_cloud a:hover,.widget-login-password a:hover,.navigation.navigation-2 li:hover ul,.related-post-one:hover .related-post-type,.blockquote-2,.widget .search-submit,input[type="submit"],input[type="button"],.portfolio-filter li a:hover,.portfolio-filter li.current a,.portfolio-one:hover .portfolio-content,.block-box-title-more:hover,.block-box-img a:before,.carousel-box-img a:before,#footer-top .widget_tag_cloud a:hover,.box-slideshow .bx-controls-direction a:hover,.news-ticker-title,.news-ticker-content .bx-controls-direction a:hover,.review_rating,.rating_score,.styled-select::before,.fileinputs span,.post-edit a:hover i,.post-delete a:hover i {
		background-color: <?php echo esc_attr($color_1);?>;
	}
	.color,.logo a,.navigation li:hover > a,.navigation li ul li:hover > a,.navigation li.current_page_item > a,.navigation li ul li.current_page_item > a,.navigation.navigation-2 > ul > li.current_page_item > a,.navigation.navigation-2 > ul > li:hover > a,.copyrights a,.post-meta div a,.post-head > h3 a:hover,.post-meta div span,.widget li a:hover,.widget-posts-content span a,.widget .widget-comments-content > a,.page-404 h2,.related-post-head span a,.commentlist li.comment .comment-body .comment-text .author span,.commentlist li.comment .comment-body .comment-text .author span a,blockquote .author,.post-meta .post-tags a:hover,.post a:hover,.accordion-archive ul li:hover:before,.navigation_mobile > ul li:hover > a,.navigation_mobile > ul li:hover:before,.navigation_mobile > ul li:hover > a > span i,.navigation_mobile_click:hover,.navigation_mobile_click:hover:before,.portfolio-meta h6 a:hover,.portfolio-item-2 .portfolio-meta h6 a,.block-box-title-more,.block-box-title a:hover,.block-box-1 li .block-box-content > a:first-child:hover,.block-box-1 li .block-box-content span a,.carousel-box-1 li .block-box-content > a:first-child:hover,.carousel-box-1 li .block-box-content span a,.carousel-box-2 li .block-box-content span a,.footer-subscribe h3,#footer-top a:hover,#footer-top .widget-posts-content span a,#footer-top .widget .post-content-small span a,.box-slideshow-content > a:hover,.box-slideshow-content span a,.criteria_score,.summary_score,.post .post-inner-content a {
		color: <?php echo esc_attr($color_1);?>;
	}
	.loader_html,.widget_tag_cloud a:hover,.related-posts .bx-controls-direction a:hover,blockquote,.block-box-title-more:hover,#footer-top .widget_tag_cloud a:hover {
		border-color: <?php echo esc_attr($color_1);?>;
	}
	.post-quote-top,.post-quote-bottom,.post-link .post-inner.link .fa-link,.widget .widget-posts-img i.fa,.block-box-img:hover i.fa,.carousel-box-img:hover i.fa {
		color: <?php echo esc_attr($color_2);?>;
	}
	.widget-about-img {
		border-color: <?php echo esc_attr($color_2);?>;
	}
	<?php
}

/* Content Width */
if (!isset( $content_width ))
	$content_width = 785;

/* vpanel_feed_request */
function vpanel_feed_request ($qv) {
	if (isset($qv['feed']) && !isset($qv['post_type']))
		$qv['post_type'] = array('post', 'question');
	return $qv;
}
add_filter('request', 'vpanel_feed_request');
/* Save default options */
$default_options = array(
	"loader" => "on",
	"stripe" => "on",
	"seo_active" => "on",
	"header_style" => "1",
	"header_menu" => "on",
	"header_menu_style" => "1",
	"header_fixed" => 0,
	"header_fixed_responsive" => 0,
	"header_search" => "on",
	"header_follow" => "on",
	"header_follow_style" => "1",
	"facebook_icon_h" => "#",
	"twitter_icon_h" => "#",
	"gplus_icon_h" => "#",
	"linkedin_icon_h" => "#",
	"dribbble_icon_h" => "#",
	"youtube_icon_h" => "#",
	"skype_icon_h" => "#",
	"vimeo_icon_h" => "#",
	"flickr_icon_h" => "#",
	"logo_display" => "display_title",
	"head_slide" => "none",
	"head_slide_style" => "slideshow_thumbnail",
	"slide_overlay" => "enable",
	"excerpt_title" => "5",
	"excerpt" => "25",
	"slides_number" => "5",
	"head_slide_background" => "transparent",
	"news_ticker" => "on",
	"news_excerpt_title" => "5",
	"news_number" => "5",
	"the_captcha" => 0,
	"the_captcha_register" => 0,
	"the_captcha_comment" => 0,
	"show_captcha_answer" => 0,
	"captcha_style" => "question_answer",
	"captcha_question" => "What is the capital of Egypt ?",
	"captcha_answer" => "Cairo",
	"add_post_no_register" => 0,
	"post_publish" => "draft",
	"tags_post" => "on",
	"attachment_post" => "on",
	"content_post" => "on",
	"can_edit_post" => 0,
	"post_delete" => 0,
	"author_img" => "on",
	"author_post_style" => "slider",
	"author_blog_pages_show" => 4,
	"author_excerpt_title" => 5,
	"author_sidebar_layout" => "default",
	"author_sidebar" => "default",
	"author_layout" => "default",
	"author_template" => "default",
	"author_skin_l" => "default",
	"author_skin" => "default_color",
	"post_style" => "style_1",
	"author_by" => "on",
	"category_post" => "on",
	"post_meta" => "on",
	"post_review" => "on",
	"post_share" => "on",
	"post_type" => "on",
	"post_author" => "on",
	"post_pagination" => "standard",
	"post_excerpt_title" => 5,
	"post_excerpt" => 40,
	"post_author_box" => "on",
	"related_post" => "on",
	"related_number" => 4,
	"excerpt_related_title" => 5,
	"post_comments" => "on",
	"post_comments_user" => 0,
	"post_navigation" => "on",
	"404_page" => "Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque.",
	"sidebar_layout" => "right",
	"sidebar_home" => "default",
	"else_sidebar" => "default",
	"home_layout" => "full",
	"home_template" => "grid_1200",
	"site_skin_l" => "site_light",
	"site_skin" => "default",
	"footer_top" => "on",
	"subscribe_f" => "on",
	"feedburner_h3" => "Subscribe for our weekly news letter",
	"feedburner_p" => "Suspendisse non augue tincidunt, ullamcorper odio vel, tempor risus. In cursus lacus at mattis consectetur.",
	"footer_layout_1" => "footer_no",
	"footer_layout_2" => "footer_no",
	"footer_copyrights" => "",
	"social_icon_f" => "on",
	"facebook_icon_f" => "#",
	"twitter_icon_f" => "#",
	"gplus_icon_f" => "#",
	"linkedin_icon_f" => "#",
	"dribbble_icon_f" => "#",
	"youtube_icon_f" => "#",
	"skype_icon_f" => "#",
	"vimeo_icon_f" => "#",
	"flickr_icon_f" => "#",
	"rss_icon_f" => "on",
	"rss_icon_f_other" => "",
);

if (!get_option("vpanel_".vpanel_name)) {
	add_option("vpanel_".vpanel_name,$default_options);
}