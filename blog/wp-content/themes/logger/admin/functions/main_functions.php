<?php
/* excerpt */
function excerpt($excerpt_length) {
	global $post;
	$content = $post->post_content;
	$words = explode(' ',$content,$excerpt_length + 1);
	if (count($words) > $excerpt_length) :
		array_pop($words);
		array_push($words,'...');
		$content = implode(' ',$words);
	endif;
		$content = strip_tags($content);
	echo esc_attr($content);
}
/* excerpt_title */
function excerpt_title($excerpt_length) {
	global $post;
	$title = $post->post_title;
	$words = explode(' ',$title,$excerpt_length + 1);
	if (count($words) > $excerpt_length) :
		array_pop($words);
		array_push($words,'');
		$title = implode(' ',$words);
	endif;
		$title = strip_tags($title);
	echo esc_attr($title);
}
/* excerpt_any */
function excerpt_any($excerpt_length,$title) {
	$words = explode(' ',$title,$excerpt_length + 1);
	if (count($words) > $excerpt_length) :
		array_pop($words);
		array_push($words,'');
		$title = implode(' ',$words);
	endif;
		$title = strip_tags($title);
	return $title;
}
/* add post-thumbnails */
add_theme_support('post-thumbnails');
/* get_aq_resize_img */
function get_aq_resize_img($thumbnail_size,$img_width_f,$img_height_f,$img_lightbox="") {
	$thumb = get_post_thumbnail_id();
	if ($thumb != "") {
		$img_url = wp_get_attachment_url($thumb,$thumbnail_size);
		$img_width = $img_width_f;
		$img_height = $img_height_f;
		$image = aq_resize($img_url,$img_width,$img_height,true);
		if ($image) {
			$last_image = $image;
		}else {
			$last_image = "http://placehold.it/".$img_width_f."x".$img_height_f;
		}
		return ($img_lightbox == "lightbox"?"<a href='".$img_url."'>":"")."<img alt='".get_the_title()."' width='".$img_width."' height='".$img_height."' src='".$last_image."'>".($img_lightbox == "lightbox"?"</a>":"");
	}else {
		return ($img_lightbox == "lightbox"?"<a href='".$img_url."'>":"")."<img alt='".get_the_title()."' src='".vpanel_image()."'>".($img_lightbox == "lightbox"?"</a>":"");
	}
}
/* get_aq_resize_img_url */
function get_aq_resize_img_url($url,$thumbnail_size,$img_width_f,$img_height_f) {
	$thumb = $url;
	if ($thumb != "") {
		$img_url = $thumb;
		$img_width = $img_width_f;
		$img_height = $img_height_f;
		$image = aq_resize($img_url,$img_width,$img_height,true);
		if ($image) {
			$last_image = $image;
		}else {
			$last_image = "http://placehold.it/".$img_width_f."x".$img_height_f;
		}
		return "<img alt='".get_the_title()."' width='".$img_width."' height='".$img_height."' src='".$last_image."'>";
	}else {
		return "<img alt='".get_the_title()."' src='".vpanel_image()."'>";
	}
}
/* get_aq_resize_url */
function get_aq_resize_url($url,$thumbnail_size,$img_width_f,$img_height_f) {
	$img_url = $url;
	$img_width = $img_width_f;
	$img_height = $img_height_f;
	$image = aq_resize($img_url,$img_width,$img_height,true);
	if ($image) {
		$last_image = $image;
	}else {
		$last_image = "http://placehold.it/".$img_width_f."x".$img_height_f;
	}
	return $last_image;
}
/* get_aq_resize_img_full */
function get_aq_resize_img_full($thumbnail_size) {
	$thumb = get_post_thumbnail_id();
	if ($thumb != "") {
		$img_url = wp_get_attachment_url($thumb,$thumbnail_size);
		$image = $img_url;
		return "<img alt='".get_the_title()."' src='".$image."'>";
	}else {
		return "<img alt='".get_the_title()."' src='".vpanel_image()."'>";
	}
}
/* vpanel_image */
function vpanel_image() {
	global $post;
	ob_start();
	ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i',$post->post_content,$matches);
	if (isset($matches[1][0])) {
		return $matches[1][0];
	}else {
		return false;
	}
}
/* vpanel_wp_title */
function vpanel_wp_title ($title, $sep) {
    global $paged,$page;
    if (is_feed())
        return $title;

    $title .= get_bloginfo('name','display');

    $site_description = get_bloginfo('description','display');
    if ($site_description && ( is_home() || is_front_page()))
        $title = "$title $sep $site_description";

    if ($paged >= 2 || $page >= 2)
        $title = "$title $sep ".sprintf(__('Page %s','vbegy'),max($paged,$page));

    return $title;
}
add_filter( 'wp_title', 'vpanel_wp_title', 10, 2 );
/* formatMoney */
function formatMoney($number,$fractional=false) {
    if ($fractional) {
        $number = sprintf('%.2f',$number);
    }
    while (true) {
        $replaced = preg_replace('/(-?\d+)(\d\d\d)/','$1,$2',$number);
        if ($replaced != $number) {
            $number = $replaced;
        }else {
            break;
        }
    }
    return $number;
}
/* get_gplus_count */
function get_gplus_count($url) { //Google+
    if ($url != '') {
        $first_curl_function = strrev('tini_lruc');
        $ch = $first_curl_function();
        curl_setopt($ch,CURLOPT_URL,"https://clients6.google.com/rpc?key=AIzaSyCKSbrvQasunBoV16zDH9R33D88CeLr9gQ");
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch,CURLOPT_POSTFIELDS,'[{"method":"pos.plusones.get","id":"p",
		"params":{"nolog":true,"id":"https://plus.google.com/' . $url . '","source":"widget","userId":"@viewer","groupId":"@self"},
		"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HTTPHEADER,array('Content-type: application/json'));
        $curl_function_2 = strrev('cexe_lruc');
        $result = $curl_function_2 ($ch);
        curl_close ($ch);
        $json = json_decode($result,true);
        return(formatMoney($json[0]['result']['metadata']['globalCounts']['count']));
    }else {
        return(0);
    }
}
/* get_facebook_count */
function get_facebook_count($fan_page) { //Facebook
    if ($fan_page != '') {
        $url = 'http://graph.facebook.com/'.trim($fan_page);
        $file_get_function =  strrev('stnetnoc_teg_elif');
        $counter = json_decode($file_get_function($url));
        return(formatMoney(intval($counter->likes)));
    }else {
        return('0');
    }
}
/* get_twitter_count */
function get_twitter_count($user_name) {
	if ($user_name != "") {
		try {
			$twitter_username 		= $user_name;
			$consumer_key 			= vpanel_options('twitter_consumer_key');
			$consumer_secret		= vpanel_options('twitter_consumer_secret');
			$access_token 			= vpanel_options('twitter_access_token');
			$access_token_secret 	= vpanel_options('twitter_access_token_secret');
			
			$twitterConnection = new TwitterOAuth( $consumer_key , $consumer_secret , $access_token , $access_token_secret	);
			$twitterData = $twitterConnection->get('users/show', array('screen_name' => $twitter_username));
			$twitter['page_url'] = 'http://www.twitter.com/'.$twitter_username;
			$twitter['followers_count'] = $twitterData->followers_count;;
		}catch (Exception $e) {
			$twitter['page_url'] = 'http://www.twitter.com/'.$twitter_username;
			$twitter['followers_count'] = 0;
		}
		if( !empty( $twitter['followers_count'] ) &&  get_option( 'followers_count') != $twitter['followers_count'] )
			update_option( 'followers_count' , $twitter['followers_count'] );
			
		if( $twitter['followers_count'] == 0 && get_option( 'followers_count') )
			$twitter['followers_count'] = get_option( 'followers_count');
				
		elseif( $twitter['followers_count'] == 0 && !get_option( 'followers_count') )
			$twitter['followers_count'] = 0;
		
		return $twitter;
	}
}
/* get_youtube_count */
function get_youtube_count ($channel_name) {
    if ($channel_name != '') {
        $file_get_function = strrev('stnetnoc_teg_elif');
        $youtube_data = $file_get_function('http://gdata.youtube.com/feeds/api/users/' . trim($channel_name) . '?alt=json');
        $youtube_data = json_decode($youtube_data, true);
        $youtube_count = $youtube_data['entry']['yt$statistics']['subscriberCount'];
        if (intval($youtube_count) <= 0) return 0;
        return (formatMoney($youtube_count));
    }
}
/* get_vimeo_count */
function get_vimeo_count($channel_name) {
    if($channel_name != '') {
        $url = 'http://vimeo.com/api/v2/channel/'.$channel_name.'/info.json';
        $file_get_function =  strrev('stnetnoc_teg_elif');
        $vimeo_data = json_decode($file_get_function($url));
        if (intval($vimeo_data->total_subscribers) <= 0) return 0;
        return(formatMoney($vimeo_data->total_subscribers));
    }
    return 0;
}
/* get_dribbble_count */
function get_dribbble_count($drbl_un, $cache, $expire){
		$drbl = 'http://api.dribbble.com/players/'.$drbl_un;
		$expire = time()-$expire;
		$cache_data = FetchData($cache);
		if((filemtime($cache) < $expire) or (!isset($cache_data->dribbble))){
			$drb_followers = $cache_data->dribbble = FetchData($drbl)->followers_count;
			update_cache($cache, $cache_data);
		}else{
			$drb_followers = $cache_data->dribbble;
		}
		return nbr_format($drb_followers);
}
/* FetchData */
function FetchData($json_url='',$use_curl=false){
    if($use_curl){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $json_url);
        $json_data = curl_exec($ch);
        curl_close($ch);
        return json_decode($json_data);
    }else{
        $json_data = @file_get_contents($json_url);
        if($json_data == true){
        	return json_decode($json_data);
    	}else{ return null;}
    }
}
/* update_cache */
function update_cache($cache_url, $cache_data){
	//update the cache file
	$fh = fopen($cache_url, 'w')or die("Error opening output file");
	fwrite($fh, json_encode($cache_data,JSON_UNESCAPED_UNICODE));
	fclose($fh);
}
/* nbr_format */
function nbr_format($nbr){
	if (is_numeric($nbr)) {
		return number_format($nbr);
	}else{
		return null;
	}
}
/* Vpanel_posts */
function Vpanel_posts($posts_per_page = 5,$orderby,$display_date,$posts_excerpt,$excerpt_title = 5,$show_images = "on",$post_or_portfolio = "post",$display_review = "on") {
	global $post;
	$author_by = vpanel_options("author_by");
	if ($orderby == "popular") {
		$orderby = array('orderby' => 'comment_count');
	}elseif ($orderby == "random") {
		$orderby = array('orderby' => 'rand');
	}else {
		$orderby = array();
	}
	query_posts(array_merge($orderby,array('post_type' => $post_or_portfolio,'ignore_sticky_posts' => 1,'posts_per_page' => $posts_per_page)));
	if ( have_posts()) :
		echo "<ul>";
			while ( have_posts() ) : the_post();
				$vbegy_what_post = rwmb_meta('vbegy_what_post','select',$post->ID);
				$video_type = rwmb_meta('vbegy_video_post_type',"select",$post->ID);
				$post_username = get_post_meta($post->ID, 'post_username',true);
				$post_email = get_post_meta($post->ID, 'post_email',true);?>
				<li class="widget-posts-<?php if (is_sticky()) {?>sticky<?php }else if ($vbegy_what_post == "google") {?>google<?php }else if ($vbegy_what_post == "audio") {?>volume-up<?php }else if ($vbegy_what_post == "video") {if ($video_type == 'youtube') {?>youtube<?php }else if ($video_type == 'vimeo') {?>vimeo<?php }else if ($video_type == 'daily') {?>daily<?php }?><?php }else if ($vbegy_what_post == "slideshow") {?>slideshow<?php }else if ($vbegy_what_post == "quote") {?>quote<?php }else if ($vbegy_what_post == "link") {?>link<?php }else if ($vbegy_what_post == "soundcloud") {?>soundcloud<?php }else if ($vbegy_what_post == "twitter") {?>twitter<?php }else if ($vbegy_what_post == "facebook") {?>facebook<?php }else {if (has_post_thumbnail()) {?>image<?php }else {?>text<?php }}?><?php echo (has_post_thumbnail()?'':' widget-no-img')?>">
					<?php if ($show_images == "on") {?>
						<div class="widget-posts-img">
							<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark">
								<i class="fa fa-<?php if (is_sticky()) {?>thumb-tack<?php }else if ($vbegy_what_post == "google") {?>map-marker<?php }else if ($vbegy_what_post == "audio") {?>volume-up<?php }else if ($vbegy_what_post == "video") {if ($video_type == 'youtube') {?>youtube-play<?php }else if ($video_type == 'vimeo') {?>vimeo-square<?php }else if ($video_type == 'daily') {?>video-camera<?php }?><?php }else if ($vbegy_what_post == "slideshow") {?>film<?php }else if ($vbegy_what_post == "quote") {?>quote-left<?php }else if ($vbegy_what_post == "link") {?>link<?php }else if ($vbegy_what_post == "soundcloud") {?>soundcloud<?php }else if ($vbegy_what_post == "twitter") {?>twitter<?php }else if ($vbegy_what_post == "facebook") {?>facebook<?php }else {if (has_post_thumbnail()) {?>image<?php }else {?>file-text<?php }}?>"></i>
								<?php if (has_post_thumbnail()) {echo get_aq_resize_img('full',70,70);}?>
							</a>
						</div>
					<?php }?>
					<div class="widget-posts-content">
						<a href="<?php the_permalink();?>" title="<?php printf('%s',the_title_attribute('echo=0')); ?>" rel="bookmark"><?php excerpt_title($excerpt_title);?></a>
						<?php if ($author_by == 'on') {?>
							<span><i class="fa fa-user"></i><?php _e("by","vbegy")?> : <?php echo ($post->post_author > 0?the_author_posts_link():$post_username);?></span>
						<?php }?>
						<?php if ($display_date == "on") {?>
						<span><i class="fa fa-clock-o"></i><?php the_time('F j, Y');?></span>
						<?php }
						if ($display_review == "on") {
							echo vbegy_get_review();
						}?>
					</div>
				</li>
			<?php endwhile;
		echo "</ul>";
	endif;
	wp_reset_query();
}
/* Vpanel_post_big_images */
function Vpanel_post_big_images($posts_per_page = 3,$orderby,$post_or_portfolio = "post",$excerpt_title = 5,$posts_excerpt = 10,$display_meta,$display_review = "on") {
	global $post;
	if ($orderby == "popular") {
		$orderby = array('orderby' => 'comment_count');
	}elseif ($orderby == "random") {
		$orderby = array('orderby' => 'rand');
	}else {
		$orderby = array();
	}
	query_posts(array_merge($orderby,array('post_type' => $post_or_portfolio,'ignore_sticky_posts' => 1,'posts_per_page' => $posts_per_page)));
	if ( have_posts()) :
		echo "<ul>";
			while ( have_posts() ) : the_post();?>
				<li class="widget-posts-image">
					<?php if (has_post_thumbnail()) {?>
						<div class="post-img-big">
							<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark">
								<?php echo get_aq_resize_img('full',320,320)?>
							</a>
						</div>
					<?php }?>
					<div class="clearfix"></div>
					<div class="post-content-small">
						<h3>
							<a href="<?php the_permalink();?>" title="<?php printf('%s',the_title_attribute('echo=0')); ?>" rel="bookmark">
								<?php if ($posts_excerpt == 0 && $show_images != "on") {?>
									<i class="fa fa-angle-double-right"></i>
								<?php }
								excerpt_title($excerpt_title);?>
							</a>
						</h3>
						<?php if ($display_meta == "on") {?>
							<div class="clearfix"></div>
							<span <?php echo ($posts_excerpt == 0?"class='margin_t_5'":"")?>><?php the_time('F j, Y');?></span>
							<span <?php echo ($posts_excerpt == 0?"class='margin_t_5'":"")?>><?php comments_popup_link(__('0 Comments', 'vbegy'), __('1 Comment', 'vbegy'), '% '.__('Comments', 'vbegy'));?></span>
						<?php }
						if ($posts_excerpt != 0) {?>
							<p><?php excerpt($posts_excerpt);?></p>
						<?php }
						if ($display_review == "on") {
							echo vbegy_get_review();
						}?>
					</div>
				</li>
			<?php endwhile;
		echo "</ul>";
	endif;
	wp_reset_query();
}
/* Vpanel_post_slideshow */
function Vpanel_post_slideshow($posts_per_page,$orderby,$post_or_portfolio = "post") {
	global $post;
	if ($orderby == "popular") {
		$orderby = array('orderby' => 'comment_count');
	}elseif ($orderby == "random") {
		$orderby = array('orderby' => 'rand');
	}else {
		$orderby = array();
	}
	query_posts(array_merge($orderby,array('post_type' => $post_or_portfolio,'ignore_sticky_posts' => 1,'posts_per_page' => $posts_per_page)));
	if ( have_posts()) :
		$post_width = 300;
		$post_height = 300;
		$excerpt_related_title = vpanel_options('excerpt_related_title') ? vpanel_options('excerpt_related_title') : 5;?>
		<div class="related-posts">
			<div>
				<?php while ( have_posts() ) : the_post();
					$vbegy_what_post = rwmb_meta('vbegy_what_post','select',$post->ID);
					$video_id = rwmb_meta('vbegy_video_post_id',"select",$post->ID);
					$video_type = rwmb_meta('vbegy_video_post_type',"text",$post->ID);
					if ($video_type == 'youtube') {
						$type = "http://www.youtube.com/embed/".$video_id;
					}else if ($video_type == 'vimeo') {
						$type = "http://player.vimeo.com/video/".$video_id;
					}else if ($video_type == 'daily') {
						$type = "http://www.dailymotion.com/swf/video/".$video_id;
					}
					$vbegy_slideshow_type = rwmb_meta('vbegy_slideshow_type','select',$post->ID);
					if (has_post_thumbnail() || $vbegy_what_post == "video") {?>
						<div class="related-post-item">
							<div class="related-post-one">
								<div class="related-post-img">
									<a itemprop="url" href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark">
										<?php
										if ($vbegy_what_post == "image" || $vbegy_what_post == "slideshow") {
											if (has_post_thumbnail() && $vbegy_what_post == "image") {
												echo get_aq_resize_img('full',$post_width,$post_height,$img_lightbox = "lightbox");
											}else if (has_post_thumbnail() && $vbegy_what_post == "slideshow") {
												echo get_aq_resize_img('full',$post_width,$post_height);
											}
										}else if ($vbegy_what_post == "video") {
											echo '<iframe height="'.$post_height.'" src="'.$type.'"></iframe>';
										}else {
											if (has_post_thumbnail()) {
												echo get_aq_resize_img('full',$post_width,$post_height,$img_lightbox = "lightbox");
											}
										}
										?>
									</a>
									<div class="related-post-type">
										<i class="fa fa-<?php if (is_sticky()) {?>thumb-tack<?php }else if ($vbegy_what_post == "google") {?>map-marker<?php }else if ($vbegy_what_post == "video") {if ($video_type == 'youtube') {?>youtube-play<?php }else if ($video_type == 'vimeo') {?>vimeo-square<?php }else if ($video_type == 'daily') {?>video-camera<?php }?><?php }else if ($vbegy_what_post == "slideshow") {?>film<?php }else if ($vbegy_what_post == "quote") {?>quote-left<?php }else if ($vbegy_what_post == "link") {?>link<?php }else if ($vbegy_what_post == "soundcloud") {?>soundcloud<?php }else if ($vbegy_what_post == "twitter") {?>twitter<?php }else if ($vbegy_what_post == "facebook") {?>facebook<?php }else {if (has_post_thumbnail()) {?>image<?php }else {?>file-text<?php }}?>"></i>
									</div>
								</div>
								<div class="related-post-head">
									<a itemprop="url" href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark"><?php excerpt_title($excerpt_related_title)?></a>
									<span><i class="fa fa-clock-o"></i><?php the_time('F j , Y');?></span>
									<span><i class="fa fa-comments"></i><?php comments_popup_link(__('0 Comments', 'vbegy'), __('1 Comment', 'vbegy'), '% '.__('Comments', 'vbegy'));?></span>
								</div>
							</div>
						</div>
					<?php }
				endwhile;?>
			</div>
		</div><!-- End related-posts -->
	<?php endif;
	wp_reset_query();
}
/* Vpanel_posts_images */
function Vpanel_posts_images($posts_per_page = 5,$orderby,$post_or_portfolio = "post") {
	global $post;
	if ($orderby == "popular") {
		$orderby = array('orderby' => 'comment_count');
	}elseif ($orderby == "random") {
		$orderby = array('orderby' => 'rand');
	}else {
		$orderby = array();
	}
	query_posts(array_merge($orderby,array('post_type' => $post_or_portfolio,'ignore_sticky_posts' => 1,'posts_per_page' => $posts_per_page)));
	if ( have_posts()) :
		while ( have_posts() ) : the_post();
			if (has_post_thumbnail()) {?>
				<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark">
					<?php echo get_aq_resize_img('full',76,76)?>
				</a>
			<?php }
		endwhile;
	endif;
	wp_reset_query();
}
/* Vpanel_comments */
function Vpanel_comments($post_or_portfolio = "post",$comments_number = 5,$comment_excerpt = 30,$show_images = "on") {
	$comments = get_comments(array("post_type" => $post_or_portfolio,"status" => "approve","number" => $comments_number));
	echo "<div class='widget-comments'><ul>";
		foreach ($comments as $comment) {
			$you_avatar = get_the_author_meta('you_avatar',$comment->user_id);
			$user_profile_page = get_author_posts_url($comment->user_id);
		    ?>
		    <li>
		    	<?php if ($show_images == "on") {?>
		    		<div class="widget-comments-img">
		    			<?php if ($comment->user_id != 0) {?>
		    				<a href="<?php echo esc_url($user_profile_page)?>">
		    			<?php }
		    				if ($you_avatar && $comment->user_id != 0) {
		    					$you_avatar_img = get_aq_resize_url(esc_attr($you_avatar),"full",65,65);
		    					echo "<img alt='".$comment->comment_author."' src='".$you_avatar_img."'>";
		    				}else {
		    					echo get_avatar(get_the_author_meta('user_email',$comment->user_id),'65','');
		    				}
		    			if ($comment->user_id != 0) {?>
		    				</a>
		    			<?php }?>
		    		</div>
		    	<?php }?>
		    	<div class="widget-comments-content">
		    		<?php echo ($comment->comment_author_url != ""?"<a href='".$comment->comment_author_url."' target='_blank'>":"").strip_tags($comment->comment_author).($comment->comment_author_url != ""?"</a>":"") ?>
		    		<p><a href="<?php echo get_permalink($comment->comment_post_ID);?>#comment-<?php echo esc_attr($comment->comment_ID);?>"><?php echo wp_html_excerpt($comment->comment_content,$comment_excerpt);?></a></p>
		    	</div>
		    </li>
		    <?php
		}
	echo "</ul></div>";
}
/* vbegy_comment */
function vbegy_comment($comment,$args,$depth) {
    $GLOBALS['comment'] = $comment;
    $add_below = '';
    ?>
    <li <?php comment_class('comment');?> id="comment-<?php comment_ID();?>">
    	<div class="comment-body clearfix"> 
    	    <div class="avatar">
    	    	<?php 
    	    	if ($comment->user_id != 0 && get_the_author_meta('you_avatar', $comment->user_id)) {
    	    		$you_avatar_img = get_aq_resize_url(esc_attr(get_the_author_meta('you_avatar', $comment->user_id)),"full",70,70);
    	    		echo "<img alt='".$comment->comment_author."' src='".$you_avatar_img."'>";
    	    	}else {
    	    		echo get_avatar($comment,70);
    	    	}?>
    	    </div>
    	    <div class="comment-text">
    	        <div class="author clearfix">
    	        	<div class="comment-meta">
    	                <span><?php echo get_comment_author();?></span>
    	                <div class="date"><?php printf('%1$s at %2$s',get_comment_date(), get_comment_time()) ?></div>
    	            </div>
    	            <?php
    	            edit_comment_link('Edit','','');
    	            comment_reply_link(array_merge($args,array('reply_text' => __("Reply","vbegy"),'add_below' => $add_below,'depth' => $depth,'max_depth' => $args['max_depth'])));
    	            ?>
    	        </div>
    	        <div class="text">
    	        	<?php if ($comment->comment_approved == '0') : ?>
    	        	    <em><?php _e('Your comment is awaiting moderation.','vbegy')?></em><br>
    	        	<?php endif; ?>
    	        	<?php comment_text() ?>
    	        </div>
    	    </div>
    	</div>
    <?php
}
/* vpanel_pagination */
if ( ! function_exists('vpanel_pagination')) {
	function vpanel_pagination( $args = array(),$query = '') {
		global $wp_rewrite,$wp_query;
		do_action('vpanel_pagination_start');
		if ( $query) {
			$wp_query = $query;
		} // End IF Statement
		/* If there's not more than one page,return nothing. */
		if ( 1 >= $wp_query->max_num_pages)
			return;
		/* Get the current page. */
		$current = ( get_query_var('paged') ? absint( get_query_var('paged')) : 1);
		/* Get the max number of pages. */
		$max_num_pages = intval( $wp_query->max_num_pages);
		/* Set up some default arguments for the paginate_links() function. */
		$defaults = array(
			'base' => add_query_arg('paged','%#%'),
			'format' => '',
			'total' => $max_num_pages,
			'current' => $current,
			'prev_next' => true,
			'prev_text' => __('<i class="fa fa-angle-left"></i>','vbegy'),// Translate in WordPress. This is the default.
			'next_text' => __('<i class="fa fa-angle-right"></i>','vbegy'),// Translate in WordPress. This is the default.
			'show_all' => false,
			'end_size' => 1,
			'mid_size' => 1,
			'add_fragment' => '',
			'type' => 'plain',
			'before' => '<div class="pagination">',// Begin vpanel_pagination() arguments.
			'after' => '</div>',
			'echo' => true,
		);
		/* Add the $base argument to the array if the user is using permalinks. */
		if ( $wp_rewrite->using_permalinks())
			$defaults['base'] = user_trailingslashit( trailingslashit( get_pagenum_link()) . 'page/%#%');
		/* If we're on a search results page,we need to change this up a bit. */
		if ( is_search()) {
		/* If we're in BuddyPress,use the default "unpretty" URL structure. */
			if ( class_exists('BP_Core_User')) {
				$search_query = get_query_var('s');
				$paged = get_query_var('paged');
				$base = user_trailingslashit( home_url()) . '?s=' . $search_query . '&paged=%#%';
				$defaults['base'] = $base;
			} else {
				$search_permastruct = $wp_rewrite->get_search_permastruct();
				if ( !empty( $search_permastruct))
					$defaults['base'] = user_trailingslashit( trailingslashit( get_search_link()) . 'page/%#%');
			}
		}
		/* Merge the arguments input with the defaults. */
		$args = wp_parse_args( $args,$defaults);
		/* Allow developers to overwrite the arguments with a filter. */
		$args = apply_filters('vpanel_pagination_args',$args);
		/* Don't allow the user to set this to an array. */
		if ('array' == $args['type'])
			$args['type'] = 'plain';
		/* Make sure raw querystrings are displayed at the end of the URL,if using pretty permalinks. */
		$pattern = '/\?(.*?)\//i';
		preg_match( $pattern,$args['base'],$raw_querystring);
		if ( $wp_rewrite->using_permalinks() && $raw_querystring)
			$raw_querystring[0] = str_replace('','',$raw_querystring[0]);
			if (!empty($raw_querystring)) {
				@$args['base'] = str_replace( $raw_querystring[0],'',$args['base']);
				@$args['base'] .= substr( $raw_querystring[0],0,-1);
			}
		/* Get the paginated links. */
		$page_links = paginate_links( $args);
		/* Remove 'page/1' from the entire output since it's not needed. */
		$page_links = str_replace( array('&#038;paged=1\'','/page/1\''),'\'',$page_links);
		/* Wrap the paginated links with the $before and $after elements. */
		$page_links = $args['before'] . $page_links . $args['after'];
		/* Allow devs to completely overwrite the output. */
		$page_links = apply_filters('vpanel_pagination',$page_links);
		do_action('vpanel_pagination_end');
		/* Return the paginated links for use in themes. */
		if ( $args['echo'])
			echo $page_links;
		else
			return $page_links;
	}
}
/* vpanel_admin_bar */
function vpanel_admin_bar() {
	global $wp_admin_bar;
	if (is_super_admin()) {
		$wp_admin_bar->add_menu( array(
			'parent' => 0,
			'id' => 'vpanel_page',
			'title' => 'Logger Settings' ,
			'href' => admin_url( 'admin.php?page=options')
		));
	}
}
add_action( 'wp_before_admin_bar_render', 'vpanel_admin_bar' );
/* breadcrumbs */
function breadcrumbs($header_style,$args = array()) {
    $delimiter  = __('<span class="crumbs-span">/</span>','vbegy');
    $home       = '<i class="fa fa-home"></i>'.__('Home','vbegy');
    $before     = '<h1>';
    $after      = '</h1>';
    if ((!is_home() && !is_front_page()) || is_paged()) {
    	if ($header_style == "header_4") {
    		echo '<div class="page-content breadcrumbs_2">';
        }else {
	        echo '<div class="breadcrumbs"><section class="container"><div class="row"><div class="col-md-12">';
        }
        global $post,$wp_query;
        $item = array();
        $homeLink = home_url();
        if ($header_style == "yes") {
	        if (is_page()) {
				echo $before . get_the_title() . $after;
	        }else if (is_attachment()) {
				$parent = get_post($post->post_parent);
				$cat = get_the_category($parent->ID);
				echo $before . get_the_title() . $after;
	        }elseif ( is_singular() ) {
	    		$post = $wp_query->get_queried_object();
	    		$post_id = (int) $wp_query->get_queried_object_id();
	    		$post_type = $post->post_type;
	    		$post_type_object = get_post_type_object( $post_type );
	    		if ( 'post' === $wp_query->post->post_type || 'landing' === $wp_query->post->post_type ) {
	    			echo $before . get_the_title() . $after;
	    		}
	    		if ( 'page' !== $wp_query->post->post_type ) {
	    			if ( isset( $args["singular_{$wp_query->post->post_type}_taxonomy"] ) && is_taxonomy_hierarchical( $args["singular_{$wp_query->post->post_type}_taxonomy"] ) ) {
	    				$terms = wp_get_object_terms( $post_id, $args["singular_{$wp_query->post->post_type}_taxonomy"] );
	    				echo array_merge( $item, breadcrumbs_plus_get_term_parents( $terms[0], $args["singular_{$wp_query->post->post_type}_taxonomy"] ) );
	    			}
	    			elseif ( isset( $args["singular_{$wp_query->post->post_type}_taxonomy"] ) )
	    				echo get_the_term_list( $post_id, $args["singular_{$wp_query->post->post_type}_taxonomy"], '', ', ', '' );
	    		}
	    	}else if (is_category() || is_tag() || is_tax()) {
	            global $wp_query;
	            $term = $wp_query->get_queried_object();
				$taxonomy = get_taxonomy( $term->taxonomy );
				if ( ( is_taxonomy_hierarchical( $term->taxonomy ) && $term->parent ) && $parents = breadcrumbs_plus_get_term_parents( $term->parent, $term->taxonomy ) )
					$item = array_merge( $item, $parents );
				$item['last'] = $term->name;
	            echo $before . '' . single_cat_title('', false) . '' . $after;
	        }elseif (is_day()) {
	            echo $before . __('Daily Archives : ','vbegy') . get_the_time('d') . $after;
	        }elseif (is_month()) {
	            echo $before . __('Monthly Archives : ','vbegy') . get_the_time('F') . $after;
	        }elseif (is_year()) {
	            echo $before . __('Yearly Archives : ','vbegy') . get_the_time('Y') . $after;
	        }elseif (is_single() && !is_attachment()) {
	            if (get_post_type() != 'post') {
	                $post_type = get_post_type_object(get_post_type());
	                $slug = $post_type->rewrite;
	                echo $before . get_the_title() . $after;
	            }else {
					$cat = get_the_category(); $cat = $cat[0];
					echo $before . get_the_title() . $after;
	            }
	        }elseif (!is_single() && !is_page() && get_post_type() != 'post') {
	        	if (is_author()) {
	        	    global $author;
					$userdata = get_userdata($author);
					echo $before . $userdata->display_name . $after;
	        	}else {
					$post_type = get_post_type_object(get_post_type());
					echo $before . (isset($post_type->labels->singular_name)?$post_type->labels->singular_name:__("Error 404","vbegy")) . $after;
	        	}
	        	
	        }elseif (is_attachment()) {
	            $parent = get_post($post->post_parent);
	            $cat = get_the_category($parent->ID);
	            echo $before . get_the_title() . $after;
	        }elseif (is_page() && !$post->post_parent) {
	            echo $before . get_the_title() . $after;
	        }elseif (is_page() && $post->post_parent) {
	            $parent_id  = $post->post_parent;
	            $breadcrumbs = array();
	            while ($parent_id) {
	                $page = get_page($parent_id);
	                $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
	                $parent_id  = $page->post_parent;
	            }
	            $breadcrumbs = array_reverse($breadcrumbs);
	            foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
	            echo $before . get_the_title() . $after;
	        }elseif (is_search()) {
	            echo $before . get_search_query() . $after;
	        }elseif (is_tag()) {
	            echo $before . single_tag_title('', false) . $after;
	        }elseif ( is_author() ) {
	            global $author;
	            $userdata = get_userdata($author);
	            echo $before . $userdata->display_name . $after;
	        }elseif (is_404()) {
	            echo $before . __('Error 404 ', 'vbegy') . $after;
	        }
        }
        
        $before     = '<span class="current">';
        $after      = '</span>';
        echo '<div class="clearfix"></div>
        <div class="crumbs">
        <a itemprop="breadcrumb" href="' . $homeLink . '">' . $home . '</a>' . $delimiter . ' ';
        if (is_category() || is_tag() || is_tax()) {
            global $wp_query;
            $term = $wp_query->get_queried_object();
        	$taxonomy = get_taxonomy( $term->taxonomy );
        	if ( ( is_taxonomy_hierarchical( $term->taxonomy ) && $term->parent ) && $parents = breadcrumbs_plus_get_term_parents( $term->parent, $term->taxonomy ) )
        		$item = array_merge( $item, $parents );
        	$item['last'] = $term->name;
            echo $before . '' . single_cat_title('', false) . '' . $after;
        }elseif (is_day()) {
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>' . $delimiter . '';
            echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a>' . $delimiter . '';
            echo $before . get_the_time('d') . $after;
        }elseif (is_month()) {
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>' . $delimiter . '';
            echo $before . get_the_time('F') . $after;
        }elseif (is_year()) {
            echo $before . get_the_time('Y') . $after;
        }elseif (is_single() && !is_attachment()) {
            if (get_post_type() != 'post') {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>' . $delimiter . '';
                echo "".$before . get_the_title() . $after;
            }else {
                $cat = get_the_category(); $cat = $cat[0];
                echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
                echo $before . get_the_title() . $after;
            }
        }elseif (!is_single() && !is_page() && get_post_type() != 'post') {
            if (is_author()) {
                global $author;
				$userdata = get_userdata($author);
				echo $before . $userdata->display_name . $after;
            }else {
	            $post_type = get_post_type_object(get_post_type());
            	echo $before . (isset($post_type->labels->singular_name)?$post_type->labels->singular_name:__("Error 404","vbegy")) . $after;
            }
        }elseif (is_attachment()) {
            $parent = get_post($post->post_parent);
            $cat = get_the_category($parent->ID);
            echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>' . $delimiter . '';
            echo $before . get_the_title() . $after;
        }elseif (is_page() && !$post->post_parent) {
            echo $before . get_the_title() . $after;
        }elseif (is_page() && $post->post_parent) {
            $parent_id  = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                $parent_id  = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
            echo $before . get_the_title() . $after;
        }elseif (is_search()) {
            echo $before . __('Search results for ', 'vbegy') . '"' . get_search_query() . '"' . $after;
        }elseif (is_tag()) {
            echo $before . __('Posts tagged ', 'vbegy') . '"' . single_tag_title('', false) . '"' . $after;
        }elseif ( is_author() ) {
            global $author;
            $userdata = get_userdata($author);
            echo $before . $userdata->display_name . $after;
        }elseif (is_404()) {
            echo $before . __('Error 404 ', 'vbegy') . $after;
        }
        if (get_query_var('paged')) {
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
            echo "<span class='crumbs-span'>/</span><span class='current'>".__('Page', 'vbegy') . ' ' . get_query_var('paged')."</span>";
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
        }
        if ($header_style == "header_4") {
        	echo '</div></div>';
        }else {
	        echo '</div></div></div></section></div>';
        }
    }
}
/* breadcrumbs_plus_get_term_parents */
function breadcrumbs_plus_get_term_parents( $parent_id = '', $taxonomy = '', $separator = '/' ) {
	$html = array();
	$parents = array();
	if ( empty( $parent_id ) || empty( $taxonomy ) )
		return $parents;
	while ( $parent_id ) {
		$parent = get_term( $parent_id, $taxonomy );
		$parents[] = '<a href="' . get_term_link( $parent, $taxonomy ) . '" title="' . esc_attr( $parent->name ) . '">' . $parent->name . '</a>';
		$parent_id = $parent->parent;
	}
	if ( $parents )
		$parents = array_reverse( $parents );
	return $parents;
}
/* vpanel_show_extra_profile_fields */
add_action( 'show_user_profile', 'vpanel_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'vpanel_show_extra_profile_fields' );
function vpanel_show_extra_profile_fields( $user ) { ?>
	<table class="form-table">
		<tr>
			<th><label for="you_avatar"><?php _e("Your avatar","vbegy")?></label></th>
			<td>
				<input type="text" size="36" class="upload upload_meta regular-text" value="<?php echo esc_attr( get_the_author_meta('you_avatar', $user->ID ) ); ?>" id="you_avatar" name="you_avatar">
				<input id="you_avatar_button" class="upload_image_button button upload-button-2" type="button" value="Upload Image">
			</td>
		</tr>
		<?php if (get_the_author_meta('you_avatar', $user->ID )) {?>
		<tr>
			<th><label><?php _e("Your avatar","vbegy")?></label></th>
			<td>
				<div class="you_avatar"><img alt="" src="<?php echo esc_attr( get_the_author_meta('you_avatar', $user->ID ) ); ?>"></div>
			</td>
		</tr>
		<?php } ?>
	<h3><?php _e( 'Social Networking', 'vbegy' ) ?></h3>
	<table class="form-table">
		<tr>
			<th><label for="google"><?php _e("Google +","vbegy")?></label></th>
			<td>
				<input type="text" name="google" id="google" value="<?php echo esc_url_raw( get_the_author_meta( 'google', $user->ID ) ); ?>" class="regular-text"><br>
			</td>
		</tr>
		<tr>
			<th><label for="twitter"><?php _e("Twitter","vbegy")?></label></th>
			<td>
				<input type="text" name="twitter" id="twitter" value="<?php echo esc_url_raw( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text"><br>
			</td>
		</tr>
		<tr>
			<th><label for="facebook"><?php _e("Facebook","vbegy")?></label></th>
			<td>
				<input type="text" name="facebook" id="facebook" value="<?php echo esc_url_raw( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text"><br>
			</td>
		</tr>
		<tr>
			<th><label for="linkedin"><?php _e("linkedin","vbegy")?></label></th>
			<td>
				<input type="text" name="linkedin" id="linkedin" value="<?php echo esc_url_raw( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text"><br>
			</td>
		</tr>
		<tr>
			<th><label for="youtube"><?php _e("Youtube","vbegy")?></label></th>
			<td>
				<input type="text" name="youtube" id="youtube" value="<?php echo esc_url_raw( get_the_author_meta( 'youtube', $user->ID ) ); ?>" class="regular-text"><br>
			</td>
		</tr>
	</table>
<?php }
/* Save user's meta */
add_action( 'personal_options_update', 'vpanel_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'vpanel_save_extra_profile_fields' );
function vpanel_save_extra_profile_fields( $user_id ) {
	if ( !current_user_can( 'edit_user', $user_id ) ) return false;
	update_user_meta( $user_id, 'google', esc_url_raw($_POST['google'] ));
	update_user_meta( $user_id, 'twitter', esc_url_raw($_POST['twitter']) );
	update_user_meta( $user_id, 'facebook', esc_url_raw($_POST['facebook']) );
	update_user_meta( $user_id, 'linkedin', esc_url_raw($_POST['linkedin']) );
	update_user_meta( $user_id, 'youtube', esc_url_raw($_POST['youtube']) );
	if (isset($_POST['you_avatar'])) {
		update_user_meta( $user_id, 'you_avatar', esc_attr($_POST['you_avatar']) );
	}
}
/* post_like */
function post_like () {
	$id = (int)$_POST['id'];
	$post_like = (int)get_post_meta($id,'post_like',true);
	if(!$post_like)
		$post_like = 0;
	$post_like++;
	$update = update_post_meta($id,'post_like',$post_like);
	if($update) {
		setcookie('logger_post_like'.$id,"logger_like_yes",time()+3600*24*365,'/');
	}
	echo esc_attr($post_like);
	die();
}
add_action('wp_ajax_post_like','post_like');
add_action('wp_ajax_nopriv_post_like','post_like');
/* vbegy_review */
function vbegy_review() {
	global $post;
	$review_title = rwmb_meta('vbegy_review_title','text',$post->ID);
	$brief_summary = rwmb_meta('vbegy_brief_summary','text',$post->ID);
	$review_summary = rwmb_meta('vbegy_review_summary','textarea',$post->ID);
	$review_type = rwmb_meta('vbegy_review_type','select',$post->ID);
	$review_position = rwmb_meta('vbegy_review_position','select',$post->ID);
	$final_score = $item_length = 0;
	?>
    <div itemscope itemtype="http://data-vocabulary.org/Review-aggregate" class="review_box<?php echo ($review_position == "top"?" review_box_top":"").($review_position == "top_f"?" review_box_top_f":"").($review_position == "bottom"?" review_box_bottom":"");?> clearfix">
        <div class="post-title"><?php echo esc_attr($review_title);?></div>
        <?php $builder_rating_item = get_post_meta($post->ID,'builder_rating_item');
        if ($builder_rating_item) {?>
            <div class="review_criteria">
        		<?php
        		$builder_rating_item = $builder_rating_item[0];
        		foreach ($builder_rating_item as $builder_rating) {
        			$item_length ++;
    			    $final_score += $builder_rating["rating_score"];
        			?>
	                <div class="criteria_item clearfix">
	                    <div class="criteria_name <?php echo ($review_type == 'percentage' || $review_type == 'points'?"criteria_score_name":"")?>"><?php echo esc_attr($builder_rating["rating_description"]);?></div>
                    	<?php if ($review_type == 'percentage'):?>
                    		<div class="rating_score" style="width:<?php echo esc_attr($builder_rating["rating_score"]*10)?>%;"><?php echo esc_attr($builder_rating["rating_score"]*10)?> %</div>
                    	<?php elseif ($review_type == 'points'): $point =  $builder_rating["rating_score"]/10; ?>
                    		<div class="rating_score" style="width:<?php echo esc_attr($builder_rating["rating_score"]*10)?>%;"><?php echo esc_attr($builder_rating["rating_score"])?></div>
                    	<?php else:?>
                        	<div class="criteria_stars">
                        		<div class="criteria_active_stars" style="width:<?php echo esc_attr($builder_rating["rating_score"]*10)?>%;"></div>
                        	</div>
                    	<?php endif;?>
	                </div>
            	<?php }?>
            </div>
            
            <div class="review_results">
                <div class="review_summary">
                    <span class="summary_score title"><?php echo esc_attr($brief_summary);?></span>
                    <span class="summary_title"><?php _e('summary','vbegy')?></span> : <?php echo esc_attr($review_summary);?>
                    <div class="clearfix"></div>
                </div>
                <?php if ($review_type == 'percentage'):?>
                <div class="review_rating"><?php _e('rating','vbegy')?> : <?php echo round($final_score/$item_length,1)*10;?>%</div>
                <?php elseif ($review_type == 'points'):?>
                <div class="review_rating"><?php _e('rating','vbegy')?> : <?php echo round($final_score/$item_length,1);?></div>
                <?php else:?>
	                <div class="review_rating"><?php _e('rating','vbegy')?> : <?php echo round($final_score/$item_length,1);?>
		                <div class="criteria_stars">
		                	<div class="criteria_active_stars" style="width:<?php echo round($final_score/$item_length,1)*10?>%;"></div>
		                </div>
	                </div>
                <?php endif;?>
            </div>
        </div>
    <?php }
}
/* vbegy_get_review */
function vbegy_get_review() {
	global $post;
	$final_score = $item_length = 0;
	$builder_rating_item = get_post_meta($post->ID,'builder_rating_item');
    if ($builder_rating_item) {
    	$builder_rating_item = $builder_rating_item[0];
    	foreach ($builder_rating_item as $builder_rating) {
    		$item_length ++;
    	    $final_score += $builder_rating["rating_score"];
    	}
    	$final_score_star = round($final_score/$item_length,1)*10;
    	$out = '<div class="criteria_stars_small">
    	    <div class="criteria_active_stars_small" style="width:'.$final_score_star.'%;"></div>
    	</div>';
    	return $out;
    }
}
/* update_options && reset_options */
if (is_admin()) {
	/* update_options */
	function update_options(){
		global $themename;
		$post_re = $_POST;
		$all_save = $post_re["vpanel_".$themename];
		if(isset($all_save['import_setting']) && $all_save['import_setting'] != "") {
			$data = unserialize(base64_decode($all_save['import_setting']));
			$array_options = array("vpanel_".$themename,"sidebars");
			//echo "<pre>";print_r($all_save);echo "</pre>";
			//die();
			foreach($array_options as $option){
				if(isset($data[$option])){
					update_option($option,$data[$option]);
				}else{
					delete_option($option);
				}
			}
			echo 2;
			die();
		}else {
			update_option("vpanel_".$themename,$post_re["vpanel_".$themename]);
			/* sidebars */
			if (isset($post_re["sidebars"])) {
				update_option("sidebars",$post_re["sidebars"]);
			}else {
				delete_option("sidebars");
			}
			$builder_sidebars = get_option("sidebars");
		}
		die(1);
	}
	add_action( 'wp_ajax_update_options', 'update_options' );
	/* reset_options */
	function reset_options() {
		global $themename;
		$options = & Options_Framework::_optionsframework_options();
		foreach ($options as $option) {
			if (isset($option['id'])) {
				$option_std = $option['std'];
				$option_res[$option['id']] = $option['std'];
			}
		}
		update_option("vpanel_".$themename,$option_res);
		die(1);
	}
	add_action( 'wp_ajax_reset_options', 'reset_options' );
}
?>