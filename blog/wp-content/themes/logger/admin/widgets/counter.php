<?php
/* counter */
add_action( 'widgets_init', 'widget_counter_widget' );
function widget_counter_widget() {
	register_widget( 'Widget_Counter' );
}
class Widget_Counter extends WP_Widget {

	function Widget_Counter() {
		$widget_ops = array( 'classname' => 'widget-statistics'  );
		$control_ops = array( 'id_base' => 'widget_counter' );
		$this->WP_Widget( 'widget_counter','Logger - counter', $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args );
		$title					  = apply_filters('widget_title', $instance['title'] );
		$vimeo					  = esc_attr($instance['vimeo']);
		$dribbble                 = esc_attr($instance['dribbble']);
		$facebook				  = esc_attr($instance['facebook']);
		$twitter				  = esc_attr($instance['twitter']);
		$gplus					  = esc_attr($instance['gplus']);
		$youtube				  = esc_attr($instance['youtube']);
		$social_count_twitter	  = get_twitter_count($twitter);
		$social_count_facebook	  = get_facebook_count($facebook);
		$social_count_gplus 	  = get_gplus_count($gplus);
		$social_count_youtube 	  = get_youtube_count($youtube);
		$social_count_vimeo 	  = get_vimeo_count($vimeo);
		$social_count_dribbble 	  = get_dribbble_count($dribbble,true,"10000000000000");

		echo $before_widget;
			if ( $title )
				echo $before_title.esc_attr($title).$after_title;?>
			
			<ul>
				<?php if ($facebook != "") {?>
				<li class="statistics-facebook"><a href="http://www.facebook.com/<?php echo $facebook;?>" target="_blank"><i class="fa fa-facebook"></i><?php echo $social_count_facebook;?> <span><?php _e("Fans","vbegy")?></span></a></li>
				<?php }
				if ($twitter != "") {?>
				<li class="statistics-twitter"><a href="http://twitter.com/<?php echo $twitter;?>" target="_blank"><i class="fa fa-twitter"></i><?php echo @number_format($social_count_twitter['followers_count']);?> <span><?php _e("Followers","vbegy")?></span></a></li>
				<?php }
				if ($vimeo != "") {?>
				<li class="statistics-vimeo"><a href="http://vimeo.com/<?php echo $vimeo;?>" target="_blank"><i class="fa fa-vimeo-square"></i><?php echo $social_count_vimeo?> <span><?php _e("Subscribers","vbegy")?></span></a></li>
				<?php }
				if ($youtube != "") {?>
				<li class="statistics-youtube"><a href="http://www.youtube.com/user/<?php echo $youtube?>" target="_blank"><i class="fa fa-youtube-play"></i><?php echo $social_count_youtube?> <span><?php _e("Subscribers","vbegy")?></span></a></li>
				<?php }
				if ($gplus != "") {?>
				<li class="statistics-google"><a href="https://plus.google.com/<?php echo $gplus;?>" target="_blank"><i class="fa fa-google-plus"></i><?php echo $social_count_gplus;?> <span><?php _e("Followers","vbegy")?></span></a></li>
				<?php }
				if ($dribbble != "") {?>
				<li class="statistics-dribbble"><a href="https://dribbble.com/<?php echo $dribbble;?>" target="_blank"><i class="fa fa-dribbble"></i><?php echo $social_count_dribbble?> <span><?php _e("Followers","vbegy")?></span></a></li>
				<?php }?>
			</ul>
			<div class="clearfix"></div>
		<?php echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance			  = $old_instance;
		$instance['title']	  = strip_tags( $new_instance['title'] );
		$instance['vimeo']	  = $new_instance['vimeo'];
		$instance['dribbble'] = $new_instance['dribbble'];
		$instance['twitter']  = $new_instance['twitter'];
		$instance['facebook'] = $new_instance['facebook'];
		$instance['gplus']	  = $new_instance['gplus'];
		$instance['youtube']  = $new_instance['youtube'];
		return $instance;
	}

	function form( $instance ) {
		$defaults = array( 'title' => 'Social Statistics','vimeo' => 'vimeo','dribbble' => 'envato','facebook' => 'envato','twitter' => 'envato','gplus' => '+envato','youtube' => 'vbegy');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title : </label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo (isset($instance['title'])?$instance['title']:"");?>" class="widefat" type="text">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'facebook' ); ?>">Facebook Page ID/Name : </label>
			<input id="<?php echo $this->get_field_id( 'facebook' ); ?>" name="<?php echo $this->get_field_name( 'facebook' ); ?>" value="<?php echo (isset($instance['facebook'])?esc_attr($instance['facebook']):"");?>" class="widefat" type="text">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'twitter' ); ?>">Twitter : </label>
			<input id="<?php echo $this->get_field_id( 'twitter' ); ?>" name="<?php echo $this->get_field_name( 'twitter' ); ?>" value="<?php echo (isset($instance['twitter'])?esc_attr($instance['twitter']):"");?>" class="widefat" type="text">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'gplus' ); ?>">Google plus Page ID/Name : </label>
			<input id="<?php echo $this->get_field_id( 'gplus' ); ?>" name="<?php echo $this->get_field_name( 'gplus' ); ?>" value="<?php echo (isset($instance['gplus'])?esc_attr($instance['gplus']):"");?>" class="widefat" type="text">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'youtube' ); ?>">Youtube user : </label>
			<input id="<?php echo $this->get_field_id( 'youtube' ); ?>" name="<?php echo $this->get_field_name( 'youtube' ); ?>" value="<?php echo (isset($instance['youtube'])?esc_attr($instance['youtube']):"");?>" class="widefat" type="text">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'vimeo' ); ?>">Vimeo Page ID/Name : </label>
			<input id="<?php echo $this->get_field_id( 'vimeo' ); ?>" name="<?php echo $this->get_field_name( 'vimeo' ); ?>" value="<?php echo (isset($instance['vimeo'])?esc_attr($instance['vimeo']):"");?>" class="widefat" type="text">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'dribbble' ); ?>">Dribbble Page ID/Name : </label>
			<input id="<?php echo $this->get_field_id( 'dribbble' ); ?>" name="<?php echo $this->get_field_name( 'dribbble' ); ?>" value="<?php echo (isset($instance['dribbble'])?esc_attr($instance['dribbble']):"");?>" class="widefat" type="text">
		</p>
	<?php
	}
}
?>