<?php
/* big pic */
add_action( 'widgets_init', 'widget_posts_widget' );
function widget_posts_widget() {
	register_widget( 'Widget_posts' );
}
class Widget_posts extends WP_Widget {

	function Widget_posts() {
		$widget_ops = array( 'classname' => 'widget-posts'  );
		$control_ops = array( 'id_base' => 'widget_posts' );
		$this->WP_Widget( 'widget_posts','Logger - posts', $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args );
		$title			   = apply_filters('widget_title', $instance['title'] );
		$orderby		   = esc_attr($instance['orderby']);
		$posts_per_page	   = esc_attr($instance['posts_per_page']);
		$display_date	   = esc_attr($instance['display_date']);
		$display_review	   = esc_attr($instance['display_review']);
		$excerpt_title	   = esc_attr($instance['excerpt_title']);
		$show_images	   = esc_attr($instance['show_images']);
		$post_or_portfolio = esc_attr($instance['post_or_portfolio']);
			
		echo $before_widget;
			if ( $title )
				echo $before_title.esc_attr($title).$after_title;
			Vpanel_posts($posts_per_page,$orderby,$display_date,12,$excerpt_title,$show_images,$post_or_portfolio,$display_review);
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance					   = $old_instance;
		$instance['title']			   = strip_tags( $new_instance['title'] );
		$instance['orderby']		   = $new_instance['orderby'];
		$instance['posts_per_page']    = $new_instance['posts_per_page'];
		$instance['display_date']	   = $new_instance['display_date'];
		$instance['display_review']	   = $new_instance['display_review'];
		$instance['excerpt_title']	   = $new_instance['excerpt_title'];
		$instance['show_images']	   = $new_instance['show_images'];
		$instance['post_or_portfolio'] = $new_instance['post_or_portfolio'];
		return $instance;
	}

	function form( $instance ) {
		$defaults = array( 'title' => __('Recent','vbegy'),'orderby' => 'recent','display_date' => 'on','display_review' => 'on','show_images' => 'on','posts_per_page' => '5','excerpt_title' => '5','post_or_portfolio' => 'post' );
		$instance = wp_parse_args( (array) $instance, $defaults );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title : </label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo (isset($instance['title'])?esc_attr($instance['title']):""); ?>" class="widefat" type="text">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'post_or_portfolio' ); ?>">Post or portfolio : </label>
			<select id="<?php echo $this->get_field_id( 'post_or_portfolio' ); ?>" name="<?php echo $this->get_field_name( 'post_or_portfolio' ); ?>">
				<option value="post" <?php if( isset($instance['post_or_portfolio']) && $instance['post_or_portfolio'] == 'post' ) echo "selected=\"selected\""; else echo ""; ?>>Post</option>
				<option value="portfolio" <?php if( isset($instance['post_or_portfolio']) && $instance['post_or_portfolio'] == 'portfolio' ) echo "selected=\"selected\""; else echo ""; ?>>Portfolio</option>
			</select>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php echo (isset($instance['show_images']) && $instance['show_images'] == "on"?' checked="checked"':"");?> id="<?php echo $this->get_field_id( 'show_images' ); ?>" name="<?php echo $this->get_field_name( 'show_images' ); ?>">
			<label for="<?php echo $this->get_field_id( 'show_images' ); ?>">Show images?</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'excerpt_title' ); ?>">The number of words excerpt title</label>
			<input id="<?php echo $this->get_field_id( 'excerpt_title' ); ?>" name="<?php echo $this->get_field_name( 'excerpt_title' ); ?>" value="<?php echo (isset($instance['excerpt_title'])?(int)$instance['excerpt_title']:""); ?>" size="3" type="text">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'orderby' ); ?>">Order by : </label>
			<select id="<?php echo $this->get_field_id( 'orderby' ); ?>" name="<?php echo $this->get_field_name( 'orderby' ); ?>">
				<option value="popular" <?php if( isset($instance['orderby']) && $instance['orderby'] == 'popular' ) echo "selected=\"selected\""; else echo ""; ?>>Popular</option>
				<option value="recent" <?php if( isset($instance['orderby']) && $instance['orderby'] == 'recent' ) echo "selected=\"selected\""; else echo ""; ?>>Recent</option>
				<option value="random" <?php if( isset($instance['orderby']) && $instance['orderby'] == 'random' ) echo "selected=\"selected\""; else echo ""; ?>>Random</option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'posts_per_page' ); ?>">Number of posts to show : </label>
			<input id="<?php echo $this->get_field_id( 'posts_per_page' ); ?>" name="<?php echo $this->get_field_name( 'posts_per_page' ); ?>" value="<?php echo (isset($instance['posts_per_page'])?(int)$instance['posts_per_page']:""); ?>" size="3" type="text">
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php echo (isset($instance['display_date']) && $instance['display_date'] == "on"?' checked="checked"':"");?> id="<?php echo $this->get_field_id( 'display_date' ); ?>" name="<?php echo $this->get_field_name( 'display_date' ); ?>">
			<label for="<?php echo $this->get_field_id( 'display_date' ); ?>">Display date?</label>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php echo (isset($instance['display_review']) && $instance['display_review'] == "on"?' checked="checked"':"");?> id="<?php echo $this->get_field_id( 'display_review' ); ?>" name="<?php echo $this->get_field_name( 'display_review' ); ?>">
			<label for="<?php echo $this->get_field_id( 'display_review' ); ?>">Display review?</label>
		</p>
	<?php
	}
}
?>