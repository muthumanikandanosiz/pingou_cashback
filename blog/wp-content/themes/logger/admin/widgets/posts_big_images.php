<?php
/* big pic */
add_action( 'widgets_init', 'widget_post_big_images_widget' );
function widget_post_big_images_widget() {
	register_widget( 'Widget_post_big_images' );
}
class Widget_post_big_images extends WP_Widget {

	function Widget_post_big_images() {
		$widget_ops = array( 'classname' => 'post_big_images-widget'  );
		$control_ops = array( 'id_base' => 'post_big_images-widget' );
		$this->WP_Widget( 'post_big_images-widget','Logger - posts big images', $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args );
		$title			   = apply_filters('widget_title', $instance['title'] );
		$orderby		   = esc_attr($instance['orderby']);
		$posts_per_page	   = esc_attr($instance['posts_per_page']);
		$post_or_portfolio = esc_attr($instance['post_or_portfolio']);
		$excerpt_title     = esc_attr($instance['excerpt_title']);
		$posts_excerpt     = esc_attr($instance['posts_excerpt']);
		$display_meta      = esc_attr($instance['display_meta']);
		$display_review    = esc_attr($instance['display_review']);
			
		echo $before_widget;
			if ( $title )
				echo $before_title.esc_attr($title).$after_title;?>
			<div class="widget_post_big_img">
				<?php Vpanel_post_big_images($posts_per_page,$orderby,$post_or_portfolio,$excerpt_title,$posts_excerpt,$display_meta,$display_review);?>
			</div>
			<div class="clearfix"></div>
		<?php echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance					   = $old_instance;
		$instance['title']			   = strip_tags( $new_instance['title'] );
		$instance['orderby']		   = $new_instance['orderby'];
		$instance['posts_per_page']    = $new_instance['posts_per_page'];
		$instance['post_or_portfolio'] = $new_instance['post_or_portfolio'];
		$instance['excerpt_title']     = $new_instance['excerpt_title'];
		$instance['posts_excerpt']     = $new_instance['posts_excerpt'];
		$instance['display_meta']      = $new_instance['display_meta'];
		$instance['display_review']    = $new_instance['display_review'];
		return $instance;
	}

	function form( $instance ) {
		$defaults = array( 'title' => __('Recent','vbegy'),'orderby' => 'recent','posts_per_page' => '3','excerpt_title' => '5','posts_excerpt' => '20','display_meta' => 'on','display_review' => 'on','post_or_portfolio' => 'post' );
		$instance = wp_parse_args( (array) $instance, $defaults );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title : </label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo (isset($instance['title'])?esc_attr($instance['title']):""); ?>" class="widefat" type="text">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'post_or_portfolio' ); ?>">Post or portfolio : </label>
			<select id="<?php echo $this->get_field_id( 'post_or_portfolio' ); ?>" name="<?php echo $this->get_field_name( 'post_or_portfolio' ); ?>">
				<option value="post" <?php if( isset($instance['post_or_portfolio']) && $instance['post_or_portfolio'] == 'post' ) echo "selected=\"selected\""; else echo ""; ?>>Post</option>
				<option value="portfolio" <?php if( isset($instance['post_or_portfolio']) && $instance['post_or_portfolio'] == 'portfolio' ) echo "selected=\"selected\""; else echo ""; ?>>Portfolio</option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'excerpt_title' ); ?>">The number of words excerpt title</label>
			<input id="<?php echo $this->get_field_id( 'excerpt_title' ); ?>" name="<?php echo $this->get_field_name( 'excerpt_title' ); ?>" value="<?php echo (isset($instance['excerpt_title'])?(int)$instance['excerpt_title']:""); ?>" size="3" type="text">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'posts_excerpt' ); ?>">The number of words excerpt (If you want an empty type 0)</label>
			<input id="<?php echo $this->get_field_id( 'posts_excerpt' ); ?>" name="<?php echo $this->get_field_name( 'posts_excerpt' ); ?>" value="<?php echo (isset($instance['posts_excerpt'])?(int)$instance['posts_excerpt']:""); ?>" size="3" type="text">
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php echo (isset($instance['display_meta']) && $instance['display_meta'] == "on"?' checked="checked"':"");?> id="<?php echo $this->get_field_id( 'display_meta' ); ?>" name="<?php echo $this->get_field_name( 'display_meta' ); ?>">
			<label for="<?php echo $this->get_field_id( 'display_meta' ); ?>">Display meta?</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'orderby' ); ?>">Order by : </label>
			<select id="<?php echo $this->get_field_id( 'orderby' ); ?>" name="<?php echo $this->get_field_name( 'orderby' ); ?>">
				<option value="popular" <?php if( isset($instance['orderby']) && $instance['orderby'] == 'popular' ) echo "selected=\"selected\""; else echo ""; ?>>Popular</option>
				<option value="recent" <?php if( isset($instance['orderby']) && $instance['orderby'] == 'recent' ) echo "selected=\"selected\""; else echo ""; ?>>Recent</option>
				<option value="random" <?php if( isset($instance['orderby']) && $instance['orderby'] == 'random' ) echo "selected=\"selected\""; else echo ""; ?>>Random</option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'posts_per_page' ); ?>">Number of posts to show : </label>
			<input id="<?php echo $this->get_field_id( 'posts_per_page' ); ?>" name="<?php echo $this->get_field_name( 'posts_per_page' ); ?>" value="<?php echo (isset($instance['posts_per_page'])?(int)$instance['posts_per_page']:""); ?>" size="3" type="text">
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php echo (isset($instance['display_review']) && $instance['display_review'] == "on"?' checked="checked"':"");?> id="<?php echo $this->get_field_id( 'display_review' ); ?>" name="<?php echo $this->get_field_name( 'display_review' ); ?>">
			<label for="<?php echo $this->get_field_id( 'display_review' ); ?>">Display review?</label>
		</p>
	<?php
	}
}
?>