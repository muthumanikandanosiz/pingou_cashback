<?php
/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.WordPress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// Test data
	$test_array = array(
		'one' => 'One',
		'two' => 'Two',
		'three' => 'Three',
		'four' => 'Four',
		'five' => 'Five'
	);

	// Multicheck Array
	$multicheck_array = array(
		'one' => 'French Toast',
		'two' => 'Pancake',
		'three' => 'Omelette',
		'four' => 'Crepe',
		'five' => 'Waffle'
	);

	// Multicheck Defaults
	$multicheck_defaults = array(
		'one' => '1',
		'five' => '1'
	);

	// Background Defaults
	$background_defaults = array(
		'color' => '',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );

	// Typography Defaults
	$typography_defaults = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => '#bada55' );

	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	// Pull all tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}

	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}
	
	// Pull all the sidebars into an array
	$sidebars = get_option('sidebars');
	$new_sidebars = array('default'=> 'Default');
	foreach ($GLOBALS['wp_registered_sidebars'] as $sidebar) {
		$new_sidebars[$sidebar['id']] = $sidebar['name'];
	}
	
	$export = array("vpanel_".vpanel_name,"sidebars");
	$current_options = array();
	foreach ($export as $options) {
		if (get_option($options)) {
			$current_options[$options] = get_option($options);
		}else {
			$current_options[$options] = array();
		}
	}
	$current_options_e = base64_encode(serialize($current_options));
	
	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/admin/images/';

	$options = array();
	
	$options[] = array(
		'name' => 'General settings',
		'type' => 'heading');
	
	$options[] = array(
		'name' => 'Enable loader',
		'desc' => 'Select on to enable loader .',
		'id' => 'loader',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Enable stripe',
		'desc' => 'Select on to enable stripe .',
		'id' => 'stripe',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Enable SEO options',
		'desc' => 'Select on to enable SEO options .',
		'id' => 'seo_active',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => "Header code",
		'desc' => "Past your Google analytics code in the box",
		'id' => 'head_code',
		'std' => '',
		'type' => 'textarea');

	$options[] = array(
		'name' => "Footer code",
		'desc' => "Paste footer code in the box",
		'id' => 'footer_code',
		'std' => '',
		'type' => 'textarea');

	$options[] = array(
		'name' => "Custom CSS code",
		'desc' => "Advanced CSS options , Paste your CSS code in the box",
		'id' => 'custom_css',
		'std' => '',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => "SEO keywords",
		'desc' => "Paste your keywords in the box",
		'id' => 'the_keywords',
		'std' => '',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => "WordPress login logo",
		'desc' => "This is the logo that appears on the default WordPress login page",
		'id' => 'login_logo',
		'type' => 'upload');
	
	$options[] = array(
		'name' => "Custom favicon",
		'desc' => "Upload the site’s favicon here , You can create new favicon here favicon.cc",
		'id' => 'favicon',
		'type' => 'upload');
	
	$options[] = array(
		'name' => "Custom favicon for iPhone",
		'desc' => "Upload your custom iPhone favicon",
		'id' => 'iphone_icon',
		'type' => 'upload');
	
	$options[] = array(
		'name' => "Custom iPhone retina favicon",
		'desc' => "Upload your custom iPhone retina favicon",
		'id' => 'iphone_icon_retina',
		'type' => 'upload');
	
	$options[] = array(
		'name' => "Custom favicon for iPad",
		'desc' => "Upload your custom iPad favicon",
		'id' => 'ipad_icon',
		'type' => 'upload');
	
	$options[] = array(
		'name' => "Custom iPad retina favicon",
		'desc' => "Upload your custom iPad retina favicon",
		'id' => 'ipad_icon_retina',
		'type' => 'upload');
	
	$options[] = array(
		'name' => 'Header settings',
		'type' => 'heading');
	
	$options[] = array(
		'name' => "Header style",
		'desc' => "Select the header style .",
		'id' => "header_style",
		'std' => "1",
		'type' => "select",
		'options' => array(
			'1' => 'Header 1',
			'2' => 'Header 2',
			'3' => 'Header 3',
		)
	);
	
	$options[] = array(
		'name' => 'Advertising type',
		'desc' => 'Advertising type .',
		'id' => 'header_adv_type_3',
		'std' => 'custom_image',
		'type' => 'radio',
		'options' => array("display_code" => "Display code","custom_image" => "Custom Image"));
	
	$options[] = array(
		'name' => 'Image URL',
		'desc' => 'Upload a image, or enter URL to an image if it is already uploaded. ',
		'id' => 'header_adv_img_3',
		'std' => '',
		'type' => 'upload');
	
	$options[] = array(
		'name' => 'Advertising url',
		'desc' => 'Advertising url. ',
		'id' => 'header_adv_href_3',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => "Advertising Code html ( Ex: Google ads)",
		'desc' => "Advertising Code html ( Ex: Google ads)",
		'id' => 'header_adv_code_3',
		'std' => '',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => 'Header menu settings',
		'desc' => 'Select on to enable the menu in the header .',
		'id' => 'header_menu',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Header menu style',
		'desc' => 'Choose the header menu style .',
		'id' => 'header_menu_style',
		'std' => '1',
		'options' => array("1" => "Style 1","2" => "Style 2"),
		'type' => 'radio');
	
	$options[] = array(
		'name' => 'Fixed header option',
		'desc' => 'Select on to enable fixed header .',
		'id' => 'header_fixed',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Stop fixed header in mobile option',
		'desc' => 'Select on to stop fixed header in mobile .',
		'id' => 'header_fixed_responsive',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Logo display',
		'desc' => 'choose Logo display .',
		'id' => 'logo_display',
		'std' => 'display_title',
		'type' => 'radio',
		'options' => array("display_title" => "Display site title","custom_image" => "Custom Image"));
	
	$options[] = array(
		'name' => 'Logo upload',
		'desc' => 'Upload your custom logo. ',
		'id' => 'logo_img',
		'std' => '',
		'type' => 'upload');
	
	$options[] = array(
		'name' => 'Header search settings',
		'desc' => 'Select on to enable the search in the header .',
		'id' => 'header_search',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Header follow settings',
		'desc' => 'Select on to enable the follow me in the header .',
		'id' => 'header_follow',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Header follow style',
		'desc' => 'Choose the header follow style .',
		'id' => 'header_follow_style',
		'std' => '1',
		'options' => array("1" => "Style 1","2" => "Style 2"),
		'type' => 'radio');
	
	$options[] = array(
		'name' => 'Facebook URL',
		'desc' => 'Type the facebook URL from here .',
		'id' => 'facebook_icon_h',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Twitter URL',
		'desc' => 'Type the twitter URL from here .',
		'id' => 'twitter_icon_h',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Google plus URL',
		'desc' => 'Type the google plus URL from here .',
		'id' => 'gplus_icon_h',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Linkedin URL',
		'desc' => 'Type the linkedin URL from here .',
		'id' => 'linkedin_icon_h',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Dribbble URL',
		'desc' => 'Type the dribbble URL from here .',
		'id' => 'dribbble_icon_h',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Youtube URL',
		'desc' => 'Type the youtube URL from here .',
		'id' => 'youtube_icon_h',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Vimeo URL',
		'desc' => 'Type the vimeo URL from here .',
		'id' => 'vimeo_icon_h',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Skype',
		'desc' => 'Type the skype from here .',
		'id' => 'skype_icon_h',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Flickr URL',
		'desc' => 'Type the flickr URL from here .',
		'id' => 'flickr_icon_h',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Home page',
		'type' => 'heading');
	
	$options[] = array(
		'name' => "Head slide",
		'desc' => "Select the Head slide .",
		'id' => "head_slide",
		'std' => "none",
		'type' => "select",
		'options' => array(
			'header' => 'Header',
			'footer' => 'Footer',
			'none' => 'None',
		)
	);
	
	$options[] = array(
		'name' => "Head slide background",
		'desc' => "Select the head slide background .",
		'id' => "head_slide_background",
		'std' => "transparent",
		'type' => "select",
		'options' => array(
			'transparent' => 'Transparent',
			'blue' => 'Dark blue',
			'custom' => 'Custom',
		)
	);
	
	$options[] = array(
		'name' =>  "Custom Background",
		'desc' => "Custom Background",
		'id' => 'head_slide_custom_background',
		'std' => $background_defaults,
		'type' => 'background' );
		
	$options[] = array(
		'name' => "Full Screen Background",
		'desc' => "Click on to Full Screen Background",
		'id' => 'head_slide_full_screen_background',
		'std' => '0',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'News ticker enable or disable',
		'desc' => 'News ticker enable or disable .',
		'id' => 'news_ticker',
		'std' => "on",
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => "News ticker excerpt title",
		'desc' => "put the news ticker excerpt title",
		'id' => 'news_excerpt_title',
		'type' => 'text',
		'std' => "5");
	
	$options[] = array(
		'name' => "News ticker number",
		'desc' => "put the news ticker number",
		'id' => 'news_number',
		'type' => 'text',
		'std' => "5");
	
	$options[] = array(
		'name' => "News ticker categories",
		'desc' => "Select the news ticker categories .",
		'id' => "news_categories",
		'std' => '',
		'options' => $options_categories,
		'type' => 'multicheck');
	
	$options[] = array(
		'name' => "Head slide style",
		'desc' => "Select the head slide style .",
		'id' => "head_slide_style",
		'std' => "slideshow_thumbnail",
		'type' => "select",
		'options' => array(
			'slideshow' => 'Slideshow',
			'slideshow_thumbnail' => 'Slideshow and thumbnail',
			'thumbnail' => 'Thumbnail',
			'none' => 'None',
		)
	);
	
	$options[] = array(
		'name' => "Slideshow overlay",
		'desc' => "Select the slideshow overlay .",
		'id' => "slide_overlay",
		'std' => "enable",
		'type' => "select",
		'options' => array(
			'enable' => 'Enable',
			'disable' => 'Disable',
		)
	);
	
	$options[] = array(
		'name' => "Excerpt title",
		'desc' => "put the excerpt title",
		'id' => 'excerpt_title',
		'type' => 'text',
		'std' => "5");
	
	$options[] = array(
		'name' => "Excerpt",
		'desc' => "put the excerpt",
		'id' => 'excerpt',
		'type' => 'text',
		'std' => "25");
	
	$options[] = array(
		'name' => "Slides number",
		'desc' => "put the slides number",
		'id' => 'slides_number',
		'type' => 'text',
		'std' => "5");
	
	$options[] = array(
		'name' => "Head categories",
		'desc' => "Select the head categories .",
		'id' => "slideshow_categories",
		'std' => '',
		'options' => $options_categories,
		'type' => 'multicheck');
	
	$options[] = array(
		'name' => 'Go to the page and add new page template <a href="post-new.php?post_type=page">from here</a> , choose the template page ( Home ) set it a static page <a href="options-reading.php">from here</a> .',
		'class' => 'home_page_display',
		'type' => 'info');
	
	$options[] = array(
		'name' => 'Captcha setting',
		'type' => 'heading');
	
	$options[] = array(
		'name' => 'Captcha enable or disable in add post form',
		'desc' => 'Captcha enable or disable in add post form .',
		'id' => 'the_captcha',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Captcha enable or disable in register form',
		'desc' => 'Captcha enable or disable in register form .',
		'id' => 'the_captcha_register',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Captcha enable or disable in comment form',
		'desc' => 'Captcha enable or disable in comment form .',
		'id' => 'the_captcha_comment',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => "Captcha style",
		'desc' => "Choose the captcha style",
		'id' => 'captcha_style',
		'std' => 'question_answer',
		'type' => 'radio',
		'options' => 
			array(
				"question_answer" => "Question and answer",
				"normal_captcha" => "Normal captcha"
		)
	);
	
	$options[] = array(
		'name' => 'Captcha answer enable or disable in forms',
		'desc' => 'Captcha answer enable or disable in forms .',
		'id' => 'show_captcha_answer',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => "Captcha question",
		'desc' => "put the Captcha question",
		'id' => 'captcha_question',
		'type' => 'text',
		'std' => "What is the capital of Egypt ?");
	
	$options[] = array(
		'name' => "Captcha answer",
		'desc' => "put the Captcha answer",
		'id' => 'captcha_answer',
		'type' => 'text',
		'std' => "Cairo");
	
	$options[] = array(
		'name' => 'New post setting',
		'type' => 'heading');
	
	$options[] = array(
		'name' => 'Any one can add post without register',
		'desc' => 'Any one can add post without register enable or disable .',
		'id' => 'add_post_no_register',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Choose post status',
		'desc' => 'Choose post status after user publish the post .',
		'id' => 'post_publish',
		'options' => array("publish" => "Publish","draft" => "Draft"),
		'std' => 'draft',
		'type' => 'select');
	
	$options[] = array(
		'name' => 'Tags enable or disable in add post form',
		'desc' => 'Select on to enable the tags in add post form .',
		'id' => 'tags_post',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Attachment in add post form',
		'desc' => 'Select on to enable the attachment in add post form .',
		'id' => 'attachment_post',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Details in add post form is required',
		'desc' => 'Details in add post form is required .',
		'id' => 'content_post',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'The users can edit the posts ?',
		'desc' => 'The users can edit the posts ?',
		'id' => 'can_edit_post',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => "Edit post page",
		'desc' => "Create a page using the Edit post template and select it here",
		'id' => 'edit_post',
		'type' => 'select',
		'options' => $options_pages);
	
	$options[] = array(
		'name' => 'Active user can delete the posts',
		'desc' => 'Select on if you want the user can delete the posts .',
		'id' => 'post_delete',
		'std' => 0,
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Blog & Article settings',
		'type' => 'heading');
	
	$options[] = array(
		'name' => 'Post style',
		'desc' => 'Choose post style from here .',
		'id' => 'post_style',
		'options' => array(
			'style_1' => '1 column',
			'style_2' => '2 columns',
			'style_3' => '3 columns ( work in full with only )',
			'style_4' => '1 columns small image',
			'style_5' => '1 columns large image',
			'style_6' => 'Style 6',
		),
		'std' => 'style_1',
		'type' => 'radio');
	
	$options[] = array(
		'name' => 'Author by enable or disable',
		'desc' => 'Author by enable or disable .',
		'id' => 'author_by',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Category post enable or disable',
		'desc' => 'Category post enable or disable .',
		'id' => 'category_post',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Post meta enable or disable',
		'desc' => 'Post meta enable or disable .',
		'id' => 'post_meta',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Review enable or disable',
		'desc' => 'Review enable or disable .',
		'id' => 'post_review',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Post type enable or disable',
		'desc' => 'Post type enable or disable .',
		'id' => 'post_type',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Post author enable or disable',
		'desc' => 'Post author enable or disable .',
		'id' => 'post_author',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Share enable or disable',
		'desc' => 'Share enable or disable .',
		'id' => 'post_share',
		'std' => 'on',
		'type' => 'checkbox');
		
	$options[] = array(
		'name' => 'Excerpt title ( Work in style 6 )',
		'desc' => 'Put here the excerpt title .',
		'id' => 'post_excerpt_title',
		'std' => 5,
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Excerpt post',
		'desc' => 'Put here the excerpt post .',
		'id' => 'post_excerpt',
		'std' => 40,
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Pagination style',
		'desc' => 'Choose pagination style from here .',
		'id' => 'post_pagination',
		'options' => array(
			'standard' => 'Standard',
			'pagination' => 'Pagination',
		),
		'std' => 'standard',
		'type' => 'radio');
	
	$options[] = array(
		'name' => 'Navigation post enable or disable',
		'desc' => 'Navigation post ( next and previous posts) enable or disable .',
		'id' => 'post_navigation',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Author info box enable or disable',
		'desc' => 'Author info box enable or disable .',
		'id' => 'post_author_box',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Related post enable or disable',
		'desc' => 'Related post enable or disable .',
		'id' => 'related_post',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Related post number',
		'desc' => 'Type related post number from here .',
		'id' => 'related_number',
		'std' => '5',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Excerpt title in related post',
		'desc' => 'Type excerpt title in related post from here .',
		'id' => 'excerpt_related_title',
		'std' => '5',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Comments enable or disable',
		'desc' => 'Comments enable or disable .',
		'id' => 'post_comments',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Content in 404 page',
		'desc' => 'Type here the content in 404 page .',
		'id' => '404_page',
		'std' => 'Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque.',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => 'Sidebar',
		'type' => 'heading');
	
	$options[] = array(
		'desc' => "Add your sidebars .",
		'id' => "sidebars",
		'std' => '',
		'type' => 'sidebar');
	
	$options[] = array(
		'name' => "Sidebar layout",
		'desc' => "Sidebar layout .",
		'id' => "sidebar_layout",
		'std' => "right",
		'type' => "images",
		'options' => array(
			'right' => $imagepath.'sidebar_right.jpg',
			'full' => $imagepath.'sidebar_no.jpg',
			'left' => $imagepath.'sidebar_left.jpg',
			'centered' => $imagepath.'centered.jpg',
		)
	);
	
	$options[] = array(
		'name' => "Home Page Sidebar",
		'desc' => "Home Page Sidebar .",
		'id' => "sidebar_home",
		'std' => '',
		'options' => $new_sidebars,
		'type' => 'select');
	
	$options[] = array(
		'name' => "Else home page , single and page",
		'desc' => "Else home page , single and page .",
		'id' => "else_sidebar",
		'std' => '',
		'options' => $new_sidebars,
		'type' => 'select');
	
	$options[] = array(
		'name' => 'Author Styling',
		'type' => 'heading');
	
	$options[] = array(
		'name' => 'Post author enable or disable in about author box',
		'desc' => 'Post author enable or disable in about author box .',
		'id' => 'author_img',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Post style',
		'desc' => 'Choose post style from here .',
		'id' => 'author_post_style',
		'options' => array(
			'style_1' => '1 column',
			'style_2' => '2 columns',
			'style_3' => '3 columns ( work in full with only )',
			'style_4' => '1 columns small image',
			'style_5' => '1 columns large image',
			'style_6' => 'Style 6',
			'slider'  => 'Slider',
		),
		'std' => 'style_1',
		'type' => 'radio');
	
	$options[] = array(
		'name' => 'Blog pages show at most ( work in slider style only )',
		'desc' => 'Type here blog pages show at most ( work in slider style only )',
		'id' => 'author_blog_pages_show',
		'std' => '4',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Excerpt title in author page',
		'desc' => 'Type excerpt title in author page from here .',
		'id' => 'author_excerpt_title',
		'std' => '5',
		'type' => 'text');
	
	$options[] = array(
		'name' => "Author sidebar layout",
		'desc' => "Author sidebar layout .",
		'id' => "author_sidebar_layout",
		'std' => "default",
		'type' => "images",
		'options' => array(
			'default' => $imagepath.'sidebar_default.jpg',
			'right' => $imagepath.'sidebar_right.jpg',
			'full' => $imagepath.'sidebar_no.jpg',
			'left' => $imagepath.'sidebar_left.jpg',
			'centered' => $imagepath.'centered.jpg',
		)
	);
	
	$options[] = array(
		'name' => "Author Page Sidebar",
		'desc' => "Author Page Sidebar .",
		'id' => "author_sidebar",
		'std' => '',
		'options' => $new_sidebars,
		'type' => 'select');
	
	$options[] = array(
		'name' => "Author page layout",
		'desc' => "Author page layout .",
		'id' => "author_layout",
		'std' => "full",
		'type' => "images",
		'options' => array(
			'default' => $imagepath.'sidebar_default.jpg',
			'full' => $imagepath.'full.jpg',
			'fixed' => $imagepath.'fixed.jpg',
			'fixed_2' => $imagepath.'fixed_2.jpg'
		)
	);
	
	$options[] = array(
		'name' => "Choose template",
		'desc' => "Choose template layout .",
		'id' => "author_template",
		'std' => "grid_1200",
		'type' => "images",
		'options' => array(
			'default' => $imagepath.'sidebar_default.jpg',
			'grid_1200' => $imagepath.'template_1200.jpg',
			'grid_970' => $imagepath.'template_970.jpg'
		)
	);
	
	$options[] = array(
		'name' => "Site skin",
		'desc' => "Choose Site skin .",
		'id' => "author_skin_l",
		'std' => "site_light",
		'type' => "images",
		'options' => array(
			'default' => $imagepath.'sidebar_default.jpg',
			'site_light' => $imagepath.'light.jpg',
			'site_dark' => $imagepath.'dark.jpg'
		)
	);
	
	$options[] = array(
		'name' => "Choose Your Skin",
		'desc' => "Choose Your Skin",
		'class' => "site_skin",
		'id' => "author_skin",
		'std' => "default",
		'type' => "images",
		'options' => array(
			'default'	=> $imagepath.'default.jpg',
			'bright_cyan'	=> $imagepath.'bright_cyan.jpg',
			'bright_pink'	=> $imagepath.'bright_pink.jpg',
			'dark_grayish_blue'	=> $imagepath.'dark_grayish_blue.jpg',
			'dark_grayish_cyan'	=> $imagepath.'dark_grayish_cyan.jpg',
			'dark_grayish_cyan_2'	=> $imagepath.'dark_grayish_cyan_2.jpg',
			'dark_green'	=> $imagepath.'dark_green.jpg',
			'dark_moderate_violet'	=> $imagepath.'dark_moderate_violet.jpg',
			'dark_pink'	=> $imagepath.'dark_pink.jpg',
			'light_orange'	=> $imagepath.'light_orange.jpg',
			'lime_green'	=> $imagepath.'lime_green.jpg',
			'moderate_blue'	=> $imagepath.'moderate_blue.jpg',
			'moderate_blue_2'	=> $imagepath.'moderate_blue_2.jpg',
			'moderate_cyan'	=> $imagepath.'moderate_cyan.jpg',
			'moderate_violet'	=> $imagepath.'moderate_violet.jpg',
			'slightly_desaturated_blue'	=> $imagepath.'slightly_desaturated_blue.jpg',
			'soft_blue'	=> $imagepath.'soft_blue.jpg',
			'soft_cyan'	=> $imagepath.'soft_cyan.jpg',
			'soft_red'	=> $imagepath.'soft_red.jpg',
			'soft_yellow'	=> $imagepath.'soft_yellow.jpg',
			'strong_cyan'	=> $imagepath.'strong_cyan.jpg',
			'strong_orange'	=> $imagepath.'strong_orange.jpg',
			'strong_red'	=> $imagepath.'strong_red.jpg',
			'very_light_pink'	=> $imagepath.'very_light_pink.jpg',
			'vivid_cyan'	=> $imagepath.'vivid_cyan.jpg',
			'vivid_orange'	=> $imagepath.'vivid_orange.jpg',
			'vivid_orange_2'	=> $imagepath.'vivid_orange_2.jpg',
			'vivid_orange_3'	=> $imagepath.'vivid_orange_3.jpg',
			'vivid_yellow'	=> $imagepath.'vivid_yellow.jpg',
			'vivid_yellow_2'	=> $imagepath.'vivid_yellow_2.jpg',
			'yellow'	=> $imagepath.'yellow.jpg',
			'red'	=> $imagepath.'red.jpg',
			'purple'	=> $imagepath.'purple.jpg',
			'orange'	=> $imagepath.'orange.jpg',
			'light_red'	=> $imagepath.'light_red.jpg',
			'green'	=> $imagepath.'green.jpg',
			'gray'	=> $imagepath.'gray.jpg',
			'blue'	=> $imagepath.'blue.jpg',
		)
	);
	
	$options[] = array(
		'name' => "Primary Color",
		'desc' => "Primary Color",
		'id' => 'author_primary_color',
		'type' => 'color' );
	
	$options[] = array(
		'name' => "Secondary Color ( it's darkness more than primary color )",
		'desc' => "Secondary Color ( it's darkness more than primary color )",
		'id' => 'author_secondary_color',
		'type' => 'color' );
	
	$options[] = array(
		'name' => "Background Type",
		'desc' => "Background Type",
		'id' => 'author_background_type',
		'std' => 'patterns',
		'type' => 'radio',
		'options' => 
			array(
				"patterns" => "Patterns",
				"custom_background" => "Custom Background"
			)
	);

	$options[] = array(
		'name' => "Background Color",
		'desc' => "Background Color",
		'id' => 'author_background_color',
		'std' => "#FFF",
		'type' => 'color' );
		
	$options[] = array(
		'name' => "Choose Pattern",
		'desc' => "Choose Pattern",
		'id' => "author_background_pattern",
		'std' => "bg13",
		'type' => "images",
		'options' => array(
			'bg1' => $imagepath.'bg1.jpg',
			'bg2' => $imagepath.'bg2.jpg',
			'bg3' => $imagepath.'bg3.jpg',
			'bg4' => $imagepath.'bg4.jpg',
			'bg5' => $imagepath.'bg5.jpg',
			'bg6' => $imagepath.'bg6.jpg',
			'bg7' => $imagepath.'bg7.jpg',
			'bg8' => $imagepath.'bg8.jpg',
			'bg9' => $imagepath.'../../images/patterns/bg9.png',
			'bg10' => $imagepath.'../../images/patterns/bg10.png',
			'bg11' => $imagepath.'../../images/patterns/bg11.png',
			'bg12' => $imagepath.'../../images/patterns/bg12.png',
			'bg13' => $imagepath.'bg13.jpg',
			'bg14' => $imagepath.'bg14.jpg',
			'bg15' => $imagepath.'../../images/patterns/bg15.png',
			'bg16' => $imagepath.'../../images/patterns/bg16.png',
			'bg17' => $imagepath.'bg17.jpg',
			'bg18' => $imagepath.'bg18.jpg',
			'bg19' => $imagepath.'bg19.jpg',
			'bg20' => $imagepath.'bg20.jpg',
			'bg21' => $imagepath.'../../images/patterns/bg21.png',
			'bg22' => $imagepath.'bg22.jpg',
			'bg23' => $imagepath.'../../images/patterns/bg23.png',
			'bg24' => $imagepath.'../../images/patterns/bg24.png',
	));

	$options[] = array(
		'name' =>  "Custom Background",
		'desc' => "Custom Background",
		'id' => 'author_custom_background',
		'std' => $background_defaults,
		'type' => 'background' );
		
	$options[] = array(
		'name' => "Full Screen Background",
		'desc' => "Click on to Full Screen Background",
		'id' => 'author_full_screen_background',
		'std' => '0',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Styling',
		'type' => 'heading');
	
	$options[] = array(
		'name' => "Home page layout",
		'desc' => "Home page layout .",
		'id' => "home_layout",
		'std' => "full",
		'type' => "images",
		'options' => array(
			'full' => $imagepath.'full.jpg',
			'fixed' => $imagepath.'fixed.jpg',
			'fixed_2' => $imagepath.'fixed_2.jpg'
		)
	);
	
	$options[] = array(
		'name' => "Choose template",
		'desc' => "Choose template layout .",
		'id' => "home_template",
		'std' => "grid_1200",
		'type' => "images",
		'options' => array(
			'grid_1200' => $imagepath.'template_1200.jpg',
			'grid_970' => $imagepath.'template_970.jpg'
		)
	);
	
	$options[] = array(
		'name' => "Site skin",
		'desc' => "Choose Site skin .",
		'id' => "site_skin_l",
		'std' => "site_light",
		'type' => "images",
		'options' => array(
			'site_light' => $imagepath.'light.jpg',
			'site_dark' => $imagepath.'dark.jpg'
		)
	);
	
	$options[] = array(
		'name' => "Choose Your Skin",
		'desc' => "Choose Your Skin",
		'class' => "site_skin",
		'id' => "site_skin",
		'std' => "default",
		'type' => "images",
		'options' => array(
			'default'	=> $imagepath.'default.jpg',
			'bright_cyan'	=> $imagepath.'bright_cyan.jpg',
			'bright_pink'	=> $imagepath.'bright_pink.jpg',
			'dark_grayish_blue'	=> $imagepath.'dark_grayish_blue.jpg',
			'dark_grayish_cyan'	=> $imagepath.'dark_grayish_cyan.jpg',
			'dark_grayish_cyan_2'	=> $imagepath.'dark_grayish_cyan_2.jpg',
			'dark_green'	=> $imagepath.'dark_green.jpg',
			'dark_moderate_violet'	=> $imagepath.'dark_moderate_violet.jpg',
			'dark_pink'	=> $imagepath.'dark_pink.jpg',
			'light_orange'	=> $imagepath.'light_orange.jpg',
			'lime_green'	=> $imagepath.'lime_green.jpg',
			'moderate_blue'	=> $imagepath.'moderate_blue.jpg',
			'moderate_blue_2'	=> $imagepath.'moderate_blue_2.jpg',
			'moderate_cyan'	=> $imagepath.'moderate_cyan.jpg',
			'moderate_violet'	=> $imagepath.'moderate_violet.jpg',
			'slightly_desaturated_blue'	=> $imagepath.'slightly_desaturated_blue.jpg',
			'soft_blue'	=> $imagepath.'soft_blue.jpg',
			'soft_cyan'	=> $imagepath.'soft_cyan.jpg',
			'soft_red'	=> $imagepath.'soft_red.jpg',
			'soft_yellow'	=> $imagepath.'soft_yellow.jpg',
			'strong_cyan'	=> $imagepath.'strong_cyan.jpg',
			'strong_orange'	=> $imagepath.'strong_orange.jpg',
			'strong_red'	=> $imagepath.'strong_red.jpg',
			'very_light_pink'	=> $imagepath.'very_light_pink.jpg',
			'vivid_cyan'	=> $imagepath.'vivid_cyan.jpg',
			'vivid_orange'	=> $imagepath.'vivid_orange.jpg',
			'vivid_orange_2'	=> $imagepath.'vivid_orange_2.jpg',
			'vivid_orange_3'	=> $imagepath.'vivid_orange_3.jpg',
			'vivid_yellow'	=> $imagepath.'vivid_yellow.jpg',
			'vivid_yellow_2'	=> $imagepath.'vivid_yellow_2.jpg',
			'yellow'	=> $imagepath.'yellow.jpg',
			'red'	=> $imagepath.'red.jpg',
			'purple'	=> $imagepath.'purple.jpg',
			'orange'	=> $imagepath.'orange.jpg',
			'light_red'	=> $imagepath.'light_red.jpg',
			'green'	=> $imagepath.'green.jpg',
			'gray'	=> $imagepath.'gray.jpg',
			'blue'	=> $imagepath.'blue.jpg',
		)
	);
	
	$options[] = array(
		'name' => "Primary Color",
		'desc' => "Primary Color",
		'id' => 'primary_color',
		'type' => 'color' );
	
	$options[] = array(
		'name' => "Secondary Color ( it's darkness more than primary color )",
		'desc' => "Secondary Color ( it's darkness more than primary color )",
		'id' => 'secondary_color',
		'type' => 'color' );
	
	$options[] = array(
		'name' => "Background Type",
		'desc' => "Background Type",
		'id' => 'background_type',
		'std' => 'patterns',
		'type' => 'radio',
		'options' => 
			array(
				"patterns" => "Patterns",
				"custom_background" => "Custom Background"
			)
		);

	$options[] = array(
		'name' => "Background Color",
		'desc' => "Background Color",
		'id' => 'background_color',
		'std' => "#FFF",
		'type' => 'color' );
		
	$options[] = array(
		'name' => "Choose Pattern",
		'desc' => "Choose Pattern",
		'id' => "background_pattern",
		'std' => "bg13",
		'type' => "images",
		'options' => array(
			'bg1' => $imagepath.'bg1.jpg',
			'bg2' => $imagepath.'bg2.jpg',
			'bg3' => $imagepath.'bg3.jpg',
			'bg4' => $imagepath.'bg4.jpg',
			'bg5' => $imagepath.'bg5.jpg',
			'bg6' => $imagepath.'bg6.jpg',
			'bg7' => $imagepath.'bg7.jpg',
			'bg8' => $imagepath.'bg8.jpg',
			'bg9' => $imagepath.'../../images/patterns/bg9.png',
			'bg10' => $imagepath.'../../images/patterns/bg10.png',
			'bg11' => $imagepath.'../../images/patterns/bg11.png',
			'bg12' => $imagepath.'../../images/patterns/bg12.png',
			'bg13' => $imagepath.'bg13.jpg',
			'bg14' => $imagepath.'bg14.jpg',
			'bg15' => $imagepath.'../../images/patterns/bg15.png',
			'bg16' => $imagepath.'../../images/patterns/bg16.png',
			'bg17' => $imagepath.'bg17.jpg',
			'bg18' => $imagepath.'bg18.jpg',
			'bg19' => $imagepath.'bg19.jpg',
			'bg20' => $imagepath.'bg20.jpg',
			'bg21' => $imagepath.'../../images/patterns/bg21.png',
			'bg22' => $imagepath.'bg22.jpg',
			'bg23' => $imagepath.'../../images/patterns/bg23.png',
			'bg24' => $imagepath.'../../images/patterns/bg24.png',
	));

	$options[] = array(
		'name' =>  "Custom Background",
		'desc' => "Custom Background",
		'id' => 'custom_background',
		'std' => $background_defaults,
		'type' => 'background' );
		
	$options[] = array(
		'name' => "Full Screen Background",
		'desc' => "Click on to Full Screen Background",
		'id' => 'full_screen_background',
		'std' => '0',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Advertising',
		'type' => 'heading');
	
	$options[] = array(
		'name' => "Advertising after header 1",
		'class' => 'home_page_display',
		'type' => 'info');
	
	$options[] = array(
		'name' => 'Advertising type',
		'desc' => 'Advertising type .',
		'id' => 'header_adv_type_1',
		'std' => 'custom_image',
		'type' => 'radio',
		'options' => array("display_code" => "Display code","custom_image" => "Custom Image"));
	
	$options[] = array(
		'name' => 'Image URL',
		'desc' => 'Upload a image, or enter URL to an image if it is already uploaded. ',
		'id' => 'header_adv_img_1',
		'std' => '',
		'type' => 'upload');
	
	$options[] = array(
		'name' => 'Advertising url',
		'desc' => 'Advertising url. ',
		'id' => 'header_adv_href_1',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => "Advertising Code html ( Ex: Google ads)",
		'desc' => "Advertising Code html ( Ex: Google ads)",
		'id' => 'header_adv_code_1',
		'std' => '',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => "Advertising after header 2",
		'class' => 'home_page_display',
		'type' => 'info');
	
	$options[] = array(
		'name' => 'Advertising type',
		'desc' => 'Advertising type .',
		'id' => 'header_adv_type',
		'std' => 'custom_image',
		'type' => 'radio',
		'options' => array("display_code" => "Display code","custom_image" => "Custom Image"));
	
	$options[] = array(
		'name' => 'Image URL',
		'desc' => 'Upload a image, or enter URL to an image if it is already uploaded. ',
		'id' => 'header_adv_img',
		'std' => '',
		'type' => 'upload');
	
	$options[] = array(
		'name' => 'Advertising url',
		'desc' => 'Advertising url. ',
		'id' => 'header_adv_href',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => "Advertising Code html ( Ex: Google ads)",
		'desc' => "Advertising Code html ( Ex: Google ads)",
		'id' => 'header_adv_code',
		'std' => '',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => "Advertising after share and tags ( in post )",
		'class' => 'home_page_display',
		'type' => 'info');
	
	$options[] = array(
		'name' => 'Advertising type',
		'desc' => 'Advertising type .',
		'id' => 'share_adv_type',
		'std' => 'custom_image',
		'type' => 'radio',
		'options' => array("display_code" => "Display code","custom_image" => "Custom Image"));
	
	$options[] = array(
		'name' => 'Image URL',
		'desc' => 'Upload a image, or enter URL to an image if it is already uploaded. ',
		'id' => 'share_adv_img',
		'std' => '',
		'type' => 'upload');
	
	$options[] = array(
		'name' => 'Advertising url',
		'desc' => 'Advertising url. ',
		'id' => 'share_adv_href',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => "Advertising Code html ( Ex: Google ads)",
		'desc' => "Advertising Code html ( Ex: Google ads)",
		'id' => 'share_adv_code',
		'std' => '',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => "Advertising after content",
		'class' => 'home_page_display',
		'type' => 'info');
	
	$options[] = array(
		'name' => 'Advertising type',
		'desc' => 'Advertising type .',
		'id' => 'content_adv_type',
		'std' => 'custom_image',
		'type' => 'radio',
		'options' => array("display_code" => "Display code","custom_image" => "Custom Image"));
	
	$options[] = array(
		'name' => 'Image URL',
		'desc' => 'Upload a image, or enter URL to an image if it is already uploaded. ',
		'id' => 'content_adv_img',
		'std' => '',
		'type' => 'upload');
	
	$options[] = array(
		'name' => 'Advertising url',
		'desc' => 'Advertising url. ',
		'id' => 'content_adv_href',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => "Advertising Code html ( Ex: Google ads)",
		'desc' => "Advertising Code html ( Ex: Google ads)",
		'id' => 'content_adv_code',
		'std' => '',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => 'Footer settings',
		'type' => 'heading');
	
	$options[] = array(
		'name' => 'Footer top enable or disable',
		'desc' => 'Footer top enable or disable .',
		'id' => 'footer_top',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Subscribe enable or disable',
		'desc' => 'Subscribe enable or disable .',
		'id' => 'subscribe_f',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Feedburner ID',
		'desc' => 'Type here the Feedburner ID .',
		'id' => 'feedburner_id',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'The title of subscribe',
		'desc' => 'Type here the title of subscribe .',
		'id' => 'feedburner_h3',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'The content of subscribe',
		'desc' => 'Type here the content of subscribe .',
		'id' => 'feedburner_p',
		'type' => 'text');
	
	$options[] = array(
		'name' => "Footer Layout ( level 1 )",
		'desc' => "Footer columns Layout ( level 1 ) .",
		'id' => "footer_layout_1",
		'std' => "footer_no",
		'type' => "images",
		'options' => array(
			'footer_1c' => $imagepath . 'footer_1c.jpg',
			'footer_2c' => $imagepath . 'footer_2c.jpg',
			'footer_3c' => $imagepath . 'footer_3c.jpg',
			'footer_4c' => $imagepath . 'footer_4c.jpg',
			'footer_no' => $imagepath . 'footer_no.jpg')
	);
	
	$options[] = array(
		'name' => "Footer Layout ( level 2 )",
		'desc' => "Footer columns Layout ( level 2 ) .",
		'id' => "footer_layout_2",
		'std' => "footer_no",
		'type' => "images",
		'options' => array(
			'footer_1c' => $imagepath . 'footer_1c.jpg',
			'footer_2c' => $imagepath . 'footer_2c.jpg',
			'footer_3c' => $imagepath . 'footer_3c.jpg',
			'footer_4c' => $imagepath . 'footer_4c.jpg',
			'footer_no' => $imagepath . 'footer_no.jpg')
	);
	
	$options[] = array(
		'name' => 'Copyrights',
		'desc' => 'Put the copyrights of footer .',
		'id' => 'footer_copyrights',
		'std' => 'Copyright 2014 Logger | Designed By <a href=http://7oroof.com/ target=_blank>7oroof</a> | Developed By <a href=http://2code.info/ target=_blank>2codeThemes</a>',
		'type' => 'textarea');
	
	$options[] = array(
		'name' => 'Social enable or disable',
		'desc' => 'Social enable or disable .',
		'id' => 'social_icon_f',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'Facebook URL',
		'desc' => 'Type the facebook URL from here .',
		'id' => 'facebook_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Twitter URL',
		'desc' => 'Type the twitter URL from here .',
		'id' => 'twitter_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Google plus URL',
		'desc' => 'Type the google plus URL from here .',
		'id' => 'gplus_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Linkedin URL',
		'desc' => 'Type the linkedin URL from here .',
		'id' => 'linkedin_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Dribbble URL',
		'desc' => 'Type the dribbble URL from here .',
		'id' => 'dribbble_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Youtube URL',
		'desc' => 'Type the youtube URL from here .',
		'id' => 'youtube_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Vimeo URL',
		'desc' => 'Type the vimeo URL from here .',
		'id' => 'vimeo_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Skype',
		'desc' => 'Type the skype from here .',
		'id' => 'skype_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Flickr URL',
		'desc' => 'Type the flickr URL from here .',
		'id' => 'flickr_icon_f',
		'std' => '#',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Rss enable or disable',
		'desc' => 'Rss enable or disable .',
		'id' => 'rss_icon_f',
		'std' => 'on',
		'type' => 'checkbox');
	
	$options[] = array(
		'name' => 'RSS URL if you want change the default URL',
		'desc' => 'Type the RSS URL if you want change the default URL or leave it empty for enable the default URL .',
		'id' => 'rss_icon_f_other',
		'std' => '',
		'type' => 'text');
	
	$options[] = array(
		'name' => "Advanced",
		'id' => "advanced",
		'type' => 'heading');
	
	$options[] = array(
		'name' => 'Twitter consumer key',
		'desc' => 'Twitter consumer key. ',
		'id' => 'twitter_consumer_key',
		'std' => '',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Twitter consumer secret',
		'desc' => 'Twitter consumer secret. ',
		'id' => 'twitter_consumer_secret',
		'std' => '',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Twitter access token',
		'desc' => 'Twitter access token. ',
		'id' => 'twitter_access_token',
		'std' => '',
		'type' => 'text');
	
	$options[] = array(
		'name' => 'Twitter access token secret',
		'desc' => 'Twitter access token secret. ',
		'id' => 'twitter_access_token_secret',
		'std' => '',
		'type' => 'text');
	
	$options[] = array(
		'name' => "If you wont to export setting please refresh the page before that",
		'type' => 'info');

	$options[] = array(
		'name' => "Export Setting",
		'desc' => "Copy this to saved file",
		'id' => 'export_setting',
		'export' => $current_options_e,
		'type' => 'export');

	$options[] = array(
		'name' => "Import Setting",
		'desc' => "Put here the import setting",
		'id' => 'import_setting',
		'type' => 'import');
	
	return $options;
}