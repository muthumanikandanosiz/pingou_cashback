<?php
function Vpanel_Pictures_News($box_title,$box_posts_num,$box_cats,$box_style,$orderby,$key_b,$vbegy_sidebar) {
	global $post;
	$rand_pictures = rand(1,1000);
	if ($box_style == "pictures_1") {
		$implode_c = "";
		if (isset($box_cats) && is_array($box_cats)) {
			foreach ($box_cats as $key => $value) {
				if ($value == "on") {
					$implode_key_c[] = $key;
				}
			}
			if (isset($implode_key_c) && is_array($implode_key_c)) {
				$implode_c = implode(",",$implode_key_c);
			}
		}
		?>
		<div class="block-box block-gallery<?php echo ($vbegy_sidebar == "full"?" block-box-full":"")?>">
			<div class="block-box-title"><?php echo ($box_title != ""?$box_title:"")?></div>
			<div class="block-box-1 block-gallery-1">
				<ul>
					<?php if ($orderby == "popular") {
						$orderby = array('orderby' => 'comment_count');
					}else if ($orderby == "random") {
						$orderby = array('orderby' => 'rand');
					}else {
						$orderby = array();
					}
					query_posts(array_merge($orderby,array('cat' => $implode_c,'ignore_sticky_posts' => 1,'posts_per_page' => $box_posts_num)));
					if ( have_posts() ) :
					$k = 0;
					while ( have_posts() ) : the_post();$k++;
						$vbegy_what_post = rwmb_meta('vbegy_what_post','select',$post->ID);
						$video_type = rwmb_meta('vbegy_video_post_type',"select",$post->ID);
					    if ($k == 1) {?>
							<li class="block-box-first">
								<div class="block-box-img">
									<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark">
										<i class="fa fa-<?php if (is_sticky()) {?>thumb-tack<?php }else if ($vbegy_what_post == "google") {?>map-marker<?php }else if ($vbegy_what_post == "audio") {?>volume-up<?php }else if ($vbegy_what_post == "video") {if ($video_type == 'youtube') {?>youtube-play<?php }else if ($video_type == 'vimeo') {?>vimeo-square<?php }else if ($video_type == 'daily') {?>video-camera<?php }?><?php }else if ($vbegy_what_post == "slideshow") {?>film<?php }else if ($vbegy_what_post == "quote") {?>quote-left<?php }else if ($vbegy_what_post == "link") {?>link<?php }else if ($vbegy_what_post == "soundcloud") {?>soundcloud<?php }else if ($vbegy_what_post == "twitter") {?>twitter<?php }else if ($vbegy_what_post == "facebook") {?>facebook<?php }else {if (has_post_thumbnail()) {?>image<?php }else {?>file-text<?php }}?>"></i>
										<?php if ($vbegy_sidebar == "full") {
											echo get_aq_resize_img('full',346,310);
										}else {
											echo get_aq_resize_img('full',345,310);
										}?>
									</a>
								</div>
							</li>
						<?php }else {
							if (has_post_thumbnail()) {?>
								<li>
									<div class="block-box-img">
										<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark">
											<i class="fa fa-<?php if (is_sticky()) {?>thumb-tack<?php }else if ($vbegy_what_post == "google") {?>map-marker<?php }else if ($vbegy_what_post == "audio") {?>volume-up<?php }else if ($vbegy_what_post == "video") {if ($video_type == 'youtube') {?>youtube-play<?php }else if ($video_type == 'vimeo') {?>vimeo-square<?php }else if ($video_type == 'daily') {?>video-camera<?php }?><?php }else if ($vbegy_what_post == "slideshow") {?>film<?php }else if ($vbegy_what_post == "quote") {?>quote-left<?php }else if ($vbegy_what_post == "link") {?>link<?php }else if ($vbegy_what_post == "soundcloud") {?>soundcloud<?php }else if ($vbegy_what_post == "twitter") {?>twitter<?php }else if ($vbegy_what_post == "facebook") {?>facebook<?php }else {if (has_post_thumbnail()) {?>image<?php }else {?>file-text<?php }}?>"></i>
											<?php echo get_aq_resize_img('full',71,71);?>
										</a>
									</div>
								</li>
							<?php }
						}
					endwhile;endif;?>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div><!-- End block-box -->
		<div class="clearfix"></div>
		<?php
	}else if ($box_style == "pictures_2") {
		$implode_c = "";
		if (isset($box_cats) && is_array($box_cats)) {
			foreach ($box_cats as $key => $value) {
				if ($value == "on") {
					$implode_key_c[] = $key;
				}
			}
			if (isset($implode_key_c) && is_array($implode_key_c)) {
				$implode_c = implode(",",$implode_key_c);
			}
		}
		?>
		<div class="block-box block-gallery<?php echo ($vbegy_sidebar == "full"?" block-box-full":"")?>">
			<div class="block-box-title"><?php echo ($box_title != ""?$box_title:"")?></div>
			<div class="block-box-1 block-gallery-1 block-gallery-2">
				<ul>
					<?php if ($orderby == "popular") {
						$orderby = array('orderby' => 'comment_count');
					}else if ($orderby == "random") {
						$orderby = array('orderby' => 'rand');
					}else {
						$orderby = array();
					}
					query_posts(array_merge($orderby,array('cat' => $implode_c,'ignore_sticky_posts' => 1,'posts_per_page' => $box_posts_num)));
					if ( have_posts() ) :
					while ( have_posts() ) : the_post();
						$vbegy_what_post = rwmb_meta('vbegy_what_post','select',$post->ID);
						$video_type = rwmb_meta('vbegy_video_post_type',"select",$post->ID);
						if (has_post_thumbnail()) {?>
							<li>
								<div class="block-box-img">
									<a href="<?php the_permalink();?>" title="<?php printf('%s', the_title_attribute('echo=0')); ?>" rel="bookmark">
										<i class="fa fa-<?php if (is_sticky()) {?>thumb-tack<?php }else if ($vbegy_what_post == "google") {?>map-marker<?php }else if ($vbegy_what_post == "audio") {?>volume-up<?php }else if ($vbegy_what_post == "video") {if ($video_type == 'youtube') {?>youtube-play<?php }else if ($video_type == 'vimeo') {?>vimeo-square<?php }else if ($video_type == 'daily') {?>video-camera<?php }?><?php }else if ($vbegy_what_post == "slideshow") {?>film<?php }else if ($vbegy_what_post == "quote") {?>quote-left<?php }else if ($vbegy_what_post == "link") {?>link<?php }else if ($vbegy_what_post == "soundcloud") {?>soundcloud<?php }else if ($vbegy_what_post == "twitter") {?>twitter<?php }else if ($vbegy_what_post == "facebook") {?>facebook<?php }else {if (has_post_thumbnail()) {?>image<?php }else {?>file-text<?php }}?>"></i>
										<?php echo get_aq_resize_img('full',70,70);?>
									</a>
								</div>
							</li>
						<?php }
					endwhile;endif;?>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div><!-- End block-box -->
		<div class="clearfix"></div>
		<?php
	}
}
?>