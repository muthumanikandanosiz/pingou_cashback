<?php /* Template Name: Blog */
get_header();
	$vbegy_sidebar_all  = rwmb_meta('vbegy_sidebar','radio',$post->ID);
	$post_style         = rwmb_meta('vbegy_post_style','radio',$post->ID);
	$posts_meta         = rwmb_meta('vbegy_post_meta','checkbox',$post->ID);
	$post_type_option   = rwmb_meta('vbegy_post_type','checkbox',$post->ID);
	$post_author        = rwmb_meta('vbegy_post_author','checkbox',$post->ID);
	$post_excerpt       = rwmb_meta('vbegy_post_excerpt','text',$post->ID);
	$post_excerpt       = (isset($post_excerpt) && $post_excerpt != ""?$post_excerpt:5);
	$post_excerpt_title = rwmb_meta('vbegy_post_excerpt_title','text',$post->ID);
	$post_excerpt_title = (isset($post_excerpt_title) && $post_excerpt_title != ""?$post_excerpt_title:5);
	$post_share         = rwmb_meta('vbegy_post_share','checkbox',$post->ID);
	$post_pagination    = rwmb_meta('vbegy_post_pagination','radio',$post->ID);
	$page_tamplate      = true;
	if ($posts_meta == 1) {
		$posts_meta = "on";
	}
	if ($post_type_option == 1) {
		$post_type_option = "on";
	}
	if ($post_author == 1) {
		$post_author = "on";
	}
	if ($post_share == 1) {
		$post_share = "on";
	}
	global $vbegy_sidebar;
	$vbegy_sidebar = $vbegy_sidebar_all;
	$author_by     = vpanel_options("author_by");
	if ($post_style == "style_2" || $post_style == "style_3") {echo "<div class='row blog-all isotope'>";}
	$paged = (get_query_var("paged") != ""?(int)get_query_var("paged"):(get_query_var("page") != ""?(int)get_query_var("page"):1));
	query_posts(array('paged' => $paged,'post_type' => 'post'));
	get_template_part("loop");
	if ($post_style == "style_2" || $post_style == "style_3") {echo "</div>";}
	get_template_part("includes/pagination");
get_footer();?>