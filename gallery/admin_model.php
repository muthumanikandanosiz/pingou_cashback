<?php
class Admin_model extends CI_Model
{
	// this is to login check 
	function logincheck(){	
		$this->db->connection_check();
		$admin_username = $this->input->post('username');
		$admin_password = $this->input->post('password');
		
		$this->db->where('admin_username',$admin_username);
		$this->db->where('admin_password',$admin_password);
		
		$query = $this->db->get('admin');
		if($query->num_rows==1){
			$fetch = $query->row();
			        $admin_id = $fetch->admin_id;
			        $admin_username = $fetch->admin_username;
			
				 	$main_access=$fetch->main_access;
					$sub_access=$fetch->sub_access;

			$this->session->set_userdata('admin_id',$admin_id);
			$this->session->set_userdata('admin_username',$admin_username);
			
			$this->session->set_userdata('admin_email',$admin_email);
			$this->session->set_userdata('main_access',unserialize($main_access));
			$this->session->set_userdata('sub_access',unserialize($sub_access));
			return true;
		}
		return false;
	}

	
	//New code for user settings details 16-04-16//
	function getusersettings()
	{
		$this->db->connection_check();
		$this->db->where('setting_id','1');
		$query_admin = $this->db->get('tbl_usersettings');
		if($query_admin->num_rows >= 1) 
		{
			$row = $query_admin->row();
			return $query_admin->result();
		}
		else
		{
			return false;		
		}	
	}	
	//End//
	
	// get admin details..
	function getadmindetails(){
		$this->db->connection_check();
	$this->db->where('admin_id','1');
	$query_admin = $this->db->get('admin');
		if($query_admin->num_rows >= 1){
			$row = $query_admin->row();
			return $query_admin->result();
		}
		else
		{
			return false;		
		}	
	}
	function get_admindetails()
	{
		$this->db->connection_check();
		$this->db->where('admin_id','1');
		$query_admin = $this->db->get('admin');
			if($query_admin->num_rows == 1){
				return $query_admin->row();
			}else{
				return false;		
			}	
	}
	function check_cate($cate)
	{
		$this->db->connection_check();
		$this->db->where('category_name',$cate);
		$qry = $this->db->get('categories');
		$numrows1 = $qry->num_rows();
		if($numrows1 == 0)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	function check_sub_cate($sub,$cate)
	{
		$this->db->connection_check();
		$this->db->where('sub_category_name',$sub);
		$this->db->where('cate_id',$cate);
		$qry = $this->db->get('sub_categories');
		$numrows1 = $qry->num_rows();
		if($numrows1 == 0)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	
	// update admin settings..
	function updatesettings($logo,$site_logo,$site_favicon,$backtype,$background_image,$background_color,$covertype,$cover_color,$cover_image,$topcashback_type,$topcashback_image,$topcashback_color,$image_topcashback)
	{
		$this->db->connection_check();
		$admin_logo = $logo;
		$posted = $this->input->post('username');
		$this->session->set_userdata('admin_username',$posted);
		$data = array(
		'admin_username'=>$this->input->post('username'),
		'admin_email'=>$this->input->post('email'),
		'email_notify'=>$this->input->post('email_notify'),
		'admin_paypal'=>$this->input->post('paypal_email'),
		'paypal_mode'=>$this->input->post('paypal_mode'),
		'admin_logo'=>$admin_logo,
		'site_logo'=>$site_logo,
		'background_type'=>$backtype,
		'background_image'=>$background_image,
		'background_color'=>$background_color,
		'storecover_type'  =>$covertype,
		'storecover_image'=>$cover_image,
		'storecover_color'=>$cover_color,
		'site_favicon'=>$site_favicon,
		'homepage_title'=>$this->input->post('homepage_title'),			//'referral_cashback'=>$this->input->post('referral_cashback'),
		'minimum_cashback'=>$this->input->post('minimum_cashback'),
		'site_name'=>$this->input->post('site_name'),
		'site_url'=>$this->input->post('site_url'),
		'admin_fb'=>$this->input->post('fb_url'),
		'admin_twitter'=>$this->input->post('twitter_url'),
		'admin_gplus'=>$this->input->post('gplus_url'),
		
		'admin_instagram'=>$this->input->post('admin_instagram'),
		
		'admin_pintrust'=>$this->input->post('admin_pintrust'),
		'contact_number'=>$this->input->post('contact_number'),
		'contact_info'=>$this->input->post('contact_info'),
		
		'address'=>$this->input->post('address'),
		
		'meta_title'=>$this->input->post('meta_title'),
		'meta_keyword'=>$this->input->post('meta_keyword'),
		'meta_description'=>$this->input->post('meta_description'),
		'site_mode'=>$this->input->post('site_mode'),
		'google_analytics'=>$this->input->post('google_analytics'),
		'google_key'=>$this->input->post('google_key'),
		'google_secret'=>$this->input->post('google_secret'),
		/*facebook App Id & secret key seetha -----*/
		'facebook_key'=>$this->input->post('facebook_key'),
		'facebook_secret'=>$this->input->post('facebook_secret'),
		'yahoo_key'=>$this->input->post('yahoo_key'),
		'yahoo_secret'=>$this->input->post('yahoo_secret'),
		/*-----*/
		'enable_blog'=>$this->input->post('enable_blog'),
		
		'enable_shopping'=>$this->input->post('enable_shopping'),
		
		'enable_slider'=>$this->input->post('enable_slider'),
		'blog_url'=>$this->input->post('blog_url'),
		'unic_bonus'=>$this->input->post('benefit_bonus'),
		'activate_method'=>$this->input->post('activate_method'),
		
		'unlog_status'=>$this->input->post('unlog_status'),
		'unlog_content'=>$this->input->post('unlog_content'),
		'log_status'=>$this->input->post('log_status'),
		'log_content'=>$this->input->post('log_content'),
		'unlog_menu_status'=>$this->input->post('unlog_usr_status'),
		'log_menu_status'=>$this->input->post('log_usr_status'),
		'notify_color'=>$this->input->post('notify_color'),


/*New code for top cashback page background images 26-4-16*/
'topcashback_type' =>$topcashback_type,
'topcashback_image'=>$topcashback_image,
'topcashback_color'=>$topcashback_color,
'image_topcashback'=>$image_topcashback,
/*End*/

/*New code for Category type enable and disable settings 3-5-16*/
'cat_two_status' =>$this->input->post('cat_two_status'),
'cat_three_status'=>$this->input->post('cat_three_status'),
'cat_four_status'=>$this->input->post('cat_four_status'),
'cat_five_status'=>$this->input->post('cat_five_status'),
/*End*/

/* Session time setting  */
'ses_datetime'=>$this->input->post('ses_datetime')

		);
		 //echo "<pre>";print_r($data);
		 //exit;
		
		$id = $this->input->post('admin_id');
		$this->db->where('admin_id',$id);
		$updation = $this->db->update('admin',$data);
		
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}
	
	}
	
	//  change password for main admin
	function update_password()
	{
	
		$this->db->connection_check();
		$old_password = $this->input->post('old_password');
		$new_password = $this->input->post('new_password');
		$id = $this->input->post('admin_id');
		
		$where = array('admin_password'=>$old_password,'admin_id'=>$id);
		$this->db->where($where);
		$query_admin = $this->db->get('admin');
		if($query_admin->num_rows >= 1) 
		{
			$data = array(
			'admin_password'=>$new_password
			);
			$this->db->where('admin_id',$id);	
			$this->db->update('admin',$data);
			return true;
		}    
		else 
		{     
			return false;
		}			
	}
	
	// get referral cashback %
	function get_referral_percent(){
		$this->db->connection_check();
		$result = $this->db->get_where('admin',array('admin_id'=>'1'))->row('referral_cashback');
		return $result;
	
	}
	//adding cms..
	function addcms(){
		$this->db->connection_check();
		$seo_url  = $this->admin_model->seoUrl($this->input->post('page_title'));
		$data = array(
		'cms_heading' => $this->input->post('page_title'),
		'cms_metatitle' => $this->input->post('meta_title'),
		'cms_metakey' => $this->input->post('meta_keyword'),
		'cms_metadesc' => $this->input->post('meta_description'),
		'cms_content' => $this->input->post('cms_content'),
		
		'cms_position' => $this->input->post('cms_position'),	
		
		'cms_title' => $seo_url,	
		'cms_status' => $this->input->post('cms_status')
		);
		
		$this->db->insert('tbl_cms',$data);
		return true;
	}
	// get all cms
	function get_allcms()
	{
		$this->db->connection_check();
		$this->db->order_by('cms_id','desc');
		$cms_query = $this->db->get('tbl_cms');
		if($cms_query->num_rows > 0)
        {
            $row = $cms_query->row();
            return $cms_query->result();
        }
		else
		{
			return false;		
		}
	}
	
	// get particular cms
	function get_cmscontent($id){
		$this->db->connection_check();
		$this->db->where('cms_id',$id);        
        $query = $this->db->get('tbl_cms');
        if($query->num_rows >= 1)
		{
           $row = $query->row();			
            return $query->result();			
        }      
        return false;		
	}
	
	
	//update cms ..
	function updatecms()
	{
		$this->db->connection_check();
	$seo_url  = $this->admin_model->seoUrl($this->input->post('page_title'));
		$data = array(
			'cms_heading' => $this->input->post('page_title'),
			'cms_metatitle' => $this->input->post('meta_title'),
			'cms_metakey' => $this->input->post('meta_keyword'),
			'cms_metadesc' => $this->input->post('meta_description'),
			'cms_content' => $this->input->post('cms_content'),
			
			'cms_position' => $this->input->post('cms_position'),	
			
			'cms_title' => $seo_url,		
			'cms_status' =>  $this->input->post('cms_status')
		);
		$id =  $this->input->post('cms_id');
		$this->db->where('cms_id',$id);
		$upd = $this->db->update('tbl_cms',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}	
	}
	
	
	// delete cms..
	function deletecms($id){
		$this->db->connection_check();
		$this->db->delete('tbl_cms',array('cms_id' => $id));
		return true;
	
	}
	
	
	
	// get all faqs..
	function get_allfaqs(){
		$this->db->connection_check();
		$this->db->order_by('faq_id','desc');
		$allfaqs = $this->db->get('tbl_faq');
		if($allfaqs->num_rows > 0)
        {
            $row = $allfaqs->row();
            return $allfaqs->result();
        }
		else
		{
			return false;
		}
	}
	
	// add new faq..
	function addfaqs(){
		$this->db->connection_check();
		$data = array(
		'faq_qn' => $this->input->post('faq_qn'),
		'faq_ans' => $this->input->post('faq_ans'),
		'status' => '1'
		);
		
		$this->db->insert('tbl_faq',$data);
		return true;
	}
	// get particular faq
	function get_faqcontent($id){
		$this->db->connection_check();
		$this->db->where('faq_id',$id);
        $query = $this->db->get('tbl_faq');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
           return $query->result();
        }
        return false;
	}
	
	// update faq details..
	function updatefaq(){
		$this->db->connection_check();
		$data = array(
			'faq_qn' => $this->input->post('faq_qn'),
			'faq_ans' => $this->input->post('faq_ans'),
			'status' =>  $this->input->post('status')
		);
		$id =  $this->input->post('faq_id');
		$this->db->where('faq_id',$id);
		$upd = $this->db->update('tbl_faq',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}
	}
	
	// delete faq..
	function deletefaq($id)
	{
		$this->db->connection_check();
		$this->db->delete('tbl_faq',array('faq_id' => $id));
		return true;
	}
	
// view all users..
	function get_allusers()
	{
		$this->db->connection_check();
		$this->db->where('admin_status','');
		$this->db->order_by('user_id','desc');
		$user_query = $this->db->get('tbl_users');
		if($user_query->num_rows > 0)
        {
            $row = $user_query->row();
            return $user_query->result();
        }
		else
		{
			return false;
		}
	}
	// view user details
	function view_user($userid)
	{
		$this->db->connection_check();
		$this->db->where('user_id',$userid);        
        $query = $this->db->get('tbl_users');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
            return $query->result();
        }
        return false;
	}	
	// update user status..
	function userupdate()
	{
		$this->db->connection_check();
	$data = array(
		'status' => $this->input->post('status')
	);
	
		$user_id = $this->input->post('user_id');
		$this->db->where('user_id',$user_id);
		$upd = $this->db->update('tbl_users',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}
	}
	
	// delete user details..
	function deleteuser($id)
	{
		$this->db->connection_check();
		$this->db->where('user_id',$id);        
        $query = $this->db->get('tbl_users');
		$email = $query->row('email');
		$data = array(
			'admin_status' => 'deleted',
			'status' =>  '0'
		);
		$this->db->where('user_id',$id);
		$upd = $this->db->update('tbl_users',$data);
		
		$this->db->delete('referrals',array('referral_email' => $email));            
		//$this->db->delete('tbl_users',array('user_id' => $id)); 
		return true;
	}
	
	// get country name
	function get_country($country)
	{
		$this->db->connection_check();	
		$this->db->where('id',$country);
		$res = $this->db->get('countries');
		if($res->num_rows > 0){
			return $res->row('name');
		}
		return false;	
	}	
	// view all categories..
	function premium_categories()
	{	
		$this->db->connection_check();
		$this->db->order_by('sort_order');
		
		 
		 $this->db->where('category_status',1);
		 	
		$result = $this->db->get('premium_categories');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	
	// view all categories..
	function categories(){
		$this->db->connection_check();
		$this->db->order_by('sort_order');
		$result = $this->db->get('categories');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	// add premium  category
	function addpremiumcategory()
	{	
		$this->db->connection_check();
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('premium_categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$max_val = $get->sort_order;
		}
		$maxval = $max_val + 1;
		$seo_url  = $this->admin_model->seoUrl($this->input->post('category_name'));
		$data = array(
		'category_name'=>$this->input->post('category_name'),
		'meta_keyword'=>$this->input->post('meta_keyword'),
		'meta_description'=>$this->input->post('meta_description'),
		'sort_order'=>$maxval,
		'category_status'=>$this->input->post('category_status'),
		'category_url'=>$seo_url
		);
		
		$this->db->insert('premium_categories',$data);
		return true;
	}	
	
	// add new category
	function addcategory(){
		$this->db->connection_check();
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$max_val = $get->sort_order;
		}
		$maxval = $max_val + 1;
		$seo_url  = $this->admin_model->seoUrl($this->input->post('category_name'));
		//echo print_r($_POST); exit;
		$cat_image = $_FILES['categoryimg']['name'];

		if($cat_image!="") 
				{
					$cat_image = remove_space($new_random.$cat_image);
					$config['upload_path'] ='uploads/img';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['file_name']=$cat_image;
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);	
					if($cat_image!="" && (!$this->upload->do_upload('categoryimg')))
					{
						$cat_imageerror = $this->upload->display_errors();
					}
						if(isset($cat_imageerror))        
						{
							$this->session->set_flashdata('error',$cat_imageerror);
							redirect('adminsettings/addcategory','refresh');
						}
				}
				 
		 
		$data = array(
		'category_name'=>$this->input->post('category_name'),
		'meta_keyword'=>$this->input->post('meta_keyword'),
		'meta_description'=>$this->input->post('meta_description'),
		'sort_order'=>$maxval,
		'category_status'=>$this->input->post('category_status'),
		'category_url'=>$seo_url,
		'category_img'=>$cat_image,
		'category_desc'=>$this->input->post('cat_description')
		);
		//echo print_r($data); exit;
		$this->db->insert('categories',$data);
		return true;
	}	
	
	// edit category
	function get_category($category_id){
	$this->db->connection_check();
		$this->db->where('category_id',$category_id);
        $query = $this->db->get('categories');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
           return $query->result();
        }
        return false;	
	}
	
	function get_premium_category($category_id){
		$this->db->connection_check();
		$this->db->where('category_id',$category_id);
        $query = $this->db->get('premium_categories');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
           return $query->result();
        }
        return false;	
	}
	
	//update category
		function update_category()
		{
			$this->db->connection_check();
		$seo_url  = $this->admin_model->seoUrl($this->input->post('category_name'));
			$data = array(
			'category_name'=>$this->input->post('category_name'),
			'meta_keyword'=>$this->input->post('meta_keyword'),
			'meta_description'=>$this->input->post('meta_description'),
			'category_status'=>$this->input->post('category_status'),
			'category_url'=>$seo_url
		);
		$id = $this->input->post('category_id');
		$this->db->where('category_id',$id);
		$upd = $this->db->update('categories',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}
	}
	
	//update premium category
		function update_premium_category()
		{
		$this->db->connection_check();
		$seo_url  = $this->admin_model->seoUrl($this->input->post('category_name'));
			$data = array(
			'category_name'=>$this->input->post('category_name'),
			'meta_keyword'=>$this->input->post('meta_keyword'),
			'meta_description'=>$this->input->post('meta_description'),
			'category_status'=>$this->input->post('category_status'),
			'category_url'=>$seo_url
		);
		$id = $this->input->post('category_id');
		$this->db->where('category_id',$id);
		$upd = $this->db->update('premium_categories',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}
	}
	
	// delete category
	function deletecategory($id)
	{
		$this->db->connection_check();	
		// get order of category which is to be deleted.
		$start_order = $this->db->get_where('categories',array('category_id'=>$id))->row('sort_order');
		
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$end_order = $get->sort_order;
		}
		$this->db->delete('categories',array('category_id' => $id));
		$newval = $start_order;
		for($inc=$start_order; $inc<=$end_order;$inc++){
			$newval = $newval + 1;
			
			$data = array('sort_order'=>$inc);
			$this->db->where('sort_order',$newval);
			$this->db->update('categories',$data);
		}
		return true;
	}
	
	// delete category
	function deletepremiumcategory($id)
	{
		$this->db->connection_check();
	
		// get order of category which is to be deleted.
		$start_order = $this->db->get_where('premium_categories',array('category_id'=>$id))->row('sort_order');
		
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('premium_categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$end_order = $get->sort_order;
		}
		$this->db->delete('premium_categories',array('category_id' => $id));
		$newval = $start_order;
		for($inc=$start_order; $inc<=$end_order;$inc++){
			$newval = $newval + 1;
			
			$data = array('sort_order'=>$inc);
			$this->db->where('sort_order',$newval);
			$this->db->update('premium_categories',$data);
		}
		return true;
	}
	
	// view all affiliates
	function affiliates()
	{
		$this->db->connection_check();
	$this->db->order_by('affiliate_id','desc');
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	// add new affiliates
	// add new affiliates
	function addaffiliate($logo, $ban,$sidebar_image=null,$cover_photo=null,$report_date)
	{
		$this->db->connection_check();
		$stcat = $this->input->post('categorys_list');
		
		
		if($this->input->post('categorys_list'))
		{
			$store_categorys =implode(",",$this->input->post('categorys_list'));
		}
		else
		{
			$store_categorys='';
		}
		$seo_url  = $this->admin_model->seoUrl($this->input->post('affiliate_name'));
		$data = array(
		'affiliate_name'=>$this->input->post('affiliate_name'),
		'affiliate_logo'=>$logo,
		
		/*'site_url' => $this->input->post('site_url'),*/
		'logo_url'=>$this->input->post('logo_url'),
		'affiliate_desc'=>$this->input->post('affiliate_desc'),
		'cashback_percentage'=>$this->input->post('cashback_percentage'),
		'meta_keyword'=>$this->input->post('meta_keyword'),
		'meta_description'=>$this->input->post('meta_description'),
		'featured'=>$this->input->post('featured'),
		'store_of_week'=>$this->input->post('store_of_week'),
		'affiliate_status'=>$this->input->post('affiliate_status'),
		'affiliate_cashback_type'=>$this->input->post('affiliate_cashback_type'),
		'retailer_ban_url'=>$this->input->post('retailer_ban_url'),	
		
		'how_to_get_this_offer'=>$this->input->post('how_to_get_this_offer'),				
		
		'terms_and_conditions'=>$this->input->post('terms_and_conditions'),		
		
		'sidebar_image_url'=>$this->input->post('sidebar_image_url'),
						
		'coupon_image'=>$ban,
		
		'sidebar_image'=>$sidebar_image,
		'affiliate_url'=>$seo_url,
		'store_categorys'=>$store_categorys,
		//Pilaventhiran 03/05/2016 START
		'report_date'=>$report_date,
		'notify_desk'=>$this->input->post('notify_desk'),
		//Pilaventhiran 13/05/2016 START
		'notify_mobile'=>$this->input->post('notify_mobile'),
		//Pilaventhiran 13/05/2016 END
		'old_cashback'=>$this->input->post('old_cashback'),
		'redir_notify'=>$this->input->post('redir_notify'),
		'name_extra_param'=>$this->input->post('name_extra_param'),
		'content_extra_param'=>$this->input->post('content_extra_param'),
		//Pilaventhiran 13/05/2016 START
		'content_extra_param_android'=> $this->input->post('content_extra_param_android'),
		//Pilaventhiran 13/05/2016 END
		'cashback_percent_android'=>$this->input->post('cashback_percent_android'),
		'cashback_content_android'=>$this->input->post('cashback_content_android'),
		'cover_photo'=>$cover_photo,
		/*New related field added 1-6-16.*/
		'related_details' =>$this->input->post('related_details')
		/*End*/
		//Pilaventhiran 03/05/2016 END
		);
		$this->db->insert('affiliates',$data);
		$store_id = $this->db->insert_id();
		$store_category_count	=	count($stcat);
		if($stcat!="")
		{
			foreach($stcat as $maincat)
			{
				$var="size_".$maincat;
				$subcat = $this->input->post($var);
				if($subcat)
				{
					foreach($subcat as $subcategory)
					{
						$data = array(
						'category_id'=>$maincat,
						'sub_category_id'=>$subcategory,
						'store_id'=>$store_id
						);
						$this->db->insert('tbl_store_sub_cate',$data);
					}
				}
			}
		}
		return true;
	}
	
	// view affiliate
	function get_affiliate($id)
	{
	$this->db->connection_check();	
	$this->db->where('affiliate_id',$id);
	$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->result();		
		}	
	}
	
	
	// update affiliate
	function updateaffiliate($logo,$banimg,$sidebar_image,$cover_photo){
		$this->db->connection_check();
		$affiliate_id = $this->input->post('affiliate_id');
		
		if($this->input->post('categorys_list'))
		{	
		$stcat = $this->input->post('categorys_list');	
			$store_categorys =implode(",",$this->input->post('categorys_list'));
		}
		else
		{
			$stcat = '';	
			$store_categorys='';
		}
		$seo_url  = $this->admin_model->seoUrl($this->input->post('affiliate_name'));
		
		$this->db->delete('tbl_store_sub_cate',array('store_id' => $affiliate_id));
		
		$data = array(
			'affiliate_name'=>$this->input->post('affiliate_name'),
			'affiliate_logo'=>$logo,
			'logo_url'=>$this->input->post('logo_url'),
			
			//'site_url' => $this->input->post('site_url'),
			'meta_keyword'=>$this->input->post('meta_keyword'),
			'meta_description'=>$this->input->post('meta_description'),
			'affiliate_desc'=>$this->input->post('affiliate_desc'),
			'cashback_percentage'=>$this->input->post('cashback_percentage'),
			'featured'=>$this->input->post('featured'),
			'store_of_week'=>$this->input->post('store_of_week'),
			'affiliate_status'=>$this->input->post('affiliate_status'),
			'affiliate_cashback_type'=>$this->input->post('affiliate_cashback_type'),
			
			'retailer_ban_url'=>$this->input->post('retailer_ban_url'),
			'how_to_get_this_offer'=>$this->input->post('how_to_get_this_offer'),				
			
			'terms_and_conditions'=>$this->input->post('terms_and_conditions'),		
			
			'sidebar_image_url'=>$this->input->post('sidebar_image_url'),		
			
			'affiliate_url'=>$seo_url,
			'coupon_image'=>$banimg,
			
			'sidebar_image'=>$sidebar_image,
			'store_categorys'=>$store_categorys,
			//Pilaventhiran 03/05/2016 START
			'notify_desk'=>$this->input->post('notify_desk'),
			//Pilaventhiran 13/05/2016 START
			'notify_mobile'=>$this->input->post('notify_mobile'),
			//Pilaventhiran 13/05/2016 END
			'old_cashback'=>$this->input->post('old_cashback'),
			'redir_notify'=>$this->input->post('redir_notify'),
			'name_extra_param'=>$this->input->post('name_extra_param'),
			'content_extra_param'=>$this->input->post('content_extra_param'),
			'content_extra_param_android'=>$this->input->post('content_extra_param_android'),
			'cashback_percent_android'=>$this->input->post('cashback_percent_android'),
			'cashback_content_android'=>$this->input->post('cashback_content_android'),
			'cover_photo'=>$cover_photo,
			'report_date'=>$this->input->post('report_date'),
			/*new related details 1-6-16.*/
			'related_details' =>$this->input->post('related_details'),
			/*End*/
			//Pilaventhiran 03/05/2016 END	
		);
		
		$this->db->where('affiliate_id',$affiliate_id);
		$updation = $this->db->update('affiliates',$data);
		$store_id= $affiliate_id;
		if($updation!="" )
		{
			if($stcat!='')
			{
				foreach($stcat as $maincat)
				{
					$var="size_".$maincat;
					$subcat = $this->input->post($var);
					if($subcat)
					{
						foreach($subcat as $subcategory)
						{
							$data = array(
							'category_id'=>$maincat,
							'sub_category_id'=>$subcategory,
							'store_id'=>$store_id
							);
							$this->db->insert('tbl_store_sub_cate',$data);
						}
					}
				}
			}
		
			return true;
		}
		else 
		{ 
			return false;   
		}
	//	return true;
	}
	
	// delete affiliate
	function deleteaffiliate($id){
	$this->db->connection_check();
		$this->db->delete('affiliates',array('affiliate_id' => $id));
		$this->db->delete('tbl_store_sub_cate',array('store_id' => $id));
		
		$this->db->delete('click_history',array('affiliate_id' => $id));
		
		return true;	
	}
	
	//view all banners
	function banners(){
	$this->db->connection_check();
		$banners = $this->db->get('tbl_banners');
		if($banners->num_rows > 0){
			return $banners->result();		
		}
		return false;
	}
	
	// add banner
	function addbanner($img){
	$this->db->connection_check();
		$data = array(
			'banner_heading'=>$this->input->post('banner_name'),
			'banner_image'=>$img,
			'banner_url'=>$this->input->post('banner_url'),
			'banner_position'=>$this->input->post('banner_position'),
			'banner_status'=>$this->input->post('banner_status')
		);
	
		$this->db->insert('tbl_banners',$data);
		return true;
	}
	
	//edit banner
	function get_banner($id){
	$this->db->connection_check();
		$this->db->where('banner_id',$id);
		$banner = $this->db->get('tbl_banners');
		if($banner->num_rows > 0){
			return $banner->result();
		}
		return false;
	}
	
	// update banner
	function updatebanner($img){
	$this->db->connection_check();
	$banner_id = $this->input->post('banner_id');
	$data = array(
		'banner_heading'=>$this->input->post('banner_name'),
		'banner_image'=>$img,
		'banner_url'=>$this->input->post('banner_url'),
		'banner_position'=>$this->input->post('banner_position'),
		'banner_status'=>$this->input->post('banner_status')		
	);
	
	$this->db->where('banner_id',$banner_id);
	$update = $this->db->update('tbl_banners',$data);
		if($update!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}
	}
	
	// delete banner
	function deletebanner($delete){ 
		$this->db->connection_check();
		$this->db->delete('tbl_banners',array('banner_id' => $delete));
		return true;
	}
	
	// view all subscribers
	function subscribers(){	
		$this->db->connection_check();
		$this->db->order_by("subscriber_id", "desc");
		$all = $this->db->get('subscribers');
		if($all->num_rows > 0) {
			return $all->result();
		}
		return false;
	}
	// delete subscriber
	function deletesubscriber($id){
		$this->db->connection_check();
		$this->db->delete('subscribers',array('subscriber_id' => $id));
		return true;
	}
	
	
	function send_mail(){
	$this->db->connection_check();
	// get admin email
		$admin_email = $this->db->get_where('admin', array(
			'admin_id'=>'1'
		))->row('admin_email');
		//echo $admin_email; echo "<br>";
		$to_users = $this->input->post('to');
		
		if($to_users=="users")
		{
			$this->db->where('newsletter_mail',1);
			$users = $this->db->get('tbl_users');
			$results = $users->result();
			$emails='';
			foreach($results as $get){
				$emails .= $get->email.',';
			}
			$emails  = rtrim($emails,',');
			$semail  = explode(",", $emails);
 			foreach($semail as $newemail)
			{
				$user_id = $this->db->get_where('tbl_users',array('email'=>$newemail))->row('user_id');		
				 
				$message = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">
					<br>
					<br>'.$this->input->post('message').'
					<br>
					<br>
					Você está recebendo esta beacuse você é um membro Pingou . Você pode apenas <a href='.base_url().'cashback/un_subscribe/newsletter/'.$user_id.'>un-subscrever</a> se você quiser.	
					</span>';
				
				/*New code for email unsubscribe settings 26-5-16.*/

				$this->load->library('email');
				$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8'
				);  	 

				$this->email->set_newline("\r\n");
				$this->email->initialize($config);
				$this->email->from($admin_email);
				$this->email->to($newemail);
				//$this->email->bcc($emails);
				$this->email->subject($this->input->post('subject'));
				$this->email->message($message);
				$this->email->send();
        		$this->email->print_debugger();
			} 				
			 
		}
		else if($to_users=="subscribers")
		{
			$this->db->where('subscriber_status',1);
			$subscribers = $this->db->get('subscribers');
			$results = $subscribers->result();
			$emails='';
			foreach($results as $get){
				$emails .= $get->subscriber_email.',';
			}
			$emails = rtrim($emails,',');
			$semail  = explode(",", $emails);
			foreach($semail as $newemail)
			{
				
				$user_id = $this->db->get_where('subscribers',array('subscriber_email'=>$newemail))->row('subscriber_id');		
				$message = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">
					<br>
					<br>'.$this->input->post('message').'
					<br>
					<br>
					Você está recebendo esta beacuse você é um membro Pingou . Você pode apenas <a href='.base_url().'cashback/un_subscribe/subscribers/'.$user_id.'>un-subscrever</a> se você quiser.
					</span>';
			
				/*New code for email unsubscribe settings 26-5-16.*/

				$this->load->library('email');
				$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8'
				);  	 

				$this->email->set_newline("\r\n");
				$this->email->initialize($config);
				$this->email->from($admin_email);
				$this->email->to($newemail);
			//	$this->email->bcc($emails);
				$this->email->subject($this->input->post('subject'));
				$this->email->message($message);

				$this->email->send();
        		$this->email->print_debugger();
			

			} 
			
		}
		
			//echo $emails; exit;
		/*
		$config['protocal']  = 'smtp';
		$config['smtp_host'] = 'ssl://smtp.googlemail.com';
		$config['smtp_port'] = '465';
		$config['smtp_user'] = 'padmanathan@osiztechnologies.com';
		$config['smtp_pass'] = '';
		$config['mailtype']  = 'html';		
		$config['charset']   = 'iso-8859-1';
		$config['wordwrap']  = TRUE;
		$config['crlf'] = '\r\n';      //should be "\r\n"
		$config['newline'] = '\r\n';   //should be "\r\n"
		*/

		/*$this->load->library('email');

			  $config = Array(

				'mailtype'  => 'html',
				'charset'   => 'utf-8'
			);  	 

		$this->email->set_newline("\r\n");
		$this->email->initialize($config);
		$this->email->from($admin_email);
		$this->email->to("");
		$this->email->bcc($emails);
		$this->email->subject($this->input->post('subject'));
		$this->email->message($message);*/
		/*
		if($this->email->send())
        { 
           return true;
        }
		else{
            show_error($this->email->print_debugger());
			 return false;
        }
        */
        return true;
	}
	
	//get email template content..
	function get_email_template($id){
		$this->db->connection_check();
		$this->db->where('mail_id',$id);
		$mail_template = $this->db->get('tbl_mailtemplates');
		if($mail_template->num_rows > 0){
			return $mail_template->result();
		}
		return false;	
	}
	
	// update template
	function update_email_template(){
		$this->db->connection_check();
		$mail_id = $this->input->post('mail_id');
			
		$data = array(
			'email_subject'=>$this->input->post('email_subject'),
			'email_template'=>$this->input->post('email_template')			
		);
		
		$this->db->where('mail_id',$mail_id);
		$update = $this->db->update('tbl_mailtemplates',$data);
		if($update!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}	
	}
	
	// all referrals
	function referrals(){
		$this->db->connection_check();
		$this->db->group_by('user_id');
		$this->db->order_by("user_id", "desc");
		$referrals = $this->db->get('referrals');
		if($referrals->num_rows > 0)
        {
           return $referrals->result();
        }
		else
		{
			return false;
		}
	}
	
	// total count for referral
	function referral_count($email){
		$this->db->connection_check();
		$this->db->where('user_email',$email);
		$count = $this->db->get('referrals');
		return $count->num_rows();
	}	
	
	// delete referral by user id
	function deletereferral($user_id){
		$this->db->connection_check();
		$this->db->delete('referrals',array('user_id'=>$user_id));
		return true;
	}
	
	// view all coupons..
	function coupons($store_name=null){
		$this->db->connection_check();
		$this->db->order_by("coupon_id", "desc");
		if($store_name)
		{
			$this->db->like('offer_name', $store_name);	
		}
		$result = $this->db->get('coupons');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	//bulk coupon action..
function bulkcoupon($bulkcoupon){
	  	$this->db->connection_check();
	    // delete all the previously added coupons of the coupon type (vcommission/icubewire)
		// $coupon_type = $this->input->post('coupon_type');
		$coupon_type = '';
		//$this->db->delete('coupons',array('coupon_type'=>$coupon_type));
		//$this->db->truncate('coupons');
		/*$this->db->delete('coupons');*/
		$this->load->library('CSVReader');
		$main_url = 'uploads/coupon/'.$bulkcoupon;
	 	$result =   $this->csvreader->parse_file($main_url);
		if(count($result)!=0)
		{
			
			foreach($result as $res)
			{
				
				$strdate = date('m/d/Y',strtotime($res['start_date(m/d/Year)']));
				
				$exp = date('m/d/Y',strtotime($res['Expiry']));
				$offer_url = $res['Offer Page  ( Ctrl C to copy )'];
				$Offer_Name = $this->db->escape_str($res['Offer Name']);
				$Title = $this->db->escape_str($res['Title']);
				$Description = $this->db->escape_str($res['Description']);
				$Type = $res['Type'];
				$Code = $res['Code'];
				$start_date = $strdate;
				$Expiry = $exp;
				
				$Coupon_featured = $res['Coupon_featured'];
				/*$Featured = $res['Featured ( 0 or 1)'];
				$Exclusive = $res['Exclusive (0 or 1)'];*/
				$traking_param = $res['Tracking Extra parameter'];	
				$results = $this->db->query("INSERT INTO `coupons` (`offer_name`, `title`, `description`, `type`, `code`, `offer_page`, `start_date`, `expiry_date`, `coupon_options`, `Tracking`) VALUES ('$Offer_Name', '$Title', '$Description', '$Type', '$Code', '$offer_url', '$start_date', '$Expiry', '$Coupon_featured', '$traking_param');");
			}
		}
		
		/*print_r($result);
		exit;*/
		/*
		$this->load->library('excel_reader');
		$excel = new Excel_reader;
		$main_url = 'uploads/coupon/'.$bulkcoupon;
		$excel->read($main_url);
		$curr_date= date('Y-m-d');
		//echo $curr_date;
		$x=2;
  	    $new_var = '';
		while($x <= $excel->sheets[0]['numRows']) {
		   $y = 1;
		   while($y <= $excel->sheets[0]['numCols']) {
				$cell = isset($excel->sheets[0]['cells'][$x][$y]) ? $excel->sheets[0]['cells'][$x][$y] : '';
				if($y==3){
					$con = str_replace(',', ' ', $cell);
					$a = explode(' ',$con);
					$cell2 = $a[0];
					$new_var .= "'".$cell2."',";
				} else {
				  $new_var .= "'".$cell."',";
				}
				//$new_var .= "'".$cell."',";
				$y++;
		   }
		  echo  $new_vars = 'NULL,'.$new_var."'".$coupon_type."','0','0','".$curr_date."'";
		  exit;
		   	mysql_query('INSERT INTO coupons VALUES('.$new_vars.')');
			//echo 'INSERT INTO coupons VALUES('.$new_vars.')';
			$new_var = '';
		   $x++;
		}*/
    //exit;
		return true;
	}
	
/* 	function sheetData($sheet){
		$x=2;
		while($x <= $sheet['numRows']) {
		   $y = 1;
		   $new_var = '';
		   while($y <= $sheet['numCols']) {
				$cell = isset($sheet['cells'][$x][$y]) ? $sheet['cells'][$x][$y] : '';
				$new_var .= "'".$cell."',";
				$y++;
		   }
		   $insert_query = 'NULL,'.$new_var.'VCommission';
		   $insert_query.="<br>";
		   $insert_query.="--------------------------------------------------------";
		   $insert_query.="<br>";
		   $x++;
		}
		return $insert_query;
	} */
	
	// add new coupon..
	function addcoupon(){
		$this->db->connection_check();
		$start_date = $this->input->post('start_date');
		$start_date = date('Y-m-d',strtotime($start_date));
		$expiry_date =$this->input->post('expiry_date');
		$expiry_date = date('Y-m-d',strtotime($expiry_date));
		
				
		$data = array(
			'offer_name'=>$this->input->post('offer_name'),
			'title'=>$this->input->post('title'),
			'description'=>$this->input->post('description'),
			'type'=>$this->input->post('type'),
			'code'=>$this->input->post('code'),
			'offer_page'=>$this->input->post('offer_page'),
			'expiry_date'=>$expiry_date,
			'start_date'=>$start_date,
			'featured'=>$this->input->post('featured'),
			'exclusive'=>$this->input->post('exclusive'),
			'Tracking'=>$this->input->post('Tracking'),
			
			'coupon_options'=>$this->input->post('coupon_options'),
			'cashback_description'=>$this->input->post('cashback_description')
			
			
			
		);
		$this->db->insert('coupons',$data);
		return true;	
	}
	
	// view coupon..	
	function editcoupon($coupon_id){
	$this->db->connection_check();
	$this->db->where('coupon_id',$coupon_id);
		$coupons = $this->db->get('coupons');
		if($coupons->num_rows > 0){
			return $coupons->result();
		}
		return false;
	}
	
	// update coupon details..
	function updatecoupon() {
		$this->db->connection_check();
			$start_date = $this->input->post('start_date');
		
		$expiry_date = date('Y-m-d',strtotime($this->input->post('expiry_date')));
		$coupon_id = $this->input->post('coupon_id');
		$data = array(
			'offer_name'=>$this->input->post('offer_name'),
			'title'=>$this->input->post('title'),
			'description'=>$this->input->post('description'),
			'type'=>$this->input->post('type'),
			'code'=>$this->input->post('code'),
			'offer_page'=>$this->input->post('offer_page'),
			'expiry_date'=>$expiry_date,
			'start_date'=>$start_date,
			'featured'=>$this->input->post('featured'),
			'exclusive'=>$this->input->post('exclusive'),
			'Tracking'=>$this->input->post('Tracking'),
			
			'coupon_options'=>$this->input->post('coupon_options'),
			
			'cashback_description'=>$this->input->post('cashback_description')
		);
		$this->db->where('coupon_id',$coupon_id);
		$updation = $this->db->update('coupons',$data);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;
		}
	}		
	
	// delete coupon..
	function deletecoupon($delete_id){
		$this->db->connection_check();
		$this->db->delete('coupons',array('coupon_id'=>$delete_id));
		return true;	
	}	
	
	// view all shopping coupons..
	function shoppingcoupons(){
		$this->db->connection_check();
	//expiry_date >='".date('Y-m-d')."' 
	 $selqry="SELECT * FROM shopping_coupons  WHERE expiry_date >='".date('Y-m-d')."' order by shoppingcoupon_id desc";   
	 
	//$result = $this->db->get('shopping_coupons');
	
	 $result=$this->db->query("$selqry"); 
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	function exp_shoppingcoupons(){
	$this->db->connection_check();
	//expiry_date >='".date('Y-m-d')."' 
	 $selqry="SELECT * FROM shopping_coupons  WHERE expiry_date <='".date('Y-m-d')."' order by shoppingcoupon_id desc";   	 
	//$result = $this->db->get('shopping_coupons');
	
	 $result=$this->db->query("$selqry"); 
	//$result = $this->db->get('shopping_coupons');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	// add new shopping coupon..
	
	// edit shopping coupon
	function edit_shoppingcoupon($coupon_id){
	$this->db->connection_check();
	$this->db->where('shoppingcoupon_id',$coupon_id);
		$coupons = $this->db->get('shopping_coupons');
		if($coupons->num_rows > 0){
			return $coupons->result();
		}
		return false;	
	}
	
	// update shopping coupon details..	
	
	// delete shopping coupon..
	function delete_shoppingcoupon($delete_id){
	$this->db->connection_check();
		$this->db->delete('shopping_coupons',array('shoppingcoupon_id'=>$delete_id));
		return true;
	}	
	// view user email	
	function user_email($user_id){
		$this->db->connection_check();
		$result = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('email');
		return $result;	
	}
	
	// view store name
	function view_store($affiliate_id){
		$this->db->connection_check();
		$result = $this->db->get_where('affiliates',array('affiliate_id'=>$affiliate_id))->row('affiliate_name');
		return $result;
	}
	
	// get promo id
	function view_promo($coupon_id){
		$this->db->connection_check();
		$result = $this->db->get_where('coupons',array('coupon_id'=>$coupon_id))->row('promo_id');
		return $result;
	}
	
	// get cashback % from store..
	function get_fromstore($affiliate_id){
		$this->db->connection_check();
		$result = $this->db->get_where('affiliates',array('affiliate_id'=>$affiliate_id))->row('cashback_percentage');
		return $result;	
	}
	function get_fromstore1($affiliate_id){
		$this->db->connection_check();
		$result = $this->db->get_where('affiliates',array('affiliate_url'=>$affiliate_id))->row('cashback_percentage');
		return $result;	
	}
	// view all cashback details
	function cashback(){
		$this->db->connection_check();
		$this->db->order_by('date_added','desc');
		$result = $this->db->get('cashback');
		if($result->num_rows > 0){
	
			return $result->result();
		}
		return false;
	}
	
	// get cashback details
	function cashback_details($id){
		$this->db->connection_check();
		$this->db->where('cashback_id',$id);
		$result = $this->db->get('cashback');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	// update cashback status..
	function updatecashback($id){
		$this->db->connection_check();
		//$trans_id = $this->input->post('trans_id');
		if($this->input->post('transation_status') != 'Approved' || $this->input->post('transation_status') != 'Completed'){//echo "fjg";exit;
		if($this->input->post('current_status') == 'Approved'){
		$user_bale = $this->view_balance($this->input->post('user_id'));
		$newbalnce= $user_bale+$this->input->post('transation_amount');
		$data = array(
		'balance' => $newbalnce);
		$this->db->where('user_id',$this->input->post('user_id'));
		$update_qry = $this->db->update('tbl_users',$data);
		}
	}
		if($this->input->post('transation_status') == 'Approved' || $this->input->post('transation_status') == 'Paid'){
		if($this->input->post('current_status') == 'Canceled' || $this->input->post('current_status') == 'Pending'){
		$user_bale = $this->view_balance($this->input->post('user_id'));
		$newbalnce= $user_bale-$this->input->post('transation_amount');
		$data = array(		
		'balance' => $newbalnce);
		$this->db->where('user_id',$this->input->post('user_id'));
		$update_qry = $this->db->update('tbl_users',$data);
		}
	}
		$current_status = $this->input->post('current_status');
			$data = array(
				'status'=>$current_status
			);
		$this->db->where('cashback_id',$id);
		$updation = $this->db->update('cashback',$data);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;
		}
	}
	
	// delete cashback details..
	function deletecashback($id){

		$this->db->connection_check();
		$cb_r  = $this->db->get_where('cashback',array('cashback_id'=>$id))->row();   
		 
		if($cb_r)
		{
            $txn_id  = $cb_r->txn_id;
        	$amount  = $cb_r->cashback_amount;
            $user_id = $cb_r->user_id;
        }
		if($cb_r->status == "Completed")
		{
		 	//echo "hai"; exit;
		 	/*New code for update a user balance details 27-5-16.*/
			$total_amt 	   = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('balance');
			$remain_amount = ($total_amt - $amount);  
			
			$data = array(		
				'balance' => $remain_amount);
				
			$this->db->where('user_id',$user_id);
			$update_qry = $this->db->update('tbl_users',$data);
			/*End 27-5-16.*/

		 }	
		

		$this->db->delete('cashback',array('cashback_id'=>$id));
		$this->db->delete('transation_details',array('trans_id'=>$txn_id));
		return true;
	}

	//New code for delete multiple cashback records 6/4/16//

	function delete_multi_cashbacks()
	{
		 
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		foreach($sort_order as $key=>$val)
		{
			$id = $key;		
			$this->db->delete('cashback',array('cashback_id' => $id));			
		}
		return true;
	}
	//End//

	 
	// view balance..
	function view_balance($user_id){
		$this->db->connection_check();
		$balace = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('balance');
		return $balace;
	}
	// withdraw ..
	function withdraw(){
		$this->db->connection_check();
	$this->db->order_by('withdraw_id','desc');
		$result = $this->db->get('withdraw');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;	
	}

	//New code for Process status for multiple Withdraw records 6/4/16//

	function process_multi_withdraw()
	{
		 
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		foreach($sort_order as $key=>$val)
		{
			$id = $key;	
			
			$data = array(		
			'status' => 'Processing');
			$this->db->where('withdraw_id',$id);
			$update_qry = $this->db->update('withdraw',$data);				
		}
		return true;
	}
	//End//

	//New code for Completed status for withdraw records 6/4/16//

	function complete_multi_withdraw()
	{
		 
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		foreach($sort_order as $key=>$val)
		{
			$id = $key;	
            $date = date('Y-m-d'); //SATz
			$data = array(		
			'status' => 'Completed',
			'closing_date'=>$date
			);                       //SATz
			$this->db->where('withdraw_id',$id);
			$update_qry = $this->db->update('withdraw',$data);		
		}
		return true;
	}
	//End//

	//New code for Cancel status for multiple Withdraw records 6/4/16//


function cancel_multi_withdraw()
	{
		 
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		
//SATz 29 04 2016 --> 07 05 2016
	
 foreach($sort_order as $key=>$val)
		{
			$id = $key;   
			$user_id=$val;

            $withdraw_id_amount=$this->get_requested_amount($id);     //Get withdraw or added amount
            $user_id_amount=$this->get_user_amount($user_id);         //Get Original amount
            $cancell_update_amount=$user_id_amount + $withdraw_id_amount ;
            $data=array('balance' => $cancell_update_amount);
            $this->db->where('user_id',$user_id);
		    $updation = $this->db->update('tbl_users',$data);        //Tbl_user update


            $date = date('Y-m-d');
			$data = array(		
			'closing_date'=>$date,
			'status' => 'Cancelled'

			);
		  //print_r($id);
			$this->db->where('withdraw_id',$id);
			$update_qry = $this->db->update('withdraw',$data);			//withdraw status update
		}
/*
        foreach($sort_order as $key=>$val)
		{
			$id = $key;		
			$data = array(		
			'status' => 'Cancelled');
			$this->db->where('withdraw_id',$id);
			$update_qry = $this->db->update('withdraw',$data);			
		}
*/

//SATz //

		return true;
	}
	//End//


	
	// view withdraw details..
	function editwithdraw($id){
		$this->db->connection_check();
		$this->db->where('withdraw_id',$id);
		$result = $this->db->get('withdraw');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}	

//SATz 28 04 2016 
	function get_requested_amount($id){
		$this->db->connection_check();
		$this->db->where('withdraw_id',$id);
		$result = $this->db->get('withdraw');
	    return $result->row("requested_amount");
	}

function get_user_amount($user_id){
	    $id=$user_id;
		$this->db->connection_check();
		$this->db->where('user_id',$id);
		$result = $this->db->get('tbl_users');
	    return $result->row("balance");
	}
	
	// update withdraw status..
	function updatewithdraw($id){
		$this->db->connection_check();
	        $current_status = $this->input->post('current_status');
       	        if($current_status == 'Cancelled'){
                    $user_id=$this->input->post('user_id');
                    $user_bal=$this->get_user_amount($user_id);
		            $requested_amount=$this->get_requested_amount($id); //Fetch from withdraw requested_amount
                    $update_bal=$user_bal+$requested_amount;
                  $data=array(
                  	'balance' => $update_bal
                  	      	 );
                  //print_r($data);exit;
				$this->db->where('user_id',$user_id);
				$updation = $this->db->update('tbl_users',$data);

		    }

//SATz 28 04 2016 
		
		//Pilaventhiran 04/05/2016 START
		// Processing , Cancelled, Completed
           
		    $name = $this->db->query("select * from admin")->row();
				$subject = "Your Withdraw Ticket Reply";
				$admin_emailid = $name->admin_email;
				$site_logo = $name->site_logo;
				$site_name  = $name->site_name;
				$contact_number = $name->contact_number;
		$DADOS_BANCARIOS = "<a href='".base_url().">DADOS_BANCARIOS</a>";
		switch($current_status)
		{
			case 'Processing':
				
			$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Só para avisar que já estamos processando o seu resgate. Já já vai pingar dinheiro na sua conta, mas a gente avisa por email assim que o pagamento for realizado.</span>';
				

			break;
			case 'Cancelled':
			
			$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Ôpa, tentamos fazer o seu pagamento mas o banco não aceitou a operação porquê os dados estavam errados. O valor do seu resgate foi creditado novamente na sua conta. Pedimos que atualize os '.$DADOS_BANCARIOS.'  e qualquer problema entre em contato com a gente no '.$contact_number.'.</span>';
			break;
			case 'Completed':
			
				$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> Seu resgate foi realizado com sucesso! Acabou de pingar R$ '.str_replace('.', ',', $requested_amount).' na sua conta bancária. ”
where '.$requested_amount.' is the amount of the withdraw realized</span>';
			break;
			
			
		}

		$date = date('Y-m-d');
		if($current_status=='Completed'){
			$data = array(
				'closing_date'=>$date,
				'status'=>$current_status
			);			
		}
		else if($current_status=='Cancelled'){  //SATz

                $data = array(
				'closing_date'=>$date,
				'status'=>$current_status
			    );	
		}                                          //SATz
		else {
				$data = array(
				'status'=>$current_status
			);
		}
		$this->db->where('withdraw_id',$id);
		$updation = $this->db->update('withdraw',$data);

		if($updation)
		{
			if($current_status!='Requested')
			{

				$mail_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='15'")->row();
				$fe_cont = $mail_temp->email_template;
				
				$servername = base_url();
				$nows = date('Y-m-d');	
				$this->load->library('email');
		
				$gd_api=array(
							'###ADMINNO###'=>$contact_number,
							'###EMAIL###'=>$username,
							'###DATE###'=>$nows,
							'###MESSAGE###'=>$current_msg,
							'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
							'###SITENAME###' =>$site_name,
							'###WITHDRAW_STATUS###'=>$current_status
							);
						   
				$gd_message=strtr($fe_cont,$gd_api);
				//echo $gd_message;
				/*
				$config['protocol'] = 'sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				*/
				/*
				$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' => 'vivek.developer@osiztechnologies.com',
				'smtp_pass' => 'iamnotlosero',
				'mailtype'  => 'html', 
				'charset'   => 'iso-8859-1'
				);
				*/
				$config = Array(
				 'mailtype'  => 'html',
				  'charset'   => 'utf-8',
				  );
$us_email = $User_details->email;
				$list = array($us_email);
				
				$this->email->initialize($config);
				$this->email->set_newline("\r\n");
				$this->email->from($admin_emailid,$site_name.'!');
				$this->email->to($list);
				$this->email->subject($subject);
				$this->email->message($gd_message);
				$this->email->send();
				$this->email->print_debugger();
			}
			return true;
		}
		else
		{
			return false;
		}

		//Pilaventhiran 04/05/2016 END
		
	}
	
	// deleting withdraw..
	function deletewithdraw($id){
		$this->db->connection_check();
		$this->db->delete('withdraw',array('withdraw_id'=>$id));
		return true;
	}
	
	// to check the user is referral..
	function check_refer($email){
		$this->db->connection_check();
		$this->db->where('referral_email',$email);
		$refer = $this->db->get('referrals');
		// print_r($refer->num_rows());
		return $refer->num_rows();
	}
	
	// to fetch the code and amount using shoppingcoupon_id..
	function get_allcoupons($id){
		$this->db->connection_check();
		$this->db->where('shoppingcoupon_id',$id);
		$results = $this->db->get('shoppingcodes');
		if($results->num_rows > 0){
			return $results->result();
		}
		return false;
	}
	
	//  removes code on editing premium coupon..
	function delete_shopcoupon($ids){
	$this->db->connection_check();
		$this->db->delete('shoppingcodes',array('shoppingcode_id'=>$ids));
		return true;
	}
	
	// change  category order..
	function change_cate_order($old_order,$new_order){
		$this->db->connection_check();
		// fetching category id using sort_order id..
		$old_category = $this->db->get_where('categories',array('sort_order'=>$old_order))->row('category_id');
		$new_category = $this->db->get_where('categories',array('sort_order'=>$new_order))->row('category_id');
		
		$data1 = array('sort_order'=>$new_order);
		$this->db->where('category_id',$old_category);
		$this->db->update('categories',$data1);
		
		$data2 = array('sort_order'=>$old_order);
		$this->db->where('category_id',$new_category);
		$updation = $this->db->update('categories',$data2);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;
		}		
	}
	
	// change  premium category order..
	function change_premium_cate_order($old_order,$new_order)
	{	
		$this->db->connection_check();
		// fetching category id using sort_order id..
		$old_category = $this->db->get_where('premium_categories',array('sort_order'=>$old_order))->row('category_id');
		$new_category = $this->db->get_where('premium_categories',array('sort_order'=>$new_order))->row('category_id');
		
		$data1 = array('sort_order'=>$new_order);
		$this->db->where('category_id',$old_category);
		$this->db->update('premium_categories',$data1);
		
		$data2 = array('sort_order'=>$old_order);
		$this->db->where('category_id',$new_category);
		$updation = $this->db->update('premium_categories',$data2);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;
		}		
	}
	
	// get maximum category order..
	function get_maxcategory(){
		$this->db->connection_check();
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('categories');
		return $get_max->result();
	}
	
	// get overall count for shopping coupon code..
	function get_countshopcoupon($shopping_id){
		$this->db->connection_check();
		//$this->db->where('shoppingcoupon_id',$shopping_id);
		
		$result = $this->db->query("SELECT LENGTH(coupon_code) - LENGTH(REPLACE(coupon_code, ',', '')) as counting FROM `shopping_coupons` where shoppingcoupon_id=".$shopping_id);
		//$result = $this->db->get('shoppingcodes');
		if($result->num_rows > 0){
			return $result->row()->counting;
		}
		return false;
	}
	
	// get all referral emails..
	function all_referrals($email){
		$this->db->connection_check();
	$this->db->order_by('referral_id','desc');
		$this->db->where('user_email',$email);
		$referrals = $this->db->get('referrals');
		if($referrals->num_rows > 0){
			return $referrals->result();
		}
		return false;
	}
	
	// get all click history..
	function click_history($userid=null){
		$this->db->connection_check();
		if($userid!='')
		{
			if(is_numeric($userid))
			{
				$this->db->where('user_id',$userid);
			}
			else
			{
				$this->db->where('store_name',$userid);
			}
		}
		/*if($store_name!="")
		{
			$this->db->where('store_name',$store_name);
		}*/
		$this->db->order_by('click_id','desc');
		$all = $this->db->get('click_history');
		if($all->num_rows > 0){
			return $all->result();
		}
		return false;
	}
	
	// delete click history..
	function deletehistory($id){
		$this->db->connection_check();
		$this->db->delete('click_history',array('click_id'=>$id));
		return true;
	}
	
	//delete cashback
	function deletecashbackdetails($id){
		$this->db->connection_check();
		$this->db->delete('category_cashback',array('cbid'=>$id));
		return true;
	}
	
	//nathan
	
	// view all categories..
	function sub_categories($cateid){
		$this->db->connection_check();
		$this->db->order_by('sort_order');
		$this->db->where('cate_id',$cateid);
		$result = $this->db->get('sub_categories');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	// view all premium categories..
	function premium_sub_categories($cateid){
		$this->db->connection_check();
		$this->db->order_by('sort_order');
		$this->db->where('cate_id',$cateid);
		$result = $this->db->get('premium_sub_categories');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	// view cat name from id..
	function get_category_name($cateid){
		$this->db->connection_check();
		$this->db->where('category_id',$cateid);
		$result = $this->db->get('categories');
		if($result->num_rows > 0){
			return $result->row('category_name');	
		}
			return false;
	}
	// view cat name from id..
	function get_premium_category_name($cateid){
		$this->db->connection_check();
		$this->db->where('category_id',$cateid);
		$result = $this->db->get('premium_categories');
		if($result->num_rows > 0){
			return $result->row('category_name');	
		}
			return false;
	}
	// view sub cat name from id..
	function get_sub_category_name($sub_cateid){
		$this->db->connection_check();
		$this->db->where('sun_category_id',$sub_cateid);
		$result = $this->db->get('sub_categories');
		if($result->num_rows > 0){
			return $result->result('sub_category_name');	
		}
			return false;
	}
	
	// add new category
	function addsubcategory(){
		$this->db->connection_check();
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('sub_categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$max_val = $get->sort_order;
		}
		$maxval = $max_val + 1;
		$seo_url  = $this->admin_model->seoUrl($this->input->post('sub_category_name'));
		
		$data = array(
		'sub_category_name'=>$this->input->post('sub_category_name'),
		'cate_id'=>$this->input->post('cate_id'),
		'meta_keyword'=>$this->input->post('meta_keyword'),
		'meta_description'=>$this->input->post('meta_description'),
		'sort_order'=>1,
		'category_status'=>$this->input->post('category_status'),
		'sub_category_url'=> $seo_url
		);
		
		$this->db->insert('sub_categories',$data);
		return true;
	}		
	
	// add new premium category
	function addpremiumsubcategory(){
	/*print_r($this->input->post());
	exit;*/
		$this->db->connection_check();
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('premium_sub_categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$max_val = $get->sort_order;
		}
		$maxval = $max_val + 1;
		$seo_url  = $this->admin_model->seoUrl($this->input->post('sub_category_name'));
		
		$data = array(
		'sub_category_name'=>$this->input->post('sub_category_name'),
		'cate_id'=>$this->input->post('cate_id'),
		'meta_keyword'=>$this->input->post('meta_keyword'),
		'meta_description'=>$this->input->post('meta_description'),
		'sort_order'=>1,
		'category_status'=>$this->input->post('category_status'),
		'sub_category_url'=> $seo_url
		);
		
		$this->db->insert('premium_sub_categories',$data);
		return true;
	}		
	
	
	// edit category
	function get_subcategory($category_id){
		$this->db->connection_check();
		$this->db->where('sun_category_id',$category_id);
        $query = $this->db->get('sub_categories');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
           return $query->result();
        }
        return false;	
	}
	
	// edit category
	function get_premium_subcategory($category_id){
		$this->db->connection_check();
		$this->db->where('sun_category_id',$category_id);
        $query = $this->db->get('premium_sub_categories');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
           return $query->result();
        }
        return false;	
	}
	
	//update category
	function update_subcategory(){
			$this->db->connection_check();
	$seo_url  = $this->admin_model->seoUrl($this->input->post('sub_category_name'));
		$data = array(
			'cate_id'=>$this->input->post('cate_id'),
			'sub_category_name'=>$this->input->post('sub_category_name'),
			'meta_keyword'=>$this->input->post('meta_keyword'),
			'meta_description'=>$this->input->post('meta_description'),
			'category_status'=>$this->input->post('category_status'),
			'sub_category_url'=>$seo_url
		);
		$id = $this->input->post('sun_category_id');
		$this->db->where('sun_category_id',$id);
		$upd = $this->db->update('sub_categories',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}
	}
	
	//update category
	function update_permiumsubcategory(){
	$this->db->connection_check();
	$seo_url  = $this->admin_model->seoUrl($this->input->post('sub_category_name'));
		$data = array(
			'cate_id'=>$this->input->post('cate_id'),
			'sub_category_name'=>$this->input->post('sub_category_name'),
			'meta_keyword'=>$this->input->post('meta_keyword'),
			'meta_description'=>$this->input->post('meta_description'),
			'category_status'=>$this->input->post('category_status'),
			'sub_category_url'=>$seo_url
		);
		$id = $this->input->post('sun_category_id');
		$this->db->where('sun_category_id',$id);
		$upd = $this->db->update('premium_sub_categories',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	// delete category
	function deletesubcategory($id){
		$this->db->connection_check();
		// get order of category which is to be deleted.
		$start_order = $this->db->get_where('sub_categories',array('sun_category_id'=>$id))->row('sort_order');
		
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('sub_categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$end_order = $get->sort_order;
		}
		$this->db->delete('sub_categories',array('sun_category_id' => $id));
		$newval = $start_order;
		for($inc=$start_order; $inc<=$end_order;$inc++){
			$newval = $newval + 1;
			
			$data = array('sort_order'=>$inc);
			$this->db->where('sort_order',$newval);
			$this->db->update('sub_categories',$data);
		}
		return true;
	}
	
	// delete category
	function deletepremiumsubcategory($id){
		$this->db->connection_check();
		// get order of category which is to be deleted.
		$start_order = $this->db->get_where('premium_sub_categories',array('sun_category_id'=>$id))->row('sort_order');
		
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('sub_categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$end_order = $get->sort_order;
		}
		$this->db->delete('premium_sub_categories',array('sun_category_id' => $id));
		$newval = $start_order;
		for($inc=$start_order; $inc<=$end_order;$inc++){
			$newval = $newval + 1;
			
			$data = array('sort_order'=>$inc);
			$this->db->where('sort_order',$newval);
			$this->db->update('premium_sub_categories',$data);
		}
		return true;
	}
	
	
	function seoUrl($string) {
		$this->db->connection_check();
		//Lower case everything
		$string = strtolower($string);
		//Make alphanumeric (removes all other characters)
		$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
		//Clean up multiple dashes or whitespaces
		$string = preg_replace("/[\s-]+/", " ", $string);
		//Convert whitespaces and underscore to dash
		$string = preg_replace("/[\s_]/", "-", $string);
		return $string;
	}
	
	function get_updated_categories($cateid,$maincateid)
	{
		$this->db->connection_check();
		$this->db->where('category_id',$maincateid);
		$this->db->where('store_id',$cateid);
        $query = $this->db->get('tbl_store_sub_cate');
        if($query->num_rows >= 1)
		{
           return $query->result();
        }
        return false;	
	}
	
	function get_premium_updated_categories($cateid,$maincateid)
	{
		$this->db->connection_check();
		$this->db->where('category_id',$maincateid);
		$this->db->where('store_id',$cateid);
        $query = $this->db->get('tbl_premium_sub_cate');
        if($query->num_rows >= 1)
		{
           return $query->result();
        }
        return false;	
	}
	
	// view all affiliates
	function site_affiliates(){
		$this->db->connection_check();
		$this->db->order_by('affiliate_id','desc');
		$result = $this->db->get('providers');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	// add new affiliates
	// add new affiliates
	function site_addaffiliate(){
		$this->db->connection_check();
		$data = array(
		'affiliate_name'=>$this->input->post('affiliate_name'),
		'affiliate_type'=>$this->input->post('affiliate_type'),
		'affiliate_status'=>$this->input->post('affiliate_status'),
		'affiliate_param'=>$this->input->post('affiliate_param'),
		'affiliate_traking_param'=>$this->input->post('affiliate_traking_param')		
		);
		$this->db->insert('providers',$data);		
		return true;
	}

	// New code for add bank details page//
	function bank_details()
	{
		$this->db->connection_check();
		$this->db->order_by('bankid','desc');
		$result = $this->db->get('tbl_banknames');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}

	function get_bankdetails($id){
	$this->db->connection_check();
	$this->db->where('bankid',$id);
	$result = $this->db->get('tbl_banknames');
		if($result->num_rows > 0){
			return $result->result();		
		}	
	}

	function add_bankdetails(){
		$this->db->connection_check();
		$data = array(
		'bank_name'=>$this->input->post('bank_name'),
		'bank_id'  =>$this->input->post('bank_num_id'),
		);
		$this->db->insert('tbl_banknames',$data);		
		return true;
	}

	function update_bankdetails(){
		$this->db->connection_check();
		
		$bank_id = $this->input->post('bank_id');
		$data = array(
			'bank_name'=>$this->input->post('bank_name'),
			'bank_id'  =>$this->input->post('bank_num_id'),	
		);
		
		$this->db->where('bankid',$bank_id);
		$updation = $this->db->update('tbl_banknames',$data);
		return true;	
	}
	function delete_bankdetails($id)
	{	
		$this->db->connection_check();
		$this->db->delete('tbl_banknames',array('bankid' => $id));
		return true;	
	}

	function delete_multi_bank_details()
	 {
		 $this->db->connection_check();
		  $sort_order = $this->input->post('chkbox');
		 foreach($sort_order as $key=>$val)
		 {
			 $id = $key;		
				$this->db->delete('tbl_banknames',array('id' => $id));			
		 }
		 return true;
	 }
	 function stores()
	 {
	 	$this->db->connection_check();
		$data = array(
		'store_name'=>$this->input->post('store_name'),
		);
		$this->db->insert('tbl_stores',$data);		
		return true;
	 }

	//End//
	
	// view affiliate
	function site_get_affiliate($id){
	$this->db->connection_check();
	$this->db->where('affiliate_id',$id);
	$result = $this->db->get('providers');
		if($result->num_rows > 0){
			return $result->result();		
		}	
	}
	
	
	function site_get_store($id){
	$this->db->connection_check();
	$this->db->where('affiliate_id',$id);
	$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->row();		
		}	
	}
	
	
	
	
	
	// update affiliate
	function site_updateaffiliate(){
		$this->db->connection_check();
		
		$affiliate_id = $this->input->post('affiliate_id');
		$data = array(
			'affiliate_name'=>$this->input->post('affiliate_name'),
			'affiliate_type'=>$this->input->post('affiliate_type'),
			'affiliate_status'=>$this->input->post('affiliate_status'),
			'affiliate_traking_param'=>$this->input->post('affiliate_traking_param'),
			'affiliate_param'=>$this->input->post('affiliate_param')
		
		);
		
		$this->db->where('affiliate_id',$affiliate_id);
		$updation = $this->db->update('providers',$data);
		return true;	
	}
	
	// delete affiliate
	function site_deleteaffiliate($id)
	{	
		$this->db->connection_check();
		$this->db->delete('providers',array('affiliate_id' => $id));
		return true;	
	}
	
	/************ Dec 11th *************/
	
	// view all cashbcak..
	function get_all_missing_cashback(){
		$this->db->connection_check();
		$this->db->order_by('cashback_id','desc');
		//new//
		$this->db->where('missing_reason','Missing Cashback');
		//$user_query = $this->db->get('cashback');
		//End//
		//old//
		$user_query = $this->db->get('missing_cashback');
		
		if($user_query->num_rows > 0)
        {
            return $user_query->result();
        }
		else
		{
			return false;
		}
	}

	//Pilaventhiran 07/05/2016 START
	function get_all_missing_approval(){
		$this->db->connection_check();
		$this->db->order_by('cashback_id','desc');
		//new//
		$this->db->where('missing_reason','Missing Approval');
		//$user_query = $this->db->get('cashback');
		//End//
		//old//
		$user_query = $this->db->get('missing_cashback');
		
		if($user_query->num_rows > 0)
        {
            return $user_query->result();
        }
		else
		{
			return false;
		}
	}
	//Pilaventhiran 07/05/2016 END
	
	function view_missing_cb($cbid)
	{
		$this->db->connection_check();
		$this->db->where('cashback_id',$cbid);        
        $query = $this->db->get('missing_cashback');
        if($query->num_rows >= 1)
		{
          return  $row = $query->row();
           // return $query->result();
        }
        return false;
	}
	
	
	
	// update user status..
	function missiing_cashback_update()
	{
		$name = $this->db->query("select * from admin")->row();
				$site_name  = $name->site_name;
				
		$this->db->connection_check();
		$curr_status = $this->input->post('status');	
		$cashback_id = $this->input->post('cashback_id');
		$username = $this->input->post('username');
		$us_email = $this->input->post('us_email');	
		$ticket_id = $this->input->post('ticket_id');
		$retailer_name = $this->input->post('retailer_name');
		$cancel_reason = $this->input->post('cancel_reason');
		$Cashback_Return_Amount = $this->input->post('Cashback_Return_Amount');
		$user_id = $this->input->post('user_id');
		switch($curr_status)
		{
			case 0:
				$userbalance = $this->user_balance($user_id);
				$new_balnce = $userbalance+$Cashback_Return_Amount;
				$mode = "Credited";
				$transation_reason = 'Cashback';
				$details_id = $this->input->post('cashback_id');
				$this->update_users_balance($user_id,$Cashback_Return_Amount,$mode,$new_balnce,$transation_reason,$details_id,'missing_cashback');				
		//	$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> Dear '.$username.',<br><br>Thank you for sending us the details of your transaction. Your Missing Cashback Ticket: '.$ticket_id.' has been Completed Successfully. Your Cashback Amount Added into your Account.<br><br>Please let us know if you have any further queries. Thaks For your business.<br><br> Current Status: Completed<br><br> Warm regards,<br> '.$site_name.' Team</span>';
				$current_msg ='';

			break;
			case 1:
			$mode_1 = "Sent to retailer";
			$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Acabamos de enviar a sua reclamação para a loja. O processo de conferência é manual e pode levar até 40 dias úteis para que eles nos respondam. (A gente sabe que isso é muito demorado e é um saco esperar tanto, mas infelizmente não depende de nós . E esse é um “prazo máximo” pode ser que leve bem menos que isso).</span>';
			break;
			case 2:
			$mode_1 = "Cancelled";
				$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> A loja acabou de nos atualizar sobre o seu caso e, infelizmente, eles não aprovaram o seu pedido. A justificativa que nos deram foi: '.$cancel_reason.'. Para evitar problemas futuros leia as Regras e exceções da loja e siga os ###RECOMENDACOES_CANCELAR###”</span>';
			break;
			case 3:
			$mode_1 = "Approved";
				$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> 
Novidades! A loja acabou de nos confirmar que eles realmente não nos informaram da sua compra. Mas agora já está tudo resolvido! Acabou de pingar R$ '.$Cashback_Return_Amount.' na sua conta.
				</span>';
			break;
			
		}
		$data = array(
			'status'=>$this->input->post('status'),
			'status_update_date'=>$this->input->post('status_update_date'),
			'cancel_msg'=>$this->input->post('cancel_msg'),
			'current_msg'=>$current_msg,
		);	
		$cashback_id = $this->input->post('cashback_id');
		$this->db->where('cashback_id',$cashback_id);
		$upd = $this->db->update('missing_cashback',$data);
		if($upd)
		{
			if($curr_status!=1)
			{

				$mail_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='14'")->row();
				$fe_cont = $mail_temp->email_template;
				$name = $this->db->query("select * from admin")->row();
				$subject = "Your Missing Ticket Reply";
				$admin_emailid = $name->admin_email;
				$site_logo = $name->site_logo;
				$site_name  = $name->site_name;
				$contact_number = $name->contact_number;
				$servername = base_url();
				$nows = date('Y-m-d');	
				$this->load->library('email');
				$see_status_missing = "<a href='".base_url()."loja_nao_avisou_compra'>status da solicitação</a>";
				$unsuburl     = base_url().'cashback/un_subscribe/myaccount/'.$get_userid;
            $myaccount    = base_url().'cashback/minha_conta';
				$gd_api=array(
							'###ADMINNO###'=>$contact_number,
							'###EMAIL###'=>$username,
							'###DATE###'=>$nows,
							'###MESSAGE###'=>$current_msg,
							'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
							'###SITENAME###' =>$site_name,
							'###MISSING_CASHBACK_STATUS###'=>$mode_1,
							'###SEE_STATUS_MISSING###'=>$see_status_missing,
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
                        	'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
							);
						   
				$gd_message=strtr($fe_cont,$gd_api);
				//echo $gd_message;
				/*
				$config['protocol'] = 'sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				*/
				/*
				$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' => 'vivek.developer@osiztechnologies.com',
				'smtp_pass' => 'iamnotlosero',
				'mailtype'  => 'html', 
				'charset'   => 'iso-8859-1'
				);
				*/
				$config = Array(
				 'mailtype'  => 'html',
				  'charset'   => 'utf-8',
				  );

				$list = array($us_email);
				
				$this->email->initialize($config);
				$this->email->set_newline("\r\n");
				$this->email->from($admin_emailid,$site_name.'!');
				$this->email->to($list);
				$this->email->subject($subject);
				$this->email->message($gd_message);
				$this->email->send();
				$this->email->print_debugger();
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	
	// delete user details..
	function delete_missing_cashback($id)
	{
		$this->db->connection_check();
		$this->db->delete('missing_cashback',array('cashback_id' => $id));  
		$this->db->delete('transation_details',array('details_id' => $id));     
		return true;
	}
	
	function update_users_balance($userid,$trans_amount,$mode,$newbalnce,$transation_reason,$details_id=null,$table=null)
	{
		$this->db->connection_check();
		$data = array(		
		'balance' => $newbalnce);
		$this->db->where('user_id',$userid);
		$update_qry = $this->db->update('tbl_users',$data);
		if($update_qry)
		{
			$now = date('Y-m-d H:i:s');
			// Transation
			
			$this->db->select_max('trans_id');
				$result = $this->db->get('transation_details')->row();  
				$trans_id = $result->trans_id;
				$trans_id = $trans_id+1;
				$n9 = '5236555';
				$n12 = $n9 + $trans_id; 
			$data = array(		
			'transation_amount' => $trans_amount,
			'user_id' => $userid,
			'transation_date' => $now,
			'transation_reason' => $transation_reason,
			'mode' => $mode,
			'transation_id'=>$n12,
			'details_id'=>$details_id,
			'table'=>$table,
			'transation_status ' => 'Pending');
			$this->db->insert('transation_details',$data);
			$user_deta = $this->view_user($userid);
			//$name = $this->db->query("select * from admin")->row();
			if($user_deta->refer!=0)
			{
				$refer = $user_deta->refer;
				
				//New code for refferal category type//
					$categorytype = $user_deta->referral_category_type;
					if($categorytype == 1)
					{
						$name   = $this->db->query("select * from referral_settings where category_type='categoryone'")->row();
						$status = $name->ref_by_rate;
						if($status == 1)
						{	
							$caspe = $name->ref_cashback_rate;
						}
					}
					if($categorytype == 2)
					{
						$name   = $this->db->query("select * from referral_settings where category_type='categorytwo'")->row();
						$status = $name->ref_by_rate;
						if($status == 1)
						{	
							$caspe = $name->ref_cashback_rate;
						}	
					}
					if($categorytype == 3)
					{
						$name   = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();
						$status = $name->ref_by_rate;
						if($status == 1)
						{	
							$caspe = $name->ref_cashback_rate;
						}	
					}
					if($categorytype == 5)
					{
						$name   = $this->db->query("select * from referral_settings where category_type='categoryfive'")->row();
						$status = $name->ref_by_rate;
						if($status == 1)
						{	
							$caspe = $name->ref_cashback_rate;
						}	
					}
				//End//
				//$caspe = $name->referral_cashback;
				$cal_percent =($trans_amount*$caspe)/100;
				$this->db->select_max('trans_id');
				$result = $this->db->get('transation_details')->row();  
				$trans_id = $result->trans_id;
				$trans_id = $trans_id+1;
				$n9 = '5236555';
				$n12 = $n9 + $trans_id; 
	
				$data = array(		
				'transation_amount' => $cal_percent,
				'user_id' => $userid,
				'transation_date' => $now,
				'transation_id'=>$n12,
				'transation_reason' => "Pending Referal Payment",
				'mode' => $mode,
				'details_id'=>'',
				'table'=>'',
				'transation_status ' => 'Pending');
				$this->db->insert('transation_details',$data);
			}

			if($user_deta->referral_category_type == 3)
			{

			}
			
			$this->db->where('cashback_id',$details_id);        
       		$querys = $this->db->get('missing_cashback')->row();
			$click_details = $this->click_history_details($querys->click_id);
		$this->db->select_max('cashback_id');
				$result = $this->db->get('cashback')->row();  
				$cashback_id = $result->cashback_id;
				$cashback_id = $cashback_id+1;
				$n9 = '666554';
				$n12 = $n9 + $cashback_id; 
				$m = '1';
			$data = array(		
			'user_id' => $userid,
			'coupon_id' => $click_details->voucher_name,
			'affiliate_id' => $querys->retailer_name,
			'status' => 'Completed',
			'cashback_amount'=>$trans_amount,
			'reference_id'=>$n12,
			'referral'=>$m,
			'date_added' => $now);
			$this->db->insert('cashback',$data);
			
			
			return true;
		}
		else 
		{ 
			return false;
		}	
	}
	
	function user_balance($user_id=null)
	{
		$this->db->connection_check();
		if($user_id!="")
		{
			$this->db->where('user_id',$user_id);
			$allfaqs = $this->db->get('tbl_users');
			return $allfaqs->row("balance");
		}
		else
		{
			return 0;
		}
		
	}	
	
	function click_history_details($click_id)
	{
		$this->db->connection_check();
		$this->db->where('click_id',$click_id);
		$all = $this->db->get('click_history');
		if($all->num_rows > 0){
			return $all->row();
		}
		return false;
	}
	/************ Dec 11th *************/
	
	function bulk_stores($bulkcoupon)
	{
		$this->db->connection_check(); 
		$coupon_type = '';
		$this->load->library('CSVReader');
		$main_url = 'uploads/stores/'.$bulkcoupon;
	 	$result =   $this->csvreader->parse_file($main_url);
	
		if(count($result)!=0)
		{
			foreach($result as $res)
			{
				$new_random = mt_rand(0,99999);
				$affiliate_name = $res['affiliate_name'];
				$seo_url  = $this->admin_model->seoUrl($affiliate_name);
				$affiliate_logo_1 = $res['affiliate_logo'];
				
				//$file = 'http://media.themalaysianinsider.com/assets/uploads/articles/flipkart-logo-072914.jpg.jpg';
				// Open the file to get existing content
				$data = file_get_contents($affiliate_logo_1);
				
				$affiliate_logo = $new_random.'.jpg';
				
				// New file
				$new = 'uploads/affiliates/'.$affiliate_logo;
				// Write the contents back to a new file
				file_put_contents($new, $data);				
				$affiliate_desc = $this->db->escape_str($res['affiliate_desc']);
				$cashback_percentage = $res['cashback_percentage'];
				$site_url = $res['site_url'];
				
				/*$website_url = $res['website_url'];*/
				$meta_keyword = $this->db->escape_str($res['meta_keyword']);
				$meta_description = $this->db->escape_str($res['meta_description']);
				$affiliate_cashback_type = $this->db->escape_str($res['affiliate_cashback_type']);				
				$featured = $res['featured'];
				$now = date('Y-m-d H:i:s');
				$store_of_week = $res['store_of_week'];				
				//Pilaventhiran 14/05/2016 START
				$logo_url = $res['logo_url'];
				
				$retailer_ban_url = $res['retailer_ban_url'];
				$sidebar_image_url = $res['sidebar_image_url'];
				$terms_and_conditions = $res['terms_and_conditions'];
				$how_to_get_this_offer = $res['how_to_get_this_offer'];
				$notify_desk = $res['notify_desk'];
				$old_cashback = $res['old_cashback'];
				$redir_notify = $res['redir_notify'];
				$name_extra_param = $res['name_extra_param'];
				$content_extra_param = $res['content_extra_param'];
				$cashback_percent_android = $res['cashback_percent_android'];
				$cashback_content_android = $res['cashback_content_android'];
				$content_extra_param_android = $res['content_extra_param_android'];
				//Pilaventhiran 14/05/2016 END
				$this->db->where('affiliate_name',$affiliate_name);
				$result = $this->db->get('affiliates');
				
				if($result->num_rows == 0)
				{
					$results = $this->db->query("INSERT INTO `affiliates` (`affiliate_name`, `affiliate_url`, `affiliate_logo`, `affiliate_desc`, `affiliate_status`, `cashback_percentage`, `logo_url`, `meta_keyword`, `meta_description`, `featured`, `affiliate_cashback_type`,`store_of_week`, `date_added`, `retailer_ban_url`, `sidebar_image_url`, `terms_and_conditions`, `how_to_get_this_offer`, `notify_desk`, `old_cashback`, `redir_notify`, `name_extra_param`, `content_extra_param`, `cashback_percent_android`, `cashback_content_android`, `content_extra_param_android`) VALUES ('$affiliate_name', '$seo_url', '$affiliate_logo', '$affiliate_desc', '1','$cashback_percentage', '$logo_url', '$meta_keyword', '$meta_description', '$featured', '$affiliate_cashback_type','$store_of_week','$now','$retailer_ban_url','$sidebar_image_url','$terms_and_conditions','$how_to_get_this_offer','$notify_desk','$old_cashback','$redir_notify','$name_extra_param','$content_extra_param','$cashback_percent_android','$cashback_content_android','$content_extra_param_android');");
				}				
				
			}
		}
		
		return true;
	}
	
	
	function reports_upload($bulkcoupon){ 
		$this->db->connection_check();
		$coupon_type = '';
		$this->load->library('CSVReader');
		$main_url = 'uploads/reports/'.$bulkcoupon;
	 	$result =   $this->csvreader->parse_file($main_url);
		//$name = $this->db->query("select * from admin")->row();
		//$ref_cashbcak_percent =  $name->referral_cashback;

		if(count($result)!=0)
		{	

			
		 
			$s =1;
			foreach($result as $res)
			{
				$pay_out_amount    = $res['pay_out_amount'];
				$sale_amount       = $res['sale_amount'];
				$offer_provider    = $res['offer_provider'];
				$transaction_id    = $res['transaction_id'];
				$user_tracking_id  = $res['user_tracking_id'];
				//$transaction_id1   = rand(10000,99999);
	  		    //$newtransaction_id = md5($transaction_id1);
	  		    //echo $newtransaction_id; exit;
				$store_details = $this->get_offer_provider_cashback($offer_provider);
			
				
				
				$get_userid = decode_userid($user_tracking_id);
			
				if($store_details)
				{
				
					if($store_details->cashback_percentage)
					{
						$affiliate_cashback_type = $store_details->affiliate_cashback_type;
					if($store_details->affiliate_cashback_type=='Percentage')//Percent
						{
							$is_cashback = 1;
							
							$cashback_percentage = $store_details->cashback_percentage;
							$cashback_calc = ($sale_amount*$cashback_percentage)/100;
							$cashback_amount = number_format($cashback_calc, 2);
							$check_ref = $this->check_ref_user($get_userid);
							$ref_cashback_amount = 0;
							if($check_ref>0)					
							{	
								/*old code
								$ref_id  = $check_ref;
								$ref_cashback_percent = $ref_cashbcak_percent;
								$ref_cashback_amount = number_format((($cashback_amount*$ref_cashbcak_percent)/100), 2);
								*/
								//New code for referral details//
								$ref_id  = $check_ref;
								$return = $this->check_active_user($ref_id);
								if($return)
								{
									foreach($return as $newreturn)
									{
										$category_type = $newreturn->referral_category_type; 
										if($category_type == 1)
										{	
											$referrals 	  		  = $this->db->query("select * from referral_settings where category_type='categoryone'")->row();	
											$status 			  = $referrals->ref_by_rate;
											if($status == 1)
											{
												$ref_cashback_percent = $referrals->ref_cashback_rate;
												$ref_cashback_amount  = $ref_cashback_percent;
											}
											 
										}
										if($category_type == 2)
										{
											$referrals 	  		  = $this->db->query("select * from referral_settings where category_type='categorytwo'")->row();	
											$status 			  = $referrals->ref_by_rate;
											if($status == 1)
											{	

												$ref_cashback_percent = $referrals->ref_cashback_rate;
												$ref_cashback_amount  = $ref_cashback_percent;
											}	
										}
										if($category_type == 3)
										{
											$referrals 	  		  = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
											$status 			  = $referrals->ref_by_rate;
											if($status == 1)
											{	
												$ref_cashback_percent = $referrals->ref_cashback_rate;
												$ref_cashback_amount  = $ref_cashback_percent;
											}
										}
										if($category_type == 5)
										{
											$referrals 	  		  = $this->db->query("select * from referral_settings where category_type='categoryfive'")->row();	
											$status 			  = $referrals->ref_by_rate;
											if($status == 1)
											{
												$ref_cashback_percent = $referrals->ref_cashback_rate;
												$ref_cashback_amount  = $ref_cashback_percent;
											}
										} 
									}
								}

								//End//
							}
							$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
						}
						else //Flat
						{
							$is_cashback = 1;
							$cashback_percentage = $store_details->cashback_percentage;
							//$cashback_calc = ($sale_amount*$cashback_percentage)/100;
							$cashback_amount = number_format($cashback_percentage, 2);
							$check_ref = $this->check_ref_user($get_userid);
							$ref_cashback_amount = 0;
							if($check_ref>0)					
							{
								/*old code
								$ref_id  = $check_ref;
								$ref_cashback_percent = $ref_cashbcak_percent;
								$ref_cashback_amount = number_format((($cashback_amount*$ref_cashbcak_percent)/100), 2);
								*/

								//New code for referral details//
								$ref_id  = $check_ref;
								$return = $this->check_active_user($ref_id);
								if($return)
								{
									foreach($return as $newreturn)
									{
										$category_type = $newreturn->referral_category_type; 
										if($category_type == 1)
										{	
											$referrals 	  		  = $this->db->query("select * from referral_settings where category_type='categoryone'")->row();	
											$status 			  = $referrals->ref_by_rate;
											if($status == 1)
											{
												$ref_cashback_percent = $referrals->ref_cashback_rate;
												$ref_cashback_amount  = $ref_cashback_percent;
											}
											 
										}
										if($category_type == 2)
										{
											$referrals 	  		  = $this->db->query("select * from referral_settings where category_type='categorytwo'")->row();	
											$status 			  = $referrals->ref_by_rate;
											if($status == 1)
											{	

												$ref_cashback_percent = $referrals->ref_cashback_rate;
												$ref_cashback_amount  = $ref_cashback_percent;
											}	
										}
										if($category_type == 3)
										{
											$referrals 	  		  = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
											$status 			  = $referrals->ref_by_rate;
											if($status == 1)
											{	
												$ref_cashback_percent = $referrals->ref_cashback_rate;
												$ref_cashback_amount  = $ref_cashback_percent;
											}
										}
										if($category_type == 5)
										{
											$referrals 	  		  = $this->db->query("select * from referral_settings where category_type='categoryfive'")->row();	
											$status 			  = $referrals->ref_by_rate;
											if($status == 1)
											{
												$ref_cashback_percent = $referrals->ref_cashback_rate;
												$ref_cashback_amount  = $ref_cashback_percent;
											}
										} 
										 
									}
								}
								//End//
							}

							$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
						}
					}
					else 
					{
						$is_cashback = 0;
						$cashback_percentage = 0;
						$cashback_amount=0;
						$ref_id = 0;
						$ref_cashback_percent= 0;
						$ref_cashback_amount =0;
						$total_Cashback_paid= 0;
						$affiliate_cashback_type ='';
					}
				}
				else
				{
					
						$is_cashback = 0;
						$cashback_percentage = 0;
						$cashback_amount=0;
						$ref_id = 0;
						$ref_cashback_percent= 0;
						$ref_cashback_amount =0;
						$total_Cashback_paid= 0;
						$affiliate_cashback_type='';
				}
				$date = $res['date'];
				$now = date('Y-m-d H:i:s');
				$last_updated = $now;
				
				$results = $this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`, `is_cashback`, `cashback_percentage`, `affiliate_cashback_type`,`cashback_amount`, `ref_id`, `ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`) VALUES ('$offer_provider', '$date', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '$last_updated', '$is_cashback', '$cashback_percentage', '$affiliate_cashback_type','$cashback_amount', '$ref_id','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','$now');");
				//,`report_update_id`,'$newtransaction_id'
				
				$insert_id = $this->db->insert_id();
				
				if($is_cashback!=0)
				{
					$update_user_bal = $this->update_user_bal($get_userid,$cashback_amount);
					$now = date('Y-m-d H:i:s');
					$transation_reason = "Cashback";
					$mode = "Credited";
					$this->db->select_max('trans_id');
				$result = $this->db->get('transation_details')->row();  
				$trans_id = $result->trans_id;
				$trans_id = $trans_id+1;
				$n9 = '5236555';
				$n12 = $n9 + $trans_id; 
					$data = array(		
						'transation_amount' => $cashback_amount,
						'user_id' => $get_userid,
						'transation_date' => $now,
						'transation_reason' => $transation_reason,
						'mode' => $mode,
                        'cashback_transaction' =>$sale_amount,
                        'cashback_reason' => $offer_provider,
						'details_id'=>$insert_id,
						'transation_id'=>$n12,
						'table'=>'tbl_report',
						'report_update-id'=>$newtransaction_id,
						'transation_status ' => 'Pending');
					$this->db->insert('transation_details',$data);
				
					if($ref_cashback_amount!=0)
					{
$this->db->select_max('trans_id');
				$result = $this->db->get('transation_details')->row();  
				$trans_id = $result->trans_id;
				$trans_id = $trans_id+1;
				$n9 = '5236555';
				$n12 = $n9 + $trans_id; 
						$update_user_bal = $this->update_user_bal($ref_id,$ref_cashback_amount);
						$data = array(		
							'transation_amount' => $ref_cashback_amount,
							'user_id' => $ref_id,
							'transation_date' => $now,
							'transation_reason' => 'Pending Referal Payment',
							'mode' => $mode,
							'transation_id'=>$n12,
							'details_id'=>'',
							'table'=>'',
							'transation_status ' => 'Pending');						
						$this->db->insert('transation_details',$data);
					}
					
					$this->db->select_max('cashback_id');
				$result = $this->db->get('cashback')->row();  
				$cashback_id = $result->cashback_id;
				$cashback_id = $cashback_id+1;
				$n9 = '666554';
				$n12 = $n9 + $cashback_id; 
					
					$data = array(		
					'user_id' => $get_userid,
					'coupon_id' => $offer_provider,
					'affiliate_id' => $offer_provider,
					'status' => 'Completed',
					'cashback_amount'=>$cashback_amount,
					'transaction_amount' => $sale_amount,
					'reference_id'=>$n12,
					//'report_update-id'=>$newtransaction_id,
					'date_added' => $now);
					$this->db->insert('cashback',$data);
				}
		
				$s++;
			}
		}
		
		return true;
	}
	
	function get_offer_provider_cashback($storename)
	{
		$this->db->connection_check();
		$this->db->like('affiliate_name', $storename);
		$this->db->limit(1,0);
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->result();
			return $result->row();
		}
		return false;
	}
//SATz //	
	function check_ref_user($ref_user)
	{
		$this->db->connection_check();
		$this->db->where('user_id',$ref_user);        
		$result = $this->db->get('tbl_users');
		if($result->num_rows > 0){
			return $result->row('refer');

		}
		return false;
	}

//SATz //	
	function update_user_bal($user_id,$newamount)
	{		
				$this->db->connection_check();	
				$old_bal = $this->user_balance($user_id);
				$new_bal = $old_bal+$newamount;
				$data = array(		
				'balance' => $new_bal);
				$this->db->where('user_id',$user_id);
				$update_qry = $this->db->update('tbl_users',$data);
	}
	
	function reports()
	{
		$this->db->connection_check();
		$this->db->order_by("report_id", "desc");
		$result = $this->db->get('tbl_report');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	function transaction_id_reports($transaction_id)
	{
		$this->db->connection_check();
		//$this->db->order_by("report_id", "desc");
		$con=$this->db->where('transaction_id',$transaction_id);
		$result = $this->db->get('tbl_report');
		if($result->num_rows > 0){
			return true;
		}
		return false;
	}
	
	
	function view_report($report_id)
	{
		$this->db->connection_check();
		$this->db->where('report_id',$report_id);        
        $query = $this->db->get('tbl_report');
        if($query->num_rows >= 1)
		{
          return $row = $query->row();
        }
        return false;
	}

	//New code for Pending cashback details//
	function report_export()
	{
		$this->db->connection_check();
		$this->db->order_by('affiliate_id','desc');
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	} 

	
	// view user email	
	function user_name($user_id)
	{
		$this->db->connection_check();	
		$result = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('first_name');
		return $result;	
	}
		
	
	function transactions(){
			$this->db->connection_check();
			$this->db->select("*");
			$this->db->from("transation_details");
			$this->db->order_by("trans_id", "desc");
			$result = $this->db->get();
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
		function get_transaction1($id){
			$this->db->connection_check();
			$this->db->where("trans_id", $id);
			$this->db->from("transation_details");
			$result = $this->db->get();
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	
	
	//adding cms..
	function addblog($logo)
	{
		$now = date('Y-m-d H:i:s');
		$data = array(
		'cms_heading' => $this->input->post('page_title'),
		'cms_metatitle' => $this->input->post('meta_title'),
		'cms_metakey' => $this->input->post('meta_keyword'),
		'cms_metadesc' => $this->input->post('meta_description'),
		'cms_content' => $this->input->post('cms_content'),
		'affiliate_logo' => $logo,
		'blog_time' =>  $now,
		'cms_status' => $this->input->post('cms_status')
		);
		
		$this->db->insert('tbl_blog',$data);
		return true;
	}
	// get all cms
	function get_allblog(){
		
		$cms_query = $this->db->get('tbl_blog');
		if($cms_query->num_rows > 0)
        {
            $row = $cms_query->row();
            return $cms_query->result();
        }
		else
		{
			return false;		
		}
	}
	
	// get particular cms
	function get_blogcontent($id){
	
		$this->db->where('cms_id',$id);        
        $query = $this->db->get('tbl_blog');
        if($query->num_rows >= 1)
		{
           $row = $query->row();			
            return $query->result();			
        }      
        return false;		
	}
	
	
	//update cms ..
	function updateblog($img){
	$now = date('Y-m-d H:i:s');
		$data = array(
			'cms_heading' => $this->input->post('page_title'),
			'cms_metatitle' => $this->input->post('meta_title'),
			'cms_metakey' => $this->input->post('meta_keyword'),
			'cms_metadesc' => $this->input->post('meta_description'),
			'cms_content' => $this->input->post('cms_content'),
			'cms_status' =>  $this->input->post('cms_status'),
			'blog_time' =>  $now,
			'affiliate_logo' =>  $img
		);
		$id =  $this->input->post('cms_id');
		$this->db->where('cms_id',$id);
		$upd = $this->db->update('tbl_blog',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}	
	}
	
	
	// delete cms..
	function deleteblog($id){
	
		$this->db->delete('tbl_blog',array('cms_id' => $id));
		return true;
	
	}
	
	function get_allcomments($blogid){
		$this->db->where('bid',$blogid);
		$cms_query = $this->db->get('tbl_bloguser_comments');
		if($cms_query->num_rows > 0)
        {
            $row = $cms_query->row();
            return $cms_query->result();
        }
		else
		{
			return false;		
		}
	}
	
	function delete_comments($id){
	
		$this->db->delete('tbl_bloguser_comments',array('cid' => $id));
		return true;
	
	}
	
	function status_change_comments($changes_id)
	{
		$this->db->where('cid',$changes_id);
		$cms_query = $this->db->get('tbl_bloguser_comments')->row();
		if($cms_query->status=='active')
		{
			$st = 'deactive';
		}
		else
		{
			$st = 'active';
		}
		$data = array(
			'status' =>  $st
		);
		$this->db->where('cid',$changes_id);
		$upd = $this->db->update('tbl_bloguser_comments',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}	
	}
	
	function add_comments(){
		$now = date('Y-m-d H:i:s');
		$data = array(
		'bid'=>$this->input->post('blog_id'),
		'user_id'=>'Admin',
		'comments'=>$this->input->post('comments'),
		'created_date'=>$now,
		'c_date'=>$now,
		'status'=>'active'	
		);
		$this->db->insert('tbl_bloguser_comments',$data);		
		return true;
	}
	
	function add_manual_credit($id)
	{
		if($id == '1'){
			if($this->input->post('status') == 'Approved'){
		$user_bale = $this->view_balance($this->input->post('user_id'));
		$newbalnce= $user_bale+$this->input->post('transation_amount');
		$data = array(		
		'balance' => $newbalnce);
		$this->db->where('user_id',$this->input->post('user_id'));
		$update_qry = $this->db->update('tbl_users',$data);
		}
		$this->db->select_max('trans_id');
				$result = $this->db->get('transation_details')->row();  
				$trans_id = $result->trans_id;
				$trans_id = $trans_id+1;
				$n9 = '5236555';
				$n12 = $n9 + $trans_id; 
		$now = date('Y-m-d');
		$data = array(
		'user_id'=>$this->input->post('user_id'),
		'transation_reason'=>$this->input->post('transation_reason'),
		'transation_amount'=>$this->input->post('transation_amount'),
		'transation_date'=>$now,
		'transaction_date'=>$now,
		'transation_id'=>$n12,
		'mode'=>'Credited',
		'transation_status'=>$this->input->post('status')	
		);
		$this->db->insert('transation_details',$data);	
		if($this->input->post('transation_reason')=="Cashback"){
			$refer_user = $this->view_user($this->input->post('user_id'));
			if($refer_user){
				foreach($refer_user as $single){
					$referral_user = $single->refer;
				}
				
				$referral_user_det = $this->view_user($referral_user);
				if($referral_user_det){
					foreach($referral_user_det as $single1){
						$referral_balance = $single1->balance;
						$referral_id 	  = $single1->user_id;
						
						//new code for referral cashback amount details//

						$category_type 	  = $single1->referral_category_type;

						if($category_type == 1)
						{
							$referrals = $this->db->query("select * from referral_settings where category_type='categoryone'")->row();	
							$status    = $referrals->ref_by_rate;
							if($status == 1)
							{
								$caspe     = $referrals->ref_cashback_rate;
							}
						}

						if($category_type == 2)
						{
							$referrals = $this->db->query("select * from referral_settings where category_type='categorytwo'")->row();	
							$status    = $referrals->ref_by_rate;
							if($status == 1)
							{
								$caspe     = $referrals->ref_cashback_rate;
							}
						}

						if($category_type == 3)
						{
							$referrals = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
							$status    = $referrals->ref_by_rate;
							if($status == 1)
							{
								$caspe     = $referrals->ref_cashback_rate;
							}
						}

						if($category_type == 5)
						{
							$referrals = $this->db->query("select * from referral_settings where category_type='categoryfive'")->row();	
							$status    = $referrals->ref_by_rate;
							if($status == 1)
							{
								$caspe     = $referrals->ref_cashback_rate;
							}
						}

						//End//
					}
					
					//$name = $this->db->query("select * from admin")->row();
					//$caspe = $name->referral_cashback;

					$tr_amt = $this->input->post('transation_amount');
					//$cal_percent =($tr_amt*$caspe)/100;
					//echo $cal_percent; exit;
					$this->db->select_max('trans_id');
				$result = $this->db->get('transation_details')->row();  
				$trans_id = $result->trans_id;
				$trans_id = $trans_id+1;
				$n9 = '5236555';
				$n12 = $n9 + $trans_id; 
					$data = array(		
					'transation_amount' => $cal_percent,
					'user_id' => $referral_id,
					'transation_date' => $now,
					'transation_reason' => "Referal Payment",
					'mode'=>'Credited',
					'transation_id'=>$n12,
					'details_id'=>'',
					'table'=>'',
					'transation_status ' => $this->input->post('status'));
					$this->db->insert('transation_details',$data);
					if($this->input->post('status') == 'Approved'){
					$this->db->where('user_id',$referral_id);
					$this->db->update('tbl_users',array('balance'=>$referral_balance+$cal_percent));
				}
				}
			}
		}		
		return true;
	}
	else if($id=='0'){//echo "dfkmdkfd";exit;
		$trans_id = $this->input->post('trans_id');
		if($this->input->post('transation_status') != 'Approved' || $this->input->post('transation_status') != 'Paid'){//echo "fjg";exit;
		if($this->input->post('status') == 'Approved'){
		$user_bale = $this->view_balance($this->input->post('user_id'));
		$newbalnce= $user_bale+$this->input->post('transation_amount');
		$data = array(		
		'balance' => $newbalnce);
		$this->db->where('user_id',$this->input->post('user_id'));
		$update_qry = $this->db->update('tbl_users',$data);
		}
	}
		if($this->input->post('transation_status') == 'Approved' || $this->input->post('transation_status') == 'Paid'){
		if($this->input->post('status') == 'Canceled' || $this->input->post('status') == 'Pending'){
		$user_bale = $this->view_balance($this->input->post('user_id'));
		$newbalnce= $user_bale-$this->input->post('transation_amount');
		$data = array(		
		'balance' => $newbalnce);
		$this->db->where('user_id',$this->input->post('user_id'));
		$update_qry = $this->db->update('tbl_users',$data);
		}
	}
		$data = array(
		'transation_status'=>$this->input->post('status')
		);
		$this->db->where('trans_id',$trans_id);
		$this->db->update('transation_details',$data);		
		return true;
	}
}
	
	function count_coupons($catename=null)
	{
		$count_coupons = $this->db->query("SELECT count(*) as counting FROM `coupons` where offer_name like '%$catename%'");
		if($count_coupons->num_rows > 0)
        {
            return $count_coupons->row();
        }
		else
		{
			return false;
		}
	}
	
	function count_clicks($catid)
	{
		$count_coupons = $this->db->query("SELECT count(*) as counting FROM `click_history` where affiliate_id='$catid'");
		if($count_coupons->num_rows > 0)
        {
            return $count_coupons->row();
        }
		else
		{
			return false;
		}
	}
	
	
/*New changes 9-5-16*/
// add new shopping coupon..
function add_shoppingcoupon($img)
 {
	 $this->db->connection_check();
	   // premium_coupon_feature
	   
		$premium_coupon_feature = $this->input->post('premium_coupon_feature'); 
		$start_date = $this->input->post('start_date');
		if($start_date!=""){
			$start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
		}
		else {
			$start_date = date('Y-m-d');
		}
		$expiry_date = date('Y-m-d',strtotime($this->input->post('expiry_date')));
		$stcat = $this->input->post('categorys_list');
		
		if($this->input->post('categorys_list'))
		{
			$store_categorys =implode(",",$this->input->post('categorys_list')); 
		}
		else
		{
			$store_categorys ='';
		}
		
		if($premium_coupon_feature)
		{
			$exp_couponfeatures =implode(",",$premium_coupon_feature);  
		}
		else
		{
			$exp_couponfeatures ='';  
		}
		
		$store_name    = $this->input->post('store_name');  
		$extra_param   = $this->input->post('extra_param');  
		$features_type = $this->input->post('premium_coupon_feature'); 


		if($features_type)
		{
			$new_features_type =implode(",",$features_type);  
		}
		else
		{
			$new_features_type ='';  
		}
		

		 
		 $codes=$this->input->post('code');  
		  
		$seo_url  = $this->admin_model->newseoUrl($this->input->post('offer_name'));
		
		$data = array(
			 
			'offer_name'=>$this->input->post('offer_name'),
			'coupon_image'=>$img,
			'description'=>$this->input->post('description'),
			'about'=>$this->input->post('about'),
			'nutshel'=>$this->input->post('nutshel'),
			'fine_print'=>$this->input->post('fine_print'),
			'company'=>$this->input->post('company'),
			'location'=>$this->input->post('location'),
			'user_max' => $this->input->post('user_max'),
			'seo_url'=>$seo_url,
				
			'category'=>$store_categorys,
			'type'=>$this->input->post('type'),
			 // 'title'=>$this->input->post('title'),
			// 'code'=>$this->input->post('code'),
			'long_description' => $this->input->post('long_description'),
			'offer_page'=>$this->input->post('offer_page'),
			'amount'=>$this->input->post('amount'),
			 	'coupon_code'=>$codes,  
			'remain_coupon_code'=>$codes,   
			'status'=>"1",    
			'start_date'=>$start_date,
			'expiry_date'=>$expiry_date,

			/*New code 9-5-16*/
			'price'=>$this->input->post('org_price'),
			'store_name'=>$store_name,
			'tracking'=>$extra_param,
			'features_type'=>$new_features_type
			/*end code 9-5-16*/
		); 
		
		
		$this->db->insert('shopping_coupons',$data);
		$new_id = $this->db->insert_id();
		
		/*$codes = $this->input->post('code');
		foreach($codes as $val){
			
			$data = array(
				'shoppingcoupon_id'=>$new_id,
				'status'=>'1',
				'code'=>$val
			);
			
			
			$this->db->insert('shoppingcodes',$data);		
		} */
		
		/* 
		foreach($stcat as $maincat)
		 {
				$var="size_".$maincat;
				$subcat = $this->input->post($var);
				foreach($subcat as $subcategory)
				{
					$data = array(
					'category_id'=>$maincat,
					'sub_category_id'=>$subcategory,
					'store_id'=>$new_id
					);
					$this->db->insert('tbl_premium_sub_cate',$data);
			}
		} 
		
		*/ 
		 
		 
		return true;
 }
/*New changes are accure 11-5-16*/

function update_shoppingcoupon($img) 
{
	$this->db->connection_check();
		/*print_r($this->input->post());
		exit;*/
		$premium_coupon_feature = $this->input->post('premium_coupon_feature'); 
		if($premium_coupon_feature)
		{
			$exp_couponfeatures =implode(",",$premium_coupon_feature); 
		}
		else
		{
			$exp_couponfeatures ='';
		}
			
		if($this->input->post('start_date')){
			$start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
		}
		else 
		{
			$start_date = date('Y-m-d');
		}
		$stcat = $this->input->post('categorys_list');
		if($this->input->post('categorys_list'))
		{
			$store_categorys =implode(",",$this->input->post('categorys_list'));
		}
		else
		{
			$store_categorys ='';
		}
		
		$expiry_date 		= date('Y-m-d',strtotime($this->input->post('expiry_date')));	
		$shoppingcoupon_id  = $this->input->post('shoppingcoupon_id'); 
		$codes 			    = $this->input->post('code');
		$seo_url  	        = $this->admin_model->seoUrl($this->input->post('offer_name'));
		$data = array(
			 
			'offer_name'	   	 =>$this->input->post('offer_name'), 
			'seo_url'		   	 =>$seo_url, 			
			'coupon_image'     	 =>$img,
			'description'	   	 =>$this->input->post('description'),
			'about'			   	 =>$this->input->post('about'),
			'nutshel'		   	 =>$this->input->post('nutshel'),
			'fine_print'	   	 =>$this->input->post('fine_print'),
			'company'		   	 =>$this->input->post('company'),
			'location'		   	 =>$this->input->post('location'),
			'category'	 	   	 =>$store_categorys,
			'type'			   	 =>$this->input->post('type'),	
			'amount'		   	 =>$this->input->post('amount'),
			'offer_page'	   	 =>$this->input->post('web_url'),  
			'start_date'	   	 =>$start_date,
			'long_description' 	 =>$this->input->post('long_description'),
			'date_added'       	 =>$start_date,
			'user_max' 		   	 => $this->input->post('user_max'),
			'expiry_date'      	 =>$expiry_date, 
			'coupon_code'        =>$codes,  
			'remain_coupon_code' =>$codes, 
			'price'				 =>$this->input->post('org_price'),
			'store_name'		 =>$this->input->post('store_name'),
			'tracking'  	  	 =>$this->input->post('extra_param'),
			'features'		     =>$exp_couponfeatures
		);
		
		$this->db->where('shoppingcoupon_id',$shoppingcoupon_id);
		$updation = $this->db->update('shopping_coupons',$data);
   
		/*
		$codes = $this->input->post('code');
		$shoppingcode_id = $this->input->post('shoppingcode_id');
		if($codes)
		{
			foreach($codes as $key=>$val){
				
				$data = array(
					'code'=>$val
				);
				$this->db->where('shoppingcode_id',$shoppingcode_id[$key]);
				$updation = $this->db->update('shoppingcodes',$data);
			}  
		} */  
		  
		// this is for inserting new coupons..
		/* 	$new_codes = $this->input->post('codes');
			if($new_codes!=""){
				foreach($new_codes as $val){
			
					$data = array(
						'shoppingcoupon_id'=>$shoppingcoupon_id,
						'status'=>'1',
						'code'=>$val
					);
					$this->db->insert('shoppingcodes',$data); 
				}
			} */
			//print_r($this->input->post());  
			 
		/* 	foreach($stcat as $maincat)
			{
				
				$var="size_".$maincat;
				$subcat = $this->input->post($var);
				foreach($subcat as $subcategory)
				{
					$data = array(
					'category_id'=>$maincat,
					'sub_category_id'=>$subcategory,
					'store_id'=>$shoppingcoupon_id
					); 
					$this->db->insert('tbl_premium_sub_cate',$data);
			}
		}		 */  
		
	//	exit; 
		
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;
		}
 }	
function reviews(){
	$this->db->connection_check();
	/* $this->db->order_by('id','desc');
	 $result = $this->db->get('revi ews');*/
	 
	 $result = $this->db->query("SELECT * FROM (reviews) where coupon_id IN
							(
								SELECT  shoppingcoupon_id 
									FROM
										`shopping_coupons`
									
							) ORDER BY `id` desc");
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	 
}
 function get_name($cis,$uid)
 {
	 $this->db->connection_check();
	 //echo $uid;die;
	 $this->db->where('user_id',$uid);
	 $result = $this->db->get('tbl_users');
	//echo $this->db->last_query();die;
		if($result->num_rows > 0){
			$uresult = $result->row();
			$uname = $uresult->first_name;
		}else{
			$uname = '';
		}
	
	 $this->db->where('shoppingcoupon_id',$cis);
	
		$result = $this->db->get('shopping_coupons');
		if($result->num_rows > 0){
			$uresult = $result->row();
			$cname = $uresult->offer_name;
			$seo_url = $uresult->seo_url;
		}else{
			$cname = '';
			$seo_url = '';
		}
		return $uname.",".$cname.",".$seo_url;
 }
	
	function changestatus($id,$status)
	{
		$this->db->connection_check();
		if($status==1) $var=0;else $var=1;
			$data = array(
			'approve'=>$var,
			
			);
			$this->db->where('id',$id);
			$updation = $this->db->update('reviews',$data);	
	}
	
	function orders()
	{
		$this->db->connection_check();
		$this->db->order_by('id','desc');
		$this->db->where('status','Paid');
		$result = $this->db->get('premium_order');
		if($result->num_rows > 0){
			return $result->result();
		}
			return false;
	}
	 
	 function sort_categorys_new()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('sort_arr');
		 foreach($sort_order as $key=>$val)
		 {
			  $data = array(
				'sort_order'=>$val			
				);
			$this->db->where('category_id',$key);
			$updation = $this->db->update('categories',$data);	
		 }
		 return true;
			
	 }
	 
	 function sort_premium_categorys_new()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('sort_arr');
		 foreach($sort_order as $key=>$val)
		 {
			  $data = array(
				'sort_order'=>$val			
				);
			$this->db->where('category_id',$key);
			$updation = $this->db->update('premium_categories',$data);	
		 }
		 return true;
			
	 }
	 
	 
	 function multi_delete_user()
	 {
		 $this->db->connection_check();
		 if($this->input->post('chkbox'))
		 {
			  $sort_order = $this->input->post('chkbox');
			  foreach($sort_order as $key=>$val)
			  {
				   $this->db->where('user_id',$key);
				   $query = $this->db->get('tbl_users');
				   $email = $query->row('email');
					
					$data = array(
						'admin_status' => 'deleted',
						'status' =>  '0'
					);
					$this->db->where('user_id',$key);
					$upd = $this->db->update('tbl_users',$data);
					$this->db->delete('referrals',array('referral_email' => $email));   
			  }
		 	return true;
		 }
			
	 }
	 
	 function sort_categorys_new_delete()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
		 foreach($sort_order as $key=>$val)
		 {
			 $id = $key;
		
			// get order of category which is to be deleted.
			$start_order = $this->db->get_where('categories',array('category_id'=>$id))->row('sort_order');
			
			$this->db->select_max('sort_order');
			$get_max = $this->db->get('categories');
			$gets = $get_max->result();
			foreach($gets as $get){
				$end_order = $get->sort_order;
			}
	
			$this->db->delete('categories',array('category_id' => $id));
			$newval = $start_order;
			for($inc=$start_order; $inc<=$end_order;$inc++){
				$newval = $newval + 1;
				
				$data = array('sort_order'=>$inc);
				$this->db->where('sort_order',$newval);
				$this->db->update('categories',$data);
			}
			
		 }
		 return true;
			
	 }
	 
	 
	 
	 
	  function sort_sub_categorys_new()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('sort_arr');
		 foreach($sort_order as $key=>$val)
		 {
			  $data = array(
				'sort_order'=>$val			
				);
			$this->db->where('sun_category_id',$key);
			$updation = $this->db->update('sub_categories',$data);	
		 }
		 return true;
			
	 }
	 
	  function sort_sub_categorys_new_delete()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
		 foreach($sort_order as $key=>$val)
		 {
			 $id = $key;
		
			// get order of category which is to be deleted.
			$start_order = $this->db->get_where('sub_categories',array('sun_category_id'=>$id))->row('sort_order');
			
			$this->db->select_max('sort_order');
			$get_max = $this->db->get('sub_categories');
			$gets = $get_max->result();
			foreach($gets as $get){
				$end_order = $get->sort_order;
			}
	
			$this->db->delete('sub_categories',array('sun_category_id' => $id));
			$newval = $start_order;
			for($inc=$start_order; $inc<=$end_order;$inc++){
				$newval = $newval + 1;
				
				$data = array('sort_order'=>$inc);
				$this->db->where('sort_order',$newval);
				$this->db->update('sub_categories',$data);
			}
			
		 }
		 return true;
			
	 }
	 		
	 function sort_premium_categorys_new_delete()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
		 foreach($sort_order as $key=>$val)
		 {
			 $id = $key;		
				$start_order = $this->db->get_where('premium_categories',array('category_id'=>$id))->row('sort_order');		
				$this->db->select_max('sort_order');
				$get_max = $this->db->get('premium_categories');
				$gets = $get_max->result();
				foreach($gets as $get){
					$end_order = $get->sort_order;
				}
		
				$this->db->delete('premium_categories',array('category_id' => $id));
				$newval = $start_order;
				for($inc=$start_order; $inc<=$end_order;$inc++){
					$newval = $newval + 1;
					
					$data = array('sort_order'=>$inc);
					$this->db->where('sort_order',$newval);
					$this->db->update('premium_categories',$data);
				}			
			
		 }
		 return true;
			
	 }
	 
	 function delete_multi_site_affiliate()
	 {
		 $this->db->connection_check();
		  $sort_order = $this->input->post('chkbox');
		 foreach($sort_order as $key=>$val)
		 {
			 $id = $key;		
				$this->db->delete('providers',array('affiliate_id' => $id));			
		 }
		 return true;
	 }
	 
	 
	 function sort_affiliates()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('sort_arr');
		 foreach($sort_order as $key=>$val)
		 {
			  $data = array(
				'sort_order'=>$val			
				);
			$this->db->where('affiliate_id',$key);
			$updation = $this->db->update('affiliates',$data);	
		 }
		 return true;
	 }
	 
	 function sort_affiliates_delete()
	 {
		 $this->db->connection_check();
		  $sort_order = $this->input->post('chkbox');
		 foreach($sort_order as $key=>$val)
		 {
				 $id = $key;
				 $this->db->delete('affiliates',array('affiliate_id' => $id));
				$this->db->delete('tbl_store_sub_cate',array('store_id' => $id));
				
				$this->db->delete('click_history',array('affiliate_id' => $id));	
					
		 }
		  return true;
		
	 }
	 
	 function click_history_bulk_delete()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
			 foreach($sort_order as $key=>$val)
			 {
					 $id = $key;
					 $this->db->delete('click_history',array('click_id'=>$id));
			 }
		return true;	
	 }
	 
	 function cashback_details_bulk_delete()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
			 foreach($sort_order as $key=>$val)
			 {
					 $id = $key;
					 $this->db->delete('category_cashback',array('cbid'=>$id));
			 }
		return true;	
	 }
	 
	 
	  function coupons_bulk_delete()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
			 foreach($sort_order as $key=>$val)
			 {
					 $delete_id = $key;
					 $this->db->delete('coupons',array('coupon_id'=>$delete_id));
	
			 }
		return true;	
	 }
	 
	 
	 function bulk_shopping_delete()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
			foreach($sort_order as $key=>$val)
			 {
					 $delete_id = $key;
					$this->db->delete('shopping_coupons',array('shoppingcoupon_id'=>$delete_id)); 
			 }
		return true;
	 }
	 
	 function delete_bulk_orders()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
			foreach($sort_order as $key=>$val)
			 {
					 $delete_id = $key;
					$this->db->delete('premium_order',array('id'=>$delete_id)); 
			 }
		return true;
	 }
	 
	 function bulk_reviews_delete()
	 {
		 $this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
			foreach($sort_order as $key=>$val)
			 {
					 $delete_id = $key;
					$this->db->delete('reviews',array('id'=>$delete_id)); 
			 }
		return true; 
	 }
	 
	 function reports_bulk_delete()
	 {
		 $this->db->connection_check();
			$sort_order = $this->input->post('chkbox');
			foreach($sort_order as $key=>$val)
			 {
					 $delete_id = $key;
					$this->db->delete('tbl_report',array('report_id'=>$delete_id)); 
			 }
		return true;  
	 }
	 
	 
	function delete_bulk_records($sort_order,$table,$feald)
	{		 
			$this->db->connection_check();
			foreach($sort_order as $key=>$val)
			{
			    $delete_id = $key;
			    /*New code for update multiple users balance details 27-5-16.*/
			 	$cb_r  = $this->db->get_where('cashback',array('cashback_id'=>$delete_id))->row();   
				if($cb_r)
				{
					$txn_id  = $cb_r->txn_id;
				    $amount  = $cb_r->cashback_amount;
				    $user_id = $cb_r->user_id;
				}
				if($cb_r->status == "Completed")
				{
					//echo "hai"; exit;
					/*New code for update a user balance details 27-5-16.*/
					$total_amt 	   = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('balance');
					$remain_amount = ($total_amt - $amount);  
					$data = array(		
						'balance' => $remain_amount);
								
					$this->db->where('user_id',$user_id);
					$update_qry = $this->db->update('tbl_users',$data);
					/*End 27-5-16.*/
				}
			 	/*End*/
				$this->db->delete($table,array($feald=>$delete_id));
				$this->db->delete('transation_details',array('trans_id'=>$txn_id)); 
			}
			return true;  
	}
	 
	 
	//forgetpassword
	function forgetpassword(){
		$this->db->connection_check();
		$email = $this->input->post('forget_email');
		
		 $admin_email = $this->db->get_where('admin', array('admin_id' =>'1'))->row('admin_email');
		
		if($admin_email!=$email){
			return false;
		}
		//send email 
			$this->load->library('email');
			
			$this->db->where('admin_id',1);
			$admin_det = $this->db->get('admin');
			if($admin_det->num_rows >0) 
			{    
				 $admin = $admin_det->row();
				 $admin_email = $admin->admin_email;
				 $site_name = $admin->site_name;
				 $admin_no = $admin->contact_number;
				 $admin_password = $admin->admin_password;
				   $site_logo = $name->site_logo;
			}			
			$date =date('Y-m-d');
			
			$this->db->where('mail_id','6');
			$mail_template = $this->db->get('tbl_mailtemplates');
			if($mail_template->num_rows >0) 
			{        
			   $fetch = $mail_template->row();
			   $subject = $fetch->email_subject;  
			   $templete = $fetch->email_template;
			    $config = Array(
				  'protocol' => 'smtp',
				  'smtp_host' => 'ssl://smtp.googlemail.com',
				  'smtp_port' => 465,
				  'smtp_user' => 'vivek.developer@osiztechnologies.com',
				  'smtp_pass' => 'iamnotlosero',
				  'mailtype'  => 'html',
				  'charset'   => 'iso-8859-1'
				  );
     // $this->email->initialize($config);        
     			 $this->email->set_newline("\r\n");
			   $this->email->from($admin_email,$site_name.'!');
			   $this->email->to($user_email);
			   $this->email->subject($subject);
				
			    $data = array(
					'###PASSWORD###'=>$admin_password,
					'###COMPANYLOGO###'=>'<img alt="" src='.base_url()."/uploads/adminpro/".$site_logo.' />',
					'###SITENAME###'=>$site_name,
					'###ADMINNO###'=>$admin_no,
					'###DATE###'=>$date
			    );
			   $content_pop=strtr($templete,$data);
			   $this->email->message($content_pop);
			   $this->email->send(); 
				return 'success';
			}
			
	}
	
function multi_delete_banners()
{
	$this->db->connection_check();
		 if($this->input->post('chkbox'))
		 {
			  $sort_order = $this->input->post('chkbox');
			  foreach($sort_order as $key=>$val)
			  {
				  $this->db->delete('tbl_banners',array('banner_id' => $key));
			  }
		 	return true;
		 }
}
function payment_settings()
{
		$this->db->connection_check();
		$merchant_key = $this->input->post('merchant_key');
		$merchant_salt = $this->input->post('merchant_salt');
		$merchant_id = $this->input->post('merchant_id');
		$payment_mode = $this->input->post('payment_mode');
		//$data['payment_mode'] = $details->payment_mode;			
			$data = array(
			'merchant_key'=>$merchant_key,
			'merchant_salt'=>$merchant_salt,
			'merchant_id'=>$merchant_id,
			'payment_mode'=>$payment_mode
			);
			$this->db->where('admin_id',1);	
			$this->db->update('admin',$data);
			return true;
}
function category_cashback($cateid)
{
		$this->db->connection_check();
		$this->db->where('store_categorys !=', '');
		$this->db->where('affiliate_id',$cateid);	
		$all = $this->db->get('affiliates');
		if($all->num_rows > 0)
		{
			$results=  $all->row();
			$store_categorys = $results->store_categorys;
			//echo "SELECT * FROM `categories` where FIND_IN_SET(`category_id`,'$store_categorys')";
			$quers = $this->db->query("SELECT * FROM `categories` where FIND_IN_SET(`category_id`,'$store_categorys')");
			if($quers->num_rows > 0)
			{
				$fetch = $quers->result();
				return $fetch;
			//$catelist = explode(',',$store_categorys);
			}
			else
			{
				return false;
			}
						
		}
		else
		{
			return false;
		}
}
function cashback_details_category($catid,$storeid)
{
	$this->db->connection_check();
	$this->db->where('category_id',$catid);	
	$this->db->where('store_id',$storeid);
	$all = $this->db->get('category_cashback');
	if($all->num_rows > 0)
	{
		$results=  $all->row();
		return $results;
	}
	else
	{
		return false;
	}
	
}
	function update_catecashback_ins($caskbackid=null)
	{
		$this->db->connection_check();
		if($this->input->post('action')=='new')
		{
			$data = array(			
			'store_id' => $this->input->post('store_id'),
			'cashback_type' => $this->input->post('cashback_type'),
			'cashback' => $this->input->post('cashback'),
			'cashback_details' => $this->input->post('cashback_details'),
			'status' => $this->input->post('status')		
			);
			$this->db->insert('category_cashback',$data);
			return true;
		}
		else
		{
			$data = array(			
			'store_id' => $this->input->post('store_id'),
			'cashback_type' => $this->input->post('cashback_type'),
			'cashback' => $this->input->post('cashback'),
			'cashback_details' => $this->input->post('cashback_details'),
			'status' => $this->input->post('status')		
			);
			
			$cbid = $this->input->post('caskbackid');
			$this->db->where('cbid',$cbid);
			$updation = $this->db->update('category_cashback',$data);
			return true;
		}
		
	}
	function upload_reports($bulkcoupon)
	{
		$this->db->connection_check();
		$coupon_type = '';	
		$this->load->library('CSVReader');	
		$main_url = 'uploads/reports/'.$bulkcoupon;	
		$result =   $this->csvreader->parse_file($main_url);	
		//print_r($result);exit;
		//$name = $this->db->query("select * from admin")->row();	
		//$ref_cashbcak_percent =  $name->referral_cashback;	
		if(count($result)!=0)	
		{
			
			$s=1;
			
			//$effectiveDate = "2016-05-23";
			//echo date('Y-m-d', strtotime("+3 months", strtotime($effectiveDate))); 
			//echo "<pre>";print_r($result);exit;			
			
			foreach($result as $res)	
			{ 
//SATz report duplication identifier 
				$all=0;
				$transaction_id   = $res['transaction_id'];
				$this->db->where('transaction_id',$transaction_id);
				$all = $this->db->get('tbl_report')->num_rows();
//                echo '-->'.$all;

      

				if($all == 0)
				{
				$dt = new DateTime($res['date']);
				$datess = $dt->format('Y-m-d');
			
				$pay_out_amount   = $res['pay_out_amount'];	
				$sale_amount 	  = $res['sale_amount'];	
				$offer_provider   = $res['offer_provider'];	
				
				
			
//SATz report duplication identifier 				
				$cashback 		  = $res['cashback'];	
				$user_tracking_id = $res['user_tracking_id'];		
//SATz//

                $plataform = $res['plataform'];
				$type_cb = $res['type_cb'];
				$calc_cb = $res['calc_cb'];
				$commission = $res['commission'];

//SATz//
				$store_details 		 = $this->get_offer_provider_cashback($offer_provider);
			 	$get_userid 		 = decode_userid($user_tracking_id); 
				//echo $get_userid; exit;
				$cashback_amount 	 = $cashback;
				$check_ref 		 	 = $this->check_ref_user($get_userid);
				if($transaction_id !='')
				{
					$newtransaction_id   = $transaction_id;
				}else
				{
					$newtransaction_id   = rand(1000,9999);	
				}	
				
				$getuser 			 = $this->view_user($get_userid);
				
				//echo "<pre>"; print_r($getuser);
				/*//foreach($getuser as $newusers)
				//{
				 $reg_date     = '2016-02-23';
				 $monthcount   = 1;						
				echo $exp_date      = date('Y-m-d', strtotime("+$monthcount months", strtotime($reg_date)));
				echo "<br>";echo $new_expdate  = strtotime($exp_date); echo "<br>";

				echo $current_date = date("Y-m-d"); 
				echo "<br>"; echo $tday_date    =  strtotime($current_date);
				//}
				exit;*/
				//$this->db->where('user_id',$storeid);
				//$all = $this->db->get('category_cashback');
	  		    //$newtransaction_id = md5($transaction_id1);
				//print_r($check_ref);echo"fdnk";
				$new_txn_id = rand(1000,9999);
				$ref_cashback_amount = 0;
				$ref_id = 0;
				$referred = 0;
				$txn_id_new = 0;
				if($check_ref>0)		
				{
					$ref_id  = $check_ref;
					$return = $this->check_active_user($ref_id);

					//New code for referral amount bu categorywise//
					 
					$now  = date('Y-m-d');
					$n9   = '666554';
					$n12  = $n9 + $ref_id; 	
					$mode = "Credited";	
					

					if($return)
					{	
						//$new_txn_id = rand(1000,9999);
						$referred = 1;
						$i =1;
						foreach($return as $newreturn)
						{
							 
							$category_type = $newreturn->referral_category_type; 
							//echo $category_type;die; 

							if($category_type == 1)
							{	
								//echo "hai";
								$referrals 	  		  = $this->db->query("select * from referral_settings where category_type ='categoryone'")->row();	
								$ref_by_percentage    = $referrals->ref_by_percentage;
								$ref_by_rate          = $referrals->ref_by_rate;
								$bonus_by_ref_rate    = $referrals->bonus_by_ref_rate;

								//Code for (1** Type--Refferal by Percentage) 30-3-16//
								if($ref_by_percentage == 1)
								{	
									$cashback_percentage = $referrals->ref_cashback;
									$monthcount			 = $referrals->valid_months;

									foreach($getuser as $newusers)
									{
										$reg_date     = date('Y-m-d', strtotime($newusers->date_added));
										$exp_date     = date('Y-m-d', strtotime("+$monthcount months", strtotime($reg_date)));
										$new_expdate  = strtotime($exp_date);
									 	$current_date = date("Y-m-d"); 
										$tday_date    = strtotime($current_date);
										$mailids 	  = $newusers->email;
										//23-3-2015 >= 24-3-2015
										if(($new_expdate) >= ($tday_date))
										{	
											
											$new_cashback_amount = (($cashback_amount)*($cashback_percentage)/100);  
											$data = array(			
											'transation_amount' => $new_cashback_amount,	
											'user_id' => $ref_id,	
											'transation_date' => $now,
											'transation_id'=>$n12,	
											'transation_reason' => 'Referral Cashback amount',	
											'mode' => $mode,
											'transation_date' => $datess,
											'details_id'=>'',	
											'table'=>'',	
											'transation_status ' => 'Pending',
											'new_txn_id'=> $new_txn_id,
											'report_update_id'=>$newtransaction_id
											);

											$this->db->insert('transation_details',$data);
											$txn_id_new = $this->db->insert_id();
										}
										else
										{
											$data = array(
												'status' => 'Inativa',
											);
											$this->db->where('referral_email',$mailids);
											$updates = $this->db->update('referrals',$data);	 
										}
									}
								}
								//Code for (1** Type--Refferal by Percentage) End//

								//Code for (2** Type--Refferal by Rate) 30-3-16//
								if($ref_by_rate == 1)
								{	
									$ref_cashback_amount  = $referrals->ref_cashback_rate;
								}
							}

							if($category_type == 2)
							{	
								
								$referrals 	  		  = $this->db->query("select * from referral_settings where category_type='categorytwo'")->row();	
								$ref_by_percentage    = $referrals->ref_by_percentage;
								$ref_by_rate          = $referrals->ref_by_rate;
								$bonus_by_ref_rate    = $referrals->bonus_by_ref_rate;

								//Code for (1** Type--Refferal by Percentage) 30-3-16//
								if($ref_by_percentage == 1)
								{	
									$cashback_percentage = $referrals->ref_cashback;
									$monthcount			 = $referrals->valid_months;

									foreach($getuser as $newusers)
									{
										$reg_date     = date('Y-m-d', strtotime($newusers->date_added));
										$exp_date     = date('Y-m-d', strtotime("+$monthcount months", strtotime($reg_date)));
										$new_expdate  = strtotime($exp_date);
									 	$current_date = date("Y-m-d"); 
										$tday_date    = strtotime($current_date);
										$mailids 	  = $newusers->email;
										//23-3-2015 >= 24-3-2015
										if(($new_expdate) >= ($tday_date))
										{	
											
											$new_cashback_amount = (($cashback_amount)*($cashback_percentage)/100);  
											$data = array(			
											'transation_amount' => $new_cashback_amount,	
											'user_id' => $ref_id,	
											'transation_date' => $now,
											'transation_id'=>$n12,	
											'transation_reason' => 'Referral Cashback amount',	
											'mode' => $mode,
											'transation_date' => $datess,
											'details_id'=>'',	
											'table'=>'',	
											'transation_status ' => 'Pending',
											'new_txn_id'=> $new_txn_id,
											'report_update_id'=>$newtransaction_id
											);

											$this->db->insert('transation_details',$data);
											$txn_id_new = $this->db->insert_id();
										}
										else
										{
											$data = array(
												'status' => 'Inativa',
											);
											$this->db->where('referral_email',$mailids);
											$updates = $this->db->update('referrals',$data);	 
										}
									}
								}
								//Code for (2** Type--Refferal by Rate) 30-3-16//
								if($ref_by_rate == 1)
								{	
									$ref_cashback_amount  = $referrals->ref_cashback_rate;
								}
							}

							if($category_type == 3)
							{	
								
								$referrals 	  		  = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
								$ref_by_percentage    = $referrals->ref_by_percentage;
								$ref_by_rate          = $referrals->ref_by_rate;
								$bonus_by_ref_rate    = $referrals->bonus_by_ref_rate;


								//Code for (1** Type--Refferal by Percentage) 30-3-16//
								if($ref_by_percentage == 1)
								{	
									$cashback_percentage = $referrals->ref_cashback;
									$monthcount			 = $referrals->valid_months;

									foreach($getuser as $newusers)
									{
										$reg_date     = date('Y-m-d', strtotime($newusers->date_added));
										$exp_date     = date('Y-m-d', strtotime("+$monthcount months", strtotime($reg_date)));
										$new_expdate  = strtotime($exp_date);
									 	$current_date = date("Y-m-d"); 
										$tday_date    = strtotime($current_date);
										$mailids 	  = $newusers->email;
										//23-3-2015 >= 24-3-2015
										if(($new_expdate) >= ($tday_date))
										{	
											
											$new_cashback_amount = (($cashback_amount)*($cashback_percentage)/100);  
											$data = array(			
											'transation_amount' => $new_cashback_amount,	
											'user_id' => $ref_id,	
											'transation_date' => $now,
											'transation_id'=>$n12,	
											'transation_reason' => 'Referral Cashback amount',	
											'mode' => $mode,
											'transation_date' => $datess,
											'details_id'=>'',	
											'table'=>'',	
											'transation_status ' => 'Pending',
											'new_txn_id'=> $new_txn_id,
											'report_update_id'=>$newtransaction_id
											);

											$this->db->insert('transation_details',$data);
											$txn_id_new = $this->db->insert_id();
										}
										else
										{
											$data = array(
												'status' => 'Inativa',
											);
											$this->db->where('referral_email',$mailids);
											$updates = $this->db->update('referrals',$data);	 
										}
									}
								}
								//Code for (2** Type--Refferal by Rate) 30-3-16//
								if($ref_by_rate == 1)
								{	
									$ref_cashback_amount  = $referrals->ref_cashback_rate;
								}
							}

							if($category_type == 4)
							{ 
								
								$referrals 	  		  = $this->db->query("select * from referral_settings where category_type='categoryfour'")->row();	
								$ref_by_percentage    = $referrals->ref_by_percentage;
								$ref_by_rate          = $referrals->ref_by_rate;
								$bonus_by_ref_rate    = $referrals->bonus_by_ref_rate;

								 
								//Code for (1** Type--Refferal by Percentage) 30-3-16//
								if($ref_by_percentage == 1)
								{	
									$cashback_percentage = $referrals->ref_cashback;
									$monthcount			 = $referrals->valid_months;

									foreach($getuser as $newusers)
									{
										$reg_date     = date('Y-m-d', strtotime($newusers->date_added));
										$exp_date     = date('Y-m-d', strtotime("+$monthcount months", strtotime($reg_date)));
										$new_expdate  = strtotime($exp_date);
									 	$current_date = date("Y-m-d"); 
										$tday_date    = strtotime($current_date);
										$mailids 	  = $newusers->email;
										//23-3-2015 >= 24-3-2015
										if(($new_expdate) >= ($tday_date))
										{	
											
											$new_cashback_amount = (($cashback_amount)*($cashback_percentage)/100);  
											$data = array(			
											'transation_amount' => $new_cashback_amount,	
											'user_id' => $ref_id,	
											'transation_date' => $now,
											'transation_id'=>$n12,	
											'transation_reason' => 'Referral Cashback amount',	
											'mode' => $mode,
											'transation_date' => $datess,
											'details_id'=>'',	
											'table'=>'',	
											'transation_status ' => 'Pending',
											'new_txn_id'=> $new_txn_id,
											'report_update_id'=>$newtransaction_id
											);

											$this->db->insert('transation_details',$data);
											$txn_id_new = $this->db->insert_id();
										}
										else
										{
											$data = array(
												'status' => 'Inativa',
											);
											$this->db->where('referral_email',$mailids);
											$updates = $this->db->update('referrals',$data);	 
										}
									}
								}
								//Code for (2** Type--Refferal by Rate) 30-3-16//
								if($ref_by_rate == 1)
								{	
									$ref_cashback_amount  = $referrals->ref_cashback_rate;
								}	
							}

							if($category_type == 5)
							{	
								
								$referrals 	  		  = $this->db->query("select * from referral_settings where category_type='categoryfive'")->row();	
								$ref_by_percentage    = $referrals->ref_by_percentage;
								$ref_by_rate          = $referrals->ref_by_rate;
								$bonus_by_ref_rate    = $referrals->bonus_by_ref_rate;

								//Code for (1** Type--Refferal by Percentage) 30-3-16//
								if($ref_by_percentage == 1)
								{	
									$cashback_percentage = $referrals->ref_cashback;
									$monthcount			 = $referrals->valid_months;

									foreach($getuser as $newusers)
									{
										$reg_date     = date('Y-m-d', strtotime($newusers->date_added));
										$exp_date     = date('Y-m-d', strtotime("+$monthcount months", strtotime($reg_date)));
										$new_expdate  = strtotime($exp_date);
									 	$current_date = date("Y-m-d"); 
										$tday_date    = strtotime($current_date);
										$mailids 	  = $newusers->email;
										//23-3-2015 >= 24-3-2015
										if(($new_expdate) >= ($tday_date))
										{	
											
											$new_cashback_amount = (($cashback_amount)*($cashback_percentage)/100);  
											$data = array(			
											'transation_amount' => $new_cashback_amount,	
											'user_id' => $ref_id,	
											'transation_date' => $now,
											'transation_id'=>$n12,	
											'transation_reason' => 'Referral Cashback amount',	
											'mode' => $mode,
											'transation_date' => $datess,
											'details_id'=>'',	
											'table'=>'',	
											'transation_status ' => 'Pending',
											'new_txn_id'=> $new_txn_id,
											'report_update_id'=>$newtransaction_id
											);

											$this->db->insert('transation_details',$data);
											$txn_id_new = $this->db->insert_id();
										}
										else
										{
											$data = array(
												'status' => 'Inativa',
											);
											$this->db->where('referral_email',$mailids);
											$updates = $this->db->update('referrals',$data);	 
										}
									}
								}

								//Code for (2** Type--Refferal by Rate) 30-3-16//
								if($ref_by_rate == 1)
								{	
									$ref_cashback_amount  = $referrals->ref_cashback_rate;
								}
							}
							$i++;	 
						} 
					}

					//End//
				}

				//echo $s;
				$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
				
				//main contents
				$is_cashback = 1;	
				$cashback_percentage = 0;	
				$cashback_amount=$cashback;	
				$ref_id = $ref_id;	
				$ref_cashback_percent= $ref_cashback_amount;	
				$ref_cashback_amount =$ref_cashback_amount;	
				$total_Cashback_paid= $total_Cashback_paid;	
				$affiliate_cashback_type='';
				//main contents
				$date = $res['date'];	
				$now = date('Y-m-d H:i:s');	
				$last_updated = $now;
				$results = $this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`, `is_cashback`, `cashback_percentage`, `affiliate_cashback_type`,`cashback_amount`, `ref_id`, `ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`) VALUES ('$offer_provider', '$date', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '$last_updated', '$is_cashback', '$cashback_percentage', '$affiliate_cashback_type','$cashback_amount', '$ref_id','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','$now','$newtransaction_id');");	
				$insert_id = $this->db->insert_id();	
				if($is_cashback!=0)	
				{	
					// $update_user_bal = $this->update_user_bal($get_userid,$cashback_amount);	
					$now = date('Y-m-d');	
					// $transation_reason = "Pending Cashback";	
					$mode = "Credited";	
					/* $data = array(		
						'transation_amount' => $cashback_amount,	
						'user_id' => $get_userid,	
						'transation_date' => $now,	
						'transation_reason' => $transation_reason,	
						'mode' => $mode,	
						'details_id'=>$insert_id,	
						'table'=>'tbl_report',	
						'transation_status ' => 'Progress');	
					$this->db->insert('transation_details',$data); */

					if($ref_cashback_amount!=0)	
					{
						$refer_details = $this->db->query("select * from tbl_users where user_id=$get_userid")->row();
						$refer_status  = $refer_details->referral_amt;
						if($refer_status == 0)
						{	
							$this->db->select_max('trans_id');
							$result = $this->db->get('transation_details')->row();  
							$trans_id = $result->trans_id;
						 
							$trans_id = $trans_id+1;
							$n9 = '5236555';
							$n12 = $n9 + $trans_id; 
							// $update_user_bal = $this->update_user_bal($ref_id,$ref_cashback_amount);	
							$data = array(			
								'transation_amount' => $ref_cashback_amount,	
								'user_id' => $ref_id,	
								'transation_date' => $now,
								'transation_id'=>$n12,	
								'transation_reason' => 'Pending Referal Payment',	
								'mode' => $mode,
								'transation_date' => $datess,
								'details_id'=>'',	
								'table'=>'',
								'new_txn_id'=> '',	
								'transation_status ' => 'Pending',
								'report_update_id'=>$newtransaction_id
								);
							//echo print_r($data); exit;
							$this->db->insert('transation_details',$data);
							$txn_id_new = $this->db->insert_id();

							//User table update referral status//
							
							$data = array(		
							'referral_amt' => 1);
							$this->db->where('user_id',$get_userid);
							$update_qry = $this->db->update('tbl_users',$data);	

							//End//
						}	
					}

					/*else
					{
						$this->db->select_max('trans_id');
						$result = $this->db->get('transation_details')->row();  
						$trans_id = $result->trans_id;
						$trans_id = $trans_id+1;
						$n9 = '5236555';
						$n12 = $n9 + $trans_id; 
						// $update_user_bal = $this->update_user_bal($ref_id,$ref_cashback_amount);	
							$data = array(			
							'transation_amount' => $cashback_amount,	
							'user_id' => $get_userid,	
							'transation_date' => $now,
							'transation_id'=>$n12,	
							'transation_reason' => 'Cashback',	
							'mode' => $mode,	
							'details_id'=>'',	
							'table'=>'',	
					        'cashback_transaction' =>$sale_amount,
					        'cashback_reason' => $offer_provider,
							'transation_status ' => 'Pending');
						$this->db->insert('transation_details',$data);
						$txn_id_new = $this->db->insert_id();
					}*/

					$this->db->select_max('cashback_id');
					$result = $this->db->get('cashback')->row();  
					$cashback_id = $result->cashback_id;
					$cashback_id = $cashback_id+1;
					$n9 = '666554';
					$n12 = $n9 + $cashback_id; 

//print_r($get_userid);exit;

						$data = array(
						'user_id' => $get_userid,	
						'coupon_id' => $offer_provider,	
						'affiliate_id' => $offer_provider,	
						'status' => 'Pending',	
						'cashback_amount'=>$cashback_amount,	
						'date_added' => $now,
						'referral' => $referred,
						'transaction_amount' => $sale_amount,
						'transaction_date' => $datess,
						'report_update_id'=>$newtransaction_id,
						'reference_id'=>$n12,
						'new_txn_id'=>$new_txn_id,
						'txn_id' => $txn_id_new,
//SATz//
					'plataform' =>$plataform,
					'type_cb' => $type_cb,
					'calc_cb' => $calc_cb,
					'commission' => $commission
//SATz//					
					
						);	
						$this->db->insert('cashback',$data);	
									
						/* mail for pending cashback */
					$user_detail = $this->view_user($get_userid);
					if($user_detail)
					{
						foreach($user_detail as $user_detail_single)
						{
								$referral_balance = $user_detail_single->balance;
								$user_email = $user_detail_single->email;
								$user_name  = $user_detail_single->first_name.' '.$user_detail_single->last_name;
								$accbalance = $user_detail_single->acbalance_mail;
						}
					}
						$this->db->where('admin_id',1);
						$admin_det = $this->db->get('admin');
					if($admin_det->num_rows >0) 
					{    
							$admin = $admin_det->row();
							$admin_email = $admin->admin_email;
							$site_name = $admin->site_name;
							$admin_no = $admin->contact_number;
							$site_logo = $admin->site_logo;
					}
						
					$date =date('Y-m-d');
				
					if($accbalance == 1)
					{

						$this->db->where('mail_id',10);
						$mail_template = $this->db->get('tbl_mailtemplates');
						if($mail_template->num_rows >0) 
						{
							   $fetch = $mail_template->row();
							   $subject = $fetch->email_subject;
							   $templete = $fetch->email_template;
							   // $url = base_url().'cashback/my_earnings/';
							   $unsuburl	 = base_url().'cashback/un_subscribe/myaccount/'.$get_userid;
							   $myaccount    = base_url().'cashback/minha_conta';
							   
								$this->load->library('email');
								
								$config = Array(
									'mailtype'  => 'html',
									'charset'   => 'utf-8',
								);
								
								$sub_data = array(
									'###SITENAME###'=>$site_name
								);
								$subject_new = strtr($subject,$sub_data);
								
								//$this->email->initialize($config);
								 $this->email->set_newline("\r\n");
								   $this->email->initialize($config);
								   $this->email->from($admin_email,$site_name.'!');
								   $this->email->to($user_email);
								   $this->email->subject($subject_new);
							   
								$data = array(
									'###NAME###'=>$user_name,
									'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
									'###SITENAME###'=>$site_name,
									'###ADMINNO###'=>$admin_no,
									'###DATE###'=>$date,
									'###AMOUNT###'=>str_replace('.', ',', $cashback_amount),
									'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
									'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
							    );
							   
							   $content_pop=strtr($templete,$data);
							   // echo $content_pop; echo $subject_new;
							   $this->email->message($content_pop);
							   $this->email->send();  
						}
					}	/* mail for pending cashback */
				}
				$s++;
			}	
		  }//exit;
		}
//echo "454";exit;
		return true;
	}
	function check_active_user($user_id){
		
		$this->db->connection_check();
		$this->db->where('user_id',$user_id);
		$this->db->where('status','1');
		$this->db->where('admin_status','');
		$ret = $this->db->get('tbl_users');
		 
		if($ret->num_rows > 0){
			//return 1;
			return $ret->result();
		}
			return false;
		//return 0;
	}
	
	function ads(){
	$this->db->connection_check();
	$this->db->order_by('ads_id','ASC');
		$result = $this->db->get('ads');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	function get_ads($ads_id)
	{
		$this->db->connection_check();
		$result = $this->db->get_where('ads',array('ads_id'=>$ads_id))->row();
		return $result;
	}
	
	function updateads($img)
	{
		$this->db->connection_check();
		$data = array(
			'ads_url' => $this->input->post('ads_url'),
			'ads_position' => $this->input->post('ads_position'),
			'ads_image' =>  $img
		);
		
		$id =  $this->input->post('ads_id');
		$this->db->where('ads_id',$id);
		$upd = $this->db->update('ads',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}	
	}
	
	function contacts()
	{
		$this->db->connection_check();
		$this->db->order_by('id','desc');
		$result = $this->db->get('tbl_contact');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	 }
	 
	 
	 function deletecontact($id)
	 {
		 $this->db->connection_check();	
		$this->db->connection_check();$this->db->delete('tbl_contact',array('id' => $id));
		return true;
	
	}
	
	function multi_delete_contacts()
	{
			$this->db->connection_check();
			 if($this->input->post('chkbox'))
			 {
				  $sort_order = $this->input->post('chkbox');
				  foreach($sort_order as $key=>$val)
				  {
					  $this->db->delete('tbl_contact',array('id' => $key));
				  }
				return true;
			 }
	}
	function select_table($tab_conti)
	{
		$this->db->connection_check();
		if($tab_conti=='active')
		{
			 $selqry="SELECT * FROM shopping_coupons  WHERE expiry_date >='".date('Y-m-d')."' order by shoppingcoupon_id desc";  
			 $result=$this->db->query("$selqry"); 
				if($result->num_rows > 0)
				{		
					return $result->result();
				}
		}
		else
		{
			 $selqry="SELECT * FROM shopping_coupons  WHERE expiry_date <='".date('Y-m-d')."' order by shoppingcoupon_id desc";  
			 $result=$this->db->query("$selqry"); 
				if($result->num_rows > 0)
				{		
					return $result->result();
				}
		}
		
		
	}
	//New code for Report Export page//
	//SATz //
	function users_bank_details()
	{
		$this->db->connection_check();
		//$this->db->order_by('bank_id','desc');
		$bank_array = array(
		'account_holder !='=> "",
		'bank_name != '=> "",
		'branch_name != '=> "",
		'account_number !='=> "",
		'ifsc_code !='=> "",
			);
		$this->db->where($bank_array);
		$result = $this->db->get('tbl_users');
		if($result->num_rows > 0){
			return $result->result();
			//return true;
		}
		return false;
	}
	

	function pending_report()
	{
			$store_name  = $this->input->post('store_name');
			$start_date1 = $this->input->post('start_date');
			$end_date1 	 = $this->input->post('end_date');
			$bank_name 	 = $this->input->post('bank_name');
			$start_date  = date("Y-m-d",strtotime($start_date1));
			$end_date    = date("Y-m-d",strtotime($end_date1));
			$user_id 	 = $this->input->post('user_id');
            $sep_user    = explode(',',$user_id);
            $type 		 = $this->input->post('type');
            $user 		 = $user_id;

            //New code for csv downloader//

				$filename   = "export_report-".$t_date.".csv";
				$this->load->dbutil();
				$this->load->helper('download');
				$delimiter  = ",";
				$newline    = "\r\n";
				$enclosure  = '"';

			//End//
             
            //Pending cashback menu//

            if($type == 'pending_cashback')
			{
					$i=0;         	
	            
	            //foreach($sep_user as $user)
				//{	
					 
					if(($store_name =="All") && ($start_date1=='') && ($end_date1=='') && ($user ==''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`coupon_id`,`transaction_amount`,`status`,`date_added` FROM cashback";
					}
					if(($store_name !="All") && ($start_date1=='') && ($end_date1=='') && ($user ==''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`coupon_id`,`transaction_amount`,`status`,`date_added` FROM `cashback` where coupon_id='$store_name'";
					}

					if(($store_name=="All") && ($user=='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`coupon_id`,`transaction_amount`,`status`,`date_added` FROM cashback where date_added BETWEEN '$start_date' AND '$end_date'";
					}
					
					if(($store_name !="All") && ($start_date1!='') && ($end_date1!='') && ($user ==''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`coupon_id`,`transaction_amount`,`status`,`date_added` FROM cashback where coupon_id='$store_name' AND date_added BETWEEN '$start_date' AND '$end_date'";
					}
					if(($store_name =="All") && ($start_date1=='') && ($end_date1=='') && ($user!=''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`coupon_id`,`transaction_amount`,`status`,`date_added` FROM cashback where  user_id IN ($user)";
					}
					if(($store_name !="All") && ($start_date1=='') && ($end_date1=='') && ($user!=''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`coupon_id`,`transaction_amount`,`status`,`date_added` FROM cashback where coupon_id='$store_name' AND user_id IN ($user)";
					}	
					  	
					if(($store_name=="All") && ($start_date1!='') && ($end_date1!='') && ($user!=''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`coupon_id`,`transaction_amount`,`status`,`date_added` FROM  cashback  where user_id IN ($user) AND `date_added` BETWEEN '$start_date' AND '$end_date'";
					} 
					if(($store_name!="All") && ($start_date1!='') && ($end_date1!='') && ($user!=''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`coupon_id`,`transaction_amount`,`status`,`date_added` FROM  cashback  where coupon_id='$store_name' AND user_id IN ($user) AND `date_added` BETWEEN '$start_date' AND '$end_date'";
					} 
				 	 
					$result =$this->db->query("$selqry");
					//echo $this->db->last_query(); die;
					ob_end_clean();
					$data = $this->dbutil->csv_from_result($result, $delimiter, $newline, $enclosure);
    				force_download($filename, $data);


					//if($result->num_rows > 0)
					//{		
					//	return $result->result();
					//}
					//$i++;
				//} $i++;	
			}

			//END//

			//Missing cashback Menu //

			if($type == 'missing_cashback')
			{           	
            	$status      = $this->input->post('status');
	            //foreach($sep_user as $user)
				//{	
					if(($store_name =="All") && ($status =="All") && ($user =='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback"; 	 
					}
					 
					if(($store_name =="All") && ($status !="All") && ($user !='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where status ='$status' AND user_id IN ($user)"; 	 
					}
					if(($store_name =="All") && ($status =="All") && ($user =='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where status ='$status' AND trans_date BETWEEN  '$start_date' AND '$end_date'"; 	 
					}

					if(($store_name =="All") && ($status !="All") && ($user =='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where status ='$status'"; 	 
					}
					if(($store_name =="All") && ($status !="All") && ($user =='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where status ='$status' AND trans_date BETWEEN  '$start_date' AND '$end_date'"; 	 
					}
					if(($store_name =="All") && ($status !="All") && ($user !='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where status ='$status' AND user_id IN ($user) AND trans_date BETWEEN  '$start_date' AND '$end_date'"; 	 
					}
					if(($store_name =="All") && ($status =="All") && ($user !='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where user_id IN ($user)"; 	 
					}
					if(($store_name =="All") && ($status =="All") && ($start_date1!='') && ($end_date1!='') && ($user ==''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where trans_date BETWEEN  '$start_date' AND '$end_date'";
					}


					if(($store_name !="All") && ($status =="All") && ($user !='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where retailer_name= '$store_name' AND user_id IN ($user)";
					}
					if(($store_name !="All") && ($status =="All") && ($user =='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where retailer_name= '$store_name' AND trans_date BETWEEN  '$start_date' AND '$end_date'";
					}
					if(($store_name !="All") && ($status =="All") && ($user !='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where retailer_name= '$store_name' AND user_id IN ($user) AND trans_date BETWEEN  '$start_date' AND '$end_date'";
					}
					if(($store_name !="All") && ($status !="All") && ($user =='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where retailer_name= '$store_name' AND status = '$status' AND trans_date BETWEEN  '$start_date' AND '$end_date'";
					}



					if(($store_name !="All") && ($status =="All") && ($start_date1=='') && ($end_date1=='') && ($user ==''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where retailer_name= '$store_name'";
					}
					if(($store_name !="All") && ($status !="All") && ($start_date1=='') && ($end_date1=='') && ($user ==''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where retailer_name='$store_name' AND status = '$status'";
					}
					if(($store_name !="All") && ($status !="All") && ($start_date1=='') && ($end_date1=='') && ($user !=''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where user_id IN ($user) AND retailer_name='$store_name' AND status = '$status'";
					}
					if(($store_name !="All") && ($status !="All") && ($start_date1!='') && ($end_date1!='') && ($user !=''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where user_id IN ($user) AND retailer_name='$store_name' AND status = '$status' AND trans_date BETWEEN  '$start_date' AND '$end_date'";
					}
					if(($store_name =="All") && ($status =="All") && ($start_date1!='') && ($end_date1!='') && ($user !=''))
					{
						$selqry ="SELECT `cashback_id`,`user_id`,`trans_date`,`retailer_name`,`transaction_ref_id`,`transation_amount`,`coupon_code`,`cashback_details`,`status` FROM missing_cashback where user_id IN ($user) AND trans_date BETWEEN  '$start_date' AND '$end_date'";
					}
					 
					$result =$this->db->query("$selqry");
					ob_end_clean();
					$data = $this->dbutil->csv_from_result($result, $delimiter, $newline, $enclosure);
    				force_download($filename, $data);
					/*$result =$this->db->query("$selqry");
					if($result->num_rows > 0)
					{		
						return $result->result();
					}*/
					//$i++;
				//}	
			}	
			//END
			//Withdraw Menu //

			if($type == 'withdraws')
			{   
            	//echo "<pre>"; print_r($_POST); 
            	$status    = $this->input->post('status');
            	//New code for withdraw page add abankname details 7-5-16//
            	$bank_name = $this->input->post('bank_name');
            	//End//


            	//New code functionalities for withdraw export page 7-5-16//
            	
            	/*Status details 7-5-16*/

            		if(isset($status))
            		{
            			if($status == "All")
            			{
            				$new_status = "`status`!='' ";
            			}
            			else
            			{
            				$new_status = "`status` = '$status'";	
            			}
            		}

            	/*End Status details 7-5-16*/

            	/*Bank name details 7-5-16*/

            		if(isset($bank_name))
	            	{
	            		if($bank_name=="All")
	            		{
	            			$new_bank_name = "AND `bank_name`!=''";
	            		}
	            		else
	            		{
	            			if($new_status == '')
	            			{
	            				$new_bank_name = "AND `bank_name`!=''";	
	            			}
	            			else
	            			{
	            				$new_bank_name = "AND `bank_name` = '$bank_name'";	
	            			}
	            				
	            		}
	            	}

            	/*End bank name details 7-5-16*/

            	/*Add Date field details 7-5-16*/

            		if(isset($start_date1))
	            	{
	            		if($start_date1=="")
	            		{
	            			$new_start_date = "";
	            		}
	            		else
	            		{
	            			if(($new_status == '') && ($new_bank_name == ''))
	            			{
	            				$new_start_date = "`date_added` = '$start_date'";	
	            			}
	            			else
	            			{
	            				$new_start_date = "AND `date_added` = '$start_date'";	
	            			}
	            				
	            		}
	            	}
	            /*End Add Date field details 7-5-16*/

	            /*Close Date field details 7-5-16*/

	            	if(isset($end_date1))
            		{
	            		if($end_date1=="")
	            		{
	            			$new_end_date = "";
	            		}
	            		 
	            		else
	            		{
	            			if(($new_status == '') && ($new_bank_name == '') && ($new_start_date == ''))
	            			{
	            				 		
	            				$new_end_date = "`closing_date` = '$end_date'";
	 						}
	            			else
	            			{	
	            				$new_end_date = "AND `closing_date` = '$end_date'";
	            							
	            			}		
	            		}
	            	}

	            /*End Close Date field details 7-5-16*/

            		if(($new_status == '') && ($new_bank_name == '') && ($new_start_date == '') && ($new_end_date == ''))
	            	{
	            		$selqry ="SELECT `user_id`,`requested_amount`,`date_added`,`closing_date`,`account_holder`,`bank_name`,`branch_name`,`account_number`,`status` FROM withdraw";
	            	}
	            	else
	            	{
	            		$selqry ="SELECT `user_id`,`requested_amount`,`date_added`,`closing_date`,`account_holder`,`bank_name`,`branch_name`,`account_number`,`status` FROM withdraw  where $new_status $new_bank_name $new_start_date $new_end_date";
	            	}	
					 
					$result =$this->db->query("$selqry");
					ob_end_clean();
					$data = $this->dbutil->csv_from_result($result, $delimiter, $newline, $enclosure);
    				force_download($filename, $data); 
					/*$result =$this->db->query("$selqry");
					if($result->num_rows > 0)
					{		
						return $result->result();
					}*/
					//$i++;	
			}	

			//END
			//Payments Menu //

			if($type == 'payments')
			{ 
            	$status       = $this->input->post('status');
            	$trans_reason = $this->input->post('trans_reason');
            	$mode 		  = $this->input->post('mode');

            	//foreach($sep_user as $user)
				//{
					if(($status =="all") && ($mode =="all") && ($trans_reason =='') && ($start_date1=='') && ($end_date1=='') && ($user =='')) 
					{
						 $selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details";
					}
					if(($status =="all") && ($mode =="all") && ($user !='') && ($trans_reason =='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where user_id IN ($user)";
					}
					if(($status =="all") && ($mode =="all") && ($user =='') && ($trans_reason !='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where transation_reason ='$trans_reason'";
					}
					if(($status =="all") && ($mode =="all") && ($user =='') && ($trans_reason =='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where transation_date BETWEEN '$start_date' AND '$end_date'";
					}
					if(($status =="all") && ($mode =="all") && ($user !='') && ($trans_reason =='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where user_id IN ($user) AND transation_date BETWEEN '$start_date' AND '$end_date'";
					}
					if(($status =="all") && ($mode =="all") && ($user =='') && ($trans_reason !='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where transation_reason ='$trans_reason' AND transation_date BETWEEN '$start_date' AND '$end_date'";
					}
					if(($status =="all") && ($mode =="all") && ($user !='') && ($trans_reason !='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where user_id IN ($user) AND transation_reason ='$trans_reason'";
					}
					if(($status =="all") && ($mode =="all") && ($user !='') && ($trans_reason !='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where user_id IN ($user) AND transation_reason ='$trans_reason' AND transation_date BETWEEN '$start_date' AND '$end_date'";
					}
					if(($status =="all") && ($mode !="all") && ($user !='') && ($trans_reason !='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode ='$mode' AND transation_reason ='$trans_reason' AND transation_date BETWEEN '$start_date' AND '$end_date'";
					}


					if(($status =="all") && ($mode !="all") && ($user !='') && ($trans_reason =='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where user_id IN ($user) AND mode = '$mode' AND user_id = '$user'";
					}
					if(($status =="all") && ($mode !="all") && ($user =='') && ($trans_reason !='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode ='$mode' AND transation_reason ='$trans_reason'";
					}
					if(($status =="all") && ($mode !="all") && ($trans_reason =='') && ($start_date1=='') && ($end_date1=='') && ($user ==''))
					{
						 $selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode = '$mode'"; 	 
					}
					if(($status =="all") && ($mode !="all") && ($trans_reason =='') && ($start_date1!='') && ($end_date1!='') && ($user ==''))
					{
						 $selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode = '$mode' AND transation_date BETWEEN '$start_date' AND '$end_date' "; 	 
					}
					if(($status =="all") && ($mode !="all") && ($trans_reason =='') && ($start_date1!='') && ($end_date1!='') && ($user !=''))
					{
						 $selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode = '$mode' AND transation_date BETWEEN '$start_date' AND '$end_date' AND user_id IN ($user)"; 	 
					}
					if(($status =="all") && ($mode !="all") && ($trans_reason !='') && ($start_date1!='') && ($end_date1!='') && ($user ==''))
					{
						 $selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode = '$mode' AND transation_date BETWEEN '$start_date' AND '$end_date' AND transation_reason ='$trans_reason'"; 	 
					}
					if(($status =="all") && ($mode !="all") && ($trans_reason !='') && ($start_date1!='') && ($end_date1!='') && ($user !=''))
					{
						 $selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode = '$mode' AND transation_date BETWEEN '$start_date' AND '$end_date' AND transation_reason ='$trans_reason' AND user_id IN ($user)"; 	 
					}

					if(($status !="all") && ($mode =="all") && ($trans_reason !='') && ($start_date1=='') && ($end_date1=='') && ($user ==''))
					{
						 $selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where transation_status ='$status' AND transation_reason ='$trans_reason'"; 	 
					}



					if(($status !="all") && ($mode =="all") && ($user !='') && ($trans_reason =='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where user_id = '$user' AND transation_status ='$status' AND user_id IN ($user)";
					}
					if(($status !="all") && ($mode =="all") && ($user =='') && ($trans_reason =='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where transation_status ='$status'";
					}

					//if(($status !="all") && ($mode =="all") && ($user =='') && ($trans_reason !='') && ($start_date1=='') && ($end_date1==''))
					//{
					//	$selqry ="SELECT * FROM  transation_details where transation_reason ='$trans_reason' AND transation_status ='$status' AND user_id IN ($user)";
					//}
					if(($status !="all") && ($mode =="all") && ($user =='') && ($trans_reason =='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where transation_status ='$status' AND transation_date BETWEEN '$start_date' AND '$end_date'";
					}

					if(($status !="all") && ($mode =="all") && ($user !='') && ($trans_reason =='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where transation_status ='$status' AND transation_date BETWEEN '$start_date' AND '$end_date' AND user_id IN ($user)";
					}
					if(($status !="all") && ($mode =="all") && ($user =='') && ($trans_reason !='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where transation_status ='$status' AND transation_date BETWEEN '$start_date' AND '$end_date' AND transation_reason ='$trans_reason'";
					}
					if(($status !="all") && ($mode =="all") && ($user !='') && ($trans_reason !='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where transation_status ='$status' AND transation_date BETWEEN '$start_date' AND '$end_date' AND transation_reason ='$trans_reason' AND user_id IN ($user)";
					}
					

					if(($status !="all") && ($mode !="all") && ($trans_reason !='') && ($start_date1=='') && ($end_date1=='') && ($user ==''))
					{
						 $selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode = '$mode' AND transation_reason ='$trans_reason' AND transation_status ='$status'"; 	 
					}
					if(($status !="all") && ($mode =="all") && ($trans_reason !='') && ($start_date1=='') && ($end_date1=='') && ($user !=''))
					{
						 $selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode = '$mode' AND transation_reason ='$trans_reason' AND transation_status ='$status'"; 	 
					}
					if(($status !="all") && ($mode !="all") && ($trans_reason =='') && ($start_date1!='') && ($end_date1!='') && ($user !=''))
					{
						 $selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode = '$mode' AND transation_status ='$status' AND transation_date BETWEEN '$start_date' AND '$end_date' AND user_id IN ($user)"; 	 
					}
					if(($status !="all") && ($mode !="all") && ($trans_reason !='') && ($start_date1!='') && ($end_date1!='') && ($user ==''))
					{
						 $selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode = '$mode' AND transation_reason ='$trans_reason' AND transation_status ='$status' AND transation_date BETWEEN '$start_date' AND '$end_date'"; 	 
					}


					if(($status !="all") && ($mode !="all") && ($trans_reason =='') && ($start_date1!='') && ($end_date1!='') && ($user ==''))
					{
						 $selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode = '$mode' AND transation_status ='$status' AND transation_date BETWEEN '$start_date' AND '$end_date' "; 	 
					}
					if(($status !="all") && ($mode !="all") && ($trans_reason =='') && ($start_date1=='') && ($end_date1=='') && ($user ==''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode = '$mode' AND transation_status ='$status'";
					}
					if(($status !="all") && ($mode !="all") && ($user !='') && ($trans_reason =='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode = '$mode' AND transation_status ='$status' AND user_id IN ($user)";
					}
					if(($status !="all") && ($mode !="all") && ($user !='') && ($trans_reason !='') && ($start_date1=='') && ($end_date1==''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode = '$mode' AND transation_status ='$status' AND user_id IN ($user) AND transation_reason ='$trans_reason'";
					}
					if(($status !="all") && ($mode !="all") && ($user !='') && ($trans_reason !='') && ($start_date1!='') && ($end_date1!=''))
					{
						$selqry ="SELECT `transation_id`,`user_id`,`transation_reason`,`mode`,`transation_status`,`transation_date` FROM  transation_details where mode = '$mode' AND transation_status ='$status' AND user_id IN ($user) AND transation_reason ='$trans_reason' AND transation_date BETWEEN '$start_date' AND '$end_date'";
					}

					$result =$this->db->query("$selqry");
					ob_end_clean();
					$data = $this->dbutil->csv_from_result($result, $delimiter, $newline, $enclosure);
    				force_download($filename, $data);
					/*$result =$this->db->query("$selqry");
					if($result->num_rows > 0)
					{		
						return $result->result();
					}*/
					//$i++;
				//}		 	
			}	

			//END
			//Subscribers Menu //

			if($type == 'subscribers')
			{    
            	
            	$status       = $this->input->post('status');

					if(($status =="all") && ($start_date1 == "") && ($end_date1 == ""))
					{
						$selqry ="SELECT * FROM  subscribers";  	 
					}
					 
					if(($status!="all") && ($start_date1 == "") && ($end_date1 == ""))
					{
						$selqry ="SELECT * FROM  subscribers where subscriber_status = $status";  	 
					}
					if(($status!="all") && ($start_date1!= "") && ($end_date1!= ""))
					{	 
						$selqry ="SELECT * FROM  subscribers  where subscriber_status = $status AND date_subscribed BETWEEN  '$start_date' AND '$end_date'";
					}
					if(($status=="all") && ($start_date1!= "") && ($end_date1!= ""))
					{	 
						$selqry ="SELECT * FROM  subscribers  where date_subscribed BETWEEN  '$start_date' AND '$end_date'";
					}

					$result =$this->db->query("$selqry");
					ob_end_clean();
					$data = $this->dbutil->csv_from_result($result, $delimiter, $newline, $enclosure);
    				force_download($filename, $data);
					
					/*if($result->num_rows > 0)
					{		
						return $result->result();
					}*/			 
			}	
			//END

			//New code for userdetails menu 6-5-16//
			
			//User details Menu //

			if($type == 'users')
			{    
            	
            	$acc_status    = $this->input->post('acc_status');
            	$category_type = $this->input->post('category_type');
            	$login_app     = $this->input->post('login_app');
            	$bal_status    = $this->input->post('bal_status');
            	$news_status   = $this->input->post('news_status');
            	$ref_status    = $this->input->post('ref_status');
            	$bank_status   = $this->input->post('bank_status');

            	//echo "<pre>";print_r($_POST); exit;
            	/*Account status details 6-5-16*/
            	if(isset($acc_status))
            	{
            		if($acc_status == "All")
            		{
            			$new_acc_status = "";
            		}
            		else
            		{
            			$new_acc_status = " `status` = $acc_status";	
            		}
            	}
            	
            	/*Caetgory type details 6-5-16*/
            	if(isset($category_type))
            	{
            		if($category_type=="All")
            		{
            			$new_category_type = "";
            		}
            		else
            		{
            			if($new_acc_status == '')
            			{
            				$new_category_type = "`referral_category_type` = $category_type";	
            			}
            			else
            			{
            				$new_category_type = "AND `referral_category_type` = $category_type";	
            			}
            				
            		}
            	}

            	/*App login details 6-5-16*/
            	if(isset($login_app))
            	{
            		if($login_app=="All")
            		{
            			$new_login_app = "";
            		}
            		else
            		{
            			if(($new_acc_status == '') && ($new_category_type == ''))
            			{
            				$new_login_app = "`app_login` = $login_app";	
            			}
            			else
            			{
            				$new_login_app = "AND `app_login` = $login_app";	
            			}
            				
            		}
            	}

            	/*User account balance details 6-5-16*/
            	if(isset($bal_status))
            	{
            		if($bal_status=="All")
            		{
            			$new_bal_status = "";
            		}
            		 
            		else
            		{


            			if(($new_acc_status == '') && ($new_category_type == '') && ($new_login_app == ''))
            			{
            				if($bal_status=="2")
            				{
            					$new_bal_status = "`balance` != 0";
            				}
            				else
            				{ 
            					
            					$new_bal_status = "`balance` = $bal_status";
 							}
            			}
            			else
            			{	
            				if($bal_status=="2")
            				{
            					$new_bal_status = "AND `balance` != 0";
            				}
            				else
            				{
            					$new_bal_status = "AND `balance` = $bal_status";
            				}			
            			}
            				
            		}
            	}

            	/*New letter email notification details 6-5-16*/
            	if(isset($news_status))
            	{
            		if($news_status=="All")
            		{
            			$new_news_status = "";
            		}
            		else
            		{
            			if(($new_acc_status == '') && ($new_category_type == '') && ($new_login_app == '') && ($new_bal_status == ''))
            			{
            				$new_news_status = "`newsletter_mail` = $news_status";	
            			}
            			else
            			{
            				$new_news_status = "AND `newsletter_mail` = $news_status";	
            			}
            				
            		}
            	}

            	/*Referral details 6-5-16*/
            	if(isset($ref_status))
            	{
            		if($ref_status=="All")
            		{
            			$new_ref_status = "";
            		}
            		else
            		{
            			if(($new_acc_status == '') && ($new_category_type == '') && ($new_login_app == '') && ($new_bal_status == '') && ($new_news_status == ''))
            			{
            				if($ref_status == '1')
            				{
            					$new_ref_status = "`refer`!=0";	
            				}
            				if($ref_status == '0')
            				{
            					$new_ref_status = "`refer`=$ref_status";
            				}	
            					
            			}
            			else
            			{
            				if($ref_status == '1')
            				{
            					$new_ref_status = "AND `refer`!=0";	
            				}
            				if($ref_status == '0')
            				{
            					$new_ref_status = "AND `refer`=$ref_status";
            				}		
            			}
            				
            		}
            	}

            	/*Bank Information details 6-5-16*/
            	if(isset($bank_status))
            	{
            		if($bank_status=="All")
            		{
            			$new_bank_status = "";
            		}
            		else
            		{
            			if(($new_acc_status == '') && ($new_category_type == '') && ($new_login_app == '') && ($new_bal_status == '') && ($new_news_status == '') && ($new_ref_status == ''))
            			{
            				if($bank_status=="1")
            				{
            					$new_bank_status = "`bank_name` !=''";
            				}
            				else
            				{ 
            					
            					$new_bank_status = "`bank_name` = ''";
 							}	
            			}
            			else
            			{	
            				if($bank_status=="1")
            				{
            					$new_bank_status = " AND `bank_name` !=''";
            				}
            				else
            				{ 
            					$new_bank_status = "AND `bank_name` = ''";	
            				}
            			}
            				
            		}
            	}

            	if(($new_acc_status == '') && ($new_category_type == '') && ($new_login_app == '') && ($new_bal_status == '') && ($new_news_status == '') && ($new_ref_status == '') && ($new_bank_status == ''))
            	{
            		$selqry ="SELECT `user_id`,`first_name`,`last_name`,`email`,`status`,`referral_category_type`,`app_login`,`balance`,`newsletter_mail`,`refer`,`bank_name`,`account_number`,`ifsc_code` FROM tbl_users";
            	}
            	else
            	{
            		$selqry ="SELECT `user_id`,`first_name`,`last_name`,`email`,`status`,`referral_category_type`,`app_login`,`balance`,`newsletter_mail`,`refer`,`bank_name`,`account_number`,`ifsc_code` FROM tbl_users  where $new_acc_status $new_category_type $new_login_app $new_bal_status $new_news_status $new_ref_status $new_bank_status";
            	}	
            		 	 
				$result =$this->db->query("$selqry");
				ob_end_clean();
				$data = $this->dbutil->csv_from_result($result, $delimiter, $newline, $enclosure);
    			force_download($filename, $data);
							 
			}	
			//END 
			//End user details 6-5-16//
		
	}

	//END//

	///New code for report update page//

	function update_reports($updatereport,$type)
	{
		//echo $updatereport; exit;

		$this->db->connection_check();
		$coupon_type = '';	
		$this->load->library('CSVReader');	
		$main_url = 'uploads/updatereports/'.$updatereport;	
		$result =   $this->csvreader->parse_file($main_url);	
		$name = $this->db->query("select * from admin")->row();	
		//$ref_cashbcak_percent =  $name->referral_cashback;	
		if(count($result)!=0)	
		{
			$s =1;
		//echo $type;	exit;
					
		if($type == 'pending_cashback')
		{ 
			foreach($result as $res)	
			{	
				$user_id 		  = $res['user_id'];	
				$coupon_id  	  = $res['coupon_id'];	
				$status 		  = $res['status'];	
				$report_update_id = $res['report_update_id'];
				 	
			           	
				$data = array(
								
								'coupon_id'=>$coupon_id,
								'status'=>$status,
								'user_id'=>$user_id
							);

				$this->db->where('report_update_id',$report_update_id);
				$updation = $this->db->update('cashback',$data);
				//echo $user_id; 
				
				//print_r($getuser);
			if($status == 'Completed')
			{
				$getuser_mail    = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('email');
				$username        = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('first_name');
				$cashback_amount = $this->db->get_where('cashback',array('user_id'=>$user_id))->row('cashback_amount');
				$cashback_status = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('cashback_mail');

				/*mail for Completed pending cashback */
			 	$this->db->where('admin_id',1);
				$admin_det = $this->db->get('admin');
				if($admin_det->num_rows >0) 
				{    
					$admin = $admin_det->row();
					$admin_email = $admin->admin_email;
					$site_name = $admin->site_name;
					$admin_no = $admin->contact_number;
					$site_logo = $admin->site_logo;
				}
				
				$date =date('Y-m-d');
				if($cashback_status == 1)
				{
					$this->db->where('mail_id',8);
					$mail_template = $this->db->get('tbl_mailtemplates');
					if($mail_template->num_rows >0) 
					{
					   $fetch 		 = $mail_template->row();
					   $subject 	 = $fetch->email_subject;
					   $templete 	 = $fetch->email_template;
					   $url 		 = base_url().'cashback/my_earnings/';
					   $unsuburl	 = base_url().'cashback/un_subscribe/cashback/'.$user_id;
					   $myaccount    = base_url().'cashback/minha_conta';
					   
						$this->load->library('email');
						
						$config = Array(
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
						);
						
						$sub_data = array(
							'###SITENAME###'=>$site_name
						);
						$subject_new = strtr($subject,$sub_data);
						
						// $this->email->initialize($config);
						 $this->email->set_newline("\r\n");
						   $this->email->initialize($config);
						   $this->email->from($admin_email,$site_name.'!');
						   $this->email->to($getuser_mail);
						   $this->email->subject($subject_new);
					   
						$data = array(
							'###NAME###'=>$username,
							'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
							'###SITENAME###'=>$site_name,
							'###ADMINNO###'=>$admin_no,
							'###DATE###'=>$date,
							'###AMOUNT###'=>str_replace('.', ',', $cashback_amount),
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
					    );
					   
					   $content_pop=strtr($templete,$data);
					   // echo $content_pop; echo $subject_new;
					   $this->email->message($content_pop);
					   $this->email->send();  
					}
				}	
				/* mail for pending cashback */
			}
			
			if($status == 'Pending')
			{	
				$getuser_mail    = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('email');
				$username        = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('username');
				$cashback_amount = $this->db->get_where('cashback',array('user_id'=>$user_id))->row('cashback_amount');
				$cashback_status = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('cashback_mail');
				/* mail for pending cashback */
				 
				$this->db->where('admin_id',1);
				$admin_det = $this->db->get('admin');
				if($admin_det->num_rows >0) 
				{    
					$admin = $admin_det->row();
					$admin_email = $admin->admin_email;
					$site_name = $admin->site_name;
					$admin_no = $admin->contact_number;
					$site_logo = $admin->site_logo;
				}
				
				$date =date('Y-m-d');
				
				if($cashback_status == 1)
				{

					$this->db->where('mail_id',10);
					$mail_template = $this->db->get('tbl_mailtemplates');
					if($mail_template->num_rows >0) 
					{
					   $fetch = $mail_template->row();
					   $subject = $fetch->email_subject;
					   $templete = $fetch->email_template;
					   $unsuburl	 = base_url().'cashback/un_subscribe/cashback/'.$user_id;
					   $myaccount    = base_url().'cashback/minha_conta';
					   // $url = base_url().'cashback/my_earnings/';
					   
						$this->load->library('email');
						
						$config = Array(
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
						);
						
						$sub_data = array(
							'###SITENAME###'=>$site_name
						);
						$subject_new = strtr($subject,$sub_data);
						
						// $this->email->initialize($config);
						 $this->email->set_newline("\r\n");
						   $this->email->initialize($config);
						   $this->email->from($admin_email,$site_name.'!');
						   $this->email->to($getuser_mail);
						   $this->email->subject($subject_new);
					   
						$data = array(
							'###NAME###'=>$username,
							'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
							'###SITENAME###'=>$site_name,
							'###ADMINNO###'=>$admin_no,
							'###DATE###'=>$date,
							'###AMOUNT###'=>str_replace('.', ',', $cashback_amount),
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
					    );
					   
					   $content_pop=strtr($templete,$data);
					   // echo $content_pop; echo $subject_new;
					   $this->email->message($content_pop);
					   $this->email->send();  
					
					}
				}	
				/* mail for pending cashback */

			}

			if($status == 'Canceled')
			{	
				$getuser_mail    = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('email');
				$username        = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('username');
				$cashback_status = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('cashback_mail');
				$cashback_amount = $this->db->get_where('cashback',array('user_id'=>$user_id))->row('cashback_amount');

				/* mail for pending cashback */
				 
				$this->db->where('admin_id',1);
				$admin_det = $this->db->get('admin');
				if($admin_det->num_rows >0) 
				{    
					$admin = $admin_det->row();
					$admin_email = $admin->admin_email;
					$site_name = $admin->site_name;
					$admin_no = $admin->contact_number;
					$site_logo = $admin->site_logo;
				}
				
				$date =date('Y-m-d');
				
				if($cashback_status == 1)
				{

					$this->db->where('mail_id',11);
					$mail_template = $this->db->get('tbl_mailtemplates');
					if($mail_template->num_rows >0) 
					{
					   $fetch = $mail_template->row();
					   $subject = $fetch->email_subject;
					   $templete = $fetch->email_template;
					   $unsuburl	 = base_url().'cashback/un_subscribe/cashback/'.$user_id;
					   $myaccount    = base_url().'cashback/minha_conta';
					   // $url = base_url().'cashback/my_earnings/';
					   
						$this->load->library('email');
						
						$config = Array(
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
						);
						
						$sub_data = array(
							'###SITENAME###'=>$site_name
						);
						$subject_new = strtr($subject,$sub_data);
						
						// $this->email->initialize($config);
						 $this->email->set_newline("\r\n");
						   $this->email->initialize($config);
						   $this->email->from($admin_email,$site_name.'!');
						   $this->email->to($getuser_mail);
						   $this->email->subject($subject_new);
					   
						$data = array(
							'###NAME###'=>$username,
							'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
							'###SITENAME###'=>$site_name,
							'###ADMINNO###'=>$admin_no,
							'###DATE###'=>$date,
							'###AMOUNT###'=>str_replace('.', ',', $cashback_amount),
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
					    );
					   
					   $content_pop=strtr($templete,$data);
					   // echo $content_pop; echo $subject_new;
					   $this->email->message($content_pop);
					   $this->email->send();  
					
					}
				}	
				/* mail for pending cashback */

			}


			} 
		}	
		if($type == 'missing_cashback')
		{ 	
			foreach($result as $res)	
			{	
				$user_id 		  = $res['user_id'];	
				$cashback_id  	  = $res['cashback_id'];	
				$retailer_name	  = $res['retailer_name'];	
				$status 		  = $res['status'];
				 	  
				$data = array(
								
								'retailer_name'=>$retailer_name,
								'status'=>$status,
								'user_id'=>$user_id
							);

				$this->db->where('cashback_id',$cashback_id);
				$updation = $this->db->update('missing_cashback',$data);
			}	
				
		}

		if($type == 'withdraws')
		{ 	
			foreach($result as $res)	
			{	
				$user_id 		  = $res['user_id'];	
				$withdraw_id  	  = $res['withdraw_id'];	
				$requested_amount = $res['requested_amount'];	
				$status 		  = $res['status'];
				 	  
				$data = array(
								
								'requested_amount'=>$requested_amount,
								'status'=>$status,
								'user_id'=>$user_id
							);

				$this->db->where('withdraw_id',$withdraw_id);
				$updation = $this->db->update('withdraw',$data);
			}	
				
		}

		if($type == 'payments')
		{ 	
			foreach($result as $res)	
			{	
				$user_id 		  = $res['user_id'];	
				$trans_id  	  	  = $res['trans_id'];	
				$trans_reason	  = $res['transation_reason'];	
				$trans_status 	  = $res['transation_status'];
				$report_update_id = $res['report_update_id'];
				 	  
				$data = array(
								
								//'trans_id'=>$trans_id,
								'user_id'=>$user_id,
								'transation_reason'=>$trans_reason,
								'transation_status'=>$trans_status
							);

				$this->db->where('report_update_id',$report_update_id);
				//print_r($data); exit;
				$updation = $this->db->update('transation_details',$data);
			}	
				
		}
				$s++;
		}	
		return true;
	}
	//End//
	function download_free_coupons()
	{
			$this->db->connection_check();
			 $selqry="SELECT * FROM  coupons  order by coupon_id desc";  
			 $result=$this->db->query("$selqry"); 
				if($result->num_rows > 0)
				{		
					return $result->result();
				}
	}
	function cashback_details_cb($cateid)
	{
		$this->db->connection_check();
		$this->db->order_by('cbid ','desc');
		$this->db->where('store_id',$cateid);
			$result = $this->db->get('category_cashback');
			
			if($result->num_rows > 0){
				return $result->result();
			}
			return false;
			
	}
	function cashback_details_byid($cashbackid)
	{
		$this->db->connection_check();
		$this->db->where('cbid',$cashbackid);
			$result = $this->db->get('category_cashback');
			
			if($result->num_rows > 0){
				return $result->row();
			}
			return false;
			
	}
	function get_typehead_citys_list($query)
	{
		$this->db->connection_check();
		$this->db->like('city_name', $query);	
		$query = $this->db->get('citys');
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	//
	function show_list(){
		$this->db->connection_check();
		$this->db->where('click_id','203');
		$query = $this->db->get('click_history');
		//print_r($query->row());exit;
		return $query->result();
	}
	/*Seetha 24/10/15 */
	function pending_cashback(){
		$this->db->connection_check();
		$this->db->where('status','Pending');
		$this->db->order_by('cashback_id','desc');
		$result = $this->db->get('cashback');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	function approve_cashback($cashback_id)
	{

		$this->db->connection_check();		
		$this->db->where('cashback_id',$cashback_id);
		$cb = $this->db->get('cashback');
		$user_id ='';
		if($cb)
		{
			
			$cb_r = $cb->row();
			$user_id = $cb_r->user_id;
			$cashback_amount = $cb_r->cashback_amount;
			$transaction_date = $cb_r->transaction_date;
            $txn_id = $cb_r->txn_id;
		
			$user = $this->view_user($user_id);
			$newid               = rand(1000,9999);
			$newtransaction_id   = md5($newid);

			if($user)
			{
				foreach($user as $single)
				{
					$balance = $single->balance;
					$user_email = $single->email;
					$user_name = $single->first_name.' '.$single->last_name;	
				}
				
				$this->db->where('user_id',$user_id);
				$this->db->update('tbl_users',array('balance'=>$balance+$cashback_amount));
				$ins_data = array('user_id'=>$user_id,'transation_id'=>$txn_id,'transation_amount'=>$cashback_amount,'mode'=>'Credited','transaction_date'=>$transaction_date,'transation_date'=>date('Y-m-d'),'transation_status'=>'Paid','transation_reason'=>'Cashback','report_update_id'=>$newtransaction_id);
				$this->db->insert('transation_details',$ins_data);
				
				$data = array('status'=>'Completed');
				$this->db->where('cashback_id',$cashback_id);
				$this->db->update('cashback',$data);
				
				/* mail for pending cashback */
				$this->db->where('admin_id',1);
				$admin_det = $this->db->get('admin');
				if($admin_det->num_rows >0) 
				{    
					$admin = $admin_det->row();
					$admin_email = $admin->admin_email;
					$site_name = $admin->site_name;
					$admin_no = $admin->contact_number;
					$site_logo = $admin->site_logo;
				}
				
				$date =date('Y-m-d');
				
				//Check a condition for email notification//
				if($single->cashback_mail == 1)
				{	

					$this->db->where('mail_id',8);
					$mail_template = $this->db->get('tbl_mailtemplates');
					if($mail_template->num_rows >0) 
					{
					   $fetch = $mail_template->row();
					   $subject = $fetch->email_subject;
					   $templete = $fetch->email_template;
					   $url = base_url().'cashback/my_earnings/';
					   $unsuburl	 = base_url().'cashback/un_subscribe/cashback/'.$user_id;
					   $myaccount    = base_url().'cashback/minha_conta';
					   
						$this->load->library('email');
						
						$config = Array(
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
						);
						
						$sub_data = array(
							'###SITENAME###'=>$site_name
						);
						$subject_new = strtr($subject,$sub_data);
						
						// $this->email->initialize($config);
						 $this->email->set_newline("\r\n");
						   $this->email->initialize($config);
						   $this->email->from($admin_email, $site_name.'!');
						   $this->email->to($user_email);
						   $this->email->subject($subject_new);
					   
						$data = array(
							'###NAME###'=>$user_name,
							'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
							'###SITENAME###'=>$site_name,
							'###ADMINNO###'=>$admin_no,
							'###DATE###'=>$date,
							'###AMOUNT###'=>str_replace('.', ',', $cashback_amount),
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

					    );
					   
					   $content_pop=strtr($templete,$data);
					   $this->email->message($content_pop);
					   $this->email->send();  
					}
				}	
				
				/* mail for pending cashback */

				/*approve pending referral */
				$this->db->where('cashback_id',$cashback_id);
				$cashbacks = $this->db->get('cashback');
				$cashback_data = $cashbacks->row();
				if($cashback_data->referral!=0)
				{
					$this->db->where('trans_id',$cashback_data->txn_id);
					$txn = $this->db->get('transation_details');
					$txn_detail = $txn->row();
					if($txn_detail)
					{
						$txn_id = $txn_detail->trans_id;
						$ref_user_id = $txn_detail->user_id;
						$transation_amount = $txn_detail->transation_amount;
						$refer_user = $this->view_user($ref_user_id);
						if($refer_user)
						{
							foreach($refer_user as $single)
							{
								$referral_balance = $single->balance;
								$user_email = $single->email;
								$user_name = $single->first_name.' '.$single->last_name;
							}
							$this->db->where('user_id',$ref_user_id);
							$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amount));
							
							$data = array('transation_status'=>'Approved','transaction_date'=>$date,'transation_reason'=>'Referal Payment');
							$this->db->where('transation_reason','Pending Referal Payment');
							$this->db->where('trans_id',$txn_id);
							$this->db->update('transation_details',$data);
							/* mail for pending referral */
							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name = $admin->site_name;
								$admin_no = $admin->contact_number;
								$site_logo = $admin->site_logo;
							}
							
							$date =date('Y-m-d');

							if($single->referral_mail == 1)
							{	
								$this->db->where('mail_id',9);
								$mail_template = $this->db->get('tbl_mailtemplates');
								if($mail_template->num_rows >0) 
								{
								   $fetch = $mail_template->row();
								   $subject = $fetch->email_subject;
								   $templete = $fetch->email_template;
								   $url = base_url().'cashback/my_earnings/';
								   $unsuburl	 = base_url().'cashback/un_subscribe/referral/'.$user_id;
					   			   $myaccount    = base_url().'cashback/minha_conta';
								   
									$this->load->library('email');
									
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);
									
									$sub_data = array(
										'###SITENAME###'=>$site_name
									);
									$subject_new = strtr($subject,$sub_data);
									
									// $this->email->initialize($config);
									 $this->email->set_newline("\r\n");
									   $this->email->initialize($config);
									   $this->email->from($admin_email,$site_name.'!');
									   $this->email->to($user_email);
									   $this->email->subject($subject_new);
								   
									$data = array(
										'###NAME###'=>$user_name,
										'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###AMOUNT###'=>str_replace('.', ',', $transation_amount),
										'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
										'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
									);
								   
								   $content_pop=strtr($templete,$data);
								  // echo $subject_new;  echo $content_pop;exit;
								   $this->email->message($content_pop);
								   $this->email->send();  
								}
							}	
								/* mail for pending referral */
						}
					}
				}
				 
				if($ref_user_id!='')
				{	
					$this->db->where('cashback_id',$cashback_id);
					$cashbacks = $this->db->get('cashback');
					$cashback_data = $cashbacks->row();

					if($cashback_data->referral!=0)
					{
							$this->db->where('new_txn_id',$cashback_data->new_txn_id);
							$txn = $this->db->get('transation_details');
							$txn_detail = $txn->row();
						
						if($txn_detail)
						{
							
							$new_txn_ids = $txn_detail->new_txn_id;
							if($single->referral_category_type == 1)
							{	
								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryone'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

								//3** Bonus by Refferal Rate type//
								if($bonus_by_ref_rate == 1)
								{
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Referal Payment' AND user_id=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{
										$fetch = $query->row();
										$usercount = $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categoryone'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;
										if($usercount == $friends_count)
										{	
										if($bonus_amount!='')
										{	 
											$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category One User' AND `user_id`=$ref_user_id"; 
							 				$query1=$this->db->query("$selqry");
							 				$newnumrows = $query1->num_rows();
											if($newnumrows > 0)
											{
												$fetch = $query1->row();
												$users_count = $fetch->userid;
												if($users_count == 0)	
												{	
													$data = array(			
													'transation_amount' => $bonus_amount,	
													'user_id' => $ref_user_id,	
													'transation_date' => $now,
													'transaction_date' => $now,
													'transation_id'=>$n12,	
													'transation_reason' => 'Referral Bonus for Category One User',	
													'mode' => 'Credited',
													'details_id'=>'',	
													'table'=>'',	
													'new_txn_id'=>0,
													'transation_status ' => 'Approved',
													'report_update_id'=>$newtransaction_id
													);	
													$this->db->insert('transation_details',$data);
												}	
											}
										}	
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//
								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{

									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount');
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}	
							if($single->referral_category_type == 2)
							{	
							 	
								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categorytwo'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

								//3** Bonus by Refferal Rate type//
								if($bonus_by_ref_rate == 1)
								{
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$newtransaction_id   = rand(1000,9999);
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{
										$fetch 			= $query->row(); 
										$usercount 		= $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categorytwo'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;	
										if($usercount == $friends_count)
										{
											if($bonus_amount!='')
											{
												$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category Two User' AND `user_id`=$ref_user_id"; 
								 				$query1=$this->db->query("$selqry");
								 				$newnumrows = $query1->num_rows();
												if($newnumrows > 0)
												{
													$fetch = $query1->row();
													$users_count = $fetch->userid;
													if($users_count == 0)	
													{
														$data = array(			
														'transation_amount' => $bonus_amount,	
														'user_id' => $ref_user_id,	
														'transation_date' => $now,
														'transaction_date' => $now,
														'transation_id'=>$n12,	
														'transation_reason' => 'Referral Bonus for Category Two User',	
														'mode' => 'Credited',
														'details_id'=>'',	
														'table'=>'',	
														'new_txn_id'=>0,
														'transation_status ' => 'Approved',
														'report_update_id'=>$newtransaction_id
														);
														$this->db->insert('transation_details',$data);
													}
												}
											}		
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//
								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{
									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount');
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}
							if($single->referral_category_type == 3)
							{	
							 
								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

								//3** Bonus by Refferal Rate type//

								if($bonus_by_ref_rate == 1)
								{
									
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$newtransaction_id   = rand(1000,9999);
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{
										$fetch = $query->row();
										$usercount = $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;
											
										if($usercount == $friends_count)
										{
											if($bonus_amount!='')
											{
												$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category Three User' AND `user_id`=$ref_user_id"; 
								 				$query1=$this->db->query("$selqry");
								 				$newnumrows = $query1->num_rows();
												if($newnumrows > 0)
												{
													$fetch = $query1->row();
													$users_count = $fetch->userid;
													if($users_count == 0)	
													{
														
														$data = array(			
														'transation_amount' => $bonus_amount,	
														'user_id' => $ref_user_id,	
														'transation_date' => $now,
														'transaction_date' => $now,
														'transation_id'=>$n12,	
														'transation_reason' => 'Referral Bonus for Category Three User',	
														'mode' => 'Credited',
														'details_id'=>'',	
														'table'=>'',	
														'new_txn_id'=>0,
														'transation_status ' => 'Approved',
														'report_update_id'=>$newtransaction_id
														);
													 	$this->db->insert('transation_details',$data);
													}
												}
											}		
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//

								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{

									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount');
									//$this->db->where('transation_reason','Referral Cashback amount');
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}
							if($single->referral_category_type == 4)
							{   
								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryfour'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;
								$bonus_amount   	= $referrals->ref_cashback_rate_bonus;
								$friends_count  	= $referrals->friends_count;

								//3** Bonus by Refferal Rate type//

								if($bonus_by_ref_rate == 1)
								{
									
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$newtransaction_id   = rand(1000,9999);
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referal Payment' AND `user_id`=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{

										$fetch 			= $query->row();
										$usercount 		= $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;

										if($usercount == $friends_count)
										{
											if($bonus_amount!='')
											{
												$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category Four User' AND `user_id`=$ref_user_id"; 
								 				$query1=$this->db->query("$selqry");
								 				$newnumrows = $query1->num_rows();
												if($newnumrows > 0)
												{
													$fetch = $query1->row();
													$users_count = $fetch->userid;
													if($users_count == 0)	
													{
											
														$data = array(			
														'transation_amount' => $bonus_amount,	
														'user_id' => $ref_user_id,	
														'transation_date' => $now,
														'transaction_date' => $now,
														'transation_id'=>$n12,	
														'transation_reason' => 'Referral Bonus for Category Four User',	
														'mode' => 'Credited',
														'details_id'=>'',	
														'table'=>'',	
														'new_txn_id'=>0,
														'transation_status ' => 'Approved',
														'report_update_id'=>$newtransaction_id
														);
												 
														$this->db->insert('transation_details',$data);
													}
												}
											}		
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//

								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{

									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount');
									//$this->db->where('transation_reason','Referral Cashback amount');
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}
							if($single->referral_category_type == 5)
							{	

								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryfive'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

								//3** Bonus by Refferal Rate type//

								if($bonus_by_ref_rate == 1)
								{
									
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$newtransaction_id   = rand(1000,9999);
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{
										$fetch = $query->row();
										$usercount = $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categoryfive'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;
											
										if($usercount == $friends_count)
										{
											if($bonus_amount!='')
											{
												$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category five User' AND `user_id`=$ref_user_id"; 
								 				$query1=$this->db->query("$selqry");
								 				$newnumrows = $query1->num_rows();
												if($newnumrows > 0)
												{
													$fetch = $query1->row();
													$users_count = $fetch->userid;
													if($users_count == 0)	
													{
													
														$data = array(			
														'transation_amount' => $bonus_amount,	
														'user_id' => $ref_user_id,	
														'transation_date' => $now,
														'transaction_date' => $now,
														'transation_id'=>$n12,	
														'transation_reason' => 'Referral Bonus for Category five User',	
														'mode' => 'Credited',
														'details_id'=>'',	
														'table'=>'',	
														'new_txn_id'=>0,
														'transation_status ' => 'Approved',
														'report_update_id'=>$newtransaction_id
														);
														$this->db->insert('transation_details',$data);
													}
												}
											}		
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//

								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{

									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount');
									//$this->db->where('transation_reason','Referral Cashback amount');
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}
						}
					}		
				}
				
				//Pilaventhiran 04/05/2016 START

				
				$this->db->where('admin_id',1);
				$Admin_Details_Query    = $this->db->get('admin');
				$Admin_Details 		    = $Admin_Details_Query->row();
				$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
				$Total_Amount 			= ($balance+$cashback_amount);
				$Site_Logo 				= $Admin_Details->site_logo;
				//echo "userid".$user_id; echo "<br>";
				$User_details 			= $this->admin_model->view_user($user_id);
				//echo "<pre>"; print_r($User_details); exit;
				$us_email 				= $User_details->email;
				$unsuburl	 			= base_url().'cashback/un_subscribe/referral/'.$user_id;
				$myaccount    			= base_url().'cashback/minha_conta';

				if($Total_Amount>=$Admin_Minimum_Cashback)
				{

					$mail_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'")->row();
					if($mail_temp->num_rows>0)
					{
						$fe_cont = $mail_temp->email_template;			
						$servername = base_url();
						$nows = date('Y-m-d');	
						$this->load->library('email');
						$gd_api=array(
							
							'###NAME###'=>$username,
							'###AMOUNT###'=>str_replace('.', ',', $Total_Amount),
							'###REQUEST_WITHDRAW###'=>str_replace('.', ',', $Admin_Minimum_Cashback),
							'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
							);
										   
						$gd_message=strtr($fe_cont,$gd_api);
						$config = Array(
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
						);

						$list = array($us_email);
						$this->email->initialize($config);
						$this->email->set_newline("\r\n");
						$this->email->from($admin_emailid,$site_name.'!');
						$this->email->to($list);
						$this->email->subject($subject);
						$this->email->message($gd_message);
						$this->email->send();
						$this->email->print_debugger();
					}
				}
//Pilaventhiran 04/05/2016 END
				 
			}
			return true;
		}
		return false;
	}

	//New code for Approve multiple cashback records 6/4/16//

	function approve_multi_cashbacks()
	{
		
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		$a1=0;
		foreach($sort_order as $key=>$val)
		{
			$a1++;
			$cashback_id = $key;	
			$this->db->where('cashback_id',$cashback_id);
			$cb = $this->db->get('cashback');
			//echo "<pre>";print_r($cb);
			$user_id ='';
			if($cb)
			{
			$cb_r = $cb->row();
			$user_id = $cb_r->user_id;
			$cashback_amount = $cb_r->cashback_amount;
			$transaction_date = $cb_r->transaction_date;
            $txn_id = $cb_r->txn_id;
		 	$user = $this->view_user($user_id);
			$newid               = rand(1000,9999);
			$newtransaction_id   = md5($newid);
			if($user)
			{
				foreach($user as $single)
				{
					$balance = $single->balance;
					$user_email = $single->email;
					$user_name = $single->first_name.' '.$single->last_name;
				}
				$this->db->where('user_id',$user_id);
				$this->db->update('tbl_users',array('balance'=>$balance+$cashback_amount));
				$ins_data = array('user_id'=>$user_id,'transation_id'=>$txn_id,'transation_amount'=>$cashback_amount,'mode'=>'Credited','transaction_date'=>$transaction_date,'transation_date'=>date('Y-m-d'),'transation_status'=>'Paid','transation_reason'=>'Cashback','report_update_id'=>$newtransaction_id);
			
				$this->db->insert('transation_details',$ins_data);
				//$this->db->update('transation_details',$ins_data);
			
				$data = array('status'=>'Completed');
				$this->db->where('cashback_id',$cashback_id);
				$this->db->update('cashback',$data);
				
				/* mail for pending cashback */
				$this->db->where('admin_id',1);
				$admin_det = $this->db->get('admin');
				if($admin_det->num_rows >0) 
				{    
					$admin = $admin_det->row();
					$admin_email = $admin->admin_email;
					$site_name = $admin->site_name;
					$admin_no = $admin->contact_number;
					$site_logo = $admin->site_logo;
				}
				
				$date =date('Y-m-d');
				
				//Check a condition for email notification//
				if($single->cashback_mail == 1)
				{	

					$this->db->where('mail_id',8);
					$mail_template = $this->db->get('tbl_mailtemplates');
					if($mail_template->num_rows >0) 
					{
					   $fetch = $mail_template->row();
					   $subject = $fetch->email_subject;
					   $templete = $fetch->email_template;
					   $url = base_url().'cashback/my_earnings/';
					   $unsuburl	 = base_url().'cashback/un_subscribe/cashback/'.$user_id;
					   $myaccount    = base_url().'cashback/minha_conta';
					   
						$this->load->library('email');
						
						$config = Array(
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
						);
						
						$sub_data = array(
							'###SITENAME###'=>$site_name
						);
						$subject_new = strtr($subject,$sub_data);
						
						// $this->email->initialize($config);
						 $this->email->set_newline("\r\n");
						   $this->email->initialize($config);
						   $this->email->from($admin_email,$site_name.'!');
						   $this->email->to($user_email);
						   $this->email->subject($subject_new);
					   
						$data = array(
							'###NAME###'=>$user_name,
							'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
							'###SITENAME###'=>$site_name,
							'###ADMINNO###'=>$admin_no,
							'###DATE###'=>$date,
							'###AMOUNT###'=>str_replace('.', ',', $cashback_amount),
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

					    );
					   
					   $content_pop=strtr($templete,$data);
					   // echo $content_pop; echo $subject_new;
					   $this->email->message($content_pop);
					   $this->email->send();  
					}
				}	
				/* mail for pending cashback */

				/*approve pending referral */
				$this->db->where('cashback_id',$cashback_id);
				$cashbacks = $this->db->get('cashback');
				$cashback_data = $cashbacks->row();
				if($cashback_data->referral!=0){
					$this->db->where('trans_id',$cashback_data->txn_id);
					$txn = $this->db->get('transation_details');
					$txn_detail = $txn->row();
					if($txn_detail){
						$txn_id = $txn_detail->trans_id;
						$ref_user_id = $txn_detail->user_id;
						$transation_amount = $txn_detail->transation_amount;
						$refer_user = $this->view_user($ref_user_id);
						if($refer_user){
							foreach($refer_user as $single){
								$referral_balance = $single->balance;
								$user_email = $single->email;
								$user_name = $single->first_name.' '.$single->last_name;
							}
							$this->db->where('user_id',$ref_user_id);
							$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amount));
							
							$data = array('transation_status'=>'Approved','transaction_date'=>$date,'transation_reason'=>'Referal Payment');
							$this->db->where('transation_reason','Pending Referal Payment');
							$this->db->where('trans_id',$txn_id);
							$this->db->update('transation_details',$data);
							/* mail for pending referral */
							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name = $admin->site_name;
								$admin_no = $admin->contact_number;
								$site_logo = $admin->site_logo;
							}
							
							$date =date('Y-m-d');

						if($single->referral_mail == 1)
						{	
							$this->db->where('mail_id',9);
							$mail_template = $this->db->get('tbl_mailtemplates');
							if($mail_template->num_rows >0) 
							{
							   $fetch = $mail_template->row();
							   $subject = $fetch->email_subject;
							   $templete = $fetch->email_template;
							   $url = base_url().'cashback/my_earnings/';
							   $unsuburl	 = base_url().'cashback/un_subscribe/referral/'.$user_id;
				   			   $myaccount    = base_url().'cashback/minha_conta';
							   
								$this->load->library('email');
								
								$config = Array(
									'mailtype'  => 'html',
									'charset'   => 'utf-8',
								);
								
								$sub_data = array(
									'###SITENAME###'=>$site_name
								);
								$subject_new = strtr($subject,$sub_data);
								
								// $this->email->initialize($config);
								 $this->email->set_newline("\r\n");
								   $this->email->initialize($config);
								   $this->email->from($admin_email,$site_name.'!');
								   $this->email->to($user_email);
								   $this->email->subject($subject_new);
							   
								$data = array(
									'###NAME###'=>$user_name,
									'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
									'###SITENAME###'=>$site_name,
									'###ADMINNO###'=>$admin_no,
									'###DATE###'=>$date,
									'###AMOUNT###'=>str_replace('.', ',', $transation_amount),
									'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
									'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
								);
							   
							   $content_pop=strtr($templete,$data);
							  // echo $subject_new;  echo $content_pop;exit;
							   $this->email->message($content_pop);
							   $this->email->send();  
							}
						}	
							/* mail for pending referral */
						}
					}
				}
				 
				if($ref_user_id!='')
				{	
					$this->db->where('cashback_id',$cashback_id);
					$cashbacks = $this->db->get('cashback');
					$cashback_data = $cashbacks->row();
				if($cashback_data->referral!=0)
				{
						$this->db->where('new_txn_id',$cashback_data->new_txn_id);
						$txn = $this->db->get('transation_details');
						$txn_detail = $txn->row();
					
					if($txn_detail)
					{
						
						$new_txn_ids = $txn_detail->new_txn_id;
						
						if($single->referral_category_type == 1)
						{	
							$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryone'")->row();	
							$ref_by_percentage  = $referrals->ref_by_percentage;
							$ref_by_rate 		= $referrals->ref_by_rate;
							$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

							//3** Bonus by Refferal Rate type//

							if($bonus_by_ref_rate == 1)
							{
								
								$n9  = '333445';
								$n12 = $n9 + $ref_user_id;
								$now = date('Y-m-d H:i:s');	
								
								$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Referal Payment' AND user_id=$ref_user_id"; 
						 		$query=$this->db->query("$selqry");
								$numrows = $query->num_rows();
								if($numrows > 0)
								{
									$fetch = $query->row();
									$usercount = $fetch->userid;
									$referrals      = $this->db->query("select * from referral_settings where category_type='categoryone'")->row();	
									$bonus_amount   = $referrals->ref_cashback_rate_bonus;
									$friends_count  = $referrals->friends_count;
										
									if($usercount == $friends_count)
									{	
									if($bonus_amount!='')
									{
										//echo "hai";
										$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category One User' AND `user_id`=$ref_user_id"; 
						 				$query1=$this->db->query("$selqry");
						 				$newnumrows = $query1->num_rows();
										if($newnumrows > 0)
										{
											$fetch = $query1->row();
											$users_count = $fetch->userid;
											if($users_count == 0)	
											{	
												$data = array(			
												'transation_amount' => $bonus_amount,	
												'user_id' => $ref_user_id,	
												'transation_date' => $now,
												'transaction_date' => $now,
												'transation_id'=>$n12,	
												'transation_reason' => 'Referral Bonus for Category One User',	
												'mode' => 'Credited',
												'details_id'=>'',	
												'table'=>'',	
												'new_txn_id'=>0,
												'transation_status ' => 'Approved',
												'report_update_id'=>$newtransaction_id
												);	
												$this->db->insert('transation_details',$data);
											}	
										}
									}	
									}
								} 
							}
							//3** Bonus by Refferal Rate type End//

							//1** Refferal by Percentage type Start//
							if($ref_by_percentage == 1)
							{

								$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount');
								//$this->db->where('transation_reason','Referral Cashback amount');
								$this->db->where('new_txn_id',$new_txn_ids);
								$this->db->update('transation_details',$data);
							}
							//1** Refferal by Percentage type End//	

						}	

						if($single->referral_category_type == 2)
						{	
						 	
							$referrals    	    = $this->db->query("select * from referral_settings where category_type='categorytwo'")->row();	
							$ref_by_percentage  = $referrals->ref_by_percentage;
							$ref_by_rate 		= $referrals->ref_by_rate;
							$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

							//3** Bonus by Refferal Rate type//

							if($bonus_by_ref_rate == 1)
							{
								
								$n9  = '333445';
								$n12 = $n9 + $ref_user_id;
								$now = date('Y-m-d H:i:s');	
								$newtransaction_id   = rand(1000,9999);
								$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
						 		$query=$this->db->query("$selqry");
								$numrows = $query->num_rows();
								if($numrows > 0)
								{
									$fetch 			= $query->row(); 
									$usercount 		= $fetch->userid;
									$referrals      = $this->db->query("select * from referral_settings where category_type='categorytwo'")->row();	
									$bonus_amount   = $referrals->ref_cashback_rate_bonus;
									$friends_count  = $referrals->friends_count;
										
									if($usercount == $friends_count)
									{
										if($bonus_amount!='')
										{
											$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category Two User' AND `user_id`=$ref_user_id"; 
							 				$query1=$this->db->query("$selqry");
							 				$newnumrows = $query1->num_rows();
											if($newnumrows > 0)
											{
												$fetch = $query1->row();
												$users_count = $fetch->userid;
												if($users_count == 0)	
												{
										
													$data = array(			
													'transation_amount' => $bonus_amount,	
													'user_id' => $ref_user_id,	
													'transation_date' => $now,
													'transaction_date' => $now,
													'transation_id'=>$n12,	
													'transation_reason' => 'Referral Bonus for Category Two User',	
													'mode' => 'Credited',
													'details_id'=>'',	
													'table'=>'',	
													'new_txn_id'=>0,
													'transation_status ' => 'Approved',
													'report_update_id'=>$newtransaction_id
													);
													$this->db->insert('transation_details',$data);
												}
											}
										}		
									}
								} 
							}
							//3** Bonus by Refferal Rate type End//

							//1** Refferal by Percentage type Start//
							if($ref_by_percentage == 1)
							{

								$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount');
								//$this->db->where('transation_reason','Referral Cashback amount');
								$this->db->where('new_txn_id',$new_txn_ids);
								$this->db->update('transation_details',$data);
							}
							//1** Refferal by Percentage type End//	

						}

						if($single->referral_category_type == 3)
						{	
						 
							$referrals    	    = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
							$ref_by_percentage  = $referrals->ref_by_percentage;
							$ref_by_rate 		= $referrals->ref_by_rate;
							$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

							//3** Bonus by Refferal Rate type//

							if($bonus_by_ref_rate == 1)
							{
								
								$n9  = '333445';
								$n12 = $n9 + $ref_user_id;
								$now = date('Y-m-d H:i:s');	
								$newtransaction_id   = rand(1000,9999);
								$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
						 		$query=$this->db->query("$selqry");
								$numrows = $query->num_rows();
								if($numrows > 0)
								{
									$fetch = $query->row();
									$usercount = $fetch->userid;
									$referrals      = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
									$bonus_amount   = $referrals->ref_cashback_rate_bonus;
									$friends_count  = $referrals->friends_count;
										
									if($usercount == $friends_count)
									{
										if($bonus_amount!='')
										{
											$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category Three User' AND `user_id`=$ref_user_id"; 
							 				$query1=$this->db->query("$selqry");
							 				$newnumrows = $query1->num_rows();
											if($newnumrows > 0)
											{
												$fetch = $query1->row();
												$users_count = $fetch->userid;
												if($users_count == 0)	
												{
													
													$data = array(			
													'transation_amount' => $bonus_amount,	
													'user_id' => $ref_user_id,	
													'transation_date' => $now,
													'transaction_date' => $now,
													'transation_id'=>$n12,	
													'transation_reason' => 'Referral Bonus for Category Three User',	
													'mode' => 'Credited',
													'details_id'=>'',	
													'table'=>'',	
													'new_txn_id'=>0,
													'transation_status ' => 'Approved',
													'report_update_id'=>$newtransaction_id
													);
												 	$this->db->insert('transation_details',$data);
												}
											}
										}		
									}
								} 
							}
							//3** Bonus by Refferal Rate type End//

							//1** Refferal by Percentage type Start//
							if($ref_by_percentage == 1)
							{

								$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount');
								//$this->db->where('transation_reason','Referral Cashback amount');
								$this->db->where('new_txn_id',$new_txn_ids);
								$this->db->update('transation_details',$data);
							}
							//1** Refferal by Percentage type End//	

						}

						if($single->referral_category_type == 4)
						{   
							$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryfour'")->row();	
							$ref_by_percentage  = $referrals->ref_by_percentage;
							$ref_by_rate 		= $referrals->ref_by_rate;
							$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;
							$bonus_amount   	= $referrals->ref_cashback_rate_bonus;
							$friends_count  	= $referrals->friends_count;

							//3** Bonus by Refferal Rate type//

							if($bonus_by_ref_rate == 1)
							{
								
								$n9  = '333445';
								$n12 = $n9 + $ref_user_id;
								$now = date('Y-m-d H:i:s');	
								$newtransaction_id   = rand(1000,9999);
								$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referal Payment' AND `user_id`=$ref_user_id"; 
						 		$query=$this->db->query("$selqry");
								$numrows = $query->num_rows();
								if($numrows > 0)
								{

									$fetch 			= $query->row();
									$usercount 		= $fetch->userid;
									$referrals      = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
									$bonus_amount   = $referrals->ref_cashback_rate_bonus;
									$friends_count  = $referrals->friends_count;

									if($usercount == $friends_count)
									{
										if($bonus_amount!='')
										{
											$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category Four User' AND `user_id`=$ref_user_id"; 
							 				$query1=$this->db->query("$selqry");
							 				$newnumrows = $query1->num_rows();
											if($newnumrows > 0)
											{
												$fetch = $query1->row();
												$users_count = $fetch->userid;
												if($users_count == 0)	
												{
										
													$data = array(			
													'transation_amount' => $bonus_amount,	
													'user_id' => $ref_user_id,	
													'transation_date' => $now,
													'transaction_date' => $now,
													'transation_id'=>$n12,	
													'transation_reason' => 'Referral Bonus for Category Four User',	
													'mode' => 'Credited',
													'details_id'=>'',	
													'table'=>'',	
													'new_txn_id'=>0,
													'transation_status ' => 'Approved',
													'report_update_id'=>$newtransaction_id
													);
											 
													$this->db->insert('transation_details',$data);
												}
											}
										}		
									}
								} 
							}
							//3** Bonus by Refferal Rate type End//

							//1** Refferal by Percentage type Start//
							if($ref_by_percentage == 1)
							{

								$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount');
								//$this->db->where('transation_reason','Referral Cashback amount');
								$this->db->where('new_txn_id',$new_txn_ids);
								$this->db->update('transation_details',$data);
							}
							//1** Refferal by Percentage type End//	

						}

						if($single->referral_category_type == 5)
						{	

							$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryfive'")->row();	
							$ref_by_percentage  = $referrals->ref_by_percentage;
							$ref_by_rate 		= $referrals->ref_by_rate;
							$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

							//3** Bonus by Refferal Rate type//

							if($bonus_by_ref_rate == 1)
							{
								
								$n9  = '333445';
								$n12 = $n9 + $ref_user_id;
								$now = date('Y-m-d H:i:s');	
								$newtransaction_id   = rand(1000,9999);
								$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
						 		$query=$this->db->query("$selqry");
								$numrows = $query->num_rows();
								if($numrows > 0)
								{
									$fetch = $query->row();
									$usercount = $fetch->userid;
									$referrals      = $this->db->query("select * from referral_settings where category_type='categoryfive'")->row();	
									$bonus_amount   = $referrals->ref_cashback_rate_bonus;
									$friends_count  = $referrals->friends_count;
										
									if($usercount == $friends_count)
									{
										if($bonus_amount!='')
										{
											$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category five User' AND `user_id`=$ref_user_id"; 
							 				$query1=$this->db->query("$selqry");
							 				$newnumrows = $query1->num_rows();
											if($newnumrows > 0)
											{
												$fetch = $query1->row();
												$users_count = $fetch->userid;
												if($users_count == 0)	
												{
												
													$data = array(			
													'transation_amount' => $bonus_amount,	
													'user_id' => $ref_user_id,	
													'transation_date' => $now,
													'transaction_date' => $now,
													'transation_id'=>$n12,	
													'transation_reason' => 'Referral Bonus for Category five User',	
													'mode' => 'Credited',
													'details_id'=>'',	
													'table'=>'',	
													'new_txn_id'=>0,
													'transation_status ' => 'Approved',
													'report_update_id'=>$newtransaction_id
													);
													$this->db->insert('transation_details',$data);
												}
											}
										}		
									}
								} 
							}
							//3** Bonus by Refferal Rate type End//

							//1** Refferal by Percentage type Start//
							if($ref_by_percentage == 1)
							{

								$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount');
								//$this->db->where('transation_reason','Referral Cashback amount');
								$this->db->where('new_txn_id',$new_txn_ids);
								$this->db->update('transation_details',$data);
							}
							//1** Refferal by Percentage type End//	

						}
					}
				}		
				}
				 
			}
			//return true;
			} 
			//End//
		}

		if($a1!=0)
			{
				return true;
			}else
			{
				return false;
			}
	}
	//End//




	function cancel_cashback($cashback_id)
	{
		$this->db->connection_check();		
		$this->db->where('cashback_id',$cashback_id);
		$cb = $this->db->get('cashback');
		$user_id ='';
		if($cb)
		{
			$cb_r    		 = $cb->row();
			$user_id 		 = $cb_r->user_id;
            $txn_id  		 = $cb_r->txn_id;
			$cashback_amount = $cb_r->cashback_amount;
		
			$user = $this->view_user($user_id);
			if($user)
			{
				foreach($user as $single)
				{
					$balance    = $single->balance;
					$user_email = $single->email;
					$user_name  = $single->first_name.' '.$single->last_name;
				}

				//$this->db->where('user_id',$user_id);
				//$this->db->update('tbl_users',array('balance'=>$balance+$cashback_amount));				
				//$ins_data = array('user_id'=>$user_id,'transation_amount'=>$cashback_amount,'mode'=>'Credited','transation_date'=>date('Y-m-d'),'transation_status'=>'Paid','transation_reason'=>'Cashback');
				//$this->db->insert('transation_details',$ins_data);
				//echo "kjfdj";exit;
				$data = array('status'=>'Canceled');
				$this->db->where('cashback_id',$cashback_id);
				$this->db->update('cashback',$data);
                if($txn_id)
                {
					$data2 = array('transation_status'=>'Canceled');
					$this->db->where('trans_id',$txn_id);
					$this->db->update('transation_details',$data2);
                }
				/* mail for pending cashback */
				$this->db->where('admin_id',1);
				$admin_det = $this->db->get('admin');
				if($admin_det->num_rows >0) 
				{    
					$admin = $admin_det->row();
					$admin_email = $admin->admin_email;
					$site_name = $admin->site_name;
					$admin_no = $admin->contact_number;
					$site_logo = $admin->site_logo;
				}
				$date =date('Y-m-d');
				if($single->cashback_mail == 1)
				{
					$this->db->where('mail_id',11);
					$mail_template = $this->db->get('tbl_mailtemplates');
					if($mail_template->num_rows >0) 
					{
					    
					   $fetch = $mail_template->row();
					   $subject = $fetch->email_subject;
					   $templete = $fetch->email_template;
					   $url = base_url().'cashback/my_earnings/';
					   $unsuburl	 = base_url().'cashback/un_subscribe/cashback/'.$user_id;
					   $myaccount    = base_url().'cashback/minha_conta';
					   
						$this->load->library('email');
						
						$config = Array(
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
						);
						
						$sub_data = array(
							'###SITENAME###'=>$site_name
						);
						$subject_new = strtr($subject,$sub_data);
						
						// $this->email->initialize($config);
						 $this->email->set_newline("\r\n");
						   $this->email->initialize($config);
						   $this->email->from($admin_email,$site_name.'!');
						   $this->email->to($user_email);
						   $this->email->subject($subject_new);
					   
						$data = array(
							'###NAME###'=>$user_name,
							'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
							'###SITENAME###'=>$site_name,
							'###ADMINNO###'=>$admin_no,
							'###DATE###'=>$date,
							'###AMOUNT###'=>str_replace('.', ',', $cashback_amount),
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
					    );
					   
					   $content_pop=strtr($templete,$data);
					   //echo print_r($content_pop); exit;
					   // echo $content_pop; echo $subject_new;
					   $this->email->message($content_pop);
					   $this->email->send();  
					}
				}     
				$this->db->where('cashback_id',$cashback_id);
				$cashbacks = $this->db->get('cashback');
				$cashback_data = $cashbacks->row();
				if($cashback_data->referral!=0)
				{
					$this->db->where('trans_id',$cashback_data->txn_id);
					$txn = $this->db->get('transation_details');
					$txn_detail = $txn->row();
					if($txn_detail)
					{
						$txn_id = $txn_detail->trans_id;
						$ref_user_id = $txn_detail->user_id;
						$transation_amount = $txn_detail->transation_amount;
						$refer_user = $this->view_user($ref_user_id);
						if($refer_user)
						{
							foreach($refer_user as $single)
							{
								$referral_balance = $single->balance;
								$user_email = $single->email;
								$user_name = $single->first_name.' '.$single->last_name;
							}
							//$this->db->where('user_id',$ref_user_id);
							//$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amount));
							$data = array('transation_status'=>'Canceled','transation_reason'=>'Referal Payment');
							$this->db->where('trans_id',$txn_id);
							$this->db->update('transation_details',$data);
							/* mail for pending referral */
							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name = $admin->site_name;
								$admin_no = $admin->contact_number;
								$site_logo = $admin->site_logo;
							}
							$date =date('Y-m-d');
							
							if($single->referral_mail == 1)
							{
								$this->db->where('mail_id',12);
								$mail_template = $this->db->get('tbl_mailtemplates');
								if($mail_template->num_rows >0) 
								{
								   $fetch = $mail_template->row();
								   $subject = $fetch->email_subject;
								   $templete = $fetch->email_template;
								   $url = base_url().'cashback/my_earnings/';
								   $unsuburl	 = base_url().'cashback/un_subscribe/referral/'.$get_userid;
					   			   $myaccount    = base_url().'cashback/minha_conta';
								   
									$this->load->library('email');
									
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);
									$sub_data = array(
										'###SITENAME###'=>$site_name
									);
									$subject_new = strtr($subject,$sub_data);
									// $this->email->initialize($config);
									 $this->email->set_newline("\r\n");
									   $this->email->initialize($config);
									   $this->email->from($admin_email,$site_name.'!');
									   $this->email->to($user_email);
									   $this->email->subject($subject_new);
								   
									$data = array(
										'###NAME###'=>$user_name,
										'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###AMOUNT###'=>str_replace('.', ',', $transation_amount),
										'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
										'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
									);
								   
								   $content_pop=strtr($templete,$data);
								  // echo $subject_new;  echo $content_pop;exit;
								   $this->email->message($content_pop);
								   $this->email->send();  
								}
							}	
							/* mail for pending referral */	 
						}
					}
				}
			}
			return true;
		}
		return false;
	}


	//New code for Cancel multiple cashback records 6/4/16//

	function cancel_multi_cashbacks()
	{
		 
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		$a1=0;
		foreach($sort_order as $key=>$val)
		{
			$a1++;
			$cashback_id = $key;	
			$this->db->where('cashback_id',$cashback_id);
			$cb = $this->db->get('cashback');
			
			$user_id ='';
			if($cb)
			{
			$cb_r    		 = $cb->row();
			$user_id 		 = $cb_r->user_id;
            $txn_id  		 = $cb_r->txn_id;
			$cashback_amount = $cb_r->cashback_amount;
		
			$user = $this->view_user($user_id);
			if($user)
			{
				foreach($user as $single)
				{
					$balance    = $single->balance;
					$user_email = $single->email;
					$user_name  = $single->first_name.' '.$single->last_name;
				}

				//$this->db->where('user_id',$user_id);
				//$this->db->update('tbl_users',array('balance'=>$balance+$cashback_amount));				
				//$ins_data = array('user_id'=>$user_id,'transation_amount'=>$cashback_amount,'mode'=>'Credited','transation_date'=>date('Y-m-d'),'transation_status'=>'Paid','transation_reason'=>'Cashback');
				//$this->db->insert('transation_details',$ins_data);
				//echo "kjfdj";exit;
				$data = array('status'=>'Canceled');
				$this->db->where('cashback_id',$cashback_id);
				$this->db->update('cashback',$data);
                if($txn_id)
                {
					$data2 = array('transation_status'=>'Canceled');
					$this->db->where('trans_id',$txn_id);
					$this->db->update('transation_details',$data2);
                }
				/* mail for pending cashback */
				$this->db->where('admin_id',1);
				$admin_det = $this->db->get('admin');
				if($admin_det->num_rows >0) 
				{    
					$admin = $admin_det->row();
					$admin_email = $admin->admin_email;
					$site_name = $admin->site_name;
					$admin_no = $admin->contact_number;
					$site_logo = $admin->site_logo;
				}
				$date =date('Y-m-d');
				if($single->cashback_mail == 1)
				{
					$this->db->where('mail_id',11);
					$mail_template = $this->db->get('tbl_mailtemplates');
					if($mail_template->num_rows >0) 
					{
					    
					   $fetch = $mail_template->row();
					   $subject = $fetch->email_subject;
					   $templete = $fetch->email_template;
					   $url = base_url().'cashback/my_earnings/';
					   $unsuburl	 = base_url().'cashback/un_subscribe/cashback/'.$user_id;
					   $myaccount    = base_url().'cashback/minha_conta';
					   
						$this->load->library('email');
						
						$config = Array(
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
						);
						
						$sub_data = array(
							'###SITENAME###'=>$site_name
						);
						$subject_new = strtr($subject,$sub_data);
						
						// $this->email->initialize($config);
						 $this->email->set_newline("\r\n");
						   $this->email->initialize($config);
						   $this->email->from($admin_email,$site_name.'!');
						   $this->email->to($user_email);
						   $this->email->subject($subject_new);
					   
						$data = array(
							'###NAME###'=>$user_name,
							'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
							'###SITENAME###'=>$site_name,
							'###ADMINNO###'=>$admin_no,
							'###DATE###'=>$date,
							'###AMOUNT###'=>str_replace('.', ',', $cashback_amount),
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
					    );
					   
					   $content_pop=strtr($templete,$data);
					   //echo print_r($content_pop); exit;
					   // echo $content_pop; echo $subject_new;
					   $this->email->message($content_pop);
					   $this->email->send();  
					}
				}     
				$this->db->where('cashback_id',$cashback_id);
				$cashbacks = $this->db->get('cashback');
				$cashback_data = $cashbacks->row();
				if($cashback_data->referral!=0)
				{
					$this->db->where('trans_id',$cashback_data->txn_id);
					$txn = $this->db->get('transation_details');
					$txn_detail = $txn->row();
					if($txn_detail)
					{
						$txn_id = $txn_detail->trans_id;
						$ref_user_id = $txn_detail->user_id;
						$transation_amount = $txn_detail->transation_amount;
						$refer_user = $this->view_user($ref_user_id);
						if($refer_user)
						{
							foreach($refer_user as $single)
							{
								$referral_balance = $single->balance;
								$user_email = $single->email;
								$user_name = $single->first_name.' '.$single->last_name;
							}
							//$this->db->where('user_id',$ref_user_id);
							//$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amount));
							$data = array('transation_status'=>'Canceled','transation_reason'=>'Referal Payment');
							$this->db->where('trans_id',$txn_id);
							$this->db->update('transation_details',$data);
							/* mail for pending referral */
							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name = $admin->site_name;
								$admin_no = $admin->contact_number;
								$site_logo = $admin->site_logo;
							}
							$date =date('Y-m-d');
							
							if($single->referral_mail == 1)
							{
								$this->db->where('mail_id',12);
								$mail_template = $this->db->get('tbl_mailtemplates');
								if($mail_template->num_rows >0) 
								{
								   $fetch = $mail_template->row();
								   $subject = $fetch->email_subject;
								   $templete = $fetch->email_template;
								   $url = base_url().'cashback/my_earnings/';
								   $unsuburl	 = base_url().'cashback/un_subscribe/referral/'.$get_userid;
					   			   $myaccount    = base_url().'cashback/minha_conta';
								   
									$this->load->library('email');
									
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);
									$sub_data = array(
										'###SITENAME###'=>$site_name
									);
									$subject_new = strtr($subject,$sub_data);
									// $this->email->initialize($config);
									 $this->email->set_newline("\r\n");
									   $this->email->initialize($config);
									   $this->email->from($admin_email,$site_name.'!');
									   $this->email->to($user_email);
									   $this->email->subject($subject_new);
								   
									$data = array(
										'###NAME###'=>$user_name,
										'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###AMOUNT###'=>str_replace('.', ',', $transation_amount),
										'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
										'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
									);
								   
								   $content_pop=strtr($templete,$data);
								  // echo $subject_new;  echo $content_pop;exit;
								   $this->email->message($content_pop);
								   $this->email->send();  
								}
							}	
							/* mail for pending referral */	 
						}
					}
				}
			}
			//return true;
			}	
			/*$data = array(		
			'status' => 'Canceled');
			$this->db->where('cashback_id',$id);
			$update_qry = $this->db->update('cashback',$data);*/		
		}
		if($a1!=0)
		{
			return true;
		}else
		{
			return false;
		}
	}
	//End//





	function pending_referral(){
		$this->db->connection_check();
		$this->db->select("*");
		$this->db->from("transation_details");
		$this->db->where('transation_reason','Pending Referal Payment');
		$this->db->where('transation_status','Pending');
		$this->db->order_by("trans_id", "desc");
		$result = $this->db->get();
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	function approve_referral($txn_id){
		$this->db->connection_check();
		$this->db->where('trans_id',$txn_id);
		$txn = $this->db->get('transation_details');
		if($txn){
			$txn_detail = $txn->row();
			$user_id = $txn_detail->user_id;
			$transation_amount = $txn_detail->transation_amount;
			
			$refer_user = $this->view_user($user_id);
			if($refer_user){
				foreach($refer_user as $single){
					$referral_balance = $single->balance;
				}
				$this->db->where('user_id',$user_id);
				$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amount));
				
				$data = array('transation_status'=>'Approved','transation_reason'=>'Referal Payment');
				$this->db->where('trans_id',$txn_id);
				$this->db->update('transation_details',$data);

				return true;
			}
		}
		return false;
	}

	function cancel_referral($txn_id){
		$this->db->connection_check();
		$this->db->where('trans_id',$txn_id);
		$txn = $this->db->get('transation_details');
		if($txn){
			$txn_detail = $txn->row();
			$user_id = $txn_detail->user_id;
			$transation_amount = $txn_detail->transation_amount;
			
			$refer_user = $this->view_user($user_id);
			if($refer_user){
				foreach($refer_user as $single){
					$referral_balance = $single->balance;
				}
				//$this->db->where('user_id',$user_id);
				//$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amount));
				
				$data = array('transation_status'=>'Canceled','transation_reason'=>'Referal Payment');
				$this->db->where('trans_id',$txn_id);
				$this->db->update('transation_details',$data);
				return true;
			}
		}
		return false;
	}
	/*Seetha 24/10/15 */

	//New code for delete multiple Referral records 6/4/16//

	function delete_multi_referral()
	{
		 
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		foreach($sort_order as $key=>$val)
		{
			$id = $key;		
			$this->db->delete('transation_details',array('trans_id' => $id));			
		}
		return true;
	}
	//End//

	//New code for Approve multiple Referral records 6/4/16//

	function approve_multi_referral()
	{
		 
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		foreach($sort_order as $key=>$val)
		{
			$id = $key;	

			$data = array(	
			'transation_reason' => 'Referal Payment',
			'transation_status' => 'Approved');
			$this->db->where('transation_reason','Pending Referal Payment');
			$this->db->where('transation_status','Pending');
			$this->db->where('trans_id',$id);
			$update_qry = $this->db->update('transation_details',$data);		
		}
		return true;
	}
	//End//

	//New code for Cancel multiple Referral records 6/4/16//

	function cancel_multi_referral()
	{
		 
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		foreach($sort_order as $key=>$val)
		{
			$id = $key;		
			$data = array(	
			'transation_status' => 'Canceled');
			$this->db->where('transation_reason','Pending Referal Payment');
			$this->db->where('transation_status','Pending');
			$this->db->where('trans_id',$id);
			$update_qry = $this->db->update('transation_details',$data);			
		}
		return true;
	}
	//End//








//Affiliate Network API 16/3/16//

function affiliate_network(){
		$this->db->connection_check();
		$this->db->order_by('id','desc');
		$affiliate_network = $this->db->get('affiliates_list');
		if($affiliate_network->num_rows > 0){
			return $affiliate_network->result();		
		}
		return false;
	}
	// addaffiliate_list
	function addaffiliate_list($img){
	$this->db->connection_check();
		$data = array(
			'affiliate_network'=>$this->input->post('affiliate_name'),			
			'secret_key'=>$this->input->post('secret_key'),
			'connectid'=>$this->input->post('connectid'),
			'signature_id'=>$this->input->post('signature_id'),
			'affiliate_logo'=>$img,
			'status'=>$this->input->post('affiliate_status')
		);	
		$this->db->insert('affiliates_list',$data);
		return true;
	}
	//edit affiliate_list
	function get_affiliate_list($id){
	$this->db->connection_check();
		$this->db->where('id',$id);
		$affiliates = $this->db->get('affiliates_list');
		if($affiliates->num_rows > 0){
			return $affiliates->result();
		}
		return false;
	}
	// update affiliate_list
	function updataffiliate_list($img){
	$this->db->connection_check();
	$affiliate_id = $this->input->post('affiliate_id');
	$data = array(
		'affiliate_network'=>$this->input->post('affiliate_name'),
		'secret_key'=>$this->input->post('secret_key'),
		'connectid'=>$this->input->post('connectid'),
		'signature_id'=>$this->input->post('signature_id'),
		'affiliate_logo'=>$img,
		'status'=>$this->input->post('affiliate_status')		
	);
	$this->db->where('id',$affiliate_id);
	$update = $this->db->update('affiliates_list',$data);
		if($update!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}
	}
	//  deleteaffiliate_list
	function deleteaffiliate_list($delete){ 
		$this->db->connection_check();
		$this->db->delete('affiliates_list',array('id' => $delete));
		return true;
	}
	function delete_multi_affiliatenetworks()
	 {
		 $this->db->connection_check();
		  $sort_order = $this->input->post('chkbox');
		 foreach($sort_order as $key=>$val)
		 {
			$id = $key;		
			$this->db->delete('affiliates_list',array('id' => $id));			
		 }
		 return true;
	 }
	function import_coupons($content)
	{
		//print_r($content);exit;	
		$tracking =$this->input->post('tracking_id');
		$coupon_type = '';
		if(count($content)!=0){
			$array = array();
			$duplicate = 0;
			$duplicate_promo_id = '';	
			//print_r($content['programItems']['programItem'][0]);exit;	
			//print_r($content);
			//unset($content['data'][0][0]);
			//unset($content['data'][0][1]);
			//unset($content['data'][0][2]);

			 
			foreach(($content['programItems']['programItem']) as $cont){
				 
				/*print_r($cont);  
			}
			exit;*/	
			//}	//print_r($cont['categories'][$i]['category']['$']);
			//exit;	//print_r($cont['categories'][$i]['category']['$']); echo "Hai"; echo"<br>";
				//$i++;
				//}
				//echo "hai"; exit;
				//exit;
				//echo $cont['categories'][0]['category']['$']; exit;

				$new_category_id='';
				 
				if (!empty($cont['description'])) 
				{	 
				 	
					$olddescription =  $cont['description'];
				    $description =  substr($olddescription,0,150);
				}	
				else
				{	 
					$description = '';
				}

				
				if (!empty($cont['categories'][0]['category']['$'])) 
				{
				  
					$category_name = $cont['categories'][0]['category']['$'];
				}	
				else
				{
					$category_name = '';
									
				}

				if (!empty($cont['categories'][0]['category']['@id']))
				{

					$promo_id = $cont['categories'][0]['category']['@id'];
					 
				}else					
				{						
					$promo_id = '';
				}

				if (!empty($cont['url'])) 
				{
					$offer_page =$cont['url'];
					 
				}else					
				{						
					$offer_page = '';
				}

				if (!empty($cont['startDate'])) 
				{
					$start_date =$cont['startDate'];
					 
				}else					
				{						
					$start_date = '';
				}
				 
				
				//echo $description;exit;
				 /*if (array_key_exists('coupon_code', $cont)) 
				{
					$code =$this->db->escape_str($cont['coupon_code']);
				
				}else					
				{						
					$code = '';
				} 
				*/ 
				/*

				$cont_offname =$this->db->escape_str($cont['offer_name']);					
				$offname =preg_split("/[ ]/", $cont_offname); 
				$name =preg_split("/[.]/", $offname[0]);
				//print_r($name);exit;
				//echo $name[0], "<br />"; exit; 

			
				$offer_name =mysql_real_escape_string($name[0]);
				$offer_url =$offname[0]; 
				$promo_id = $cont['promo_id'];
				$title = $this->db->escape_str($cont['coupon_title']);
			 	//$description = $this->db->escape_str($cont['coupon_description']);
				//$category_name = $this->db->escape_str($cont['category']);
				$type = $cont['coupon_type'];
				//$code = $cont['coupon_code'];
				//$offer_page = $this->db->escape_str($cont['link']);				
				$expiry_date = date('m/d/Y',strtotime($cont['coupon_expiry']));
				$start_date = date('m/d/Y',strtotime($cont['added']));
					$featured = $cont['featured'];
					$exclusive = $cont['exclusive'];

				*/
				
				/*

				//add new category name
				if($category_name!=""){	
                     // echo 'yes_cate';				
					//$category_name = $this->db->escape_str($cont['category']);				
					$this->db->where('category_name',$category_name);
					$cat = $this->db->get('categories');
					if($cat->num_rows()==0){
						//  echo 'ssss';
						$seo_url  = $this->admin_model->seoUrl($category_name);
						$data = array(
							'category_name' => $category_name,
							'category_url' => $seo_url,
							'category_status' => 0,						
						);
						$this->db->insert('categories',$data);					
						$new_category_id = $this->db->insert_id();
						$new_subcategory_id = '';
					} else{
						$rst = $cat->row();
						$new_category_id = $rst->category_id;
						$new_subcategory_id = '';
					}					
				}
				*/
				//echo "hai"; exit;
				//$offer_name = $cont['name'];
				//$offer_page = $cont['url'];
				$image 		 = $cont['image'];
				$title 		 = substr($olddescription,0,50);
				$type 		 = 'Coupon';
			    $expiry_date = date('Y-m-d H:m:s');
				
				//Add new store name
				$this->db->where('affiliate_name',$category_name);
				$aff = $this->db->get('affiliates');
				if($aff->num_rows()==0){
					$data = array(
						'affiliate_name' => $category_name,
						'affiliate_url' => $offer_page,
						'affiliate_logo' => $image,
						'affiliate_status' => '1',
					);
					$this->db->insert('affiliates',$data);
					$new_store_id = $this->db->insert_id();
				}else{
					///echo "hai"; exit;
					$result = $aff->row();
					$new_store_id = $result->affiliate_id;
				}
				
				//$this->db->where('promo_id',$promo_id);
				//$result = $this->db->get('coupons');
				//echo $result->num_rows;
				//if($result->num_rows == 0){					
					if($category_name)
					{	//echo "hai"; exit;
					 
						$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`,`code`,`offer_page`,`start_date`,`expiry_date`
						) VALUES ('$category_name','$title','$description','$type', '$code', '$offer_page','$start_date','$expiry_date')");
					 
					}
					else
					{
					$duplicate+=1;
					$duplicate_promo_id .= $promo_id.', ';
					}
					//echo "hai"; exit;
				unset($cont);
	 
			}
			//$i++;
			//exit;
			$array['duplicate'] = $duplicate;
			$array['promo_id'] = rtrim($duplicate_promo_id,', ');
		}
		//print_r($array);die;
		return $array;		
	}
	

//END//

//API Coupons 16/3/16//

function api_coupons($store_name=null){
		$this->db->connection_check();
		$this->db->order_by("coupon_id", "desc");
		if($store_name)
		{
			$this->db->like('offer_name', $store_name);	
		}
		$this->db->where("featured", "0");
		$result = $this->db->get('coupons');
		//echo $this->db->last_query();
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	// view coupon..	
	function api_editcoupon($coupon_id){
	$this->db->connection_check();
	$this->db->where('coupon_id',$coupon_id);
	$this->db->where('featured','0');
	$coupons = $this->db->get('coupons');
		if($coupons->num_rows > 0){
			return $coupons->result();
		}
		return false;
	}
	// update coupon details..
	function api_updatecoupon()
	{
		$this->db->connection_check();
		$start_date = $this->input->post('start_date');
		$expiry_date = date('Y-m-d',strtotime($this->input->post('expiry_date')));
		$coupon_id = $this->input->post('coupon_id');
		$data = array(
			'offer_name'=>$this->input->post('offer_name'),
			'category_name'=>$this->input->post('category_name'),
			'title'=>$this->input->post('title'),
			'description'=>$this->input->post('description'),
			'type'=>$this->input->post('type'),
			'code'=>$this->input->post('code'),
			'offer_page'=>$this->input->post('offer_page'),
			'expiry_date'=>$expiry_date,
			'start_date'=>$start_date,
			'featured'=>$this->input->post('featured'),
			'exclusive'=>$this->input->post('exclusive'),
			'Tracking'=>$this->input->post('Tracking'),
			'coupon_options'=>$this->input->post('coupon_options'),
			'cashback_description'=>$this->input->post('cashback_description')
		);
		$this->db->where('coupon_id',$coupon_id);
		$this->db->where('featured','0');
		$updation = $this->db->update('coupons',$data);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;
		}
	}		
	// delete coupon..
	function api_deletecoupon($delete_id){
		$this->db->connection_check();
		$this->db->delete('coupons',array('coupon_id'=>$delete_id,'featured'=>'0'));
		return true;	
	} 
	function api_changestatus($id,$status)
	{
		$this->db->connection_check();
		if($status==1) $var=0;else $var=1;
			$data = array(
				'featured'=>$var,
		);
		$this->db->where('coupon_id',$id);
		$updation = $this->db->update('coupons',$data);	
	}
	function api_coupons_bulk_delete()
	{
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
			 foreach($sort_order as $key=>$val)
			 {
					 $delete_id = $key;
					 $this->db->delete('coupons',array('coupon_id'=>$delete_id,'featured'=>'0'));
			 }
		return true;	
	}
	function api_download_free_coupons()
	{
		$this->db->connection_check();
		$selqry="SELECT * FROM  coupons  where featured='0' order by coupon_id desc";  
		$result=$this->db->query("$selqry"); 
		if($result->num_rows > 0)
		{		
			return $result->result();
		}
	}

//END//

//New code for referral Concept 18/3/16//

	function categoryone() 
	{
		$this->db->connection_check();
		$this->db->where('category_type','categoryone');
		$referral = $this->db->get('referral_settings');
		if($referral->num_rows >= 1){
			$row = $referral->row();
			return $referral->result();
		}
		else
		{
			return false;		
		}	
	}
	function categorytwo() 
	{
		$this->db->connection_check();
		$this->db->where('category_type','categorytwo');
		$referral = $this->db->get('referral_settings');
		if($referral->num_rows >= 1){
			$row = $referral->row();
			return $referral->result();
		}
		else
		{
			return false;		
		}	
	}

	function categorythree() 
	{
		$this->db->connection_check();
		$this->db->where('category_type','categorythree');
		$referral = $this->db->get('referral_settings');
		if($referral->num_rows >= 1){
			$row = $referral->row();
			return $referral->result();
		}
		else
		{
			return false;		
		}	
	}
	function categoryfour() 
	{
		$this->db->connection_check();
		$this->db->where('category_type','categoryfour');
		$referral = $this->db->get('referral_settings');
		if($referral->num_rows >= 1){
			$row = $referral->row();
			return $referral->result();
		}
		else
		{
			return false;		
		}	
	}
	function categoryfive() 
	{
		$this->db->connection_check();
		$this->db->where('category_type','categoryfive');
		$referral = $this->db->get('referral_settings');
		if($referral->num_rows >= 1){
			$row = $referral->row();
			return $referral->result();
		}
		else
		{
			return false;		
		}	
	}


	function userdetails($type)
	{	
		//echo "hai".$type; exit;
		$this->db->connection_check();
		$this->db->where('referral_category_type',$type);
		$result = $this->db->get('tbl_users');
		if($result->num_rows > 0)
		{		
			return $result->result();
		}
	}

	function updatecategoryone()
	{	
		$this->db->connection_check();
		$category_type 		= $this->input->post('cat_type');
		 	 
		$data = array(

			'ref_by_percentage'		 =>$this->input->post('refpercentage'),
			'ref_cashback'    		 =>$this->input->post('refcashback'),
			'valid_months'    		 =>$this->input->post('validmonth'),
			'ref_by_rate'     		 =>$this->input->post('refbyrate'),
			'ref_cashback_rate'		 =>$this->input->post('refcashback_rate'),
			'bonus_by_ref_rate'		 =>$this->input->post('bonus_rate'),
			'ref_cashback_rate_bonus'=>$this->input->post('refcashback_rate_bonus'),
			'cat_description'        =>$this->input->post('cat_description'),
			'friends_count'          =>$this->input->post('friends_count')
			
		);

		$this->db->where('category_type',$category_type);
		$updation = $this->db->update('referral_settings',$data);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}		
	}

	function updatecategorytwo()
	{	
		$this->db->connection_check();
		$category_type 		= $this->input->post('cat_type');
		 	 
		$data = array(

			'ref_by_percentage'		 =>$this->input->post('refpercentage'),
			'ref_cashback'    		 =>$this->input->post('refcashback'),
			'valid_months'    		 =>$this->input->post('validmonth'),
			'ref_by_rate'     		 =>$this->input->post('refbyrate'),
			'ref_cashback_rate'		 =>$this->input->post('refcashback_rate'),
			'bonus_by_ref_rate'		 =>$this->input->post('bonus_rate'),
			'ref_cashback_rate_bonus'=>$this->input->post('refcashback_rate_bonus'),
			'friends_count'          =>$this->input->post('friends_count'),
			'cat_description'        =>$this->input->post('cat_description')
		);

		$this->db->where('category_type',$category_type);
		$updation = $this->db->update('referral_settings',$data);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}		
	}

	function updatecategorythree()
	{	
		$this->db->connection_check();
		$category_type 		= $this->input->post('cat_type');
		 	 
		$data = array(

			'ref_by_percentage'		 =>$this->input->post('refpercentage'),
			'ref_cashback'    		 =>$this->input->post('refcashback'),
			'valid_months'    		 =>$this->input->post('validmonth'),
			'ref_by_rate'     		 =>$this->input->post('refbyrate'),
			'ref_cashback_rate'		 =>$this->input->post('refcashback_rate'),
			'bonus_by_ref_rate'		 =>$this->input->post('bonus_rate'),
			'ref_cashback_rate_bonus'=>$this->input->post('refcashback_rate_bonus'),
			'friends_count'          =>$this->input->post('friends_count'),
			'cat_description'        =>$this->input->post('cat_description')
		);

		$this->db->where('category_type',$category_type);
		$updation = $this->db->update('referral_settings',$data);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}		
	}

	function updatecategoryfour()
	{	
		$this->db->connection_check();
		$category_type 		= $this->input->post('cat_type');
		 	 
		$data = array(

			'ref_by_percentage'		 =>$this->input->post('refpercentage'),
			'ref_cashback'    		 =>$this->input->post('refcashback'),
			'valid_months'    		 =>$this->input->post('validmonth'),
			'ref_by_rate'     		 =>$this->input->post('refbyrate'),
			'ref_cashback_rate'		 =>$this->input->post('refcashback_rate'),
			'bonus_by_ref_rate'		 =>$this->input->post('bonus_rate'),
			'ref_cashback_rate_bonus'=>$this->input->post('refcashback_rate_bonus'),
			'friends_count'          =>$this->input->post('friends_count'),
			'cat_description'        =>$this->input->post('cat_description')
		);

		$this->db->where('category_type',$category_type);
		$updation = $this->db->update('referral_settings',$data);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}		
	}

	function updatecategoryfive()
	{	
		$this->db->connection_check();
		$category_type 		= $this->input->post('cat_type');
		 	 
		$data = array(

			'ref_by_percentage'		 =>$this->input->post('refpercentage'),
			'ref_cashback'    		 =>$this->input->post('refcashback'),
			'valid_months'    		 =>$this->input->post('validmonth'),
			'ref_by_rate'     		 =>$this->input->post('refbyrate'),
			'ref_cashback_rate'		 =>$this->input->post('refcashback_rate'),
			'bonus_by_ref_rate'		 =>$this->input->post('bonus_rate'),
			'ref_cashback_rate_bonus'=>$this->input->post('refcashback_rate_bonus'),
			'friends_count'          =>$this->input->post('friends_count'),
			'cat_description'        =>$this->input->post('cat_description')
		);

		$this->db->where('category_type',$category_type);
		$updation = $this->db->update('referral_settings',$data);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}		
	}

	function add_user($email,$cat_type)
	{	
		$this->db->connection_check();
		
		if($cat_type=='')
		{
			$cat_type = 1;
		}
		if($cat_type=='categorytwo')
		{
			$cat_type = 2;
		}
		if($cat_type=='categorythree')
		{
			$cat_type = 3;
		}
		if($cat_type=='categoryfour')
		{
			$cat_type = 4;
		}
		if($cat_type=='categoryfive')
		{
			$cat_type = 5;
		}
		

		$this->db->where('email',$email);
		$query = $this->db->get('tbl_users');
   
		if($query->num_rows()>= 1)
		{ 	
			$data = array(
			'referral_category_type' => $cat_type,
			);
			$this->db->where('email',$email);
			$this->db->update('tbl_users',$data);
			return true;
		}
		 else
		 {
			return false;
		 }	 
	}

	function update_user($userid)
	{		
		$this->db->connection_check();	
		$data = array(		
		'referral_category_type' => 1);
		$this->db->where('user_id',$userid);
		$update_qry = $this->db->update('tbl_users',$data);
		
		if($update_qry){
			return true;
		}
		else{
			return false;
		}	
	}

	 
//End//	
	function cat_details($categoryid)
	{
		$this->db->connection_check();
		$this->db->order_by('affiliate_status','ASC');
		$this->db->where("FIND_IN_SET('$categoryid',store_categorys) !=", 0);
		$this->db->where('affiliate_status','1');
		$query = $this->db->get('affiliates');
		//echo $this->db->last_query();die;
		if($query->num_rows >= 1)
		{	
			return $query->result();
		}
		return false;
	}

	function add_related_stores($ids,$name)
	{	
		//echo $ids; exit;
		$this->db->connection_check();
		
		//print_r($_POST); exit;
		$date = date('Y-m-d');
		$store_name   = $this->input->post('category_name');
		$store_status = $this->input->post('status');
		$store_counts = $this->input->post('counts');	
		$category_id  = $ids;
		$cat_name 	  = $name;
		$seo_url  	  = $this->admin_model->seoUrl($cat_name);

		$selqry = $this->db->query("SELECT ref_affiliate_name from aff_related_stores where ref_affiliate_name='$store_name'")->row(); 
		$category_names = $selqry->ref_affiliate_name;

		if($this->input->post('categorys_list'))
		{
			$store_categorys =implode(",",$this->input->post('categorys_list'));
		}
		else
		{
			$store_categorys='';
		}
		if($category_names =='')
		{
			$data = array(
			'ref_affiliate_name'  		 =>$store_name,
			'ref_affiliate_url'   		 =>$seo_url, 
			'ref_affiliate_status'		 =>$store_status,
			'ref_affiliate_display_count'=>$store_counts,
			'ref_affiliate_categorys' 	 =>$store_categorys,
			'ref_related_aff_name' 		 =>$cat_name,
			'date_added'				 =>$date
			);
			$this->db->insert('aff_related_stores',$data); 
			return true;
		}
		else
		{
			$data = array(
			'ref_affiliate_name'  		 =>$store_name,
			'ref_affiliate_url'   		 =>$seo_url, 
			'ref_affiliate_status'		 =>$store_status,
			'ref_affiliate_display_count'=>$store_counts,
			'ref_affiliate_categorys' 	 =>$store_categorys,
			'ref_related_aff_name' 		 =>$cat_name,
			'date_added'				 =>$date
			);
			$this->db->where('ref_affiliate_name',$store_name);
			$this->db->update('aff_related_stores',$data); 
			return true;
		}	
	}


//SATz


	// view balance..
	function withdraw_balance($withdraw_id){
		$this->db->connection_check();
		$balace = $this->db->get_where('withdraw',array('withdraw_id'=>$withdraw_id))->row('requested_amount');
		return $balace;
	}

//Get User's detail for export all 27-04-2016
function user_details(){

$selqry ="SELECT * FROM tbl_users";

$result =$this->db->query("$selqry");
					ob_end_clean();
					$data = $this->dbutil->csv_from_result($result, $delimiter, $newline, $enclosure);
    				force_download($filename, $data);
					

}


//SATz 05 04 2016

	
			// get admin details..
			function user_information(){
			$this->db->connection_check();
			$this->db->where('uid','1');
			$query_admin = $this->db->get('user_information');
			if($query_admin->num_rows >= 0){
			//return	$row = $query_admin->row();
			return $query_admin->result();
				}
				else
				{
				return false;		
				}	
			}


				function user_information_update(){

				$this->db->connection_check();

//$this->input->post('type'); exit();

		if($this->input->post('type')=='extrato')
		{


  $data=array(
    'uid' =>1,			
    'ex_cash_pending' =>$this->input->post('ex_cash_pending'), 
    'ex_cash_cancelled' =>$this->input->post('ex_cash_cancelled'), 
    'ex_cash_approved' => $this->input->post('ex_cash_approved'),
    'ex_ref_pending' =>$this->input->post('ex_ref_pending'),
    'ex_ref_cancelled' =>$this->input->post('ex_ref_cancelled'),
    'ex_ref_approved' => $this->input->post('ex_ref_approved'),
    'ex_ref_pending_sec' =>$this->input->post('ex_ref_pending_sec'),
    'ex_ref_cancelled_sec' => $this->input->post('ex_ref_cancelled_sec'),
    'ex_ref_approved_sec' =>$this->input->post('ex_ref_approved_sec'), 
    'ex_ref_pending_third' =>$this->input->post('ex_ref_pending_third'), 
    'ex_ref_cancelled_third' =>$this->input->post('ex_ref_cancelled_third'),
    'ex_ref_approved_third' =>$this->input->post('ex_ref_approved_third'),
    'ex_missing_cash_approved' =>$this->input->post('ex_missing_cash_approved'),
    'ex_credit_pending' =>$this->input->post('ex_credit_pending'),
    'ex_credit_cancelled' =>$this->input->post('ex_credit_cancelled'),
    'ex_credit_approved' =>$this->input->post('ex_credit_approved')

);



	$this->db->update('user_information',$data); 
	return true;

			}
	

	if($this->input->post('type')=='missing_cashabck')
		{
    $data=array(
    'missing_cash_created' =>$this->input->post('missing_cash_created'),
    'missing_cash_sentretailer' =>$this->input->post('missing_cash_sentretailer'),
    'missing_cash_cancelled' =>$this->input->post('missing_cash_cancelled'),
    'missing_cash_completed' =>$this->input->post('missing_cash_completed')
);    
    $this->db->update('user_information',$data); 
	return true;


}


	if($this->input->post('type')=='resgate')
		{
			$data=array(
    'resgate_requested' =>$this->input->post('resgate_requested'),
    'resgate_processing' =>$this->input->post('resgate_processing'),
    'resgate_completed' =>$this->input->post('resgate_completed'),
    'resgate_cancelled' =>$this->input->post('resgate_cancelled')
        );
	
    $this->db->update('user_information',$data); 
	return true;
}



	
				}



//SATz

	//New code for report export page bank details 7-5-16//
	function new_bank_details()
	{
		$this->db->connection_check();
		$this->db->order_by('bankid','desc');
		$this->db->where('report_export_status',0);
		$result = $this->db->get('tbl_banknames');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	//End//


//Pilaventhiran 07/05/2016 START
    function missiing_approval_update()
{
    $name = $this->db->query("select * from admin")->row();
            $site_name  = $name->site_name;

    $this->db->connection_check();
    $curr_status = $this->input->post('status');    
    $cashback_id = $this->input->post('cashback_id');
    $username = $this->input->post('username');
    $us_email = $this->input->post('us_email');    
    $ticket_id = $this->input->post('ticket_id');
    $retailer_name = $this->input->post('retailer_name');
    $cancel_reason = $this->input->post('cancel_reason');
    $Cashback_Return_Amount = $this->input->post('Cashback_Return_Amount');
    $user_id = $this->input->post('user_id');
    switch($curr_status)
    {
        case 0:
            $userbalance = $this->user_balance($user_id);
            $new_balnce = $userbalance+$Cashback_Return_Amount;
            $mode = "Credited";
            $transation_reason = 'Cashback';
            $details_id = $this->input->post('cashback_id');
            $this->update_users_balance($user_id,$Cashback_Return_Amount,$mode,$new_balnce,$transation_reason,$details_id,'missing_cashback');                
    //    $current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> Dear '.$username.',<br><br>Thank you for sending us the details of your transaction. Your Missing Cashback Ticket: '.$ticket_id.' has been Completed Successfully. Your Cashback Amount Added into your Account.<br><br>Please let us know if you have any further queries. Thaks For your business.<br><br> Current Status: Completed<br><br> Warm regards,<br> '.$site_name.' Team</span>';
            $current_msg ='';

        break;
        case 1:
        $mode_1 = "Sent to retailer";
        $current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Acabamos de enviar a sua reclamação para a loja. O processo de conferência é manual e pode levar até 40 dias úteis para que eles nos respondam. (A gente sabe que isso é muito demorado e é um saco esperar tanto, mas infelizmente não depende de nós . E esse é um “prazo máximo” pode ser que leve bem menos que isso).</span>';
        break;
        case 2:
        $mode_1 = "Cancelled";
            $current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> A loja acabou de nos atualizar sobre o seu caso e, infelizmente, eles não aprovaram o seu pedido. A justificativa que nos deram foi: '.$cancel_reason.'. Para evitar problemas futuros leia as Regras e exceções da loja e siga os ###RECOMENDACOES_CANCELAR###”</span>';
        break;
        case 3:
        $mode_1 = "Approved";
            $current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> 
Novidades! A loja acabou de nos confirmar que eles realmente não nos informaram da sua compra. Mas agora já está tudo resolvido! Acabou de pingar R$ '.$Cashback_Return_Amount.' na sua conta.
</span>';

$data_Up_Users_balance = array(
        'balance'=>$this->input->post('Cashback_Return_Amount'),
        
    );    
  
    $this->db->where('user_id',$user_id);
    $Query_Update_User_Balance = $this->db->update('tbl_users',$data);

break;
    }
    $data = array(
        'status'=>$this->input->post('status'),
        'status_update_date'=>$this->input->post('status_update_date'),
        'cancel_msg'=>$this->input->post('cancel_msg'),
        'current_msg'=>$current_msg,
    );    
    $cashback_id = $this->input->post('cashback_id');
    $this->db->where('cashback_id',$cashback_id);
    $upd = $this->db->update('missing_cashback',$data);
    if($upd)
    {
        if($curr_status!=1)
        {

            $mail_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='14'")->row();
            $fe_cont = $mail_temp->email_template;
            $name = $this->db->query("select * from admin")->row();
            $subject = "Your Missing Ticket Reply";
            $admin_emailid = $name->admin_email;
            $site_logo = $name->site_logo;
            $site_name  = $name->site_name;
            $contact_number = $name->contact_number;
            $servername = base_url();
            $nows = date('Y-m-d');    
            $this->load->library('email');
            $see_status_missing = "<a href='".base_url()."loja-cancelou-minha-compra'>status da solicitação</a>";
            $unsuburl     = base_url().'cashback/un_subscribe/myaccount/'.$get_userid;
            $myaccount    = base_url().'cashback/minha_conta';
            $gd_api=array(
                        '###ADMINNO###'=>$contact_number,
                        '###EMAIL###'=>$username,
                        '###DATE###'=>$nows,
                        '###MESSAGE###'=>$current_msg,
                        '###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
                        '###SITENAME###' =>$site_name,
                        '###MISSING_CASHBACK_STATUS###'=>$mode_1,
                        '###SEE_STATUS_MISSING###'=>$see_status_missing,
                        '###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
                        '###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

                        );



            $gd_message=strtr($fe_cont,$gd_api);

            $config = Array(
             'mailtype'  => 'html',
              'charset'   => 'utf-8',
              );

            $list = array($us_email);

            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from($admin_emailid,$site_name.'!');
            $this->email->to($list);
            $this->email->subject($subject);
            $this->email->message($gd_message);
            $this->email->send();
            $this->email->print_debugger();
        }
        return true;
    }
    else
    {
        return false;
    }
}

function missing_approval_fetch($Cashback_Id=null)
{
    $this->db->connection_check();
    if($Cashback_Id!="")
    {    
        $this->db->where('reference_id',$Cashback_Id);
        $missing_approval_fetch = $this->db->get('cashback');
        return $missing_approval_fetch->result();
    }
    else
    {
        return 0;
    }
}

//Pilaventhiran 07/05/2016 END

//SATz Sub Admin management model

	//add sub admin details
	function add_sub_admin($admin_logo){
	
		$main_access = serialize(array_filter($this->input->post('main_access')));
		 $sub_access = serialize(array_filter($this->input->post('sub_access')));

		if($_POST['perm'])
			$perm = serialize($_POST['perm']);
		else
			$perm = 'a:1:{i:0;s:1:"0";}';
		
		$data = array(
			'admin_username'=>$this->input->post('name'),
			'admin_password'=>$this->input->post('password'),
			'admin_email'=>$this->input->post('email'),
			'admin_logo'=>$admin_logo,
			// 'gender'=>$this->input->post('gender'),
			// 'job_role'=>$this->input->post('job_role'),
			// 'city'=>$this->input->post('city'),
			'contact_number'=>$this->input->post('number'),
			'contact_info'=>$this->input->post('content'),
			'status'=>$this->input->post('status'),
				'sub_access'=>$sub_access,
					'main_access'=>$main_access,
			'permission'=>$perm,
			'role'=>'sub'
		);
		//print_r($data);exit;
		$ins = $this->db->insert('admin',$data);
		if($ins!=""){ 
			return true;
		} else { 
			return false;
		}
	}
	
	// fetch sub admin detail..
	function fetch_sub_admin(){		
		$this->db->where('role','sub');
		$this->db->order_by('admin_id','desc');
		$fetch = $this->db->get('admin');
		if($fetch->num_rows>0){
			return $fetch->result();
		}
		return false;
	}
	
	// edit sub admin detail..
	function get_sub_admin($ids){		
		$this->db->where('admin_id',$ids);
		$fetch = $this->db->get('admin');
		if($fetch->num_rows>0){
			return $fetch->row();
		}
		return false;
	}
	
	// update sub admin details..
// update sub admin details..
	function update_sub_admin($admin_logo){
$main_access = serialize(array_filter($this->input->post('main_access')));
			 $sub_access = serialize(array_filter($this->input->post('sub_access')));
		if($_POST['perm'])
			$perm = serialize($_POST['perm']);
		else
			$perm = 'a:1:{i:0;s:1:"0";}';

		if($this->input->post('password')!='')
			$password = $this->input->post('password');
		else
			$password = '';

		$data = array(
			'admin_username'=>$this->input->post('name'),
			'admin_password'=>$password,
			'admin_email'=>$this->input->post('email'),
			'admin_logo'=>$admin_logo,
			// 'gender'=>$this->input->post('gender'),
			// 'job_role'=>$this->input->post('job_role'),
			// 'city'=>$this->input->post('city'),
			'contact_number'=>$this->input->post('number'),
			'contact_info'=>$this->input->post('content'),
			'status'=>$this->input->post('status'),
			'permission'=>$perm,
			'role'=>'sub',
            'main_access'=>$main_access,
			'sub_access'=>$sub_access
		);
		
		$this->db->where('admin_id',$this->input->post('admin_id'));
		$updation = $this->db->update('admin',$data);
		//echo $this->db->last_query();die;

		if($updation!=""){
			return true;
		} else { 
			return false;
		}
	}

	
	// delete sub admin..
	function delete_sub_admin($sub_admin_id){
	
		$this->db->delete('admin',array('admin_id'=>$sub_admin_id));
		return true;
	}
	
	function multi_delete_subadmin(){
		if($this->input->post('chkbox')) {
		  $sort_order = $this->input->post('chkbox');
		  foreach($sort_order as $key=>$val) {
				$this->db->where('admin_id',$key);
				$upd = $this->db->delete('admin');
		  }
			return true;
		}
	}
	
	
	function get_admin_pages(){	
		$fetch = $this->db->get('adminpages');
		if($fetch->num_rows>=1){
			return $fetch->result();
		} else {
			return false;
		}
	}
	function get_admin_pages1($id='')
	{	
	if($id!="")
	{
		$id=$id;
	}
	else
	{
		$id=0;
	}

	$this->db->where('sub_id',$id);
		$fetch = $this->db->get('admin_page_new');
		//echo $this->db->last_query();die;
		if($fetch->num_rows>=1){
			return $fetch->result();
		} else {
			return false;
		}
	}
	function get_admin_pages2($id='')
	{	
	if($id!="")
	{
		$id=$id;
	}
	else
	{
		$id=0;
	}

	$this->db->where('sub_id',$id);
		$fetch = $this->db->get('admin_page_new');
		//echo $this->db->last_query();die;
		if($fetch->num_rows>=1){
		return $fetch->result();
		} else {
		return false;
		}
	}

	//manage sub admin
	function check_sub_admin($email){
		$this->db->where('admin_email',$email);
		$res = $this->db->get('admin');
		if($res->num_rows > 0){
			return 0;	// exists.. failure..
		} else {
			return 1;	// not exists.. success..
		}
	}


	function getadmindetails_session(){
		$this->db->connection_check();
		$admin_id = $this->session->userdata('admin_id');
		$this->db->where('admin_id',$admin_id);
	    $query_admin = $this->db->get('admin');
		if($query_admin->num_rows >= 1){
		$row = $query_admin->row();
		return $query_admin->result();
		}
		else
		{
		return false;		
		}	
	}


//SATz sub admin END
function session_time_main_access(){

		       $admin_details = $this->getadmindetails();
		       if($admin_details){
			   foreach($admin_details as $details){
			   $hours=$ses_datetime_main  = $details->ses_datetime;
		       }
    		   }
		       $CI =& get_instance();
		       $CI->load->helper('my_time_convert');
		       $minutes=hoursToMinutes($hours);
		       return $session_minutes_meta=MinutesToSeconds($minutes);

} 
/* 11 05 2016 SATz sub admin END  */

	/*New code for upload premium coupons details 10-5-16*/

	function upload_coupons($bulkcoupons)
	{
		
		$this->db->connection_check();
		$coupon_type = '';
		$this->load->library('CSVReader');
		$main_url = 'uploads/premium_coupons/'.$bulkcoupons;
	 	$result =   $this->csvreader->parse_file($main_url);
		if(count($result)!=0)
		{
		
			foreach($result as $res)
			{

				$offer_name 	= $res['offer_name'];
				$image_url      = $res['image_url'];
				$location   	= $res['location'];
				$category   	= $res['category'];
				$discount_price = $res['amount'];
				$total_price 	= $res['price'];
				$start_date 	= $res['start_date'];
				$expiry_date 	= $res['expiry_date'];
				$coupon_code 	= $res['coupon_code'];
				$store_id       = $res['store_id'];
				$features_type  = $res['features_type'];
				$offer_url  	= $res['offer_page'];
				$extra_url  	= $res['extra_url'];

				$seo_url        = $this->admin_model->seoUrl($offer_name);

				$results = $this->db->query("INSERT INTO `shopping_coupons` (`seo_url`,`offer_name`,`coupon_image`, `location`, `category`, `amount`,`offer_page`, `start_date`,`date_added`, `expiry_date`, `coupon_code`,`remain_coupon_code`, `price`,`status`,`store_name`,`tracking`,`features_type`)
				VALUES ('$seo_url','$offer_name','$image_url', '$location', '$category', '$discount_price', '$offer_url','$start_date','$start_date', '$expiry_date', '$coupon_code', '$coupon_code','$total_price',1,'$store_id','$extra_url','$features_type');");
			}
		}
		
		return true;
	}

	/*End 10-5-16*/

	/*New code for seo url without replace a speical characters 12-5-16 */
	function newseoUrl($string) {
		$this->db->connection_check();
		//Lower case everything
		$string = strtolower($string);
		//Make alphanumeric (removes all other characters)
		//$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
		//Clean up multiple dashes or whitespaces
		$string = preg_replace("/[\s-]+/", " ", $string);
		//Convert whitespaces and underscore to dash
		$string = preg_replace("/[\s_]/", "-", $string);
		return $string;
	}
	/*End*/	

	/*New code for report upload via API 7-6-16.*/
	function import_apicoupons($content,$affiliate_name)
	{
		echo "<pre>";print_r($content);exit;
		//$tracking =$this->input->post('tracking_id');
		echo $content;
		print_r($content);
		//exit;
		$coupon_type = '';
		if(count($content)!=0)
		{
			$array 	   = array();
			$duplicate = 0;
			$duplicate_promo_id = '';
					
			//echo $affiliate_name; echo "<br>"; exit;

			if($affiliate_name == 'zanox')
			{
				foreach($content['incentiveItems']['incentiveItem'] as $cont)
				{
					
					$new_category_id='';
					 
					/*Coupon Description details*/
					if (array_key_exists('description', $cont)) 
					{
						$description = $this->db->escape_str($cont['admedia']['admediumItem']['description']);
					}	
					else
					{
						$description = '';
					}

					/*Coupon Name details*/
					if (array_key_exists('AdvertiserName', $cont)) 
					{
						$category_name =$this->db->escape_str($cont['name']);
					}	
					else
					{
						$category_name = '';				
					}

					/*Coupon code details*/
					if (array_key_exists('couponCode', $cont)) 
					{
						$code =$this->db->escape_str($cont['couponCode']);
					
					}else					
					{						
						$code = '';
					}

					
					/*Coupon table details 09-06-16.*/

					$cont_offname    = $cont['admedia']['admediumItem']['program']['$'];
					$offer_name      = trim($cont_offname, " BR");
				  //$offer_name      = explode(' ',$cont_offname);
				  //$offer_name      = array_pop($offer_name);
					
					$title 		     = $cont['title'];
					$type 		 	 = $cont['incentiveType'];
					$offer_page 	 = $cont['admedia']['admediumItem']['trackingLinks']['trackingLink'][0]['ppc'];
					$start_date  	 = date('Y-m-d',strtotime($cont['startDate']));	
					$expiry_date 	 = date('Y-m-d',strtotime($cont['endDate']));	
					$tracking 		 = $cont['admedia']['admediumItem']['trackingLinks']['trackingLink'][0]['@adspaceId'];
					$promo_id 	 	 = $cont['@id'];
					$featured    	 = $cont['featured'];
					$exclusive   	 = $cont['exclusive'];
					
					if($start_date == '')
					{
						$start_date = date('Y-m-d');
					}
					if($expiry_date == '1969-12-31')
					{
						$expiry_date = date('Y-m-d'); 
					}

					/*End coupon table details*/
				
					/*Add new category name*/

					if($offer_category_name!="")
					{	
	                    		
						$this->db->where('category_name',$offer_category_name);
						$cat = $this->db->get('categories');
						if($cat->num_rows()==0)
						{
							
							$seo_url  = $this->admin_model->seoUrl($offer_category_name);
							$data     = array(
								'category_name'   => $offer_category_name,
								'category_url'    => $seo_url,
								'category_status' => 0,						
							);
							$this->db->insert('categories',$data);					
							$new_category_id    = $this->db->insert_id();
							$new_subcategory_id = '';
						}
						else
						{
							$rst 			 	= $cat->row();
							$new_category_id 	= $rst->category_id;
							$new_subcategory_id = '';
						}					
					}
					 
					/*Add new store name*/
					
					if($offer_name != "")
					{
					
						$this->db->where('affiliate_name',$offer_name);
						$aff = $this->db->get('affiliates');
						if($aff->num_rows()==0)
						{	
							$offer_url  = $this->admin_model->seoUrl($offer_name);
							$data = array(
								'affiliate_name'   => $offer_name,
								'affiliate_url'    => $offer_url,
								'affiliate_status' => '1',
							);
							$this->db->insert('affiliates',$data);
							$new_store_id = $this->db->insert_id();
						}
						else
						{
							$result 	  = $aff->row();
							$new_store_id = $result->affiliate_id;
						}
					}

					//$this->db->where('promo_id',$promo_id);
					//$result = $this->db->get('coupons');
					//echo $result->num_rows;
					//if($result->num_rows == 0){	

					if($offer_name)
					{
						$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`, `code`, `offer_page`, `start_date`,`expiry_date`,`featured`,`exclusive`)
						VALUES ('$offer_name','$title', '$description','$type', '$code', '$offer_page', '$start_date','$expiry_date','$featured','$exclusive')");
						//echo "hai";
					}
					else
					{
						$duplicate+=1;
						$duplicate_promo_id .= $promo_id.', ';
					}

					unset($cont);
				}
			}
			if($affiliate_name == 'rakuten')
			{
				foreach($content['link'] as $cont)
				{
					//echo "<pre>"; print_r($cont); exit; 
					$new_category_id='';
					$type = ''; 
					
					/*Coupon table details 09-06-16.*/
					
				  	//echo $offer_name; echo "<br>";exit;
				  	//$offer_name      = explode(' ',$cont_offname);
				  	//$offer_name      = array_pop($offer_name);
					
					$old_offer_name  = $cont['offerdescription'];
					$off_name_split  = explode(' ',$old_offer_name);
					$new_offer_name  = rtrim($off_name_split[0].' '.$off_name_split[1].' '.$off_name_split[2],',');
					$offer_name      = rtrim($new_offer_name,'-');
					//echo $offer_name; echo "<br>";
					//}exit;		 
					$title 		     = $cont['couponrestriction'];
					$description     = $cont['offerdescription'];
					$code 		     = $cont['couponcode'];
					if($code !='')
					{
						$type = 'Coupon';	
					}
					else
					{
						$type = 'Promotion';
					}
					$offer_page 	 = $cont['clickurl'];
					$start_date  	 = date('Y-m-d',strtotime($cont['offerstartdate']));	
					$expiry_date 	 = date('Y-m-d',strtotime($cont['offerenddate']));	
					$affiliate_name	 = $cont['advertisername'];
					
					if($start_date == '1969-12-31')
					{
						$start_date = date('Y-m-d');
					}
					if($expiry_date == '1969-12-31')
					{
						$expiry_date = date('Y-m-d'); 
					}

					/*End coupon table details*/
					/*Add new category name in categories table 10-06-16.*/
					$old_category_name = $cont['categories']['category'];
					if(is_array($old_category_name))
					{
						$category_name = $old_category_name[0];
					} 
					else
					{
						$category_name = $old_category_name;
					}
					//echo $category_name; echo "<br>"; 
				
					//exit;
					//$name 		   = join($category_name);

					if($category_name!="")
					{	   		
						$this->db->where('category_name',$category_name);
						$cat = $this->db->get('categories');
						if($cat->num_rows()==0)
						{	
							$seo_url  = $this->admin_model->seoUrl($category_name);
							$data     = array(
								'category_name'   => $category_name,
								'category_url'    => $seo_url,
								'category_status' => 0,						
							);
							$this->db->insert('categories',$data);					
							$new_category_id    = $this->db->insert_id();
							$new_subcategory_id = '';
						}
						else
						{
							$rst 			 	= $cat->row();
							$new_category_id 	= $rst->category_id;
							$new_subcategory_id = '';
						}					
					}
					
					/*Category table end*/ 
					/*Add new store name in affiliates table 10-06-16.*/
					
					if($affiliate_name != "")
					{
					
						$this->db->where('affiliate_name',$affiliate_name);
						$aff = $this->db->get('affiliates');
						if($aff->num_rows()==0)
						{	
							$offer_url  = $this->admin_model->seoUrl($affiliate_name);
							$data = array(
								'affiliate_name'   => $affiliate_name,
								'affiliate_url'    => $offer_url,
								'affiliate_status' => '1',
							);
							$this->db->insert('affiliates',$data);
							$new_store_id = $this->db->insert_id();
						}
						else
						{
							$result 	  = $aff->row();
							$new_store_id = $result->affiliate_id;
						}
					}

					//$this->db->where('promo_id',$promo_id);
					//$result = $this->db->get('coupons');
					//echo $result->num_rows;
					//if($result->num_rows == 0){	

					if($offer_name)
					{
						$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`, `code`, `offer_page`, `start_date`,`expiry_date`,`featured`,`exclusive`)
						VALUES ('$offer_name','$title', '$description','$type', '$code', '$offer_page', '$start_date','$expiry_date','$featured','$exclusive')");
						//echo "hai";
					}
					else
					{
						$duplicate+=1;
						$duplicate_promo_id .= $promo_id.', ';
					}

					unset($cont);
				}
			}
			if($affiliate_name == 'cityads')
			{
				foreach($content['data']['items'] as $cont)
				{
					$new_category_id='';
					 
					/*Coupon Description details*/
					if (array_key_exists('description', $cont)) 
					{
						$description = $this->db->escape_str($cont['description']);
					}	
					else
					{
						$description = '';
					}

					/*Coupon Name details*/
					if (array_key_exists('name', $cont)) 
					{
						$category_name =$this->db->escape_str($cont['name']);
					}	
					else
					{
						$category_name = '';				
					}

					/*Category Name details*/
					if (array_key_exists('action_category_name', $cont)) 
					{
						//	echo 'cate';
						$offer_category_name =$this->db->escape_str($cont['action_category_name']);
					}	
					else
					{
						$offer_category_name = '';
										
					}

					/*Coupon code details*/
					if (array_key_exists('promo_code', $cont)) 
					{
						$code =$this->db->escape_str($cont['promo_code']);
					
					}else					
					{						
						$code = '';
					}

					/*Coupon offer page details*/
					if (array_key_exists('url', $cont)) 
					{
						$offer_page =$this->db->escape_str($cont['url']);
					
					}else					
					{						
						$offer_page = '';
					}


					//	echo "description".$description; echo "<br>";
					//echo "$category_name".$category_name; echo "<br><br>";
					//echo "$offer category_name".$offer_category_name; echo "<br>";
					//echo $code; echo "<br>";
					//echo $offer_page; echo "<br>"; 
				//}
				//}
				//exit; 
					//print_r($content['data']);exit;
					//echo $cont['offer_name']; exit;					
					//$offname 		 = preg_split("/[ ]/", $cont_offname); 
					//$name    		 = preg_split("/[.]/", $offname[0]);
					//print_r($name);exit;
					//echo $name[0], "<br />"; exit; 
					//$offer_name    = mysql_real_escape_string($name[0]);
					//$offer_url     = $offname[0]; 
				 	//$description   = $this->db->escape_str($cont['coupon_description']);
					//$category_name = $this->db->escape_str($cont['category']);
					//$code 		 = $cont['coupon_code'];
					//$offer_page 	 = $this->db->escape_str($cont['link']);	

					$offer_name  = $cont['offer_name'];
					$promo_id 	 = $cont['offer_id'];
					$title 		 = $cont['coupon_title'];
					$type 		 = $cont['coupon_type'];		
					$start_date  = date('Y-m-d',strtotime($cont['start_date']));	
					$expiry_date = date('Y-m-d',strtotime($cont['active_to']));
					//echo $expiry_date;echo "<br>";
					//echo $start_date; exit;
					$featured    = $cont['featured'];
					$exclusive   = $cont['exclusive'];
					
					
					/*Add new category name*/

					if($offer_category_name!="")
					{	
	                    		
						$this->db->where('category_name',$offer_category_name);
						$cat = $this->db->get('categories');
						if($cat->num_rows()==0)
						{
							
							$seo_url  = $this->admin_model->seoUrl($offer_category_name);
							$data     = array(
								'category_name'   => $offer_category_name,
								'category_url'    => $seo_url,
								'category_status' => 0,						
							);
							$this->db->insert('categories',$data);					
							$new_category_id    = $this->db->insert_id();
							$new_subcategory_id = '';
						}
						else
						{
							$rst 			 	= $cat->row();
							$new_category_id 	= $rst->category_id;
							$new_subcategory_id = '';
						}					
					}
					//$offer_name = preg_replace('/[\s!]/','',$offer_name);
					//	echo "offer Name".$offer_name;  exit;
					/*Add new store name*/
					
					/*if($offer_name != "")
					{
					
						$this->db->where('affiliate_name',$offer_name);
						$aff = $this->db->get('affiliates');
						if($aff->num_rows()==0)
						{	
							$offer_url  = $this->admin_model->seoUrl($offer_name);
							$data = array(
								'affiliate_name'   => $offer_name,
								'affiliate_url'    => $offer_url,
								'affiliate_status' => '1',
							);
							$this->db->insert('affiliates',$data);
							$new_store_id = $this->db->insert_id();
						}
						else
						{
							$result 	  = $aff->row();
							$new_store_id = $result->affiliate_id;
						}
					}*/

					//$this->db->where('promo_id',$promo_id);
					//$result = $this->db->get('coupons');
					//echo $result->num_rows;
					//if($result->num_rows == 0){	

					if($offer_name)
					{
						$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`, `code`, `offer_page`, `start_date`,`expiry_date`,`featured`,`exclusive`)
						VALUES ('$offer_name','$title', '$description','$type', '$code', '$offer_page', '$start_date','$expiry_date','$featured','$exclusive')");
						//echo "hai";
					}
					else
					{
						$duplicate+=1;
						$duplicate_promo_id .= $promo_id.', ';
					}

					unset($cont);
				}

				$array['duplicate'] = $duplicate;
				$array['promo_id'] = rtrim($duplicate_promo_id,', ');
			}
			if($affiliate_name == 'zanox')
			{
				foreach($content['incentiveItems']['incentiveItem'] as $cont)
				{
					
					$new_category_id='';
					 
					/*Coupon Description details*/
					if (array_key_exists('description', $cont)) 
					{
						$description = $this->db->escape_str($cont['admedia']['admediumItem']['description']);
					}	
					else
					{
						$description = '';
					}

					/*Coupon Name details*/
					if (array_key_exists('name', $cont)) 
					{
						$category_name =$this->db->escape_str($cont['name']);
					}	
					else
					{
						$category_name = '';				
					}

					/*Coupon code details*/
					if (array_key_exists('couponCode', $cont)) 
					{
						$code =$this->db->escape_str($cont['couponCode']);
					
					}else					
					{						
						$code = '';
					}

					
					/*Coupon table details 09-06-16.*/

					$cont_offname    = $cont['admedia']['admediumItem']['program']['$'];
					$offer_name      = trim($cont_offname, " BR");
				  //$offer_name      = explode(' ',$cont_offname);
				  //$offer_name      = array_pop($offer_name);
					
					$title 		     = $cont['title'];
					$type 		 	 = $cont['incentiveType'];
					$offer_page 	 = $cont['admedia']['admediumItem']['trackingLinks']['trackingLink'][0]['ppc'];
					$start_date  	 = date('Y-m-d',strtotime($cont['startDate']));	
					$expiry_date 	 = date('Y-m-d',strtotime($cont['endDate']));	
					$tracking 		 = $cont['admedia']['admediumItem']['trackingLinks']['trackingLink'][0]['@adspaceId'];
					$promo_id 	 	 = $cont['@id'];
					$featured    	 = $cont['featured'];
					$exclusive   	 = $cont['exclusive'];
					
					if($start_date == '')
					{
						$start_date = date('Y-m-d');
					}
					if($expiry_date == '1969-12-31')
					{
						$expiry_date = date('Y-m-d'); 
					}

					/*End coupon table details*/
				
					/*Add new category name*/

					if($offer_category_name!="")
					{	
	                    		
						$this->db->where('category_name',$offer_category_name);
						$cat = $this->db->get('categories');
						if($cat->num_rows()==0)
						{
							
							$seo_url  = $this->admin_model->seoUrl($offer_category_name);
							$data     = array(
								'category_name'   => $offer_category_name,
								'category_url'    => $seo_url,
								'category_status' => 0,						
							);
							$this->db->insert('categories',$data);					
							$new_category_id    = $this->db->insert_id();
							$new_subcategory_id = '';
						}
						else
						{
							$rst 			 	= $cat->row();
							$new_category_id 	= $rst->category_id;
							$new_subcategory_id = '';
						}					
					}
					 
					/*Add new store name*/
					
					if($offer_name != "")
					{
					
						$this->db->where('affiliate_name',$offer_name);
						$aff = $this->db->get('affiliates');
						if($aff->num_rows()==0)
						{	
							$offer_url  = $this->admin_model->seoUrl($offer_name);
							$data = array(
								'affiliate_name'   => $offer_name,
								'affiliate_url'    => $offer_url,
								'affiliate_status' => '1',
							);
							$this->db->insert('affiliates',$data);
							$new_store_id = $this->db->insert_id();
						}
						else
						{
							$result 	  = $aff->row();
							$new_store_id = $result->affiliate_id;
						}
					}

					//$this->db->where('promo_id',$promo_id);
					//$result = $this->db->get('coupons');
					//echo $result->num_rows;
					//if($result->num_rows == 0){	

					if($offer_name)
					{
						$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`, `code`, `offer_page`, `start_date`,`expiry_date`,`featured`,`exclusive`)
						VALUES ('$offer_name','$title', '$description','$type', '$code', '$offer_page', '$start_date','$expiry_date','$featured','$exclusive')");
						//echo "hai";
					}
					else
					{
						$duplicate+=1;
						$duplicate_promo_id .= $promo_id.', ';
					}

					unset($cont);
				}
			}
		}	
		//print_r($array);die;
		return $array;		
	}
	/*End*/

/*report */

function import_apireports($content,$affiliate_name)
	{
		//echo "<pre>";print_r($content);exit;
		//$tracking =$this->input->post('tracking_id');
		//echo $affiliate_name; //exit;
		//print_r($content);
		//exit;
		$coupon_type = '';
		if(count($content)!=0)
		{
			$array 	   = array();
			$duplicate = 0;
			$duplicate_promo_id = '';
					
			//echo $affiliate_name; echo "<br>"; exit;

			if($affiliate_name == 'zanox')
			{
				foreach($content['incentiveItems']['incentiveItem'] as $cont)
				{
					
					$new_category_id='';
					 
					/*Coupon Description details*/
					if (array_key_exists('description', $cont)) 
					{
						$description = $this->db->escape_str($cont['admedia']['admediumItem']['description']);
					}	
					else
					{
						$description = '';
					}

					/*Coupon Name details*/
					if (array_key_exists('AdvertiserName', $cont)) 
					{
						$category_name =$this->db->escape_str($cont['name']);
					}	
					else
					{
						$category_name = '';				
					}

					/*Coupon code details*/
					if (array_key_exists('couponCode', $cont)) 
					{
						$code =$this->db->escape_str($cont['couponCode']);
					
					}else					
					{						
						$code = '';
					}

					
					/*Coupon table details 09-06-16.*/

					$cont_offname    = $cont['admedia']['admediumItem']['program']['$'];
					$offer_name      = trim($cont_offname, " BR");
				  //$offer_name      = explode(' ',$cont_offname);
				  //$offer_name      = array_pop($offer_name);
					
					$title 		     = $cont['title'];
					$type 		 	 = $cont['incentiveType'];
					$offer_page 	 = $cont['admedia']['admediumItem']['trackingLinks']['trackingLink'][0]['ppc'];
					$start_date  	 = date('Y-m-d',strtotime($cont['startDate']));	
					$expiry_date 	 = date('Y-m-d',strtotime($cont['endDate']));	
					$tracking 		 = $cont['admedia']['admediumItem']['trackingLinks']['trackingLink'][0]['@adspaceId'];
					$promo_id 	 	 = $cont['@id'];
					$featured    	 = $cont['featured'];
					$exclusive   	 = $cont['exclusive'];
					
					if($start_date == '')
					{
						$start_date = date('Y-m-d');
					}
					if($expiry_date == '1969-12-31')
					{
						$expiry_date = date('Y-m-d'); 
					}

					/*End coupon table details*/
				
					/*Add new category name*/

					if($offer_category_name!="")
					{	
	                    		
						$this->db->where('category_name',$offer_category_name);
						$cat = $this->db->get('categories');
						if($cat->num_rows()==0)
						{
							
							$seo_url  = $this->admin_model->seoUrl($offer_category_name);
							$data     = array(
								'category_name'   => $offer_category_name,
								'category_url'    => $seo_url,
								'category_status' => 0,						
							);
							$this->db->insert('categories',$data);					
							$new_category_id    = $this->db->insert_id();
							$new_subcategory_id = '';
						}
						else
						{
							$rst 			 	= $cat->row();
							$new_category_id 	= $rst->category_id;
							$new_subcategory_id = '';
						}					
					}
					 
					/*Add new store name*/
					
					if($offer_name != "")
					{
					
						$this->db->where('affiliate_name',$offer_name);
						$aff = $this->db->get('affiliates');
						if($aff->num_rows()==0)
						{	
							$offer_url  = $this->admin_model->seoUrl($offer_name);
							$data = array(
								'affiliate_name'   => $offer_name,
								'affiliate_url'    => $offer_url,
								'affiliate_status' => '1',
							);
							$this->db->insert('affiliates',$data);
							$new_store_id = $this->db->insert_id();
						}
						else
						{
							$result 	  = $aff->row();
							$new_store_id = $result->affiliate_id;
						}
					}

					//$this->db->where('promo_id',$promo_id);
					//$result = $this->db->get('coupons');
					//echo $result->num_rows;
					//if($result->num_rows == 0){	

					if($offer_name)
					{
						$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`, `code`, `offer_page`, `start_date`,`expiry_date`,`featured`,`exclusive`)
						VALUES ('$offer_name','$title', '$description','$type', '$code', '$offer_page', '$start_date','$expiry_date','$featured','$exclusive')");
						//echo "hai";
					}
					else
					{
						$duplicate+=1;
						$duplicate_promo_id .= $promo_id.', ';
					}

					unset($cont);
				}
			}
			if($affiliate_name == 'rakuten')
			{
	//Transaction Date
				//print_r($content); 
			//$contentj=json_encode($content); 
		//	echo json_decode($contentj);
			//exit;
				foreach($content as $cont)
				{
					
					 $individual_id=$cont['Transaction ID'];
					 	$Sales=$cont['Sales'];
					 	$Baseline_Commission=$cont['Baseline Commission'];
					 	$Total_Commission=$cont['Total Commission'];
					 	$Offer_ID=$cont['Offer ID'];
					 $Offer_Name=$cont['Offer Name'];
					 	$Product_Name=$cont['Product Name'];
					 $Report_Name=$cont['id'];
					 $Member_id=$cont['MID'];
				 $Product_Name=$cont['Product Name'];
				 $currency=$cont['currency'];
					$Advertiser_Name=$cont['Advertiser Name'];
				//echo new date();
					$start_date  	 = date('Y-m-d',strtotime($cont['Transaction Date']));	
					//echo $start_date;
					$now = date('Y-m-d H:i:s');
					$current_date=date('Y-m-d',strtotime($now));
					//echo $current_date;
					//echo $now;
				//	exit;
				//cashback
					//transcationdetail
					//reports


					if($individual_id !='')
					{
						
						$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`affiliate_id`,`cashback_amount`, `transaction_date`, `status`, `date_added`,`referral`,`txn_id`,`transaction_amount`,`commission`,`type_cb`,`plataform`)
						VALUES ('$Member_id','$Advertiser_Name', '$Advertiser_Name','$Sales', '$start_date', 'Pending', '$start_date','0','$individual_id','$Total_Commission','$Baseline_Commission','0','0')");

					$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_reason`,`transation_amount`,`transation_date`, `transation_status`, `table`, `new_txn_id`)
						VALUES ('$individual_id','rakuten transaction', '$Total_Commission', '$start_date', 'Pending', 'tbl_report','0')");
//exit;
$this->db->query("INSERT INTO `tbl_report` (`offer_provider`,`date`,`sale_amount`,`transaction_id`,`user_tracking_id`,`is_cashback`,`cashback_amount`,`total_Cashback_paid`, `status`, `last_updated`)
		VALUES ('$Product_Name','$start_date','$Sales', '$individual_id','$Member_id','1','$Total_Commission', '$Total_Commission', '$now', '$now')");


					}
					else
					{				/*
							$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`affiliate_id`,`cashback_amount`, `transaction_date`, `status`, `date_added`,`referral`,`txn_id`,`transaction_amount`,`commission`,`type_cb`,`plataform`)
						VALUES ('$Member_id','$Advertiser_Name', '$Advertiser_Name','$Sales', '$start_date', 'Pending', '$start_date','0','$individual_id','$Total_Commission','$Baseline_Commission','0','0')");*/
						$duplicate+=1;
						$duplicate_promo_id .= $promo_id.', ';
					}
			//prakash
					unset($cont);
				}
				$array['duplicate'] = $duplicate;
				$array['promo_id'] = rtrim($duplicate_promo_id,', ');
			}
			if($affiliate_name == 'cityads')
			{
			//	echo '<pre>';
			print_r($content); //exit;
				foreach($content['data']['items'] as $cont)
				{
					$new_category_id='';
					 
					/*Coupon Description details*/
					if (array_key_exists('actionName', $cont)) 
					{
		$name = $this->db->escape_str($cont['actionName']);

					}	
					else
					{
						$description = '';
					}

					/*Coupon Name details*/
					if (array_key_exists('clickUniqCount', $cont)) 
					{
						$clickUniqCount =$this->db->escape_str($cont['clickUniqCount']);
					}	
					else
					{
						$category_name = '';				
					}

					/*Category Name details*/
					if (array_key_exists('action_category_name', $cont)) 
					{
						//	echo 'cate';
				$offer_category_name =$this->db->escape_str($cont['action_category_name']);
					}	
					else
					{
						$offer_category_name = '';
										
					}

					


					//	echo "description".$description; echo "<br>";
					//echo "$category_name".$category_name; echo "<br><br>";
					//echo "$offer category_name".$offer_category_name; echo "<br>";
					//echo $code; echo "<br>";
					//echo $offer_page; echo "<br>"; 
				//}
				//}
				//exit; 
					//print_r($content['data']);exit;
					//echo $cont['offer_name']; exit;					
					//$offname 		 = preg_split("/[ ]/", $cont_offname); 
					//$name    		 = preg_split("/[.]/", $offname[0]);
					//print_r($name);exit;
					//echo $name[0], "<br />"; exit; 
					//$offer_name    = mysql_real_escape_string($name[0]);
					//$offer_url     = $offname[0]; 
				 	//$description   = $this->db->escape_str($cont['coupon_description']);
					//$category_name = $this->db->escape_str($cont['category']);
					//$code 		 = $cont['coupon_code'];
					//$offer_page 	 = $this->db->escape_str($cont['link']);	

					$request_id  = $cont['request_id'];
					$actionID 	 = $cont['actionID'];
					//echo $actionID;
					// exit;
					/*$title 		 = $cont['coupon_title'];
					$type 		 = $cont['coupon_type'];		
					$start_date  = date('Y-m-d',strtotime($cont['start_date']));	
					$expiry_date = date('Y-m-d',strtotime($cont['active_to']));*/
					//echo $expiry_date;echo "<br>";
					//echo $start_date; exit;
					$typeOffers    = $cont['typeOffers'];
					$exclusive   = $cont['exclusive'];
					$Baseline_Commission=$cont['commissionOpen'];
					if($Baseline_Commission!='')
					{
$Baseline_Commission=$cont['commissionOpen'];
					}else{
						$Baseline_Commission='0';
					}
					$Total_Commission=$cont['commissionApproved'];
				if($Total_Commission!='')
				{

					$Total_Commission=$cont['commissionApproved'];

					}else
					{
						$Total_Commission='0';
					}
					if($Sales!=''){
						$Sales=$cont['saleApproved'];

					}else{
						$Sales='0';
					}

				$now = date('Y-m-d H:i:s');
				
					$current_date=date('Y-m-d',strtotime($now));
					$rand_start = rand(1,500);
				

					if($offer_name)
					{
						$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`affiliate_id`,`cashback_amount`, `transaction_date`, `status`, `date_added`,`referral`,`txn_id`,`transaction_amount`,`commission`,`type_cb`,`plataform`)
						VALUES ('$actionID','$actionID', '$name','$Sales', '$now', 'Pending', '$ow','0','$rand_start','$Total_Commission','$Baseline_Commission','0','0')");

						// 	
						//echo "hai";


					$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_reason`,`transation_amount`,`transation_date`, `transation_status`, `table`, `new_txn_id`)
						VALUES ('$rand_start','cityads transaction', '$Total_Commission', '$now', 'Pending', 'tbl_report','0')");
//exit;
$this->db->query("INSERT INTO `tbl_report` (`offer_provider`,`date`,`sale_amount`,`transaction_id`,`user_tracking_id`,`is_cashback`,`cashback_amount`,`total_Cashback_paid`, `status`, `last_updated`)
		VALUES ('$name','$now','$Sales', '$rand_start','$actionID','1','$Total_Commission', '$Total_Commission', '$now', '$now')");


					}
					else
					{
						$duplicate+=1;
						$duplicate_promo_id .= $promo_id.', ';
					}

					unset($cont);
				}

				$array['duplicate'] = $duplicate;
				$array['promo_id'] = rtrim($duplicate_promo_id,', ');
			}
			if($affiliate_name == 'afilio')
			{
				print_r($content); 
				//exit;


				foreach($content as $cont)
				{
					$sale_id=$cont['list']['sale']['saleid']['value'];
							$sale_id=$content['list']['sale']['saleid']['value'];
					 echo $sale_id; exit;
$status=$content['list']['sale']['status']['value'];
$progid=$content['list']['sale']['progid']['value'];
$order_id=$content['list']['sale']['order_id']['value'];
$order_price=$array['list']['sale']['order_price']['value'];
$comission=$array['list']['sale']['comission']['value'];
$date=$array['list']['sale']['date']['value'];
$payment=$array['list']['sale']['payment']['value'];
$xtra=$array['list']['sale']['xtra']['value'];
$aff_xtra=$array['list']['sale']['aff_xtra']['value'];	

					/*Coupon Description details*/
					if (array_key_exists('description', $cont)) 
					{
						$description = $this->db->escape_str($cont['admedia']['admediumItem']['description']);
					}	
					else
					{
						$description = '';
					}

					/*Coupon Name details*/
					if (array_key_exists('name', $cont)) 
					{
						$category_name =$this->db->escape_str($cont['name']);
					}	
					else
					{
						$category_name = '';				
					}

					/*Coupon code details*/
					if (array_key_exists('couponCode', $cont)) 
					{
						$code =$this->db->escape_str($cont['couponCode']);
					
					}else					
					{						
						$code = '';
					}

					
					/*Coupon table details 09-06-16.*/

					$cont_offname    = $cont['admedia']['admediumItem']['program']['$'];
					$offer_name      = trim($cont_offname, " BR");
				  //$offer_name      = explode(' ',$cont_offname);
				  //$offer_name      = array_pop($offer_name);
					
					$title 		     = $cont['title'];
					$type 		 	 = $cont['incentiveType'];
					$offer_page 	 = $cont['admedia']['admediumItem']['trackingLinks']['trackingLink'][0]['ppc'];
					$start_date  	 = date('Y-m-d',strtotime($cont['startDate']));	
					$expiry_date 	 = date('Y-m-d',strtotime($cont['endDate']));	
					$tracking 		 = $cont['admedia']['admediumItem']['trackingLinks']['trackingLink'][0]['@adspaceId'];
					$promo_id 	 	 = $cont['@id'];
					$featured    	 = $cont['featured'];
					$exclusive   	 = $cont['exclusive'];
					
					if($start_date == '')
					{
						$start_date = date('Y-m-d');
					}
					if($expiry_date == '1969-12-31')
					{
						$expiry_date = date('Y-m-d'); 
					}

					/*End coupon table details*/
				
					/*Add new category name*/

					if($offer_category_name!="")
					{	
	                    		
						$this->db->where('category_name',$offer_category_name);
						$cat = $this->db->get('categories');
						if($cat->num_rows()==0)
						{
							
							$seo_url  = $this->admin_model->seoUrl($offer_category_name);
							$data     = array(
								'category_name'   => $offer_category_name,
								'category_url'    => $seo_url,
								'category_status' => 0,						
							);
							$this->db->insert('categories',$data);					
							$new_category_id    = $this->db->insert_id();
							$new_subcategory_id = '';
						}
						else
						{
							$rst 			 	= $cat->row();
							$new_category_id 	= $rst->category_id;
							$new_subcategory_id = '';
						}					
					}
					 
					/*Add new store name*/
					
					if($offer_name != "")
					{
					
						$this->db->where('affiliate_name',$offer_name);
						$aff = $this->db->get('affiliates');
						if($aff->num_rows()==0)
						{	
							$offer_url  = $this->admin_model->seoUrl($offer_name);
							$data = array(
								'affiliate_name'   => $offer_name,
								'affiliate_url'    => $offer_url,
								'affiliate_status' => '1',
							);
							$this->db->insert('affiliates',$data);
							$new_store_id = $this->db->insert_id();
						}
						else
						{
							$result 	  = $aff->row();
							$new_store_id = $result->affiliate_id;
						}
					}

					//$this->db->where('promo_id',$promo_id);
					//$result = $this->db->get('coupons');
					//echo $result->num_rows;
					//if($result->num_rows == 0){	

					if($offer_name)
					{
						$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`, `code`, `offer_page`, `start_date`,`expiry_date`,`featured`,`exclusive`)
						VALUES ('$offer_name','$title', '$description','$type', '$code', '$offer_page', '$start_date','$expiry_date','$featured','$exclusive')");
						//echo "hai";
					}
					else
					{
						$duplicate+=1;
						$duplicate_promo_id .= $promo_id.', ';
					}

					unset($cont);
				}
			}
		}	
		//print_r($array);die;
		return $array;		
	}

}
?>
