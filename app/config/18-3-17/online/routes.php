<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$default_controller = "cashback";
$route['default_controller'] = "Cashback";
//$route['404_override'] = '404 Error';

$route['404_override'] = 'cashback/errorcode';

//$route['about-us'] = "cashback/cms/about-us";
//echo BASEPATH .'database/DB.php';
require_once( BASEPATH .'database/DB.php');
$db =& DB();
$query = $db->query('SELECT `cms_title` FROM tbl_cms where cms_status=1');
$result = $query->result();
//echo "<pre>"; print_r($result); 
$arr_cms = array();
foreach($result as $cmss)
{
	$arr_cms[] = $cmss->cms_title;
}
/*New code hide 23-9-16*/
$cms = "cashback/cms";
$controller_exceptions = $arr_cms;  
/*array("sasass","about-us","oportunidade-unica","perguntas-frequentes","terms-of-service","press-release","poltica-de-privacidade")*/
foreach($controller_exceptions as $v) {
  $route[$v] = "$cms/".$v;
  $route[$v."/(.*)"] = "$cms/".$v.'/$1';
}
/*End 23-9-16*/


$controller_exceptions1 = array("log_update_count_details","update_count_details","perguntas_frequentes","banner_clickcount","bannerdetails","deeplink_urldetails","coupon_url_details","store_param_details","ofertas","promocao","cms","cupom","un_subscribe_signup","un_subscribe","passwordreset","maintenance","generate_xml","rating","como_funciona","login","desconto","product_detail","add_rating","notifymail_update","stores","chk_invalid","register","cadastro","check_email","minha_conta","update_account","change_password","bankpayment","cheque_payment","forgetpassword","complete","reset_password","logout","email_subscribe","header_menu","indique_e_ganhe","invite_mail","contato","contact_form","resgate","loja_nao_avisou_compra","resgatar_compra_nao_avisada","products","add_withdraw","products","old_stores","cupom_desconto","busca","logincheck","verify_account","visit_shop","ir_loja","saidas_para_loja","ajaxcall","missing_Cashback_submit","get_clicked_store_details","top_cashback","blog","blog_details","blog_comment","produtos","ajaxsess_setpremiumcategory","pageconfig","barato","submit_ratings","addtocart","cart_listing_page","delete_cart","check_amount","payusing_site","thankyou","PayUmoney","Payment_Success","Payment_Failure","orders","extrato","store_ajax","password_reset","testt","desconto_cupom","under_maintance","downsp","getstores_listjson","getcitys_listjson","email_subscribe_shoppping","change_location","loja_cancelou_minha_compra","add_missing_approval","missing_approval_submit"); 
foreach($controller_exceptions1 as $v1) {
   $m1 = str_replace('_', '-', $v1);
  //echo"<pre>";print_r($m1) ;
  $route[$m1] = "$default_controller/".$v1;

  $route[$m1."/(.*)"] = "$default_controller/".$v1.'/$1';

  
}
	$route["cadastro-(.*)"] = "$default_controller/cadastro";

  $m2 = str_replace('_', '-','adicionar_bonus');
	
  $route[$m2] = 'extension/adicionar_bonus';
  
  $route['redirecionar'."/(.*)"] = 'extension/redirecionar/$1';	
  
	
/* End of file routes.php */
/* Location: ./application/config/routes.php */
