<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*!
* HybridAuth
* http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
* (c) 2009-2012, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
*/

// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

$query 			 = mysql_fetch_array(mysql_query("select * from admin where admin_id='1'"));
$google_key 	 =  $query['google_key'];
$google_secret 	 =  $query['google_secret'];
$facebook_key 	 =  $query['facebook_key'];   //seetha
$facebook_secret =  $query['facebook_secret'];
$yahoo_key 		 =  $query['yahoo_key'];
$yahoo_secret 	 =  $query['yahoo_secret'];

/*New code for hotmail configuration 6-9-16*/
$hotmail_key 	 =  $query['hotmail_key'];
$hotmail_secret  =  $query['hotmail_secret'];
/*End*/


$config['base_url'] = "http://".$_SERVER['HTTP_HOST'];
$config['base_url'] .= preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/'; 

$config =
	array(
		// set on "base_url" the relative url that point to HybridAuth Endpoint
		'base_url' => '/HAuth/endpoint',

		"providers" => array (
			// openid providers
			"OpenID" => array (
				"enabled" => true
			),
			
			"Yahoo" => array (  
				"enabled" => true,
				"keys"    => array ("key" =>$yahoo_key, "secret" =>$yahoo_secret),
			),

			"AOL"  => array (
				"enabled" => true
			),

			"Google" => array (
				"enabled" => true,
				"keys"    => array ("id" =>$google_key, "secret" =>$google_secret),
			),

			"Facebook" => array (
				"enabled" => true,
				"keys"    => array ("id" => $facebook_key, "secret" => $facebook_secret),
				 //"keys"    => array ( "id" => "1702659543356747", "secret" => "d1f4343c7fb2c646692386ee35ef9625" ),
                               
			),

			"Twitter" => array (
				"enabled" => true,
				"keys"    => array ("key" => "", "secret" => "")
			),

			// windows live
			"Live" => array (
				"enabled" => true,
				"keys"    => array ("id" => $hotmail_key, "secret" => $hotmail_secret),
			),

			"MySpace" => array (
				"enabled" => true,
				"keys"    => array ( "key" => "", "secret" => "" )
			),

			"LinkedIn" => array (
				"enabled" => true,
				"keys"    => array ( "key" => "", "secret" => "" )
			),

			"Foursquare" => array (
				"enabled" => true,
				"keys"    => array ( "id" => "", "secret" => "" )
			),
		),

		// if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
		"debug_mode" => (ENVIRONMENT == 'development'),

		"debug_file" => APPPATH.'/logs/hybridauth.log',
	);


/* End of file hybridauthlib.php */
/* Location: ./application/config/hybridauthlib.php */