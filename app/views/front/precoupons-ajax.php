<?php 
foreach($store_coupons as $fetrest)
{ 

    $shoppingcoupon_id     = $fetrest->shoppingcoupon_id;
    $db_offer_name         = $fetrest->offer_name;
    $db_coupon_description = $fetrest->description;
    $db_coupon_image       = $fetrest->coupon_image;
    $db_expiry_date        = $fetrest->expiry_date;
    $db_cp_price           = $fetrest->amount;
    $exp_db_coupon_image   = explode(",",$db_coupon_image);  
    $f_dbcouponfirst_img   = $exp_db_coupon_image[0];
    $len_db_offer_name     = strlen($db_offer_name);  
    $len_db_coupon_desc    = strlen($db_coupon_description);  
    $total_price            = $fetrest->price; 
    $sales_price            = $fetrest->amount;
    /*New code for precentage details 21-7-16*/
    $sales_price         = ($total_price - $sales_price);
    $discount_percentage = (($sales_price/$total_price)*100);
    $discount_percentage = preg_replace('/\./', ',',number_format(round($discount_percentage ,2),2));
    /*end*/

    /*new code for shopping coupons get store percentage details 27-1-17*/
    
    $store_cash_type  = $storedetails->affiliate_cashback_type;
    $cashback_amounts = $storedetails->cashback_percentage;

    if($store_cash_type == "Flat")
    {
        $new_cash_amt      = $cashback_amounts;
        $cashback_amounts  = "R$". $cashback_amounts;
        $cashback_amount   = "R$". $cashback_amounts;
        $cashback_amt      = $cashback_amount;
        $cashback_amts     = $this->front_model->currency_format($cashback_amt);
        //$cashback_amts     = 4.9765;
        $cashback_amts     = str_replace(',','.', $cashback_amts);
        $cashback_amts     = round($cashback_amts,2);
    } 
    if($store_cash_type == "Percentage")
    {
        $new_cash_amt      = $cashback_amounts;
        $cashback_amounts  = $cashback_amounts ."%";
        $cashback_amount   = $cashback_amounts ."%";
        $cashback_amt      = (($db_cp_price)*($cashback_amount)/100);
        $cashback_amts     = $this->front_model->currency_format($cashback_amt);
        //$cashback_amts     = 4.9765;
        $cashback_amts     = str_replace(',','.', $cashback_amts);
        $cashback_amts     = round($cashback_amts,2);
    } 
    if(strstr($cashback_amts, '.'))
    {
    }
    else 
    {
        $cashback_amts = $cashback_amts.'.00';
    }

    $cashback_amts = str_replace('.',',', $cashback_amts); 
    /*end 27-1-17*/

    if($len_db_offer_name>=20)
    {
        $f_dbcp_name=substr($db_offer_name,0,30)."..."; 
    }
    else
    {
        $f_dbcp_name=$db_offer_name; 
    }
    if($len_db_coupon_desc>=54)
    {
        $f_dbcp_desc=substr($db_coupon_description,0,20)."..."; 
    }
    else
    {
        $f_dbcp_desc=$db_coupon_description; 
    } 
    $getremain_days=$this->front_model->find_remainingdays($fetrest->expiry_date);  
    ?>    
    
    <!-- new code for category name included in url 30-9-16 -->
    <?php 
        $category_ids  = $fetrest->category;
        $category_name = $this->db->query("select * from premium_categories where category_id=$category_ids")->row('category_url'); 
    ?>
    <!-- End 30-9-16 -->

    <!-- <a href="<?php echo base_url(); ?>promocaonew/<?php echo urlencode($fetrest->seo_url); ?>"> -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="prod-photodetblk">
            <?php 
            $image_type          = $fetrest->img_type;
            $db_coupon_image     = $fetrest->coupon_image;
            if($image_type == 'url_image')
            {
                ?>
                <div class="photo">
                    <img alt="a" class="img-responsive" style="height:292px; width:264px" src="<?php echo $db_coupon_image; ?>" alt="Image Alternative text" title="Hot mixer">
                </div>
            <?php
            }
            else
            {
                ?>
                <div class="photo">
                    <img alt="a" class="img-responsive" style="height:292px; width:264px" src="<?php echo $this->front_model->get_img_url(); ?>uploads/premium/<?php echo $db_coupon_image; ?>" alt="Image Alternative text" title="Hot mixer">
                </div>
                <?php 
            } 
            ?>
            <div class="info">
                <div class="row vers">
                    <center>
                        <h4>
                            <a href="<?php echo base_url(); ?>promocao/<?php echo urlencode($fetrest->seo_url); ?>">
                                <span class="cls_sub_text">
                                    <?php echo $f_dbcp_name; ?>
                                    <br>
                                    R$ <?php echo $this->front_model->currency_format($db_cp_price); ?> e receba R$ <?php echo $cashback_amts; ?> de volta
                                </span>
                            </a>
                        </h4>
                    </center>
                </div>
                <div class="overlay-prod ">
                    <div class="row vers hov">
                        <center>
                            <h4 class="">
                                <a href="<?php echo base_url(); ?>promocao/<?php echo $category_name;?>/<?php echo urlencode($fetrest->seo_url); ?>"><?php  echo $f_dbcp_name;  ?></a>
                            </h4>
                            <p>Pague R$ <?php echo $this->front_model->currency_format($db_cp_price); ?> e receba <br> R$<?php echo $cashback_amts; ?> de volta</p>
                            <span><?php  echo $getremain_days['days']." days ".$getremain_days['hours']." h "." remaining";   ?></span>
                            <a href="<?php echo base_url(); ?>promocao/<?php echo $category_name;?>/<?php echo urlencode($fetrest->seo_url); ?>">
                            <button type="button" class="btn btn-blu">Details</button>
                            </a>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- </a> -->
    <?php 
}
?>