<?php $this->load->view('front/header'); ?>

<!-- header content End -->
<style>
.newdate{
  display:none;
}

  .img-responsive.cls_topad {
      border-radius: 20px;
  }
  a.tooltips {
    position: relative;
    display: inline;
  }
  a.tooltips .popupcls {
   background: #3da0d5 none repeat scroll 0 0;
      border-radius: 2px;
      color: #ffffff;
      line-height: 20px;
      min-height: 30px;
      min-width: 100%;
      position: absolute;
      text-align: center;
      visibility: hidden;
      width: 250px!important;
      word-wrap: break-word; left: -5em !important;
  }
  a.tooltips .popupcls:after {
    content: '';
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -8px;
    width: 0; height: 0;
    border-top: 8px solid #3da0d5;
    border-right: 8px solid transparent;
    border-left: 8px solid transparent;
  }
  a:hover.tooltips .popupcls {
    visibility: visible;

    bottom: 30px;
    left: 50%;
    margin-left: -76px;
    z-index: 999;
  }
 
  .table-responsive {
    overflow-x: unset;
  }
</style>
 
<!-- Main Content start -->
<?php $user_id = $this->session->userdata('user_id');
$getadmindetails     = $this->front_model->getadmindetails();  
$manual_credit_image = $getadmindetails[0]->manual_credit_image;
$unique_bonus_image  = $getadmindetails[0]->unique_bonus_image;
$ref_type_one        = $getadmindetails[0]->ref_type_one;
$ref_type_two        = $getadmindetails[0]->ref_type_two;
$ref_type_three      = $getadmindetails[0]->ref_type_three;
?>
<section class="cms wow fadeInDown">
  	<div class="container">
    	<div class="heading wow bounceIn">
    		<h2>
          <span>Extrato</span>
        </h2>
     		<div class="heading_border_cms">
     			<span>
     				<img src="<?php echo $this->front_model->get_img_url();?>front/new/images/top_drop.png">
     			</span>
     		</div>
    	</div>
    	<div class="myac">
      	<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1" id="hide_fn">
		        <div class="my_account my_accblk">
		          <div class="myacc-maintab">
		            	<!-- Nav tabs -->
			            <?php $this->load->view('front/user_menu'); ?>
		          </div>
		          <!-- Tab panes -->
              <?php 
              $bonus_alert = $this->session->flashdata('bonus_alert');
              if($bonus_alert)
              {
                echo '<div class="alert alert-success">
                <button data-dismiss="alert" class="close">x</button>
                <strong>'.$bonus_alert.' </strong></div>';
              }
              ?>
		          <div class="tab-content comm-greybg">
                <!-- New code for sales funnel banner details 8-11-16 -->
                <?php include('top_banners.php');?>
                <!-- End 8-11-16-->
                <div role="tabpanel" id="acc2">
                  <?php 
                  foreach ($hover as $hs) 
                  {  
                    $ex_cash_pending          = $hs->ex_cash_pending;
                    $ex_cash_cancelled        = $hs->ex_cash_cancelled; 
                    $ex_cash_approved         = $hs->ex_cash_approved;  
                    $ex_ref_pending           = $hs->ex_ref_pending;
                    $ex_ref_cancelled         = $hs->ex_ref_cancelled; 
                    $ex_ref_approved          = $hs->ex_ref_approved; 
                    $ex_ref_pending_sec       = $hs->ex_ref_pending_sec; 
                    $ex_ref_cancelled_sec     = $hs->ex_ref_cancelled_sec;
                    $ex_ref_approved_sec      = $hs->ex_ref_approved_sec; 
                    $ex_ref_pending_third     = $hs->ex_ref_pending_third; 
                    $ex_ref_cancelled_third   = $hs->ex_ref_cancelled_third; 
                    $ex_ref_approved_third    = $hs->ex_ref_approved_third; 
                    $ex_missing_cash_approved = $hs->ex_missing_cash_approved; 
                    $ex_credit_pending        = $hs->ex_credit_pending;
                    $ex_credit_cancelled      = $hs->ex_credit_cancelled; 
                    $ex_credit_approved       = $hs->ex_credit_approved;  
                  }
                  ?>    
                  <!--account table section starts-->
                    <div class="row wow fadeInDown">
                      <div class="col-md-12 col-sm-12">
                        <div class="a">
                          <?php
                          if(empty($result) && empty($result1))
                          {
                            echo "<center>Você ainda não realizou nenhuma compra usando o Pingou.<br>
                            Assim que o fizer, todos os detalhes ficarão disponíveis nessa página.</center><br>";
                          } 
                          else
                          {
                            ?>
                            <table  id="sample_teste1" class="display zui-table zui-table-rounded table acc-table1">
                              <thead>
                                <tr>
                                  <th>Data</th>
                                  <th>Loja</th>
                                  <th>Valor (Text + Tipo)</th>
                                  <th>Status</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                $k=1;
                                //echo "<pre>"; print_r($result); //exit;
                                /*foreach($result1 as $rows)
                                {
                                  
                                  //new code for amount comma issues 26-8-16//
                                  $osiz_cash_amt  = bcdiv($rows->cashback_amount,1,2);
                                  $cashbackamount = $this->front_model->currency_format($osiz_cash_amt);
                                  $osiz_tran_amt  = bcdiv($rows->transaction_amount,1,2);
                                  $transactionamt = $this->front_model->currency_format($osiz_tran_amt);
                                  //End 26-8-16//
                                  $affiliate_details = $this->db->query("SELECT * from `affiliates` where `affiliate_id`='$rows->pingou_store_id'")->row();
                                   
                                  $affiliate_logo    = $affiliate_details->affiliate_logo; 
                                  $affiliate_name    = $affiliate_details->affiliate_name;
                                  
                                  if(!empty($affiliate_logo))
                                  {
                                    if(strpos($affiliate_logo,"http") !== false)
                                    {
                                      $affiliate_logo = '<img src='.$affiliate_logo.' width="90" height="50" /></center>';
                                    }
                                    else
                                    {
                                      $affiliate_logo = '<img src='.$this->front_model->get_img_url().'uploads/affiliates/'.$affiliate_logo.' width="90" height="50" /></center>';
                                    }
                                  }
                                  else
                                  {
                                    $affiliate_logo = "<img src=".$this->front_model->get_img_url()."uploads/affiliates/no_image.png width='90' height='50'>";
                                  }  
                                  ?>     
                                  <tr class="">
                                    <td><span class="newdate"><?php echo date('Ymd',strtotime($rows->transaction_date));?></span><?php echo date('d/m/y',strtotime($rows->transaction_date)); ?></td>
                                    <td><span class="newdate"><?php echo $affiliate_name;?></span><?php echo $affiliate_logo;?></td>
                                    <td>R$ <?php echo $cashbackamount; ?> 
                                    <br>(Transaction of R$ 
                                    <?php 
                                      echo $transactionamt;   
                                      ?>
                                      )
                                    </td>
                                    <td>
                                    <?php
                                    if($rows->status=='Pending')
                                    {
                                      ?>
                                      <label class="text-muted" style="font-weight: 500;"><?php echo "Pendente";//$rows->status;?></label>
                                      <a class="tooltips" href="#">
                                        <span class="exc-blk"><i class="fa fa-info"></i></span> 
                                        <span class="popupcls"><?=$ex_cash_pending?></span>
                                      </a>  
                                      </td>
                                      <?php
                                    }
                                    if($rows->status=='Canceled')
                                    {
                                      ?>
                                      <label class="text-muted" style="font-weight: 500;"><?php echo "Cancelado";//$rows->status;?></label>
                                      <a class="tooltips" href="#">
                                        <span class="exc-blk"><i class="fa fa-info"></i></span> 
                                        <span class="popupcls"><?=$ex_cash_cancelled?></span>
                                      </a>
                                      </td>
                                      <?php
                                    }
                                    if($rows->status=='Completed' || $rows->status=='Approved')
                                    {
                                      $status = $rows->status;

                                      if($rows->status=='Approved')
                                      {
                                        $status = 'Completed';
                                      }

                                      ?>
                                      <label class="text-muted" style="font-weight: 500;"><?php echo "Confirmado";//$status;?></label>
                                      <a class="tooltips" href="#">
                                        <span class="exc-blk"><i class="fa fa-info"></i></span> 
                                        <span class="popupcls"><?=$ex_cash_approved?></span>
                                      </a>                                         
                                      </td>
                                      <?php
                                    }
                                    ?>
                                  </tr>     
                                  <?php
                                  $k++;
                                }
                                foreach($result as $rows)
                                {  
                                  if($rows->transation_reason != 'Cashback')
                                  {
                                    $cashbackamount = $rows->cashback_transaction;
                                    $cashbackamount = $this->front_model->currency_format($cashbackamount);
                                    $transactionamt = bcdiv($rows->transation_amount,1,2);
                                    $transactionamt = $this->front_model->currency_format($transactionamt);
                                    $user_details   = $this->front_model->userdetails($rows->ref_user_tracking_id);
                                    $Exp            = explode('@',$user_details->email);
                                    if($Exp!='')
                                    {
                                      $User_Email   = $Exp[0]."@...";  
                                    }
                                    ?> 
                                    <tr>
                                      <td><span class="newdate"><?php echo date('Ymd',strtotime($rows->transaction_date));?></span><?php echo date('d/m/y',strtotime($rows->transaction_date)); ?></td> 
                                      <td><span class="newdate"><?php echo $rows->transation_reason;?></span>
                                        <?php
                                        if($rows->transation_reason=='Pending Referal Payment' || $rows->transation_reason=='Referal Payment')
                                        {
                                          //echo $Referal_Payment='Referal Payment';
                                          echo "<img src=".$this->front_model->get_img_url()."uploads/adminpro/".$ref_type_two." width='90' height='50'>";
                                        }
                                        if($rows->transation_reason == 'Referral Cashback amount')
                                        {
                                          echo "<img src=".$this->front_model->get_img_url()."uploads/adminpro/".$ref_type_one." width='90' height='50'>";
                                        }
                                        if($rows->transation_reason == 'Credit Account')
                                        {
                                          echo "<img src=".$this->front_model->get_img_url()."uploads/adminpro/".$manual_credit_image." width='90' height='50'>";
                                        }
                                        if($rows->transation_reason == 'Unic bonus amonut Added')
                                        {
                                          echo "<img src=".$this->front_model->get_img_url()."uploads/adminpro/".$unique_bonus_image." width='90' height='50'>";
                                        }
                                        if (strpos($rows->transation_reason, 'Referral Bonus for Category') !== false)
                                        {
                                          echo "<img src=".$this->front_model->get_img_url()."uploads/adminpro/".$ref_type_three." width='90' height='50'>";
                                        }
                                        if($rows->transation_reason== 'Missing Cashback request')
                                        {
                                          $miss_affiliate_details = $this->db->query("SELECT * from `affiliates` where `affiliate_url`='$rows->cashback_id'")->row();
                                          //echo "<pre>"; print_r($miss_affiliate_details);
                                          $miss_affiliate_logo    = $miss_affiliate_details->affiliate_logo; 
                                          
                                          if(!empty($miss_affiliate_logo))
                                          {
                                            if(strpos($miss_affiliate_logo,"http") !== false)
                                            {
                                              echo $miss_affiliate_logo = '<img src='.$miss_affiliate_logo.' width="90" height="50" /></center>';
                                            }
                                            else
                                            {
                                              echo $miss_affiliate_logo = '<img src='.$this->front_model->get_img_url().'uploads/affiliates/'.$miss_affiliate_logo.' width="90" height="50" /></center>';
                                            }
                                          }
                                          else
                                          {
                                            echo $miss_affiliate_logo = "<img src=".$this->front_model->get_img_url()."uploads/affiliates/no_image.png width='90' height='50'>";
                                          }  
                                        }
                                        //echo $Referral_Cashback_amount= $rows->transation_reason;
                                        ?>
                                      </td>
                                      <td>R$ <?php echo $transactionamt; ?>
                                          <br>
                                          <?php
                                            if($rows->cashback_transaction =='')
                                            {
                                              if(($rows->transation_reason=='Pending Referal Payment') || ($rows->transation_reason =='Referal Payment') || ($rows->transation_reason =='Referral Cashback amount'))
                                              {
                                                if($User_Email != '')
                                                {
                                                  if($Exp[0] !='')
                                                  {
                                                    echo "(".$User_Email.")";  
                                                  }
                                                  
                                                }
                                              }
                                            }
                                            else
                                            {
                                              ?>
                                              R$ <?php  echo $cashbackamount; 
                                            }
                                          ?>
                                      </td>
                                      <td>
                                        <?php
                                        if($rows->transation_reason=='Pending Referal Payment' || $rows->transation_reason=='Referal Payment' || $rows->transation_reason=='Missing Cashback request' || $rows->transation_reason=='Unic bonus amonut Added')
                                        {
                                          if($rows->transation_status=='Approved' || $rows->transation_status=='Credited')
                                          {
                                            ?>
                                            <label class="text-muted" style="font-weight: 500;"><?php echo "Confirmado";//$rows->transation_status;?></label>
                                            <a class="tooltips" href="#">
                                              <span class="exc-blk"><i class="fa fa-info"></i></span> 
                                              <span class="popupcls"><?=$ex_ref_approved?></span>
                                            </a>
                                            </td>
                                            <?php
                                          }
                                          if($rows->transation_status == 'Pending')
                                          {
                                            ?>
                                            <label class="text-muted" style="font-weight: 500;"><?php echo "Pendente";//$rows->transation_status;?></label>
                                            <a class="tooltips" href="#">
                                              <span class="exc-blk"><i class="fa fa-info"></i></span> 
                                              <span class="popupcls"><?=$ex_ref_pending?></span>
                                            </a>
                                            </td>
                                            <?php
                                          }
                                          if($rows->transation_status == 'Canceled')
                                          {
                                            ?>
                                            <label class="text-muted" style="font-weight: 500;"><?php echo "Cancelado";//$rows->transation_status;?></label>
                                            <a class="tooltips" href="#">
                                              <span class="exc-blk"><i class="fa fa-info"></i></span> 
                                              <span class="popupcls"><?=$ex_ref_cancelled?></span>
                                            </a>
                                            </td>
                                            <?php
                                          } 
                                        }
                                        if($rows->transation_reason=='Referral Cashback amount')
                                        {
                                          if($rows->transation_status=='Approved')
                                          {
                                            ?>
                                            <label class="text-muted" style="font-weight: 500;"><?php echo "Confirmado";//$rows->transation_status;?></label>
                                            <a class="tooltips" href="#">
                                              <span class="exc-blk"><i class="fa fa-info"></i></span>
                                              <span class="popupcls"><?=$ex_ref_approved_sec?></span>
                                            </a>
                                            </td>
                                            <?php
                                          }
                                          if($rows->transation_status == 'Pending')
                                          {
                                            ?>
                                            <label class="text-muted" style="font-weight: 500;"><?php echo "Pendente";//$rows->transation_status;?></label>
                                            <a class="tooltips" href="#">
                                              <span class="exc-blk"><i class="fa fa-info"></i></span>
                                              <span class="popupcls"><?=$ex_ref_pending_sec?></span>
                                            </a>
                                            </td>
                                            <?php
                                          }
                                          if($rows->transation_status == 'Canceled')
                                          {
                                            ?>
                                            <label class="text-muted" style="font-weight: 500;"><?php echo "Cancelado";//$rows->transation_status;?></label>
                                            <a class="tooltips" href="#">
                                              <span class="exc-blk"><i class="fa fa-info"></i></span>
                                              <span class="popupcls"><?=$ex_ref_cancelled_sec?></span>
                                            </a>
                                            </td>
                                                        <?php
                                          } 
                                        }

                                        if($rows->transation_reason=='Referral Bonus for Category One User' || $rows->transation_reason=='Referral Bonus for Category Two User' || $rows->transation_reason=='Referral Bonus for Category Three User' || $rows->transation_reason=='Referral Bonus for Category Four User' || $rows->transation_reason=='Referral Bonus for Category Five User'
                                          ||$rows->transation_reason=='Referral Bonus for Category Six User' || $rows->transation_reason=='Referral Bonus for Category Seven User' || $rows->transation_reason=='Referral Bonus for Category Eight User' || $rows->transation_reason=='Referral Bonus for Category Nine User' || $rows->transation_reason=='Referral Bonus for Category Ten User'
                                          )
                                        {
                                          if($rows->transation_status=='Approved' || $rows->transation_status=='Completed')
                                          {
                                            ?>
                                            <label class="text-muted" style="font-weight: 500;"><?php echo "Confirmado";//$rows->transation_status;?></label>
                                            <a class="tooltips" href="#">
                                              <span class="exc-blk"><i class="fa fa-info"></i></span>
                                              <span class="popupcls"><?=$ex_ref_approved_third?></span>
                                            </a>
                                            </td>
                                            <?php
                                          }
                                          if($rows->transation_status == 'Pending')
                                          {
                                            ?>
                                            <label class="text-muted" style="font-weight: 500;"><?php echo "Pendente";//$rows->transation_status;?></label>
                                            <a class="tooltips" href="#">
                                              <span class="exc-blk"><i class="fa fa-info"></i></span>
                                              <span class="popupcls"><?=$ex_ref_pending_third?></span>
                                            </a>
                                            </td>
                                            <?php
                                          }
                                          if($rows->transation_status == 'Canceled')
                                          {
                                            ?>
                                            <label class="text-muted" style="font-weight: 500;"><?php echo "Cancelado";//$rows->transation_status;?></label>
                                            <a class="tooltips" href="#">
                                              <span class="exc-blk"><i class="fa fa-info"></i></span>
                                              <span class="popupcls"><?=$ex_ref_cancelled_third?></span>
                                            </a>
                                            </td>
                                            <?php
                                          } 
                                        }
                                        if($rows->transation_reason=='Credit Account')
                                        {
                                          if($rows->transation_status=='Approved')
                                          {
                                            ?>
                                            <label class="text-muted" style="font-weight: 500;"><?php echo "Confirmado";//$rows->transation_status;?></label>
                                            <a class="tooltips" href="#">
                                              <span class="exc-blk"><i class="fa fa-info"></i></span>
                                              <span class="popupcls"><?=$ex_credit_approved?></span>
                                            </a>
                                            </td>
                                            <?php
                                          }
                                          if($rows->transation_status == 'Pending')
                                          {
                                            ?>
                                            <label class="text-muted" style="font-weight: 500;"><?php echo "Pendente";//$rows->transation_status;?></label>
                                            <a class="tooltips" href="#">
                                              <span class="exc-blk"><i class="fa fa-info"></i></span>
                                              <span class="popupcls"><?=$ex_credit_pending?></span>
                                            </a>
                                            </td>
                                            <?php
                                          }
                                          if($rows->transation_status == 'Canceled')
                                          {
                                            ?>
                                            <label class="text-muted" style="font-weight: 500;"><?php echo "Cancelado";//$rows->transation_status;?></label>
                                            <a class="tooltips" href="#">
                                              <span class="exc-blk"><i class="fa fa-info"></i></span>
                                              <span class="popupcls"><?=$ex_credit_cancelled?></span>
                                            </a>
                                            </td>
                                            <?php
                                          } 
                                        }
                                        ?>
                                    </tr>     
                                    <?php 
                                    $k++;
                                  }
                                }*/
                                ?>    
                              </tbody>
                            </table>
                            <?php 
                          }
                          ?>
                          
                        
                        </div>
                      </div>
                    </div>
                    <br>
                    <span> <a style="font-family: freight-sans-pro;" href="<?php echo base_url()?>loja-nao-avisou-compra" > Seu dinheiro de volta não apareceu? Fale conosco </a> </span>
                    <!--account table section ends-->
                </div><br>
                <!-- New code for sales funnel banner details 8-11-16 -->
                <?php include('bottom_banners.php');?>
                <!-- End 8-11-16 -->
                <?php $this->load->view('front/my_earnings.php')?>
              </div>
            </div>
        </div>
      </div>
    </div>
</section>  
<!-- Main Content end -->

<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>

<!--
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.minnew.js"></script>
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.bootstrap.min.js"></script> 
-->
<script type="text/javascript">
   /*
    $('#sample_teste1').DataTable({ 
    "processing": true,
    "serverSide": true,
    "columnDefs": [{
    "targets": 1,
    "orderable": false,
    "Length": 10,
    }],
    "ajax": {
    "url": "<?php echo site_url('cashback/newextrato')?>",
    "data": {
    //"totalrecords": "<?php echo $iTotal; ?>"
    "affiliate_name": "<?php echo $affiliate_name; ?>"
    }
    }
    });*/
 
</script>
<!-- Footer menu End --> 
<style type="text/css">
 /*.dataTables_processing
{
  display:none !important;
}*/
.dataTables_processing
{
  margin-top: 10px !important;
  padding-top: 25px !important;
  background: none !important;
}
.acc-paginablk
{
  margin-top: 20px !important;  
}
</style>
<link href="<?php echo $this->front_model->get_css_js_url(); ?>assets/css/pagination.css" rel="stylesheet" />
