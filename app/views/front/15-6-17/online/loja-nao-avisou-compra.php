<?php $this->load->view('front/header'); ?>
<!-- header content End -->
<style>
  .butn_clr_success {
  	background: #68c668 none repeat scroll 0 0;
  	border-radius: 20px;
  	color: #fff;
  	font-weight: 500;
  	margin-bottom: 0;
  	padding: 5px 15px;
  }
  .butn_clr_danger {
  	background: #ff4242 none repeat scroll 0 0;
  	border-radius: 20px;
  	color: #fff;
  	font-weight: 500;
  	margin-bottom: 0;
  	padding: 5px 15px;
  }
  .error {
  	color:#ff0000;
  }
  .required_field {
  	color:#ff0000;
  }
  .view_ticket label:before {
  	content: attr(data-name);
  	left: 0;
  	position: absolute;
  	top: 0;
  }
  .view_ticket label {
  	border-top: 1px solid #e6e6e6;
  	font-size: 12px;
  	line-height: 30px;
  	padding-left: 170px;
  	position: relative;
  }
  .btn {
  	white-space: normal!important;
  }
  a.tooltips {
    position: relative;
    display: inline;
  }
  a.tooltips .popupcls {
   background: #3da0d5 none repeat scroll 0 0;
      border-radius: 2px;
      color: #ffffff;
      line-height: 20px;
      min-height: 30px;
      min-width: 100%;
      position: absolute;
      text-align: center;
      visibility: hidden;
      width: 250px!important;
      word-wrap: break-word; left: -5em !important;
  }
  a.tooltips .popupcls:after {
    content: '';
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -8px;
    width: 0; height: 0;
    border-top: 8px solid #3da0d5;
    border-right: 8px solid transparent;
    border-left: 8px solid transparent;
  }
  a:hover.tooltips .popupcls {
    visibility: visible;

    bottom: 30px;
    left: 50%;
    margin-left: -76px;
    z-index: 999;
  }
</style>
<style type="text/css">
  .table-responsive 
  {
  	overflow-x: unset;
  }

  div.selector, div.selector span, div.checker span, div.radio span, div.uploader, div.uploader span.action, div.button, div.button span
  {background-image:none;}
  .col-xs-6 {width: 47% !important; float: left;min-height:1px;padding-left: 12px;padding-right: 12px;position: relative;}
  select.input-sm { width:60px !important;}
  //.row {margin-left: -12px;margin-right: -12px;}
  .pagination {border-radius: 4px;margin: 20px 0;padding-left: 0;}
  .dataTables_filter label::after {content: '' !important;}
</style>

<?php 
foreach ($hover as $hs)
{
  $missing_cash_created=$hs->missing_cash_created;
  $missing_cash_sentretailer=$hs->missing_cash_sentretailer; 
  $missing_cash_cancelled=$hs->missing_cash_cancelled;  
  $missing_cash_completed=$hs->missing_cash_completed;
}
?>
<!-- Main Content start -->
<?php $user_id = $this->session->userdata('user_id');?>
<section class="cms wow fadeInDown">
  <div class="container">
    <div class="heading wow bounceIn">
      <h2> avisou <span> compra</span> </h2>
      <div class="heading_border_cms"> <span> <img src="<?php echo $this->front_model->get_img_url();?>front/new/images/top_drop.png"> </span> </div>
    </div>
    <div class="myac">
      <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1" id="hide_fn">
        <div class="my_account my_accblk">
          <div class="myacc-maintab">
            <!-- Nav tabs -->
            <?php $this->load->view('front/user_menu'); ?>
          </div>
          <!-- Tab panes -->
          <div class="tab-content">
            <h4 class="mar-bot20">Seu dinheiro de volta não apareceu?</h4>
            <p>Utilizar um cupom de desconto, link ou oferta de outro site pode anular seu dinheiro de volta no Pingou. Cuide sempre para que o Pingou seja o último clique antes de chegar na loja. Se precisar de mais alguma coisa é só nos <a href="<?php echo base_url();?>contact">deixar uma mensagem</a> </p>
            <?php 
              $error = $this->session->flashdata('error');
              if($error!="")
              {
                echo '<div class="alert alert-warning">
                <button data-dismiss="alert" class="close">x</button>
                '.$error.'</div>';
              }
              
              $success = $this->session->flashdata('success');
              if($success!="")
              {
                echo '<div class="alert alert-success">
                <button data-dismiss="alert" class="close">x</button>
                '.$success.'</div>';      
              }
            ?>
            <!-- <h4 class="mar-top20 mar-bot20">Extrato</h4>  -->
            <br>
            <h4 class="mar-top20 mar-bot20"> Acompanhamento da Solução de Problemas</h4>
            <div class="form-inline accblk clearfix wow fadeInLeft cls-storebtnblk">
              <button  onclick="location.href = '<?php echo base_url();?>resgatar-compra-nao-avisada';" class="acc-commbtn"> Adicionar um Ticket </button>
              <button  onclick="location.href = '<?php echo base_url();?>loja-cancelou-minha-compra';"  class="acc-commbtn"> Add Missing Approval Ticket </button>
            </div>
            <br>
            <br>
            <div role="tabpanel" id="acc2">
              <?php 
                foreach ($hover as $hs) 
                {  
                  $ex_cash_pending          = $hs->ex_cash_pending;
                  $ex_cash_cancelled        = $hs->ex_cash_cancelled; 
                  $ex_cash_approved         = $hs->ex_cash_approved;  
                  $ex_ref_pending           = $hs->ex_ref_pending;
                  $ex_ref_cancelled         = $hs->ex_ref_cancelled; 
                  $ex_ref_approved          = $hs->ex_ref_approved; 
                  $ex_ref_pending_sec       = $hs->ex_ref_pending_sec; 
                  $ex_ref_cancelled_sec     = $hs->ex_ref_cancelled_sec;
                  $ex_ref_approved_sec      = $hs->ex_ref_approved_sec; 
                  $ex_ref_pending_third     = $hs->ex_ref_pending_third; 
                  $ex_ref_cancelled_third   = $hs->ex_ref_cancelled_third; 
                  $ex_ref_approved_third    = $hs->ex_ref_approved_third; 
                  $ex_missing_cash_approved = $hs->ex_missing_cash_approved; 
                  $ex_credit_pending        = $hs->ex_credit_pending;
                  $ex_credit_cancelled      = $hs->ex_credit_cancelled; 
                  $ex_credit_approved       = $hs->ex_credit_approved;  
                }
              ?>
              <!--account table section starts-->
              <div class="row wow fadeInDown">
                <div class="col-md-12 col-sm-12">
                  <div class="a">
                    <?php 
                    if(empty($results))
                    {
                      echo "<center>Aqui você poderá acompanhar o status de todos os tickets de suporte que forem realizados.</center><br>";
                    } 
                    else
                    {
                      ?>
                      <table cl id="example" class="display zui-table zui-table-rounded table acc-table1">
                        <thead>
                          <tr>
                            <th>Date</th>
                            <th>Loja</th>
                            <th>Reason of the ticket</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if($results!='')
                          {
                            $kss=1;
                            foreach($results as $res)
                            { 
                              if($res->missing_reason == 'Missing Approval')
                              {
                                $missing_reason = 'Loja cancelou minha compa';
                              }
                              if($res->missing_reason == 'Missing Cashback')
                              {
                                $missing_reason = 'Loja não avisou minha compa';
                              }    
                              ?>
                              <tr>
                                <td><?php echo date("d/m/y",strtotime($res->ticket_created_in));?></td> <!-- $res->trans_date -->
                                <td><?php echo $res->retailer_name;?></td>
                                <td><?php echo $missing_reason;?></td>
                                <td>R$ <?php echo $this->front_model->currency_format($res->transation_amount);?></td>

                                <?php $details = $res->status;
                                            switch($details)
                                            {
                                              case 0:
                                                ?>
                                <td><label class="text-success" style="font-weight: 500;">Aprovado</label>
                                  <a class="tooltips" href="#"> <span class="exc-blk"><i class="fa fa-info"></i></span> <span class="popupcls">
                                  <?=$missing_cash_completed?>
                                  </span> </a> </td>
                                <?php
                                              break;
                                              case 1:
                                                ?>
                                <td><label class="text-danger" style="font-weight: 500;">Cancelado</label>
                                  <a class="tooltips" href="#"> <span class="exc-blk"><i class="fa fa-info"></i></span> <span class="popupcls">
                                  <?=$missing_cash_cancelled?>
                                  </span> </a> </td>
                                <?php
                                              break;
                                              case 2:
                                                ?>
                                <td><label class="text-danger" style="font-weight: 500;">Enviar para o varejista</label>
                                  <a class="tooltips" href="#"> <span class="exc-blk"><i class="fa fa-info"></i></span> <span class="popupcls">
                                  <?=$missing_cash_created?>
                                  </span> </a> </td>
                                <?php
                                              break;
                                              case 3:
                                                ?>
                                <td><label class="text-danger" style="font-weight: 500;">Pendente</label>
                                  <a class="tooltips" href="#"> <span class="exc-blk"><i class="fa fa-info"></i></span> <span class="popupcls">
                                  <?=$missing_cash_sentretailer?>
                                  </span> </a> </td>
                                <?php
                                              break;
                                              case 4:
                                                ?>
                                <td><label class="text-success" style="font-weight: 500;">Aprovado</label>
                                  <a class="tooltips" href="#"> <span class="exc-blk"><i class="fa fa-info"></i></span> <span class="popupcls">
                                  <?=$missing_cash_sentretailer?>
                                  </span> </a> </td>
                                <?php
                                              break;
                                            }
                                            $getadmindetails = $this->front_model->getadmindetails();
                                            $site_logo       = $getadmindetails[0]->site_logo;
                                            ?>
                                <td><a data-toggle="modal" style="cursor:pointer; color: #333333;" data-target="#myModal_view_<?php echo $kss;?>">Detalhes</a> </td>
                                <div class="modal fade" id="myModal_view_<?php echo $kss;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header popup">
                                    <a class="close" data-dismiss="modal"><i class="fa fa-close"></i></a>
                                        <div class="fw header"> <img width="" height="" class="logo center-block" alt="<?php $admindetails = $this->front_model->getadmindetails_main(); echo $admindetails->site_name; ?>.com" src="<?php echo $this->front_model->get_img_url(); ?>uploads/adminpro/<?php echo $site_logo;?>"> </div>
                                    </div>
                                      <div class="modal-body"> 
                                        <section class="popup">
                                          <?php
                                                      $cashback_id  = $res->cashback_id;
                                                      $stat_num = 5000+$cashback_id;
                                                      //$getclickdate = date("d F Y", strtotime($res->trans_date));
                                                      $getclickdate =date("d-m-Y",strtotime($res->ticket_created_in));
                                                      ?>
                                          <h2>Detalhes do andamento da solicitação (Protocolo:
                                            <?=$stat_num?>
                                            )</h2>
                                          <div class="view_ticket clearfix">
                                            <label data-name="Data da solicitação:" class="fw clearfix">
                                            <?=$getclickdate?>
                                            </label>
                                            <label data-name="Data da compra:" class="fw clearfix">
                                            <?=date("d-m-Y",strtotime($res->trans_date));?>
                                            </label>
                                            <label data-name="Loja:" class="fw clearfix">
                                            <?=$res->retailer_name?>
                                            </label>
                                             <label data-name="Valor da compra:" class="fw clearfix"> <span class="indianRs">R$ </span>
                                            <?=$res->transation_amount?>
                                            </label>
                                            <label data-name="Cashback amount:" class="fw clearfix"> <span class="indianRs">R$ </span>
                                            <?=$res->ordervalue?>
                                            </label>
                                            <label data-name="Nº do pedido:" class="fw clearfix">&nbsp;
                                            <?=$res->transaction_ref_id?>
                                            </label>
                                            <label data-name="Telefone:" class="fw clearfix">&nbsp;
                                            <?=$res->phone_no?>
                                            </label>
                                            <label data-name="Cupom utilizado:" class="fw clearfix">&nbsp;
                                            <?=$res->coupon_code?>
                                            </label>
                                            <label data-name="Nº da NF eletrônica:" class="fw clearfix">&nbsp;
                                            <?=$res->cashback_details?>
                                            </label>
                                            <?php
                                                        switch($res->status)
                                                        {
                                                          case 0:
                                                            $stra = 'Aprovado';
                                                          break;
                                                          case 1:
                                                            $stra = 'Cancelado';
                                                          break;
                                                          case 2:
                                                            $stra = 'Enviar para o varejista';
                                                          break;
                                                          case 3:
                                                            $stra = 'Pendente';
                                                          break;
                                                          case 4:
                                                            $stra = 'Aprovado';
                                                          break;
                                                        }
                                                        ?>
                                            <label data-name="Status:" class="fw clearfix">&nbsp;
                                            <?=$stra;?>
                                            </label>
                                            <label data-name="Status atualizado em" class="fw clearfix"> &nbsp;
                                            <?php
                                            if($res->status == 3)
                                            {

                                            }
                                            else
                                            {
                                             echo date("d-m-Y",strtotime($res->status_update_date));
                                            }?>
                                            </label>
                                            <label data-name="Cancelled reason:" class="fw clearfix"> &nbsp;
                                              <?php
                                                $echo = $res->cancel_msg;
                                                $echo = str_replace("font-size:14px;", "font-size:12px;",$echo);
                                                $echo = str_replace("font-family:Arial,Helvetica,sans-serif;", "",$echo); 
                                                echo $echo;
                                              ?>
                                            </label>
                                          </div>
                                        </section>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </tr>
                              <?php
                              $kss++;
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                      <?php
                    }
                    ?>
                  </div>
                </div>
              </div>
              <br>
              <!--account table section ends-->
            </div>
            <br>
            <div class="modal fade" id="myModal_view" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header popup">
                    <a class="close" data-dismiss="modal"><i class="fa fa-close"></i></a>
                    <div class="fw header"> <img width="" height="" class="logo center-block" alt="<?php $admindetails = $this->front_model->getadmindetails_main(); echo $admindetails->site_name; ?>.com" src="<?php echo $this->front_model->get_img_url(); ?>uploads/adminpro/<?php echo $site_logo;?>"> </div>
                  </div>
                  <div class="modal-body"> 
                    <section class="popup">
                      <?php
                      $cashback_id  = $res->cashback_id;
                      $stat_num     = 5000+$cashback_id;
                      //$getclickdate = date("d F Y", strtotime($res->trans_date));
                      $getclickdate =date("d-m-Y",strtotime($res->ticket_created_in));
                      ?>
                      <h2>Detalhes do andamento da solicitação (Protocolo:
                      <?=$stat_num?>
                      )</h2>
                      <div class="view_ticket clearfix">
                        <label data-name="Data da solicitação:" class="fw clearfix">
                          <?=$getclickdate?>
                        </label>
                        <label data-name="Data da compra:" class="fw clearfix">
                          <?=date("d-m-Y",strtotime($res->trans_date));?>
                        </label>
                        <label data-name="Loja:" class="fw clearfix">
                          <?=$res->retailer_name?>
                        </label>
                        <label data-name="Valor da compra:" class="fw clearfix"> <span class="indianRs">R$ </span>
                          <?=$res->ordervalue?>
                        </label>
                        <label data-name="Nº do pedido:" class="fw clearfix">&nbsp;
                          <?=$res->transaction_ref_id?>
                        </label>
                        <label data-name="Telefone:" class="fw clearfix">&nbsp;
                          <?=$res->phone_no?>
                        </label>
                        <label data-name="Cupom utilizado:" class="fw clearfix">&nbsp;
                          <?=$res->coupon_code?>
                        </label>
                        <label data-name="Nº da NF eletrônica:" class="fw clearfix">&nbsp;
                          <?=$res->cashback_details?>
                        </label>
                        <?php
                        switch($res->status)
                        {
                          case 0:
                            $stra = 'Aprovado';
                          break;
                          case 1:
                            $stra = 'Cancelado';
                          break;
                          case 2:
                            $stra = 'Enviar para o varejista';
                          break;
                          case 3:
                            $stra = 'Pendente';
                          break;
                          case 4:
                            $stra = 'Aprovado';
                          break;
                        }
                        ?>
                        <label data-name="Status:" class="fw clearfix">&nbsp;
                        <?=$stra;?>
                        </label>
                        <label data-name="Status atualizado em" class="fw clearfix"> &nbsp;
                        <?php
                        if($res->status == 3)
                        {

                        }
                        else
                        {
                         echo date("d-m-Y",strtotime($res->status_update_date));
                        }?>
                        </label>
                        <label data-name="Cancelled reason:" class="fw clearfix"> &nbsp;
                          <?php
                            $echo = $res->cancel_msg;
                            $echo = str_replace("font-size:14px;", "font-size:12px;",$echo);
                            $echo = str_replace("font-family:Arial,Helvetica,sans-serif;", "",$echo); 
                            echo $echo;
                          ?>
                        </label>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
            <?php $this->load->view('front/my_earnings.php')?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Main Content end -->
<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?>
<!-- Popup End -->
<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>

<script type="text/javascript">
function mymodel_views(cash_id)
{
  //var cash_id = $('#cashback_id').val();
  //alert(cash_id); die;
  $.ajax({
  type: "POST",
  url: "<?php echo base_url();?>cashback/missing_cashback_using_id",
  data: {'cash_id':cash_id},
  cache: false,
  success: function(result)
  {
     alert(<?php print_r(result);?>);
    if(result!=1)
    {
      return false;
    }
    else
    {
      <?php $redirect_urlset =  base_url(uri_string());?>
      window.location.href = '<?php echo $redirect_urlset; ?>';
      return false;          
    }             
  }
  });
  return false;
}
  
</script>

<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.minnew.js"></script>
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.bootstrap.min.js"></script> 

<script type="text/javascript">
/*$(document).ready(function() {
  $('#sample_teste1').DataTable({
  "processing": true,
  "serverSide": true,
  "columnDefs": [{
  "targets": 1,
  "orderable": false,
  "Length": 10,
  }],
  "ajax": {
  "url": "<?php echo site_url('cashback/newloja_nao_avisou_compra')?>",
  "data": {
  //"totalrecords": "<?php echo $iTotal; ?>"
  "affiliate_name": "<?php echo $affiliate_name; ?>"
  }
  }
  });
});*/
</script>
<!-- Footer menu End --> 
<style type="text/css">
 .dataTables_processing
{
  display:none !important;
}
</style>
 <link href="<?php echo $this->front_model->get_css_js_url(); ?>assets/css/pagination.css" rel="stylesheet" />
