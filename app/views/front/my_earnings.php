<?php

    $user_id 		  =  $this->session->userdata('user_id');
	$balcne  		  =  bcdiv($this->front_model->user_balance($user_id),1,2);
	$newbal           =  $this->front_model->currency_format($balcne);

	$paid_earnings    =  bcdiv($this->front_model->paid_earnings($user_id),1,2);
	$paid_earning     =  $this->front_model->currency_format($paid_earnings);

	$balcne_cb        =  bcdiv($this->front_model->user_cashback_balance($user_id),1,2);
	$new_cb_bal       =  $this->front_model->currency_format($balcne_cb);

	$total_earnings   =  bcdiv($this->front_model->total_earnings($user_id),1,2);
	$total_earning    =  $this->front_model->currency_format($total_earnings);

	$waiting          =  bcdiv($this->front_model->waiting_approval($user_id),1,2);
	$waiting_approval =  $this->front_model->currency_format($waiting);

	//$reff             =  $this->front_model->ref_earnings($user_id);
	//$ref_earning      =  $this->front_model->currency_format($reff);

	/*new code for missing cashback,bonus,Approve cashback amounts 14-3-17*/
    $miss_cash         =  $this->front_model->approve_missing_cashback($user_id);
    $app_ticket        =  $this->front_model->missing_approve_ticket($user_id);
    $unic_bonus_amt    =  $this->front_model->unic_bonus_amount($user_id);
    $credit_details    =  $this->front_model->credit_account_details($user_id);
    /*End 14-3-17*/

    $reff              =  $this->front_model->ref_earnings($user_id); 
    //$ref_earning     =  $this->front_model->currency_format($reff);
    $bonus_amts        =  $miss_cash+$app_ticket+$unic_bonus_amt+$credit_details+$reff;
    $bonus_earning     =  $this->front_model->currency_format($bonus_amts);
?>
<style type="text/css">
.comm-greybg
{
    background: #f5f5f5 none repeat scroll 0 0;
    border-radius: 27px;
    padding: 24px 43px;	
}
</style>

<!-- <div class="col-md-10 col-sm-10 col-xs-12 fn center-block rescue-earn"> -->
	<!--earning block section starts-->
	<div class="earn-accblk">
		<div class="row wow bounceInUp">
		    <center>
		    	<h1>My earnings</h1>
		    </center>
		</div>
		<div class="row wow fadeInDown">
		    <ul class="earn-listblk list-inline list-unstyled clearfix"> <!--  comm-greybg -->
		        <li>
		        	<a href="#">
		                <center>
		                  	<h3>Cashback</h3>
		                  	<img class="center-block" src="<?php echo $this->front_model->get_img_url();?>front/new/images/earn2.png" alt="earn">
		                  	<p>Rs.<?php echo $new_cb_bal;?></p>
		                </center>
		            </a>
		        </li>
		        <li>
		        	<a href="#">
		                <center>
			                <h3>Bonus</h3>
			                <img class="center-block" src="<?php echo $this->front_model->get_img_url();?>front/new/images/earn6.png" alt="earn">
			                <p>Rs.<?php echo $bonus_earning;?></p>
		                </center>
		            </a>
		        </li>
		        <li>
			        <a href="#">
			        	<center>
					        <h3>Total</h3>
					        <img class="center-block" src="<?php echo $this->front_model->get_img_url();?>front/new/images/earn1.png" alt="earn">
					        <p>Rs.<?php echo $total_earning;?></p>
			        	</center>
			        </a>
		        </li>
		        <li>
		        	<a href="#">
		                <center>
		            	    <h3>Available</h3>
		                	<img class="center-block" src="<?php echo $this->front_model->get_img_url();?>front/new/images/earn7.png" alt="earn">
		                  	<p>Rs.<?php echo $newbal;?></p>
		                </center>
		            </a>
		        </li>
		        <li>
		        	<a href="#">
		            	<center>
		                	<h3>Waiting for<br>
		                    Approval</h3>
		                  	<img class="center-block" src="<?php echo $this->front_model->get_img_url();?>front/new/images/earn5.png" alt="earn">
		                  	<p>Rs.<?php echo $waiting_approval;?></p>
		                </center>
		            </a>
		        </li>
		        <li>
		        	<a href="#">
		            	<center>
		                	<h3>Paid</h3>
		                  	<img class="center-block" src="<?php echo $this->front_model->get_img_url();?>front/new/images/earn3.png" alt="earn">
		                  	<p>Rs.<?php echo $paid_earning;?></p>
		                </center>
		            </a>
		        </li>
		    </ul>
		</div>
	</div>
	<!--earning block section ends-->
<!-- </div> -->