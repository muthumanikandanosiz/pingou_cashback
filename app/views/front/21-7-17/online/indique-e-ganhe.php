<?php $this->load->view('front/header'); ?>
<!-- header content End -->
<style type="text/css">
.table-responsive {
	overflow-x: unset;
}
.newlinkcls {
	border: 3px solid #4daed9;
	border-radius: 20px !important;
	height: 40px;
  position: relative;
  width: 107% !important;
  z-index: 3;
}
</style>
<style>
div.selector, div.selector span, div.checker span, div.radio span, div.uploader, div.uploader span.action, div.button, div.button span {
	background-image:none;
}
.col-xs-6 {
	width: 47% !important;
	float: left;
	min-height:1px;
	padding-left: 12px;
	padding-right: 12px;
	position: relative;
}
select.input-sm {
	width:60px !important;
}
.row {
margin-left: -12px;
margin-right: -12px;
}
.pagination {
	border-radius: 4px;
	margin: 20px 0;
	padding-left: 0;
}
 .dataTables_filter label::after {
content: '' !important;
}
</style>
<style type="text/css">
.newref_styles {
	margin-right: 20px;
}
.email-searchblk .email-label span.clickone {
	line-height: 24px;
}
.email-searchblk .email-label .clickone img {
	margin-bottom: 3px;
	margin-top: 5px;
	max-height: 40px;
	max-width: 40px;
}
.email-searchblk .email-label.clicktwo {
	padding-top: 4px;
}
#accordion-faq {
	margin-top: 0px !important;
}
.howworks {
	background: #ffffff;
	border: 1px solid #cccccc;
	padding: 10px 15px 5px;
}
.how_work .panel-title {
	color: inherit;
	text-align: center;
	position:relative;
}
.how_work .panel-group {
	margin-bottom: 0px;
}
.how_work .panel-default .panel-heading {
	background:none !important;
	border:0 transparent solid !important;
	color: #333;
	padding:0;
	cursor:pointer;
}
.how_work .panel {
	background:none;
	border: 1px solid transparent;
	border-radius: 4px;
	box-shadow:none;
	margin-bottom: 0px !important;
}
.how_work .panel-heading a:after {
	color: #090909;
	content: "\e113";
	font-family: "Glyphicons Halflings";
	position: absolute;
	right:15px;
	top: 10px;
	border:none;
	width:auto;
	height:auto;
	line-height:20px;
	text-align:center;
	border-radius:50%;
	transition: all 0.2s ease-in-out 0s;
	-webkit-transition: all 0.2s ease-in-out 0s;
}
.how_work .panel-heading a:hover::after {
background:#fff;
color:#8db654;
}
.how_work .panel-heading.collapsed a:after {
	content:"\e114";
	transition: all 0.2s ease-in-out 0s;
}
.how_work .panel-group .panel-heading + .panel-collapse > .list-group, .how_work .panel-group .panel-heading + .panel-collapse > .panel-body {
	border-top: 0px solid #ddd;
}
.how_work .panel-body {
	padding:0px;
}
.how_work .panel-title label.form-control-label, .how_work .panel-title label.form-control-label a, .how_lis li {
	color: #090909;
	display: inline-block;
	font-weight: 500;
	text-transform: capitalize;
	width: 100%;
}
.how_work .panel-title label.form-control-label {
	text-align:left;
}
.how_work .panel-title label.form-control-label a {
	text-align:right;
	padding-right:25px;
}
.how_work .panel-title label.form-control-label {
	padding-bottom: 10px;
	padding-top: 10px;
}
.how_work .panel-title label.form-control-label a:hover, .how_work .panel-title label.form-control-label a:focus {
	outline:none;
	text-decoration:none;
}
</style>
<!-- Main Content start -->
<?php $user_id       = $this->session->userdata('user_id');
$userdetails         = $this->front_model->userdetails($user_id);
$clickcounts         = $userdetails->reflink_click_counts;

$ref_counts          = $this->db->query("SELECT count(`refer`) as ref_count FROM `tbl_users` where `refer`='$user_id'")->row();
$referral_counts     = $ref_counts->ref_count;
$category            = $this->front_model->referal__category(); 
$admindetails        = $this->front_model->getadmindetails();
$admin_email         = $admindetails[0]->admin_email;

$conversion          = (($referral_counts/$clickcounts)*100);

$ref_categorydetails = $this->db->query("select * from referral_settings where ref_id='$category'")->row();
?>
<section class="cms wow fadeInDown">
<div class="container">
  <div class="heading wow bounceIn">
    <h2><?php echo $ref_categorydetails->category_title; ?></h2>
    <!-- <h2>
        Indique e <span>Ganhe</span>
      </h2> -->
    <div class="heading_border_cms"> <span> <img src="<?php echo $this->front_model->get_img_url();?>front/new/images/top_drop.png"> </span> </div>
  </div>
  <div class="myac">
    <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1" id="hide_fn">
      <div class="my_account my_accblk">
        <div class="myacc-maintab">
          <!-- Nav tabs -->
          <?php $this->load->view('front/user_menu'); ?>
          <!-- <ul class="data-drop wow fadeInDown list-inline list-unstyled cls-greybgmenublk" id="dataSubMain" role="tablist">
              <li class="active"> <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Dados do Perfil</a> </li>
              <li> <a href="#payment" aria-controls="payment" role="tab" data-toggle="tab">Dados de Pagamento</a> </li>
              <li> <a href="#password" aria-controls="password" role="tab" data-toggle="tab">Mudar Senha</a> </li>
              <li> <a href="#notify" aria-controls="notify" role="tab" data-toggle="tab">Notificação</a> </li>
            </ul> -->
        </div>
        <!-- category image 15-4-17 -->
        <center>
          <?php 
            if($ref_categorydetails->category_image != '')
            {
              ?>
              <!--<img src="<?php echo $this->front_model->get_img_url(); ?>uploads/adminpro/<?php echo $ref_categorydetails->category_image; ?>" width='350' height='200'>-->
              <img src="<?php echo $this->front_model->get_img_url(); ?>uploads/adminpro/<?php echo $ref_categorydetails->category_image; ?>" class="img-responsive center-block" > <!-- style="width:100%;" -->
          <?php 
            }
            ?>
        </center>
        <br>
        <!-- end 15-4-17 -->
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" id="acc4">
            <div class="clearfix wow fadeInDown comm-cont">
              <p>
                <?php              
                  if($ref_categorydetails->ref_by_percentage == 1)
                  {
                    ?>
              <p> Indique amigos e ganhe <?php echo $this->front_model->referal__category_percentage($category); ?>% de todo o dinheiro de volta que eles resgatarem!</p>
              <?php 
                  }
                  $k = 5000;
                  $earn_price = ($k*$this->front_model->referal_percentage())/100;
                  if($category == 1)
                  {
                    echo '<p>'.$this->front_model->referal__category_description($category).'</p>';
                    ?>
              <p>Não entendeu? Por exemplo, se seu amigo ganhou <span class="indianRs">R$</span> 10 de volta de uma compra realizada por ele, você recebe <span class="indianRs">R$</span> 1 como bônus na sua conta. <br>
                <br>
                Imagine só quanto você pode ganhar indicando <?php echo $this->front_model->referal__category_percentage(); ?> amigos! Abaixo está seu link de referência. Você vai ganhar por todos que se cadastrarem clicando nele.</p>
              <?php
                  }
                  else
                  {
                    echo '<p>'.$this->front_model->referal__category_description($category).'</p>';
                  }
                  ?>
              </p>
            </div>
            <?php 
              $result1 = $this->front_model->refer_friends();
              if($ref_categorydetails->click_count_status == 0) 
              { 
                ?>
            <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInDown email-searchblk">
              <label class="col-md-4 col-sm-12 col-xs-12 email-label">Envie este link para seus amigos:</label>
              <div class="col-md-5 col-sm-8 col-xs-12">
                <div class="input-group">
                  <?php 
                      if($result1)
                      {
                        $mi = 1;
                        foreach($result1 as $views)
                        {
                        ?>
                  <input type="text" readonly="" id='copyTarget<?php echo $mi;?>' class="form-control email-tbox" value="<?php echo base_url().'?ref='.$views->random_code; ?>">
                  <input type="hidden" id="random_code" value="<?php echo $views->random_code; ?>">
                  <?php
                        } 
                      }
                      ?>
                  <span class="input-group-btn">
                  <button type="button"  onclick='copy(<?php echo $mi; ?>);' data-id='<?php echo $mi;?>' id='copyButton<?php echo $mi;?>' class="btn btn-blu">Copy Link</button>
                  </span> </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-12 amigos">
                <label class="col-md-12 col-sm-12 col-xs-12 email-label" style="text-align:right;">Amigos indicados: <?php echo count($this->front_model->referral_network());?></label>
              </div>
            </div>
            <?php
              }   
              ?>
            <!-- New code for extra contents 17-4-17 -->
            <?php 
              if($ref_categorydetails->click_count_status == 1) 
              { 
                 
                if($conversion == '')
                {
                  $conversion = 0;
                }
                ?>
            <style type="text/css">
                  .email-searchblk .email-label {
                    color: black;
                  }

                </style>
            <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInDown email-searchblk" style="color:black !important; background:none; border: 4px solid #4daed9;">
              <label class="col-md-4 col-sm-12 col-xs-12 email-label" style="line-height:30px; margin-top: 10px;">Hi partner! in case of any doubt about you campaign please contact us in the email :<?php echo $admin_email; ?></label>
              <div class="col-md-5 col-sm-8 col-xs-12">
                <label class=" col-xs-12 email-label" style=""> Envie este link para seus amigos: </label>
                <div class="input-group">
                  <?php 
                      if($result1)
                      {
                        $mi = 1;
                        foreach($result1 as $views)
                        {
                        ?>
                  <input style="border: 3px solid #4daed9;" readonly="" type="text" id='copyTarget<?php echo $mi;?>' class="form-control email-tbox" value="<?php echo base_url().'?ref='.$views->random_code; ?>">
                  <input type="hidden" id="random_code" value="<?php echo $views->random_code; ?>">
                  <?php
                        } 
                      }
                      ?>
                  <span class="input-group-btn">
                  <button type="button"  onclick='copy(<?php echo $mi; ?>);' data-id='<?php echo $mi;?>' id='copyButton<?php echo $mi;?>' class="btn btn-blu">Copy Link</button>
                  </span> </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-12">
                <label class="email-label clicktwo"  style="margin-right:25px; color:#4daed9; font-weight:600;"> <span class="col-md-4 col-sm-4 col-xs-12 clickone">Clicks <img src="<?php echo base_url();?>front/images/click.png" style="margin-left:5px"><span style="margin-left:15px;"><?php echo $clickcounts; ?></span></span> <span class="col-md-4 col-sm-4 col-xs-12 clickone">Signup <img src="<?php echo base_url();?>front/images/signup.png" style="margin-left:8px"><span style="margin-left:20px;"><?php echo $referral_counts;?></span></span> <span class="col-md-4 col-sm-4 col-xs-12 clickone">Conversion <img src="<?php echo base_url();?>front/images/conversion.png" style="margin-left:20px"><span style="margin-left:30px;"><?php echo round($conversion,1); ?>%</span></span></label>
              </div>
            </div>
            <?php
              }
              ?>
            <!-- End 17-4-17 -->
            <br>
            <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
             
            <section class="how_work">
             <div class="panel-group col-sm-12 col-xs-12" id="accordion" style="padding-left: 0px; padding-right: 0px;"> 
              <!--<div class="panel-group" id="accordion">-->
                <?php 
                $i=111;

                $user_email      = $userdetails->email;
                $getcash_details = $this->front_model->cashback_ex_details_email($user_email);
                foreach($getcash_details as $result)
                { 
                  if($result->indique_status == 1)
                  {
                    $newconversion = (($result->cash_sign_up_counts/$result->cash_click_counts)*100);
                    ?>
                    <div class="panel panel-default" id="panel<?php echo $i;?>">
                      <div class="clearfix">
                        <div class="panel-title">
                          <div class="row">
                            <div class="col-sm-3 col-xs-12">
                                <label class="form-control-label">
                                  <b>
                                    <?php echo $result->campaign_name;?>
                                  </b>
                                </label>
                            </div>
                            <div class="col-md-6 col-sm-5 col-xs-12">
                              <div class="input-group">
                                <input type="text" readonly="" id='copyTarget<?php echo $i;?>' class="form-control-label newlinkcls" value="<?php echo base_url();?><?php echo $result->link_name;?>">
                                <input type="hidden" id="exclusive_link" value="<?php echo base_url();?><?php echo $result->link_name;?>">
                                <span class="input-group-btn">
                                  <button type="button"  onclick='copy(<?php echo $i; ?>);' data-id='<?php echo $i;?>' id='copyButton<?php echo $i;?>' class="btn btn-blu" style="margin-top:0px;">Copy Link</button>
                                </span>
                              </div>  
                            </div>
                            <div data-toggle="collapse" data-parent="#accordion" data-target="#collapsen<?php echo $i;?>" class="panel-heading collapsed" aria-expanded="false">
                              <div class="col-md-3 col-sm-4 col-xs-12">
                                <label class="form-control-label acc_toggle" id="loadmore"><a>Show Details</a></label>
                                <label class="form-control-label acc_toggle" id="showless" style="display:none;"><a>Hide Details</a></label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div id="collapsen<?php echo $i;?>" class="panel-collapse collapse" aria-expanded="false">
                        <div class="panel-body">
                          <div class="clearfix">
                            <div class=" howworks">
                              <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                  <p> <strong>Campanha    : </strong> <?php echo $result->campaign_name; ?> <br>
                                    <strong>Start Date  : </strong> <?php echo $result->start_date; ?><br>
                                    <strong>End date    : </strong> <?php echo $result->expirydate; ?> (What extend this date? <a href="mailto:marketing@pingou.com.br">talk to us</a>)<br>
                                    <strong>Link        : </strong> <?php echo base_url();?><?php echo $result->link_name; ?><br>
                                    <strong>Description : </strong> <?php echo $result->campaign_desc; ?> <br>
                                    <strong>Comments    : </strong> <?php echo $result->campaign_comments; ?><br>
                                  </p>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                  <label class="email-label clicktwo"  style="margin-top:1em; color:#4daed9; font-weight:600; margin-left:100px;">
                                  <span class="col-md-4 col-sm-4 col-xs-12 clickone text-center">
                                  <p>Clicks</p>
                                  <img src="<?php echo base_url();?>front/images/click.png" style="margin-left:5px">
                                  <p style="margin-left:15px;"><?php echo $result->cash_click_counts; ?></p>
                                  </span> <span class="col-md-4 col-sm-4 col-xs-12 clickone text-center">
                                  <p>Signup</p>
                                  <img src="<?php echo base_url();?>front/images/signup.png" style="margin-left:8px">
                                  <p style="margin-left:20px;"><?php echo $result->cash_sign_up_counts;?></p>
                                  </span> <span class="col-md-4 col-sm-4 col-xs-12 clickone text-center">
                                  <p>Conversion</p>
                                  <img src="<?php echo base_url();?>front/images/conversion.png" style="margin-left:20px">
                                  <p style="margin-left:20px;"><?php echo round($newconversion,1); ?>%</p>
                                  </span>
                                  </label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php
                  }  
                  $i++;
                }   
                ?>
              </div>
            </section>
            <?php
              include('oauth.php'); 
              include('outlook.php'); 
              $redirectUri = base_url().'authorize.php';
              ?>
            <!-- <center><p style='text-align:center; margin-right: 10%;' id='msg<?php echo $mi;?>'></p></center> -->
            <div class="row wow fadeInDown">
              <div class="comm-cont wow fadeInLeft col-md-12 col-sm-6 col-xs-12">
                <h2 style="margin-bottom: 20px; text-align:center !important;">get started by searching your email 
                  contacts</h2>
                <br>
                <div class="row wow bounceInUp acc-login">
                  <center>
                    <a href="http://www.facebook.com/sharer.php?u=<?php echo base_url().'?ref='.$views->random_code; ?>" class="facebook" data-toggle="modal" target="_blank"> <i class="fa fa-facebook"></i> Facebook </a> <a href="<?php echo base_url().'HAuth/invite/Google'; ?>" target="_black" class="google"> <i class="fa fa-google-plus"></i> Google + </a> <a href="http://twitter.com/home?status=<?php echo base_url().'?ref='.$views->random_code; ?>" class="twitter" target="_blank"> <i class="fa fa-twitter"></i> Twitter </a>
                  </center>
                </div>
              </div>
              <br>
              <center>
                <h4>OR</h4>
              </center>
              <center>
                <div class="row emailbtnblk">
                  <div class="col-md-7 col-sm-12 col-xs-12" style="float:none;">
                    <div class="input-group">
                      <input type="text" placeholder="Endereços de email, separados por vírgula" id="invite_mail" onClick="clears(1);" class="form-control" style="border-radius: 0px;">
                      <span class="input-group-btn">
                      <button type="button"  onClick="send_mail();" class="btn btn-blu">Enviar Convites</button>
                      </span> </div>
                    <span id="success" style="color:green;"></span> </div>
                </div>
              </center>
              <br>
              <?php 
                if($ref_categorydetails->social_status == 1)
                {
                  ?>
              <div class="comm-cont wow fadeInRight col-md-12 col-sm-6 col-xs-12">
                <h2 style="margin-bottom: 20px; text-align:center !important;">get started by searching your social
                  networks </h2>
                <ul class="soc-iconblk list-unstyled">
                  <center>
                    <a href="<?php echo base_url().'HAuth/invite/Google'; ?>" target="_black"><img src="<?php echo $this->front_model->get_img_url();?>front/new/images/socialnet1.png" alt="network">&nbsp; Gmail </a>
                    </li>
                    &nbsp;&nbsp;&nbsp; <a href="<?php echo base_url().'HAuth/invite/Yahoo'; ?>"  target="_black"><img src="<?php echo $this->front_model->get_img_url();?>front/new/images/socialnet2.png" alt="network">&nbsp; Yahoo Mail </a>
                    </li>
                    &nbsp;&nbsp;&nbsp; <a href="<?php echo oAuthService::getLoginUrl($redirectUri)?>"  target="_black" style="text-decoration: none;"><img src="<?php echo $this->front_model->get_img_url();?>front/new/images/socialnet3.png" alt="network">&nbsp; Hot Mail </a>
                    </li>
                  </center>
                  <!--<li><a href="#"><img src="<?php echo base_url();?>front/new/images/socialnet4.png" alt="network"> Outlook.com </a></li>
                      <li><a href="#"><img src="<?php echo base_url();?>front/new/images/socialnet5.png" alt="network"> Windows live mail </a></li>
                      <li><a href="#"><img src="<?php echo base_url();?>front/new/images/socialnet6.png" alt="network"> MSN </a></li> -->
                </ul>
              </div>
              <?php 
                }  
                ?>
            </div>
            <br>
            <h2 style="margin-bottom: 25px;">Amigos Indicados</h2>
            <div class="col-md-12 col-sm-12">Essa é a relação de todos os amigos que se cadastraram pelo seu link <br> </div>
            <br>
            <!--account table section starts-->
            <div class="wow fadeInDown">
              <div class="col-md-12 col-sm-12">
                <div class="a">
                  <?php 
                     if(empty($result))
                    {
                      echo "<br><center>Por enquanto, nenhum amigo seu realizou cadastro utilizando o seu link.<br>
                      Comece a divulgar agora mesmo!.</center><br>";
                    } 
                    else
                    { 
                      ?>
                  <table id="sample_teste1" class="display zui-table zui-table-rounded table acc-table1 table_indique">
                    <thead>
                      <tr>
                        <th> Date </th>
                        <th> Email </th>
                        <th> Status da Referencia </th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                          /*if($result)
                          {
                            foreach($result as $rows)
                            {
                              ?>
                              <tr>
                                <td><?php $data = $rows->date_added; echo date('d/m/y',strtotime($data)); ?></td>
                                <td><?php echo $rows->referral_email; ?></td>
                                <?php
                                  if($rows->status == 'Ativa')
                                  {
                                    ?>
                                    <td><?php echo 'Ativa'; ?></td>
                                    <?php
                                  }
                                  if($rows->status == 'Inativa')
                                  {
                                    ?>
                                    <td><?php echo 'Inativa'; ?></td>
                                    <?php
                                  }
                                ?>
                              </tr>
                              <?php
                            }
                          }*/
                          ?>
                    </tbody>
                  </table>
                  <?php 
                    }
                    ?>
                </div>
              </div>
            </div>
          </div>
          <br>
          <?php $this->load->view('front/my_earnings.php')?>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<!-- Main Content end -->
<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?>
<!-- Popup End -->
<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>

<script type="text/javascript">
  $('#loadmore').click(function()
  {
    $('#showless').show();
    $('#loadmore').hide();
  });
  $('#showless').click(function()
  {
    $('#showless').hide();
    $('#loadmore').show();
  });  
</script>


<script type="text/javascript">
  //Start Email subscribe
  function email_sub()
  {
    var email = $("#email").val();
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,3})?$/;
    if(!email || !emailReg.test(email))
      $('#email').css('border', '2px solid red');
    else
    {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>cashback/email_subscribe/",
        data: {'email':email},
        success: function(msg)
        {
          if(msg==1)
          {
            $('#msg').text('Activated Successfully');
            $('#email').css('border', '');
          }
          else
          {
            $('#msg').text('Already Activated');
            $('#email').css('border', '');
          } 
        }
      });
    } 
  }
  function clears(val)
  {
    if(val==1)
      $('#invite_mail').css('border', '');
    else
      $('#news_email').css('border', '');
  } 
  //End Email subscribe
  //Friends Invite Mail
  function send_mail()
  {
    var mail = $('#invite_mail').val();
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,3})?$/;
    var myList = mail.replace(/\s+/, "");
    var emails = mail.split(',');
    var random = $('#random_code').val();
    var text = $('#mail_text').val();
    
    if(mail=='')
      $('#invite_mail').css("border","2px solid red");
    else if(mail!='')
    {
      var err=0;
      for (var i = 0; i < emails.length; i++) {
        value = emails[i];
        if(!emailReg.test(value)){
          $('#invite_mail').css("border","2px solid red");
          err=1;
          }
      }
      if(err == 0)
      {
        $.ajax({
        type: "post",
        url: "<?php echo base_url(); ?>cashback/invite_mail/",
        data: {'email':mail,'random':random,'mail_text':text},
        success: function()
        {
          $('#invite_mail').val('');
          $('#success').text('Convites enviados com sucesso!');
        }
      
      });
      }
    }
  }
  function MyPopUpWin(url, width, height) 
  {
      var leftPosition, topPosition;
      //Allow for borders.
      leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
      //Allow for title and status bars.
      topPosition = (window.screen.height / 2) - ((height / 2) + 50);
      //Open the window.
      window.open(url, "Window2",
      "status=no,height=" + height + ",width=" + width + ",resizable=yes,left="
      + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY="
      + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no");
  } 
</script>
<!-- fim copiar -->
<script type="text/javascript">
  function copy(id)
  { 
    var user_id = $('#user_id').val();
    $('#copyTarget'+id).select();
    document.execCommand('copy');
    setTimeout(function(){
    $('#msg'+id).text('Text has coppied');  
    }, 1000);

    /*new code for referral link click count details 18-4-17*/
    /*$.ajax({
        type: "POST",
        url: "<?php echo base_url();?>cashback/update_refclick_count",
        data: {'user_id':user_id},
        cache: false,
        success: function(result)
        {
          if(result!=1)
          {
            return false;
          }
          else
          {
            setTimeout(function(){
            $('#msg'+id).text('Text has coppied');  
            }, 1000);          
          }             
        }
      });*/
    /*End 18-4-17*/

  }
</script>
<!--<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.minnew.js"></script>
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.bootstrap.min.js"></script>
-->
<!-- Footer menu End -->
<style type="text/css">
 .dataTables_processing
{
  display:none !important;
}
</style>
<link href="<?php echo $this->front_model->get_css_js_url(); ?>assets/css/pagination.css" rel="stylesheet" />
