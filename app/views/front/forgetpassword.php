<!-- header -->
  <?php $this->load->view('front/header');?>
<!-- Header ends here -->
<style>
.error
{
	color:#ff0000;
}
.required_field
{
 color:#ff0000;
}
.btncls
{
	border-radius: 5px !important;
	padding: 5px 30px !important;
}
</style>  
</head>

<body>

<section class="cms wow fadeInDown">
  	<div class="container">
	    <div class="heading wow bounceIn">
	      <h2><span>Forgot </span>Password</h2>
	      <div class="heading_border_cms"><span><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/top_drop.png"></span> </div>
	    </div>
	    <div class="col-md-6 col-sm-12 col-xs-12 fn center-block">
		    <div class="wow fadeInDown">
				<div class="panel-body">
	            	<?php
					$error = $this->session->flashdata('error');
					if($error!="") 
					{
						echo '<div class="alert alert-danger">
						<button data-dismiss="alert" class="close">x</button>
						<strong>Error! </strong>'.$error.'</div>';
					}  
					$success = $this->session->flashdata('success');
					if($success!="") 
					{
						echo '<div class="alert alert-success">
						<button data-dismiss="alert" class="close">x</button>
						<strong>Success! </strong>'.$success.'</div>';
					} 
					//form begin
					$attributes = array('role'=>'form','name'=>'forget_password','id'=>'forget_password','method'=>'post');
					echo form_open('forgetpassword/',$attributes);
					?>
	                
	                <div class="form-group mb-lg">
						<input type="email" class="form-control input-lg" placeholder="E-mail" name="email" id="email">
					</div>
	                            
					<div class="form-group mb-none">
						<div class="input-group accblk">
							 
								<input type="submit" name="reset" id="reset" value="Reset!" class="acc-commbtn btncls">
							 
						</div>
					</div>

					<p>If you Remembered? &nbsp;
			            <a href="#login" data-toggle="modal" id="forgotclick">Sign In</a>
			        </p>

					<?php echo form_close();?>
				</div>
		 	</div>
	    </div>
  	</div>
</section>
<!-- footer -->
<?php $this->load->view('front/site_intro');?>
 
 <?php $this->load->view('front/sub_footer');?>	  
        
 
<script type="text/javascript">
/* form validation*/
 $(document).ready(function() {
         $("#forget_password").validate({
	          rules: {
				email: {
                     required: true,
						email :true
                }			
            },
            messages: {
				email: {
                   required: "Please enter  your valid Emailid."                  
                }
			}
				
        });
});
</script>
