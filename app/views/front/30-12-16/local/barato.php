<?php $this->load->view('front/header'); ?>
<!-- header content End -->
<style type="text/css">
	.ovel-item
	{
		width:243px !important;
	}
</style>
<!-- Main Content start -->
<?php

$total_price    = $details->price; 
$sales_price    = $details->amount;
$coupon_id      = $details->shoppingcoupon_id;
$seo_url        = $details->seo_url;
$store_id       = $details->store_name;
$selqry         = $this->db->query("SELECT * from affiliates where affiliate_id='$store_id'")->row();
$store_name     = $selqry->affiliate_name;
$store_img      = $selqry->affiliate_logo;
$store_cash_type= $selqry->affiliate_cashback_type;
$store_cat_type = $selqry->store_categorys;
$cat_type       = explode(',', $store_cat_type);
$cat_type       = $cat_type[0];
/*New code 12-5-16.*/
$aff_url        = $details->offer_page;
$extra_url      = $details->extra_param_url;
$selqrys        = $this->db->query("SELECT * from categories where category_id='$cat_type'")->row();                
$category_name  = $selqrys->category_url; 
$cashback_amount= $selqry->cashback_percentage;
/*Cashback type and earn cashback amount details 10-5-16*/
if($store_cash_type == "Flat")
{
	$cashback_amount  = "R$". $cashback_amount;
	$cashback_amt     = $cashback_amount;
	$cashback_amt     = $this->front_model->currency_format($cashback_amt);
} 
if($store_cash_type == "Percentage")
{
	$cashback_amount  = $cashback_amount ."%";
	$cashback_amt     = (($sales_price)*($cashback_amount)/100);
	$cashback_amt     = $this->front_model->currency_format($cashback_amt);
} 
 
/*End 10-5-16*/
$new_total_amt       = $this->front_model->currency_format($total_price);
$new_discount_price  = $this->front_model->currency_format($sales_price); 
/*Discount percentage calcluation 10-5-16*/
$sales_price         = ($total_price - $sales_price);
$discount_percentage = (($sales_price/$total_price)*100);
$discount_percentage = $this->front_model->currency_format($discount_percentage);
/*End 10-5-16*/
/*Expiry date details 10-5-16*/
$newgetremain_days=$this->front_model->find_remainingdays($details->expiry_date);
?>

 

<!-- baroto -->
<section class="cms wow fadeInDown">
  	<div class="container">
    	<div class="heading wow bounceIn">
      		<h2>barato</h2>
      		<div class="heading_border_cms">
      			<span>
      				<img src="<?php echo $this->front_model->get_img_url();?>front/new/images/top_drop.png">
      			</span>
      		</div>
    	</div>
    	<div class="wow fadeInDown">
    		<!-- New code for Sales funnel banner details 8-11-16 -->
	    	<?php include('top_banners.php'); ?>
	    	<!-- End 8-11-16 -->
    		<br>
      		<div class="col-md-8 col-sm-8 col-xs-12">
   				<!--baroto ad section starts-->
   				<div class="wow fadeInDown baroto-ad">
    				<section id="sixth-section">
    					<div class="sixth-section-area" style="padding:0px 0 !important;">
					        <!-- start single effect -->
					        <div class="single-effect">
						        <figure class="wpf-demo-5">
						          	<?php
						      		$db_coupon_image 	 = $details->coupon_image;
									$exp_db_coupon_image = explode(",",$db_coupon_image);  							 
									for($i=0;$i<count($exp_db_coupon_image);$i++)
									{ 
										?>
						        		<a href="">
						        			<img style="width:960px !important; height:565px !important;"src="<?php echo $this->front_model->get_img_url(); ?>uploads/premium/<?php echo $exp_db_coupon_image[$i]; ?>" alt="baroto" class="img-responsive center-block">
						        		</a>
						            	<figcaption class="view-caption">
						            		<a class="zoom-img" href="<?php echo $this->front_model->get_img_url(); ?>uploads/premium/<?php echo $exp_db_coupon_image[$i]; ?>">
							          			<span>
							        				<img src="<?php echo $this->front_model->get_img_url();?>front/new/images/baroto-zoom.png">
							        			</span>
						               		</a>
						            	</figcaption>
						            <?php
						      		}
						      		?>		
						        </figure>
					        </div>
    					</div>
    				</section>      
    	        </div>
   				<!--baroto ad section ends-->

		        <!--tab section starts-->
		        <div class="coupon">
			      	<!-- Nav tabs -->
			      	<ul class="nav nav-tabs cash_tab baroto_tab wow fadeInUp" role="tablist">
				        <li class="active"> <a href="#1" aria-controls="1" role="tab" data-toggle="tab">
				          <div>Description</div>
				          <div align="center">
				           <i class="fa fa-exclamation desc-exc"></i>            
				          </div>
				          </a></li>
				        <li> <a href="#2" aria-controls="2" role="tab" data-toggle="tab">
				          <div>The Deal</div>
				           <div align="center">
				           <i class="fa fa-thumbs-up"></i>            
				          </div>
				          </a> </li>
				        <li> <a href="#3" aria-controls="3" role="tab" data-toggle="tab">
				          <div>Location </div>
				           <div align="center">
				           <i class="fa fa-map-marker"></i>            
				          </div>
				          </a> </li>
				        <li> <a href="#4" aria-controls="4" role="tab" data-toggle="tab">
				          <div> The Company </div>
				           <div align="center">
				           <i class="fa fa-exclamation desc-exc"></i>            
				          </div>
				          </a> </li>
				        <li> <a href="#5" aria-controls="5" role="tab" data-toggle="tab">
				        	<div>Reviews</div>
				        	<div align="center">
				        		<i class="fa fa-comments"></i>            
				        	</div>
				        	</a>
				        </li>     
			      	</ul>
			      	<!-- Tab panes -->
			      	<div class="tab-content baroto-tabcont clearfix">
    					<div class="tab-pane active" id="1">
				          	<div class="wow fadeInDown">
				            	<div class="col-md-12 col-sm-12 col-xs-12">
				            		<div class="cmt-blk clearfix">
				             			<i class="fa fa-clock-o"></i>
                   						<?php  echo $newgetremain_days['days']." days ".$newgetremain_days['hours']." h "." remaining";   ?>
				            		</div>
				            		<div class="cmt-cont">
							            <p><?php echo $details->long_description; ?></p>
        							</div>
        						</div>
      						</div>          
    					</div>
    					<div class="tab-pane" id="2">
			          		<div class="wow fadeInDown">
			            		<div class="col-md-12 col-sm-12 col-xs-12">
					            	<div class="cmt-cont">
						            	<p><?php echo $details->about; ?></p>
							            <p><?php echo $details->nutshel; ?></p>
							            <p><?php echo $details->fine_print; ?></p>
						            </div>
        						</div>
				       		</div>
				    	</div>
				    	<div class="tab-pane" id="3">
			          		<div class="wow fadeInDown">
			            		<div class="col-md-12 col-sm-12 col-xs-12">
					            	<div class="cmt-cont">
						            	<iframe width="100%" height="350px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/?q=<?php echo $details->location; ?>&ll=&spn=0.009935,0.01929&t=h&layer=c&cbll=&z=16&cbp=12,354.03,,0,-11.17&amp;source=embed&amp;output=svembed"></iframe>
              							<br/>
              							<small>
              								<a href="https://maps.google.com/?q=<?php echo $details->location; ?>&ll=&spn=0.009935,0.01929&t=h&layer=c&cbll=&z=16&cbp=12,354.03,,0,-11.17&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a>
              							</small>
						            </div>
        						</div>
				       		</div>
				    	</div>
				    	<div class="tab-pane" id="4">
			          		<div class="wow fadeInDown">
			            		<div class="col-md-12 col-sm-12 col-xs-12">
					            	<div class="cmt-cont">
					            		<h3><?php echo $details->title; ?></h3>
						                <p><?php echo $details->company; ?></p>
						                <?php
						        		if($details->offer_page) 
						        		{
						          			$attribute = array('class'=>'btn btn-primary');?>
						                    <a href="<?php echo $details->offer_page;?>">'Company Website<i class="fa fa-external-link"></i>'</a>
						          			<?php
						                }
						                ?>
						            </div>
        						</div>
				       		</div>
				    	</div>
				    	<div class="tab-pane" id="5">
			          		<div class="wow fadeInDown">
			            		<div class="col-md-12 col-sm-12 col-xs-12">
					            	<div class="cmt-cont">
						            	<ul class="comments-list">
						                    <?php if($reviews)
						                    {
						                      	foreach($reviews  as $review) 
						                      	{
						                        	?>
							                        <li>
							                          <article class="comment">
							                            <div class="comment-author">
							                              <img class="img-responsive"  alt="" src="http://mouzakinews.gr/wp-content/uploads/2013/07/user-icon-512.png" width="72" >
							                            </div>
							                            <div class="comment-inner">
							                              <ul title="4/5 rating" class="icon-group icon-list-rating comment-review-rate">
							                                <?php                       
							      							            //echo $review->ratings;
							      							   					for($i=0;$i<$review->ratings;$i++)
							                                {
							                                  ?>
							                                  <li><i class="fa fa-star"></i> </li>
							                                  <?php
							                                }
							                                ?>
							                              </ul>
							                              <h5 class="thumb-list-item-title"><a href="#"><?php echo $review->first_name; ?></a></h5>
							                              <p class="thumb-list-item-author"><?php echo $review->comments; ?></p>
							                            </div>
							                          </article>
							                        </li>
							                        <?php
						                      	}
						                    }
						                    ?>
						                </ul>
						                <?php
						                if($this->session->userdata('user_id')!='')
						                {
						                	?>
						                    <a data-toggle="modal" href="#myModal-review" class="btn btn-blu btn-barot hvr-pulse-shrink"><i class="fa fa-pencil"></i> Add a review</a>
						                    <?php
						                }
						                else
						                {
						                ?>
						                	<a class="btn btn-blu btn-barot hvr-pulse-shrink" href="#entrar" data-toggle="modal"><i class="fa fa-sign-in pad-rht"></i>Sign in to add review</a>
						                    <?php
						                }
						                ?>
						                <br>	
						            </div>
        						</div>
				       		</div>
				    	</div>

  					</div>
				</div>
    			<!--tab section ends-->
        			
    			<!-- New code for Sales funnel banner details 8-11-16 -->
		    	<?php 
		    	//$data['page_bot_status'] = $page_bot_status;
		    	include('bottom_banners.php'); ?>
		    	<!-- End 8-11-16 -->

			        <!--owl carousel starts-->
			        <h2 style="text-align: left;">related products</h2>
			        <div class="baroto-carousel clearfix">
			          	<div id="owl-example" class="owl-carousel">
			          		<?php 
	                    	if($related_products) 
	                    	{ 
		                      	//echo "<pre>";print_r($related_products);
		                      	$k=1;
		                      	$counting = count($related_products);
		                      	foreach($related_products as $related_product) 
		                      	{ 
			                        $store_id            = $related_product->store_name;
			                        $sales_price         = $related_product->amount;
			                        $new_discount_price  = $this->front_model->currency_format($new_discount_price);
			                        $db_coupon_image     = $related_product->coupon_image;
			            			$exp_db_coupon_image = explode(",",$db_coupon_image);  							 
		          					$f_dbcouponfirst_img = $exp_db_coupon_image[0];
		          					$getremain_days      = $this->front_model->find_remainingdays($related_product->expiry_date);
		                		    $selqry         = $this->db->query("SELECT * from affiliates where affiliate_id='$store_id'")->row(); 
	                        		$store_name     = $selqry->affiliate_name;
			                        $store_img      = $selqry->affiliate_logo;
			                        $store_cash_type= $selqry->affiliate_cashback_type;
			                        $store_cat_type = $selqry->store_categorys;
			                        $cat_type       = explode(',', $store_cat_type);
			                        $cat_type       = $cat_type[0];
			                        /*New code 12-5-16.*/
			                        $aff_url        = $details->offer_page;
			                        $extra_url      = $details->extra_param_url;

			                        $selqrys        = $this->db->query("SELECT * from categories where category_id='$cat_type'")->row();                
			                        $category_name  = $selqrys->category_url; 
			                        $cashback_amount= $selqry->cashback_percentage;

			                        /*new code 05-10-16*/
			                        $categorydetails = $this->db->query("SELECT * from premium_categories where category_id = $related_product->category")->row();
			                        $category_names  = $categorydetails->category_url;
			                        /*End*/

			                        /*Cashback type and earn cashback amount details 17-5-16*/
			                        
			                        if($store_cash_type == "Flat")
			                        {
			                          $cashback_amount  = "R$". $cashback_amount;
			                          $cashback_amt     = $cashback_amount;
			                          $cashback_amt     = $this->front_model->currency_format($cashback_amt);  
			                        } 
			                        if($store_cash_type == "Percentage")
			                        {
			                          $cashback_amount  = $cashback_amount ."%";
			                          $cashback_amt     = (($sales_price)*($cashback_amount)/100);
			                          $cashback_amt     = $this->front_model->currency_format($cashback_amt);
			                        } 
			                        /*End 17-5-16*/

			                          $new_total_amt       = $this->front_model->currency_format($total_price); 
			                          $new_discount_price  = $this->front_model->currency_format($sales_price); 

			                          //Discount percentage calcluation 17-5-16//
			                          $sales_price         = ($total_price - $sales_price);
			                          $discount_percentage = (($sales_price/$total_price)*100);
			                          $discount_percentage = number_format(round($discount_percentage ,2),2);
			                          //End 17-5-16//
			                        ?>
						            	<div class="prod-photodetblk">
						              		<div class="photo">
						              			<img style="width:264px;height:292px;" class="img-responsive center-block" alt="Image Alternative text" src="<?php echo $this->front_model->get_img_url(); ?>uploads/premium/<?php echo $f_dbcouponfirst_img; ?>">
						              		</div>
							                <div class="info">
							                	<div class="row vers">
										            <center>
										                <h4>
										                	<a href="<?php echo base_url(); ?>barato/<?php echo  $category_names;?>/<?php echo $related_product->seo_url;?>">
										                		<span class="cls_sub_text">
										                			<?php echo substr($related_product->offer_name,0,30)."...";   ?>
										                		</span>
										                	</a>
										                </h4>
										            </center>
										        </div>
									            <div class="overlay-prod ">
									                <div class="row vers hov">
									                	<center>
									                    	<h4 class=""><a href="<?php echo base_url(); ?>barato/<?php echo  $category_names;?>/<?php echo $related_product->seo_url;?>"><?php echo substr($related_product->offer_name,0,30)."...";   ?></a></h4>
									                      	<p>
									                      		Cashback of <?php echo $this->front_model->currency_format($cashback_amount);?> in <?php echo $store_name;?>
									                      	</p>
								                            <p>
								                            	Pay <font style="font-weight:bold;" color="blue">R$<?php echo $new_discount_price;?></font>&nbsp;and&nbsp;earn&nbsp;<font style="font-weight:bold;" color="green">R$<?php echo $cashback_amt; ?></font>
								                            </p>
									                      	<span>
									                      		<i class="fa fa-clock-o"></i>
	                                  							<?php  echo $getremain_days['days']." days ".$getremain_days['hours']." h "." remaining";   ?>
	                                  						</span>
									                      	<a href="<?php echo base_url(); ?>barato/<?php echo  $category_names;?>/<?php echo $related_product->seo_url;?>" class="btn btn-blu">More details</a>
									                   </center>
									                </div>
									            </div>
							              	</div>
						            	</div>
						            <?php
                        		$k++; 
                        		}
                          		 
                      		} 
                      		?>
			          	</div>
			        </div>
			        <!--owl carousel ends-->
      		</div>

      		
		   	<div class="col-md-4 col-sm-4 col-xs-12">
			    <div class="baroto-sidebar1">
			          	<div class="baroto-head1"> <span><?php echo $this->front_model->currency_format($discount_percentage); ?>%</span> </div>
			          	<div class="baroto-cont1">
				            <h3><?php echo $details->offer_name;?></h3>
				            <h3 style="text-decoration: line-through;">R$<?php echo $new_total_amt;?></h3>
				            <h2>R$.<?php echo $new_discount_price; ?></h2>
				            <p>Buy it and receive <span class="amt-barot">R$ <?php echo $this->front_model->currency_format($cashback_amt); ?></span> of cashback</p>
				            <div class="barot-listblk clearfix">
				              	<div class="col-md-4 col-sm-4 col-xs-4 pd-left-0 pd-right-0"> <img src="<?php echo $this->front_model->get_img_url();?>uploads/affiliates/<?php echo $store_img; ?>" style="height:72px;" alt="baroto" class="img-responsive center-block"> </div>
				              	<div class="col-md-8 col-sm-8 col-xs-8">
				                	<p>Compare na <?php echo $store_name; ?>, e receba <?php echo $this->front_model->currency_format($cashback_amount); ?> de volta</p>
				              	</div>
				            </div>
				            <p>aproveite outros <a style="color:blue; font-weight:bold;" href="<?php echo base_url();?>desconto/<?php echo $category_name; ?>"><span class="coupon-barot">cupons de <?php echo $category_name;?></span></a></p>
				            <p class="text-muted"><?php echo strip_tags($details->description)."...";   ?></p>
			            	<div class="clearfix">
				              	<center>
				              		<?php
				              		if($this->session->userdata('user_id')!='')
				              		{
				              			?>
				              			
					              			<a class="after_login" href="<?php echo base_url();?>barato/<?php echo $seo_url;?>" target="_blank;" data-id="<?php echo base_url();?>ir-loja/<?php echo $store_id;?>/<?php echo $coupon_id;?>/shop" show_id="<?php echo $coupon_id;?>">
					              				<button class="btn btn-blu btn-barot hvr-pulse-shrink" style=" font-size: 13px !important; padding: 10px 5px; !important" title="Activate cashback">
					              				<i class="fa fa-sign-in"></i> Activate cashback and reveal offer </button>
					              			</a>
				                		
				                		<?php
					                }
				                	else
				                	{	
				                		?>
				                		<a  class="btn btn-blu btn-barot hvr-pulse-shrink" href="#login" data-toggle="modal"><i class="fa fa-sign-in pad-rht"></i>Sign in to add to cart</a>
				                		<?php
				                	}
				                	?>
				                	<!-- <button type="button" class="btn btn-blu btn-barot hvr-pulse-shrink"> <i class="fa fa-sign-in"></i> sign in to go to  Store </button> -->
				              	</center>
			            	</div>
			            </div>
			    </div>
			    <div class="baroto-sidebar2">
			          	<h1>Recent viewed</h1>
			          	<div class="baroto-cont1">
				            <div class="barot-listblk clearfix">
				              	<ul class="list-unstyled">
				              		<?php 
				  				    $stk =0;
				  				    $kk=0;
				  				    if($recently_viewd)  
				  					{ 
				    					$kk=1;
				    					$stk=1;
				    					foreach($recently_viewd as $recently_view1) 
				    					{
							                $stk=1;
				    						$recently_view=$this->front_model->details($recently_view1->name);  
				    						if($recently_view)
				    						{
				    							$recently_view_image=$recently_view->coupon_image;
				    							$exp_db_coupon_image_1=explode(",",$recently_view_image);  							 
				    							$recently_view_load_image=$exp_db_coupon_image_1[0];
				    							?>
					                			<li>
								                  	<div class="col-md-4 col-sm-4 col-xs-4 pd-left-0 pd-right-0">
								                  		<a href="<?php echo base_url(); ?>barato/<?php echo $recently_view->seo_url; ?>">
								                  			<img src="<?php echo $this->front_model->get_img_url(); ?>uploads/premium/<?php echo $recently_view_load_image; ?>" alt="baroto" class="img-responsive">
								                  		</a> 
								                  	</div>
								                  	<div class="col-md-8 col-sm-8 col-xs-8">
								                    	<h4><a href="<?php echo base_url(); ?>barato/<?php echo $recently_view->seo_url; ?>"><?php echo $recently_view->offer_name; ?></a></h4>
								                    	<p>R$.<?php echo $this->front_model->currency_format($recently_view->amount); ?></p>
								                  	</div>
					                			</li>
					                			<?php
    											$stk++;
    										}
  					    				}
  					 				} 
  					  				else
              						{
              							?>
              							<div class="alert alert-error">
						                <button class="close" data-dismiss="alert">x</button>
						                <strong>Oops! </strong> No products found </div>	
						            <?php
						            }
						            if($stk<2 && $kk=1)
  					  				{					
				  						?>
				                		<div class="alert alert-error">
				                  		<button class="close" data-dismiss="alert">x</button>
				                 		<strong>Oops! </strong> No products found 
				                		</div>
				              			<?php 
				  					}
				              		?>
				              	</ul>
				            </div>
				            <div class="clearfix">
				              	<center>
				                	<a href="<?php echo base_url();?>produtos" class="btn btn-blu btn-barot hvr-pulse-shrink"> View More </a>
				              	</center>
				            </div>
			          	</div>
			    </div>
			    <div class="baroto-sidebar2">
			          	<h1>Popular</h1>
			          	<div class="baroto-cont1">
			            	<div class="barot-listblk clearfix">
			              		<ul class="list-unstyled">
			                	<?php 
			                	if($popular) 
					            { 
									//print_r($popular);
									foreach($popular as $popula) 
					              	{ 
									    $db_coupon_image 	 = $popula->coupon_image;
										$exp_db_coupon_image = explode(",",$db_coupon_image);  							 
					  					$f_dbcouponfirst_img = $exp_db_coupon_image[0];
					     				?>
					                	<li>
						                  	<div class="col-md-4 col-sm-4 col-xs-4 pd-left-0 pd-right-0">
						                  		<a href="<?php echo base_url(); ?>barato/<?php echo $popula->seo_url; ?>">
						                  	 		<img src="<?php echo $this->front_model->get_img_url(); ?>uploads/premium/<?php echo $f_dbcouponfirst_img; ?>" alt="baroto" class="img-responsive">
						                  		</a>
						                  	</div>
						                  	<div class="col-md-8 col-sm-8 col-xs-8">
						                    	<h4><a href="<?php echo base_url(); ?>barato/<?php echo $popula->seo_url; ?>"><?php echo $popula->offer_name; ?></a></h4>
						                    	<p>R$ <?php echo $this->front_model->currency_format($popula->amount); ?></p>
						                    </div>
					                	</li>
			                			<?php
                  						$exp_db_coupon_image='';
              						}
              					}
					            else
					            {
					            	?>
					                <div class="alert alert-error">
					                	<button class="close" data-dismiss="alert">x</button>
					                    <strong>Oops! </strong> No products found </div>
					              	<?php
					            }
					            ?>
			              		</ul>
			            	</div>
			            	<div class="clearfix">
			              		<center>
			                		<a href="<?php echo base_url();?>produtos" class="btn btn-blu btn-barot hvr-pulse-shrink"> View More </a>
			              		</center>
			            	</div>
			          	</div>
			    </div>
		   	</div>
    	</div>
  	</div>
</section>

<!-- Main Content end -->

<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<div id="myModal-review" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      	<div class="modal-content">
	        <div class="modal-header">
	        	<a class="btn btn-default" data-dismiss="modal">
	        		<span class="glyphicon glyphicon-remove"></span>
	        	</a>
	        </div>
		    <div class="modal-body">
		          <form class="form col-md-8 col-md-offset-2" action="#" method="post" onSubmit="return funct_setpremium_cat();">
		            <h4> User Reviews</h4>
		            <hr>
		            <div class="form-group clearfix">
		              <label class="col-md-3"> Write A Review </label>
		              <div class="col-md-9">
		                <textarea class="form-control" name="review_text" id="review_text"  rows="5"></textarea>
		              </div>
		            </div>
		            <input type="hidden" value="<?php echo $details->remain_coupon_code; ?>" id="coupon_code">
		            <input type="hidden" value="<?php echo $details->shoppingcoupon_id; ?>" name="coupon_id" id="coupon_id">
		            <div class="form-group clearfix">
		              <label class="col-md-3"> Rating </label>
		              <div class="col-md-9">
		                <div class="rating">
		                  <input type="radio" id="star5"  name="rating" value="5" />
		                  <label for="star5">5 stars</label>
		                  <input type="radio" id="star4"   name="rating" value="4" />
		                  <label for="star4">4 stars</label>
		                  <input type="radio" id="star3"   name="rating" value="3" />
		                  <label for="star3">3 stars</label>
		                  <input type="radio" id="star2"   name="rating" value="2" />
		                  <label for="star2">2 stars</label>
		                  <input type="radio" id="star1"   name="rating" value="1" />
		                  <label for="star1">1 star</label>
		                </div>
		              </div>
		            </div>
		            <div class="from-group clearfix">
		              <div class="col-md-9 col-md-offset-3">
		                <button class="btn btn-success" type="button" onclick="return funct_setpremium_cat();"> Submit </button>
		              </div>
		            </div>
		          </form>
		    </div>
	        <div class="modal-footer pad">
	        </div>
      	</div>
    </div> 
</div>
<!-- Popup End -->

<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>
<!-- Footer menu End -->

<!-- Owl carousel starts-->
            
    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/owl-carousel/css/owl.carousel.css">
    <!-- Default Theme -->
    <link rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/owl-carousel/css/owl.theme.css">
    <!--  jQuery 1.7+  -->
    <script src="<?php echo $this->front_model->get_css_js_url();?>front/owl-carousel/js/jquery-1.9.1.min.js"></script>
    <!-- Include js plugin -->
    <script src="<?php echo $this->front_model->get_css_js_url();?>front/owl-carousel/js/owl.carousel.js"></script>
    
    <script type="text/javascript">
	    $(document).ready(function() {     
	      $("#owl-example").owlCarousel();
    	});
	</script>
    
 <!--Owl carousel ends-->


<script type="text/javascript">

function funct_setpremium_cat()
{   
	//alert('sasas_1');
	if($("#review_text").val()==''){
		alert("Please enter the comments");
		return false;
	}
	if($('input:radio[name=rating]:checked').length == 0){
		alert("Please enter the rating");
		return false;
	}
	
	var review_text_new = $("#review_text").val();
	//console.log($("input:radio[name=rating]:checked" ));
	var radio_rating = $("input:radio[name=rating]:checked").length;
	var coupons = $("#coupon_id").val();
	$.ajax({
	url:"<?php echo base_url(); ?>index.php/cashback/submit_ratings",
	data: "comments="+ review_text_new + "&rating=" + radio_rating +"&coupon="+coupons,    
	type:"POST",
	success:function(html_butadelike)
	{  
		$('.comments-list').append("Added Successfully");
		$('#myModal-review').css('display','none');
		$('.modal-backdrop').remove();
	}  
	});  
}
function add_to_cart()
{
	if($("#order_max").val()<=$("#user_max").val())
	{
		if($("#coupon_code").val())
		{
		$.ajax({  
		url:"<?php echo base_url(); ?>index.php/cashback/addtocart",
		data: "coupon="+$("#coupon_id").val(),     
		type:"POST",
		success:function(html_butadelike)
		{          
		if(html_butadelike==1)
		{
			$('#cart_succ').show();
			$('#addtocart').hide();
			$('#viewcart').show();
			window.location.href = '<?php echo base_url()."cart-listing-page";?>';
			//$('#result').load('<?php echo base_url()."cashback/detailspage";?>', function(){ $('#cart').attr("class","shopping-cart open"); });			
		}
		else
		{
			$('#cart_succ_already').show();
			$('#viewcart').show();
			$('#result').load('<?php echo base_url()."barato";?>', function(){ $('#cart').attr("class","shopping-cart open"); });		
		}
		}  
		});  	
		}
		else
		{
			alert("Coupon is not available");
		}
	}
	else
	{	
		$('#cart_succ_already').html('User Limit Exceed');
		$('#cart_succ_already').show();
	}
}

</script>