<?php //$this->load->view('front/header');
include('header.php'); ?>
<!-- header content End -->
<style>
  .img-responsive.cls_topad {
      border-radius: 20px;
  }
  .btn{
    white-space: normal!important;
  }
  a.tooltips {
    position: relative;
    display: inline;
  }
  a.tooltips .popupcls {
   background: #3da0d5 none repeat scroll 0 0;
      border-radius: 2px;
      color: #ffffff;
      line-height: 20px;
      min-height: 30px;
      min-width: 100%;
      position: absolute;
      text-align: center;
      visibility: hidden;
      width: 250px!important;
      word-wrap: break-word; left: -5em !important;
      font-size: 15px !important;
  }
  a.tooltips .popupcls:after {
    content: '';
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -8px;
    width: 0; height: 0;
    border-top: 8px solid #3da0d5;
    border-right: 8px solid transparent;
    border-left: 8px solid transparent;
  }
  a:hover.tooltips .popupcls {
    visibility: visible;

    bottom: 30px;
    left: 50%;
    margin-left: -76px;
    z-index: 999;
  }
</style>
<style type="text/css">
  .table-responsive 
  {
    overflow-x: unset;
  }
</style>
<style>
  div.selector, div.selector span, div.checker span, div.radio span, div.uploader, div.uploader span.action, div.button, div.button span
  {background-image:none;}
  .col-xs-6 {width: 47% !important; float: left;min-height:1px;padding-left: 12px;padding-right: 12px;position: relative;}
  select.input-sm { width:60px !important;}
  //.row {margin-left: -12px;margin-right: -12px;}
  .pagination {border-radius: 4px;margin: 20px 0;padding-left: 0;}
  .dataTables_filter label::after {content: '' !important;}
</style>
<!-- Main Content start -->
<?php $user_id = $this->session->userdata('user_id');?>
<section class="cms wow fadeInDown">
  <div class="container">
    <div class="heading wow bounceIn">
      <h2>
        <span>Resgate</span>
      </h2>
      <div class="heading_border_cms">
        <span>
          <img src="<?php echo $this->front_model->get_img_url();?>front/new/images/top_drop.png">
        </span>
      </div>
    </div>
    <div class="myac">
      <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1" id="hide_fn">
        <div class="my_account my_accblk">
          <div class="myacc-maintab">
            <!-- Nav tabs -->
            <?php $this->load->view('front/user_menu'); ?>
          </div>

          <!-- Tab panes -->
          <div class="tab-content">
            <?php include('top_banners.php');?><br>
            <div role="tabpanel" id="acc3">
              <?php 

                foreach ($hover as $hs) 
                {
                  $resgate_requested  = $hs->resgate_requested;
                  $resgate_processing = $hs->resgate_processing; 
                  $resgate_completed  = $hs->resgate_completed;  
                  $resgate_cancelled  = $hs->resgate_cancelled;
                }
 

                $pending_cb       =  bcdiv($this->front_model->pending_cashback($user_id),1,2);
                $new_pending_cb   =  $this->front_model->currency_format($pending_cb);
                $pending_ref      =  bcdiv($this->front_model->pending_referral($user_id),1,2);
                $new_pending_ref  =  $this->front_model->currency_format($pending_ref);
                $balcne_cb        =  $this->front_model->user_cashback_balance($user_id);
                //$new_cb_bal       =  $this->front_model->currency_format($balcne_cb);
                /*29-8-16*/
                $miss_cash        =  $this->front_model->newapprove_missing_cashback($user_id);
                $app_ticket       =  $this->front_model->newmissing_approve_ticket($user_id);
                /*New code 8-6-17*/
                $new_cb_bal       =  bcdiv($balcne_cb+$miss_cash+$app_ticket,1,2);
                $new_cb_bal       =  $this->front_model->currency_format($new_cb_bal); 
                /*End 8-6-17*/
               
                /*new code for missing cashback,bonus,Approve cashback amounts 14-3-17*/
                
                //$app_ticket       =  $this->front_model->missing_approve_ticket($user_id);
                $unic_bonus_amt   =  $this->front_model->unic_bonus_amount($user_id);
                $credit_details   =  $this->front_model->credit_account_details($user_id);
                /*End 14-3-17*/

                $reff             =  $this->front_model->ref_earnings($user_id); 
                //$reff             =  $this->front_model->currency_format($ref_earning);

                $bonus_amts       =  bcdiv($unic_bonus_amt+$credit_details+$reff,1,2); //$miss_cash+$app_ticket+
                $bonus_earning    =  $this->front_model->currency_format($bonus_amts);

                $balcne           =  bcdiv($this->front_model->user_balance($user_id),1,2);
                $newbal           =  $this->front_model->currency_format($balcne);
                $minimum_withdraw =  $this->front_model->minimum_withdraw();
                $remain_minimum_amt = $this->front_model->request_minimum_withdraw();

                $req_withdraw_amt = $this->db->query("SELECT count(`withdraw_id`) as counts from `withdraw` where user_id=$user_id")->row();
                $request_counts   = $req_withdraw_amt->counts;
              ?>
              <div class="resc-cont wow fadeInDown comm-greybg clearfix">
                <?php 
                if($request_counts == 0)
                {
                  ?>
                  <input type="hidden" name="minimum_balance" value="<?php echo $minimum_withdraw;?>" id="minimum_balance">
                  <?php 
                }
                else
                {
                  ?>
                  <input type="hidden" name="minimum_balance" value="<?php echo $remain_minimum_amt;?>" id="minimum_balance">
                  <?php 
                }
                ?>
                <div class="col-md-4 col-sm-4 col-xs-4 resc-contblk">
                  <center>
                    <h2>pending</h2>
                    <div class="morph">
                      <img class="center-block img-responsive" src="<?php echo $this->front_model->get_img_url();?>front/new/images/resc-icon1.png">
                    </div>
                    <p>Cashback R$ <?php echo $new_pending_cb;?></p>
                    <p>Bonus R$  <?php echo $new_pending_ref;?></p>
                    <?php $totalp = $pending_cb + $pending_ref; ?>
                    <div class="links cl-effect-8"> <a href=""> R$ <?php echo $this->front_model->currency_format($totalp); ?> </a> </div>
                  </center>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 resc-contblk">
                  <center>
                    <h2>confirmed</h2>
                    <div class="morph">
                      <img class="center-block img-responsive" src="<?php echo $this->front_model->get_img_url();?>front/new/images/resc-icon2.png">
                    </div>
                    <p>Cashback R$ <?php echo $new_cb_bal;//$newbal;?></p>
                    <p>Bonus R$ <?php echo $bonus_earning;?></p>
                    <?php $totalc = $balcne_cb + $bonus_amts; ?>
                    <div class="links cl-effect-8"> <a href=""> R$ <?php echo $this->front_model->currency_format($totalc); ?> </a> </div>
                  </center>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 resc-contblk">
                  <center>
                    <h2>available</h2>
                    <div class="morph">
                      <img class="center-block img-responsive" src="<?php echo $this->front_model->get_img_url();?>front/new/images/resc-icon3.png">
                    </div>
                    <h2> R$ <?php echo $newbal;?></h2>
                    <button type="button" data-toggle="modal" href="#withdraw" class="btn btn-resc1 hidden-xs">withdraw money</button>
                    <button type="button" data-toggle="modal" href="#withdraw" class="btn btn-resc1 btn-resc2">withdraw <br> money</
                  </center>
                </div>
              </div>
              <br>
              <div class="comm-cont wow fadeInDown">
                <h2 style="float:left;margin-right: 77%; margin-bottom: 20px; margin-top: 20px;">Meus Resgates</h2>
              </div>
              <!--account table section starts-->
              <div class="row wow fadeInDown">
                <div class="col-md-12 col-sm-12">
                  <div class="a">
                    <?php 
                    if(empty($result))
                    {
                      echo "<center>Você ainda não resgatou dinheiro nenhuma vez.</center><br>";
                    } 
                    else
                    { 
                      ?>
                      <table  id="sample_teste1" class="display zui-table zui-table-rounded table acc-table1">
                        <thead>
                          <tr>
                            <th class="hidden-xs">No</th> 
                            <th>Data</th>
                            <th>Valor</th>
                            <th>Status</th>   
                          </tr>
                        </thead>
                         
                        <tbody>
                          <?php
                         /* $k=1;
                          foreach($result as $rows)
                          {
                            ?>
                            <tr>
                              <td class="hidden-xs"><?php echo $k; ?></td>
                              <td><?php $data = $rows->date_added; echo date('d/m/y',strtotime($data)); ?></td>
                              <td>R$ <?php echo $this->front_model->currency_format($rows->requested_amount); ?></td>
                              <td>
                                <?php
                                if($rows->status=='Requested')
                                {
                                  ?>
                                  <label class="text-success" style="font-weight: 500;">Solicitado</label>
                                  <a class=" btn-xs pop tooltips" href="#">
                                    <span class="exc-blk"><i class="fa fa-info"></i></span>
                                    <span class="popupcls"><?=$resgate_requested?></span>
                                  </a>
                                  </td>
                                  <?php
                                }
                                if($rows->status=='Processing')
                                {
                                  ?>
                                  <label class="text-danger" style="font-weight: 500;">Enviado ao Banco</label>
                                  <a class=" btn-xs pop tooltips" href="#">  
                                    <span class="exc-blk"><i class="fa fa-info"></i></span>
                                    <span class="popupcls"><?=$resgate_processing?></span>
                                  </a>
                                  </td>
                                  <?php
                                }           
                                if($rows->status=='Completed')
                                {
                                  ?>
                                  <label class="text-danger" style="font-weight: 500;">Pagamento Realizado</label>
                                  <a class=" btn-xs pop tooltips" href="#">  
                                    <span class="exc-blk"><i class="fa fa-info"></i></span>
                                    <span class="popupcls"><?=$resgate_completed?></span>
                                  </a>
                                  </td>
                                  <?php
                                }
                                if($rows->status=='Cancelled')
                                {
                                  ?>
                                  <label class="text-danger" style="font-weight: 500;">Não Realizado</label>
                                  <a class=" btn-xs pop tooltips" href="#">   
                                    <span class="exc-blk"><i class="fa fa-info"></i></span>
                                    <span class="popupcls"><?=$resgate_cancelled?></span>
                                  </a>
                                  </td>
                                  <?php
                                }
                                ?>
                            </tr>     
                            <?php
                            $k++;
                          }*/
                          ?>
                        </tbody>
                         
                      </table>
                      <?php 
                    }
                    ?>
                    <input type="hidden" name="avail_bal" value="<?php echo $balcne;?>" id="avail_bal">
                  </div>
                </div>
              </div>
              <!--account table section ends-->
            </div>
            <br>
            <?php include('bottom_banners.php');?>
            <?php $this->load->view('front/my_earnings.php')?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>    
<!-- Main Content end -->

<!-- Withdraw modal popup section starts -->
<div class="modal fade wow bounceIn referfriend" id="withdraw" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header friend-popuphead">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i> </span></button>
        <center>
          <img src="<?php echo $this->front_model->get_img_url();?>front/new/images/pingou-logo1.png" alt="pingou" class="img-responsive">
        </center>
      </div>
      <div class="modal-body friend-popupbody">
          <div class="row fb-block">
            <center>
                <h3>Resgate seu dinheiro</h3>
            </center><br>
            <div class="center-block pad-no  accblk" style="background-color: #eee; width: 70%;border-radius: 10px;">
              <center><br>
                <h4>Solicitar Pagamento</h4><br>
                <h4>Disponível para saque: <strong>R$ <?php echo $newbal;?></strong></h4><br>
              </center>
              <br>
              <div class="alert alert-danger" id="requestset" style="display:none;" >
                <strong>
                  <span id="alertspan"></span>
                </strong>
              </div>
              <form role="form" action="javascript:;" method="post" id="login-form" autocomplete="off">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <label for="email" class="">Valor a ser sacado</label>
                  <input type="text" name="req_amount" id="req_amount" class="form-control req_amount" onkeypress="return isNumber(evt)" placeholder="">
                </div>
                <input type="hidden" id="acholder"   name="acholder" value="<?php echo $results->account_holder;?>">
                <input type="hidden" id="bankname"   name="bankname" value="<?php echo $results->bank_name;?>">
                <input type="hidden" id="branchname" name="branchname" value="<?php echo $results->branch_name;?>">
                <input type="hidden" id="acnumber"   name="account_number" value="<?php echo $results->account_number;?>">
                <input type="hidden" id="ifsccode"   name="ifsccode" value="<?php echo $results->ifsc_code;?>">
                <span id="testloader" style="display:none"> <center><img src="http://www.financemalta.org/design_images/loader.gif" width="120"></center></span>
                <center><input type="submit" id="btn-login_btk"  onClick="return validationset()" class="acc-commbtn hvr-pulse-shrink" value="Enviar"></center>
              </form>
            </div>
          </div>         
          <!-- <center>
            <button type="submit" class="btn btn-blu hvr-pulse-shrink">
              JOIN NOW FOR FREE
            </button>
          </center> -->          
      </div>    
    </div>
  </div>
</div>
<!-- Withdraw Popup End -->

<?php $withdraw_amt = $this->db->query("SELECT `requested_amount` FROM withdraw where user_id=$user_id ORDER BY withdraw_id DESC LIMIT 1")->row('requested_amount');
      $withdraw_amt = $this->front_model->currency_format($withdraw_amt);
?>
      
<!-- New code for success popup after withdraw request 16-3-17-->
<?php $result1       = $this->front_model->refer_friends();
      $referral_link = base_url().'?ref='.$result1[0]->random_code;
?>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  id="withdraw_success">
    <div class="modal-dialog" >
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title"><center><?php echo $usernames; ?> Your Withdraw is <br> successfully requested!</center></h4>
        </div>
        <div class="modal-body">
        Your referral Link is : <?php echo $referral_link; ?>
        <br><br/>
         <center>the amount of R$ <?php echo $withdraw_amt; ?> will be <br> available in your bank account in next monday.</center>
         <br><br>
         Enjoy it.
         <br>
         Thank you for using <?php echo $site_name;?>.

        </div>
        <center><img src="<?php echo base_url()?>uploads/adminpro/<?php echo $logo;?>"></center><br>
        <?php
        $listing = $this->db->query("select * from admin")->row();
        ?>
        <div class="modal-footer">
          <center><ul class="list-socials pull-rightnew">
            <?php if($listing->admin_twitter) {?> <li><a href="<?php echo $listing->admin_twitter;?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
            <?php if($listing->admin_fb)      {?> <li><a href="<?php echo $listing->admin_fb;?>">     <i class="fa fa-facebook"></i></a></li><?php } ?>
            <?php if($listing->admin_gplus)   {?> <li><a href="<?php echo $listing->admin_gplus;?>">  <i class="fa fa-google-plus"></i></a></li><?php } ?>
          </ul></center>
        </div>
        <!-- 
          <button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
         -->
      </div>

    </div>
</div>
<!-- End 16-3-17 -->

<style type="text/css">
  .pull-rightnew
  {
    float: none !important;
    margin-right: 40px;
  }
  .pull-rightnew li
  {
    display: inline-block;
    margin-right: 5px;
  }
  .pull-rightnew li a
  {
    background: #4daed9 none repeat scroll 0 0;
    border: 2px solid #407086;
    border-radius: 50%;
    color: #fff;
    display: block;
    height: 35px;
    line-height: 35px;
    text-align: center;
    transition: all 0.5s ease-out 0s;
    width: 35px;
  }
</style>

<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>


<?php $pagenames = $this->uri->segment(2);
if($pagenames == 'pending')
{
  ?>
    <script type="text/javascript">
      $(window).load(function()
      {
        $('#withdraw_success').modal('show');
      });
    </script>
  <?php 
}
?>

<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/mask.js"></script>

<script type="text/javascript"> 
  $('.req_amount').mask('00000000000,00', {reverse: true});
</script>



<script type="text/javascript">
/*$("#req_amount").on("keypress keyup",function()
{

  var len = $(this).val();
  len = len.replace(/^0+/, '');
  var len_1 = (len.length)+1;
  var ent_len = 3 - len_1;
  var out_val = '';
  if(len_1<3){
    for(var i=ent_len;i<3;i++){
      out_val = out_val + '1';
    }
    out_val = out_val + len;
    $(this).val(out_val);
  }
  else{
    len = len.replace(/^0+/, '');
    $(this).val(len);
  }

    //if($(this).val() == '0'){
    //  $(this).val('');  
    //}
});*/

function validationset()
{

  var acholder   = $('#acholder').val();
  var bankname   = $('#bankname').val();
  var branchname = $('#branchname').val();
  var acnumber   = $('#acnumber').val();
  var ifsccode   = $('#ifsccode').val();
  var req_amount = $('#req_amount').val();

  if(req_amount=='')
  {
    $('#req_amount').attr('style','border: 1px solid red;');
    $('#alertspan').html("Digite o valor que deseja sacar");
    $('#requestset').show();
    return false;
  }
 
  function isNumber(evt) 
  {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    if (key.length == 0) return;
    var regex = /^[0-9.,\b]+$/;
    if (!regex.test(key)) 
    {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
  }
  
  var avail_bal = $('#avail_bal').val();
  var minimum_balance = $('#minimum_balance').val();
  
  if(avail_bal =='' || avail_bal < 0)
  {
    $('#alertspan').html("Avaliable balance is needed");
    $('#requestset').show();
    return false;
  }

  if(parseFloat(req_amount)<parseFloat(minimum_balance))
  {
    $('#alertspan').html("O valor mínimo para solicitar um resgate é R$ "+minimum_balance);
    $('#requestset').show();
    return false;
  }

  if(parseFloat(avail_bal)<parseFloat(req_amount))
  {
    $('#alertspan').html("Você tem R$ "+avail_bal +" disponível para saque");
    $('#requestset').show();
    return false;
  }
  if(req_amount!='')
  {    
    if(parseFloat(req_amount) <= parseFloat(avail_bal))
    {
      if((acholder == '') || (bankname == '') || (branchname == '') || (acnumber == '') || (ifsccode == ''))
      {
        $('#req_amount').val('');
        $('#alertspan').html("Fill out your bank details before requesting withdraw");
        $('#requestset').show();
        return false;
      }
    }
  }
    $('#testloader').show();
    $('#btn-login_btk').hide();
    //alert(req_amount);
    
    $.ajax({
    type:'POST',
    url:'<?php echo base_url(); ?>cashback/add_withdraw',
    data:{'request':req_amount,'ifsccode':ifsccode},
    success:function(msg){
      $('#testloader').hide();
      $('#btn-login_btk').show();
      window.location.href = "<?php echo base_url(); ?>resgate/pending";
    }
  });
}
</script> 

<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.minnew.js"></script>
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.bootstrap.min.js"></script> 

<script type="text/javascript">
  $(document).ready(function() {
    $('#sample_teste1').DataTable({
    "processing": true,
    "serverSide": true,
    "columnDefs": [{
    "targets": 1,
    "orderable": false,
    "Length": 10,
    }],
    "ajax": {
    "url": "<?php echo site_url('cashback/newresgate')?>",
    "data": {
    //"totalrecords": "<?php echo $iTotal; ?>"
    "affiliate_name": "<?php echo $affiliate_name; ?>"
    }
    }
    });
  });
</script>
<!-- Footer menu End --> 
<style type="text/css">
 .dataTables_processing
{
  display:none !important;
}
</style>
 <link href="<?php echo $this->front_model->get_css_js_url(); ?>assets/css/pagination.css" rel="stylesheet" />
