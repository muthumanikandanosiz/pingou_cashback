<!--New code for Sales funnel banner details 8-11-16 -->
<?php
if($user_id == '')
{
  if($page_bot_status == 1)
  {
    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
    $imagename     = explode('.',$bannerdetails[0]->image_name);
    $image_name    = $imagename[0];
    ?>
    <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
      <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
    </a>
    <input type="hidden" name="bottom_bannerid" value="<?php echo $bannerdetails[0]->banner_id;?>">
    <?php 
  }
  if($page_bot_status == 2)
  {
    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
    $html_content  = $this->db->query("SELECT `not_log_html_settings` from `sales_funnel` where not_log_status !=0")->result();
    echo $html_content[0]->not_log_html_settings; 
  }
  if($page_bot_status == 3)
  {
    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0 order by RAND() limit 1")->result();
    $imagename     = explode('.',$bannerdetails[0]->image_name);
    $image_name    = $imagename[0];
    ?>
    <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
      <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
    </a>
    <input type="hidden" name="bottom_bannerid" value="<?php echo $bannerdetails[0]->banner_id;?>">
    <?php 
  }
  if($page_bot_status == 4)
  {
    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0")->result();
    $html_content  = $this->db->query("SELECT `notlog_standard_html_settings` from `sales_funnel` where notlog_standard_status !=0")->result();
    echo $html_content[0]->notlog_standard_html_settings;
  }
}
else
{
  if(($bonus_status == 0) && ($login_type == 0))
  {
    $conditiondetails = 'log_notusebonus_notuseapp';
    $html_name        = 'log_notusebonus_notuseapp_html_settings';
  }
  if(($bonus_status == 1) && ($login_type == 0))
  {
    $conditiondetails = 'log_usebonus_notuseapp';
    $html_name        = 'log_usebonus_notuseapp_html_settings';
  }
  if(($bonus_status == 0) && ($login_type == 1))
  {
    $conditiondetails = 'log_notusebonus_useapp';
    $html_name        = 'log_notusebonus_useapp_html_settings';
  }
  if(($bonus_status == 1) && ($login_type == 1))
  {
    $conditiondetails = 'log_usebonus_useapp';
    $html_name        = 'log_usebonus_useapp_html_settings';
  }

  if($page_bot_status == 1)
  {
    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='$conditiondetails' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
    $imagename     = explode('.',$bannerdetails[0]->image_name);
    $image_name    = $imagename[0];
    if($bannerdetails)
    {
      ?>
      <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
        <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
      </a>
      <input type="hidden" name="bottom_bannerid" value="<?php echo $bannerdetails[0]->banner_id;?>">
      <?php 
    }  
  }
  if($page_bot_status == 2)
  {
    $conditiondetails = $conditiondetails.'_status'; 
    $html_content     = $this->db->query("SELECT $html_name from `sales_funnel` where $conditiondetails!=0")->row($html_name);
    echo $html_content;  
  }
  if($page_bot_status == 3)
  {
    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='log_standard' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
    $imagename     = explode('.',$bannerdetails[0]->image_name);
    $image_name    = $imagename[0];
    if($bannerdetails)
    {
      ?>
      <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
        <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
      </a>
      <input type="hidden" name="bottom_bannerid" value="<?php echo $bannerdetails[0]->banner_id;?>">
      <?php     
    } 
  }
  if($page_bot_status == 4)
  {
    $html_content     = $this->db->query("SELECT `log_standard_html_settings` from `sales_funnel` where log_standard_status!=0")->row('log_standard_html_settings');
    echo $html_content;      
  }  
}

/*new code for bottom banners view count update details 20-3-17*/
$banner_id   = $bannerdetails[0]->banner_id;
if($banner_id!='')
{
  $getbanner   = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_id`=$banner_id AND `banner_status`=1")->row();
  $view_counts = $getbanner->view_counts;
  $add_count   = $view_counts + 1;

  $data = array(
    'view_counts' => $add_count,
    );
  $this->db->where('banner_id',$banner_id);
  $this->db->where('banner_status',1);
  $update = $this->db->update('sales_funnel_banners',$data);
}
?>
<!--End 20-3-17  -->
<!-- End 11-8-16-->