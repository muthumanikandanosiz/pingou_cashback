<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Register | <?php $admis = $this->front_model->getadmindetails_main(); echo $admis->site_name; ?></title>

<!-- Bootstrap -->

<?php $this->load->view('front/css_script');?>
<!-- tabs -->
<style>
.error {
	color:#ff0000;
}
.required_field {
	color:#ff0000;
}
</style>
</head>

<body>

<!-- header -->

<?php $this->load->view('front/header');?>

<!-- Header ends here -->

<div class="wrap-top">
  <div id="content">
    <div class="container">
      <section class="body-sign">
        <div class="center-sign">
          <div class="panel panel-sign">
            <div class="panel-title-sign mt-xl text-right">
              <h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign Up</h2>
            </div>
            <div class="panel-body">
              <?php 

					 $error = $this->session->flashdata('error');

					 if($error!="") {

						echo '<div class="alert alert-error">

						<button data-dismiss="alert" class="close">x</button>

						<strong>'.$error.'</strong></div>';

					} ?>
              <?php

						$success = $this->session->flashdata('success');

						if($success!="") {

								echo '<div class="alert alert-success">

									<button data-dismiss="alert" class="close">x</button>

									<strong>'.$success.' </strong></div>';

					} ?>
              
              <!-- BEGIN FORM-->
              
              <?php

								$atrtibute = array('role'=>'form','name'=>'regform','id'=>'regform','method'=>'post');

								echo form_open('register',$atrtibute);

								

						?>
              <input type="hidden" name="pagename" value="<?php echo $this->uri->segment(1)?>">
              <input type="hidden" name="uni_id" value="<?php echo $this->uri->segment(2)?>">
              <div class="form-group mb-lg">
                <label>Name <span class="required_field">*</span></label>
                <input type="text" class="form-control input-lg" name="first_name" id="first_name" autocomplete="off">
              </div>
              <!--<div class="form-group mb-lg">
                <label>Last Name <span class="required_field">*</span></label>
                <input type="text" class="form-control input-lg" name="last_name"  id="last_name" autocomplete="off">
              </div>-->
              <div class="form-group mb-lg">
                <label>E-mail Address <span class="required_field">*</span></label>
                <input type="email" class="form-control input-lg" name="user_email" id="user_email" onblur="return check_email();" autocomplete="off">
                <div id="unique_name_error"></div>
              </div>
              <div class="form-group mb-none">
                <div class="row">
                  <div class="col-sm-6 mb-lg">
                    <label>Password <span class="required_field">*</span></label>
                    <input type="password" class="form-control input-lg" name="user_pwd" id="user_pwd" autocomplete="off">
                  </div>
                  <div class="col-sm-6 mb-lg">
                    <label>Password Confirmation <span class="required_field">*</span></label>
                    <input type="password" class="form-control input-lg" name="pwd_confirm" id="pwd_confirm" autocomplete="off">
                  </div>
                </div>
              </div>
             <!-- <div class="form-group mb-lg">
                <label>Street <span class="required_field">*</span></label>
                <input type="text" class="form-control input-lg" name="street" id="street" autocomplete="off">
              </div>
              <div class="form-group mb-lg">
                <label>City <span class="required_field">*</span></label>
                <input type="text" class="form-control input-lg" name="city" id="city" autocomplete="off">
              </div>
              <div class="form-group mb-lg">
                <label>State <span class="required_field">*</span></label>
                <input type="text" class="form-control input-lg" name="state" id="state" autocomplete="off">
              </div>
              <div class="form-group mb-lg">
                <label>Zip Code <span class="required_field">*</span></label>
                <input type="text" maxlength="6" minlength="6" class="form-control input-lg" name="zipcode" id="zipcode" autocomplete="off">
              </div>-->
              
              <!--<div class="form-group mb-lg">

							<label>Country <span class="required_field">*</span></label>

							<select class="form-control input-lg" name="country" id="country" >

							<option value="">- Select Country -</option>  

									<?php								

								

									$country = $this->front_model->get_allcounties();

									foreach($country as $con)

									{

									?>

									<option value="<?php echo $con->id;?>"><?php echo $con->country_name;?></option>

									<?php

									}

									?>							

							</select>

							</div>-->
              
             <!-- <div class="form-group mb-lg">
                <label>Contact No <span class="required_field">*</span></label>
                <input type="text" class="form-control input-lg" name="contact_no" id="contact_no" autocomplete="off">
              </div>-->
              <div class="row">
                <div class="col-sm-8">
                  <div class="checkbox-custom checkbox-default">
                    <input type="checkbox" name="agreeterms" id="agreeterms">
                    <label for="AgreeTerms">I agree with <a target="_blank" href="<?php echo base_url();?>cms/terms-of-service" class="clr-blu">terms of use</a></label>
                  </div>
                </div>
                <div class="col-sm-4 text-right"> 
                  
                  <!--<button class="btn btn-danger hidden-xs pop" type="submit">Sign Up</button>

									<button class="btn btn-danger btn-block btn-lg visible-xs mt-lg pop" type="submit" name="register"></button>-->
                  
                  <input type="submit" name="register" value="Sign Up" class="btn btn-danger pop">
                </div>
              </div>
              <span class="mt-lg mb-lg line-thru text-center text-uppercase"> <span>or</span> </span>
              <p class="text-center">Already have an account?
                <?php

								/*$attribute = array('class'=>'btn btn-blue btn-sm pop');

								echo anchor('login','Sign In!',$attribute);*/

								echo '<a href="#myModal" data-toggle="modal" class="btn btn-blue">Sign-in</a>';

							?>
              </p>
              <?php  echo form_close();?>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>

<!-- footer -->

<?php $this->load->view('front/site_intro');?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 

<!-- Include all compiled plugins (below), or include individual files as needed --> 

<script src="<?php echo base_url();?>front/js/bootstrap.min.js"></script> 

<!-- Slider --> 

<!-- Scripts queries -->

<?php $this->load->view('front/js_scripts');?>
<script type="text/javascript">

$(function () { $("[data-toggle='tooltip']").tooltip(); });

</script> 
<script src="<?php echo base_url();?>front/js/jquery.min.js"></script> 
<script src="<?php echo base_url();?>front/js/jquery.validate.min.js"></script> 
<script type="text/javascript">

/* form validation*/

 $(document).ready(function() {

         $("#regform").validate({

	          rules: {

				first_name: {

                    required: true

                },

				last_name: {

                    required: true

                },

				user_email: {

                    required: true,

					email :true

                },

				user_pwd: {

                    required: true,

					minlength: 6

                },

				pwd_confirm: {

                    required: true,

					minlength: 6,

					equalTo:'#user_pwd'

                },

				street: {

                    required: true

                },

				city: {

                    required: true

                },

				state: {

                    required: true

                },

				zipcode: {

                    required: true,

					 minlength: 6

                },

				country: {

                    required: true

                },

				contact_no: {

                    required: true,

					//minlength: 10

                },

				agreeterms :{

							required: true

				}			

				

				

            },

            messages: {

				first_name: {

                    required: "Please enter your first name."                    

                },

				last_name: {

                    required: "Please enter  your last name."

                    

                },

				user_email: {

                    required: "Please enter  your valid Emailid."

                    

                },

				user_pwd: {

                    required: "Please enter the password.",

					minlength: "Passwords must be minimum 6 characters."    

                },

				pwd_confirm: {

                    required: "Please confirm your password.",

					minlength: "Passwords must be minimum 6 characters.",

					equalTo : "Please enter the same password."

                    

                },

				street: {

                    required: "Please enter  your street name."

                    

                },

				city: {

                    required: "Please enter  your city name."

                    

                },

				state: {

                    required: "Please enter  your state name."

                   

                },

				zipcode: {

                    required: "Please Enter  your  zip code.",

					minlength: "Zipcode must be minimum 6 digits."

                },

				country: {

                    required: "Please select your country."

                    

                },

				contact_no: {

                    required: "Please enter your contact number.",

					//minlength: "Contact number must be minimum 10 digits."

                    

                },

				agreeterms: {

					required: "Please accept our policy."

				}

				

            }

				

        });

});



//check email for  registration

function check_email()

{

	var email = $('#user_email').val();

	//alert(email);
if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
{
		$.ajax({

			type: 'POST',

			url: '<?php echo base_url();?>check_email',

			data:{'email':email},

			 success:function(result){

				if(result.trim()==1)

				{

					$("#unique_name_error").css('color','#29BAB0');
					

					 $("#unique_name_error").html('available.');

				}

				else

				{

					$("#unique_name_error").css('color','#ff0000');

					$("#unique_name_error").html('This email is already exists.');	

				}

			}

		});
}
	return false;

}

</script>
</body>
</html>
