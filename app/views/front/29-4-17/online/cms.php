<?php 
foreach($result as $views)
{			
	$meta_title = $views->cms_metatitle;
	$meta_key = $views->cms_metakey;
	$meta_desc = $views->cms_metadesc;
	$content = $views->cms_content;
	$heading = $views->cms_heading;
	$cms_title = $views->cms_title;

	$cms_img_one   = $views->cms_img_one;
	$cms_img_two   = $views->cms_img_two;
	$cms_img_three = $views->cms_img_three;
	$cms_img_four  = $views->cms_img_four;
}

$user_id = $this->session->userdata('user_id');

if($names=="oportunidade-unica")
{ 
	$admindetails = $admin->unic_bonus; 
	$unicbonus    = preg_replace('/\./', ',', $admindetails);
	
	if($user_id !='')
	{
		if($users->bonus_benefit == '0')
	    {	
			$this->db->where('cms_id',6);
			$cms_template = $this->db->get('tbl_cms');
			if($cms_template->num_rows >0) 
			{        
				$fetch 		   = $cms_template->row();
				$cms_templete = $fetch->cms_content;  
				$url 		   = base_url();
				$data = array(
				'###URL###'=>$url,
				'###AMT###'=>$unicbonus,
				'###AMOUNT###'=>$unicbonus,
				'###USERID###'=>$users->user_id
				);
				$content=strtr($cms_templete,$data); 
			}
		}
		else 
		{ 
			$content = "<center><h3>Benefit already used this account<h3></center>";
		} 
	}
	else
	{
		$content = "<center><h3>Must Login your account and use this feature <h3></center>";
	}	 
}
?>

<!-- Header content Start-->
<?php $admindetails = $this->front_model->getadmindetails_main();?>

<?php $this->load->view('front/cms_header'); ?>
<!-- header content End -->

<?php 
$user_id            = $this->session->userdata('user_id'); 
$userdetails        = $this->front_model->userdetails($user_id);
$random_code 		= $userdetails->random_code; 
if($userdetails->random_code == '')
{
	$random_code = '';	
}
$date     = date('Y-m-d');
$newdate  = explode('-',$date);
$year     = $newdate[0];
$month    = $newdate[1];
$day      = $newdate[2];
$days     = explode(' ',$day);
$newday   = $days[0];
$off_year = date('y');
$local_month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); 
$braz_months_names = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
$dt = date('F');
$current_month     = str_ireplace($local_month_names, $braz_months_names, $dt);

if($user_id)
{
	$get_cat_type 	 	 = $this->front_model->referal__category();
	$get_cat_details 	 = $this->front_model->get_referral_settings($get_cat_type);

	$type_one_percentage = $get_cat_details->ref_cashback;
	$type_one_days  	 = $get_cat_details->valid_months;
	$type_two_amount 	 = $get_cat_details->ref_cashback_rate;  
	$type_three_amount   = $get_cat_details->ref_cashback_rate_bonus;
	$type_three_users 	 = $get_cat_details->friends_count;

}


$data = array(
'###CMS-IMG-ONE###'=>"<img src=".$this->front_model->get_img_url()."uploads/adminpro/".$cms_img_one.">",
'###CMS-IMG-TWO###'=>"<img src=".$this->front_model->get_img_url()."uploads/adminpro/".$cms_img_two.">",
'###CMS-IMG-THREE###'=>"<img src=".$this->front_model->get_img_url()."uploads/adminpro/".$cms_img_three.">",
'###CMS-IMG-FOUR###'=>"<img src=".$this->front_model->get_img_url()."uploads/adminpro/".$users->cms_img_four.">",
'###Type-ONE###'=>$type_one_percentage,
'###Type-ONE-days###'=>$type_one_days,
'###Type-TWO###'=>$type_two_amount,
'###Type-THREE###'=>$type_three_amount,
'###Type-THREE-number-of-users###'=>$type_three_users,
'###DD###'    =>$newday,
'###MM###'    =>$month,
'###MONTH###' =>$current_month,
'###YYYY###'  =>$year,
'###YY###'    =>$off_year,
'###REFERRAL-PARAMETER###'=>$random_code
);
$content=strtr($content,$data); 
 
?>

<!-- stores
    ================================================== -->
<section class="cms wow fadeInDown">
  	<div class="container">
	    <div class="heading wow bounceIn">
	      <h2><span><?php echo $heading; ?></span></h2>
	      <div class="heading_border_cms"><span><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/top_drop.png"></span> </div>
	    </div>
	    <div class="about">
		    <div class="wow fadeInDown">
		      	<div id="accordion-faq" class="panel-group">
		        	<?php
					/*if($names=='perguntas-frequentes')
					{
						$i=1;
						$faq_res = $this->db->query('SELECT * FROM `tbl_faq` where status=1')->result();
						foreach($faq_res as $result)
						{
							
							$string = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $result->faq_ans);
							?>
			       			<div class="panel panel-default">
					          	<div class="panel-heading">
						            <h4 class="panel-title">
						            	<a aria-expanded="false" class="collapsed" href="#collapseOne<?php echo $i;?>" data-parent="#accordion-faq" data-toggle="collapse">
						                	<?php echo $i;?> . <?php echo $result->faq_qn;?>  
						              	</a> 
						            </h4>
						        </div>
					          	<div aria-expanded="false" class="panel-collapse collapse" id="collapseOne<?php echo $i;?>" style="">
					            	<div class="panel-body">
						            	<p>
						                	<?php echo $string;?>
						              	</p>
						            </div>
						        </div>
		        			</div>
			     			<?php
							$i++;
						}
				    }
				    else
				    {*/
				    	echo '<div class="wow fadeInDown">';
				    	echo $content;
				    	echo '</div>';
				    //}
				    ?>
		      	</div>
		    </div>
	    </div>
  	</div>
</section>

<?php $this->load->view('front/site_intro'); ?> 

<?php $this->load->view('front/sub_footer');?>

<script type="text/javascript">
	function clears(val)
{
	if(val==1)
		$('#email').css('border', '');
	else
		$('#news_email').css('border', '');
} 
 

 
function submit_user()
{	 
  var userids = document.getElementById('userids').value;
  var amount = document.getElementById('amount').value;
  
   
   if(amount!='')
  {
    $.ajax({
       type: "POST",
        url: '<?php echo base_url();?>cashback/onetime_benefit', 
        data: {userid: userids,amt:amount},
        dataType:"text", 
        success: 
          function(data)
          { 
            if(data==1)
            {

              $('#success').css("color","green");
              $('#success').text('Unic bônus added Successfully!');
              window.location.reload(true);
              location.href="<?php echo base_url();?>";

            }
            else
            {
              $('#success').css("color","red");
              $('#success').text('Unic bônus Not added Successfully');
            } 
          }
    });
  }
}
window.onpageshow = function(event) {
if (event.persisted) {
window.location.reload();
}
};
</script>
</body>
</html>

