<?php $this->load->view('front/header'); ?>
<!-- header content End -->

<!-- stores -->
<section class="cms wow fadeInDown">
  <div class="container">
    <div class="heading wow bounceIn">
      <h2>Category</h2>
      <div class="heading_border_cms">
        <span>
          <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/top_drop.png">
        </span>
      </div>
    </div>
    <!-- stores -->
    <br>
    <?php 
    $pagename               = $this->uri->segment(1);
    $user_id                = $this->session->userdata('user_id');
    $userdetails            = $this->front_model->userdetails($user_id);
    ?>
    <br>
    <?php
     
    $catstore_banner_details = $this->db->query("SELECT * from `sales_banner_status` where sales_banner_id =1")->row();
    $store_name              = $catstore_banner_details->categorystore;
    $castore_top_status      = $catstore_banner_details->categorystore_top_status;
    $castore_bot_status      = $catstore_banner_details->categorystore_bottom_status; 
    $category_type           = $userdetails->referral_category_type;
    $bonus_status            = $userdetails->bonus_benefit;
    $login_type              = $userdetails->app_login;
    $page_url                = '?utm_source=pingou&utm_medium=top-banner-'.$pagename.'-page&utm_campaign=';

    if($user_id == '')
    {
      if($castore_top_status == 1)
      {
        $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
        $imagename     = explode('.',$bannerdetails[0]->image_name);
        $image_name    = $imagename[0];
        ?>
        <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
          <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
        </a>
        <?php 
      }
      if($castore_top_status == 2)
      {
        $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
        $html_content  = $this->db->query("SELECT `not_log_html_settings` from `sales_funnel` where not_log_status !=0")->result();
        ?>
        <?php
        echo $html_content[0]->not_log_html_settings; 
      }
      if($castore_top_status == 3)
      {
        $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0 order by RAND() limit 1")->result();
        $imagename     = explode('.',$bannerdetails[0]->image_name);
        $image_name    = $imagename[0];
        ?>
        <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
          <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
        </a>
        <?php 
      }
      if($castore_top_status == 4)
      {
        $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0")->result();
        $html_content  = $this->db->query("SELECT `notlog_standard_html_settings` from `sales_funnel` where notlog_standard_status !=0")->result();
        ?>
        <?php
        echo $html_content[0]->notlog_standard_html_settings;
      }
    }
    else
    {
      if(($bonus_status == 0) && ($login_type == 0))
      {
        $conditiondetails = 'log_notusebonus_notuseapp';
        $html_name        = 'log_notusebonus_notuseapp_html_settings';
      }
      if(($bonus_status == 1) && ($login_type == 0))
      {
        $conditiondetails = 'log_usebonus_notuseapp';
        $html_name        = 'log_usebonus_notuseapp_html_settings';
      }
      if(($bonus_status == 0) && ($login_type == 1))
      {
        $conditiondetails = 'log_notusebonus_useapp';
        $html_name        = 'log_notusebonus_useapp_html_settings';
      }
      if(($bonus_status == 1) && ($login_type == 1))
      {
        $conditiondetails = 'log_usebonus_useapp';
        $html_name        = 'log_usebonus_useapp_html_settings';
      }

      if($castore_top_status == 1)
      {
        $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='$conditiondetails' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
        $imagename     = explode('.',$bannerdetails[0]->image_name);
        $image_name    = $imagename[0];
        if($bannerdetails)
        {
          ?>
          <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
            <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
          </a>
          <?php 
        }  
      }
      if($castore_top_status == 2)
      {
        $conditiondetails = $conditiondetails.'_status'; 
        $html_content     = $this->db->query("SELECT $html_name from `sales_funnel` where $conditiondetails!=0")->row($html_name);
        echo $html_content;
      }
      if($castore_top_status == 3)
      {
        $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='log_standard' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
        $imagename     = explode('.',$bannerdetails[0]->image_name);
        $image_name    = $imagename[0];
        if($bannerdetails)
        {
          ?>
          <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
            <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
          </a>
          <?php     
        } 
      }
      if($castore_top_status == 4)
      {              
        $html_content     = $this->db->query("SELECT `log_standard_html_settings` from `sales_funnel` where log_standard_status!=0")->row('log_standard_html_settings');
        echo $html_content;     
      } 
    }

    /*new code for Top banners view count update details 20-3-17*/
    $banner_id   = $bannerdetails[0]->banner_id;
    $getbanner   = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_id`=$banner_id AND `banner_status`=1")->row();
    $view_counts = $getbanner->view_counts;
    $add_count   = $view_counts + 1;

    $data = array(
      'view_counts' => $add_count,
      );
    $this->db->where('banner_id',$banner_id);
    $this->db->where('banner_status',1);
    $update = $this->db->update('sales_funnel_banners',$data);
    ?> 
    <!--End 20-3-17  -->
    <br>
    <section class="cashback" >
      <div class="categories">
        <div class="row">
          <?php 
          $categories = $this->front_model->get_all_categories();
          $kt = 1;
          foreach($categories as $view)
          {
            $category_id     = $view->category_id; 
            $category_image  = $view->category_img;
            $category_name   = $view->category_url;
            $category_names  = $view->category_name;
            ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="catcoup-container">
                  <div class="cate coup"> 
                    <?php if($category_image!='') 
                    {
                    ?>
                      <img src="<?php echo $this->front_model->get_img_url();?>uploads/img/<?php echo $category_image;?>" style="height:265px!important; max-width:none!important;" width="265" class="img-responsive center-block">
                    <?php
                    }
                    else
                    {?>
                      <img src="<?php echo $this->front_model->get_img_url();?>front/new/images/c1.png" class="img-responsive center-block">
                    <?php
                    } 
                    ?>
                    <div class="overlay">
                      <div class="cat_content"> 
                        <a class="cat_btn1" href="<?php echo base_url();?>cupom-desconto/<?php echo $category_name;?>">
                          <span> <?php echo $category_names;?> </span>
                        </a> 
                        <a class="strlnk md-trigger md-setperspective menulink" data-modal="modal-<?php echo $category_id;?>">
                          <span>
                            <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/link.png">
                          </span>
                        </a> 
                      </div>
                    </div>
                  </div>
                  <div class="cat-coup-mask">
                    <a class="cat_btn" href="#">
                      <span><?php echo $category_names;?></span>
                    </a>
                  </div>
                </div>
            </div>
            <div class="md-modal md-effect-<?php echo $view->category_id;?>" id="modal-<?php echo $view->category_id;?>">
                <div class="md-content">
                  <h3>About <?php echo $view->category_name;?></h3>                
                  <div>
                    <center>
                      <p><?php echo $view->category_desc; ?></p>                         
                      <button class="md-close btn btn-primary">CLOSE</button>
                    </center>
                  </div>
                </div>
            </div>
            <?php 
          }
          ?>
        </div>
        <div> 
        </div>
      </div>
    </section>
    <br>
    <?php 
    if($user_id == '')
    {
      if($castore_bot_status == 1)
      {
        $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
        $imagename     = explode('.',$bannerdetails[0]->image_name);
        $image_name    = $imagename[0];
        ?>
        <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
          <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
        </a>
        <?php 
      }
      if($castore_bot_status == 2)
      {
        $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
        $html_content  = $this->db->query("SELECT `not_log_html_settings` from `sales_funnel` where not_log_status !=0")->result();
        ?>
        <?php
        echo $html_content[0]->not_log_html_settings; 
      }
      if($castore_bot_status == 3)
      {
        $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0 order by RAND() limit 1")->result();
        $imagename     = explode('.',$bannerdetails[0]->image_name);
        $image_name    = $imagename[0];
        ?>
        <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
          <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
        </a>
        <?php 
      }
      if($castore_bot_status == 4)
      {
        $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0")->result();
        $html_content  = $this->db->query("SELECT `notlog_standard_html_settings` from `sales_funnel` where notlog_standard_status !=0")->result();
        echo $html_content[0]->notlog_standard_html_settings;
      }
    }
    else
    {
      if(($bonus_status == 0) && ($login_type == 0))
      {
        $conditiondetails = 'log_notusebonus_notuseapp';
        $html_name        = 'log_notusebonus_notuseapp_html_settings';
      }
      if(($bonus_status == 1) && ($login_type == 0))
      {
        $conditiondetails = 'log_usebonus_notuseapp';
        $html_name        = 'log_usebonus_notuseapp_html_settings';
      }
      if(($bonus_status == 0) && ($login_type == 1))
      {
        $conditiondetails = 'log_notusebonus_useapp';
        $html_name        = 'log_notusebonus_useapp_html_settings';
      }
      if(($bonus_status == 1) && ($login_type == 1))
      {
        $conditiondetails = 'log_usebonus_useapp';
        $html_name        = 'log_usebonus_useapp_html_settings';
      }

      if($castore_bot_status == 1)
      {
        $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='$conditiondetails' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
        $imagename     = explode('.',$bannerdetails[0]->image_name);
        $image_name    = $imagename[0];
        if($bannerdetails)
        {
          ?>
          <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
            <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
          </a>
          <?php 
        }  
      }
      if($castore_bot_status == 2)
      {
        $conditiondetails = $conditiondetails.'_status'; 
        $html_content     = $this->db->query("SELECT $html_name from `sales_funnel` where $conditiondetails!=0")->row($html_name);
        echo $html_content;  
      }
      if($castore_bot_status == 3)
      {
        $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='log_standard' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
        $imagename     = explode('.',$bannerdetails[0]->image_name);
        $image_name    = $imagename[0];
        if($bannerdetails)
        {
          ?>
          <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
            <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
          </a>
          <?php     
        } 
      }
      if($castore_bot_status == 4)
      {
        $html_content     = $this->db->query("SELECT `log_standard_html_settings` from `sales_funnel` where log_standard_status!=0")->row('log_standard_html_settings');
        echo $html_content;     
      }
    }
    /*new code for Top banners view count update details 20-3-17*/
    $banner_id   = $bannerdetails[0]->banner_id;
    $getbanner   = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_id`=$banner_id AND `banner_status`=1")->row();
    $view_counts = $getbanner->view_counts;
    $add_count   = $view_counts + 1;

    $data = array(
      'view_counts' => $add_count,
      );
    $this->db->where('banner_id',$banner_id);
    $this->db->where('banner_status',1);
    $update = $this->db->update('sales_funnel_banners',$data);
    ?> 
    <!--End 20-3-17  -->
  </div>
</section>

<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>
<!-- Footer Menu End-->