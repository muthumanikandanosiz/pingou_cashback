<?php $this->load->view('front/header'); ?>
<!-- header content End -->
<!-- New code for sales funnel page banner details 7-11-16 -->
<?php
$pagename                = $this->uri->segment(1);
$user_id       			     = $this->session->userdata('user_id');
$userdetails             = $this->front_model->userdetails($user_id);
$store_banner_details    = $this->db->query("SELECT * from `sales_banner_status` where sales_banner_id =1")->row();
$store_top_status        = $store_banner_details->categorystore_top_status;
$store_bot_status        = $store_banner_details->categorystore_bottom_status; 

$category_type           = $userdetails->referral_category_type;
$bonus_status            = $userdetails->bonus_benefit;
$login_type              = $userdetails->app_login;
$page_url                = '?utm_source=pingou&utm_medium=top-banner-'.$pagename.'-page&utm_campaign='; 
?>
<!-- End 7-11-16 -->

<!-- Main Content start -->
<!-- stores -->
 <section class="cms wow fadeInDown">
    <div class="container">
    	<div class="heading wow bounceIn">
        	<h2> All <span>Stores</span></h2>
            <div class="heading_border_cms"><span><img src="<?php echo $this->front_model->get_img_url();?>front/new/images/top_drop.png"></span> </div>
        </div>
        <!-- new code for sales funnel banner details 8-11-16 -->
        <?php 
        if($user_id == '')
        {
          if($store_top_status == 1)
          {
            $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
            $imagename     = explode('.',$bannerdetails[0]->image_name);
            $image_name    = $imagename[0];
            ?>
            <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
              <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
            </a>
            <?php 
          }
          if($store_top_status == 2)
          {
            $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0")->result();
            $html_content  = $this->db->query("SELECT `not_log_html_settings` from `sales_funnel` where not_log_status !=0")->result();
            ?>
            <!-- <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
              <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
            </a> -->
            <?php
            echo $html_content[0]->not_log_html_settings; 
          }
          if($store_top_status == 3)
          {
            $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0 order by RAND() limit 1")->result();
            $imagename     = explode('.',$bannerdetails[0]->image_name);
            $image_name    = $imagename[0];
            ?>
            <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
              <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
            </a>
            <?php 
          }
          if($store_top_status == 4)
          {
            $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0")->result();
            $html_content  = $this->db->query("SELECT `notlog_standard_html_settings` from `sales_funnel` where notlog_standard_status !=0")->result();
            ?>
           <!--  <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
              <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
            </a> -->
            <?php
            echo $html_content[0]->notlog_standard_html_settings;
          }
        }
        else
        {
          if(($bonus_status == 0) && ($login_type == 0))
          {
            $conditiondetails = 'log_notusebonus_notuseapp';
            $html_name        = 'log_notusebonus_notuseapp_html_settings';
          }
          if(($bonus_status == 1) && ($login_type == 0))
          {
            $conditiondetails = 'log_usebonus_notuseapp';
            $html_name        = 'log_usebonus_notuseapp_html_settings';
          }
          if(($bonus_status == 0) && ($login_type == 1))
          {
            $conditiondetails = 'log_notusebonus_useapp';
            $html_name        = 'log_notusebonus_useapp_html_settings';
          }
          if(($bonus_status == 1) && ($login_type == 1))
          {
            $conditiondetails = 'log_usebonus_useapp';
            $html_name        = 'log_usebonus_useapp_html_settings';
          }

          if($store_top_status == 1)
          {
            $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='$conditiondetails' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
            $imagename     = explode('.',$bannerdetails[0]->image_name);
            $image_name    = $imagename[0];
            if($bannerdetails)
            {
              ?>
              <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
              </a>
              <?php 
            }  
          }
          if($store_top_status == 2)
          {
            
            //$bannerdetails    = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='$conditiondetails' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0")->result();
            $conditiondetails = $conditiondetails.'_status'; 
            $html_content     = $this->db->query("SELECT $html_name from `sales_funnel` where $conditiondetails!=0")->row($html_name);
            //print_r($bannerdetails);
            //if($bannerdetails)
            //{
              ?>
              <!-- <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
              </a> -->
              <?php
              echo $html_content;
            //}  
          }
          if($store_top_status == 3)
          {
            $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='log_standard' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
            $imagename     = explode('.',$bannerdetails[0]->image_name);
            $image_name    = $imagename[0];
            if($bannerdetails)
            {
              ?>
              <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
              </a>
              <?php     
            } 
          }
          if($store_top_status == 4)
          {
            //$bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='log_standard' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0")->result();
            $html_content     = $this->db->query("SELECT `log_standard_html_settings` from `sales_funnel` where log_standard_status!=0")->row('log_standard_html_settings');
            //if($bannerdetails)
            //{
              ?>
              <!-- <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
              </a> -->
              <?php
              echo $html_content;     
            //} 
          }
        }
        /*new code for Top banners view count update details 20-3-17*/
        $banner_id   = $bannerdetails[0]->banner_id;
        $getbanner   = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_id`=$banner_id AND `banner_status`=1")->row();
        $view_counts = $getbanner->view_counts;
        $add_count   = $view_counts + 1;

        $data = array(
          'view_counts' => $add_count,
          );
        $this->db->where('banner_id',$banner_id);
        $this->db->where('banner_status',1);
        $update = $this->db->update('sales_funnel_banners',$data);
        ?> 
        <!--End 20-3-17  -->
        <!-- End 8-11-16 -->
        <!-- stores -->
        <section class="cashback" >
        	<div class="container">
            	<div class="coupon stores allstores">

                	<!-- Nav tabs -->
                    <?php 
		    	    if($stores_list)
				   	{
						?>
                    	<ul class="nav nav-tabs cash_tab wow fadeInUp" role="tablist">
	                     	<li class="active"> 
			               		<a href="#" data-filter="*" aria-controls="1" role="tab" data-toggle="tab">
			            			<div align="center">
			            				<div class="tab_ico">
			              					<div class="inr_sec">
			                					<div class="inr_img"> 
			                						<div class="str-cont">All</div>
			                					</div>
			              					</div>
			            				</div>
			          				</div>              
			              		</a>
			            	</li>
			            	<li class=""> 
			               		<a href="#" data-filter=".0-9" aria-controls="1" role="tab" data-toggle="tab">
			            			<div align="center">
			            				<div class="tab_ico">
			              					<div class="inr_sec">
			                					<div class="inr_img"> 
			                						<div class="str-cont">0-9</div>
			                					</div>
			              					</div>
			            				</div>
			          				</div>              
			              		</a>
			            	</li>
			            	<?php
							$i=1;
							foreach (range('A', 'Z') as $char) 
							{	
								?>
								<li class="">
									<a role="tab" data-filter=".<?php echo $char;?>" data-toggle="tab" aria-controls="<?php echo $i;?>" href="#">
										<div align="center">
			            					<div class="tab_ico">
			              						<div class="inr_sec">
			                						<div class="inr_img"> 
			                							<div class="str-cont"><?php echo $char;?></div>
			                						</div>
			                					</div>
			                				</div>
			                			</div>				
									</a>
								</li>
								<?php
								$i++;
							}
							?>    
                    	</ul>
                    
                    	<!-- Tab panes -->
                    	<div class="tab-content">
	                        <div class="tab-pane active" id="1">
	                           	<div class="mar-top-20 cashback">
	                              	<div class="row wow flipInX isotope-container categories" id="loaddiv_show" style="position: relative;  height: 786.034px; opacity: 0;">
	                                	<?php
										$k=1;
										foreach($stores_list as $stores)
										{
											$affiliate_id     = $stores->affiliate_id;
											$featured 	      = $stores->featured;
											$affiliate_name   = $stores->affiliate_name;
											$count_coupons    = $this->front_model->count_coupons($affiliate_name);
											$get_coupons_sets = $this->front_model->get_coupons_sets($affiliate_name,2);
											$setup 			      = "";
											$namess 		      = ""; //heading
											$colors_li 		    = '';		
											$setup 		        =  strtoupper($affiliate_name[0]);
											$aff_logo         = $stores->affiliate_logo;
                      if($aff_logo == '')
                      {
                        $aff_logo = 'no_image.png';
                      }
                      ?>    

		                                	<div class="col-md-2 col-sm-4 col-xs-6 isotope-item <?php echo $setup;?>" style="position: absolute; left: 0px; top: 0px;">
			                                    <div class="storeblk hvr-border-fade cate coup">
			                                        <center>
			                                          <img alt="<?php echo $aff_logo;?>" src="<?php echo $this->front_model->get_img_url(); ?>uploads/affiliates/<?php echo $aff_logo;?>" style="margin-top: -24px;" class="img-responsive">
			                                        </center>
			                                        <h5 class="str-tit"><?php echo $stores->affiliate_name; ?></h5>
			                                        <div class="overlay">
			                                          	<div class="cat_content">
			                                            	<a href="<?php echo base_url();?>cupom-desconto/<?php echo $stores->affiliate_url;?>" class="cat_btn1"> <span><?php echo $stores->affiliate_name; ?></span> </a> 
			                                             	<div class="cls_storemenubtn strlnk md-trigger" data-modal="modal-<?php echo $k;?>">
			                                             		<a href="#" class="menulink">
			                                             			<span>
			                                             				<img src="<?php echo $this->front_model->get_img_url();?>front/new/images/link.png">
			                                             			</span>
			                                             		</a>
			                                             	</div>	 
			                                          	</div>
			                                        </div>                      
			                                    </div>
		                                	</div>
		                                	
		                                	<div class="md-modal md-effect-<?php echo $k;?>" id="modal-<?php echo $k;?>">
										        <div class="md-content">
										            <h3><?php echo $stores->affiliate_name; ?></h3>
										            <div>
										        	    <center>
										            	    <p><?php echo $stores->affiliate_desc;//echo substr($stores->affiliate_desc,0,400)."...";?></p>
										                	<button class="md-close btn btn-primary">CLOSE</button>
										                </center>
										            </div>
										        </div>
										    </div>
		                                	<?php
											$k++;
										}
								    	?>    
	                            	</div>
	                            	<h3>Ofteras Descripitions</h3>
          							<?php echo $category_details->ofertas_description;?>
	                            	
	                        	</div>
	                    	</div>
                			<div></div>
        				</div>
        				<?php
			        }
			        else
			        {
			        	echo'<center>
						        <strong>No Stores are available at this category!</strong>
						    </center>';
			        }	
			        ?>	
    			</div>
    		</div>	
		</section>
		 
		<!-- New code for Sales funnel banner details 8-11-16 -->
        <?php
        if($user_id == '')
        {
          if($store_bot_status == 1)
          {
            $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
            $imagename     = explode('.',$bannerdetails[0]->image_name);
            $image_name    = $imagename[0];
            ?>
            <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
              <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
            </a>
            <?php 
          }
          if($store_bot_status == 2)
          {
            $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0")->result();
            $html_content  = $this->db->query("SELECT `not_log_html_settings` from `sales_funnel` where not_log_status !=0")->result();
            ?>
            <!-- <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
              <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
            </a> -->
            <?php
            echo $html_content[0]->not_log_html_settings; 
          }
          if($store_bot_status == 3)
          {
            $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0 order by RAND() limit 1")->result();
            $imagename     = explode('.',$bannerdetails[0]->image_name);
            $image_name    = $imagename[0];
            ?>
            <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
              <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
            </a>
            <?php 
          }
          if($store_bot_status == 4)
          {
            $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0")->result();
            $html_content  = $this->db->query("SELECT `notlog_standard_html_settings` from `sales_funnel` where notlog_standard_status !=0")->result();
            ?>
            <!-- <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
              <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
            </a> -->
            <?php
            echo $html_content[0]->notlog_standard_html_settings;
          }
        }
        else
        {
          if(($bonus_status == 0) && ($login_type == 0))
          {
            $conditiondetails = 'log_notusebonus_notuseapp';
            $html_name        = 'log_notusebonus_notuseapp_html_settings';
          }
          if(($bonus_status == 1) && ($login_type == 0))
          {
            $conditiondetails = 'log_usebonus_notuseapp';
            $html_name        = 'log_usebonus_notuseapp_html_settings';
          }
          if(($bonus_status == 0) && ($login_type == 1))
          {
            $conditiondetails = 'log_notusebonus_useapp';
            $html_name        = 'log_notusebonus_useapp_html_settings';
          }
          if(($bonus_status == 1) && ($login_type == 1))
          {
            $conditiondetails = 'log_usebonus_useapp';
            $html_name        = 'log_usebonus_useapp_html_settings';
          }

          if($store_bot_status == 1)
          {
            $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='$conditiondetails' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
            $imagename     = explode('.',$bannerdetails[0]->image_name);
            $image_name    = $imagename[0];
            if($bannerdetails)
            {
              ?>
              <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
              </a>
              <?php 
            }  
          }
          if($store_bot_status == 2)
          {
            
            //$bannerdetails    = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='$conditiondetails' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0")->result();
            $conditiondetails = $conditiondetails.'_status'; 
            $html_content     = $this->db->query("SELECT $html_name from `sales_funnel` where $conditiondetails!=0")->row($html_name);
            //print_r($bannerdetails);
            //if($bannerdetails)
            //{
              ?>
              <!-- <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
              </a> -->
              <?php
              echo $html_content;
            //}  
          }
          if($store_bot_status == 3)
          {
            $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='log_standard' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
            $imagename     = explode('.',$bannerdetails[0]->image_name);
            $image_name    = $imagename[0];
            if($bannerdetails)
            {
              ?>
              <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
              </a>
              <?php     
            } 
          }
          if($store_bot_status == 4)
          {
            //$bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='log_standard' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0")->result();
            $html_content     = $this->db->query("SELECT `log_standard_html_settings` from `sales_funnel` where log_standard_status!=0")->row('log_standard_html_settings');
           // if($bannerdetails)
            //{
              ?>
              <!-- <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
              </a> -->
              <?php
              echo $html_content;     
           // } 
          }
        }
        /*new code for Top banners view count update details 20-3-17*/
        $banner_id   = $bannerdetails[0]->banner_id;
        $getbanner   = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_id`=$banner_id AND `banner_status`=1")->row();
        $view_counts = $getbanner->view_counts;
        $add_count   = $view_counts + 1;

        $data = array(
          'view_counts' => $add_count,
          );
        $this->db->where('banner_id',$banner_id);
        $this->db->where('banner_status',1);
        $update = $this->db->update('sales_funnel_banners',$data);
        ?> 
        <!--End 20-3-17  -->
        <!-- End 8-11-16 -->
        <br>
	</div>
</section>
<!-- Main Content end -->


 <!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
 
<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>
<!-- Footer Menu End-->	

<script>
// Wait for window load
$(window).load(function() {
	// Animate loader off screen			
	$.when($('#loaddiv').fadeOut(500)).done(function() {
//    alert("Now all '.hotel_photo_select are hidden'");
	document.getElementById('loaddiv_show').style.removeProperty('opacity');
	});
});


</script>

<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.colorbox-min.js"></script> 

<script type="text/javascript">
	jQuery(document).ready(function() 
	{
	    jQuery('.colorbox').colorbox
	    ({
			overlayClose: true,
			opacity: 0.5,
			rel: false,
			onLoad:function()
			{
				jQuery("#cboxNext").remove(0);
				jQuery("#cboxPrevious").remove(0);
				jQuery("#cboxCurrent").remove(0);
			}
	    });
	});
</script> 

<script type="text/javascript">
jQuery(document).ready(function() 
{
	jQuery(".ves-colorbox").colorbox
	({
		width: '60%', 
		height: '80%',
		overlayClose: true,
		opacity: 0.5,
		iframe: true, 
	});
});
</script> 

<!-- Modernizr javascript --> 
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/modernizr.js"></script> 
<!-- Isotope javascript --> 
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/isotope.pkgd.min.js"></script> 
<!-- Initialization of Plugins --> 
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/template.js"></script> 

<script>

// Isotope filters
//-----------------------------------------------
if ($('.isotope-container').length>0) {
	$(window).load(function() {
		var $container = $('.isotope-container').isotope({
			itemSelector: '.isotope-item',
			layoutMode: 'masonry',
			transitionDuration: '0.6s',
			filter: "*"
		});
		// filter items on button click
		$('.allstores').on( 'click', 'ul.fadeInUp li a', function() {
			var filterValue = $(this).attr('data-filter');
			$(".allstores").find("li.active").removeClass("active");
			$(this).parent().addClass("active");
			$container.isotope({ filter: filterValue });
			return false;
		});
	});
};     
</script>
 

<!--<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/owl.carousel.js"></script> -->
<script>

/*$(document).ready(function() {

$('.owl-carousel').owlCarousel({

loop: true,

margin: 1,

responsiveClass: true,

responsive: {

0: {

items: 1,

nav: true

},

600: {

items: 2,

nav: true

},

1150: {

items: 4,

nav: true,

loop: false,

margin:0

}

}

})

})*/

</script> 
<script type="text/javascript">
$(function () { $("[data-toggle='tooltip']").tooltip(); });

function toggle_st(num)
{
	$('.toggle'+num).toggle('slow');
	return false;	
}

function show_cate()
{
	$('.dynamiccls').show();
	$('#load_m').hide();
}

function runcheck_1(url)
{
	window.location.href="<?php echo base_url();?>"+url;
}
</script>