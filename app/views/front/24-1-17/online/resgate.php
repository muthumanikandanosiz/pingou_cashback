<?php $this->load->view('front/header'); ?>
<!-- header content End -->
<style>
.btn{
  white-space: normal!important;
}
a.tooltips {
  position: relative;
  display: inline;
}
a.tooltips .popupcls {
 background: #3da0d5 none repeat scroll 0 0;
    border-radius: 2px;
    color: #ffffff;
    line-height: 20px;
    min-height: 30px;
    min-width: 100%;
    position: absolute;
    text-align: center;
    visibility: hidden;
    width: 250px!important;
    word-wrap: break-word; left: -5em !important;
    font-size: 15px !important;
}
a.tooltips .popupcls:after {
  content: '';
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -8px;
  width: 0; height: 0;
  border-top: 8px solid #3da0d5;
  border-right: 8px solid transparent;
  border-left: 8px solid transparent;
}
a:hover.tooltips .popupcls {
  visibility: visible;

  bottom: 30px;
  left: 50%;
  margin-left: -76px;
  z-index: 999;
}
/*}*/
</style>
<style type="text/css">
  .table-responsive {
    overflow-x: unset;
}
</style>
<!-- Main Content start -->
<?php $user_id = $this->session->userdata('user_id');?>
<section class="cms wow fadeInDown">
  <div class="container">
    <div class="heading wow bounceIn">
      <h2>
        <span>Resgate</span>
      </h2>
      <div class="heading_border_cms">
        <span>
          <img src="<?php echo $this->front_model->get_img_url();?>front/new/images/top_drop.png">
        </span>
      </div>
    </div>
    <div class="myac">
      <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1" id="hide_fn">
        <div class="my_account my_accblk">
          <div class="myacc-maintab">
            <!-- Nav tabs -->
            <?php $this->load->view('front/user_menu'); ?>
          </div>

          <!-- Tab panes -->
          <div class="tab-content">
            <?php include('top_banners.php');?><br>
            <div role="tabpanel" id="acc3">
              <?php 
              foreach ($hover as $hs) 
              {
                $resgate_requested  = $hs->resgate_requested;
                $resgate_processing = $hs->resgate_processing; 
                $resgate_completed  = $hs->resgate_completed;  
                $resgate_cancelled  = $hs->resgate_cancelled;
              }
              $pending_cb         =  $this->front_model->pending_cashback($user_id);
              $new_pending_cb     =  $this->front_model->currency_format($pending_cb);

              $pending_ref        =  $this->front_model->pending_referral($user_id);
             
              $new_pending_ref    =  $this->front_model->currency_format($pending_ref);
              
              $balcne_cb          =  $this->front_model->user_cashback_balance($user_id);
              $new_cb_bal         =  $this->front_model->currency_format($balcne_cb);


              /*29-8-16*/
              
              $reff               =  $this->front_model->ref_earnings($user_id);
              $ref_earning        =  $this->front_model->currency_format($reff);

              $balcne             =  $this->front_model->user_balance($user_id);
              $newbal             =  $this->front_model->currency_format($balcne);
             
              $minimum_withdraw   =  $this->front_model->minimum_withdraw();
              ?>
              
              <div class="resc-cont wow fadeInDown comm-greybg clearfix">
                <input type="hidden" name="minimum_balance" value="<?php echo $minimum_withdraw;?>" id="minimum_balance">
                  
                <div class="col-md-4 col-sm-4 col-xs-4 resc-contblk">
                  <center>
                    <h2>pending</h2>
                    <div class="morph">
                      <img class="center-block img-responsive" src="<?php echo $this->front_model->get_img_url();?>front/new/images/resc-icon1.png">
                    </div>
                    <p>Cashback R$ <?php echo $new_pending_cb;?></p>
                    <p>Referal R$  <?php echo $new_pending_ref;?></p>
                    <?php $totalp = $pending_cb + $pending_ref; ?>
                    <div class="links cl-effect-8"> <a href=""> R$ <?php echo $this->front_model->currency_format($totalp); ?> </a> </div>
                  </center>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 resc-contblk">
                  <center>
                    <h2>confirmed</h2>
                    <div class="morph">
                      <img class="center-block img-responsive" src="<?php echo $this->front_model->get_img_url();?>front/new/images/resc-icon2.png">
                    </div>
                    <p>Cashback R$ <?php echo $new_cb_bal;//$newbal;?></p>
                    <p>Referal R$ <?php echo $ref_earning;?></p>
                    <?php $totalc = $balcne_cb + $reff; ?>
                    <div class="links cl-effect-8"> <a href=""> R$ <?php echo $this->front_model->currency_format($totalc); ?> </a> </div>
                  </center>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 resc-contblk">
                  <center>
                    <h2>available</h2>
                    <div class="morph">
                      <img class="center-block img-responsive" src="<?php echo $this->front_model->get_img_url();?>front/new/images/resc-icon3.png">
                    </div>
                    <h2> R$ <?php echo $newbal;?></h2>
                    <button type="button" data-toggle="modal" href="#withdraw" class="btn btn-resc1 hidden-xs">withdraw money</button>
                    <button type="button" data-toggle="modal" href="#withdraw" class="btn btn-resc1 btn-resc2">withdraw <br> money</
                  </center>
                </div>
              </div>
              <br>
              <div class="comm-cont wow fadeInDown">
                <h2 style="float:left;margin-right: 77%; margin-bottom: 20px; margin-top: 20px;">Meus Resgates</h2>
              </div>
              <!--account table section starts-->
              <div class="row wow fadeInDown">
                <div class="col-md-12 col-sm-12">
                  <div class="a">
                    <?php 
                    if(empty($result))
                    {
                      echo "<center>Você ainda não resgatou dinheiro nenhuma vez.</center><br>";
                    } 
                    else
                    {
                      ?>
                      <table cl id="example" class="display zui-table zui-table-rounded table acc-table1">
                        <thead>
                          <tr>
                            <th class="hidden-xs">No</th> 
                            <th>Data</th>
                            <th>Valor</th>
                            <th>Status</th>   
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $k=1;
                          foreach($result as $rows)
                          {
                            ?>
                            <tr>
                              <td class="hidden-xs"><?php echo $k; ?></td>
                              <td><?php $data = $rows->date_added; echo date('d/m/y',strtotime($data)); ?></td>
                              <td>R$ <?php echo $this->front_model->currency_format($rows->requested_amount); ?></td>
                              <td>
                                <?php
                                if($rows->status=='Requested')
                                {
                                  ?>
                                  <label class="text-success" style="font-weight: 500;">Solicitado</label>
                                  <a class=" btn-xs pop tooltips" href="#">
                                    <span class="exc-blk"><i class="fa fa-info"></i></span>
                                    <span class="popupcls"><?=$resgate_requested?></span>
                                  </a>
                                  </td>
                                  <?php
                                }
                                if($rows->status=='Processing')
                                {
                                  ?>
                                  <label class="text-danger" style="font-weight: 500;">Enviado ao Banco</label>
                                  <a class=" btn-xs pop tooltips" href="#">  
                                    <span class="exc-blk"><i class="fa fa-info"></i></span>
                                    <span class="popupcls"><?=$resgate_processing?></span>
                                  </a>
                                  </td>
                                  <?php
                                }           
                                if($rows->status=='Completed')
                                {
                                  ?>
                                  <label class="text-danger" style="font-weight: 500;">Pagamento Realizado</label>
                                  <a class=" btn-xs pop tooltips" href="#">  
                                    <span class="exc-blk"><i class="fa fa-info"></i></span>
                                    <span class="popupcls"><?=$resgate_completed?></span>
                                  </a>
                                  </td>
                                  <?php
                                }
                                if($rows->status=='Cancelled')
                                {
                                  ?>
                                  <label class="text-danger" style="font-weight: 500;">Não Realizado</label>
                                  <a class=" btn-xs pop tooltips" href="#">   
                                    <span class="exc-blk"><i class="fa fa-info"></i></span>
                                    <span class="popupcls"><?=$resgate_cancelled?></span>
                                  </a>
                                  </td>
                                  <?php
                                }
                                ?>
                            </tr>     
                            <?php
                            $k++;
                          }
                          ?>
                        </tbody>
                      </table>
                      <?php 
                    }
                    ?>
                    <input type="hidden" name="avail_bal" value="<?php echo $balcne;?>" id="avail_bal">
                  </div>
                </div>
              </div>
              <!--account table section ends-->
            </div>
            <br>
            <?php include('bottom_banners.php');?>
            <?php $this->load->view('front/my_earnings.php')?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>    
<!-- Main Content end -->

<!-- Withdraw modal popup section starts -->
<div class="modal fade wow bounceIn referfriend" id="withdraw" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header friend-popuphead">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i> </span></button>
        <center>
          <img src="<?php echo $this->front_model->get_img_url();?>front/new/images/pingou-logo1.png" alt="pingou" class="img-responsive">
        </center>
      </div>
      <div class="modal-body friend-popupbody">
          <div class="row fb-block">
            <center>
                <h3>Resgate seu dinheiro</h3>
            </center><br>
            <div class="center-block pad-no  accblk" style="background-color: #eee; width: 70%;border-radius: 10px;">
              <center><br>
                <h4>Solicitar Pagamento</h4><br>
                <h4>Disponível para saque: <strong>R$ <?php echo $newbal;?></strong></h4><br>
              </center>
              <br>
              <div class="alert alert-danger" id="requestset" style="display:none;" >
                <strong>
                  <span id="alertspan"></span>
                </strong>
              </div>
              <form role="form" action="javascript:;" method="post" id="login-form" autocomplete="off">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <label for="email" class="">Valor a ser sacado</label>
                  <input type="text" name="req_amount" id="req_amount" class="form-control req_amount" onkeypress="return isNumber(evt)" placeholder="">
                </div>
                <input type="hidden" id="acholder"   name="acholder" value="<?php echo $results->account_holder;?>">
                <input type="hidden" id="bankname"   name="bankname" value="<?php echo $results->bank_name;?>">
                <input type="hidden" id="branchname" name="branchname" value="<?php echo $results->branch_name;?>">
                <input type="hidden" id="acnumber"   name="account_number" value="<?php echo $results->account_number;?>">
                <input type="hidden" id="ifsccode"   name="ifsccode" value="<?php echo $results->ifsc_code;?>">
                <span id="testloader" style="display:none"> <center><img src="http://www.financemalta.org/design_images/loader.gif" width="120"></center></span>
                <center><input type="submit" id="btn-login_btk"  onClick="return validationset()" class="acc-commbtn hvr-pulse-shrink" value="Enviar"></center>
              </form>
            </div>
          </div>         
          <!-- <center>
            <button type="submit" class="btn btn-blu hvr-pulse-shrink">
              JOIN NOW FOR FREE
            </button>
          </center> -->          
      </div>    
    </div>
  </div>
</div>
<!-- Withdraw Popup End -->


<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>


<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/mask.js"></script>

<script type="text/javascript">
   
    $('.req_amount').mask('00000000000,00', {reverse: true});
   
</script>



<script type="text/javascript">
/*$("#req_amount").on("keypress keyup",function(){

  var len = $(this).val();
  len = len.replace(/^0+/, '');
  var len_1 = (len.length)+1;
  var ent_len = 3 - len_1;
  var out_val = '';
  if(len_1<3){
    for(var i=ent_len;i<3;i++){
      out_val = out_val + '1';
    }
    out_val = out_val + len;
    $(this).val(out_val);
  }
  else{
    len = len.replace(/^0+/, '');
    $(this).val(len);
  }

    //if($(this).val() == '0'){
    //  $(this).val('');  
    //}
});*/

function validationset()
{

  var acholder   = $('#acholder').val();
  var bankname   = $('#bankname').val();
  var branchname = $('#branchname').val();
  var acnumber   = $('#acnumber').val();
  var ifsccode   = $('#ifsccode').val();

  var req_amount = $('#req_amount').val();

  if(req_amount=='')
  {
    $('#req_amount').attr('style','border: 1px solid red;');
    $('#alertspan').html("Digite o valor que deseja sacar");
    $('#requestset').show();
    return false;
  }
 
  function isNumber(evt) 
  {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    if (key.length == 0) return;
    var regex = /^[0-9.,\b]+$/;
    if (!regex.test(key)) 
    {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
  }
  
  var avail_bal = $('#avail_bal').val();
  var minimum_balance = $('#minimum_balance').val();
  
  if(avail_bal =='' || avail_bal < 0)
  {
    $('#alertspan').html("Avaliable balance is needed");
    $('#requestset').show();
    return false;
  }

  if(parseFloat(req_amount)<parseFloat(minimum_balance))
  {
    $('#alertspan').html("O valor mínimo para solicitar um resgate é R$ "+minimum_balance);
    $('#requestset').show();
    return false;
  }

  if(parseFloat(avail_bal)<parseFloat(req_amount))
  {
    $('#alertspan').html("Você tem R$ "+avail_bal +" disponível para saque");
    $('#requestset').show();
    return false;
  }
  if(req_amount!='')
  {    
    if(parseFloat(req_amount) <= parseFloat(avail_bal))
    {
      if((acholder == '') || (bankname == '') || (branchname == '') || (acnumber == '') || (ifsccode == ''))
      {
        $('#req_amount').val('');
        $('#alertspan').html("Fill out your bank details before requesting withdraw");
        $('#requestset').show();
        return false;
      }
    }
  }
    $('#testloader').show();
    $('#btn-login_btk').hide();
    //alert(req_amount);
    
    $.ajax({
    type:'POST',
    url:'<?php echo base_url(); ?>cashback/add_withdraw',
    data:{'request':req_amount,'ifsccode':ifsccode},
    success:function(msg){
      $('#testloader').hide();
      $('#btn-login_btk').show();
      window.location.href = "<?php echo base_url(); ?>resgate/pending";
    }
  });
  
}
</script>
 