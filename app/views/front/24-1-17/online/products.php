<?php $this->load->view('front/header'); ?>
<!-- header content End -->

<link rel="stylesheet" type="text/css" href="<?php echo $this->front_model->get_css_js_url();?>front/css/category-slide.css" media="screen" />
 
<style>

.box1:hover a {
  color: #fff !important;
}

.nav-tabs.nav-stacked.nav-coupon-category > li ul{
    transition:max-height 0.3s ease-in-out 0s;    
    
}
ul.clsin_list{
    max-height:0;
    transition:max-height 0.3s ease-in-out 0s;    
}
.nav-tabs.nav-stacked.nav-coupon-category > li:hover > ul.clsin_list{
 max-height:999px;
}
.nav-tabs.nav-stacked.nav-coupon-category > li ul.clsin_list a{
     background:rgba(255, 255, 255, 1);
    border-left: medium none;
    border-radius: 0;
    border-right: medium none;
    border-top:1px solid #f5f5f5;
    color: #666;
    display: block;
    font-size: 13px;
    height: auto;
    margin: 0;
    padding: 7px 7px 7px 55px;
    position: relative;
    text-transform:none;   /* ALTEREI era text-transform:uppercase;*/
    transition: all 0.2s ease 0s;
    z-index: 1;
}

.nav-tabs.nav-stacked.nav-coupon-category{ /* 23-15 */ background:#ddd;}
/* 23-15 */ .nav-tabs.nav-stacked.nav-coupon-category  > li + li{margin-top:1px; overflow:hidden;}
.nav-tabs.nav-stacked.nav-coupon-category > li > a {/* 23-15 */border-bottom:1px solid #eee;}






.newimg-responsive.center-block {
    height: 50px !important;
    width: 93px !important;
}
.error
{
  color:#ff0000;
}
.required_field
{
 color:#ff0000;
}


li.waves-effect {
    background: #4daed9 none repeat scroll 0 0;
    border-bottom: 0 none;
    color: #fff;
   } 
   .icon
   {
    background: #4daed9 none repeat scroll 0 0;
    color: #fff;
    display: inline-block;
    height: 45px;
    margin-bottom: -2px;
    margin-right: 5px;
    padding-top: 0;
    position: relative;
    text-align: center;
    top: 8px;
    vertical-align: middle;
    width: 45px;
   }
</style>


<!-- Main Content start -->
<?php 
$user_id      = $this->session->userdata('user_id');
$userdetails  = $this->front_model->userdetails($user_id);
?>

<section class="cms wow fadeInDown">
  <div class="container">
    <div class="about clearfix mar-top-50">
      <div class="wow fadeInDown">
        
        <div class="col-md-3 col-sm-4 col-xs-12 wow fadeInLeft">
          <div class="left-prod-sidebar sidebar-left store-title">
            <aside class="sidebar-left store-title">
              <li class="categ-menu waves-effect"><span class="icon"><img src="<?php echo $this->front_model->get_img_url();?>front/new/images/categ1.png" alt="product"></span><span style="text-transform:uppercase;"><b>Categories</b></span></li>
              <ul class="nav nav-tabs nav-stacked nav-coupon-category nav-coupon-category-left">
                <?php 
                $categories = $this->front_model->get_all_categories();
                $kt = 1;
                foreach($categories as $view)
                {
                  $category_name = $view->category_name; 
                  $subcate =  $this->front_model->get_sub_categorys_list($view->category_id);
                  if($subcate)
                  {
                    $s = 'class="dropdown-toggle " data-toggle="dropdown"';
                  }
                  else
                  {
                    $s = '';
                  }
                  if($kt>10)
                  {
                    $extracss = 'style="display:none"';
                  }
                  else
                  { 
                    $extracss='';
                  }
                  ?>
                  <li class="dynamiccls" <?php echo $extracss." "; 
                    if($main_category==$category_name)
                    {
                      echo 'class="active"';
                    }
                    ?>> <a <?php echo $s;?> onClick="runcheck_1('<?php echo 'ofertas/'.$view->category_url;?>');" class="waves-effect active" href="<?php echo base_url().'ofertas/'.$view->category_url;?>"><i class="fa fa-arrow-circle-right"></i><?php echo $view->category_name;?></a>
                    <?php
                    if(count($subcate)!=0)
                    {
                      if($subcate)
                      {
                        echo '<ul class="list-unstyled clsin_list">';
                        $category_url = $view->category_url;
                        foreach($subcate as $subcatelist)
                        {
                          $sub_category_name = $subcatelist->sub_category_name;
                          $sub_category_url = $subcatelist->sub_category_url;
                          $sun_category_id = $subcatelist->sun_category_id;
                          ?>
                          <li <?php echo $kt;?>><?php echo anchor('ofertas/'.$category_url.'/'.$sub_category_url,$sub_category_name); ?></li>
                          <?php
                        }
                        echo " </ul>";
                      }
                    }                   
                    ?>
                  </li>
                  <?php 
                  $kt++;
                }
                ?>
              </ul>
            </aside>
          </div>
          <button type="button" style="width:90%;" class="btn mar-top-20 btn-blu hvr-pulse-shrink" id="load_m" onClick="show_cate();">LOAD MORE</button>
        </div>


        <div class="col-md-9 col-sm-8 col-xs-12 wow fadeInRight">
          <!-- New code for banner details 31-10-16  -->
          <?php 
          //include('top_banners.php');
          ?>
          <!-- End 31-10-16 -->
          <h2 class="mar-bot-50"><?php echo $category_details->category_name;?> Coupon Codes & Cashback Offers</h2>
          <div class="row">
            <?php
            if($stores_list)
            {
              $k=1;
              foreach($stores_list as $stores)
              {
                $affiliate_id = $stores->affiliate_id;
                $affiliate_name = $stores->affiliate_name;
                $featured = $stores->featured;
                $count_coupons = $this->front_model->count_coupons($affiliate_name);
                $get_coupons_sets = $this->front_model->get_coupons_sets($affiliate_name,2);
                
                if($featured!=0)
                {
                  $setup     =  "feature";
                  $namess    = "Feature"; //heading
                  $colors_li = 'style="background-color:#32c2cd;"';
                }
                else
                {
                  $store_of_week = $stores->store_of_week;
                  if($store_of_week!=0)
                  {
                    $setup     = "store";
                    $namess    = "Store of the Week "; //heading
                    $colors_li = 'style="background-color:#a5d16c;"';
                  }
                  else
                  {
                    $setup     = "offers";
                    $namess    = "Offers "; //heading
                    $colors_li = 'style="background-color:#e74955;"';
                  }
                }
                ?>
                
                <div class="col-md-6 col-sm-6 col-xs-12 wow slideInLeft mar-bot-30">
                  <div class="box1 prodblk1">
                    <div class="clearfix">
                      <div class="col-md-3 col-sm-5 col-xs-4 pd-right-0 pd-left-0">
                        <div class="prod-left">
                          <a href="<?php echo base_url();?>cupom-desconto/<?php echo $stores->affiliate_url;?>">
                            <img src="<?php echo $this->front_model->get_img_url();?>uploads/affiliates/<?php echo $stores->affiliate_logo;?>" alt="product" class="newimg-responsive center-block">
                          </a>
                        </div>
                      </div>
                      <div class="col-md-9 col-sm-7 col-xs-8">
                        <a href="<?php echo base_url();?>cupom-desconto/<?php echo $stores->affiliate_url;?>">
                        <h3> 
                        <!-- <a href="<?php echo base_url();?>cupom-desconto/<?php echo $stores->affiliate_url;?>" style="color:#333;"> </a>-->
                        <?php echo $stores->affiliate_name;?>
                         -
                        <?php 
                        if($stores->cashback_percentage)
                        { 
                          if($stores->affiliate_cashback_type=="Percentage")
                          {
                            $cppercentage = $stores->cashback_percentage."%";
                          }
                          else
                          {
                            $cppercentage = "Rs. ".$stores->cashback_percentage;
                          }
                          echo "Up to ".$cppercentage;?>
                                      <?php 
                        }
                        ?>
                        Cashback & <?php echo $count_coupons->counting;?> Coupons
                        </h3></a> 
                        <button onClick="return toggle_st(<?=$k?>);" id="toggle<?=$k?>" class="btn btn-blu" type="button">Click to Expand</button>
                        <div class="toggle<?=$k?>" id="toggle<?=$k?>" style="display:none;">
                          <h4> Top Offers 
                            <small>
                              <a class = 'clr-blu' href="<?php echo base_url();?>cupom-desconto/<?php echo $stores->affiliate_url;?>">See All Offers</a>
                            </small>
                          </h4>
                          <hr>
                          <?php
                          if($get_coupons_sets)
                          {
                            foreach($get_coupons_sets as $get_coupons)
                            {
                              ?>
                              <p> <strong>></strong> <?php echo $get_coupons->title; 
                              if($stores->cashback_percentage)
                              {
                                $admindetails = $this->front_model->getadmindetails_main(); 
                                echo " + Get additional <strong>".$stores->cashback_percentage."%</strong> Cashback from ".$admindetails->site_name;
                              }
                              ?>
                              </p>
                              <?php
                            }
                          }
                          ?>
                        </div>
                      </div>
                    </div>
                     <div class="clearfix">
                    <h5 class="str-tit">Store of the week</h5>
                    </div>
                  </div>
                </div>
                
                <?php
                $k++;
              }
            }
            else
            {
              ?>
              <div class="alert alert-danger bs-alert-old-docs">
                <center>
                  <strong>No Coupons found on this category!</strong>
                </center>
              </div>
              <?php
            }
            ?>
          </div>
          <!-- <p>Ofteras Descripitions</p>
          <?php echo $category_details->ofertas_description;?> -->
          
          <!-- New code For banner Details 31-10-16 -->
          <?php
        //include('bottom_banners.php');
          ?>
          <!-- End 31-10-16 -->
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Main Content end -->

<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->

<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>
  

 
<script>
$(document).ready(function() {
$('.owl-carousel').owlCarousel({
loop: true,
margin: 1,
responsiveClass: true,
responsive: {
0: {
items: 1,
nav: true
},
600: {
items: 2,
nav: true
},
1150: {
items: 4,
nav: true,
loop: false,
margin:0
}
}
})
})

</script> 
<script type="text/javascript">
$(function () { $("[data-toggle='tooltip']").tooltip(); });

</script> 
<script type="application/javascript">

function toggle_st(num)
{
  $('.toggle'+num).toggle('slow');
  return false; 
}
//id="load_m" onClick="show_cate();
function show_cate()
{
  $('.dynamiccls').show();
  $('#load_m').hide();
}
</script> 
<script type="text/javascript">
function runcheck_1(url)
{
  window.location.href="<?php echo base_url();?>"+url;
}
</script>