		<!-- header -->
  		<?php $this->load->view('front/header');?>
		<!-- Header ends here -->
		<section class="cms wow fadeInDown">
  			<div class="container">
				<div class="heading wow bounceIn">
					<h2><span>Login</h2>
					<div class="heading_border_cms"><span ><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/top_drop.png" style="margin-top:-5px !important;"></span> </div>
			    </div>
			    <div class="col-md-6 col-sm-12 col-xs-12 fn center-block">
		    		<div class="wow fadeInDown">
						<div class="panel-body">
							<?php
							//begin form
							$attribute = array('role'=>'form','name'=>'login_form','id'=>'login_form', 'onSubmit'=>'return setupajax_login();', 'autocomplete'=>'off','method'=>'post');
        					echo form_open('chk-invalid',$attribute);
							?>
							<?php if(isset($invalid_login)) 
							{
								echo '<div class="alert alert-danger">
								<button data-dismiss="alert" class="close">x</button>
								<strong>'.$invalid_login.' </strong></div>';			
							}
							$success = $this->session->flashdata('success');
							if($success!="")
							{
								echo '<div class="alert alert-success">
								<button data-dismiss="alert" class="close">x</button>
								<strong>'.$success.'</strong></div>';
							}
							?>
							<center><span id="newdis" style="color:red; font-weight:bold;"> </span></center>		
							<div class="form-group mb-lg">
								<label>Email <span class="required_field">*</span></label>
								<input type="email" id="emails" required class="form-control input-lg" name="emails" autocomplete="off"  >
				            </div>
				                          
				            <div class="form-group mb-lg">
								<label>Password <span class="required_field">*</span></label>
				                <a class="pull-right clr-blu" href="<?php echo base_url()?>/forgetpassword">Lost Password?</a>
								<input type="password" required class="form-control input-lg" id="passwrd" name="passwrd" autocomplete="off" >
				            </div>
							
							<div class="row">
								<div class="col-sm-4">
									<input type="submit"  class="btn btn-danger hidden-xs pop" name="signin" id="signin" value="Sign In">
								</div>
							</div>

							<p class="text-center">Don't have an account yet? 
							 <a href="#register" data-toggle="modal">Sign In</a>
						</div>
					</div>	
				</div>	
			</div>
		</section>	
		<!-- footer -->
		<?php $this->load->view('front/site_intro');?>
		<?php $this->load->view('front/sub_footer');?>	   
	</body>
</html>