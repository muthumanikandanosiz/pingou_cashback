<?php $this->load->view('front/header'); ?>
<!-- header content End -->
<style type="text/css">
  .table-responsive {
    overflow-x: unset;
}
</style>

<!-- Main Content start -->
<?php $user_id = $this->session->userdata('user_id');?>
<section class="cms wow fadeInDown">
  <div class="container">
    <div class="heading wow bounceIn">
      <h2>
        Indique e <span>Ganhe</span>
      </h2>
      <div class="heading_border_cms">
        <span>
          <img src="<?php echo $this->front_model->get_img_url();?>front/new/images/top_drop.png">
        </span>
      </div>
    </div>
    <div class="myac">
      <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1" id="hide_fn">
        <div class="my_account my_accblk">
          <div class="myacc-maintab">
            <!-- Nav tabs -->
            <?php $this->load->view('front/user_menu'); ?>
            <!-- <ul class="data-drop wow fadeInDown list-inline list-unstyled cls-greybgmenublk" id="dataSubMain" role="tablist">
              <li class="active"> <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Dados do Perfil</a> </li>
              <li> <a href="#payment" aria-controls="payment" role="tab" data-toggle="tab">Dados de Pagamento</a> </li>
              <li> <a href="#password" aria-controls="password" role="tab" data-toggle="tab">Mudar Senha</a> </li>
              <li> <a href="#notify" aria-controls="notify" role="tab" data-toggle="tab">Notificação</a> </li>
            </ul> -->
          </div>
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" id="acc4">
              <div class="clearfix wow fadeInDown comm-cont">
                <p>
                  <?php  
                  $category = $this->front_model->referal__category(); 
                  $ref_category_status = $this->db->query("select * from referral_settings where ref_id='$category'")->row('ref_by_percentage');
                   
                  if($ref_category_status == 1)
                  {
                    ?>
                    <p> Indique amigos e ganhe <?php echo $this->front_model->referal__category_percentage($category); ?>% de todo o dinheiro de volta que eles resgatarem!</p>
                    <?php 
                  }
                  $k = 5000;
                  $earn_price = ($k*$this->front_model->referal_percentage())/100;
                  if($category == 1)
                  {
                    echo '<p>'.$this->front_model->referal__category_description($category).'</p>';
                    ?> 
                    <p>Não entendeu? Por exemplo, se seu amigo ganhou <span class="indianRs">R$</span> 10 de volta de uma compra realizada por ele, você recebe <span class="indianRs">R$</span> 1 como bônus na sua conta. <br><br> Imagine só quanto você pode ganhar indicando <?php echo $this->front_model->referal__category_percentage(); ?> amigos! Abaixo está seu link de referência. Você vai ganhar por todos que se cadastrarem clicando nele.</p>
                    <?php
                  }
                  else
                  {
                    echo '<p>'.$this->front_model->referal__category_description($category).'</p>';
                  }
                  ?>
                </p>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInDown email-searchblk">
                <label class="col-md-4 col-sm-12 col-xs-12 email-label">Envie este link para seus amigos:</label>
                <div class="col-md-5 col-sm-8 col-xs-12">
                  <div class="input-group">
                    <?php $result1 = $this->front_model->refer_friends();
                    if($result1)
                    {
                      $mi = 1;
                      foreach($result1 as $views)
                      {
                      ?>
                        <input type="text" id='copyTarget<?php echo $mi;?>' class="form-control email-tbox" value="<?php echo base_url().'?ref='.$views->random_code; ?>">
                        <input type="hidden" id="random_code" value="<?php echo $views->random_code; ?>">
                        <?php
                      } 
                    }
                    ?>
                    <span class="input-group-btn">
                      <button type="button"  onclick='copy(<?php echo $mi; ?>);' data-id='<?php echo $mi;?>' id='copyButton<?php echo $mi;?>' class="btn btn-blu">Copy Link</button>
                    </span>  
                  </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                  <label class="col-md-12 col-sm-12 col-xs-12 email-label" style="text-align:right;">Amigos indicados: <?php echo count($this->front_model->referral_network());?></label>
                </div>
              </div>
              <br>
              
              <?php
               include('oauth.php'); 
               include('outlook.php'); 
               $redirectUri = base_url().'authorize.php';
              ?>
              <!-- <center><p style='text-align:center; margin-right: 10%;' id='msg<?php echo $mi;?>'></p></center> -->
              <div class="row wow fadeInDown">
                <div class="comm-cont wow fadeInLeft col-md-6 col-sm-6 col-xs-12">
                  <h2 style="margin-bottom: 20px;">get started by searching your email 
                  contacts</h2>
                  <ul class="soc-iconblk list-unstyled">
                    <li><a href="<?php echo base_url().'HAuth/invite/Google'; ?>" target="_black"><img src="<?php echo $this->front_model->get_img_url();?>front/new/images/socialnet1.png" alt="network"> Gmail </a></li>
                    <li><a href="<?php echo base_url().'HAuth/invite/Yahoo'; ?>"  target="_black"><img src="<?php echo $this->front_model->get_img_url();?>front/new/images/socialnet2.png" alt="network"> Yahoo Mail </a></li>
                    <li><a href="<?php echo oAuthService::getLoginUrl($redirectUri)?>"  target="_black" style="text-decoration: none;"><img src="<?php echo $this->front_model->get_img_url();?>front/new/images/socialnet3.png" alt="network"> Hot Mail </a></li>
                    <!--<li><a href="#"><img src="<?php echo base_url();?>front/new/images/socialnet4.png" alt="network"> Outlook.com </a></li>
                    <li><a href="#"><img src="<?php echo base_url();?>front/new/images/socialnet5.png" alt="network"> Windows live mail </a></li>
                    <li><a href="#"><img src="<?php echo base_url();?>front/new/images/socialnet6.png" alt="network"> MSN </a></li> -->
                  </ul>
                  <br>
                  <center>
                    <h4>OR</h4>
                  </center>
                  <div class="row emailbtnblk">
                    <div class="col-md-12 col-sm-8 col-xs-12">
                      <div class="input-group">
                        <input type="text" placeholder="Endereços de email, separados por vírgula" id="invite_mail" onClick="clears(1);" class="form-control" style="border-radius: 0px;">
                        <span class="input-group-btn">
                          <button type="button"  onClick="send_mail();" class="btn btn-blu">Enviar Convites</button>
                          
                        </span>
                      </div>
                      <span id="success" style="color:green;"></span>
                    </div>
                  </div>              
                </div>
                <div class="comm-cont wow fadeInRight col-md-6 col-sm-6 col-xs-12">
                  <h2 style="margin-bottom: 20px;">get started by searching your social
                  networks </h2>
                  <div class="row wow bounceInUp acc-login">
                    <center>
                      <a href="http://www.facebook.com/sharer.php?u=<?php echo base_url().'?ref='.$views->random_code; ?>" class="facebook" data-toggle="modal" target="_blank"> <i class="fa fa-facebook"></i> Facebook </a>
                    </center>
                  </div>
                  <div class="row wow bounceInUp acc-login">
                    <div class="col-sm-6 col-xs-12"> <a href="<?php echo base_url().'HAuth/invite/Google'; ?>" target="_black" class="google"> <i class="fa fa-google-plus"></i> Google + </a>
                    </div>
                    <div class="col-sm-6 col-xs-12"> <a href="http://twitter.com/home?status=<?php echo base_url().'?ref='.$views->random_code; ?>" class="twitter" target="_blank"> <i class="fa fa-twitter"></i> Twitter </a> 
                    </div>
                  </div>
                </div>
              </div>
              <h2 style="margin-bottom: 25px;">Amigos Indicados</h2>
              Essa é a relação de todos os amigos que se cadastraram pelo seu link
              <br><br>
              <!--account table section starts-->
              <div class="row wow fadeInDown">
                <div class="col-md-12 col-sm-12">
                  <div class="a">
                    <?php 
                    if(empty($result))
                    {
                      echo "<br><center>Por enquanto, nenhum amigo seu realizou cadastro utilizando o seu link.<br>
                      Comece a divulgar agora mesmo!.</center><br>";
                    } 
                    else
                    {
                      ?>
                      <table cl id="example" class="display zui-table zui-table-rounded table acc-table1">
                        <thead>
                          <tr>
                            <th> Date </th>
                            <th> Email </th>
                            <th> Status da Referencia </th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if($result)
                          {
                            foreach($result as $rows)
                            {
                              ?>
                              <tr>
                                <td><?php $data = $rows->date_added; echo date('d/m/y',strtotime($data)); ?></td>
                                <td><?php echo $rows->referral_email; ?></td>
                                <?php
                                if($rows->status == 'Ativa')
                                {
                                  ?>
                                  <td><?php echo 'Ativa'; ?></td>
                                  <?php
                                }
                                if($rows->status == 'Inativa')
                                {
                                  ?>
                                  <td><?php echo 'Inativa'; ?></td>
                                  <?php
                                }
                                ?>
                              </tr>     
                              <?php
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                      <?php 
                    }
                    ?>  
                  </div>
                </div>
              </div>
            </div>
          <br>
            <?php $this->load->view('front/my_earnings.php')?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>    
<!-- Main Content end -->

<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>

<script type="text/javascript">

//Start Email subscribe
function email_sub()
{
  var email = $("#email").val();
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,3})?$/;
  if(!email || !emailReg.test(email))
    $('#email').css('border', '2px solid red');
  else
  {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>cashback/email_subscribe/",
      data: {'email':email},
      success: function(msg)
      {
        if(msg==1)
        {
          $('#msg').text('Activated Successfully');
          $('#email').css('border', '');
        }
        else
        {
          $('#msg').text('Already Activated');
          $('#email').css('border', '');
        } 
      }
    });
  } 
}

function clears(val)
{
  if(val==1)
    $('#invite_mail').css('border', '');
  else
    $('#news_email').css('border', '');
} 
//End Email subscribe
//Friends Invite Mail
function send_mail()
{
  var mail = $('#invite_mail').val();
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,3})?$/;
  var myList = mail.replace(/\s+/, "");
  var emails = mail.split(',');
  var random = $('#random_code').val();
  var text = $('#mail_text').val();
  
  if(mail=='')
    $('#invite_mail').css("border","2px solid red");
  else if(mail!='')
  {
    var err=0;
    for (var i = 0; i < emails.length; i++) {
      value = emails[i];
      if(!emailReg.test(value)){
        $('#invite_mail').css("border","2px solid red");
        err=1;
        }
    }
    if(err == 0)
    {
      $.ajax({
      type: "post",
      url: "<?php echo base_url(); ?>cashback/invite_mail/",
      data: {'email':mail,'random':random,'mail_text':text},
      success: function()
      {
        $('#invite_mail').val('');
        $('#success').text('Convites enviados com sucesso!');
      }
    
    });
    }
  }
}
function MyPopUpWin(url, width, height) 
{
    var leftPosition, topPosition;
    //Allow for borders.
    leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
    //Allow for title and status bars.
    topPosition = (window.screen.height / 2) - ((height / 2) + 50);
    //Open the window.
    window.open(url, "Window2",
    "status=no,height=" + height + ",width=" + width + ",resizable=yes,left="
    + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY="
    + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no");
}
   
</script>
<!-- fim copiar -->
<script type="text/javascript">
function copy(id)
{ 
  $('#copyTarget'+id).select();
  document.execCommand('copy');
  setTimeout(function(){
  $('#msg'+id).text('Text has coppied');  
  }, 1000); 
}

</script>
 
