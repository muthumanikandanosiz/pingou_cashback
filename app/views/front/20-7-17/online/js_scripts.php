<?php
$getadmindetails    = $this->front_model->getadmindetails();  
$unlog_menu_status  = $getadmindetails[0]->unlog_menu_status;
$log_menu_status    = $getadmindetails[0]->log_menu_status;
$log_content        = $getadmindetails[0]->log_content;
$unlog_content      = $getadmindetails[0]->unlog_content;
$log_status         = $getadmindetails[0]->log_status;
$unlog_status       = $getadmindetails[0]->unlog_status;
?>

<!--Bootstrap core JavaScript -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/jquery.1.11.1.min.js"></script>-->

<!-- Data table related scripts Start -->
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/bootstrap.min.js"></script>
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/owl-carousel.js"></script>
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/jquery.nicescroll.min.js"></script>
<!-- Scripts queries -->

<?php $table_pages = $this->uri->segment(1); ?>

<?php 
if($table_pages == 'resgate')
{
  ?>
  <script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.minnew.js"></script>
  <script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.bootstrap.min.js"></script> 
  <script type="text/javascript">
    $(document).ready(function() {
      $('#sample_teste1').DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [{
      "targets": 1,
      "orderable": false,
      "Length": 10,
      }],
      "ajax": {
      "url": "<?php echo site_url('cashback/newresgate')?>",
      "data": {
      //"totalrecords": "<?php echo $iTotal; ?>"
      "affiliate_name": "<?php echo $affiliate_name; ?>"
      }
      }
      });
    });
  </script>
  <?php
} 
if($table_pages == 'saidas-para-loja')
{
  ?>
  <script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.minnew.js"></script>
  <script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.bootstrap.min.js"></script> 
  <script type="text/javascript">
    $(document).ready(function() {
      $('#sample_teste1').DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [{
      "targets": 1,
      "orderable": false,
      "Length": 10,
      }],
      "ajax": {
      "url": "<?php echo site_url('cashback/newclickhistory')?>",
      "data": {
      //"totalrecords": "<?php echo $iTotal; ?>"
      "affiliate_name": "<?php echo $affiliate_name; ?>"
      }
      }
      });
    });
  </script>
  <?php 
}  
if($table_pages == 'indique-e-ganhe')
{
  ?>
  <script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.minnew.js"></script>
  <script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.bootstrap.min.js"></script> 
  <script type="text/javascript">
    $(document).ready(function() {
      $('#sample_teste1').DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [{
      "targets": 1,
      "orderable": false,
      "Length": 10,
      }],
      "ajax": {
      "url": "<?php echo site_url('cashback/newindique_e_ganhe')?>",
      "data": {
      //"totalrecords": "<?php echo $iTotal; ?>"
      "affiliate_name": "<?php echo $affiliate_name; ?>"
      }
      }
      });
    });
  </script>
  <?php 
}
if($table_pages == 'extrato')
{
  ?>
  <script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.minnew.js"></script>
  <script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.bootstrap.min.js"></script> 
  <script type="text/javascript">
    $(document).ready(function() {
    $('#sample_teste1').DataTable({ 
    "processing": true,
    "serverSide": true,
    "columnDefs": [{
    "targets": 1,
    "orderable": false,
    "Length": 10,
    }],
    "ajax": {
    "url": "<?php echo site_url('cashback/newextrato')?>",
    "data": {
    //"totalrecords": "<?php echo $iTotal; ?>"
    "affiliate_name": "<?php echo $affiliate_name; ?>"
    }
    }
    });
    });
  </script>
  <?php 
} 
if($table_pages == 'loja-nao-avisou-compra')
{
  ?>
  <script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.minnew.js"></script>
  <script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.bootstrap.min.js"></script> 
  <script type="text/javascript">
    $(document).ready(function() {
      $('#sample_teste1').DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [{
      "targets": 1,
      "orderable": false,
      "Length": 10,
      }],
      "ajax": {
      "url": "<?php echo site_url('cashback/newloja_nao_avisou_compra')?>",
      "data": {
      //"totalrecords": "<?php echo $iTotal; ?>"
      "affiliate_name": "<?php echo $affiliate_name; ?>"
      }
      }
      });
    });
  </script>
  <?php 
}
if($table_pages == 'loja-cancelou-minha-compra')
{
  ?>
  <script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.minnew.js"></script>
  <script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.bootstrap.min.js"></script> 
  <script type="text/javascript">
    $(document).ready(function() {
      $('#sample_teste1').DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [{
      "targets": 1,
      "orderable": false,
      "Length": 10,
      }],
      "ajax": {
      "url": "<?php echo site_url('cashback/newloja_cancelou_minha_compra')?>",
      "data": {
      //"totalrecords": "<?php echo $iTotal; ?>"
      "affiliate_name": "<?php echo $affiliate_name; ?>"
      }
      }
      });
    });
  </script>
  <?php 
}
?>  


<?php 
$user_id       = $this->session->userdata('user_id');
if($user_id == '')
{
?>
<script type="text/javascript">
  /*$(document).ready(function() 
  {
    //localStorage.clear();
    //localStorage.removeItem(key);
    console.log(localStorage.getItem('popState'));
    if(localStorage.getItem('popState') !='shown')
    {
      $('body').mouseleave(function()
      {  
        if($('#myModal').attr('counts') == 0)
        {
          if($(window).width() > 850)
          {
            //localStorage.setItem('popState','shown');
            $('#myModal').modal('show');
            $('.cus_modal').css("background","#4daed9");
            
          }  
        }
      });
    }  
    $('.cls_popup').click(function() // You are clicking the close button
    {
      $('#myModal').attr('counts',1); // Now the pop up is hiden.
      localStorage.setItem('popState','shown');
      //localStorage.clear();
    });
});*/
</script>

<script type="text/javascript">
 /*New code for exit popup details 17-11-16*/
  /*$(document).ready(function()
  {

    $('body').mouseleave(function(){
      if($('#myModal').attr('counts') == 0)
      {
        if($(window).width() > 850)
        {
          $('#myModal').modal('show');
          $('.cus_modal').css("background","#4daed9");
          //$("#unique_name_error").css('color','#ff0000');
        }  
      }

    });

    $('.cls_popup').click(function(){
      $('#myModal').attr('counts',1);
    }); 
  })*/
  /*End 17-11-16*/
</script>
<?php 
}
else
{?>
    <script type="text/javascript">
    $(document).ready(function()
    {
      $('.first_popup').click(function(){
          
        $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>cashback/update_firstpoup_satus",
        data: {'poup_status':0},
        cache: false,
        success: function(result)
        {
          if(result!=1)
          {
            return false;
          }
          else
          {
            <?php $redirect_urlset =  base_url(uri_string());?>
            window.location.href = '<?php echo $redirect_urlset; ?>';
            return false;          
          }             
        }
      });
      return false;
      });
    });  
    </script>
    <?php 
}
?>


<script>
  $(document).ready(function()
  {
    
    $('body').trigger("click");
    //$('#exit_popup').modal('hide');

    $("#RefFrndScroll").niceScroll(); 

    $('.owl-carousel').owlCarousel({
    loop: true,
    margin: 1,
    responsiveClass: true,
    responsive: {
    0: {
    items: 1,
    nav: true
    },
    600: {
    items: 2,
    nav: true
    },
    1150: {
    items: 4,
    nav: true,
    loop: false,
    margin:0
    }
    }
    })

    $("#forget_password").validate({
              rules: {
          email: {
                       required: true,
              email :true
                  }     
              },
              messages: {
          email: {
                     required: "Please enter  your valid Emailid."                  
                  }
        } 
          });

      var hiddenuserid = $('#hidden_user_id').val();
      if(hiddenuserid=='')
      {
        $('#myModal2').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
      })
      }
  })
</script> 

<!-- new code 21-9-16 -->
<script type="text/javascript">
  function isNumber(evt) 
  {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }
</script>
<!-- end -->
<script type='text/javascript'>
/*New hide code 1-7-17*/
/*$(window).load(function()
{
  $(function () { 
      $('#startdatepicker, #enddatepicker').datepicker({
          beforeShow: customRange,
          dateFormat: "dd-mm-yy",
          firstDay: 1,
          changeFirstDay: false
      });
  });

  function customRange(input) 
  {
    var min = null, // Set this to your absolute minimum date
    dateMin = min,
    dateMax = null,
    dayRange = 30; // Restrict the number of days for the date range
     
    if($('#select1').val() === '2')
    {
      if (input.id === "startdatepicker")
      { 
        if($("#enddatepicker").datepicker("getDate") != null)
        {
          dateMax = $("#enddatepicker").datepicker("getDate");
          dateMin = $("#enddatepicker").datepicker("getDate");
          dateMin.setDate(dateMin.getDate() - dayRange);
          if (dateMin < min) { dateMin = min; }
        }
        else
        {
        }
      }
      else if(input.id === "enddatepicker")
      {
        dateMin = $("#startdatepicker").datepicker('getDate');
        dateMax = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() + 30);
        if ($('#startdatepicker').datepicker('getDate') != null) 
        {
          var rangeMax = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() + dayRange);
          if (rangeMax < dateMax) { dateMax = rangeMax; }
        }
      }
    }
    else if($('#select1').val() != '2')
    {
      if (input.id === "startdatepicker")
      {
        if($('#enddatepicker').datepicker('getDate') != null) 
        {
          dateMin = null;
        }
        else
        {
        }
      }
      else if (input.id === "enddatepicker") 
      {
        dateMin = $('#startdatepicker').datepicker('getDate');
        dateMax = null;
        if ($('#startdatepicker').datepicker('getDate') != null) { dateMax = null; }
      }
    }
    return 
    {
      minDate: dateMin,
      maxDate: dateMax
    }
  }

  $('.datepicker').datepicker('widget').delegate('.ui-datepicker-close', 'mouseup', function()
  {
    var inputToBeCleared = $('.datepicker').filter(function()
    { 
      return $(this).data('pickerVisible') == true;
    });    
    $(inputToBeCleared).val('');
  });
});*/ 
/*End 1-7-17*/
</script>


<script type="text/javascript">
  $(function () { $("[data-toggle='tooltip']").tooltip(); });
</script>


<script src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>      
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/dataTables.bootstrap.js"></script>

<script type="text/javascript" charset="utf-8">
  $(document).ready(function()
  {
    $('#example').dataTable();
  });
</script>

<script type="text/javascript">

  var table = $("#example").dataTable({
  language : 
  {
    sLengthMenu: "_MENU_",
    searchPlaceholder: "Buscar por loja ou valor por loja"
  },
  order: [[ 0, "desc" ]],
   
  }); 
</script>
 
<script>
  $(function()
  {
    var x = 0;
    setInterval(function()
    {
      x-=1;
      $('.moving').css('background-position', x + 'px 0');
    }, 50);
  })
</script>

<script>
  $(function()
  {
    $('a[href*="#"]:not([href="#"])').click(function()
    {
      if($( this ).hasClass( "check" ) )
      {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if(target.length)
        {
          $('html, body').animate({
          scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });
  });
</script>


<!--LOGIN AND REGISTER POPUP PAGE VALIDATION START--> 

<script type="text/javascript">
  function setupajax_login()
  {   
    
    //var datas = $('#login_form').serialize();
    var emails     = $('#emails').val();
    var passwords  = $('#passwrd').val();
    var signins    = $('#signin').val(); 

    /*new code for session values in cashback_exclusive concept 18-7-16*/
    var new_cash_ex_id   = $('#new_cash_ex_id').val();
    var cashback_details = $('#cashback_details').val();
    var expiry_dates     = $('#expiry_dates').val();
    var cashback_web     = $('#cashback_web').val();
    
    /*end*/
      
      $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>cashback/logincheck",
      data: {'email':emails,'password':passwords,'signin':signins,'cashback_id':new_cash_ex_id,'cashback_details':cashback_details,'expirydate':expiry_dates,'cashbackweb':cashback_web},
      cache: false,
      success: function(result)
      {
        if(result!=1)
        {
          $('#newdis').html(result);
          return false;
        }
        else
        {
          <?php $redirect_urlset =  base_url(uri_string());?>
          window.location.href = '<?php echo $redirect_urlset; ?>';
          return false;          
        }             
      }
    });
    return false;
  }

  //New code for New Login page popup 27-4-16//
  function ssetupajax_login()
  { 
     
    //var datas = $('#login_form').serialize();
    var emails     = $('#semails').val();
    var passwords  = $('#spasswrd').val();
    var signins    = $('#ssignin').val(); 
    //alert(emails); die;
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>cashback/logincheck",
      data: {'email':emails,'password':passwords,'signin':signins},
      cache: false,
      success: function(result)
      {

        if(result!=1)
        {
          $('#snewdis').html(result);
          return false;
        }
        else
        {
          <?php $redirect_urlset =  base_url(uri_string());?>
          window.location.href = '<?php echo $redirect_urlset; ?>';
          return false;
        }             
      }
    });
    return false;
  }
  //End//
</script>  


<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/jquery.validate.min.js"></script> 
<script type="text/javascript">
  /* form validation*/

         $("#regform").validate({

            rules: {

        user_email: {

                    required: true,

          email :true

                },

        user_pwd: {

                    required: true,

          minlength: 6

                },    

            },

            messages: {

        
        user_email: {

                      required: "Please enter your valid Emailid."
                    },

        user_pwd: {

                    required: "Please enter the password.",

          minlength: "Passwords must be minimum 6 characters."    

                },

        pwd_confirm: {

                    required: "Please confirm your password.",

          minlength: "Passwords must be minimum 6 characters.",

                }
            },
            submitHandler:function(form) 
            {
              form.submit();
            }
        });

    $("#rregform").validate({

    rules: {

       
    user_email: {

  required: true,

    email :true

    },

    user_pwd: {

                    required: true,

          minlength: 6

                },    

            },

            messages: {

        
        user_email: {

                      required: "Please enter  your valid Emailid."
                    },

        user_pwd: {

                    required: "Please enter the password.",

          minlength: "Passwords must be minimum 6 characters."    

                },

        pwd_confirm: {

                    required: "Please confirm your password.",

          minlength: "Passwords must be minimum 6 characters.",

          //equalTo : "Please enter the same password."

                    

                }
  
            },
            submitHandler:function(form) 
            {
              form.submit();
            }
         
        });
</script>

<script type="text/javascript">
  //check email for  registration
  function check_email()
  {
    var email = $('#user_email').val();

    //alert(email);
    if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
    {
        $.ajax({

          type: 'POST',

          url: '<?php echo base_url();?>cashback/check_email',

          data:{'email':email},

           success:function(result){

            if(result.trim()==1)

            {

              $("#unique_name_error").css('color','#29BAB0');
              
               $("#unique_name_error").html('available.');

               $("input[name='register']").attr("disabled", false); 
            }

            else

            {

              $("#unique_name_error").css('color','#ff0000');
              $("#unique_name_error").css('font-size','14px');
              $("#unique_name_error").css('font-weight','bold');

              $("#unique_name_error").html('This email is already exists.'); 

              $("input[name='register']").attr("disabled", true); 

            }

          }

        });
    }
    return false;
  }

  function rcheck_email()
  {

    var email = $('#ruser_email').val();
      //alert(email);
    if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
    {
        $.ajax({

          type: 'POST',

          url: '<?php echo base_url();?>cashback/check_email',

          data:{'email':email},

           success:function(result){

            if(result.trim()==1)

            {

              $("#runique_name_error").css('color','#29BAB0');
              

               $("#runique_name_error").html('available.');

               $("input[name='register']").attr("disabled", false);

            }

            else

            {

              $("#runique_name_error").css('color','#ff0000');
              $("#runique_name_error").css('font-weight','bold');

              $("#runique_name_error").html('This email is already exists.'); 

              $("input[name='register']").attr("disabled", true);  

            }

          }

        });
    }
      return false;
  }

  function valid_email()
  {
    var emailid = $('#emailid').val();

    if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailid))
    {
        $.ajax({

          type: 'POST',

          url: '<?php echo base_url();?>cashback/check_vaild_email',

          data:{'email':emailid},

           success:function(result){

            if(result.trim()==1)

            {

              $("#unique_email_error").css('color','#29BAB0');
              

               $("#unique_email_error").html('available.');

            }

            else

            {

              $("#unique_email_error").css('color','#ff0000');

              $("#unique_email_error").html('This email is already exists.');  

            }

          }

        });
    }
    return false;
  }
</script>

<!--LOGIN AND REGISTER POPUP PAGE VALIDATIOPN END-->


<!-- All stores page (desconto) modal popp files starts -->
    <script src="<?php echo $this->front_model->get_css_js_url();?>front/js/classie.js"></script>
    <script src="<?php echo $this->front_model->get_css_js_url();?>front/js/modalEffects.js"></script>
    <script>
      // this is important for IEs
      var polyfilter_scriptpath = '/js/';
    </script>
    <!--<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/css-filters-polyfill.js"></script>-->
<!-- modal popp files ends (Desconto page)-->

<!-- LOGIN,REGISTER,FORGOT PASSWORD POPUP ONCLICK FUNCTIONALITES -->
<script type="text/javascript"> 
  $(document).on('click','#signupclk',function()
  { 
    $('#login').modal('hide');
    $("#login").on('hide', function () {
    $('#register').modal('show');
    });
  });

  $(document).on('click','#signinclk',function()
  {
    $('#register').modal('hide');
    $("#register").on('hide', function () {
    $('#login').modal('show'); 
    });
  });

  $(document).on('click','#signupclks',function()
  {  
    $('#pingou').modal('hide');
    $("#pingou").on('hide', function () {
    $('#entrars').modal('show'); 
    });
  });

  $(document).on('click','#signinclks',function()
  { 
    $('#entrars').modal('hide');
    $("#entrars").on('hide', function () {
    $('#pingou').modal('show');
    });
  });

  $(document).on('click','#forgotclk',function()
  { 
    $('#login').modal('hide');
    $("#login").on('hide', function () {
    $('#forgot').modal('show');
    });
  });

  $(document).on('click','#forgotclick',function()
  { 
    $('#forgot').modal('hide');
    $("#forgot").on('hide', function () {
    $('#login').modal('show');
    });
  });

  $(document).on('click','#store_forgotclk',function()
  { 
    $('#pingou').modal('hide');
    $("#pingou").on('hide', function () {
    $('#forgot').modal('show');
    });
  });
</script>  
<!-- END POPUP FUNCTIONALITIES -->


<!--bootstrap date picker starts -->
<script type="text/javascript">
  /*New code 1-7-17*/
  /*$(function () 
  {
    $('#datetimepicker1').datetimepicker();
  });*/
/*End 1-7-17*/
</script>

<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/moment-with-locales.js"></script>

<!--bootstrap date picker ends -->

<!-- select box file starts -->
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/jquery.dropdown.js"></script>
<script type="text/javascript">
  $( function() 
  {
    $('#cd-dropdown').dropdown({
    gutter : 2,
    stack : false,
    slidingIn : 100
    });
        
    $('#cd-dropdown1').dropdown({
    gutter : 2,
    stack : false,
    slidingIn : 100
    });
        
    $('#cd-dropdown2').dropdown({
    gutter : 2,
    stack : false,
    slidingIn : 100
    });

  });
</script>

<!--select box file ends -->
<!--datasort table section starts-->

<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/newjquery-ui.js"></script>
<script type="text/javascript">
  function changeState(el) 
  {
    if (el.readOnly) el.checked=el.readOnly=false;
    else if (!el.checked) el.readOnly=el.indeterminate=true;
  }
  var selectIds = $('#panel1,#panel2,#panel3');
  $(function ($) 
  {
    selectIds.on('show.bs.collapse hidden.bs.collapse', function () {
    $(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
    })
  });
</script>
<!--datasort table section ends-->
<!--show hide sub menu onmouseover -->

<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/bootstrap-typeahead.js"></script>

<script type="text/javascript">
  /*Search form content 6-8-16*/
    var xhr = null;
      $('input.typeahead').typeahead({
    source: function (query, process)
    {
      if( xhr != null ) 
      {
              xhr.abort();
              xhr = null;
          }
      map = {};
      objects = [];
      xhr = $.ajax({
        url: '<?php echo base_url();?>cashback/getstores_listjson/'+query,
        type: 'POST',
        dataType: 'JSON',
        data: 'query=' + query,
        success: function(data) 
        {
          alert(data);
          $.each(data, function (i, object) 
          {
            map[object.affiliate_name] = object;
            objects.push(object.affiliate_name);
          });
          process(objects);
          //alert(objects);
        }
      });
    }
    });
    
    var xhr;
    $('input#Location').typeahead({
        source: function (query, process) 
        {
      if(xhr && xhr.readyState != 4 && xhr.readyState != 0)
      {
        xhr.abort();
      }
      map = {};
      objects = [];
      xhr = $.ajax({
          url: '<?php echo base_url();?>cashback/getcitys_listjson/'+query,
          type: 'POST',
          dataType: 'JSON',
          data: 'query=' + query, 
          success: function(data) 
          {
        $.each(data, function (i, object)
        {
          map[object.city_name] = object;
          objects.push(object.city_name);
        });
            process(objects);
          }
          });
    }
        });
 
  /*end*/
</script>

<!-- Productos page scripts -->
<script type="text/javascript">
  function proRangeSlider(sliderid, outputid, colorclass) 
  {
          var x = document.getElementById(sliderid).value;  
          document.getElementById(outputid).innerHTML = x;
          document.getElementById(sliderid).setAttribute('class', colorclass);
          
          updateTotal();
  }

  var total = 0;
  function updateTotal() 
  {
          var list= document.getElementsByClassName("range");
          [].forEach.call(list, function(el) {
              console.log(el.value);
              total += parseInt(el.value);
          }); 
          document.getElementById("n_total").innerHTML = total;     
  }
  $.fn.serializeObject = function()
  {
      var o = {};
      var a = this.serializeArray();
      $.each(a, function() {
          if (o[this.name] !== undefined) {
              if (!o[this.name].push) {
                  o[this.name] = [o[this.name]];
              }
              o[this.name].push(this.value || '');
          } else {
              o[this.name] = this.value || '';
          }
      });
      return o;
  };

  $(function() 
  {
      $('form').submit(function() {
          $('#result').text(JSON.stringify($('form').serializeObject()));
          return false;
      });
  });
</script>
<!-- Productos page End-->


<!-- FOOTER PAGE EMAIL SUBSCRIBTION DETAILS START-->
<script type="text/javascript">
  function email_news()
  {
    var email = $("#news_email").val();
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,3})?$/;
    if(!email || !emailReg.test(email))
    {
      $('#news_email').css('border', '2px solid red');
    } 
    else
    {
      $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>cashback/email_subscribe/",
      data: {'email':email},
      success: function(msg)
      {
        if(msg==1)
        {
          $('#new_msg').text('Activated Successfully');
          $('#news_email').css('border', '');
        }
        else
        {
          $('#new_msg').text('Already Activated');
          $('#news_email').css('border', '');
        } 
      }
      });
    } 
  }
</script>
<!-- FOOTER PAGE EMAIL SUBSCRIBTION DETAILS END -->


<!-- HEADER PAGE SEARCH BOX FUNCTIONALITIES START-->

<!-- New code for search content in top of the page 6-8-16 -->
<script type="text/javascript">
  $('#search_content').hide();
  $('#selsearch').each(function() 
  {
    $(this).show(0).on('click', function(e) 
    {
      //alert("hai");
      // This is only needed if your using an anchor to target the "box" elements
      e.preventDefault();
      // Find the next "box" element in the DOM
      $('#search_content').slideToggle('fast');
      $('#search').focus();
    });
  });

  $('#search_content1').hide();
  $('#selsearch1').each(function() 
  {
    $(this).show(0).on('click', function(e) 
    {
      e.preventDefault();
      $('#search_content1').slideToggle('fast');
      $('.searchs').focus();
    });
  });
</script>
<!-- End -->
<script src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery-ui.js"></script>
<?php $search_name =$this->front_model->search_name(); ?>
<!-- New code fo search store and submit action -->
<script type="text/javascript">
  function submit_form()
  {
    //var storeid_enc = $('#search').val();
    //alert(storeid);
    //var storeid = storeid_enc.replace(/ /g,"-");
    //var storeid = storeid.toLowerCase();
    var storeid   = $('#search').val();
    var redirect_url = '<?php echo base_url();?>cupom-desconto/'+storeid;
    $('#storehead').val(storeid);     
    $('#dummyform').attr('action', redirect_url).submit();
    //window.location.href=redirect_url;
    return false;
  }
</script>
<!-- End -->
<script type="text/javascript">
  //New code for search//
  var avail = <?php echo $search_name; ?>;
  jQuery(".search_header").autocomplete({

     source: avail,        
     select: function (event, ui) 
     {
            autoFocus: true;
            location.href = ui.item.the_link;

      $("#product_id").val(ui.item.product_id); 

     },

  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
  var msg = '<a href="'+item.the_link+'"><div style="margin:8px;" class="search_list"><span><div class="search_icon"><img width="150" height="70" class="img-response" src="<?php echo $this->front_model->get_img_url(); ?>uploads/affiliates/'+item.logo+'" /></div><strong style="margin-left:10px; text-transform: capitalize;">'+item.label+'</strong></span></div></a>';
  return $( "<li>" )
  .append( msg )
  .appendTo( ul );

  };
  //End//
</script>
<!-- HEADER PAGE SEARCH BOX FUNCTIONALITIES END -->

<!-- NOTIFICATION CONTENT IN TOP OF THE PAGE FUNCTIONALITIES START -->
<?php 
  if(($unlog_status == 1 && $unlog_menu_status == 1) || ($unlog_status == 0 && $unlog_menu_status == 1))
  {
    ?>
    <script>
    $(window).scroll(function(){
        'use strict';
        if ($(this).scrollTop() > 50){  
            $('#cbp-af-header').addClass("sticky");
        }
        else{
            $('#cbp-af-header').removeClass("sticky");
        }
    });
    </script>
    <?php
  }
  if(($log_status == 1 && $log_menu_status == 1) || ($log_status == 0 && $log_menu_status == 1))
  {
    ?>
    <script>
    $(window).scroll(function(){
        'use strict';
        if ($(this).scrollTop() > 50){  
            $('#cbp-af-header1').addClass("sticky");
        }
        else{
            $('#cbp-af-header1').removeClass("sticky");
        }
    });
    </script>
    <?php
  }
?>
<!-- NOTIFICATION CONTENT IN TOP OF THE PAGE FUNCTIONALITIES END -->

<!-- sales bannel click count script 8-11-16 -->
<script type="text/javascript">
    $(document).delegate('.click_count','click',function()
    {
      var banner_id = $(this).attr('bannerid');
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>cashback/banner_clickcount",
        data: {'banner_id':banner_id},
        cache: false,
        success: function(result)
        {
          return true;
        }
      });
    })   

</script>
<!--End 8-11-16-->