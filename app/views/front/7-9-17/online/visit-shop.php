<?php
$cashback_details    = $this->session->userdata('link_name');
$cashback_percentage = $this->session->userdata('cashback_web');
$redirect_url = redirect_handler($coupon,$store_details,$this->front_model,$cashback_details,$name);
//echo "Redirect url :".$redirect_url; echo "<br>";exit;

/*New code for store page cotnent shortcut details 26-4-17*/
$user_id            = $this->session->userdata('user_id'); 
$userdetails        = $this->front_model->userdetails($user_id);
$firstname          = $userdetails->first_name;
$lastname           = $userdetails->last_name;
$emailid            = $userdetails->email;

$refer_code         = $userdetails->random_code;
if($refer_code == '')
{
  $refer_code = '';
}

$date     = date('Y-m-d');
$newdate  = explode('-',$date);
$year     = $newdate[0];
$month    = $newdate[1];
$day      = $newdate[2];
$newday   = $day;
$off_year = date('y');
$local_month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); 
$braz_months_names = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
$dt = date('F');
$current_month     = str_ireplace($local_month_names, $braz_months_names, $dt);

$get_cat_type        = $this->front_model->referal__category();
$get_cat_details     = $this->front_model->get_referral_settings($get_cat_type);

$type_one_percentage = $get_cat_details->ref_cashback;
$type_one_days       = $get_cat_details->valid_months;
$type_two_amount     = $get_cat_details->ref_cashback_rate;  
$type_three_amount   = $get_cat_details->ref_cashback_rate_bonus;
$type_three_users    = $get_cat_details->friends_count;
$unique_bonus        = str_replace('.', ',',$get_cat_details->category_bonus_amount);

if($user_id == '')
{
  $get_cat_detailss  = $this->front_model->get_referral_settings(1);
  $unique_bonus     = str_replace('.', ',',$get_cat_detailss->category_bonus_amount);
}
else
{
  if($firstname =='' && $lastname =='')
  { 
    $emails    = explode('@', $emailid);
    $usernames = $emails[0];  
  }
  else
  {
    $usernames = ucfirst($firstname)." ".ucfirst($lastname);
  }
}


/*New code for shortcut details 28-8-17*/
$stcashback_percentage = $this->front_model->shortcutdetails($store_details->cashback_percentage);
$cashback_percentage   = $this->front_model->shortcutdetails($cashback_percentage); 
$site_url   		   = $this->front_model->shortcutdetails($store_details->logo_url); 
$redir_notify  		   = $this->front_model->shortcutdetails($store_details->redir_notify);
$mobile_redir_notify   = $this->front_model->shortcutdetails($store_details->mobile_redir_notify);

/*End 28-8-17*/

$affiliate_cashback_type = $store_details->affiliate_cashback_type;
if($affiliate_cashback_type == 'Percentage')
{
  $cashbacks = $stcashback_percentage."%";
}
else if($affiliate_cashback_type == 'Flat')
{
  $cashbacks = "R$ ".$stcashback_percentage;
}
else
{
  $cashbacks = ""; 
}


$data = array(
  '###STORE-NAME###'=>$store_details->affiliate_name,
  '###CASHBACK###'=>$cashbacks,
  '###TRACKING-SPEED###'=>$store_details->report_date,
  '###ESTIMATED-PAYMENT###'=>$store_details->retailer_ban_url,
  '###COUPON-NUMBER###' =>$count_act_coupons,
  '###DD###'    =>$newday,
  '###MM###'    =>$month,
  '###MONTH###' =>$current_month,
  '###YYYY###'  =>$year,
  '###YY###'    =>$off_year,
  '###STORE_IMG_ONE###'  =>$this->front_model->get_img_url()."/uploads/affiliates/".$store_details->store_one_img,
  '###STORE_IMG_TWO###'  =>$this->front_model->get_img_url()."/uploads/affiliates/".$store_details->store_two_img,
  '###STORE_IMG_THREE###'=>$this->front_model->get_img_url()."/uploads/affiliates/".$store_details->store_three_img,
  '###STORE_IMG_FOUR###' =>$this->front_model->get_img_url()."/uploads/affiliates/".$store_details->store_four_img,
  '###STORE_IMG_FIVE###' =>$this->front_model->get_img_url()."/uploads/affiliates/".$store_details->store_five_img,
  '###STORE_IMG_SIX###'  =>$this->front_model->get_img_url()."/uploads/affiliates/".$store_details->store_six_img,         
  '###REFERRAL-PARAMETER###'=> $refer_code,
  '###Type-ONE###'=>$type_one_percentage,
  '###Type-ONE-days###'=>$type_one_days,
  '###Type-TWO###'=>$type_two_amount,
  '###Type-THREE###'=>$type_three_amount,
  '###Type-THREE-number-of-users###'=>$type_three_users,
  '###UNIQUE-BONUS###' => $unique_bonus,
  '###USER-NAME###' => $usernames
  );
/*End 26-4-17*/

	if($coupon == '')
	{
		$store_url    = $site_url;
		if(strpos($store_url,'?') !== false) 
		{
		    $redirect_url = str_replace("?","&",$redirect_url);
		}

		$Redirect_URI = $store_url.$redirect_url;
		//$Redirect_URI = $store_details->logo_url.$redirect_url;
		//echo "Redirect url without Coupon code :".$Redirect_URI; echo "<br>";
	}
	else
	{
	    $Redirect_URI = $redirect_url;
	    //echo "Redirect url with Coupon code :".$Redirect_URI; echo "<br>";
	} 
	//exit;

	/*new code for cashback exclusive rediect notification details 22-4-17*/
	$analytics_info = $this->session->userdata('analytics_info');
	if($analytics_info!='')
	{
		$getcash_ex_details = $this->front_model->getcashback_ex_details($analytics_info);	
	}

	//echo $getcash_ex_details; 
	if(isset($getcash_ex_details))
    {
    	if($getcash_ex_details->store_name == $store_details->affiliate_name)
        { 

	    	$cashback_percentage     = $cashback_percentage;
	    	$ex_redir_notify 		 = $this->front_model->shortcutdetails($getcash_ex_details->desk_redir_notify);
	    	$ex_mobile_redir_notify  = $this->front_model->shortcutdetails($getcash_ex_details->mob_redir_notify);
	    	

	      	$store_name       = $getcash_ex_details->store_name;
	       	$desk_redirect 	  = $ex_redir_notify; //$getcash_ex_details->desk_redir_notify; 24-8-17
	      	$mobile_redirect  = $ex_mobile_redir_notify;//$getcash_ex_details->mob_redir_notify; 24-8-17
	    }
	    else
	    {
	    	$stcashback_percentage     = $stcashback_percentage;
	    	$store_name      = strtr($store_details->affiliate_name,$data);
	    	$desk_redirect   = strtr($redir_notify,$data); //store_details->redir_notify 24-8-17
	    	$mobile_redirect = strtr($mobile_redir_notify,$data); //store_details->mobile_redir_notify 24-8-17
    	}
    }
    else
    {
    	$stcashback_percentage     = $stcashback_percentage;
    	$store_name      = strtr($store_details->affiliate_name,$data);
    	$desk_redirect   = strtr($redir_notify,$data); //store_details->redir_notify 24-8-17
    	$mobile_redirect = strtr($mobile_redir_notify,$data); //store_details->mobile_redir_notify 24-8-17
	}
	/*End 22-4-17*/


?>
<!--Header content Start-->
<!DOCTYPE html>
<html lang="en" class="loadarea">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="refresh" content="2; url=<?php echo $Redirect_URI; ?>" />
	<title><?php $admindetailssss = $this->front_model->getadmindetails_main(); echo $admindetailssss->homepage_title; ?></title>
	<meta name="Description" content="Get the best Cashback Offers at top brands. Never pay full price again. Join now Free & Start Saving!"/>
	<meta name="keywords" content="cashback, vouchers, coupons, discounts, offers, deals, promo codes, onlin shopping, best online shopping sites" />
	<meta name="robots" CONTENT="INDEX, FOLLOW" />
	<?php $this->load->view('front/css_script');?>
</head>																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																	
<?php //$this->load->view('front/header'); ?>
<!-- header content End -->
<style type="text/css">
	.text-center
	{
		color:#333333!important;
	}
	.stores p
	{
		color: #fff !important;
	    font-size: 20px !important;
	    margin-bottom: 0 !important;
	}
	.cls_loadbtn {white-space: normal;}
</style>
<!-- Main Content start -->
<body class="loader-bg">
	<div class="wow fadeInDown">
	  	<div class="loader">
		    <center>
		    	<div class="wow fadeInDown loader-box">
			        <!-- animated loading progressbar starts -->
			        <div class="progress">
			          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"> </div>
			        </div>
			        <!-- animated loading progressbar ends -->
		        
			        <div class="load-inn">
				        <!-- loader image block starts -->
				        <div class="load-imgblk">
				          <center>
				            <img src="<?php echo $this->front_model->get_img_url();?>uploads/affiliates/<?php echo $store_details->affiliate_logo;?>" align="middle" alt="<?php echo $store_details->affiliate_name;?>" class="img-responsive">
				          </center>
				        </div>
				        <!-- loader image block ends -->
				        
				        <center>
				          <br>
							<div class="hidden-xs">
								<button class="btn btn-info cls_loadbtn stores">
				            		<?= $desk_redirect; ?>
				            	</button>
							</div>	
				            <div class="hidden-md hidden-sm">
								<button class="btn btn-info cls_loadbtn stores">
				            		<?php echo str_replace('<p>','',$mobile_redirect);?> 
				            	</button>
				            </div>
					            
				          
				          <br>
				          <h3>Estamos ativando seu dinheiro de volta</h3>
				          <h1><?php if($cashback_percentage!='') { echo $cashback_percentage; } else { echo $stcashback_percentage; } ?><sup>%</sup> </h1>
				          <p>de volta</p>
				          <h3>Faca sua compra normalmente na <?php echo $store_name;?>.</h3>
				        </center>
			        </div>
		        
			        
					<p class="text-center">Se não for redirecionado automaticamente, 
					<a href="<?php $redirect_url;?>" style="color:#4daed9;"> clique aqui </a></p>	
	        	</div>
		    </center>
	 	</div>
	</div>
<!-- Main Content end -->
<!-- Footer menu start -->
<?php $this->load->view('front/js_scripts');?>
</body>
</html>