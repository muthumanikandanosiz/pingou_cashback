<?php $this->load->view('front/header'); ?>
<!-- header content End -->
<style>
.icon-cont
{
    font-family: "Source Sans Pro",sans-serif !important;
}

/*Banner style start*/
.newimg-responsive.center-block {
    height: 391px !important;
    width: 823px !important;
}
/*Banner End*/

li.waves-effect {
    background: #4daed9 none repeat scroll 0 0;
    border-bottom: 0 none;
    color: #fff;
}
.product-time
{
  font-size: 12px !important;
}
.checkbox-list
{

  padding: 0 25px !important;
}
#loading-circle-overlay {
    background: rgba(255, 255, 255, 0.8);
    margin: 0 auto;
    min-height: 150px;
    width:100%;
    margin-top:-50px;
}
.loadinh_bg {
    margin: 0 auto !important;
    width:50% !important;
    background:none repeat scroll 0 0 white !important;
    margin-top:75px !important;
    position:relative;
    left:0;
    right:0;
}
.full-width {
    width: 100% !important;
}
</style>
<style>
#loading-circle-overlay {
    bottom: 0;
    left: 0;
    position: fixed;
    right: 0;
    top: 51px;
    z-index: 100;
    background:#ggg;
}
.loadinh_bg {
    border-radius: 10px;/*    margin: 200px auto 0;

    padding:10px;

    width:100px;
*/
}
.loader {
    border: 2px solid #555555 !important;
    font-style: italic;
    height: 75px;
    position: relative;
}
.irs-slider {
    background: black none repeat scroll 0 0 !important;
    border-radius: 15%!important;
    cursor: pointer !important;
    display: block!important;
    height: 30px!important;
     
    position: absolute !important;
    top: 0px !important;
    width: 13px !important;
}

.irs-line {
    background: #3da0d5 none repeat scroll 0 0 !important;
   
    top: 10px !important;
}
.irs-diapason {
    background: #3da0d5 none repeat scroll 0 0 !important; 
    display: block;
    height: 10px;
    left: 0;
    position: absolute;
    top: 10px !important;
    width: 100%;
}
.irs-from, .irs-to, .irs-single {
    top: 35px !important;
    }
    .irs-min, .irs-max {
        top: 35px !important;
    }

/*New code for design changes 30-9-16*/
/*.modal-header {
    border-bottom: 0px !important;
    padding: 0px;
}*/
/*end*/

.modal-title 
{
   margin: 30px !important;
}
.modal-body
{
    padding:0px !important;
}

</style>


<!-- Main Content start -->
<?php
$user_id  = $this->session->userdata('user_id');
$cityname = $this->session->userdata('cityname');
?>

<section class="cms wow fadeInDown">
    <div class="container">
        <div id="loading-circle-overlay" class="loading-circle-overlay" style="display:none;">
            <div id="model-back">
                <div class="loadinh_bg">
                    <div class="main_content_bg">
                        <div class="details_bg">
                            <div style="overflow:hidden;clear:both;">
                                <div > <!--<font size="-1" color="#A2A2A2" style="margin-left: 26px;">Please Wait...</font>--> 
                                    <img src="<?php echo $this->front_model->get_img_url();?>front/images/inspiroo_logo_loader_pop.gif" class="img-responsive center-block" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        $rest_catcoupon=$this->front_model->getcnt_allpremiumcoupon_incat();    
        ?>
        <div class="about clearfix mar-top-50">
            <div class="wow fadeInDown">
                <!-- Side menu list start -->
                <!-- <div class="col-md-3 col-sm-4 col-xs-12 wow fadeInLeft">
                    <div class="left-prod-sidebar mar-bot-20">
                        <ul class="list-accordion list-unstyled" id="removeclass">
                            
                            <li class="categ-menu" id="claa_all">
                                <a href="javascript:void('0');" class="waves-effect" onclick="funct_setpremium_cat('all','cat');">
                                    <span class="icon">
                                        <img src="<?php echo $this->front_model->get_img_url();?>front/new/images/categ1.png" alt="product">
                                    </span>
                                    <span>All</span>
                                </a>
                            </li>
                            <?php  
                            $rest_catcoupon=$this->front_model->get_allpremiumcoupon_cat();    
                            if($rest_catcoupon!="")
                            {
                                foreach($rest_catcoupon as $fet_premiumcat) 
                                { 
                                    $db_category_id    = $fet_premiumcat->category_id;  
                                    $db_category_name  = $fet_premiumcat->category_name; 
                                    $db_catdate_added  = $fet_premiumcat->date_added; 
                                    $rest_pcatcnt      = $this->front_model->get_countofpremiumcat_addcoupon($db_category_id);
                                    ?>
                                    <li><a href="javascript:void('0');" id="claa_<?php echo $db_category_id  ?>" onclick="funct_setpremium_cat('<?php echo $db_category_id; ?>','cat');" class=""><span class="icon"><i class="fa fa-mobile"></i></span><span class="icon-cont"><?php echo $db_category_name;  ?></span></a></li>
                                    <?php    
                                } 
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="sidebar-box">
                        <h2>Filter By Price</h2>
                        <input type="text" id="price-slider_1" onblur="funct_setpremium_cat(1,0)" >
                    </div>
                    <div class="prod-comp-sidebar">
                        <h5>Product feature</h5>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <ul class="checkbox-list">
                              <li class="checkbox clip-check check-primary" >
                                <input type="checkbox" id="check1"  value="new"  class="i-check" >
                                <label for="check1" onClick="funct_setpremium_cat('new','feature')">
                                  <span >New</span> <small></small>
                                  <input type="hidden" id="new"  value="0 " class="i-check" >
                                </label>
                              </li>
                              <li class="checkbox clip-check check-primary" >
                                <input type="checkbox" id="check2" value="es"  class="i-check" >
                                <label for="check2" onClick="funct_setpremium_cat('es','feature')">
                                  <span >Ending Soon</span> <small></small>
                                  <input type="hidden" id="es"  value="0 " class="i-check" >
                                </label>
                              </li>
                              <li class="checkbox clip-check check-primary" >
                                <input type="checkbox"  id="check3" value="popular"  class="i-check" >
                                <label for="check3" onClick="funct_setpremium_cat('popular','feature')">
                                  <span>Popular</span> <small></small>
                                  <input type="hidden" id="popular"  value="0" class="i-check" >
                                </label>
                              </li>
                              <li class="checkbox clip-check check-primary" >
                                <input type="checkbox" id="check4" value="featured" class="i-check" >
                                <label for="check4" onClick="funct_setpremium_cat('featured','feature')">
                                  <span>Featured</span> <small></small>
                                  <input type="hidden" id="featured"  value="0" class="i-check" >
                                </label>
                              </li>
                            </ul>
                        </div>
                    </div>
                </div> -->
                <!-- End Menu list -->
                <div class="col-md-12 col-sm-8 col-xs-12 wow fadeInRight">
                    <div class="clearfix wow bounceIn topprodblk mar-bot-20 cls_indban">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel" > 
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" >
                                <?php
                                $resultsss = $this->front_model->premium_home_slider();
                                $k=1;
                                foreach($resultsss as $imgs)
                                {
                                    $view_img = $imgs->banner_image;
                                    $banner_url = $imgs->banner_url;
                                    $img_name = $imgs->banner_heading;
                                    if($k==1)
                                    {
                                        $st = 'active';
                                        //$st = '';
                                    }
                                    else
                                    {
                                        $st = '';
                                    }
                                    ?>
                                    <div class="item <?php echo $st;?>" >
                                        <a href="<?php echo $banner_url;?>">
                                            <img class="new img-responsive center-block" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$view_img; ?>">
                                        </a>
                                    </div>
                                    <?php
                                    $k++;
                                } 
                                ?>
                                <!-- End Item --> 
                            </div>
                        </div>
                    </div>

                    <div id="resp_scrool"></div>
                    <div id="response"> 
                        <?php
                            //echo "<pre>";print_r($result); exit;
                            if($result!="0")
                            {    
                                ?>
                                <div class="row wow fadeInDown"> <!-- row wow fadeInDown -->
                                    <?php 
                                     
                                    foreach($result as $fetrest)
                                    { 

                                        $shoppingcoupon_id     = $fetrest->shoppingcoupon_id;
                                        $db_offer_name         = $fetrest->offer_name;
                                        $db_coupon_description = $fetrest->description;
                                        $db_coupon_image       = $fetrest->coupon_image;
                                        $db_expiry_date        = $fetrest->expiry_date;
                                        $db_cp_price           = $fetrest->amount;
                                        $exp_db_coupon_image   = explode(",",$db_coupon_image);  
                                        $f_dbcouponfirst_img   = $exp_db_coupon_image[0];
                                        $len_db_offer_name     = strlen($db_offer_name);  
                                        $len_db_coupon_desc    = strlen($db_coupon_description);  
                                        $total_price            = $fetrest->price; 
                                        $sales_price            = $fetrest->amount;
                                        /*New code for precentage details 21-7-16*/
                                        $sales_price         = ($total_price - $sales_price);
                                        $discount_percentage = (($sales_price/$total_price)*100);
                                        $discount_percentage = preg_replace('/\./', ',',number_format(round($discount_percentage ,2),2));
                                        /*end*/

                                        /*new code for shopping coupons get store percentage details 27-1-17*/
                                        
                                        $store_cash_type  = $storedetails->affiliate_cashback_type;
                                        $cashback_amounts = $storedetails->cashback_percentage;

                                        
                                        /*New code for cashback exclusive content 31-8-17*/
                                        $newcashback_ex_id = $this->session->userdata('cash_ex_id');
                                        $cashback_details  = $this->session->userdata('link_name');

                                        $cash_query        = $this->db->query("SELECT * from cashback_exclusive where id='$newcashback_ex_id'")->row();
                                        $cashback_type     = $cash_query->cashback_type;

                                        if($cashback_type!='')
                                        {
                                            $cashback_amounts = $this->front_model->shortcutdetails($cash_query->cashback_web);
                                        }
                                        else
                                        {
                                            /*New code for shortcut details 28-8-17*/
                                            $cashback_amounts = $this->front_model->shortcutdetails($cashback_amounts);
                                            /*end 28-8-17*/
                                        }
                                        /*End 31-8-17*/


                                        /*New code for shortcut details 28-8-17*/
                                        //$cashback_amounts = $this->front_model->shortcutdetails($cashback_amounts);
                                        /*end 28-8-17*/

                                        if($store_cash_type == "Flat")
                                        {
                                            $new_cash_amt      = $cashback_amounts;
                                            $cashback_amounts  = "R$". $cashback_amounts;
                                            $cashback_amount   = "R$". $cashback_amounts;
                                            $cashback_amt      = $cashback_amount;
                                            $cashback_amts     = $this->front_model->currency_format($cashback_amt);
                                            //$cashback_amts     = 4.9765;
                                            $cashback_amts     = str_replace(',','.', $cashback_amts);
                                            $cashback_amts     = round($cashback_amts,2);
                                        } 
                                        if($store_cash_type == "Percentage")
                                        {
                                            $new_cash_amt      = $cashback_amounts;
                                            $cashback_amounts  = $cashback_amounts ."%";
                                            $cashback_amount   = $cashback_amounts ."%";
                                            $cashback_amt      = (($db_cp_price)*($cashback_amount)/100);
                                            $cashback_amts     = $this->front_model->currency_format($cashback_amt);
                                            //$cashback_amts     = 4.9765;
                                            $cashback_amts     = str_replace(',','.', $cashback_amts);
                                            $cashback_amts     = round($cashback_amts,2);
                                        } 
                                        if(strstr($cashback_amts, '.'))
                                        {
                                        }
                                        else 
                                        {
                                            $cashback_amts = $cashback_amts.'.00';
                                        }

                                        $cashback_amts = str_replace('.',',', $cashback_amts); 
                                        /*end 27-1-17*/




                                        if($len_db_offer_name>=20)
                                        {
                                            $f_dbcp_name=substr($db_offer_name,0,30)."..."; 
                                        }
                                        else
                                        {
                                            $f_dbcp_name=$db_offer_name; 
                                        }
                                        if($len_db_coupon_desc>=54)
                                        {
                                            $f_dbcp_desc=substr($db_coupon_description,0,20)."..."; 
                                        }
                                        else
                                        {
                                            $f_dbcp_desc=$db_coupon_description; 
                                        } 
                                        $getremain_days=$this->front_model->find_remainingdays($fetrest->expiry_date);  
                                        ?>    
                                        
                                        <!-- new code for category name included in url 30-9-16 -->
                                        <?php 
                                            $category_ids  = $fetrest->category;
                                            $category_name = $this->db->query("select * from premium_categories where category_id=$category_ids")->row('category_url'); 
                                        ?>
                                        <!-- End 30-9-16 -->

                                        <!-- <a href="<?php echo base_url(); ?>promocaonew/<?php echo urlencode($fetrest->seo_url); ?>"> -->
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="prod-photodetblk">
                                                <?php 
                                                $image_type          = $fetrest->img_type;
                                                $db_coupon_image     = $fetrest->coupon_image;
                                                if($image_type == 'url_image')
                                                {
                                                    ?>
                                                    <div class="photo">
                                                        <img alt="a" class="img-responsive" style="height:292px; width:264px" src="<?php echo $db_coupon_image; ?>" alt="Image Alternative text" title="Hot mixer">
                                                    </div>
                                                <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                    <div class="photo">
                                                        <img alt="a" class="img-responsive" style="height:292px; width:264px" src="<?php echo $this->front_model->get_img_url(); ?>uploads/premium/<?php echo $db_coupon_image; ?>" alt="Image Alternative text" title="Hot mixer">
                                                    </div>
                                                    <?php 
                                                } 
                                                ?>
                                                <div class="info">
                                                    <div class="row vers">
                                                        <center>
                                                            <h4>
                                                                <a href="<?php echo base_url(); ?>promocao/<?php echo urlencode($fetrest->seo_url); ?>">
                                                                    <span class="cls_sub_text">
                                                                        <?php echo $f_dbcp_name; ?>
                                                                        <br>
                                                                        R$ <?php echo $this->front_model->currency_format($db_cp_price); ?> e receba R$ <?php echo $cashback_amts; ?> de volta
                                                                    </span>
                                                                </a>
                                                            </h4>
                                                        </center>
                                                    </div>
                                                    <div class="overlay-prod ">
                                                        <div class="row vers hov">
                                                            <center>
                                                                <h4 class="">
                                                                    <a href="<?php echo base_url(); ?>promocao/<?php echo $category_name;?>/<?php echo urlencode($fetrest->seo_url); ?>"><?php  echo $f_dbcp_name;  ?></a>
                                                                </h4>
                                                                <p>Pague R$ <?php echo $this->front_model->currency_format($db_cp_price); ?> e receba <br> R$<?php echo $cashback_amts; ?> de volta</p>
                                                                <span><?php  echo $getremain_days['days']." days ".$getremain_days['hours']." h "." remaining";   ?></span>
                                                                <a href="<?php echo base_url(); ?>promocao/<?php echo $category_name;?>/<?php echo urlencode($fetrest->seo_url); ?>">
                                                                <button type="button" class="btn btn-blu">Details</button>
                                                                </a>
                                                            </center>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- </a> -->
                                    <?php 
                                    }
                                    ?>    
                                </div>
                                <?php 
                            }
                            else
                            {
                                ?>
                                <div class="alert alert-danger">
                                <button class="close" data-dismiss="alert">x</button>
                                <strong>Oops! </strong> <?php echo "No Coupons Are Found";   ?> </div>
                                <?php 
                            }   
                        ?>
                        <?php if($result!=0) echo $this->pagination->create_links();?>                 
                    </div>
                <div class="gap"></div>
            </div>
        </div>
    </div>
</section>
<?php  $maxva = $this->db->query('SELECT max(amount) as maxva FROM `shopping_coupons`')->row('maxva');?>
<input type="hidden" name="max_val" id="max_val" value="<?php echo $maxva;?>" >
<!-- Main Content end -->

<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->

<!-- Modal -->
<!-- <div id="myModal2" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:900px;">
        <div class="modal-content">
            <div class="modal-header">
                <a class="btn btn-default" data-dismiss="modal" style="float:right;"><span class="glyphicon glyphicon-remove"></span></a>
                <h3 class="modal-title text-center" id="myModalLabel"> Subscribe with <?php $admindetails = $this->front_model->getadmindetails_main(); echo $admindetails->site_name; ?></h3>
            </div>
            <div class="modal-body">
                <div class="account-login">
                  <div class="col-md-6 no-left-margin">
                    <div class="registered-users bot-shadow">
                      <div class="content">
                        <div id="login">
                          <div class="row">
                            <div class="col-xs-12">
                              <div class="form-wrap">
                                <center><h2>Subscribe with <?php $admindetails = $this->front_model->getadmindetails_main(); echo $admindetails->site_name; ?></h2></center>  
                                <div class="signin-or">
                                  <hr class="hr-or">
                                  <span class="span-or"></span>
                                </div>
                                <?php
                                         //begin form
                                            $attribute = array('role'=>'form','name'=>'sub_form_1','id'=>'sub_form', 'onsubmit'=>'return onlyAlphabets();', 'autocomplete'=>'off','method'=>'post');
                                            echo form_open('email-subscribe-shoppping',$attribute);                     
                                        ?>
                                <?php
                                        if(!$this->session->userdata('user_id'))
                                        {
                                        ?>
                                  <div class="form-group">
                                    <center>
                                      <span id="userstatus" style="color:red; font-weight:bold;"> </span>
                                    </center>
                                    <label for="email" class="sr-only">Email<span class="required_field">*</span></label>
                                    <input type="email" required id="email" class="form-control input-lg" placeholder="somebody@example.com" name="email" autocomplete="off"  >
                                  </div>
                                <?php
                                          } 
                                          ?>
                                  <div class="form-group">
                                    <label for="key" class="sr-only">Location<span class="required_field">*</span></label>
                                    <input type="text" required id="Location" class="form-control input-lg" name="location" onsubmit="return onlyAlphabets()"  placeholder="Enter Your Location" >
                                    <div id="notification" style="color: red;"></div>
                                  </div>
                                  <input type="submit"  class="btn btn-custom btn-lg btn-block" name="signin" id="signin" value="Submit">
                                </form>
                                <hr>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 no-right-margin">
                    <div class="new-user">
                      <div class="content">
                        <div class="section-line">
                          <?php
                              $getadmindetails = $this->front_model->getadmindetails(); 
                                $logo = $getadmindetails[0]->site_logo;
                                ?>
                          <center>
                            <img src="<?php echo $this->front_model->get_img_url();?>uploads/adminpro/<?php echo $logo;?>" class="img-responsive">
                          </center>
                          <hr>
                          <h3 class="clr-blu">SAVE MONEY at 500+ brands -<small> why pay more when you can pay less? </small></h3>
                          <ul class="list-inline clearfix mar40">
                            <li><a href="<?php echo base_url();?>desconto-cupom" ><img src="<?php echo $this->front_model->get_img_url();?>front/images/store-img-1.png" class="img-thumbnail mar-bot" width="70"></a></li>
                            <li><a  href="<?php echo base_url();?>desconto-cupom"><img src="<?php echo $this->front_model->get_img_url();?>front/images/store-img-2.png" class="img-thumbnail mar-bot" width="70"></a></li>
                            <li><a  href="<?php echo base_url();?>desconto-cupom"><img src="<?php echo $this->front_model->get_img_url();?>front/images/store-img-3.png" class="img-thumbnail mar-bot" width="70"></a></li>
                            <li><a href="<?php echo base_url();?>desconto-cupom" ><img src="<?php echo $this->front_model->get_img_url();?>front/images/store-img-4.png" class="img-thumbnail mar-bot" width="70"></a></li>
                            <li><a href="<?php echo base_url();?>desconto-cupom" ><img src="<?php echo $this->front_model->get_img_url();?>front/images/store-img-5.png" class="img-thumbnail mar-bot" width="70"></a></li>
                            <li><a href="<?php echo base_url();?>desconto-cupom" ><img src="<?php echo $this->front_model->get_img_url();?>front/images/store-img-6.png" class="img-thumbnail mar-bot" width="70"></a></li>
                            <li><a href="<?php echo base_url();?>desconto-cupom" ><img src="<?php echo $this->front_model->get_img_url();?>front/images/store-img-7.png" class="img-thumbnail mar-bot" width="70"></a></li>
                            <li><a  href="<?php echo base_url();?>desconto-cupom"><img src="<?php echo $this->front_model->get_img_url();?>front/images/store-img-8.png" class="img-thumbnail mar-bot" width="70"></a></li>
                          </ul>
                          <hr>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer"> </div>
        </div>
    </div> 
</div> -->
 


<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>

<?php
if($this->session->userdata('cityname'))
{
    $cityname = $this->session->userdata('cityname');
}
?>

    <input type="hidden" value="<?php if($this->session->userdata('cityname')){echo $cityname;};?>" name="" id="hidden_user_id" />
    <!-- /.modal --> 

        
    <!-- Filter by price Scripts queries -->
    <script src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/ionrangeslider.js"></script>
    <!-- End -->
 
<script type="text/javascript">


$(document).ready(function() {
$( "#price-slider" ).blur(function() {
alert( "Handler for .blur() called." );
});
});

 

 function funct_setpremium_cat(catid,cat)

 {   

  $(".loading-circle-overlay").show();

    if(cat=="feature")

    {

      if($("#"+catid).val()==0)

      {

          $("#"+catid).val('1');

      }

      else

      {

          $("#"+catid).val('0');

      }

    }

    

    if(cat=="cat")

    {

      catid =catid;

      $('#removeclass li a').removeClass( "waves-effect active" );

      $('#claa_'+catid).addClass( "waves-effect active" );

    }

    else

    {

      catid="0";    

    }



    var starting_price=$(".irs-from").text().split('Rs');

    var end_price=$(".irs-to").text().split('Rs');

//if(starting_price)
//
    //{

        //alert(starting_price);

        //alert(end_price);

    //}

    //else

    //{

    //  alert('ssssssss');

    //} 

    

    if (starting_price.length == 1) 

    {

        var final_start_price = starting_price[0];

    // not found

    }  else {

        var final_start_price = starting_price[1];

     // multiple items found

    }

    

    if (end_price.length == 1)

     {

        var final_send_price = end_price[0];

    }  else

     {

         var final_send_price = end_price[1];

     // multiple items found

    }





    

    var featured=$("#featured").val();

    var popular=$("#popular").val();

    var new1=$("#new").val();

    var es=$("#es").val();



       $.ajax({  

    url:"<?php echo base_url(); ?>index.php/cashback/ajaxsess_setpremiumcategory",

    data: "catid="+catid+"&starting_price="+final_start_price+"&end_price="+final_send_price+"&featured="+featured+"&popular="+popular+"&new="+new1+"&es="+es,     

    type:"POST",

    success:function(html_butadelike)

    {          

    $('#response').html(html_butadelike);

     $(".loading-circle-overlay").hide();

     $('html, body').animate({

        'scrollTop' : $("#resp_scrool").position().top

    });

     //resp_scrool

    }  

    });     

  

 }    

var maxsel = <?php echo $maxva;?>;
$("#price-slider_1").ionRangeSlider({

    min: 0,

    max: maxsel,

    type: 'double',

    prefix: "Rs",

    prettify: false,

    hasGrid: false,

    onFinish: function (obj) {

            funct_setpremium_cat(1,0);

    }

});

/*$(".irs-from").text(<?php echo $this->session->userdata("sess_cashback_starting_price"); ?>);

$(".irs-to").text(<?php echo $this->session->userdata("sess_cashback_end_price"); ?>);*/



</script>

 <script type="text/javascript">
    var hiddenuserid = $('#hidden_user_id').val();

        if(hiddenuserid=='')
        {
         $(document).ready(function() {
           $('#myModal2').modal({
             show: true,
             backdrop: 'static',
             keyboard: false
           })
         });
        }
        

        function onlyAlphabets() {

              var regex = /^[a-zA-Z]*$/;
              if (regex.test(document.sub_form_1.location.value)) {
            
                  //document.getElementById("notification").innerHTML = "Watching.. Everything is Alphabet now";
                  return true;
              } else {
                  document.getElementById("notification").innerHTML = "Alphabets Only";
                  return false;
              } 
        
        }
 

</script>


<script>

$(document).ready(function() 
    {
        var xhr = null;
        $('input.typeahead').typeahead({
        source: function (query, process)
        {
            if( xhr != null ) 
            {
                xhr.abort();
                xhr = null;
            }
            map = {};
            objects = [];
            xhr = $.ajax({
                url: '<?php echo base_url();?>cashback/getstores_listjson/'+query,
                type: 'POST',
                dataType: 'JSON',
                data: 'query=' + query,
                success: function(data) 
                {
                    alert(data);
                    $.each(data, function (i, object) 
                    {
                        map[object.affiliate_name] = object;
                        objects.push(object.affiliate_name);
                    });
                    process(objects);
                    //alert(objects);
                }
            });
        }
        });
        
        var xhr;
        $('input#Location').typeahead({
        source: function (query, process) 
        {
            if(xhr && xhr.readyState != 4 && xhr.readyState != 0)
            {
                xhr.abort();
            }
            map = {};
            objects = [];
            xhr = $.ajax({
            url: '<?php echo base_url();?>cashback/getcitys_listjson/'+query,
            type: 'POST',
            dataType: 'JSON',
            data: 'query=' + query, 
            success: function(data) 
            {
                $.each(data, function (i, object)
                {
                    map[object.city_name] = object;
                    objects.push(object.city_name);
                });
                process(objects);
            }
            });
        }
        });
    });
</script>
