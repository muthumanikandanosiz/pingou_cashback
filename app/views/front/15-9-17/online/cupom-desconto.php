<?php include('storeheader.php');?>
<style type="text/css">
  .storeblkmain{background: none;}
</style>
<?php 
$pagename = $this->uri->segment(1);
if($this->input->get('ref'))
{
  $random_ref=$this->input->get('ref');
  $this->session->set_userdata('ses_random_ref',$random_ref);
}


//echo "<Pre>";print_r($store_details);exit;
$ratingRow         = $this->db->query("SELECT rating_number, FORMAT((total_points / rating_number),1) as average_rating FROM post_rating WHERE post_id = '$store_details->affiliate_id' AND status = 1")->row();
//$store_names       = $store_details->affiliate_url;
$store_names       = $store_details->affiliate_name;
$store_namesnew    = $store_details->affiliate_url;

//$expiry_cash_ex_id = $this->session->userdata('expiry_cash_ex_id');
if($expiry_cash_ex_id)
{
  $newcashback_ex_id = $expiry_cash_ex_id;
  $cashback_details  = $this->session->userdata('link_name');
  //$expiry_dates      = $ex_expiry_dates;
}
else
{
  $newcashback_ex_id = $this->session->userdata('cash_ex_id');
  $cashback_details  = $this->session->userdata('link_name');
  $expiry_dates      = $this->session->userdata('expiry_dates');
}



$cashback_web      = $this->front_model->shortcutdetails($this->session->userdata('cashback_web'));
$affiliate_urls    = $this->session->userdata('affiliate_urls');
//$expiry_dates      = strtotime($expiry_dates); 
$tday_dates        = strtotime(date('Y-m-d'));
$cash_query        = $this->db->query("SELECT * from cashback_exclusive where id='$newcashback_ex_id'")->row();
$cashback_type     = $cash_query->cashback_type;

if($expiry_cash_ex_id)
{
  $ex_expiry_dates = $cash_query->expirydate;
  $expiry_dates    = strtotime($ex_expiry_dates); 
}
else
{
  $expiry_dates    =  $expiry_dates;
}

//echo "<pre>";print_r($cash_query); exit;
/*New code for shortcut details for cashback exclusive 28-8-17*/
$ex_cashback_percentage  = $this->front_model->shortcutdetails($cash_query->cashback_web);
$ex_old_cashback_content = $this->front_model->shortcutdetails($cash_query->old_cashback);
$ex_notify_desk          = $this->front_model->shortcutdetails($cash_query->desk_notification);
$exc_notify_mobile       = $this->front_model->shortcutdetails($cash_query->mob_notification);
/*end 28-8-17*/


$firstname    = $userdetails->first_name;
$lastname     = $userdetails->last_name;
$emailid      = $userdetails->email;
$refer_code   = $userdetails->random_code;
if($refer_code == '')
{
  $refer_code = '';
}

$date     = date('Y-m-d');
$newdate  = explode('-',$date);
$year     = $newdate[0];
$month    = $newdate[1];
$day      = $newdate[2];
$newday   = $day;
$off_year = date('y');
$local_month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); 
$braz_months_names = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
$dt = date('F');
$current_month     = str_ireplace($local_month_names, $braz_months_names, $dt);

$get_cat_type        = $this->front_model->referal__category();
$get_cat_details     = $this->front_model->get_referral_settings($get_cat_type);

$type_one_percentage = $get_cat_details->ref_cashback;
$type_one_days       = $get_cat_details->valid_months;
$type_two_amount     = $get_cat_details->ref_cashback_rate;  
$type_three_amount   = $get_cat_details->ref_cashback_rate_bonus;
$type_three_users    = $get_cat_details->friends_count;
$unique_bonus        = str_replace('.', ',',$get_cat_details->category_bonus_amount);

if($user_id == '')
{
  $ref_id  = $this->session->userdata('ses_random_ref'); 
  if($ref_id!='')
  {  
    //$ref_id  = $_REQUEST['ref'];
    
    $this->db->where('random_code',$ref_id);
    $user_details = $this->db->get('tbl_users');
    if($user_details->num_rows() > 0)
    {
      $userdetails       = $user_details->row();
      $ref_cat_type      = $userdetails->referral_category_type;

      $get_cat_detailss    = $this->front_model->get_referral_settings($ref_cat_type);
      $get_future_category = $get_cat_detailss->new_ref_cat_types;
      $new_fut_catdetails  = $this->front_model->get_referral_settings($get_future_category);
      $unique_bonus        = str_replace('.', ',',$new_fut_catdetails->category_bonus_amount);
    }
  }
  else
  {
    $get_cat_detailss  = $this->front_model->get_referral_settings(1);
    $unique_bonus      = str_replace('.', ',',$get_cat_detailss->category_bonus_amount);
  }
}
else
{
  if($firstname =='' && $lastname =='')
  { 
    $emails    = explode('@', $emailid);
    $usernames = $emails[0];  
  }
  else
  {
    $usernames = ucfirst($firstname)." ".ucfirst($lastname);
  }
}




$affiliate_cashback_type = $store_details->affiliate_cashback_type;
if($affiliate_cashback_type == 'Percentage')
{
  $cashbacks = $cashback_percentage."%";
}
else if($affiliate_cashback_type == 'Flat')
{
  $cashbacks = "R$ ".$cashback_percentage;
}
else
{
  $cashbacks = ""; 
}
 
/*New code for shortcut details 26-4-17*/
$get_coupondetails = $this->front_model->get_coupons_from_store($store_details->affiliate_name);
$count_act_coupons = count($get_coupondetails);
$short_data = array(
  '###STORE-NAME###'=>$store_details->affiliate_name,
  '###CASHBACK###'=>$cashbacks,
  '###TRACKING-SPEED###'=>$store_details->report_date,
  '###ESTIMATED-PAYMENT###'=>$store_details->retailer_ban_url,
  '###COUPON-NUMBER###' =>$count_act_coupons,
  '###DD###'    =>$newday,
  '###MM###'    =>$month,
  '###MONTH###' =>$current_month,
  '###YYYY###'  =>$year,
  '###YY###'    =>$off_year,
  '###STORE-IMG-ONE###'  =>$this->front_model->get_img_url()."uploads/affiliates/".$store_details->store_one_img,
  '###STORE-IMG-TWO###'  =>$this->front_model->get_img_url()."uploads/affiliates/".$store_details->store_two_img,
  '###STORE-IMG-THREE###'=>$this->front_model->get_img_url()."uploads/affiliates/".$store_details->store_three_img,
  '###STORE-IMG-FOUR###' =>$this->front_model->get_img_url()."uploads/affiliates/".$store_details->store_four_img,
  '###STORE-IMG-FIVE###' =>$this->front_model->get_img_url()."uploads/affiliates/".$store_details->store_five_img,
  '###STORE-IMG-SIX###'  =>$this->front_model->get_img_url()."uploads/affiliates/".$store_details->store_six_img,
  '###REFERRAL-PARAMETER###'=> $refer_code,
  '###Type-ONE###'=>$type_one_percentage,
  '###Type-ONE-days###'=>$type_one_days,
  '###Type-TWO###'=>$type_two_amount,
  '###Type-THREE###'=>$type_three_amount,
  '###Type-THREE-number-of-users###'=>$type_three_users,
  '###UNIQUE-BONUS###' => $unique_bonus,
  '###USER-NAME###' => $usernames
  );

$cashbacks = strtr($cashbacks,$short_data); 
/*End 26-4-17*/





if($getadmindetails[0]->storecover_type =='coverimage')
{
  if($getadmindetails[0]->store_back_img_settings =='zoomimage')
  {
    ?>
    <section class="wow fadeInDown storeblkmain" style="background: rgba(0, 0, 0, 0) url(<?php echo $this->front_model->get_img_url();?>uploads/adminpro/<?php echo $getadmindetails[0]->storecover_image;?>) repeat-y scroll left center / cover;">
    <?php
  }
  else
  {
    ?>
    <section class="wow fadeInDown storeblkmain" style="background: rgba(0, 0, 0, 0) url(<?php echo $this->front_model->get_img_url();?>uploads/adminpro/<?php echo $getadmindetails[0]->storecover_image;?>);">
    <?php 
  }
}
if($getadmindetails[0]->storecover_type =='covercolor')
{
  ?>
  <section class="wow fadeInDown storeblkmain" style="background-color:<?php echo $getadmindetails[0]->storecover_color; ?> !important;">
  <?php
}
?>
  <div class="container">
    <div class="store-normalblk store-respblk">
      <div class="col-md-12 col-sm-12 col-xs-12 turn-shopblk clearfix">
        <div class="col-sm-3 col-xs-12 pd-left-0 pd-right-0">
          <?php 
          if($user_id=="")
          {
            ?>  
            <img src="<?php echo $this->front_model->get_img_url();?>uploads/lojas/<?php echo $store_details->cover_photo;?>" alt="cupom-desconto-<?php echo $store_details->affiliate_name;?>" class="img-responsive shopdetblk1-logo"> 
            <?php
            $anew = '<a href="#myModal" data-toggle="modal" class="btn-loja-pos">';
          }
          else
          {
            ?>
            <img src="<?php echo $this->front_model->get_img_url();?>uploads/lojas/<?php echo $store_details->cover_photo;?>" alt="cupom-desconto-<?php echo $store_details->affiliate_name;?>" class="img-responsive shopdetblk1-logo"> 
            <?php
            $anew = '<a href="'.$site_url.'" target="_blank" class="btn-loja-pos">';
          }   
          ?>
        </div>
        <div class="col-md-4 col-sm-5 col-xs-12 pd-left-0 pd-right-0">
          <h4>
            <?php 

            if($cash_query->cashback_type)
            { 
              if($store_names == $cash_query->store_name) //affiliate_urls
              { 
                if($cash_query->cashback_type == 'Percentage')
                {
                  $cppercentage = $ex_cashback_percentage."%"; //cash_query->cashback_web 24-8-17
                }
                else
                {
                  $cppercentage = "R$ ".$ex_cashback_percentage; //cash_query->cashback_web
                }
              } 
              else
              {
                if($store_details->affiliate_cashback_type=="Percentage")
                {
                   
                  $cppercentage = $cashback_percentage."%";
                }
                else if($store_details->affiliate_cashback_type=="Flat")
                {
                   $cppercentage = "R$ ".$cashback_percentage;
                }
                else
                {
                  $cppercentage = '';
                }
              } 
            }
            else
            {
              if($store_details->affiliate_cashback_type!='')
              {
                if($store_details->affiliate_cashback_type=="Percentage")
                {
                   
                  $cppercentage = $cashback_percentage."%";
                }
                else if($store_details->affiliate_cashback_type=="Flat")
                {
                   $cppercentage = "R$ ".$cashback_percentage;
                }
                else
                {
                  $cppercentage = '';
                }
              } 
            }   
            /*if($cashback_percentage!='')
            {
              if($store_details->affiliate_cashback_type=="Percentage")
              {
                if($store_names == $affiliate_urls)
                { 
                  if($tday_dates <= $expiry_dates)
                  {
                    if($cashback_type == 'Flat')
                    {
                      $cppercentage = "R$ ".$cashback_web;
                    }
                    else
                    {
                      $cppercentage = $cashback_web."%";
                    }
                  }
                  else
                  {
                    $cppercentage = $cashbacks;
                  } 
                }
                else
                {
                  $cppercentage = $cashbacks;
                }
              }
              else
              {
                $cppercentage = $cashbacks;
              }
              ?> Ganhe <?php echo str_replace('.', ',', $cppercentage); ?> de volta <?php 
            }*/
            ?> Ganhe <?php echo str_replace('.', ',', $cppercentage); ?> de volta <?php 
            if(!empty($newcashback_ex_id))
            { 
              if($cash_query->old_cashback!='')
              { 
                if($store_names == $cash_query->store_name) //$affiliate_urls
                {
                  ?>
                  <span>(<?= $ex_old_cashback_content;?>)</span> <!-- cash_query->old_cashback 24-8-17 -->
                  <?php
                }
                else
                {
                  ?>
                  <span>(<?= strtr($old_cashback_content,$short_data); //store_details->old_cashback ?>)</span>
                  <?php 
                }  
              }   
            }
            //if($store_details->old_cashback!='')
            else
            { 
              if($store_details->old_cashback!='')
              {
                ?>
                <span>(<?= strtr($old_cashback_content,$short_data); //store_details->old_cashback ?>)</span>
                <?php
              }  
            }
            ?>
          </h4>
        </div>
        <?php
        $affid =  $store_details->affiliate_id; 
        ?>
        <div class="col-sm-4 col-xs-12">
          <?php 
          if($coupon_id == '')
          {
            if($user_id == '')
            {
              ?>
              <button data-toggle="modal" href="#login" class="turn-btn"> Ativar e ir para <?php echo $store_details->affiliate_name;?> </button>
              <?php 
            }
            else
            { 
              ?>
                <button class="turn-btn" onclick="window.open('<?php echo base_url().'ir-loja/'.$affid;?>');">Ativar e ir para <?php echo $store_details->affiliate_name;?></button>
                <?php
              }
          }
          else
          { 
            if($user_id == '')
            {
              ?>
              <button data-toggle="modal" href="#login" class="turn-btn"> Ativar e ir para <?php echo $store_details->affiliate_name;?> </button>
              <?php 
            }
            else
            { 
              ?>
              <button class="turn-btn" onclick="window.open('<?php echo base_url().'ir-loja/'.$affid;?>');">Ativar e ir para <?php echo $store_details->affiliate_name;?></button>
              <?php
            }
          }
          ?>
        </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12 cls-shopdetblk1">
        <div class="col-md-offset-3 col-sm-offset-0 col-xs-offset-0 col-md-9 col-sm-12 col-xs-12 pd-left-0 pd-right-0 cls-substorehead">
          <?php
          if($cash_query->desk_notification)
          { 
            if($store_names == $cash_query->store_name) //$affiliate_urls
            {
              if($expiry_dates < $tday_dates)
              {
                ?>
                <div class="hidden-xs">
                <?php echo $cash_query->expiry_notify; ?>
                </div>
                <?php
              }
              else
              {
                ?>
                <div class="hidden-xs"><p><?= $ex_notify_desk; //cash_query->desk_notification 24-8-17 ?></p></div>
                <?php
              }
            }  
            else
            {
              ?>
              <div class="hidden-xs"><p><?= strtr($notify_desk,$short_data); //cash_query->desk_notification 24-8-17 ?></p></div>
              <?php   
            }
          }  
          else
          {
            ?>
            <div class="hidden-xs"><?= strtr($notify_desk,$short_data); //store_details->notify_desk24-8-17 ?></div>
            <?php 
          }  
          
          /*} 
          else
          {
            if($notify_desk<>'') //store_details->notify_desk24-8-17
            {
              if($store_names == $affiliate_urls)
              {
                if($expiry_dates < $tday_dates)
                {
                  ?>
                  <div class="hidden-xs">
                  <?php echo $cash_query->expiry_notify; ?>
                  </div>
                  <?php
                }
                else
                { 
                  ?>
                  <div class="hidden-xs"><?= strtr($notify_desk,$short_data); //store_details->notify_desk24-8-17 ?></div>
                  <?php
                } 
              }
              else
              { 
                ?>
                <div class="hidden-xs">
                <?= strtr($notify_desk,$short_data); //store_details->notify_desk; 24-8-17 ?>
                </div>
                <?php
              } 
            }
          }*/

          if($cash_query->mob_notification)
          {
            if($store_names == $cash_query->store_name) //$affiliate_urls
            {
              ?>
              <div class="hidden-md hidden-sm"><p><?= $exc_notify_mobile; //cash_query->mob_notification 24-8-17 ?></p></div>
              <?php 
            }
            else
            {
              ?>
              <div class="hidden-md hidden-sm">
                <?= strtr($notify_mobile,$short_data); //store_details->notify_mobile ?>
              </div>
              <?php
            }  
          }
          else
          {
            if($notify_mobile <>'') //store_details->notify_mobile 24-8-17
            {
              ?>
              <div class="hidden-md hidden-sm">
                <?= strtr($notify_mobile,$short_data); //store_details->notify_mobile 24-8-17 ?>
              </div>
              <?php
            }
          }  

          if($notify_desk == '' ||  $notify_mobile == '') //store_details->notify_desk   store_details->notify_mobile 24-8-17
          {
            ?>
            <style type="text/css">
            @media screen and (min-width: 992px){.turn-shopblk .shopdetblk1-logo{bottom:-55px;}}                
            </style>
            <?php 
          }
          ?>
        </div>          
        <div class="col-md-offset-3 col-sm-offset-0 col-xs-offset-0 col-md-2 col-sm-3 col-xs-12 pd-left-0 pd-right-0">
          <div class="cls-substorehead">
            <p>
              <?php 
              if($cashback_percentage!='')
              {
                ?>
                <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal_terms" style="color:#333 !important;">Regras e exceções</a> 
                <?php
              }
              ?>
            </p>
            <p class="hidden-xs">Tracking Speed : <?php echo strtr($store_details->report_date,$short_data); ?> days</p>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12 pd-left-0 pd-right-0">
          <div class="cls-substorehead">
            <p>
              <a style="color:#333 !important;" href="javascript:void(0)" data-toggle="modal" data-target="#myModal_how_to_get">  Como funciona o dinheiro de volta? </a>
            </p>
            <p class="hidden-xs">Estimated Payment: <?php echo strtr($store_details->retailer_ban_url,$short_data);?> weeks</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-5 col-xs-12 share-blk pd-left-0 pd-right-0">
          <div class="cls-substorehead">
            <p>Share and Earn :</p>
            <?php 
            if($user_id!='')
            {
              $ref_code = "?ref=".$userdetails->random_code;
            }
            else
            {
              $ref_code = '';
            }
            ?>
            <ul class="list-inline list-unstyled">
              <li class="fb"><a href="<?php echo 'https://www.facebook.com/sharer.php?s=100&p[url]='.base_url();?>cupom-desconto/<?php echo $store_details->affiliate_url.$ref_code;?>" class="hvr-pulse-shrink"> <i class="fa fa-facebook-square"></i> Share </a></li>
              <li class="gplus"><a href="#" class="hvr-pulse-shrink"> <span class="icon-google-plus"></span>Compartilhar</a></li>
              <li class="twit"><a href="#" class="hvr-pulse-shrink"> <i class="fa fa-twitter"></i> Tweet </a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
$affiliate_desc     = str_ireplace('<p>','',$store_details->affiliate_desc); 
$new_affiliate_desc = str_ireplace('</p>','',$affiliate_desc);
?>
<section class="wow fadeInDown storeblkmaincont" itemtype="http://schema.org/CollectionPage" itemscope="">
    <div class="container">
      <?php
      if($user_id !='')
      {
        ?>
        <div class="col-md-9 col-sm-12 col-xs-12 fn center-block">
        <?php
      }
      else
      {
        ?>
        <div class="col-md-9 col-sm-12 col-xs-12">
        <?php
      }
      ?>
        <div class="comm-store-contblk">
          <?php 
           
          /*New code for store name title (dynamic) 20-4-17*/
          $page_title = strtr($store_details->page_title,$short_data);
          /*$data = array(
            '###STORE-NAME###' => $store_details->affiliate_name);
          $page_titles       = strtr($page_title,$data);*/
          /*End 20-4-17*/
          ?> 
          <h2><?php echo $page_title;?></h2>
          <?php
          $store_banner_details    = $this->db->query("SELECT * from `sales_banner_status` where sales_banner_id =1")->row();
          $store_name              = $store_banner_details->store;
          $store_top_status        = $store_banner_details->store_top_status;
          $store_bot_status        = $store_banner_details->store_bottom_status; 
          $category_type           = $userdetails->referral_category_type;
          $bonus_status            = $userdetails->bonus_benefit;
          $login_type              = $userdetails->app_login;
          $page_url                = '?utm_source=pingou&utm_medium=top-banner-'.$pagename.'-page&utm_campaign=';
          
          ?>
          <br>
          <?php 
          if($user_id =='')
          {
            ?>
            <div class='forum-content'>
              <div class='comments-space'><?php echo strtr($new_affiliate_desc,$short_data);?></div>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
      <div class="clearfix mar-bot-31">
        <?php
        if($user_id !='')
        {
          ?>
          <div class="col-md-9 col-sm-12 col-xs-12 fn center-block">
          <?php
        }
        else
        {
          ?>
          <div class="col-md-9 col-sm-12 col-xs-12"> 
          <?php
        }
        ?>
          <div class="comm-store-contblk">
              <?php
              $affid   =  $store_details->affiliate_id;
              $affname =  $store_details->affiliate_name;
              if($store_coupons!="")
              {
                ?>
                <div class="cls_store_lisr_on">
                  <div id='active_sampleajax'>
                    <?php
                    $kt=1;
                    $mi=1001;
                    foreach($store_coupons as $coupons)
                    {
                      $coupon_id    = $coupons->coupon_id;
                      $start_date   = $coupons->start_date;
                      $expiry_date  = $coupons->expiry_date;
                      $exp          = date('m/d/Y',strtotime($expiry_date));
                      $date         = DateTime::createFromFormat('m/d/Y', date('m/d/Y'));
                      $date1        = date_create($date->format('Y-m-d'));
                      $date         = DateTime::createFromFormat('m/d/Y', $exp);
                      $date2        = date_create($date->format('Y-m-d'));
                      $diff         = date_diff($date1,$date2);
                      $coupondate   =  $diff->format("%a dias");
                      $coupon_type  = $coupons->type;  
                      ?>
                      <div class="cls-storelistcontblk1">
                        <div class="cls-storelisthead1 clearfix">
                          <div class="col-md-11 col-sm-11 col-xs-10">
                            <?php
                            if($user_id=="")
                            { 
                              $hres      = '<a cuptype="'.$coupon_type.'" cupid="'.$coupon_id.'" href="#entrars" id="cid_'.$coupon_id.'" data-toggle="modal" class="newclr newnew1">';
                              $ss_button = '<a href="'.base_url().'cupom-desconto/'.$store_details->affiliate_name.'?oid='.$coupon_id.'" target="_blank;" data-id="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'" showcoupon_id="'.$coupon_id.'" class="btn btn-blue dsf">Ver cupom</a>';
                            }
                            else
                            {
                              if($coupon_type == 'Promotion')
                              {
                                $hres = '<a class="newclr" href="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'"  target="_blank">';
                              }
                              else
                              {
                                $hres =  '<a class="newclr" href="'.base_url().'cupom-desconto/'.$store_details->affiliate_name.'?oid='.$coupon_id.'" target="_blank" data-id="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'" show_id="'.$coupon_id.'">'; 
                              }
                              $ss_button = '<a href="'.base_url().'cupom-desconto/'.$store_details->affiliate_name.'?oid='.$coupon_id.'" target="_blank" data-id="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'" show_id="'.$coupon_id.'" class="btn btn-blue after_login"> Ver cupom LGN </a>';   
                            }
                            ?>
                            <h5>
                              <?php 
                                echo  $coupons->title;
                                if($cashback_percentage!="")
                                {
                                  if($store_details->affiliate_cashback_type=="Percentage")
                                  {
                                    if($store_names == $cash_query->store_name) //$affiliate_urls
                                    {
                                      //if($tday_dates <= $expiry_dates)
                                      //{
                                        $cppercentage = $ex_cashback_percentage."%"; //$cashback_web
                                      //} 
                                    }
                                    else
                                    {
                                      $cppercentage = $cashback_percentage."%";
                                    }
                                  }
                                  else
                                  {
                                    $cppercentage = "R$. ".$cashback_percentage;
                                  } 
                                }
                                if($cppercentage!="")
                                {
                                  $admindetails = $this->front_model->getadmindetails_main();
                                  echo " + ".$cppercentage." de volta " ;
                                }
                              ?>
                            </h5>
                            <?php $countchar = strlen($coupons->description);?>
                          </div>
                          <?php if(($coupons->coupon_options == 1) || ($coupons->coupon_options == 2))
                          {
                            ?>
                            <div class="col-md-1 col-sm-1 col-xs-2">
                              <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAeCAYAAAAo5+5WAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3BJREFUeNqMlnmIT1EUx+f3jLFkb2xDlrGF7GTfdzNmhuxM2SmKyBaKlCWUxlaWMn+IxNBkYiT7LmTJvmdJiMjO8Dn6vrq93pNbn7nzfu++7zvn3HPOfbH+OXfiIkYR6As9oQdUBw+ew3E4DPtzB9R9H/ZwLEJ4AsyDmvAGzsMLKICK0BKqwjtYAVm84Isr4AUEy0AubILHstYsTYWJMBkGQDK0hkOwHE6m7b1bK0q4PByQyCjoJnc/h3j0Ay7AMIXLXn4M8eQwYbOyDaTB9rj/HITgIFN3KA17EC/mCo+ADJhhGxKhMR5OQZ0Q8WuyvinM9zcvgfk2PIUuIYIW17HQQobYJh6TV/mI/vIXYm0202DbA1vYXru/LER0HGyEVo53SfJwJRQKrF8CFop0Tzn6Ec6GCI/WbBs1Fb7DFDgKC3XthuQ+k4VlYDx/esNV+BAi7FtUBTpa6JRiG/6xn6bV0VOqvIpYlOUID4XCcBFyFJ6wYXtVyoR/y5KwsRM6yPUPThFZkeRDjSizPb2hQsT9kvBIxTIdvsF6FUjZiCyqZkZ4enMTiQTjuw6smayC+vATboKfYt9DhC2X73oqYxPtGlhQTpVYAmbCLCgui4vqBfnuA+SxvbwR7PKUZg9hTkD4tYRnQ55iHNPazdAP3gaesaqzLrffk1sLoJ1K2h3vVAipundKnlmnexKwNoVpJKwmn1/51bTDGoj9aFUTsZFblctPgzcQbS6NS7A02N3GwAnYrVKO/U93Q7Q/0xGwkyTdb/iusJX1cPgEW+AyTIvI1VJgggd1MNh+dEb0ub8gPvBAW4legcWwRpgnt+RFkoqmjDbP1tWDZ/86mjIUCmuJDRXvLKXjJBiiYylHp0xtrFykAzbFFXItbgCV1MniVGW5IqYsyFZXC448tdJ9YRYPUzoVhDzYB65DM0gIuW/PJbKRSUHhwjrSt0Vs/iBZeg96hRxN1jvOQWZQ2PLzgdztIJca615llfdlhcXN89ZYOVz/m1EpXCe4whnKw7UwF746b09X87ZxWllRUdd2qkxDbI96hBnWyRVOVF84p+M/U/G03wcqU/xudlZ9wjxJJAxtlJKZ8rKFmxVjlAX+KJB1dgS9hBvOvd36/Er2uxviJ/U11Ng/jeKd1AoOS60zqkZ33FbjStOHSvD74u/4I8AAS4nzKRLPcIQAAAAASUVORK5CYII=" alt="currency" class="img-responsive center-block">
                            </div>
                            <?php 
                          }
                          ?>
                        </div>
                        <div class="cls-storelistcont1 clearfix">
                          <div class="col-md-8 col-sm-8 col-xs-12">
                            <div id="forum-contents<?php echo $kt;?>" class='forum-contents'>
                              <div id="comments-spaces<?php echo $kt;?>" class='comments-spaces'>
                                <?php
                                  $osiz_des_length = strlen($coupons->description); 
                                  echo $substrings = substr($coupons->description, 0,70); 
                                  if($osiz_des_length > 70) 
                                  {
                                    echo "
                                    <span class='plus_click' style='float:right;font-size: 16px;position: absolute; cursor: pointer; right:10px; top:0px;'>
                                      <i class='fa fa-plus-square'></i>
                                      <i class='fa fa-minus-square' style='display:none;'></i>
                                    </span>";
                                  }
                                  ?>
                                  <div class="full_desc" style="display:none;text-align:unset !important;"><?php echo substr($coupons->description, 70,$osiz_des_length);?></div>
                              </div>
                            </div>
                            <?php 
                            if($coupons->cashback_description!="")
                            {
                              ?>
                              <div class = "cls-store-grp">
                                  <label> <span>Dica : </span><?php echo $coupons->cashback_description; ?></label>      
                              </div>
                              <?php
                            }
                            ?>
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12 pd-left-0 pd-right-0">
                            <p class="text-right mt15"> 
                              <?php 
                              if($user_id!="")
                              {
                                if($coupons->type == 'Promotion')
                                {?>
                                  <a href="<?php echo base_url().'ir-loja/'.$affid.'/'.$coupon_id;?>" class="after_login"  target="_blank"><button class="btn-danger btn-coupon btn-couponactiv center-block"> Ativar desconto </button></a>
                                  <?php
                                }
                                else
                                {
                                ?>  
                                  <a class="after_login" href="<?php echo base_url();?>cupom-desconto/<?php echo $store_details->affiliate_url; ?>?oid=<?php echo $coupon_id;?>" target="_blank" data-id="<?php echo base_url();?>ir-loja/<?php echo $affid;?>/<?php echo $coupon_id;?>" show_id="<?php echo $coupon_id;?>">
                                  <button class="btn-danger btn-coupon center-block btn1_<?php echo $coupon_id;?>"><small class="hvr-curl-bottom-right"> Ver cupom </small><span><?php echo $coupons->code;?></span></button>
                                  </a>
                                <?php
                                }
                              }
                              else
                              {
                                if($coupons->type == 'Promotion')
                                {
                                  ?>
                                  <?php echo $hres;?><button class="btn-danger btn-coupon btn-couponactiv center-block">Ativar desconto</button></a>
                                  <?php
                                }
                                else
                                {
                                  echo $hres;?><button class="btn-danger btn-coupon center-block"><small class="hvr-curl-bottom-right">Usar cupom </small><span><?php echo $coupons->code;?></span> </button></a>
                                  <?php
                                }
                              }
                              ?>
                            </p>
                            <br>                
                          </div>   
                        </div>
                      </div>
                      <!--New code for schema Micro format (SEO realted code) 18-3-17-->
                      <!-- Event markup -->
                      <script type="application/ld+json">
                      {
                        "@context"   : "http://schema.org",
                        "@type"      : "SaleEvent",
                        "name"       : "<?php echo $coupons->title;?>",
                        "url"        : "<?php echo base_url();?>cupom-desconto/<?php echo $store_details->affiliate_url; ?>?oid=<?php echo $coupon_id;?>",
                        "startDate"  : "<?php echo $start_date; ?>",
                        "endDate"    : "<?php echo $expiry_date;?>",
                        "description": "<?php echo $coupons->description;?>",
                        "image"      : "",
                        "location"   : 
                        {
                          "@type"  : "Place",
                          "name"   : "<?php echo $coupons->offer_name; ?>",
                          "url"    : "",
                          "address": "<?php echo $getadmindetails[0]->address; ?> , <?php echo $getadmindetails[0]->contact_number; ?>"
                        }
                        ,"offers": 
                        {
                          "@type": "AggregateOffer",
                          "lowPrice": "100",
                          "url": "<?php echo base_url();?>cupom-desconto/<?php echo $store_details->affiliate_url; ?>?oid=<?php echo $coupon_id;?>"
                        }
                      }
                      </script>
                      <!-- End 18-3-17 -->
                      <?php
                      $kt++;
                      $mi++;
                    }
                    ?> 
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                      <a id="active_more_button" href="javascript:void(0);" class="uppercase full-width btn btn-lg center-block">load more</a> 
                      <a id="active_more_button_null" style="display:none" class="uppercase full-width btn btn-lg center-block">Sorry! No more results found</a>
                    </div>
                  </div>
                <?php
              }
              ?>
              <br>
              <!-- Banner details Start -->
              <?php 
              if($user_id == '')
              {
                if($store_top_status == 1)
                {
                  $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
                  $imagename     = explode('.',$bannerdetails[0]->image_name);
                  $image_name    = $imagename[0];
                  ?>
                  <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                    <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
                  </a>
                  <?php 
                }
                if($store_top_status == 2)
                {
                  $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
                  $html_content  = $this->db->query("SELECT `not_log_html_settings` from `sales_funnel` where not_log_status !=0")->result();
                  ?>
                  <?php
                  echo strtr($html_content[0]->not_log_html_settings,$short_data); 
                }
                if($store_top_status == 3)
                {
                  $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0 order by RAND() limit 1")->result();
                  $imagename     = explode('.',$bannerdetails[0]->image_name);
                  $image_name    = $imagename[0];
                  ?>
                  <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                    <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
                  </a>
                  <?php 
                }
                if($store_top_status == 4)
                {
                  $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0")->result();
                  $html_content  = $this->db->query("SELECT `notlog_standard_html_settings` from `sales_funnel` where notlog_standard_status !=0")->result();
                  ?>
                  <?php
                  echo strtr($html_content[0]->notlog_standard_html_settings,$short_data);
                }
              }
              else
              {
                if(($bonus_status == 0) && ($login_type == 0))
                {
                  $conditiondetails = 'log_notusebonus_notuseapp';
                  $html_name        = 'log_notusebonus_notuseapp_html_settings';
                }
                if(($bonus_status == 1) && ($login_type == 0))
                {
                  $conditiondetails = 'log_usebonus_notuseapp';
                  $html_name        = 'log_usebonus_notuseapp_html_settings';
                }
                if(($bonus_status == 0) && ($login_type == 1))
                {
                  $conditiondetails = 'log_notusebonus_useapp';
                  $html_name        = 'log_notusebonus_useapp_html_settings';
                }
                if(($bonus_status == 1) && ($login_type == 1))
                {
                  $conditiondetails = 'log_usebonus_useapp';
                  $html_name        = 'log_usebonus_useapp_html_settings';
                }

                if($store_top_status == 1)
                {
                  $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='$conditiondetails' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
                  $imagename     = explode('.',$bannerdetails[0]->image_name);
                  $image_name    = $imagename[0];
                  if($bannerdetails)
                  {
                    ?>
                    <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                      <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
                    </a>
                    <?php 
                  }  
                }
                if($store_top_status == 2)
                {
                  $conditiondetails = $conditiondetails.'_status'; 
                  $html_content     = $this->db->query("SELECT $html_name from `sales_funnel` where $conditiondetails!=0")->row($html_name);
                  echo strtr($html_content,$short_data);
                }
                if($store_top_status == 3)
                {
                  $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='log_standard' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
                  $imagename     = explode('.',$bannerdetails[0]->image_name);
                  $image_name    = $imagename[0];
                  if($bannerdetails)
                  {
                    ?>
                    <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                      <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
                    </a>
                    <?php     
                  } 
                }
                if($store_top_status == 4)
                {              
                  $html_content     = $this->db->query("SELECT `log_standard_html_settings` from `sales_funnel` where log_standard_status!=0")->row('log_standard_html_settings');
                  echo strtr($html_content,$short_data);     
                } 
              }
              /*new code for Top banners view count update details 20-3-17*/
              $banner_id   = $bannerdetails[0]->banner_id;
              if($banner_id!='')
              {
                $getbanner   = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_id`=$banner_id AND `banner_status`=1")->row();
                $view_counts = $getbanner->view_counts;
                $add_count   = $view_counts + 1;

                $data = array(
                  'view_counts' => $add_count,
                  );
                $this->db->where('banner_id',$banner_id);
                $this->db->where('banner_status',1);
                $update = $this->db->update('sales_funnel_banners',$data);
              }
              ?> 
              <!--End 20-3-17  -->
              <!-- Banner details End -->
              <br>
              <?php
              $datass = array(
                '###STORE-NAME###'=>$store_details->affiliate_name,
                '###CASHBACK###'=>$cashbacks,
                '###TRACKING-SPEED###'=>$store_details->report_date,
                '###ESTIMATED-PAYMENT###'=>$store_details->retailer_ban_url,
                '###COUPON-NUMBER###' =>$count_act_coupons,
                '###DD###'    =>$newday,
                '###MM###'    =>$month,
                '###MONTH###' =>$current_month,
                '###YYYY###'  =>$year,
                '###YY###'    =>$off_year,
                '###STORE-IMG-ONE###'  =>$this->front_model->get_img_url()."uploads/affiliates/".$store_details->store_one_img,
                '###STORE-IMG-TWO###'  =>$this->front_model->get_img_url()."uploads/affiliates/".$store_details->store_two_img,
                '###STORE-IMG-THREE###'=>$this->front_model->get_img_url()."uploads/affiliates/".$store_details->store_three_img,
                '###STORE-IMG-FOUR###' =>$this->front_model->get_img_url()."uploads/affiliates/".$store_details->store_four_img,
                '###STORE-IMG-FIVE###' =>$this->front_model->get_img_url()."uploads/affiliates/".$store_details->store_five_img,
                '###STORE-IMG-SIX###'  =>$this->front_model->get_img_url()."uploads/affiliates/".$store_details->store_six_img,
                '###REFERRAL-PARAMETER###'=> $refer_code,
                '###Type-ONE###'=>$type_one_percentage,
                '###Type-ONE-days###'=>$type_one_days,
                '###Type-TWO###'=>$type_two_amount,
                '###Type-THREE###'=>$type_three_amount,
                '###Type-THREE-number-of-users###'=>$type_three_users,
                '###UNIQUE-BONUS###' => $unique_bonus,
                '###USER-NAME###' => $usernames
                );
              if($user_id=='')
              { 
                if(!empty($expiry_coupons))
                {
                  ?> 
                  <h2>Unreliable Coupons</h2>      
                  <div class="cls_store_lisr_on">
                    <div id='sampleajax'>
                      <?php
                      $kt=1;
                      $mi=1001;
                      //echo "<pre>"; print_r($expiry_coupons); exit;
                      foreach($expiry_coupons as $coupons)
                      {

                        $coupon_id   = $coupons->coupon_id;
                        $expiry_date = $coupons->expiry_date;
                        if(($expiry_date !='0000-00-00'))
                        {
                          $exp         = date('m/d/Y',strtotime($expiry_date));
                          $date        = DateTime::createFromFormat('m/d/Y', date('m/d/Y'));
                          $date1       = date_create($date->format('Y-m-d'));
                          $date        = DateTime::createFromFormat('m/d/Y', $exp);
                          $date2       = date_create($date->format('Y-m-d'));
                          $diff        = date_diff($date1,$date2);
                          $coupondate  = $diff->format("%a dias");
                        }  
                        ?>
                        <div class="cls-storelistcontblk1">
                          <div class="cls-storelisthead1 clearfix">
                            <div class="col-md-11 col-sm-11 col-xs-10">
                              <?php
                              if($user_id=="")
                              {
                                $hres =  '<a class="popup-text dsf newclr" href="'.base_url().'cupom-desconto/'.$store_details->affiliate_name.'?oid='.$coupon_id.'" target="_blank;" data-id="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'" showcoupon_id="'.$coupon_id.'">';
                                $ss_button = '<a href="'.base_url().'cupom-desconto/'.$store_details->affiliate_name.'?oid='.$coupon_id.'" target="_blank;" data-id="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'" showcoupon_id="'.$coupon_id.'" class="btn btn-blue dsf">Ver cupom</a>';
                              }
                              else
                              {
                                $hres =  '<a class="popup-text after_login newclr" href="'.base_url().'cupom-desconto/'.$store_details->affiliate_name.'?oid='.$coupon_id.'" target="_blank" data-id="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'" show_id="'.$coupon_id.'">';
                                $ss_button = '<a href="'.base_url().'cupom-desconto/'.$store_details->affiliate_name.'?oid='.$coupon_id.'" target="_blank" data-id="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'" show_id="'.$coupon_id.'" onclick="return addRow();" class="btn btn-blue after_login"> Ver cupom LGN </a>';    
                              }
                              ?>
                              <h5>
                                <?php //echo $hres;?><?php echo $coupons->title; ?> 
                                <?php
                                if($cashback_percentage!="")
                                {
                                  if($store_details->affiliate_cashback_type=="Percentage")
                                  {
                                    if($store_names == $affiliate_urls)
                                    {
                                      if($tday_dates <= $expiry_dates)
                                      {
                                        $cppercentage = $cashback_web."%";
                                      } 
                                    }
                                    else
                                    {
                                      $cppercentage = $cashback_percentage."%";
                                    }
                                  }
                                  else
                                  {
                                    $cppercentage = "Rs. ".$cashback_percentage;
                                  } 
                                }
                                if($cppercentage!="")
                                {
                                  $admindetails = $this->front_model->getadmindetails_main();
                                }
                                ?>
                              </h5>
                            </div>
                            <?php if(($coupons->coupon_options == 1) || ($coupons->coupon_options == 2))
                            {
                              ?>
                              <div class="col-md-1 col-sm-1 col-xs-2">
                              <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAeCAYAAAAo5+5WAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3BJREFUeNqMlnmIT1EUx+f3jLFkb2xDlrGF7GTfdzNmhuxM2SmKyBaKlCWUxlaWMn+IxNBkYiT7LmTJvmdJiMjO8Dn6vrq93pNbn7nzfu++7zvn3HPOfbH+OXfiIkYR6As9oQdUBw+ew3E4DPtzB9R9H/ZwLEJ4AsyDmvAGzsMLKICK0BKqwjtYAVm84Isr4AUEy0AubILHstYsTYWJMBkGQDK0hkOwHE6m7b1bK0q4PByQyCjoJnc/h3j0Ay7AMIXLXn4M8eQwYbOyDaTB9rj/HITgIFN3KA17EC/mCo+ADJhhGxKhMR5OQZ0Q8WuyvinM9zcvgfk2PIUuIYIW17HQQobYJh6TV/mI/vIXYm0202DbA1vYXru/LER0HGyEVo53SfJwJRQKrF8CFop0Tzn6Ec6GCI/WbBs1Fb7DFDgKC3XthuQ+k4VlYDx/esNV+BAi7FtUBTpa6JRiG/6xn6bV0VOqvIpYlOUID4XCcBFyFJ6wYXtVyoR/y5KwsRM6yPUPThFZkeRDjSizPb2hQsT9kvBIxTIdvsF6FUjZiCyqZkZ4enMTiQTjuw6smayC+vATboKfYt9DhC2X73oqYxPtGlhQTpVYAmbCLCgui4vqBfnuA+SxvbwR7PKUZg9hTkD4tYRnQ55iHNPazdAP3gaesaqzLrffk1sLoJ1K2h3vVAipundKnlmnexKwNoVpJKwmn1/51bTDGoj9aFUTsZFblctPgzcQbS6NS7A02N3GwAnYrVKO/U93Q7Q/0xGwkyTdb/iusJX1cPgEW+AyTIvI1VJgggd1MNh+dEb0ub8gPvBAW4legcWwRpgnt+RFkoqmjDbP1tWDZ/86mjIUCmuJDRXvLKXjJBiiYylHp0xtrFykAzbFFXItbgCV1MniVGW5IqYsyFZXC448tdJ9YRYPUzoVhDzYB65DM0gIuW/PJbKRSUHhwjrSt0Vs/iBZeg96hRxN1jvOQWZQ2PLzgdztIJca615llfdlhcXN89ZYOVz/m1EpXCe4whnKw7UwF746b09X87ZxWllRUdd2qkxDbI96hBnWyRVOVF84p+M/U/G03wcqU/xudlZ9wjxJJAxtlJKZ8rKFmxVjlAX+KJB1dgS9hBvOvd36/Er2uxviJ/U11Ng/jeKd1AoOS60zqkZ33FbjStOHSvD74u/4I8AAS4nzKRLPcIQAAAAASUVORK5CYII=" alt="currency" class="img-responsive center-block">
                              </div>
                              <?php 
                            }
                            ?>
                          </div>
                          <?php $countchar = strlen($coupons->description);?>
                          <div class="cls-storelistcont1 clearfix">
                            <div class="col-md-8 col-sm-8 col-xs-12">
                              <div id="forum-contents<?php echo $kt;?>" class='forum-contents'>
                                <div id="comments-spaces<?php echo $kt;?>" class='comments-spaces'>
                                  <?php
                                    $osiz_des_length = strlen($coupons->description); 
                                    echo $substrings = substr($coupons->description, 0,70); 
                                    if($osiz_des_length > 70) 
                                    {
                                      echo "
                                      <span class='plus_click' style='float:right;font-size: 16px;position: absolute; cursor: pointer; right:10px; top:0px;'>
                                        <i class='fa fa-plus-square'></i>
                                        <i class='fa fa-minus-square' style='display:none;'></i>
                                      </span>";
                                    }
                                    ?>
                                    <div class="full_desc" style="display:none;text-align:unset !important;"><?php echo substr($coupons->description, 70,$osiz_des_length);?></div>
                                </div>
                              </div>
                              <?php 
                              if($coupons->cashback_description!="")
                              { 
                                ?>
                                <div class = "cls-store-grp">
                                  <label> <span>Dica : </span><?php echo $coupons->cashback_description; ?></label>                                
                                </div>
                                <?php
                              }
                              ?>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 pd-left-0 pd-right-0">
                              <p class="text-right mt15">
                                <?php 
                                if($user_id=="")
                                {
                                  if($coupons->type=='Promotion')
                                  {
                                    ?>
                                    <a class="after_login" href="<?php echo base_url();?>cupom-desconto/<?php echo $store_details->affiliate_url; ?>?oid=<?php echo $coupon_id;?>" target="_blank;" data-id="<?php echo base_url();?>ir-loja/<?php echo $affid;?>/<?php echo $coupon_id;?>" show_id="<?php echo $coupon_id;?>"><button class="btn-danger btn-coupon btn-couponactiv center-block"> Ativar desconto </button></a>
                                    <?php
                                  }
                                  else
                                  {
                                    ?>  
                                    <a class="after_login" href="<?php echo base_url();?>cupom-desconto/<?php echo $store_details->affiliate_url; ?>?oid=<?php echo $coupon_id;?>" target="_blank" data-id="<?php echo base_url();?>ir-loja/<?php echo $affid;?>/<?php echo $coupon_id;?>" show_id="<?php echo $coupon_id;?>"><button class="btn-danger btn-coupon btn-couponactiv center-block"> Ativar desconto </button>
                                    <button class="btn btn-orange btn2_<?php echo $coupon_id;?>" style="display:none"><?php echo $coupons->code;?></button></a>
                                    <?php
                                  }
                                }
                                ?>
                              </p> 
                            </div>   
                          </div>
                        </div>
                        <?php
                        $mi++;
                        $kt++;
                      }  
                      ?>
                    </div>   
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <a id="more_button" href="javascript:void(0);" class="uppercase full-width btn btn-lg center-block">load more</a> 
                      <a id="more_button_null" style="display:none" class="uppercase full-width btn btn-lg center-block">Sorry! No more results found</a>
                    </div>
                  </div>
                  <?php
                }
                else
                {
                  ?>
                  <div class="store-title_red"><h2 class="mar-no mar-bot20">No Expiry Coupons available at this time</h2></div>
                  <br>
                  <?php
                }
                ?>
                <!-- Banner details Start -->
                <?php 
                
                if($user_id == '')
                {
                  if($store_bot_status == 1)
                  {
                    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
                    $imagename     = explode('.',$bannerdetails[0]->image_name);
                    $image_name    = $imagename[0];
                    ?>
                    <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                      <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
                    </a>
                    <?php 
                  }
                  if($store_bot_status == 2)
                  {
                    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
                    $html_content  = $this->db->query("SELECT `not_log_html_settings` from `sales_funnel` where not_log_status !=0")->result();
                    ?>
                    <?php
                    echo strtr($html_content[0]->not_log_html_settings,$datass); 
                  }
                  if($store_bot_status == 3)
                  {
                    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0 order by RAND() limit 1")->result();
                    $imagename     = explode('.',$bannerdetails[0]->image_name);
                    $image_name    = $imagename[0];
                    ?>
                    <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                      <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
                    </a>
                    <?php 
                  }
                  if($store_bot_status == 4)
                  {
                    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0")->result();
                    $html_content  = $this->db->query("SELECT `notlog_standard_html_settings` from `sales_funnel` where notlog_standard_status !=0")->result();
                    echo strtr($html_content[0]->notlog_standard_html_settings,$datass);
                  }
                }
                else
                {
                  if(($bonus_status == 0) && ($login_type == 0))
                  {
                    $conditiondetails = 'log_notusebonus_notuseapp';
                    $html_name        = 'log_notusebonus_notuseapp_html_settings';
                  }
                  if(($bonus_status == 1) && ($login_type == 0))
                  {
                    $conditiondetails = 'log_usebonus_notuseapp';
                    $html_name        = 'log_usebonus_notuseapp_html_settings';
                  }
                  if(($bonus_status == 0) && ($login_type == 1))
                  {
                    $conditiondetails = 'log_notusebonus_useapp';
                    $html_name        = 'log_notusebonus_useapp_html_settings';
                  }
                  if(($bonus_status == 1) && ($login_type == 1))
                  {
                    $conditiondetails = 'log_usebonus_useapp';
                    $html_name        = 'log_usebonus_useapp_html_settings';
                  }

                  if($store_bot_status == 1)
                  {
                    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='$conditiondetails' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
                    $imagename     = explode('.',$bannerdetails[0]->image_name);
                    $image_name    = $imagename[0];
                    if($bannerdetails)
                    {
                      ?>
                      <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                        <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
                      </a>
                      <?php 
                    }  
                  }
                  if($store_bot_status == 2)
                  {
                    $conditiondetails = $conditiondetails.'_status'; 
                    $html_content     = $this->db->query("SELECT $html_name from `sales_funnel` where $conditiondetails!=0")->row($html_name);
                    echo strtr($html_content,$datass);  
                  }
                  if($store_bot_status == 3)
                  {
                    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='log_standard' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
                    $imagename     = explode('.',$bannerdetails[0]->image_name);
                    $image_name    = $imagename[0];
                    if($bannerdetails)
                    {
                      ?>
                      <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                        <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
                      </a>
                      <?php     
                    } 
                  }
                  if($store_bot_status == 4)
                  {
                    $html_content     = $this->db->query("SELECT `log_standard_html_settings` from `sales_funnel` where log_standard_status!=0")->row('log_standard_html_settings');
                    echo strtr($html_content,$datass);     
                  }
                }
                /*new code for Top banners view count update details 20-3-17*/
               
                $banner_id   = $bannerdetails[0]->banner_id;
                if($banner_id!='')
                {
                  $getbanner   = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_id`=$banner_id AND `banner_status`=1")->row();
                  $view_counts = $getbanner->view_counts;
                  $add_count   = $view_counts + 1;

                  $data = array(
                    'view_counts' => $add_count,
                    );
                  $this->db->where('banner_id',$banner_id);
                  $this->db->where('banner_status',1);
                  $update = $this->db->update('sales_funnel_banners',$data);
                }
                ?> 
                <!--End 20-3-17  -->
                <!-- Banner details End -->
                <?php   
              }
              ?>  
              <br>
              <?php 
              if($user_id =='')
              {
                ?>
                <p><?php echo strtr($store_details->how_to_get_this_offer,$datass); ?></p>
                <?php
              }
              ?>
          </div>
        </div>
        <?php
        if($user_id=='')
        {
          //echo $store_id;
          ?>
          <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="store-sidebarcont comm-store-contblk">
            <h2>Rating</h2>
            <center>
              <input type="hidden" class="baseurls" value="<?php echo base_url();?>front/">
              <input name="rating"  value="0" id="rating_star" type="hidden" postID="<?php echo $store_details->affiliate_id; ?>" storename='<?php echo $store_details->affiliate_name;?>' />
              <div class="overall-rating">(Average Rating <span id="avgrat"><?php if($ratingRow->average_rating == '') { echo '0';} else { echo $ratingRow->average_rating; } ?>/5</span>
              - <span id="totalrat"><?php if($ratingRow->rating_number == ''){ echo '0';} else { echo $ratingRow->rating_number;} ?></span>  rating)</span>
              </div>
            </center>
          </div>

          <div class="store-sidebarcont comm-store-contblk">
            <h2>Lojas Relacionadas</h2>
            <input type="hidden" id="counts" name="cat_counts" value="<?php echo $storescount;?>">
            <input type="hidden" id="new_rel_counts" name="new_rel_counts" value="<?php echo $rel_storescount;?>">
            <?php
            if($storecategories)
            {
              $store_categories      = $storecategories;
              $split_categories      = explode(",",$store_categories);
              $counts                = count($split_categories);
              echo '<span id="myList">';
              foreach($split_categories  as $splitcategories)
              { 
                $ref_cat_details = $this->front_model->get_related_categories($splitcategories);
                
                /*new code for related stores name details 18-3-17*/
                $rel_store_id = $ref_cat_details->affiliate_id;
                
                if(($store_id % 2 == 0)  && ($rel_store_id % 2 == 0)) 
                {
                  $pre_store_names = 'Cupom Desconto';
                }
                if(($store_id % 2 != 0)  && ($rel_store_id % 2 == 0))
                {
                  $pre_store_names = 'Cupom';
                }
                if(($store_id % 2 == 0)  && ($rel_store_id % 2 != 0))
                {
                  $pre_store_names = 'Cupom';
                }
                if(($store_id % 2 != 0)  && ($rel_store_id % 2 != 0))
                {
                  $pre_store_names = 'Cupom Desconto';
                }

                /*end 18-3-17*/


                 $store_name      =  $ref_cat_details->affiliate_url;
                 $store_names     =  $ref_cat_details->affiliate_name;

                if($store_name!=$store_details->affiliate_url)
                { 
                  echo
                  '<ul class="list-unstyled">
                    <li>
                      <span class="links cl-effect-5">
                        <a href="'.base_url().'cupom-desconto/'.$store_name.'" class="clsreadblk">
                          <span data-hover="'.$pre_store_names." ".$store_names.'">'.$pre_store_names." ".$store_names.'</span>
                        </a>
                      </span>
                    </li>
                  </ul>';
                }   
              }
              echo '</span>';
              echo '<div id="loadMore">Ver Todas</div>
              <div id="showLess" style="display:none">Ver Menos</div>';     
            }
            else if($rel_store_detail)
            {
              $store_categories      = $rel_store_detail;
              $split_categories      = explode(",",$store_categories);
              $counts                = count($split_categories);
              echo '<span id="myList">';
              foreach($split_categories  as $splitcategories)
              { 
                $ref_cat_details = $this->front_model->get_related_categories($splitcategories);

                /*new code for related stores name details 18-3-17*/
                $rel_store_id = $ref_cat_details->affiliate_id;
                
                if(($store_id % 2 == 0)  && ($rel_store_id % 2 == 0)) 
                {
                  $pre_store_names = 'Cupom Desconto';
                }
                if(($store_id % 2 != 0)  && ($rel_store_id % 2 == 0))
                {
                  $pre_store_names = 'Cupom';
                }
                if(($store_id % 2 == 0)  && ($rel_store_id % 2 != 0))
                {
                  $pre_store_names = 'Cupom';
                }
                if(($store_id % 2 != 0)  && ($rel_store_id % 2 != 0))
                {
                  $pre_store_names = 'Cupom Desconto';
                }

                /*end 18-3-17*/

                $store_name      =  $ref_cat_details->affiliate_url;
                $store_names     =  $ref_cat_details->affiliate_name;
                if($store_name!=$store_details->affiliate_url)
                { 
                  echo
                  '<ul class="list-unstyled">
                    <li>
                      <span class="links cl-effect-5">
                        <a href="'.base_url().'cupom-desconto/'.$store_name.'" class="clsreadblk">
                          <span data-hover="'.$pre_store_names." ".$store_names.'">'.$pre_store_names." ".$store_names.'</span>
                        </a>
                      </span>
                    </li>
                  </ul>';
                }   
              }
              echo '</span>';
              echo '<div id="loadMore1">Ver Todas</div>
              <div id="showLess1" style="display:none">Ver Menos</div>';  
            }
            else
            {
              echo '<h5><center>Related stores not found</center></h5>';
            }
            ?>         
          </div>
          <?php $related_details   = $getadmindetails[0]->rel_store_details; ?>
          <div class="store-sidebarcont comm-store-contblk">
            <h2>Store details</h2>
            <span>
              <?php 
              if($related_details!='')
              {
                 echo ucfirst($related_details); 
              }
              else
              {
                echo ucfirst("<center><b>Records are Not found</b></center>");  
              }
              ?>
            </span>    
          </div>
          </div>
          <?php
        }
        ?>
      </div> 
    </div>
    <input type="hidden" name="pagenum" id="pagenum" value="2">
    <input type="hidden" name="store_name" id="store_name" value="<?php echo $store_details->affiliate_url?>">
    <input type="hidden" name="coupon_id" value="<?php echo $store_details->affiliate_id; ?>"> 
    <div class="modal fade" id="myModal_how_to_get" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Como funciona o dinheiro de volta? </h4>
          </div>
          <hr>
          <div class="modal-body">
            <p class="txt">
              <strong>Passo 1:</strong> Cadastre-se grátis no <?php $admindetails = $this->front_model->getadmindetails_main(); echo $admindetails->site_name; ?>. &amp; login<br><br>
              <strong>Passo 2:</strong> Faça login e clique na oferta desejada em <?php echo $store_details->affiliate_name;?>. 
              <br><br>
              <strong>Passo 3:</strong> Compre normalmente <?php echo $store_details->affiliate_name?>. <br><br>
              <strong>Passo 4:</strong> Seu dinheiro de volta aparecerá no seu extrato em até 2 dias úteis.
            </p> 
          </div>
          <hr>     
        </div>
      </div>
    </div>   
    <div class="modal fade" id="myModal_terms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
            <h4 class="modal-title" id="myModalLabel">Regras & Exceções </h4>
          </div>
          <hr>
          <div class="modal-body">
            <?php
              if($store_details->terms_and_conditions)
              {
                echo strtr($store_details->terms_and_conditions,$datass);
              } 
            ?> 
            <hr>     
          </div>
        </div>
      </div>
    </div>  
</section>
<?php $this->load->view('front/site_intro'); ?> 
<!-- Footer menu start -->
<footer>
  <section class="footer-section innerfoot-sect">
    <div class="container">
      <div class="clearfix wow fadeInDown">
      <div class="wow fadeInDown">
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Help And Support</h5>
          <ul class="footer-nav">
            <li><a href="<?php echo base_url();?>contato"><i class="fa fa-caret-right"></i> Contact Us</a></li>
            <li><a href="<?php echo base_url();?>perguntas-frequentes"><i class="fa fa-caret-right"></i> Perguntas Frequentes</a></li>
            <?php 
            $result = $this->front_model->sub_menu();
            foreach($result as $view)
            {
            ?>
              <li><a href="<?php echo base_url();?><?php echo $view->cms_title; ?>"><i class="fa fa-caret-right"></i> <?php echo $view->cms_heading;?></a></li>
            <?php 
            }
            ?>
            <li><a href="<?php echo base_url(); ?>sitemap.xml"><i class="fa fa-caret-right"></i> Site map</a></li>
          </ul>
        </div>
        <?php 
        if($user_id== '')
        {
          ?>
          <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Email Newsletter</h5>
          <ul class="footer-nav">
            <li><a href="#"><i class="fa fa-caret-right"></i> Subscribe to recieve inspiration, ideas and news in your inbox.</a></li>
            <li>
              <div class="col-md-12 col-sm-12 col-xs-12 pd-left-0">
              <input type="text" id="news_email" onkeypress="clears(2);" class="form-control tbox1" placeholder="Email Address">
              <input type="button" class="tbtn1" value="send" onClick="email_news();">
              </div>
              &nbsp;
              <span id="new_msg" style="color:red"></span>
            </li>
          </ul>
          </div>
          <?php
        }
        ?>
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Top Categories</h5>
          <ul class="footer-nav">
            <?php 
            $categories = $this->front_model->get_all_categories(8);
            foreach($categories as $view)
            {
            ?> 
            <li><a href="<?php echo base_url();?>ofertas/<?php echo $view->category_url;?>"><i class="fa fa-caret-right"></i> <?php echo $view->category_name;?></a></li>
            <?php
            }
            ?>
          </ul>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Top Stores</h5>
          <ul class="footer-nav">
          <?php 
          $stores = $this->front_model->get_all_stores(8);
          foreach($stores as $view)
          {
          ?>
            <li><a href="<?php echo base_url(); ?>cupom-desconto/<?php echo $view->affiliate_url?>"><i class="fa fa-caret-right"></i> <?php echo $view->affiliate_name; ?></a></li>
          <?php
          }
          ?>
          </ul>
        </div>
      </div>
      </div>
    </div>
  </section>
  <section class="copyright-section">
    <div class="container">
     <div class="row wow fadeInDown">
      <div class="col-md-6 col-sm-6 col-xs-12 wow flipInX">
        <p>Copyright &copy; <?php echo date('Y')?> <?php echo $admindetails->site_name;?>  |  All Rights Reserved </p>
      </div>
      <?php
      $listing = $this->db->query("select * from admin")->row();
      ?>
      <div class="col-md-6 wow flipInX">
        <ul class="list-socials pull-right">
          <?php if($listing->admin_twitter) {?> <li><a href="<?php echo $listing->admin_twitter;?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
          <?php if($listing->admin_fb)      {?> <li><a href="<?php echo $listing->admin_fb;?>">     <i class="fa fa-facebook"></i></a></li><?php } ?>
          <?php if($listing->admin_gplus)   {?> <li><a href="<?php echo $listing->admin_gplus;?>">  <i class="fa fa-google-plus"></i></a></li><?php } ?>
        </ul>
      </div>
     </div>
    </div>
  </section>
</footer>
<!-- Start a new code for codeigniter minify method 28-11-16-->
<?php 
$this->minify->add_js(array('jquery.1.11.1.min.js'))->add_js('bootstrap.min.js,jquery.validate.min.js');
$this->minify->add_js(array('jquery-ui.js'))->add_js('rating.js');
$js_minify = $this->minify->deploy_js();
?>
<script src="<?php echo $this->front_model->get_css_js_url();?>front/minify/scripts.min.js"></script>
<!-- End 28-11-16 -->
<?php include('store_footer.php'); ?>
<!-- New code for popup details 18-11-16 -->
<?php include('popup_details.php'); ?>
<!-- End 18-11-16 -->

<!-- New code for onesignal notification details 30-8-17 -->
<?php 
$onesignal_sess_count    = $userdetails->onesignal_sess_count;
$onesignal_popup_details = $this->db->query("select * from popup_details where popup_id=7")->result();

if($onesignal_popup_details[0]->popup_status == 1)
{
  if($onesignal_popup_details[0]->dont_show_status == 0)
  {
    if($user_id!='') 
    {
      if($onesignal_popup_details[0]->popup_content > $onesignal_sess_count)
      {
        ?>
        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
        <script>
          setTimeout(function()
          {
            var OneSignal = window.OneSignal || [];
            OneSignal.push(["init", {
            appId: "71c56ef2-c81e-44be-b9f0-dfdb770ecd43",
            autoRegister: false,
            promptOptions: 
            {
              /* These prompt options values configure both the HTTP prompt and the HTTP popup. */
              /* actionMessage limited to 90 characters */
              //actionMessage: "Não perca uma oportunidade.",
              actionMessage: "Quer que a gente te avise quando aparecer alguma super promoção, ou cashback em dobro, neste navegador?",
              //Não perca uma oportunidade! 
              //Quer que a gente te avise quando aparecer alguma super promoção, ou cashback em dobro, neste navegador?",
              
              /* Example notification title */
              exampleNotificationTitle: 'Não perca uma oportunidade!',

              /* acceptButtonText limited to 15 characters */
              acceptButtonText: "Ativar",
              /* cancelButtonText limited to 15 characters ã*/
              cancelButtonText: "Agora não" 
            },
            httpPermissionRequest: 
            {
              enable: true
            },
            notifyButton: 
            {
              enable: false /* Set to false to hide */
            }
            }]);

         
            var OneSignal = window.OneSignal || [];
            OneSignal.push(function() {
            /* These examples are all valid */
            // Occurs when the user's subscription changes to a new value.
            OneSignal.on('subscriptionChange', function (isSubscribed) {
            console.log("The user's subscription state is now:", isSubscribed);
            //OneSignal.sendTag("user_id","<?php echo $user_id;?>","category","<?php echo $referral_category_type;?>", function(tagsSent)
            OneSignal.sendTags({"user_id":"<?php echo $user_id;?>","category":"<?php echo $referral_category_type;?>"}, function(tagsSent)
            {
              // Callback called when tags have finished sending
              console.log("Tags have finished sending!");
            });
            });

            var isPushSupported = OneSignal.isPushNotificationsSupported();
            if (isPushSupported)
            {
              // Push notifications are supported
              OneSignal.isPushNotificationsEnabled().then(function(isEnabled)
              {
                if (isEnabled)
                {
                  //OneSignal.registerForPushNotifications({
                  //modalPrompt: true
                  
                  OneSignal.showHttpPrompt();
                  console.log("Push notifications are enabled!");
                } else {
                  OneSignal.showHttpPrompt();
                  console.log("Push notifications are not enabled yet.");
                }
              });
            }
            else
            {
              console.log("Push notifications are not supported.");
            }
            });
          },<?php echo $onesignal_popup_details[0]->session_count;?>000);
        </script>
        <?php
      }   
    }
  }  
}    

?>
<!-- End 30-8-17 -->
