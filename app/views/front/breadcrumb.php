
<?php 
$pagename         = $this->uri->segment(1);
$cat_name         = $this->uri->segment(2);
if($cat_name!='')
{
  $store_details    = $this->front_model->get_store_details($cat_name);
  $categorydetails  = $category_details = $this->front_model->get_category_details($cat_name); 
}
?>
<div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <ul class="breadcrumb">
          <li><a class="link-breadcrumb" href="<?php echo base_url();?>">Início</a></li>
          <?php 
          if($store_details)
          {
            if($pagename === 'cupom-desconto')
            { 
              ?>
              <li><a class="link-breadcrumb" href="<?php echo base_url()."cupom-desconto/".$stores_list->category_url;?>"><?php echo ucfirst($stores_list->category_name);?></a></li>
              <li class="">Cupom Desconto <?php echo $store_details->affiliate_name;?></a></li>
              <?php
            }else
            {
              ?>
              <li class=""><a class="link-breadcrumb" href="<?php echo base_url().$pagename?>"><?php echo ucfirst($pagename);?></a></li>
              <?php
            }
          }
          else if($categorydetails)
          {
            ?>
            <li><a class="link-breadcrumb" href="<?php echo base_url()."cupom-desconto/".$categorydetails->category_url;?>"><?php echo ucfirst($categorydetails->category_name);?></a></li>
            <li class="">Cupom Desconto <?php echo $categorydetails->category_name;?></a></li>
            <?php
          }
          else
          { 
            ?>
            <li class=""><a class="link-breadcrumb" href="<?php echo base_url().$pagename;?>"><?php echo ucfirst($pagename);?></a></li>
            <?php
          }
          if($pagename == 'cms')
          {
            ?>
            <li class=""><a class="link-breadcrumb" href="<?php echo base_url().$pagename."/".$this->uri->segment(2);?>"><?php echo ucfirst($this->uri->segment(2));?></a></li>
            <?php 
          }
          ?>
        </ul>
      </div>
    </div>
  </div>