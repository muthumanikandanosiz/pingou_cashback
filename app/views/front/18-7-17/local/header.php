<?php
$admindetails = $this->front_model->getadmindetails();
$title  = $admindetails[0]->homepage_title;
if($this->uri->segment(1) == 'barato')
{
  $images = $this->front_model->get_img_url().'uploads/adminpro/'.$admindetails[0]->storepage_meta_image;  
}
else
{
  $images = $this->front_model->get_img_url().'uploads/adminpro/'.$admindetails[0]->pingou_meta_image;
}

if(!isset($page_title)){
  $page_title = $title;
}
if(!isset($page_image)){
  $page_image = $images;
}
if(!isset($page_desc)){
  $page_desc = 'Get the best Cashback Offers at top brands. Never pay full price again. Join now Free & Start Saving!';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>

<!-- New code for referral link to register 18-7-17 -->
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/jquery.1.11.1.min.js"></script>

<script type="text/javascript">

$(window).load(function(){
var rand_code = "<?php echo $_GET['ref'] ?>";
if(rand_code!="")
{
  sessionStorage.setItem('rand_code','yes');
  $.post('<?php echo base_url();?>cashback/getref_details', function(data)
  {
    $('#show_val').html(data);
  });
} 
else 
{
  if(sessionStorage.getItem('rand_code') == null) 
  {
    $.post('<?php echo base_url();?>cashback/unsetref_session', function(data)
    {
      
       $('#show_val').html(data);
    });
  }
  else
  {
    $.post('<?php echo base_url();?>cashback/getref_details', function(data)
    {
      
       $('#show_val').html(data);
    });
  }
}
});
</script>
<!-- End 18-7-17 -->   

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $this->front_model->shortcut_details($page_title);?></title>
<meta name="Description" content="<?php echo $page_desc; ?>"/>
<meta name="keywords" content="cashback, vouchers, coupons, discounts, offers, deals, promo codes, onlin shopping, best online shopping sites" />
<meta name="robots" CONTENT="INDEX, FOLLOW" />
<meta property="og:url"         content="<?php echo $this->front_model->curPageURL();?>" />
<meta property="og:type"        content="article"/>
<meta property="og:title"       content="<?php echo $this->front_model->shortcut_details($page_title); ?>" />
<meta property="og:description" content="<?php echo $this->front_model->shortcut_details($page_desc); ?>" />
<meta property="og:image"       content="<?php echo $this->front_model->shortcut_details($page_image); ?>" />
   
<?php $this->load->view('front/css_script');?>
</head>
<!-- End 11-11-16 -->



<style type="text/css">
.row.newnewnew {
    background: #4daed9 none repeat scroll 0 0;
    margin-top: 2px;
}  

@media screen and (max-width: 750px) {
  .cls-for-popup .modal-dialog{display: none !important;}
   
} 
</style>
<!-- New code for amazon Elasticache 27-9-16 -->
<?php 
 
  $cache_details   = $this->db->query("SELECT * FROM amazon_s3_settings WHERE s3_id = 1")->row();
  $e_cache_status  = $cache_details->sys_memory_cache_status;
  
  //$server_endpoint = "pingou.jpsdi1.0001.sae1.cache.amazonaws.com";
  //$server_port     = 6379;
  /*if(class_exists('Redis'))
  {
    echo "Redis"; 
  }
  else
  {
    echo "No Redis"; 
  }
  
 //echo CI_VERSION;
 //Connecting to Redis server on localhost
   $redis = new Redis();
   $redis->connect('127.0.0.1',6379);
   echo "Connection to server sucessfully<br>";
   //set the data in redis string
   $redis->set("tutorial-name", "Redis tutorial");
   // Get the stored data and print it
   echo "Stored string in redis "; echo $redis->get("tutorial-name");*/
    
 ?>
<!-- End 27-9-16 -->


<?php
$ip_address      = $_SERVER['REMOTE_ADDR'];
if( ($_SERVER['REQUEST_URI'] != "/") and preg_match('{/$}',$_SERVER['REQUEST_URI']) ) {
    header ('Location: '.preg_replace('{/$}', '', $_SERVER['REQUEST_URI']));
    exit();
}

$ip_address         = $_SERVER['REMOTE_ADDR'];
$getadmindetails    = $this->front_model->getadmindetails();  
$logo               = $getadmindetails[0]->site_logo;
$blog_url           = $getadmindetails[0]->blog_url;
$site_mode          = $getadmindetails[0]->site_mode;
$site_name          = $getadmindetails[0]->site_name;
$background_image   = $getadmindetails[0]->background_image;
$background_color   = $getadmindetails[0]->background_color;
$background_type    = $getadmindetails[0]->background_type;
$unlog_menu_status  = $getadmindetails[0]->unlog_menu_status;
$log_menu_status    = $getadmindetails[0]->log_menu_status;
$log_content        = $getadmindetails[0]->log_content;
$unlog_content      = $getadmindetails[0]->unlog_content;
$log_status         = $getadmindetails[0]->log_status;
$unlog_status       = $getadmindetails[0]->unlog_status;
$notify_color       = $getadmindetails[0]->notify_color;
$site_favicon       = $getadmindetails[0]->site_favicon;
$ip                 = $_SERVER['REMOTE_ADDR'];
$unique_visits      = $this->front_model->unique_visits($ip);
$user_id            = $this->session->userdata('user_id'); 
$userdetails        = $this->front_model->userdetails($user_id);
$firstname          = $userdetails->first_name;
$lastname           = $userdetails->last_name;
$emailid            = $userdetails->email;
$category_type      = $userdetails->referral_category_type;

/*referral system function calling this place 12-9-16*/
$referal_systems    = $this->front_model->update_referral_systems($user_id);

/*New code for user balance changes 26-8-16*/
$osiz_usr_bal       = bcdiv($userdetails->balance,1,2);

//echo $sasasasa      = $this->front_model->currency_format(2.3992); exit;

$balance            = $this->front_model->currency_format($osiz_usr_bal);
/*end 26-8-16*/
 
$profile_photo      = $userdetails->profile; 
 

/*New code for session set refferal details 30-7-16*/
$referral_id       = $_REQUEST['ref'];
if($referral_id!='')
{
  $this->session->set_userdata('referral_id',$referral_id);
  $this->session->set_userdata('ses_random_ref',$referral_id);
  $getuserid = $this->db->query("SELECT `user_id` from `tbl_users` where `random_code`='$referral_id'")->row('user_id');
  if(!empty($getuserid))
  {
    $this->front_model->update_refclick_count($getuserid);  
  }
  else
  {
    $pagename    = uri_string();
    redirect($pagename,'refresh');
  }
} 
if($user_id!='')
{
  $this->session->unset_userdata('referral_id');
  $this->session->unset_userdata('ses_random_ref');
} 
/*End 30-7-16*/      
 
//Available balance details//

$cashback_count = $this->front_model->pending_cashback($user_id);
$referal_count  = $this->front_model->pending_referral($user_id);


/*New code for user balance changes 26-8-16*/
 
$osiz_pen_bal       = bcdiv($cashback_count + $referal_count,1,2);

$osiz_pen_bal       = $osiz_pen_bal;
$total_amt          = $this->front_model->currency_format($osiz_pen_bal);

/*End*/

if($firstname =='' && $lastname =='')
{ 
  $emails    = explode('@', $emailid);
  $usernames = $emails[0];  
}
else
{
  $usernames = ucfirst($firstname)." ".ucfirst($lastname);
}
$username  = substr($usernames,0,15);
//$usernamesasa = $userdetails[0]->first_name;

//New hide 14-7-17
/*if($site_mode==0)
{ 
  redirect('maintenance','refresh');
}*/
//End 14-7-17

echo $google_analytics = $getadmindetails[0]->google_analytics;
?>

<!-- New code for background image and color settings 20-9-16-->
<?php if($background_type == 'image'){?>
<style type="text/css">
body 
{
  background:url(<?php echo $this->front_model->get_img_url();?>uploads/adminpro/<?php echo $background_image;?>)!important; 
  //background-repeat:repeat;
  //background-size: 100% 100%;
} 
</style>
<?php 
}
if($background_type == 'color')
{?>
<style type="text/css">
body {
  background-color:<?php echo $background_color;?> !important;
} 
</style>
<?php } ?>
<!-- End -->


<!-- NAVBAR -->
<?php
if($user_id == '')
{ 
  
  if($unlog_status == 0)
  {
    ?>
    <style type="text/css">
    .navbar-wrapper
    {
      margin-top: 0px !important;
    }
    </style>
    <?php
  }
  if(($unlog_status == 1) && ($unlog_menu_status == 0))
  {
    ?>
    <style type="text/css">
    
    /*code for (position) based menu*/
    .main{
      top: 0;
      width: 100%;
      z-index: 10;
    }
    .newnews
    { 
      padding:10px 0 !important;
      position: relative;
      width: 100%;
    }
     
    </style>
    <?php 
  }
  if(($unlog_status == 1) && ($unlog_menu_status == 1))
  {
    ?>
    <style type="text/css">
     
    /*code for (position) based menu*/
    .newnews
    {
      display: block;
      padding:10px 0 !important;
    }
    .main{
      position: fixed;
      top: 35px;
      width: 100%;
      z-index: 10;
    }
    .newclass
    {
      margin-top: 70px !important;
    }
      
    </style>
    <?php
  }
  if(($unlog_status == 1) && ($unlog_menu_status == 2))
  { 
    ?>
    <style type="text/css">
    
    /*code for (position) based menu*/
    .newnew{
      position: fixed;
      width: 100%;
      z-index: 999;
    }
    .navbar-wrapper
    {  
      position: fixed !important;  
    }
    .page-intro 
    {
      margin-top: 35px !important;
    }
    @media screen and (min-width: 768px) and (max-width: 1024px) {
    .blue_h.innerblue_h .navbar-wrapper {
        margin-top: 35px;
    }
    body .page-intro {
    margin-top: 35px !important;
    }
  }  
    </style>
    <?php
  }
  
  /*Unlog notification bar and Unlog menu stauts conditions*/

  if(($unlog_status == 0) && ($unlog_menu_status == 0)) 
  {
    ?>
    <style type="text/css">
      .newnews
      {
          display: none !important;
          padding-top: 70px !important;
        }
        .main 
        {
          top:0px !important;
      }
       
    </style>
    <?php
  }

  if(($unlog_status == 0) && ($unlog_menu_status == 1)) 
  {  
    ?>
    <style type="text/css">
      .newnews
      {
          display: none !important;
          padding-top: 70px !important;
        }
        .main 
        {
          top:0px !important;
      }
      .newclass
      {
        margin-top: 70px !important;
      }
       
    </style>
    <?php
  }
  if(($unlog_status == 0) && ($unlog_menu_status == 2)) 
  {
    ?>
    <style type="text/css">
      .newnews
      {
          display: none !important;
          padding-top: 70px !important;
        }
        .main 
        {
          top:0px !important;
      }
      .newclass
      {
        margin-top: 70px !important;
      }
      .top-main-area
      {
          display: none !important;
      }
      .navbar-wrapper
      {  
        position: fixed !important;
      }
       
    </style>
    <?php
  } 
}
if($user_id !='')
{
  if($log_status == 0)
  {
    ?>
    <style type="text/css">
    .navbar-wrapper
    {
      margin-top: 0px !important;
    }
    </style>
    <?php
  }

  if(($log_status == 0) && ($log_menu_status == 0)) 
  {
    ?>
    <style type="text/css">
      .newnews
      {
          display: none !important;
      }
    </style>
    <?php
  }
  if(($log_status == 0) && ($log_menu_status == 1)) 
  {
    ?>
    <style type="text/css">
      .newnews
      {
          display: none !important;
        }
      .navbar-wrapper
      {  
        position: fixed !important;
      }
    </style>
    <?php
  }
  if(($log_status == 0) && ($log_menu_status == 2)) 
  {
    ?>
    <style type="text/css">
      .top-main-area
      {
          display: none !important;
      }
      .navbar-wrapper
      {  
        position: fixed !important;
      }

    </style>
    <?php
  } 
  if(($log_status == 1) && ($log_menu_status == 0)) 
  {
    ?>
    <style type="text/css">
      
      .newclass
       {
        //padding-top:100px !important;
       } 
         
    </style>
    <?php
  }
  if(($log_status == 1) && ($log_menu_status == 1)) 
  {
    ?>
    <style type="text/css">
    .newclass
      {
        margin-top: 70px !important;
      }  
       
    </style>
    <?php
  }
  if(($log_status == 1) && ($log_menu_status == 2)) 
  { 
    ?>
    <style type="text/css">
      
    .newnew
    {
      position: fixed;
      width: 100%;
      z-index: 999;
    }
    .navbar-wrapper
    {  
      position: fixed !important;
    }
    .page-intro
    {
      margin-top: 35px !important;
    }

    @media screen and (min-width: 768px) and (max-width: 1024px) {
    .blue_h.innerblue_h .navbar-wrapper {
        margin-top: 35px;
    }
    body .page-intro {
    margin-top: 35px !important;
    }
  }
    </style>
    <?php
  }
}
?>
<body>
<div class="top-main-area text-center newnew newnews" style="padding:6px 0;">
  <div class="wrap-top">
    <!--New code for Notification bar content 16-4-16-->
    <?php 
    $cat_details   = $this->db->query("select * from referral_settings where ref_id='$category_type'")->row();
    $log_content   = $cat_details->notify_log_users;
    //$log_content = $getadmindetails[0]->log_content;
    $unlog_content = $getadmindetails[0]->unlog_content;
    $log_status    = $getadmindetails[0]->log_status;
    $unlog_status  = $getadmindetails[0]->unlog_status;
    $url           = base_url()."#register";
    
     
    $content     = $this->front_model->shortcut_details($unlog_content);
    $log_content = $this->front_model->shortcut_details($log_content); 


    if($user_id!='')
    { 
        if($log_status == 1)
        {
          if($notify_color !='')
          { 
            ?>
            <style type="text/css">
              .newnew
              {
                background: <?php echo $notify_color;?>
              }
              .wrap-top p { color: #fff; font-weight: 500; margin-bottom: 0; }
              .navbar-wrapper {margin-top: 35px;}
            </style>
            <b><?php echo $log_content; ?></b>
            <?php
          }   
        } 
    } 
    if($user_id == '')
    { 
        if($unlog_status == 1)
        {
          if($notify_color !='')
          { 
            ?>
            <style type="text/css">
              .newnew
              {
                background:<?php echo $notify_color;?>
              }
              .wrap-top p { color: #fff; font-weight: 500; margin-bottom: 0; }
              .navbar-wrapper {margin-top: 35px;}
            </style>
            <span id="show_val"><b></b></span> <!-- id="show_val" -->
            <?php
          }
        }
    }   
    ?>
  </div>
</div>
<?php 
if($user_id =='')
{
?>
  <div class="blue_h innerblue_h">
<?php 
}
else
{?>
  <div class="blue_h innerblue_h">
<?php 
}
?>
  <div class="navbar-wrapper wow fadeInDown">
    <?php if($user_id == '') 
    {
      ?>
      <div class="cbp-af-header" id="cbp-af-header">
      <?php 
    }
    else
    {
      ?>
      <div class="cbp-af-header" id="cbp-af-header1">
      <?php 
    }
    ?>
      <div class="cbp-af-inner">
        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">           
            <div class="tophead-resp-md"> 

              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                 data-target="#navbar_p" aria-expanded="false" aria-controls="navbar"> 
                 <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
                  <span class="icon-bar"></span> <span class="icon-bar"></span> 
                  </button>
                  <a href="javascript:;" 
                  id="selsearch1" class="select-style1 hidden-md hidden-sm"> </a>
              </div>

              <div class="row">              
                <div class="col-md-5 col-sm-5 col-xs-3 cls_menucont">
                 
                  <div id="navbar_p" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                      <li class=""><a href="<?php echo base_url();?>cupom">Destaques</a></li>
                      <li><a href="<?php echo base_url();?>cupom-desconto">Coupons</a></li>
                      <?php
                      /*$enable_shopping = $getadmindetails[0]->enable_shopping;
                      if($enable_shopping == 1)
                      {
                        ?>  
                        <li><a href="<?php echo base_url();?>produtos">Ofertas</a></li>
                        <?php
                      }*/
                      ?>
                      <li>
                          <a href="javascript:;" id="selsearch" class="select-style1 hidden-xs">
                          <div class="select">
                              buscar lojas
                          </div>
                          </a>
                        
                      </li>
                    </ul>
                  </div>

                </div>
                <div class="col-md-2 col-sm-2 col-xs-5 toplogo-md">
                  <center>
                    <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/logo.png"></a>
                  </center>
                </div> 
                <?php
                if($user_id == '')
                {
                  ?>
                  <div class="col-md-5 col-sm-5 col-xs-4 pull-right">
                    <div class="sign-topblk">
                      <ul class="nav navbar-nav pull-right">
                        <li><a class="hidden-xs" href="#register" data-toggle="modal">Sign up</a></li>
                        <li><a href="#login" data-toggle="modal">Login</a></li>                       
                      </ul>
                    </div>
                  </div>
                  <?php
                }
                else
                {
                  ?>
                  <!-- New design for my account menu 14-7-16-->
                  <div class="col-md-5 col-sm-5 col-xs-12 topuserdet">
                    <div id="" class="pro_im clearfix">
                      <ul class="nav navbar-nav pull-right col-md-6 col-sm-10 col-xs-12">
                        <li class="dropdown top-login">
                          <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <div class="pro_pic">
                              <?php 
                              if($profile_photo =='')
                              {
                              ?>
                              <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/profile_pic.png" class="img-responsive">
                              <?php
                              }
                              else
                              {
                              ?>
                                <img style="border-radius: 16px;" class="img-responsive" alt="" src="<?php echo $profile_photo; ?>">
                              <?php
                              }
                              ?>
                            </div>
                            <div class="pro_piccont">
                                <!-- <a href="<?php echo base_url(); ?>minha-contanew"> -->
                                  <p class="usr-log"> <?php echo $username;?> <span class="caret pull-right"></span></p> 
                                  <p class="usr-log1">R$<?php echo $balance; ?>  <span class="usr-log2"> R$<?php echo $total_amt; ?></span> </p>
                                <!-- </a> -->
                            </div>
                            <!-- <a href="<?php echo base_url(); ?>minha-contanew"><?php echo $username;?> </a><br> -->
                            <!-- <span style="color:#5ef05a; font-size:14px;top: 29px; left:55px; position:absolute; line-height:10px; width:40%;" >R$<?php echo $balance; ?></span>
                            <span style="color:#f5ff55; font-size:14px;top: 29px; left:135px; position:absolute; line-height:10px;width:40%;"> R$<?php echo $total_amt; ?></span>  -->
                          </a>
                          <ul class="dropdown-menu">
                            <center><h4>Visao geral</h4></center>
                            <center>
                              <ul class="appr-listblk list-inline clearfix">
                                <li>
                                  <a href="<?php echo base_url();?>minha-conta">
                                    <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr1.png" alt="app" class="img-responsive center-block">
                                    dados
                                  </a>
                                </li>
                                <li>
                                  <a href="<?php echo base_url();?>extrato">
                                    <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr2.png" alt="app" class="img-responsive center-block">
                                    Extrato
                                  </a>
                                </li>
                                <li>
                                  <a href="<?php echo base_url();?>resgate">
                                    <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr3.png" alt="app" class="img-responsive center-block">
                                    Resgate
                                  </a>
                                </li>
                                <li>
                                  <a href="<?php echo base_url();?>indique-e-ganhe">
                                    <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr4.png" alt="app" class="img-responsive center-block">
                                    Indicacoes
                                  </a>
                                </li>
                              </ul>
                            </center>
                            <center><h4>Facilidades</h4></center>
                            <ul class="appr-listblk1 list-inline clearfix">
                              <li>
                                <a href="#">
                                  <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr5.png" alt="app" class="img-responsive pull-left">
                                   <span> Lembrador </span>
                                </a>
                              </li>                       
                              <li class="pull-right">
                                <a href="#">
                                  <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr6.png" alt="app" class="img-responsive pull-left">
                                  <span> Aplicativo </span>
                                </a>
                              </li>
                            </ul>
                            <ul class="appr-listbtnblk1 list-inline clearfix">
                              <li>
                                <button class="btn btn-appr" type="button"> <img alt="chat" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr-chat.png" class="img-responsive"> Ajuda </button>
                              </li>
                              <li class="pull-right">
                                <button onclick="window.location.href='<?php echo base_url();?>logout'" class="btn btn-appr" type="button"> <img alt="chat" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr-logout.png" class="img-responsive"> Sair </button>
                              </li>
                            </ul>
                          </ul>  
                        </li>
                      </ul>
                    </div>
                  </div>  
                  <!-- End New my account menu -->
                  <?php
                } 
                ?>
              </div>              
            </div>
          </div>  
     
        <!-- New code for search 6-8-16 -->
        <div class="search_content" id="search_content" style=" background-color: transparent; display:none;">
              <div class="container">
                <?php
                if((strpos($_SERVER['REQUEST_URI'],'cashback/shopping') == true))
                {
                  ?>
                  <form class="search-area form-group search-area-white search-style" action="<?php echo base_url();?>cashback/change_location" method="POST">
                    <div class="wrap-top">
                      <div class="row">
                        <div class="col-md-11 clearfix">
                          <div class="search-area-division search-area-division-input">
                            <input type="text" required id="Location" placeholder="Pesquise Lojas ou Ofertas" class="form-control" name="location" 
                            value="<?php if($this->session->userdata('cityname')){ echo $cityname = $this->session->userdata('cityname');}?>" data-provide="typeahead">
                          </div>
                        </div>
                        <div class="col-md-1">
                          <button class="btn btn-block btn-white search-btn pop" type="submit">
                            <i class="fa fa-search"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                  <?php
                }
                else
                {  
                  if(isset($_POST['storehead']))
                  {
                    $storehead  = $_POST['storehead'];
                  } 
                  ?>
                    <!--New search form-->
                    <form id="search_mini_form" class="search-area form-group search-area-white search-style"  onsubmit="return submit_form();" method="get">
                      <div class="wrap-top">
                        <div class="row">
                          <div class="col-md-11 col-sm-10 col-xs-9 clearfix">
                            <div class="search-area-division search-area-division-input">
                              <input id="search" required type="text" name="store" class="form-control cls_searchtbox search_header" value="<?php if(isset($_POST['storehead'])){echo $storehead;}?>" placeholder="Aliexpress, Submarino, Netshoes" data-provide="typeahead" style="width:102% !important;"/>
                              <!-- <input id="catsearch" type="hidden" name="cat" /> -->
                            </div>
                          </div>  
                          <div class="col-md-1 col-sm-2 col-xs-3 search-botao">
                            <button type="submit" class="btn btn-block btn-white cls_searchtbtn search-btn pop" style="float:right; margin-right: 17px;"><i class="fa fa-search"></i>
                            </button> 
                          </div>
                          <div id="search_autocomplete" class="search-autocomplete">
                          </div>
                        </div>        
                      </div>
                    </form>
                    <!--End-->
                    <form id="dummyform" method="post">
                        <input type="hidden" name="storehead" id="storehead" value="">
                    </form>
                    <?php
                } 
                ?>
              </div>
        </div>
         <div class="search_content" id="search_content1" style=" background-color: transparent; display:none;">
              <div class="container">
                <?php
                if((strpos($_SERVER['REQUEST_URI'],'cashback/shopping') == true))
                {
                  ?>
                  <form class="search-area form-group search-area-white search-style" action="<?php echo base_url();?>cashback/change_location" method="POST">
                    <div class="wrap-top">
                      <div class="row">
                        <div class="col-md-11 clearfix">
                          <div class="search-area-division search-area-division-input">
                            <input type="text" required id="Location" placeholder="Pesquise Lojas ou Ofertas" class="form-control" name="location" 
                            value="<?php if($this->session->userdata('cityname')){ echo $cityname = $this->session->userdata('cityname');}?>" data-provide="typeahead">
                          </div>
                        </div>
                        <div class="col-md-1">
                          <button class="btn btn-block btn-white search-btn pop" type="submit">
                            <i class="fa fa-search"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                  <?php
                }
                else
                {  
                  if(isset($_POST['storehead']))
                  {
                    $storehead  = $_POST['storehead'];
                  } 
                  ?>
                    <form id="search_mini_form" class="search-area form-group search-area-white search-style"  onsubmit="return submit_form();" method="get">
                      <div class="wrap-top">
                        <div class="row">
                          <div class="col-md-11 col-sm-10 col-xs-9 clearfix">
                            <div class="search-area-division search-area-division-input">
                              <input id="search" required type="text" name="store" class="form-control cls_searchtbox search_header searchs" value="<?php if(isset($_POST['storehead'])){echo $storehead;}?>" placeholder="Aliexpress, Submarino, Netshoes" data-provide="typeahead" style="width:102% !important;"/>
                             
                            </div>
                          </div>  
                          <div class="col-md-1 col-sm-2 col-xs-3 search-botao">
                            <button type="submit" class="btn btn-block btn-white cls_searchtbtn search-btn pop" style="float:right; margin-right: 17px;"><i class="fa fa-search"></i>
                            </button> 
                          </div>
                          <div id="search_autocomplete" class="search-autocomplete">
                          </div>
                        </div>        
                      </div>
                    </form>
                     
                    <form id="dummyform" method="post">
                        <input type="hidden" name="storehead" id="storehead" value="">
                    </form>
                    <?php
                } 
                ?>
              </div>
        </div> 
        <!-- End -->
        </nav>
      </div>
    </div>
  </div>
</div>

<?php 
$pagename         = $this->uri->segment(1);
$cat_name         = $this->uri->segment(2);
if($cat_name!='')
{
  $store_details    = $this->front_model->get_store_details($cat_name);
  $categorydetails  = $category_details = $this->front_model->get_category_details($cat_name); 
}

//echo "<pre>";print_r($categorydetails);
$store_category   = $store_details->store_categorys;  
if(strpos($store_category, ','))
{
  $store_categories = explode(",",$store_category);
  foreach($store_categories as $newstorecategories) 
  {
     $newstorecategories;
  } 
}
else
{
  $newstore_categories = $store_category; 
}
$stores_list = $this->front_model->get_category_details_byid($newstore_categories);
//echo "<pre>";print_r($stores_list);
?>
<?php 
if($user_id =='')
{
  ?>
  <div class="page-intro">
  <?php 
}
else
{
  ?>
  <div class="page-intro">
    <?php 
}
    ?>
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <ul class="breadcrumb">
            <li><a class="link-breadcrumb" href="<?php echo base_url();?>">Início</a></li>
            <?php 
            $pagenames = str_replace('-', ' ', $pagename);
            if($pagenames)
            {
              $pagenamess = ucwords($pagenames); 
            }
            else
            {
              $pagenamess = ucwords($pagename);
            }
             
            if($pagename == 'barato')
            {
              ?>
              <li class=""><a class="link-breadcrumb" href="<?php echo base_url()."barato/".$this->uri->segment(2);?>"><?php echo str_replace('-', ' ', ucfirst($this->uri->segment(2)));?></a></li>
              <li class=""><?php echo str_replace('-', ' ', ucfirst(urldecode($this->uri->segment(3))));?></li>
              <?php 
            }
            else if($store_details)
            {
              if($pagename === 'cupom-desconto')
              { 
                ?>
                <li><a class="link-breadcrumb" href="<?php echo base_url()."cupom-desconto/".$stores_list->category_url;?>"><?php echo ucfirst($stores_list->category_name);?></a></li>
                <li class="">Cupom Desconto <?php echo $store_details->affiliate_name;?></a></li>
                <?php
              }
              /*else
              {
                ?>
                <li class=""><a class="link-breadcrumb" href="<?php echo base_url().$pagename?>"><?php echo ucfirst($pagename);?></a></li>
                <?php
              }*/
            }
            else if($categorydetails)
            {
              ?>
              <li><a class="link-breadcrumb" href="<?php echo base_url()."cupom-desconto/".$categorydetails->category_url;?>"><?php echo ucfirst($categorydetails->category_name);?></a></li>
              <li class="">Cupom Desconto <?php echo $categorydetails->category_name;?></a></li>
              <?php
            }
            else
            { 
               
                ?>
                <li class=""><a class="link-breadcrumb" href="<?php echo base_url().$pagename;?>"><?php echo $pagenamess;?></a></li>
                <?php
               
            }
            if($pagename == 'cms')
            {
                $pagenames = str_replace('-', ' ', $this->uri->segment(2));
                if($pagenames)
                {
                  $pagenamess = ucwords($pagenames); 
                }
                else
                {
                  $pagenamess = ucwords($this->uri->segment(2));
                }
              ?>
               <li class=""><a class="link-breadcrumb" href="<?php echo base_url().$pagename."/".$this->uri->segment(2);?>"><?php echo $pagenamess;?></a></li>
              <?php 
            }
            ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" id="rand_code" value="<?php echo $this->session->userdata('ses_random_ref'); ?>">
