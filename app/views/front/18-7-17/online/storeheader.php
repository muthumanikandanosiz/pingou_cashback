<!--Header content Start-->
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php
$user_id          = $this->session->userdata('user_id'); 
$userdetails      = $this->front_model->userdetails($user_id);
$refer_code       = $userdetails->random_code;
$firstname        = $userdetails->first_name;
$lastname         = $userdetails->last_name;
$emailid          = $userdetails->email;
if($firstname =='' && $lastname =='')
{ 
  $emails    = explode('@', $emailid);
  $usernames = $emails[0];  
}
else
{
  $usernames = ucfirst($firstname)." ".ucfirst($lastname);
}

$cat_name         = $this->uri->segment(2);
$store_detailss   = $this->front_model->get_store_details($cat_name);
$page_title       = $store_detailss->meta_title;
if($refer_code == '')
{
  $refer_code = '';
}
/*new code for store page shrotcut details 26-4-17*/
$date     = date('Y-m-d');
$newdate  = explode('-',$date);
$year     = $newdate[0];
$month    = $newdate[1];
$day      = $newdate[2];
$days     = explode(' ',$day);
$newday   = $days[0];
$off_year = date('y');
$local_month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); 
$braz_months_names = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
$dt = date('F');
$current_month     = str_ireplace($local_month_names, $braz_months_names, $dt);


$affiliate_cashback_type = $store_detailss->affiliate_cashback_type;
if($affiliate_cashback_type == 'Percentage')
{
  $cashbacks = $store_detailss->cashback_percentage."%";
}
else if($affiliate_cashback_type == 'Flat')
{
  $cashbacks = "R$ ".$store_detailss->cashback_percentage;
}
else
{
  $cashbacks = ""; 
}

$get_cat_type        = $this->front_model->referal__category();
$get_cat_details     = $this->front_model->get_referral_settings($get_cat_type);

$type_one_percentage = $get_cat_details->ref_cashback;
$type_one_days       = $get_cat_details->valid_months;
$type_two_amount     = $get_cat_details->ref_cashback_rate;  
$type_three_amount   = $get_cat_details->ref_cashback_rate_bonus;
$type_three_users    = $get_cat_details->friends_count;
$unique_bonus        = str_replace('.', ',',$get_cat_details->category_bonus_amount);

if($user_id == '')
{
  if($_REQUEST['ref'])
  { 
    $ref_id  = $_REQUEST['ref'];
    
    $this->db->where('random_code',$ref_id);
    $user_details = $this->db->get('tbl_users');
    if($user_details->num_rows() > 0)
    {
      $userdetails       = $user_details->row();
      $ref_cat_type      = $userdetails->referral_category_type;

      $get_cat_detailss    = $this->front_model->get_referral_settings($ref_cat_type);
      $get_future_category = $get_cat_detailss->new_ref_cat_types;
      $new_fut_catdetails  = $this->front_model->get_referral_settings($get_future_category);
        $unique_bonus        = str_replace('.', ',',$new_fut_catdetails->category_bonus_amount);
    }
  }
  else
  {
    $get_cat_detailss  = $this->front_model->get_referral_settings(1);
      $unique_bonus     = str_replace('.', ',',$get_cat_detailss->category_bonus_amount);
  }
}

$datass = array(
  '###STORE-NAME###'=>$store_detailss->affiliate_name,
  '###CASHBACK###'=>$cashbacks,
  '###TRACKING-SPEED###'=>$store_detailss->report_date,
  '###ESTIMATED-PAYMENT###'=>$store_detailss->retailer_ban_url,
  '###COUPON-NUMBER###' =>$count_act_coupons,
  '###DD###'    =>$newday,
  '###MM###'    =>$month,
  '###MONTH###' =>$current_month,
  '###YYYY###'  =>$year,
  '###YY###'    =>$off_year,
  '###STORE-IMG-ONE###'  =>$this->front_model->get_img_url()."uploads/affiliates/".$store_detailss->store_one_img,
  '###STORE-IMG-TWO###'  =>$this->front_model->get_img_url()."uploads/affiliates/".$store_detailss->store_two_img,
  '###STORE-IMG-THREE###'=>$this->front_model->get_img_url()."uploads/affiliates/".$store_detailss->store_three_img,
  '###STORE-IMG-FOUR###' =>$this->front_model->get_img_url()."uploads/affiliates/".$store_detailss->store_four_img,
  '###STORE-IMG-FIVE###' =>$this->front_model->get_img_url()."uploads/affiliates/".$store_detailss->store_five_img,
  '###STORE-IMG-SIX###'  =>$this->front_model->get_img_url()."uploads/affiliates/".$store_detailss->store_six_img,
  '###REFERRAL-PARAMETER###'=> $refer_code,
  
  '###Type-ONE###'=>$type_one_percentage,
  '###Type-ONE-days###'=>$type_one_days,
  '###Type-TWO###'=>$type_two_amount,
  '###Type-THREE###'=>$type_three_amount,
  '###Type-THREE-number-of-users###'=>$type_three_users,
  '###UNIQUE-BONUS###' => $unique_bonus,
  '###USER-NAME###' => $usernames,
  );

$page_title = strtr($page_title,$datass); 

/*End 24-4-17*/ 

$page_desc        = $store_detailss->meta_description;
$page_desc        = strtr($page_desc,$datass);

$meta_keyword     = $store_detailss->meta_keyword;
$meta_keyword     = strtr($meta_keyword,$datass); 

$getadmindetails  = $this->front_model->getadmindetails();

$images           = $this->front_model->get_img_url().'uploads/adminpro/'.$getadmindetails[0]->storepage_meta_image;
$site_favicon     = $getadmindetails[0]->site_favicon;

//if(!isset($page_image)){$page_image = $images;}
//$title = $getadmindetails[0]->homepage_title;
//if(!isset($page_title)){$page_title = $title;}
//if(!isset($page_desc)){//$page_desc = 'Get the best Cashback Offers at top brands. Never pay full price again. Join now Free & Start Saving!';}

?>
<title><?php echo $page_title;?></title>
<meta name="Description"        content="<?php echo $page_desc; ?>"/>
<meta name="keywords"           content="<?php echo $meta_keyword; ?>" />
<meta name="robots"             CONTENT="INDEX, FOLLOW" />
<meta property="og:url"         content="<?php echo $this->front_model->curPageURL();?>" />
<meta property="og:type"        content="article"/>
<meta property="og:title"       content="<?php echo $page_title; ?>" />
<meta property="og:description" content="<?php echo $page_desc; ?>" />
<meta property="og:image"       content="<?php echo $images; ?>" />

<link  type="image/x-icon" rel="shortcut icon"  href="<?php echo $this->front_model->get_img_url()."uploads/adminpro/".$site_favicon;?>">
<link  type="text/css" rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/css/fonts.css">
<link  type="text/css" rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/css/bootstrap.min.css">
<?php  
  $this->minify->add_css(array('carousel.css'))->add_css('style.css,newfont-awesome.css,pre-pge.css,jquerys-ui.css,storepage_style.css,rating.css,icomoon_style.css');
  $news = $this->minify->deploy_css();
?>
  <link  type="text/css" rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/minify/style.min.css">
</head>
<style type="text/css">
.row.newnewnew {background: #4daed9 none repeat scroll 0 0; margin-top: 2px;}  
@media screen and (max-width: 750px){.cls-for-popup .modal-dialog{display: none !important;}} 

</style>
<?php
$ip_address      = $_SERVER['REMOTE_ADDR'];
if( ($_SERVER['REQUEST_URI'] != "/") and preg_match('{/$}',$_SERVER['REQUEST_URI']) ) {
    header ('Location: '.preg_replace('{/$}', '', $_SERVER['REQUEST_URI']));
    exit();
}
$logo               = $getadmindetails[0]->site_logo;
$site_mode          = $getadmindetails[0]->site_mode;
$background_image   = $getadmindetails[0]->background_image;
$background_color   = $getadmindetails[0]->background_color;
$background_type    = $getadmindetails[0]->background_type;
$unlog_menu_status  = $getadmindetails[0]->unlog_menu_status;
$log_menu_status    = $getadmindetails[0]->log_menu_status;
$log_content        = $getadmindetails[0]->log_content;
$unlog_content      = $getadmindetails[0]->unlog_content;
$log_status         = $getadmindetails[0]->log_status;
$unlog_status       = $getadmindetails[0]->unlog_status;
$notify_color       = $getadmindetails[0]->notify_color;
$ip                 = $_SERVER['REMOTE_ADDR'];
$unique_visits      = $this->front_model->unique_visits($ip);

$category_type      = $userdetails->referral_category_type;
$referal_systems    = $this->front_model->update_referral_systems($user_id);
$osiz_usr_bal       = bcdiv($userdetails->balance,1,2);
$balance            = $this->front_model->currency_format($osiz_usr_bal);
$profile_photo      = $userdetails->profile; 


$referral_id        = $_REQUEST['ref'];
if($referral_id!='')
{
  $this->session->set_userdata('referral_id',$referral_id);
  $this->session->set_userdata('ses_random_ref',$referral_id);
  $getuserid = $this->db->query("SELECT `user_id` from `tbl_users` where `random_code`='$referral_id'")->row('user_id');

  if(!empty($getuserid))
  {
    $this->front_model->update_refclick_count($getuserid);  
  }
  else
  {
    $pagename    = uri_string();
    redirect($pagename,'refresh');
  }
} 



if($user_id!='')
{
  $this->session->unset_userdata('referral_id');
  $this->session->unset_userdata('ses_random_ref');
} 

$cashback_count = bcdiv($this->front_model->pending_cashback($user_id),1,2);
$referal_count  = bcdiv($this->front_model->pending_referral($user_id),1,2);
$osiz_pen_bal   = $cashback_count + $referal_count;
$total_amt      = $this->front_model->currency_format($osiz_pen_bal);

/*new code for cashback exclusive code 21-4-17*/
  /*$cash_ex_url    = "$_SERVER[REQUEST_URI]";
  $ex_cash_ex_url = explode('/', $cash_ex_url);*/
  $extra_param    = $this->session->userdata('analytics_info');
  //$extra_param    = $ex_cash_ex_url[3];//Local    //Online $ex_cash_ex_url[3]; 

  if($extra_param)
  {
    $getcash_ex_details = $this->front_model->getcashback_ex_details($extra_param);
  }
/*end 21-4-17*/

 


if($firstname =='' && $lastname =='')
{ 
  $emails    = explode('@', $emailid);
  $usernames = $emails[0];  
}
else
{$usernames = ucfirst($firstname)." ".ucfirst($lastname);}
$username  = substr($usernames,0,15);
//New hide 14-7-17
/*if($site_mode==0)
{ redirect('maintenance','refresh');}*/
echo $google_analytics = $getadmindetails[0]->google_analytics;

if($background_type == 'image')
{
  ?>
  <style type="text/css">
  body 
  {background:url(<?php echo $this->front_model->get_img_url();?>uploads/adminpro/<?php echo $background_image;?>)!important;} 
  </style>
  <?php 
}
if($background_type == 'color')
{
  ?>
  <style type="text/css">
  body {background-color:<?php echo $background_color;?> !important;} 
  </style>
  <?php
}

if($user_id == '')
{ 
  if($unlog_status == 0)
  {
    ?>
    <style type="text/css">
    .navbar-wrapper{margin-top: 0px !important;}
    </style>
    <?php
  }
  if(($unlog_status == 1) && ($unlog_menu_status == 0))
  {
    ?>
    <style type="text/css">
    .main{top: 0;width: 100%;z-index: 10;}
    .newnews{padding:10px 0 !important;position: relative;width: 100%;}
    </style>
    <?php 
  }
  if(($unlog_status == 1) && ($unlog_menu_status == 1))
  {
    ?>
    <style type="text/css">
    .newnews{display: block;padding:10px 0 !important;}
    .main{position: fixed;top: 35px;width: 100%;z-index: 10;}
    .newclass{margin-top: 70px !important;}
    </style>
    <?php
  }
  if(($unlog_status == 1) && ($unlog_menu_status == 2))
  { 
    ?>
    <style type="text/css">
      .newnew{position: fixed;width: 100%;z-index: 999;}
      .navbar-wrapper{position: fixed !important;}
      .page-intro{margin-top: 35px !important;}
      @media screen and (min-width: 768px) and (max-width: 1024px) {
      .blue_h.innerblue_h .navbar-wrapper {margin-top: 35px;}
      body .page-intro {margin-top: 35px !important;}
      }  
    </style>
    <?php
  }
  if(($unlog_status == 0) && ($unlog_menu_status == 0)) 
  {
    ?>
    <style type="text/css">
      .newnews{display: none !important;padding-top: 70px !important;}
      .main{top:0px !important;}       
    </style>
    <?php
  }
  if(($unlog_status == 0) && ($unlog_menu_status == 1)) 
  {  
    ?>
    <style type="text/css">
      .newnews{display: none !important;padding-top: 70px !important;}
      .main{top:0px !important;}
      .newclass{margin-top: 70px !important;}
    </style>
    <?php
  }
  if(($unlog_status == 0) && ($unlog_menu_status == 2)) 
  {
    ?>
    <style type="text/css">
      .newnews{display: none !important;padding-top: 70px !important;}
      .main{top:0px !important;}
      .newclass{margin-top: 70px !important;}
      .top-main-area{display: none !important;}
      .navbar-wrapper{position: fixed !important;}
    </style>
    <?php
  } 
}
if($user_id !='')
{
  if($log_status == 0)
  {
    ?>
    <style type="text/css">
    .navbar-wrapper{margin-top: 0px !important;}
    </style>
    <?php
  }
  if(($log_status == 0) && ($log_menu_status == 0)) 
  {
    ?>
    <style type="text/css">
      .newnews{display: none !important;}
    </style>
    <?php
  }
  if(($log_status == 0) && ($log_menu_status == 1)) 
  {
    ?>
    <style type="text/css">
      .newnews{display: none !important;}
      .navbar-wrapper{position: fixed !important;}
    </style>
    <?php
  }
  if(($log_status == 0) && ($log_menu_status == 2)) 
  {
    ?>
    <style type="text/css">
      .top-main-area{display: none !important;}
      .navbar-wrapper{position: fixed !important;}
    </style>
    <?php
  } 
  if(($log_status == 1) && ($log_menu_status == 0)) 
  {
    ?>
    <style type="text/css">
      .newclass{//padding-top:100px !important;} 
    </style>
    <?php
  }
  if(($log_status == 1) && ($log_menu_status == 1)) 
  {
    ?>
    <style type="text/css">
    .newclass{margin-top: 70px !important;}  
    </style>
    <?php
  }
  if(($log_status == 1) && ($log_menu_status == 2)) 
  { 
    ?>
    <style type="text/css"> 
    .newnew{position: fixed;width: 100%;z-index: 999;}
    .navbar-wrapper{position:fixed !important;}
    .page-intro{margin-top: 35px !important;}
    @media screen and (min-width: 768px) and (max-width: 1024px) {
    .blue_h.innerblue_h .navbar-wrapper 
    {margin-top: 35px;}
    body .page-intro {margin-top: 35px !important;}
    }
    </style>
    <?php
  }
}
?>
<body>
<div class="main-cls-all">
  <div class="top-main-area text-center newnew newnews" style="padding:6px 0;">
    <div class="wrap-top">
      <?php 
      $cat_details   = $this->db->query("select * from referral_settings where ref_id='$category_type'")->row();
      $log_content   = $cat_details->notify_log_users;
      $url           = base_url()."#register";
      
      $content     = strtr($unlog_content,$datass);
      $log_content = strtr($log_content,$datass);
      if($user_id!='')
      { 
        if($log_status == 1)
        {
          if($notify_color !='')
          { 
            ?>
            <style type="text/css">
              .newnew{background: <?php echo $notify_color;?>}
              .wrap-top p{color: #fff; font-weight: 500; margin-bottom: 0; }
              .navbar-wrapper{margin-top: 35px;}
            </style>
            <b><?php echo $log_content; ?></b>
            <?php
          }   
        } 
      } 
      if($user_id == '')
      { 
        if($unlog_status == 1)
        {
          if($notify_color !='')
          { 
            ?>
            <style type="text/css">
              .newnew{background:<?php echo $notify_color;?>}
              .wrap-top p{ color: #fff; font-weight: 500; margin-bottom: 0; }
              .navbar-wrapper{margin-top: 35px;}
            </style>
            <b><?php echo $content;?></b>
            <?php
          }
        }
      }   
      ?>
    </div>
  </div>
  <?php 
  if($user_id =='')
  {
    ?>
    <div class="blue_h innerblue_h">
    <?php 
  }
  else
  {
    ?>
    <div class="blue_h innerblue_h">
    <?php 
  }
  ?>
    <div class="navbar-wrapper wow fadeInDown">
      <?php if($user_id == '') 
      {
        ?>
        <div class="cbp-af-header" id="cbp-af-header">
        <?php 
      }
      else
      {
        ?>
        <div class="cbp-af-header" id="cbp-af-header1">
        <?php 
      }
      ?>
          <div class="cbp-af-inner">
            <nav class="navbar navbar-inverse navbar-static-top">
              <div class="container">           
                <div class="tophead-resp-md"> 
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                      data-target="#navbar_p" aria-expanded="false" aria-controls="navbar"> 
                      <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
                      <span class="icon-bar"></span> <span class="icon-bar"></span> 
                    </button>
                    <a href="javascript:;" 
                    id="selsearch1" class="select-style1 hidden-md hidden-sm"> </a>
                  </div>
                  <div class="row">              
                    <div class="col-md-5 col-sm-5 col-xs-3 cls_menucont">
                      <div id="navbar_p" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                          <li class=""><a href="<?php echo base_url();?>cupom">Destaques</a></li>
                          <li><a href="<?php echo base_url();?>cupom-desconto">Coupons</a></li>
                          <?php
                          /*$enable_shopping = $getadmindetails[0]->enable_shopping;
                          if($enable_shopping == 1)
                          {
                            ?>  
                            <li><a href="<?php echo base_url();?>produtos">Ofertas</a></li>
                            <?php
                          }*/
                          ?>
                          <li>
                            <a href="javascript:;" id="selsearch" class="select-style1 hidden-xs">
                              <div class="select">
                                  buscar lojas
                              </div>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-5 toplogo-md">
                      <center>
                        <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/logo.png"></a>
                      </center>
                    </div> 
                    <?php
                    if($user_id == '')
                    {
                      ?>
                      <div class="col-md-5 col-sm-5 col-xs-4 pull-right">
                        <div class="sign-topblk">
                          <ul class="nav navbar-nav pull-right">
                            <li><a class="hidden-xs" href="#register" data-toggle="modal">Sign up</a></li>
                            <li><a href="#login" data-toggle="modal">Login</a></li>                       
                          </ul>
                        </div>
                      </div>
                      <?php
                    }
                    else
                    {
                      ?>
                      <!-- New design for my account menu 14-7-16-->
                      <div class="col-md-5 col-sm-5 col-xs-12 topuserdet">
                        <div id="" class="pro_im clearfix">
                          <ul class="nav navbar-nav pull-right col-md-6 col-sm-10 col-xs-12">
                            <li class="dropdown top-login">
                              <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <div class="pro_pic">
                                  <?php 
                                  if($profile_photo =='')
                                  {
                                  ?>
                                  <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/profile_pic.png" class="img-responsive">
                                  <?php
                                  }
                                  else
                                  {
                                  ?>
                                    <img style="border-radius: 16px;" class="img-responsive" alt="" src="<?php echo $profile_photo; ?>">
                                  <?php
                                  }
                                  ?>
                                </div>
                                <div class="pro_piccont">
                                      <p class="usr-log"> <?php echo $username;?> <span class="caret pull-right"></span></p> 
                                      <p class="usr-log1">R$<?php echo $balance; ?>  <span class="usr-log2"> R$<?php echo $total_amt; ?></span> </p>
                                </div>
                              </a>
                              <ul class="dropdown-menu">
                                <center><h4>Visao geral</h4></center>
                                <center>
                                  <ul class="appr-listblk list-inline clearfix">
                                    <li>
                                      <a href="<?php echo base_url();?>minha-conta">
                                        <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr1.png" alt="app" class="img-responsive center-block">
                                        dados
                                      </a>
                                    </li>
                                    <li>
                                      <a href="<?php echo base_url();?>extrato">
                                        <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr2.png" alt="app" class="img-responsive center-block">
                                        Extrato
                                      </a>
                                    </li>
                                    <li>
                                      <a href="<?php echo base_url();?>resgate">
                                        <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr3.png" alt="app" class="img-responsive center-block">
                                        Resgate
                                      </a>
                                    </li>
                                    <li>
                                      <a href="<?php echo base_url();?>indique-e-ganhe">
                                        <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr4.png" alt="app" class="img-responsive center-block">
                                        Indicacoes
                                      </a>
                                    </li>
                                  </ul>
                                </center>
                                <center><h4>Facilidades</h4></center>
                                <ul class="appr-listblk1 list-inline clearfix">
                                  <li>
                                    <a href="#">
                                      <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr5.png" alt="app" class="img-responsive pull-left">
                                       <span> Lembrador </span>
                                    </a>
                                  </li>                       
                                  <li class="pull-right">
                                    <a href="#">
                                      <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr6.png" alt="app" class="img-responsive pull-left">
                                      <span> Aplicativo </span>
                                    </a>
                                  </li>
                                </ul>
                                <ul class="appr-listbtnblk1 list-inline clearfix">
                                  <li>
                                    <button class="btn btn-appr" type="button"> <img alt="chat" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr-chat.png" class="img-responsive"> Ajuda </button>
                                  </li>
                                  <li class="pull-right">
                                    <button onclick="window.location.href='<?php echo base_url();?>logout'" class="btn btn-appr" type="button"> <img alt="chat" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr-logout.png" class="img-responsive"> Sair </button>
                                  </li>
                                </ul>
                              </ul>  
                            </li>
                          </ul>
                        </div>
                      </div>  
                      <!-- End New my account menu -->
                      <?php
                    } 
                    ?>
                  </div>              
                </div>
              </div>  
              <!-- New code for search 6-8-16 -->
              <div class="search_content" id="search_content" style=" background-color: transparent; display:none;">
                <div class="container">
                  <?php
                  if((strpos($_SERVER['REQUEST_URI'],'cashback/shopping') == true))
                  {
                    ?>
                    <form class="search-area form-group search-area-white search-style" action="<?php echo base_url();?>cashback/change_location" method="POST">
                      <div class="wrap-top">
                        <div class="row">
                          <div class="col-md-11 clearfix">
                            <div class="search-area-division search-area-division-input">
                              <input type="text" required id="Location" placeholder="Pesquise Lojas ou Ofertas" class="form-control" name="location" 
                              value="<?php if($this->session->userdata('cityname')){ echo $cityname = $this->session->userdata('cityname');}?>" data-provide="typeahead">
                            </div>
                          </div>
                          <div class="col-md-1">
                            <button class="btn btn-block btn-white search-btn pop" type="submit">
                              <i class="fa fa-search"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </form>
                    <?php
                  }
                  else
                  {  
                    if(isset($_POST['storehead']))
                    {
                      $storehead  = $_POST['storehead'];
                    } 
                    ?>
                    <!--New search form-->
                    <form id="search_mini_form" class="search-area form-group search-area-white search-style"  onsubmit="return submit_form();" method="get">
                      <div class="wrap-top">
                        <div class="row">
                          <div class="col-md-11 col-sm-10 col-xs-9 clearfix">
                            <div class="search-area-division search-area-division-input">
                              <input id="search" required type="text" name="store" class="form-control cls_searchtbox search_header" value="<?php if(isset($_POST['storehead'])){echo $storehead;}?>" placeholder="Aliexpress, Submarino, Netshoes" data-provide="typeahead" style="width:102% !important;"/>
                            </div>
                          </div>  
                          <div class="col-md-1 col-sm-2 col-xs-3 search-botao">
                            <button type="submit" class="btn btn-block btn-white cls_searchtbtn search-btn pop" style="float:right; margin-right: 17px;"><i class="fa fa-search"></i>
                            </button> 
                          </div>
                          <div id="search_autocomplete" class="search-autocomplete">
                          </div>
                        </div>        
                      </div>
                    </form>
                    <form id="dummyform" method="post">
                        <input type="hidden" name="storehead" id="storehead" value="">
                    </form>
                    <?php
                  } 
                  ?>
                </div>
              </div>
              <div class="search_content" id="search_content1" style=" background-color: transparent; display:none;">
                <div class="container">
                  <?php
                  if((strpos($_SERVER['REQUEST_URI'],'cashback/shopping') == true))
                  {
                    ?>
                    <form class="search-area form-group search-area-white search-style" action="<?php echo base_url();?>cashback/change_location" method="POST">
                      <div class="wrap-top">
                        <div class="row">
                          <div class="col-md-11 clearfix">
                            <div class="search-area-division search-area-division-input">
                              <input type="text" required id="Location" placeholder="Pesquise Lojas ou Ofertas" class="form-control" name="location" 
                              value="<?php if($this->session->userdata('cityname')){ echo $cityname = $this->session->userdata('cityname');}?>" data-provide="typeahead">
                            </div>
                          </div>
                          <div class="col-md-1">
                            <button class="btn btn-block btn-white search-btn pop" type="submit">
                              <i class="fa fa-search"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </form>
                    <?php
                  }
                  else
                  {  
                    if(isset($_POST['storehead']))
                    {
                      $storehead  = $_POST['storehead'];
                    } 
                    ?>
                      <form id="search_mini_form" class="search-area form-group search-area-white search-style"  onsubmit="return submit_form();" method="get">
                        <div class="wrap-top">
                          <div class="row">
                            <div class="col-md-11 col-sm-10 col-xs-9 clearfix">
                              <div class="search-area-division search-area-division-input">
                                <input id="search" required type="text" name="store" class="form-control cls_searchtbox search_header searchs" value="<?php if(isset($_POST['storehead'])){echo $storehead;}?>" placeholder="Aliexpress, Submarino, Netshoes" data-provide="typeahead" style="width:102% !important;"/>
                               
                              </div>
                            </div>  
                            <div class="col-md-1 col-sm-2 col-xs-3 search-botao">
                              <button type="submit" class="btn btn-block btn-white cls_searchtbtn search-btn pop" style="float:right; margin-right: 17px;"><i class="fa fa-search"></i>
                              </button> 
                            </div>
                            <div id="search_autocomplete" class="search-autocomplete">
                            </div>
                          </div>        
                        </div>
                      </form>
                       
                      <form id="dummyform" method="post">
                          <input type="hidden" name="storehead" id="storehead" value="">
                      </form>
                      <?php
                  } 
                  ?>
                </div>
              </div> 
              <!-- End -->
            </nav>
          </div>
        </div>
      </div>
    </div>

    <?php 
    $pagename         = $this->uri->segment(1);
    $cat_name         = $this->uri->segment(2);
    if($cat_name!='')
    {
      $store_detailss   = $this->front_model->get_store_details($cat_name);
      //$categorydetails  = $category_details = $this->front_model->get_category_details($cat_name); 
    }
    $store_category   = $store_detailss->store_categorys;  
    if(strpos($store_category, ','))
    {
      $store_categories = explode(",",$store_category);
      foreach($store_categories as $newstorecategories) 
      {
         $newstorecategories;
      } 
    }
    else
    {
      $newstore_categories = $store_category; 
    }
    $stores_list = $this->front_model->get_category_details_byid($newstore_categories);
    ?>
    <?php 
    if($user_id =='')
    {
      ?>
      <div class="page-intro">
        <?php 
    }
    else
    {
      ?>
      <div class="page-intro">
        <?php 
    }
    ?>
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <ul class="breadcrumb">
            <li><a class="link-breadcrumb" href="<?php echo base_url();?>">Início</a></li>
            <?php 
            $pagenames = str_replace('-', ' ', $pagename);
            if($pagenames)
            {
              $pagenamess = ucwords($pagenames); 
            }
            else
            {
              $pagenamess = ucwords($pagename);
            }
            if($pagename === 'cupom-desconto')
            { 
              ?>
              <li><a class="link-breadcrumb" href="<?php echo base_url()."cupom-desconto/".$stores_list->category_url;?>"><?php echo ucfirst($stores_list->category_name);?></a></li>
              <li class="">Cupom Desconto <?php echo $store_detailss->affiliate_name;?></a></li>
              <?php
            }
            ?>
          </ul>
        </div>
      </div>
    </div>
  </div>