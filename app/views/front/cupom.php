<?php $this->load->view('front/header'); ?>
<!-- header content End -->
<style type="text/css">
  @media screen and (min-width:768px) {
.box1 .cashsubblk .tear {
border: 10px solid rgba(0, 0, 0, 0.1);
height: 86px;
width: 84px;
}
.box1 .cashsubblk .tear > span {
background: #fff none repeat scroll 0 0;
height: 75px;
left: 8px;
line-height: 65px;
text-align: center;
vertical-align: middle;
width: 70px;
}
.box1 .tear img {
height: auto;
width: 100%;
}
.cashboxmain:hover .box1 .cashsubblk .annie {margin-top:20px;}
.cashboxmain .box1 .annie .tear span img.first, .cashboxmain:hover .box1 .annie .tear span img.first, .cashboxmain .box1 .annie .tear span img.last, .cashboxmain:hover .box1 .annie .tear span img.last { transition:none;}
}

</style>
<!-- Main Content start -->
<!-- Top cashback content starts -->
<div class="clearfix wow bounceIn topcashbanblk">
  <?php 
  /*if($admindetails->topcashback_type =='topcashback_image')
  {
  ?>
    <div class="container-fluid pad-no topcash-bann" style="background-image:url(<?php echo base_url();?>uploads/adminpro/<?php echo $admindetails->topcashback_image;?>);height: 300px !important;">
    <?php 
  }*/
  if($admindetails->topcashback_type =='topcashback_image')
  {
    if($admindetails->top_back_img_settings =='top_zoomimage')
    { 
      ?>
      <section class="wow fadeInDown storeblkmain" style="background: rgba(0, 0, 0, 0) url(<?php echo $this->front_model->get_img_url();?>uploads/adminpro/<?php echo $admindetails->topcashback_image;?>) repeat-y scroll left center / cover; height: 300px !important;">
      <?php
    }
    else
    {
      ?>
      <section class="wow fadeInDown storeblkmain" style="background: rgba(0, 0, 0, 0) url(<?php echo $this->front_model->get_img_url();?>uploads/adminpro/<?php echo $admindetails->topcashback_image;?>);height: 300px !important;">
      <?php 
    }
  }

  if($admindetails->topcashback_type =='topcashback_color')
  {
    ?>
      <div style="background-color:<?php echo $admindetails->topcashback_color;?>;background-size: 100% 100%;background-repeat:no-repeat; height: 300px !important;" class="container-fluid pad-no topcash-bann">
      <?php
  }
  ?>
  <div class="container">
          <div class="cabecalho-loja"  style="left:40% !important;">
          <img  class="img-responsive center-block rectanguler-logo" alt="" src="<?php echo $this->front_model->get_img_url();?>uploads/adminpro/<?php echo $admindetails->image_topcashback;?>">      
        </div>
        </div>
</section>
  

<section class="cms wow fadeInDown">
  <div class="clearfix wow fadeInDown">
    <div class="container" style="margin-bottom:20px;">
      <div class="heading wow bounceIn">
        <h2> Top <span>Cashback</span></h2>
        <div class="heading_border_cms"> <span><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/top_drop.png"></span> </div>
      </div>

    </div>
    <section class="cashback"> 
      <?php
      if($stores_list)
      {
      ?>
        <div class="container">
          <div class="row">

          <?php
          $k=1;
          foreach($stores_list as $stores)
          {
            $affiliate_id     = $stores->affiliate_id;
            $featured         = $stores->featured;
            $affiliate_name   = $stores->affiliate_name;
            $count_coupons    = $this->front_model->count_coupons($affiliate_name);
            $get_coupons_sets = $this->front_model->get_coupons_sets($affiliate_name,2);
            
            if($featured!=0)
            {
              $setup = "feature";
              $namess = "Feature"; //heading
              $colors_li= 'style="background-color:#e74955;"';
            }
            else
            {
              $store_of_week = $stores->store_of_week;
              if($store_of_week!=0)
              {
                $setup    = "store";
                $namess   = "Store of the Week "; //heading
                $colors_li= 'style="background-color:#a5d16c;"';
              }
              else
              {
                $setup     = "offers";
                $namess    = "Offers "; //heading
                $colors_li = 'style="background-color:#32c2cd;"';
              }
            }
            ?>
            <div class="col-md-4 col-sm-6 col-xs-12 wow slideInLeft <?php echo $setup;?>"  style="visibility: visible; animation-name: slideInLeft;">
              <div class="cashboxmain">
                <a href="<?php echo base_url();?>cupom-desconto/<?php echo $stores->affiliate_url;?>">
                <div class="imgwrap"> <img style="width:100px; height:200px;" class="img-responsive center-block" alt="<?php echo base_url(); ?>/uploads/sidebar_image/<?php echo $stores->sidebar_image;?>" src="<?php echo $this->front_model->get_img_url(); ?>uploads/sidebar_image/<?php echo $stores->sidebar_image;?>">
                  <div class="rollover">
                    <div class="roll-inner">
                      <div class="roll-content">
                        <h3 data-toggle="modal" data-target="#decolar"><?php echo $stores->affiliate_name; ?></h3>
                      </div>
                    </div>
                  </div>
                </div>
                </a>

                <div class="box1">
                  <div class="row cashsubblk">
                    <div class="col-md-5 col-sm-5 col-xs-6">
                      <div class="annie">
                        <div class="tear">
                          <!-- 
                          <span>
                            <img class="first" src="<?php echo base_url(); ?>/uploads/affiliates/<?php echo $stores->affiliate_logo;?>">
                            <div data-toggle="modal" data-target="#decolar_<?php echo $stores->affiliate_url;?>">
                              <img class="last" src="<?php echo base_url(); ?>/front/images/link1.png">
                            </div>
                          </span>
                           -->
                          <span>
                            <div class="vin_cha_drop">
                              <img class="first" src="<?php echo $this->front_model->get_img_url(); ?>uploads/affiliates/<?php echo $stores->affiliate_logo;?>">
                              <div data-toggle="modal" data-target="#decolar_<?php echo $stores->affiliate_url;?>">
                                <img class="last" src="<?php echo $this->front_model->get_img_url(); ?>front/images/link1.png">
                              </div>
                            </div>
                          </span> 
                      </div>
                    </div>
                    </div>
                    <div class="col-md-7 col-sm-5 col-xs-6">
                      <a href="<?php echo base_url();?>cupom-desconto/<?php echo $stores->affiliate_url;?>">
                      <p><b><?php if($stores->cashback_percentage)
                          {  
                            if($stores->affiliate_cashback_type=="Percentage")
                            {
                              $cppercentage = $stores->cashback_percentage."%";
                            }
                            else
                            {
                              $cppercentage = "R$. ".$stores->cashback_percentage;
                            }
                            echo $cppercentage." de Volta "; ?>
                                          <?php 
                             echo "<br>".$stores->sidebar_image_url;
                          }
                          else
                          {
                            echo "Best Offers ";
                          }
                    
                          /*if($count_coupons->counting!=0 && $count_coupons->counting!='')
                          {
                            if($stores->cashback_percentage)
                            {
                              echo '&';
                            }
                            echo $count_coupons->counting;?> Coupons
                            <?php
                          }*/
                          ?>
                          </b></p>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="col-md-4 col-sm-6 col-xs-12 wow slideInLeft <?php echo $setup;?>">
              <div class="cashboxmain">
                <a href="<?php echo base_url();?>cupom-desconto/<?php echo $stores->affiliate_url;?>">
                  <div class="imgwrap">
                    <img class="img-responsive center-block" style="width:360px !important; height:270px !important;" src="<?php echo base_url(); ?>/front/new/images/cash1.png">
                    <div class="rollover">
                      <div class="roll-inner">
                        <div class="roll-content">
                          <h3 data-toggle="modal" data-target="#decolar">
                            <?php echo $stores->affiliate_name; ?>
                          </h3>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
                <div class="box1">
                  <div class="row cashsubblk">
                    <div class="col-md-5 col-sm-5 col-xs-6">
                      <div class="annie">
                        <div class="tear">
                        <span> 
                        <img class="first" style="width:70px !important; margin-left: 10px; height:50px !important;" src="<?php echo base_url(); ?>uploads/affiliates/<?php echo $stores->affiliate_logo;?>"> 
                        <h3 data-toggle="modal" data-target="#decolar_<?php echo $stores->affiliate_url;?>">
                          <img class="last" src="<?php echo base_url(); ?>/front/new/images/link1.png"> 
                        </h3>
                        </span> 
                        </div>
                      </div>
                    </div>
                    <div class="col-md-7 col-sm-5 col-xs-6">
                       <a href="<?php echo base_url();?>cupom-desconto/<?php echo $stores->affiliate_url;?>">
                      <p style="font-size:15px !important;">
                        <b>
                          <?php if($stores->cashback_percentage)
                          {  
                            if($stores->affiliate_cashback_type=="Percentage")
                            {
                              $cppercentage = $stores->cashback_percentage."%";
                            }
                            else
                            {
                              $cppercentage = "R$. ".$stores->cashback_percentage;
                            }
                            echo "Get Up to ".$cppercentage;?>
                                          <?php 
                            echo "Cashback ";
                          }
                          else
                          {
                            echo "Best Offers ";
                          }
                    
                          if($count_coupons->counting!=0 && $count_coupons->counting!='')
                          {
                            if($stores->cashback_percentage)
                            {
                              echo '&';
                            }
                            echo $count_coupons->counting;?> Coupons
                            <?php
                          }
                          ?>
                          from <?php $admindetails = $this->front_model->getadmindetails_main(); echo $admindetails->site_name;?>
                        </b>
                      </p>
                      </a>
                    </div>
                  </div>
                </div>
                 
              </div>
            </div> -->
           
            <!-- Model popup start -->
              <div class="modal fade wow bounceIn prod-descpop" id="decolar_<?php echo $stores->affiliate_url;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header login-popuphead"> <a class="login_tit"><span aria-hidden="true"><img src="<?php echo $this->front_model->get_img_url();?>front/new/images/lock.png"></span></a>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span> </button>
                      <h4 class="modal-title" id="myModalLabel"><?php echo $stores->affiliate_url;?></h4>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="form-group enve"> 
                          <center>
                            <h4>About <?php echo $stores->affiliate_url;?></h4>
                            <p><?php echo $stores->affiliate_desc;?></p>
                          </center>
                        </div>    
                        <!-- <center>
                          <button type="submit" class="btn btn-signin">Add to cart</button>
                        </center>    
                        <center>
                          <button type="button" class="btn btn-signin btn-danger" data-dismiss="modal"><span>Cancel</span></button>
                        </center>    --> 
                      </form>
                    </div>
                  </div>
                </div>
              </div>
          <!-- Model popup End -->

            <?php
            $k++;
          }
          ?>
          </div>
        </div>
      <?php
      }
      else
      {
      ?>
        <div class="container">
          <div class="row">
              <center>
                  <strong>No Stores are available at this category!</strong>
              </center>
          </div>
        </div>  
      <?php
      }  
      ?>  
    </section>
  </div>
  </div>
</section>

<!-- Top cashback content End -->

<!-- Main Content end -->

 <!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
 
<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>
<!-- Footer Menu End-->