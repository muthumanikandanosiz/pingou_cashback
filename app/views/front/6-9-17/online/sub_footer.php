<?php $admindetails = $this->front_model->getadmindetails_main();
$user_id     = $this->session->userdata('user_id');
$userdetails = $this->front_model->userdetails($user_id); 
?>
<footer>
  <!-- ========================== -->
  <!-- FOOTER - FOOTER -->
  <!-- ========================== -->
  <section class="footer-section innerfoot-sect">
    <div class="container">
      <div class="clearfix wow fadeInDown">
      <div class="wow fadeInDown">
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Help And Support</h5>
          <ul class="footer-nav">
            <li><a href="<?php echo base_url();?>contato"><i class="fa fa-caret-right"></i> Contact Us</a></li>
            <li><a href="<?php echo base_url();?>perguntas-frequentes"><i class="fa fa-caret-right"></i> Perguntas Frequentes</a></li>
            <?php 
            $result = $this->front_model->sub_menu();
            foreach($result as $view)
            {
            ?>
              <li><a href="<?php echo base_url();?><?php echo $view->cms_title; ?>"><i class="fa fa-caret-right"></i> <?php echo $view->cms_heading;?></a></li>
            <?php 
            }
            ?>
            <li><a href="<?php echo base_url(); ?>sitemap.xml"><i class="fa fa-caret-right"></i> Site map</a></li>
          </ul>
        </div>
        <?php 
        if($user_id== '')
        {
          ?>
          <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Email Newsletter</h5>
          <ul class="footer-nav">
            <li><a href="#"><i class="fa fa-caret-right"></i> Subscribe to recieve inspiration, ideas and news in your inbox.</a></li>
            <li>
              <div class="col-md-12 col-sm-12 col-xs-12 pd-left-0">
              <input type="text" id="news_email" onkeypress="clears(2);" class="form-control tbox1" placeholder="Email Address">
              <input type="button" class="tbtn1" value="send" onClick="email_news();">
              </div>
              &nbsp;
              <span id="new_msg" style="color:red"></span>
            </li>
          </ul>
          </div>
          <?php
        }
        ?>  


        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Top Categories</h5>
          <ul class="footer-nav">
            <?php 
            $categories = $this->front_model->get_all_categories(8);
            foreach($categories as $view)
            {
            ?> 
            <li><a href="<?php echo base_url();?>ofertas/<?php echo $view->category_url;?>"><i class="fa fa-caret-right"></i> <?php echo $view->category_name;?></a></li>
            <?php
            }
            ?>
          </ul>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Top Stores</h5>
          <ul class="footer-nav">
          <?php 
          $stores = $this->front_model->get_all_stores(8);
          foreach($stores as $view)
          {
          ?>
            <li><a href="<?php echo base_url(); ?>cupom-desconto/<?php echo $view->affiliate_url?>"><i class="fa fa-caret-right"></i> <?php echo $view->affiliate_name; ?></a></li>
          <?php
          }
          ?>
          </ul>
        </div>
      </div>
      </div>
    </div>
  </section>
  <section class="copyright-section">
    <div class="container">
     <div class="row wow fadeInDown">
      <div class="col-md-6 col-sm-6 col-xs-12 wow flipInX">
        <p>Copyright &copy; <?php echo date('Y')?> <?php echo $admindetails->site_name;?>  |  All Rights Reserved </p>
      </div>
      <?php
      $listing = $this->db->query("select * from admin")->row();
      ?>
      <div class="col-md-6 wow flipInX">
        <ul class="list-socials pull-right">
          <?php if($listing->admin_twitter) {?> <li><a href="<?php echo $listing->admin_twitter;?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
          <?php if($listing->admin_fb)      {?> <li><a href="<?php echo $listing->admin_fb;?>">     <i class="fa fa-facebook"></i></a></li><?php } ?>
          <?php if($listing->admin_gplus)   {?> <li><a href="<?php echo $listing->admin_gplus;?>">  <i class="fa fa-google-plus"></i></a></li><?php } ?>
        </ul>
      </div>
     </div>
    </div>
  </section>
</footer>

<?php $this->load->view('front/js_scripts');?>
<?php 
 
//print_r($popup_templete);
$admin_number  = $admindetails->contact_number;
$site_logo     = $this->front_model->get_img_url()."uploads/adminpro/".$admindetails->site_logo;
$site_name     = $admindetails->site_name;

if($user_id == '')
{
  $popup_templete = $this->db->query("SELECT * from exit_popup where all_status=1")->row(); 
  $url            = base_url();
  $all_content    = $popup_templete->popup_template;
 

  $data = array(
  '###ADMINNO###'=>$admin_number,
  '###COMPANYLOGO###'=>$site_logo,
  '###SITE NAME###'=>$site_name,
  );
  
  $contents = strtr($all_content,$data);
  
  if($popup_templete->all_status == 1)
  {
    ?>
    <!-- <div class="modal fade cus_modal" id="myModal" counts="0" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none!important;">
      <div class="modal-dialog" style="width:604px !important;">
        <div class="modal-content" id="newcontent" style="height:545px;">
          <div class="modal-header">
            <button aria-hidden="true"  data-dismiss="modal" class="close cls_popup" type="button">×</button>
          </div>
          <div class="modal-body-default">
            <center> -->
            <?php 
              //echo $contents;
            /*New code for shortcut details content 26-5-17*/
            echo $this->front_model->shortcut_details($contents);//$newcontent  = strtr($contents,$short_data);
            /*End 26-5-17*/
            ?>
            <!-- </center>              
          </div>
        </div>
      </div>
    </div> -->
    <?php
  }   
}
?>

<!-- new code for First access popup details 20-3-17 -->
<?php 
$popup_templete    = $this->db->query("SELECT * from first_access_popup where all_status_firstpopup=1")->row(); 
$first_acc_content = $popup_templete->first_popup_template;

$datas = array(
  '###ADMINNO###'=>$admin_number,
  '###COMPANYLOGO###'=>$site_logo,
  '###SITE NAME###'=>$site_name,
  );

$first_acc_popup    = strtr($first_acc_content,$datas);
$first_popup_status = $popup_templete->all_status_firstpopup;

/*New code for shortcut details content 26-5-17*/
//$first_acc_popup  = strtr($first_acc_popup,$short_data);
/*End 26-5-17*/


if($user_id!='')
{
  if($first_popup_status == 1)
  {
    if($userdetails->first_ac_popup_status == 1)
    { 
      echo $this->front_model->shortcut_details($first_acc_popup);
      ?>
      <style type="text/css">
      .modal-backdrop.fade
      {
        display: block !important;
      }
      </style>
      <script type="text/javascript">
          
         
          $('#first_acc_popup').modal('show');    
          //$('.modal-backdrop').css('display','block');
          //$('.modal-backdrop').css('display','block');
          $("#first_acc_popup").on("click", function(e)
          {
            $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>cashback/update_firstpoup_satus",
            data: {'poup_status':0},
            cache: false,
            success: function(result)
            {
              if(result!=1)
              {
                return false;
              }
              else
              {
                <?php $redirect_urlset =  base_url(uri_string());?>
                //window.location.href = '<?php echo $redirect_urlset; ?>';
                return false;          
              }             
            }
            });
            return false;
          }); 
            
         
      </script>

      <script type="text/javascript">
        /*$(document).keyup(function(e) 
        {    
            if (e.keyCode == 27) 
            {
              $('.modal-backdrop').css('display','none'); 
            }
        });*/
      </script>
      <?php 
      $data = array(
        'first_ac_popup_status' => 0,
      );

      $this->db->where('user_id',$user_id);
      $update_qry = $this->db->update('tbl_users',$data);
    }
  }
}

?>
<!-- End 20-3-17 -->


<!-- New code for popup details 18-11-16 -->
<?php include('popup_details.php'); ?>
<!-- End 18-11-16 -->

<!-- New code for onesignal notification details 30-8-17 -->
<?php
$onesignal_sess_count    = $userdetails->onesignal_sess_count; 
$onesignal_popup_details = $this->db->query("select * from popup_details where popup_id=7")->result();

if($onesignal_popup_details[0]->popup_status == 1)
{
  if($onesignal_popup_details[0]->dont_show_status == 0)
  {
    if($user_id!='') 
    {
      if($onesignal_popup_details[0]->popup_content > $onesignal_sess_count)
      {
        ?>
        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
        <script>
          setTimeout(function()
          {
            
            /*function activate_notify() {
                  // OneSignal.push(["registerForPushNotifications"]);
                  OneSignal.push(["registerForPushNotifications"]);
                  event.preventDefault();
            }
            function unsubscribe(){
                OneSignal.setSubscription(true);
            }*/

            var OneSignal = window.OneSignal || [];
            OneSignal.push(["init", {
            appId: "71c56ef2-c81e-44be-b9f0-dfdb770ecd43",
            autoRegister: false,
            promptOptions: 
            {
              /* These prompt options values configure both the HTTP prompt and the HTTP popup. */
              /* actionMessage limited to 90 characters */
              //actionMessage: "Não perca uma oportunidade.",
              actionMessage: "Quer que a gente te avise quando aparecer alguma super promoção, ou cashback em dobro, neste navegador?",
              //Não perca uma oportunidade! 
              //Quer que a gente te avise quando aparecer alguma super promoção, ou cashback em dobro, neste navegador?",
              
              /* Example notification title */
              exampleNotificationTitle: 'Não perca uma oportunidade!',

              /* acceptButtonText limited to 15 characters */
              acceptButtonText: "Ativar",
              /* cancelButtonText limited to 15 characters ã*/
              cancelButtonText: "Agora não" 
            },
            httpPermissionRequest: 
            {
              enable: true
            },
            notifyButton: 
            {
              enable: false /* Set to false to hide */
            }
            }]);

         
            var OneSignal = window.OneSignal || [];
            OneSignal.push(function() {
            /* These examples are all valid */
            // Occurs when the user's subscription changes to a new value.
            OneSignal.on('subscriptionChange', function (isSubscribed) {
            console.log("The user's subscription state is now:", isSubscribed);
            OneSignal.sendTags({"user_id":"<?php echo $user_id;?>","category":"<?php echo $referral_category_type;?>"}, function(tagsSent)
            {
              // Callback called when tags have finished sending
              console.log("Tags have finished sending!");
            });
            });

            var isPushSupported = OneSignal.isPushNotificationsSupported();
            if (isPushSupported)
            {
              // Push notifications are supported
              OneSignal.isPushNotificationsEnabled().then(function(isEnabled)
              {
                if (isEnabled)
                {
                  //OneSignal.registerForPushNotifications({
                  //modalPrompt: true
                  
                  OneSignal.showHttpPrompt();
                  console.log("Push notifications are enabled!");
                } else {
                  OneSignal.showHttpPrompt();
                  console.log("Push notifications are not enabled yet.");
                }
              });
            }
            else
            {
              console.log("Push notifications are not supported.");
            }
            });
          },<?php echo $onesignal_popup_details[0]->session_count;?>000);
        </script>
        <?php
      }   
    }
  }  
}    
?>
<!-- End 30-8-17 -->
<script type="text/javascript">
function activate_notify(e)
{
    alert(e.value);
    if(e.value=="ATIVAR")
    {
      e.value = "DESATIVAR";
      OneSignal.push(["registerForPushNotifications"]);
      event.preventDefault(); 
    } 
    else
    {
      e.value = "ATIVAR"; 
      OneSignal.setSubscription(true);
    } 
}

</script>

</body>
</html>


