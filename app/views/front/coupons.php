<?php
/*$str = '12/24/2013';
$date = DateTime::createFromFormat('m/d/Y', $str);
echo $date->format('Y-m-d'); // => 2013-12-24*/
 $user_id = $this->session->userdata('user_id');
 
?>
<!DOCTYPE html>
<html lang="en"><head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php $admis = $this->front_model->getadmindetails_main(); echo $admis->site_name; ?> - <?php echo $this->lang->line('Coupons');?></title>
    
<!-- Bootstrap -->

<?php $this->load->view('front/css_script'); 
$admindetailss = $this->front_model->getadmindetails();

?>	

<link href="<?php echo base_url();?>front/css/hover.css" rel="stylesheet" type="text/css">
<style>


/* body {
	font-family: 'PT Sans', sans-serif;
	font-size:13px;
	line-height:20px;
	background:url(<?php echo base_url();?>front/images/body-bg_store.png) left top repeat;
} */
.loadedcontent {min-height: 1200px; }
.cus_modal .modal-header-default > .lead3 p {
    background-color: #eef6fc;
    border-radius: 4px;
    line-height: 18px;
    padding: 10px 10px 10px 35px;
}

.cus_modal .modal-header-default > .lead3 {
    margin-top: 15px;
}
.cus_modal .modal-header-default > div:first-child h3 {
    color: #1d7bce;
    font-size: 25px;
    line-height: 25px;
	font-weight:bold;
}
.cus_modal .voucher-code p {
    color: #004a86;
    font-size: 16px;
    font-weight: 700;
    margin: 0;
    padding-bottom: 10px;
}
.cus_modal .voucher-code {
    text-align: center;
}
.cus_modal .voucher-code, .cus_modal .follow-retailer, .cus_modal .refer {
    border-radius: 4px;
    margin: 0 0 25px;
    padding: 10px;
}
.cus_modal .voucher-code span {
    background: url("//static.quidco.com/v3/assets/img/common/modal/voucher-icon.png") no-repeat scroll 6px 2px transparent;
    border: 1px dashed #f00;
    display: block;
    font-size: 16px;
    font-weight: 700;
    margin: 0;
    padding: 4px 0;
}
.copy-medium, .copy-medium p, ul.copy-medium li, ol.copy-medium li {
    font-size: 14px;
    line-height: 22px;
}

.copy-medium .modal-body p {
    font-size: 16px;
    font-weight: 300;
    line-height: 24px;
}
.copy-medium .alert-info {
    background-color: #f4f8fd;
    border-color: #1d7bce;
    color: #444;
	
}

</style>

</head>



<body>

<?php $this->load->view('front/header'); ?>

  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
      	<div class="col-xs-12">
		<ul>
		  <li class="home">  <a href="<?php echo base_url(); ?>" title="<?php echo $this->lang->line('go_to_home_page');?>"><?php echo $this->lang->line('home');?></a> <span> <i class="fa fa-angle-double-right"></i> </span> </li>
		  <li class="category34">  <strong><?php echo $this->lang->line('Coupons');?></strong>  </li>
	  	</ul>
	  </div><!--col-xs-12-->
      </div> <!--row-->
    </div> <!--container-->
  </div>
  
  <!-- Home Slider Block -->
  <div id="magik-slideshow" class="magik-slideshow">
    <div class="container">
      <div class="row">
		<div class="col-md-3 col-sm-3">
			<div id="magik-verticalmenu" class="block magik-verticalmenu">
				<?php $headerimg1 = $this->front_model->getads('Sidebar-1'); ?>
				<a href="<?php echo $headerimg1->ads_url?>">
					<img src="<?php echo base_url(); ?>uploads/ads/<?php echo $headerimg1->ads_image?>" class="img-responsive mar-bot">
				</a> 
				<?php $headerimg2 = $this->front_model->getads('Sidebar-2'); ?>
				<a href="<?php echo $headerimg2->ads_url?>">
					<img src="<?php echo base_url(); ?>uploads/ads/<?php echo $headerimg2->ads_image?>" class="img-responsive mar-bot">
				</a> 
			</div>
		</div>
        <div class="col-lg-9 col-sm-12 col-md-9">
          <div class="filters mar-bot">
            <ul class="nav nav-pills">
              <li class="active"><a data-filter="*" href="#" class="select"><?php echo $this->lang->line('All');?></a></li>
             <li class=""><a data-filter=".A"  href="#" class="select"><?php echo $this->lang->line('A');?></a></li>
              <li class=""><a data-filter=".B"  href="#" class="select"><?php echo $this->lang->line('B');?></a></li>
              <li class=""><a data-filter=".C"  href="#" class="select"><?php echo $this->lang->line('C');?></a></li>
              <li class=""><a data-filter=".D"  href="#" class="select"><?php echo $this->lang->line('D');?></a></li>
              <li class=""><a data-filter=".E"  href="#" class="select"><?php echo $this->lang->line('E');?></a></li>
              <li class=""><a data-filter=".F"  href="#" class="select"><?php echo $this->lang->line('F');?></a></li>
              <li class=""><a data-filter=".G"  href="#" class="select"><?php echo $this->lang->line('G');?></a></li>
              <li class=""><a data-filter=".H"  href="#" class="select"><?php echo $this->lang->line('H');?></a></li>
              <li class=""><a data-filter=".I"  href="#" class="select"><?php echo $this->lang->line('I');?></a></li>
              <li class=""><a data-filter=".J"  href="#" class="select"><?php echo $this->lang->line('J');?></a></li>
              <li class=""><a data-filter=".K"  href="#" class="select"><?php echo $this->lang->line('K');?></a></li>
              <li class=""><a data-filter=".L"  href="#" class="select"><?php echo $this->lang->line('L');?></a></li>
              <li class=""><a data-filter=".M"  href="#" class="select"><?php echo $this->lang->line('M');?></a></li>
              <li class=""><a data-filter=".N"  href="#" class="select"><?php echo $this->lang->line('N');?></a></li>
              <li class=""><a data-filter=".O"  href="#" class="select"><?php echo $this->lang->line('O');?></a></li>
              <li class=""><a data-filter=".P"  href="#" class="select"><?php echo $this->lang->line('P');?></a></li>
              <li class=""><a data-filter=".Q"  href="#" class="select"><?php echo $this->lang->line('Q');?></a></li>
              <li class=""><a data-filter=".R"  href="#" class="select"><?php echo $this->lang->line('R');?></a></li>
              <li class=""><a data-filter=".S"  href="#" class="select"><?php echo $this->lang->line('S');?></a></li>
              <li class=""><a data-filter=".T"  href="#" class="select"><?php echo $this->lang->line('T');?></a></li>
              <li class=""><a data-filter=".U"  href="#" class="select"><?php echo $this->lang->line('U');?></a></li>
              <li class=""><a data-filter=".V"  href="#" class="select"><?php echo $this->lang->line('V');?></a></li>
              <li class=""><a data-filter=".W"  href="#" class="select"><?php echo $this->lang->line('W');?></a></li>
              <li class=""><a data-filter=".X"  href="#" class="select"><?php echo $this->lang->line('X');?></a></li>
              <li class=""><a data-filter=".Y"  href="#" class="select"><?php echo $this->lang->line('Y');?></a></li>
              <li class=""><a data-filter=".Z"  href="#" class="select"><?php echo $this->lang->line('Z');?></a></li>
            </ul>
          </div>
          
          <div class="rex-features  clearfix" id="mycupons_listed">

            <input type="hidden" class="category" name="category" value=""> 
          
            <?php $this->load->view('front/mylist'); ?>
          
           </div>
          
        </div>
      </div>
    </div>
  </div>
  
  <!--- NEWSLETTER -->
  
  <section class="newsletter-bg mar-no">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="block-subscribe">
              <div class="newsletter">
                <form action="" method="post" onsubmit="return false;" id="newsletter-validate-detail1">
                  <h4><span><?php echo $this->lang->line('Signup_for');?>  <?php echo $admis->site_name;?><?php echo $this->lang->line('Exclusive');?> </span></h4>
                  <input type="text" name="email" id="emails" onkeypress="clears(2);" title="<?php echo $this->lang->line('title1');?>" class="input-text required-entry validate-email" placeholder="<?php echo $this->lang->line('place1');?>" />
                  <button type="button" title="<?php echo $this->lang->line('');?>" id="news_letter_submit" onClick="email_sub();" class="subscribe"><span><?php echo $this->lang->line('');?></span></button>
				   <div id="msg" style="color:red;text-align:center;"></div>
                </form>
              </div>
              <!--newsletter--> 
            </div>
            <!--block-subscribe--> </div>
        </div>
      </div>
    </section>


    <footer>
  <?php
//sub footer
	$this->load->view('front/sub_footer');
	
//Footer
//	$this->load->view('front/site_intro');	

?>
</footer>



<!-- FAQ -->




<?php $this->load->view('front/js_scripts');?>

<script type="text/javascript">
$('.select').click(function(){
  var va = $(this).html();
  var cate =  $('.category').val();
  $('.select').css('background','');
  $(this).css('background','#f7c411');
  $.ajax({
           type:'POST',
           data:{'va':va,'cate':cate},
           url:'<?php echo base_url(); ?>index.php/cashback/coupons_list',
           success:function(data)
           { 
             $('#mylisted_seems').html(data);
             return false; 
           }
        
      });
  return false; 
});

$('.selectcat').click(function(){
   var cate = $(this).attr('id');   
   $('.category').val(cate);
   var va = 'All';
   $.ajax({
           type:'POST',
           data:{'va':va,'cate':cate},
           url:'<?php echo base_url(); ?>index.php/cashback/coupons_list',
           success:function(data)
           { 
             $('#mylisted_seems').html(data);
             return false; 
           }        
      });

   return false;

});

//Start Email subscribe
function email_sub()
{
	var email = $("#emails").val();
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,3})?$/;
	if(!email || !emailReg.test(email))
		$('#emails').css('border', '2px solid red');
	else
	{
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>cashback/email_subscribe/",
			data: {'email':email},
			success: function(msg)
			{
				if(msg==1)
				{
					$('#msg').text('<?php echo $this->lang->line('ajaz2');?>');
					$('#email').css('border', '');
				}
				else
				{
					$('#msg').text('<?php echo $this->lang->line('ajaz1');?>');
					$('#email').css('border', '');
				}	
			}
		});
	}	
}
function clears(val)
{
	if(val==1)
		$('#invite_mail').css('border', '');
	else
		$('#emails').css('border', '');
} 
//End Email subscribe
</script>


 <!-- contact page specific js starts -->
 
    <script type="text/javascript" src="<?php echo base_url(); ?>front/js/map/jquery.validate.min.js"></script>       
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>front/js/map/gmaps.js"></script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>front/js/map/map.js"></script>

<!-- Slider --> 







</body>
</html>
