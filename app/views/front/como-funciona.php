<?php $this->load->view('front/header'); ?>
 <!-- header content End -->

<!-- Main content start -->

<section class="cms howitworks wow fadeInDown">
  	<div class="container">
    	<div class="heading wow bounceIn">
     		<h2> Como <span>Funciona</span></h2>
     		<div class="heading_border_cms">
     			<span>
     				<img src="<?php echo $this->front_model->get_img_url();?>front/new/images/top_drop.png">
     			</span>
     		</div>
    	</div>
    	<div class="a">
    		<div class="wow fadeInDown">
		      	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
				Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p> 
		    </div>
     		<div class="wow fadeInLeft">
				<div class="work">
					<!--<div class="drop"><span aria-hidden="true"> <div> 1</div></span></div>-->
					<div class="row">
						<div class="col-md-4 ">
							<div class="work_left hw-banner" align="center">
								<div class="drop">
									<span aria-hidden="true">
										<div> 1</div>
									</span>
								</div>
								<h2>Browse</h2>
								<img src="<?php echo $this->front_model->get_img_url();?>front/new/images/browse.png">
							</div>
						</div>
						<div class="col-md-8 ">
							<div class="work_right">
								<p>
									Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam 
									rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
									Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni 
									dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor 
									sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore 
									magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam
								</p>
							</div>
						</div>
					</div>
				</div> 
			</div>
      		<div class="wow fadeInRight">
      			<div class="work">
				<!--<div class="drop1"><span aria-hidden="true"> <div> 2</div></span></div>-->
					<div class="row">
						<div class="col-md-8">
							<div class="work_right">
								<p>
									Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam 
									rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
									Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni 
									dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor 
									sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore 
									magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam
								</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="work_left hw-banner" align="center">
								<div class="drop1">
									<span aria-hidden="true"> 
										<div> 2</div>
									</span>
								</div>
								<h2>Shop</h2>
								<img src="<?php echo $this->front_model->get_img_url();?>front/new/images/shop.png">
							</div>
						</div>
					</div>
				</div>
			</div>
	     	<div class="wow fadeInLeft">
			    <div class="work">
				<!--<div class="drop"><span aria-hidden="true"> <div> 3</div></span></div>-->
					<div class="row">
						<div class="col-md-4">
							<div class="work_left hw-banner" align="center">
								<div class="drop">
									<span aria-hidden="true"> 
										<div> 3</div>
									</span>
								</div>
								<h2>EARN CASHBACK</h2>
								<img src="<?php echo $this->front_model->get_img_url();?>front/new/images/earn.png">
							</div>
						</div>
						<div class="col-md-8">
							<div class="work_right">
								<p>
									Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam 
									rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
									Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni 
									dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor 
									sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore 
									magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Main Content end -->

 <!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
 
<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>
<!-- Footer Menu End-->	