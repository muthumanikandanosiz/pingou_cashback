<?php $this->load->view('front/header'); ?>
<!-- header content End -->
<style type="text/css">
.img-responsive.cls_topad {
    border-radius: 20px;
}
  .table-responsive {
    overflow-x: unset;
}
</style>
<style>
    div.selector, div.selector span, div.checker span, div.radio span, div.uploader, div.uploader span.action, div.button, div.button span
    {background-image:none;}
    .col-xs-6 {width: 47% !important; float: left;min-height:1px;padding-left: 12px;padding-right: 12px;position: relative;}
    select.input-sm { width:60px !important;}
    //.row {margin-left: -12px;margin-right: -12px;}
    .pagination {border-radius: 4px;margin: 20px 0;padding-left: 0;}
    .dataTables_filter label::after {content: '' !important;}
  </style>

<!-- Main Content start -->
<?php $user_id = $this->session->userdata('user_id');?>
<section class="cms wow fadeInDown">
  	<div class="container">
	    <div class="heading wow bounceIn">
	      	<h2>
	        saidas para <span>loja</span>
	      	</h2>
	      	<div class="heading_border_cms">
		        <span>
		          	<img src="<?php echo $this->front_model->get_img_url();?>front/new/images/top_drop.png">
		        </span>
	      	</div>
	    </div>
	    <div class="myac">
	      	<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1" id="hide_fn">
		        <div class="my_account my_accblk">
		          	<div class="myacc-maintab">
			            <?php $this->load->view('front/user_menu'); ?>
		          	</div>
		          	 <!-- Tab panes -->
		          	<div class="tab-content">
		          		<!-- New code for sales funnel banner details 8-11-16 -->
		          		<?php include('top_banners.php');?>
		          		<!-- End 8-11-16 -->
		          		<br>
						<div role="tabpanel" id="acc5">
							<!--account table section starts-->
							<div class="row wow fadeInDown">
								<div class="col-md-12 col-sm-12">
									<div class="a">
										<?php
				                        if(empty($result) && empty($result1))
				                        {
				                        	echo "<center>Você ainda não visitou nenhuma loja usando o Pingou.</center><br>";
				                        } 
				                        else
				                        {
					                        ?>
										    <table id="sample_teste1" class="display zui-table zui-table-rounded table acc-table1">
							                  	<thead>
													<tr>
														<th>No</th>
														<th>Store</th>
														<th style="width:95px!important;">Click Date</th>
														<th>Purchase will appear in your "Extrato" until</th>
														<th class="hidden-xs">Voucher Name</th>			
													</tr>
							                  	</thead>
							          			<tbody>
													<?php
													/*$k=1;
													foreach($result as $rows)
													{
														$datetime 	  = $rows->date_added;
														$split    	  = explode(' ',$datetime);
														$date_special = $date     = $split[0];
														$time     	  = $split[1];
														?>
														<tr>
															<?php $date=date("d/m/y", strtotime("$date +0 day"));
															//SATz
															$report_date=$rows->report_date;
															if($report_date !='')
															{
																//$report_date_alert= date('d/m/y', strtotime($report_date.'days'));
																//new changes 22-9-16//
																$report_date_alert= date('d/m/y', strtotime("+$report_date day", strtotime($date_special)));
															}
															else
															{
																//fixed after 10 days
																$date_alert 	   = strtotime($date_special);
																$date_alert 	   = strtotime("+10 day", $date_alert);
																$date_alert 	   = date('d/m/y', $date_alert);
																$report_date_alert = $date_alert;
																//$report_date_alert=date('Y-m-d', strtotime($date.'days'));
															}
															?>
															<td><?php echo $k; ?></td>
															<td><?php echo $rows->store_name;   ?></td>
															<td><?php echo $date;               ?></td>
															<td><?php echo $report_date_alert;  ?></td>
														    <td class="hidden-xs"><?php echo $rows->voucher_name; ?></td>
														</tr>	    
														<?php
														$k++;
													}
													foreach($result1 as $rows)
													{

														$datetime = $rows->date_added;
														$split    = explode(' ',$datetime);
														$date     = $split[0];
														$time     = $split[1];
														?>
														<tr>
															<td><?php echo $k; ?></td>
															<td><?php echo $rows->store_name;   ?></td>
															<td><?php echo $date;               ?></td>
															<td><?php echo $time;               ?></td>
															<td><?php echo $rows->voucher_name; ?></td>
														</tr>	    
											  			<?php
														//$k++;
														//}
													}*/
													?>
												</tbody>
							        		</table>
											<?php 
										}
										?>
									</div>
								</div>
							</div>
							<!--account table section ends-->
						</div><br>
						<!-- New code for sales funnel banner details 8-11-16 -->
						<?php include('bottom_banners.php');?>
		          		<!-- End 8-11-16 -->           	
		            	<?php $this->load->view('front/my_earnings.php')?>
		          	</div>
		        </div>
	      	</div>
	    </div>
  	</div>
</section>    
<!-- Main Content end -->

<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>



<!--<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.minnew.js"></script>
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.dataTables.bootstrap.min.js"></script> -->


<!-- Footer menu End --> 
<style type="text/css">
.dataTables_processing
{
  margin-top: -3px !important;
  padding-top: 13px !important;
  background: #4daed9 !important;
}
.acc-paginablk
{
  margin-top: 20px !important;  
}
</style>
 <link href="<?php echo $this->front_model->get_css_js_url(); ?>assets/css/pagination.css" rel="stylesheet" />
