<style type="text/css">
  #limenu{
    margin-right:0% !important;
  }
</style>
<ul class="nav nav-tabs clearfix" role="tablist">
<?php
$method = $this->router->fetch_method(); // for method
//echo $method;
?>
	<li id="limenu"  <?php if($method=='minha_conta'){echo 'class="active"';}?> >				
	 <a href="<?php echo base_url();?>minha-conta">Meus Dados</a>
	</li>
  <li id="limenu" <?php if($method=='extrato' || $method == 'loja_nao_avisou_compra' || $method == 'loja_cancelou_minha_compra') {echo 'class="active"';}?>>
    <a href="<?php echo base_url();?>extrato">Extrato</a>
  </li>
  <li id="limenu" <?php if($method=='resgate'){echo 'class="active"';}?>>
		<a href="<?php echo base_url();?>resgate">Resgate</a>
  </li>
	<li id="limenu" <?php if($method=='indique_e_ganhe'){echo 'class="active"';}?>>
    <a href="<?php echo base_url();?>indique-e-ganhe">Indique e Ganhe</a>
  </li>
  <li id="limenu" <?php if($method=='saidas_para_loja'){echo 'class="active"';}?>>
    <a href="<?php echo base_url();?>saidas-para-loja">Saidas_para_loja</a>
  </li>         
</ul>
<!--
<ul class="nav nav-tabs" role="tablist">
  <li role="presentation" id="dataMain" class="active" onMouseOver="show_datasub()"> <a href="#profile" aria-controls="acc1" role="tab" data-toggle="tab">Menus Dados</a> </li>
  <li role="presentation"> <a href="#acc2" aria-controls="acc2" role="tab" data-toggle="tab">Extrato</a> </li>
  <li role="presentation"> <a href="#acc3" aria-controls="acc3" role="tab" data-toggle="tab">Resgate</a> </li>
  <li role="presentation"> <a href="#acc4" aria-controls="acc4" role="tab" data-toggle="tab">Indique e Ganhe</a> </li>
  <li role="presentation"> <a href="#acc5" aria-controls="acc5" role="tab" data-toggle="tab">Saidas para Loja</a> </li>
</ul>
-->