<?php $this->load->view('front/header');?>
<!-- Header ends here -->
<section class="cms wow fadeInDown">
  	<div class="container">
      	<div class="heading wow bounceIn">
          	<h2> Un <span>subscribe</span></h2>
          	<div class="heading_border_cms">
            	<span>
              		<img src="<?php echo $this->front_model->get_img_url(); ?>/front/new/images/top_drop.png">
           		 </span>
          	</div>
      	</div>
  	</div>
	<div class="wrap-top">
	  <div id="content">
	  
		<?php
			$complete = $this->session->flashdata('un_subscribe');
			if($complete!="") {
			echo '<div class="alert alert-success">
			<button data-dismiss="alert" class="close">x</button>
			<strong>'.$complete.'</strong></div>';
		} ?>
	   
	    <div class="container">

	      <section class="contact-section section section-on-bg">
	        <div class="row">
	        <div class="col-md-12 col-sm-12 col-xs-12 accblk">
	        
	        <?php 
			if($user_ids == '')
			{	
				?>
					
					<center>
					<span>Please, Login to unsubscribe the newsletter</span>
					<br>
					<br>
					<a class="acc-commbtn" href="#login" data-toggle="modal" style="border-radius:5px !important; padding: 10px 55px !important;">Login</a></center>
				<?php 
			}
			else
			{
				?>
				<center>
					<span>Whould you like to unsubscribe from <?php echo $admindetails->site_name; ?> newsletters emails?</span>
					<br><br>
					<a class="acc-commbtn" style="border-radius:5px !important; padding: 10px 55px !important;" href="<?php echo base_url();?>un-subscribe/subscribers/<?php echo $user_ids;?>">un-subscrever</a>
				</center>
				<?php 
			}
			?>
	  
	        </div> 
	        </div> 
	        </section>
	    </div>
	  </div>
	</div>
</section>	
<!-- footer -->

<?php 

$this->load->view('front/site_intro');
$this->load->view('front/sub_footer');
?>

