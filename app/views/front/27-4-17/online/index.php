<?php
$admindetails = $this->front_model->getadmindetails();
$title  = $admindetails[0]->homepage_title;
$images = $this->front_model->get_img_url().'uploads/adminpro/'.$admindetails[0]->pingou_meta_image;
if(!isset($page_title)){
  $page_title = $title;
}
if(!isset($page_image)){
  $page_image = $images;
}
if(!isset($page_desc)){
  $page_desc = 'Get the best Cashback Offers at top brands. Never pay full price again. Join now Free & Start Saving!';
}?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="verification" content="8191171033199f6a1f5c89901db3a2e0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $page_title;?></title>
  <meta name="Description" content="<?php echo $admindetails[0]->meta_description;?>"/>    
  <meta name="keywords" content="<?php echo $admindetails[0]->meta_keyword;?>" />    
  <meta name="robots" CONTENT="INDEX, FOLLOW" />
  <meta http-equiv="no-cache">
  <meta http-equiv="Expires" content="-1">
  <meta http-equiv="Cache-Control" content="no-cache">

  <!-- New code for met img property 11-11-16 -->
  <meta property="og:url"         content="<?php echo $this->front_model->curPageURL();?>" />
  <meta property="og:type"        content="article"/>
  <meta property="og:title"       content="<?php echo $page_title; ?>" />
  <meta property="og:description" content="<?php echo $page_desc; ?>" />
  <meta property="og:image"       content="<?php echo $page_image; ?>" />
  <!-- End 11-11-16 -->
  <?php $this->load->view('front/css_script');?>
  <style type="text/css">
    body {
        padding:0px !important;
      }
      @media screen and (max-width: 750px) {
      .cls-for-popup .modal-dialog{display: none !important;}
      } 
      /*New style 5-12-16*/
      .coup_content.cls_indcoupct p{min-height:80px;}
      /*End 5-12-16*/

      /*new code 6-4-17*/
      .newfavstore p
      {
        word-wrap: break-word;
      }
  </style>
</head>
<!-- NAVBAR -->
<body>
<?php 
$ip_address         = $_SERVER['REMOTE_ADDR'];
$getadmindetails    = $this->front_model->getadmindetails();  
$logo               = $getadmindetails[0]->site_logo;
$blog_url           = $getadmindetails[0]->blog_url;
$site_mode          = $getadmindetails[0]->site_mode;
$background_image   = $getadmindetails[0]->background_image;
$background_color   = $getadmindetails[0]->background_color;
$background_type    = $getadmindetails[0]->background_type;
$unlog_menu_status  = $getadmindetails[0]->unlog_menu_status;
$log_menu_status    = $getadmindetails[0]->log_menu_status;
$log_content        = $getadmindetails[0]->log_content;
$unlog_content      = $getadmindetails[0]->unlog_content;
$log_status         = $getadmindetails[0]->log_status;
$unlog_status       = $getadmindetails[0]->unlog_status;
$notify_color       = $getadmindetails[0]->notify_color;
$site_favicon       = $getadmindetails[0]->site_favicon;
$ip                 = $_SERVER['REMOTE_ADDR'];
$unique_visits      = $this->front_model->unique_visits($ip);
$user_id            = $this->session->userdata('user_id'); 
$userdetails        = $this->front_model->userdetails($user_id);
$firstname          = $userdetails->first_name;
$lastname           = $userdetails->last_name;
$emailid            = $userdetails->email;
/*referral system function calling this place 12-9-16*/
$referal_systems    = $this->front_model->update_referral_systems($user_id);
/*New code for user balance changes 26-8-16*/
$osiz_usr_bal       = bcdiv($userdetails->balance,1,2);
$balance            = $this->front_model->currency_format($osiz_usr_bal);
/*end 26-8-16*/ 
$profile_photo      = $userdetails->profile; 

if($_REQUEST['ref'])
{
  $random_ref= $_REQUEST['ref'];
  $this->session->set_userdata('ses_random_ref',$random_ref);
  $getuserid = $this->db->query("SELECT `user_id` from `tbl_users` where `random_code`='$random_ref'")->row('user_id');
  $this->front_model->update_refclick_count($getuserid);
}           
//Available balance details//
$cashback_count = bcdiv($this->front_model->pending_cashback($user_id),1,2);
$referal_count  = bcdiv($this->front_model->pending_referral($user_id),1,2);


/*New code for user balance changes 26-8-16*/

$osiz_pen_bal       = $cashback_count + $referal_count;
$total_amt          = $this->front_model->currency_format($osiz_pen_bal);
       
//End//

if($firstname =='' && $lastname =='')
{ 
  $emails    = explode('@', $emailid);
  $usernames = $emails[0];  
}
else
{
  $usernames = ucfirst($firstname)." ".ucfirst($lastname);
}
$username  = substr($usernames,0,15);
//$usernamesasa = $userdetails[0]->first_name;
if($site_mode==0)
{ 
  redirect('maintenance','refresh');
}

echo $google_analytics = $getadmindetails[0]->google_analytics;
 
$admindetails = $this->front_model->getadmindetails_main();
//echo "<pre>";print_r($admindetails); 
?>

<!-- New code for background image and color settings 20-9-16-->
<?php if($background_type == 'image'){?>
<style type="text/css">
body 
{
  background:url(<?php echo $this->front_model->get_img_url();?>uploads/adminpro/<?php echo $background_image;?>)!important; 
  //background-repeat:repeat;
  //background-size: 100% 100%;
} 
</style>
<?php 
}
if($background_type == 'color')
{?>
<style type="text/css">
body {
  background-color:<?php echo $background_color;?> !important;
} 
</style>
<?php } ?>
<!-- End -->




<input type="hidden" name="cashback_exc" value="<?php echo $_REQUEST['pro'];?>">
<!-- New code for cashback_exclusive page details 13-7-16-->

<?php
$new_storename       = $this->session->userdata('store_name');
$affiliate_names     = $this->session->userdata('affiliate_names');
$affiliate_urls      = $this->session->userdata('affiliate_urls');
$new_cash_ex_id      = $this->session->userdata('cash_ex_id');

  if($new_storename)
  { 
    if($new_storename == $affiliate_names)
    {
      /*new changes 16-7-16*/
      ?>
      <form name='fr' action='cupom-desconto/<?php echo $affiliate_urls;?>' method='POST'>
        <input type='hidden' name='id' value='<?php echo $new_cash_ex_id;?>'>
        <input type='hidden' name='cashback_details' value='<?php echo $affiliate_urls;?>'>
      </form>
      <script type='text/javascript'>
      document.fr.submit();
      </script>
      <?php 
     }
  }

?>
<!-- End cashback exclusive details -->
  
<div class="navbar-wrapper index-headwrap wow fadeInDown">
  <div class="cbp-af-header">
    <div class="cbp-af-inner">
      <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
          <div class="tophead-resp-md">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
           <a href="javascript:;" id="selsearch1" class="select-style1 hidden-md hidden-sm"> </a>
            </div>

            <div class="row">
              <div class="col-md-5 col-sm-6 col-xs-4 cls_padresno cls_menucont">              
                <div id="navbar" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                    <li class=""><a href="<?php echo base_url();?>cupom">Destaques</a></li>
                    <li><a href="<?php echo base_url();?>cupom-desconto">Coupons</a></li>
                    <li>
                      <a href="javascript:;" id="selsearch" class="select-style1 hidden-xs">
                          <div class="select">
                             buscar lojas
                          </div>
                        </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-4 cls_padresno toplogo-md">
                <center>
                  <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo $this->front_model->get_img_url();?>front/new/images/logo.png"></a>
                </center>
              </div>
              <?php
              if($user_id == '')
              {
                  ?>
                  <div class="col-md-5 col-sm-4 col-xs-4 cls_padresno pull-right">
                  
                    <div class="sign-topblk">
                      <ul class="nav navbar-nav pull-right">
                        <li><a class="hidden-xs" href="#register" data-toggle="modal">Sign up</a></li>
                        <li><a href="#login" data-toggle="modal">Login</a></li>                       
                      </ul>
                    </div>

                  </div>
                  <?php
              }
              else
              {
                  ?>
                  <!-- New design for my account menu 14-7-16-->
                  <div class="col-md-5 col-sm-4 col-xs-4">
                    <div id="" class="pro_im clearfix">
                      <ul class="nav navbar-nav pull-right col-md-6 col-sm-12 col-xs-12">
                        <li class="dropdown top-login">
                          <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <div class="pro_pic">
                              <?php 
                              if($profile_photo =='')
                              {
                              ?>
                              <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/profile_pic.png" class="img-responsive">
                              <?php
                              }
                              else
                              {
                              ?>
                                <img style="border-radius: 16px;" class="img-responsive" alt="" src="<?php echo $profile_photo; ?>">
                              <?php
                              }
                              ?>
                            </div>
                            <div class="pro_piccont">
                                <!-- <a href="<?php echo base_url(); ?>minha-contanew"> -->
                                  <p class="usr-log"> <?php echo $username;?> <span class="caret pull-right"></span></p> 
                                  <p class="usr-log1">R$<?php echo $balance; ?>  <span class="usr-log2"> R$<?php echo $total_amt; ?></span> </p>
                                <!-- </a> -->
                            </div>
                            <!-- <a href="<?php echo base_url(); ?>minha-contanew"><?php echo $username;?> </a><br> -->
                            <!-- <span style="color:#5ef05a; font-size:14px;top: 29px; left:55px; position:absolute; line-height:10px; width:40%;" >R$<?php echo $balance; ?></span>
                            <span style="color:#f5ff55; font-size:14px;top: 29px; left:135px; position:absolute; line-height:10px;width:40%;"> R$<?php echo $total_amt; ?></span>  -->
                          </a>
                          <ul class="dropdown-menu">
                            <center><h4>Visao geral</h4></center>
                            <center>
                              <ul class="appr-listblk list-inline clearfix">
                                <li>
                                  <a href="<?php echo base_url();?>minha-conta">
                                    <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr1.png" alt="app" class="img-responsive center-block">
                                    dados
                                  </a>
                                </li>
                                <li>
                                  <a href="<?php echo base_url();?>extrato">
                                    <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr2.png" alt="app" class="img-responsive center-block">
                                    Extrato
                                  </a>
                                </li>
                                <li>
                                  <a href="<?php echo base_url();?>resgate">
                                    <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr3.png" alt="app" class="img-responsive center-block">
                                    Resgate
                                  </a>
                                </li>
                                <li>
                                  <a href="<?php echo base_url();?>indique-e-ganhe">
                                    <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr4.png" alt="app" class="img-responsive center-block">
                                    Indicacoes
                                  </a>
                                </li>
                              </ul>
                            </center>
                            <center><h4>Facilidades</h4></center>
                            <ul class="appr-listblk1 list-inline clearfix">
                              <li>
                                <a href="#">
                                  <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr5.png" alt="app" class="img-responsive pull-left">
                                   <span> Lembrador </span>
                                </a>
                              </li>                       
                              <li class="pull-right">
                                <a href="#">
                                  <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr6.png" alt="app" class="img-responsive pull-left">
                                  <span> Aplicativo </span>
                                </a>
                              </li>
                            </ul>
                            <ul class="appr-listbtnblk1 list-inline clearfix">
                              <li>
                                <button class="btn btn-appr" type="button"> <img alt="chat" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr-chat.png" class="img-responsive"> Ajuda </button>
                              </li>
                              <li class="pull-right">
                                <button onclick="window.location.href='<?php echo base_url();?>logout'" class="btn btn-appr" type="button"> <img alt="chat" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/appr-logout.png" class="img-responsive"> Sair </button>
                              </li>
                            </ul>
                          </ul>  
                        </li>
                      </ul>
                    </div>
                  </div>   
                  <!-- End New my account menu -->
                  <?php
              } 
              ?>
            </div>
          </div>  
        </div>
        <!-- New code for search 6-8-16 -->
        <div class="search_content" id="search_content" style=" background-color: transparent; display:none;">
              <div class="container">
                <?php
                if((strpos($_SERVER['REQUEST_URI'],'cashback/shopping') == true))
                {
                  ?>
                  <form class="search-area form-group search-area-white search-style" action="<?php echo base_url();?>cashback/change_location" method="POST">
                    <div class="wrap-top">
                      <div class="row">
                        <div class="col-md-11 clearfix">
                          <div class="search-area-division search-area-division-input">
                            <input type="text" required id="Location" placeholder="Pesquise Lojas ou Ofertas" class="form-control" name="location" 
                            value="<?php if($this->session->userdata('cityname')){ echo $cityname = $this->session->userdata('cityname');}?>" data-provide="typeahead">
                          </div>
                        </div>
                        <div class="col-md-1">
                          <button class="btn btn-block btn-white search-btn pop" type="submit">
                            <i class="fa fa-search"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                  <?php
                }
                else
                {  
                  if(isset($_POST['storehead']))
                  {
                    $storehead  = $_POST['storehead'];
                  } 
                  ?>
                    <!--New search form-->
                    <form id="search_mini_form" class="search-area form-group search-area-white search-style"  onsubmit="return submit_form();" method="get">
                      <div class="wrap-top">
                        <div class="row">
                          <div class="col-md-11 col-sm-10 col-xs-9 clearfix">
                            <div class="search-area-division search-area-division-input">
                              <input id="search" required type="text" name="store" class="form-control cls_searchtbox search_header" value="<?php if(isset($_POST['storehead'])){echo $storehead;}?>" placeholder="Aliexpress, Submarino, Netshoes" data-provide="typeahead" style="width:102% !important;"/>
                              <!-- <input id="catsearch" type="hidden" name="cat" /> -->
                            </div>
                          </div>  
                          <div class="col-md-1 col-sm-2 col-xs-3 search-botao">
                            <button type="submit" class="btn btn-block btn-white cls_searchtbtn search-btn pop" style="float:right; margin-right: 17px;"><i class="fa fa-search"></i>
                            </button> 
                          </div>
                          <div id="search_autocomplete" class="search-autocomplete">
                          </div>
                        </div>        
                      </div>
                    </form>
                    <!--End-->
                    <form id="dummyform" method="post">
                        <input type="hidden" name="storehead" id="storehead" value="">
                    </form>
                    <?php
                } 
                ?>
              </div>
        </div>
        <div class="search_content" id="search_content1" style=" background-color: transparent; display:none;">
              <div class="container">
                <?php
                if((strpos($_SERVER['REQUEST_URI'],'cashback/shopping') == true))
                {
                  ?>
                  <form class="search-area form-group search-area-white search-style" action="<?php echo base_url();?>cashback/change_location" method="POST">
                    <div class="wrap-top">
                      <div class="row">
                        <div class="col-md-11 clearfix">
                          <div class="search-area-division search-area-division-input">
                            <input type="text" required id="Location" placeholder="Pesquise Lojas ou Ofertas" class="form-control" name="location" 
                            value="<?php if($this->session->userdata('cityname')){ echo $cityname = $this->session->userdata('cityname');}?>" data-provide="typeahead">
                          </div>
                        </div>
                        <div class="col-md-1">
                          <button class="btn btn-block btn-white search-btn pop" type="submit">
                            <i class="fa fa-search"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                  <?php
                }
                else
                {  
                  if(isset($_POST['storehead']))
                  {
                    $storehead  = $_POST['storehead'];
                  } 
                  ?>
                    
                    <form id="search_mini_form" class="search-area form-group search-area-white search-style"  onsubmit="return submit_form();" method="get">
                      <div class="wrap-top">
                        <div class="row">
                          <div class="col-md-11 col-sm-10 col-xs-9 clearfix">
                            <div class="search-area-division search-area-division-input">
                              <input id="search" required type="text" name="store" class="form-control cls_searchtbox search_header searchs" value="<?php if(isset($_POST['storehead'])){echo $storehead;}?>" placeholder="Aliexpress, Submarino, Netshoes" data-provide="typeahead" style="width:102% !important;"/>
                              
                            </div>
                          </div>  
                          <div class="col-md-1 col-sm-2 col-xs-3 search-botao">
                            <button type="submit" class="btn btn-block btn-white cls_searchtbtn search-btn pop" style="float:right; margin-right: 17px;"><i class="fa fa-search"></i>
                            </button> 
                          </div>
                          <div id="search_autocomplete" class="search-autocomplete">
                          </div>
                        </div>        
                      </div>
                    </form>
              
                    <form id="dummyform" method="post">
                        <input type="hidden" name="storehead" id="storehead" value="">
                    </form>
                    <?php
                } 
                ?>
              </div>
        </div>
        <!-- End -->

      </nav>
    </div>
  </div>
  
</div>


<!-- Header content End -->

<!-- Main Section content Start -->

<section class="banner">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active ">
        <div class="moving"> </div>
        <div class="container">
          <div class="carousel-caption wow slideInLeft">
            <h2>for those who</h2>
            <h1>love to shop</h1>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
            <p class="mar-top-10"><a class="btn-header mar-rht-30" href="<?php echo base_url();?>como-funciona">how it works</a> <a class="btn-header" data-toggle="modal" data-target="#register" href="#">Sign up !</a> </p>
          </div>
          <div class="carousel-img wow slideInRight"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/side.png"> </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="lead top-leadban wow bounce">
  <div class="leadin"> <a href="#howworks" class="check">
    <div class="goto">
      <div class="inr_sec">
        <p><i class="fa fa-angle-down"></i></p>
      </div>
    </div>
    </a> </div>
</div>
<section class="howworks"  id="howworks">
  <div class="container">
    <div class="heading wow bounceIn">
      <h2> How Cashback <span>Works</span></h2>
      <div class="heading_border_red"><span><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/top_drop.png"></span> </div>
    </div>
    <div class="work_img">
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-4 col-sm-offset-4 col-xs-offset-0 mar-bot-20 wow fadeInUp">
          <div class="box3">
            <div class="row">
              <div class="col-md-3 col-sm-4 col-xs-6 hidden-xs">
                <div class="annie">
                  <div class="tear"><span> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/icon_shop.png"></span> </div>
                </div>
              </div>
              <div class="col-md-9 col-sm-8 col-xs-12">
                <div>
                  <h3>SHOP</h3>
                  <p>Click through to your favourite retailers and shop as usual.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12 pad-no wow slideInLeft">
          <div class="box1">
            <div class="row">
              <div class="col-md-3 col-sm-4 col-xs-6 hidden-xs">
                <div class="annie">
                  <div class="tear"><span> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/icon_browse.png"></span> </div>
                </div>
              </div>
              <div class="col-md-9 col-sm-8 col-xs-12">
                <h3>BROWSE</h3>
                <p>Browse our site and choose from 100s of retailers and exclusive offers.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 pad-no wow slideInRight hidden-earn">
          <div class="box2">
            <div class="row">
              <div class="col-md-3 col-sm-4 col-xs-6 hidden-xs">
                <div class="annie">
                  <div class="tear"> <span><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/icon_earn.png"></span> </div>
                </div>
              </div>
              <div class="col-md-9 col-sm-8 col-xs-12">
                <div>
                  <h3>EARN CASHBACK</h3>
                  <p>The retailer pay us commission foryour purchase and we add this as cashback to your earnings.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 pad-no  wow bounceIn">
          <center>
            <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/howitworks.png" class="img-responsive work_centerblk">
          </center>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 pad-no wow slideInRight hidden-xs">
          <div class="box2">
            <div class="row">
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="annie">
                  <div class="tear"> <span><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/icon_earn.png"></span> </div>
                </div>
              </div>
              <div class="col-md-9 col-sm-8 col-xs-6">
                <div>
                  <h3>EARN CASHBACK</h3>
                  <p>The retailer pay us commission foryour purchase and we add this as cashback to your earnings.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="lead wow bounce">
  <div class="leadin"> <a href="#bestcoupon" class="check">
    <div class="goto">
      <div class="inr_sec">
        <p><i class="fa fa-angle-down"></i></p>
      </div>
    </div>
    </a> </div>
</div>
<section class="bestcoupon" id="bestcoupon">
  <div class="container">
    <div class="heading wow bounceIn">
      <h2> Best Coupon & <span>Cashback</span></h2>
      <div class="heading_border_red"><span><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/top_drop.png"></span> </div>
    </div>
    <div class="coupon">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs cash_tab wow fadeInUp" role="tablist">
        <?php
        $categories = $this->front_model->get_all_categories(7);
        $i=1;
        foreach($categories as $view)
        { 
          $imagename = 'tab'.$i;
          if($i==1)
          {
            $active = 'active';
          }
          else
          {
            $active = ''; 
          }
          ?>
          <li class="<?php echo $active; ?>">
            <a href="<?php echo base_url();?>cupom-desconto/<?php echo $view->category_url;?>"> <!-- aria-controls="<?php echo $i;?>" role="tab" data-toggle="tab" -->
              <div><?php echo $view->category_name;?></div>
              <div align="center">
                <div class="tab_ico hidden-xs">
                  <div class="inr_sec">
                    <div class="inr_img">
                      <img class="first img img-middle img-responsive" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/<?php echo $imagename;?>.png">
                      <img class="last img img-middle img-responsive" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/tab1-hover.png">
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <?php
          $i++;
        }
        ?>
        <li>
          <a href="<?php echo base_url();?>cupom-desconto"> <!-- aria-controls="8" role="tab" data-toggle="tab" -->
            <div>All</div>
            <div align="center">
              <div class="tab_ico hidden-xs">
                <div class="inr_sec">
                  <div class="inr_img"> <img class="first img img-middle img-responsive" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/tab8.png"> <img class="last img img-middle img-responsive" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/tab8-hover.png"> </div>
                </div>
              </div>
            </div>
          </a>
        </li>
      </ul>
      <?php
      $recom_viewd = $this->front_model->clock_history_stores_limit();
      ?>
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="1">
          <div class="row wow fadeInDown">
            <?php 
            foreach($recom_viewd as $recom)
            {
              if($recom->cashback_percentage)
              { 
                if($recom->affiliate_cashback_type=="Percentage")
                {
                  $cppercentage = $recom->cashback_percentage."%";
                }
                else
                {
                  $cppercentage = "R$. ".$recom->cashback_percentage;
                }
              }
              $count_coupons = $this->front_model->count_coupons($recom->affiliate_name);
              $ctp = $count_coupons->counting;   
              ?>
              <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="coup">
                  <div class="overlay">
                    <center>
                      <a href="<?php echo base_url();?>cupom-desconto/<?php echo $recom->affiliate_url;?>" class="shonow">shop now</a>
                    </center>
                  </div>
                  <a href="#">
                  <div class="coup_content cls_indcoupct">
                    <p><?php echo $recom->affiliate_name;?>: <?php echo $cppercentage; ?> OFF & Cashback Offer on All
                      Categories</p>
                    <div class="contentimg">
                      <center>
                        <img alt="<?php echo $recom->affiliate_name;?>" src="<?php echo $this->front_model->get_img_url();?>uploads/affiliates/<?php echo $recom->affiliate_logo;?>" width="130" height="50">
                      </center>
                    </div>
                    <h3><?php echo $cppercentage; ?> OFFER </h3>
                  </div>
                  <div class="coup_btm"> <i class="fa fa-tag"></i> <?php echo $ctp;?> <?php echo $recom->affiliate_name;?> Coupons </div>
                  </a> </div>
              </div>
              <?php
            }
            ?>
          </div>
        </div>
      </div>
      <div> </div>
    </div>
  </div>
</section>
<div class="lead wow bounce">
  <div class="leadin"> <a href="#features" class="check">
    <div class="goto">
      <div class="inr_sec">
        <p><i class="fa fa-angle-down"></i></p>
      </div>
    </div>
    </a> </div>
</div>
<section class="features" id="features">
  <div class="container">
    <div class="col-md-6 wow slideInLeft col-sm-4 col-xs-12">
      <center>
        <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/features.png" class="img-responsive mar-top-50">
      </center>
    </div>
    <div class="col-md-6 wow slideInRight col-sm-8 col-xs-12">
      <div class="heading wow bounceIn">
        <h2> Features</h2>
        <div class="heading_border_white"><span><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/top_drop1.png"></span> </div>
      </div>
      <div class="fea_list">
        <ul>
          <li>
            <div class="fea_media media">
              <div class="media-left"> <a href="#">
                <div class="fea_sec">
                  <div class="inr_sec">
                    <p><img class="media-object img img-middle" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/fea1.png" alt=""></p>
                  </div>
                </div>
                </a> </div>
              <div class="media-body">
                <h4 class="media-heading">Same Prices, But Extra Cashback</h4>
              </div>
            </div>
          </li>
          <li>
            <div class="fea_media media">
              <div class="media-left"> <a href="#">
                <div class="fea_sec">
                  <div class="inr_sec">
                    <p><img class="media-object img img-middle" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/fea2.png" alt=""></p>
                  </div>
                </div>
                </a> </div>
              <div class="media-body">
                <h4 class="media-heading">Best Deals & Cashback Rates</h4>
              </div>
            </div>
          </li>
          <li>
            <div class="fea_media media">
              <div class="media-left"> <a href="#">
                <div class="fea_sec">
                  <div class="inr_sec">
                    <p><img class="media-object img img-middle" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/fea3.png" alt=""></p>
                  </div>
                </div>
                </a> </div>
              <div class="media-body">
                <h4 class="media-heading">No Redeem Fee or Charges</h4>
              </div>
            </div>
          </li>
          <li>
            <div class="fea_media media">
              <div class="media-left"> <a href="#">
                <div class="fea_sec">
                  <div class="inr_sec">
                    <p><img class="media-object img img-middle" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/fea4.png" alt=""></p>
                  </div>
                </div>
                </a> </div>
              <div class="media-body">
                <h4 class="media-heading">Multiple Redeem Options</h4>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<div class="lead wow bounce">
  <div class="leadin"> <a href="#favstore" class="check">
    <div class="goto">
      <div class="inr_sec">
        <p><i class="fa fa-angle-down"></i></p>
      </div>
    </div>
    </a> </div>
</div>
<section class="favstore" id="favstore">
  <div class="container">
    <div class="heading wow bounceIn">
      <h2> favourite <span>Stores</span></h2>
      <div class="heading_border_red"><span><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/top_drop.png"></span> </div>
    </div>
    <div class="mar-top-20">
      <div class="row wow flipInX">
        <?php
        for($x = 1; $x <= 18; $x++) 
        { 
          $selectname  = $this->db->query("SELECT `grid_name_$x` from homepage_fav_store_grids where homepage_grid_id=1")->row('grid_name_'.$x);
          $store_image = $this->db->query("SELECT `affiliate_logo` from affiliates where affiliate_url='$selectname'")->row('affiliate_logo');
          $store_name  = $this->db->query("SELECT `affiliate_name` from affiliates where affiliate_url='$selectname'")->row('affiliate_name');
          ?>
          <div class="col-md-2 col-sm-4 col-xs-6">
            <div class="fav">
              <center>
                <img alt="<?php echo $selectname;?>" src="<?php echo $this->front_model->get_img_url(); ?>uploads/affiliates/<?php echo $store_image; ?>" class="img-responsive">
              </center>
              <div class="overlay">
                <a href="<?php echo base_url(); ?>cupom-desconto/<?php echo $selectname;?>"><p>Cupons <br>
                  <?php echo $store_name;?></p></a>
              </div>
            </div>
          </div>
          <?php 
        }
        ?>  
      </div>
    </div>
  </div>
</section>
<!-- new code for fav store description details 6-4-17 -->
<div class="lead wow bounce">
  <div class="leadin"> <a href="#favstoredesc" class="check">
    <div class="goto">
      <div class="inr_sec">
        <p><i class="fa fa-angle-down"></i></p>
      </div>
    </div>
    </a> </div>
</div>
<section class="favstore" id="favstoredesc" style="background: #fff none repeat scroll 0 0;">
  <div class="container">
    <div class="heading wow bounceIn">
      <h2>Favourite Stores <span>Descriptions</span></h2>
      <div class="heading_border_red"><span><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/top_drop.png"></span> </div>
    </div>
    <div class="mar-top-20">
      <div class="row wow flipInX newfavstore">
        <?php
        echo $store_desc = $this->db->query("SELECT `grid_img_desc` from homepage_fav_store_grids where homepage_grid_id=1")->row('grid_img_desc');
        ?>  
      </div>
    </div>
  </div>
</section>
<!-- End -->


<?php $this->load->view('front/site_intro'); ?> 
<footer>
  <!-- ========================== -->
  <!-- BUY SECTION -->
  <!-- ========================== -->
  <section class="with-icon buy-section">
    <div class="container">
      <div class="wow fadeInDown">
        <div class="col-md-4 pad-no col-sm-4 col-xs-12">
          <div class="con1 top_foot">
            <div class="section-text">
              <div class=" vcenter like"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/add1.png"> </div>
              <div class="buy-text vcenter">
                <div class="top-text"><span>address</span><br>
                  <?php echo $admindetails->address;?> </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 pad-no col-sm-4 col-xs-12">
          <div class="con2 top_foot">
            <div class="section-text">
              <div class=" vcenter like"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/add2.png"> </div>
              <div class="buy-text vcenter">
                <div class="top-text"><span>Phone</span><br>
                  <?php echo $admindetails->contact_number;?></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 pad-no col-sm-4 col-xs-12">
          <div class="con3 top_foot">
            <div class="section-text">
              <div class=" vcenter like"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/add3.png"> </div>
              <div class="buy-text vcenter">
                <div class="top-text"><span>Email</span><br>
                  <?php echo $admindetails->admin_email;?> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ========================== -->
  <!-- FOOTER - FOOTER -->
  <!-- ========================== -->
  <section class="footer-section">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInDown">
          <h5>Recent Blogs</h5>
          <div class="row">
            <div class="gallery">
              <div class="col-md-3 col-xs-3 col-sm-6 pad-rht0"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/t1.png" class="img-responsive"> </div>
              <div class="col-md-3 col-xs-3 col-sm-6 pad-rht0"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/t2.png" class="img-responsive"> </div>
              <div class="col-md-3 col-xs-3 col-sm-6 pad-rht0"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/t3.png" class="img-responsive"> </div>
              <div class="col-md-3 col-xs-3 col-sm-6 pad-rht0"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/t4.png" class="img-responsive"> </div>
              <div class="col-md-3 col-xs-3 col-sm-6 pad-rht0"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/t1.png" class="img-responsive"> </div>
              <div class="col-md-3 col-xs-3 col-sm-6 pad-rht0"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/t2.png" class="img-responsive"> </div>
              <div class="col-md-3 col-xs-3 col-sm-6 pad-rht0"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/t3.png" class="img-responsive"> </div>
              <div class="col-md-3 col-xs-3 col-sm-6 pad-rht0"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/t4.png" class="img-responsive"> </div>
              <div class="col-md-3 col-xs-3 col-sm-6 pad-rht0"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/t1.png" class="img-responsive"> </div>
              <div class="col-md-3 col-xs-3 col-sm-6 pad-rht0"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/t2.png" class="img-responsive"> </div>
              <div class="col-md-3 col-xs-3 col-sm-6 pad-rht0"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/t3.png" class="img-responsive"> </div>
              <div class="col-md-3 col-xs-3 col-sm-6 pad-rht0"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/t4.png" class="img-responsive"> </div>
            </div>
          </div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-12 wow fadeInDown" style=" padding-left: 26px;">
          <h5>Helpful Links</h5>
          <ul class="footer-nav">
            <li><a href="<?php echo base_url();?>contato"><i class="fa fa-caret-right"></i> Contact Us</a></li>
            <li><a href="<?php echo base_url(); ?>perguntas-frequentes"><i class="fa fa-caret-right"></i> Perguntas Frequentes</a></li>
            <?php 
            $result = $this->front_model->sub_menu();
            foreach($result as $view)
            {
              ?>
              <li><a href="<?php echo base_url();?><?php echo $view->cms_title; ?>"><i class="fa fa-caret-right"></i> <?php echo $view->cms_heading;?></a></li>
              <?php 
            }
            ?>
          </ul>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12 wow fadeInDown">
          <h5>popular categories</h5>
          <ul class="list-inline">
            <?php 
            $categories = $this->front_model->get_all_categories(9);
            foreach($categories as $view)
            {
              $new_category_name = $view->category_name;
              $category_name     = ucfirst(strtolower($new_category_name));
            ?> 
              <li><a style="color:#fff !important;" href="<?php echo base_url();?>ofertas/<?php echo $view->category_url;?>"><?php echo $category_name;?></a></li>
            <?php
            }
            ?>
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInDown">
          <h5>Testimonials</h5>
          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <div class="row">
                  <div class="col-xs-12">
                    <div class=" adjust1">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <img class="media-object img-rounded img-responsive" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/test1.png"></div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                        <div class="caption">
                          <p > <span>John Peter</span><br>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                        </div>
                      </div>
                    </div>
                    <div class=" adjust1">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <img class="media-object img-rounded img-responsive" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/test1.png"></div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                        <div class="caption">
                          <p > <span>John Peter</span><br>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="row">
                  <div class="col-xs-12">
                    <div class=" adjust1">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <img class="media-object img-rounded img-responsive" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/test1.png"></div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                        <div class="caption">
                          <p > <span>John Peter</span><br>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                        </div>
                      </div>
                    </div>
                    <div class=" adjust1">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <img class="media-object img-rounded img-responsive" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/test1.png"></div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                        <div class="caption">
                          <p > <span>John Peter</span><br>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="row">
                  <div class="col-xs-12">
                    <div class=" adjust1">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <img class="media-object img-rounded img-responsive" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/test1.png"></div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                        <div class="caption">
                          <p > <span>John Peter</span><br>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                        </div>
                      </div>
                    </div>
                    <div class=" adjust1">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <img class="media-object img-rounded img-responsive" src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/test1.png"></div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                        <div class="caption">
                          <p > <span>John Peter</span><br>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="copyright-section">
    <div class="container">
      <div class="col-md-6  wow flipInX">
        <p>Copyright &copy; <?php echo date('Y')?> <?php echo $admindetails->site_name;?>  |  All Rights Reserved </p>
      </div>
      <?php
      $listing = $this->db->query("select * from admin")->row();
      ?>
      <div class="col-md-6 wow flipInX">
        <ul class="list-socials pull-right">
          <?php if($listing->admin_twitter) {?> <li><a href="<?php echo $listing->admin_twitter;?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
          <?php if($listing->admin_fb)      {?> <li><a href="<?php echo $listing->admin_fb;?>">     <i class="fa fa-facebook"></i></a></li><?php } ?>
          <?php if($listing->admin_gplus)   {?> <li><a href="<?php echo $listing->admin_gplus;?>">  <i class="fa fa-google-plus"></i></a></li><?php } ?>
        </ul>
      </div>
    </div>
  </section>
</footer>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/newbootstrap.min.js"></script>
 

<?php 
/*New code for exit opup details 17-11-16*/

$popup_templete = $this->db->query("SELECT * from exit_popup where all_status=1")->row();  
$url            = base_url();
$all_content    = $popup_templete->popup_template;
$admin_number   = $admindetails->contact_number;
$site_logo      = $this->front_model->get_img_url()."uploads/adminpro/".$admindetails->site_logo;
$site_name      = $admindetails->site_name;

$data = array(
  '###ADMINNO###'=>$admin_number,
  '###COMPANYLOGO###'=>$site_logo,
  '###SITE NAME###'=>$site_name,
  );



if($user_id == '')
{
  ?>
  <script type="text/javascript">
  
  $(document).ready(function()
  {

    $('body').mouseleave(function(){
      if($('#myModal').attr('counts') == 0)
      {
        if($(window).width() > 850)
        {
          $('#myModal').modal('show');
          $('.cus_modal').css("background","black");
        }  
      }

    });

    $('.cls_popup').click(function(){
      $('#myModal').attr('counts',1);
    }); 
  })
  </script>
  <?php 

  $contents = strtr($all_content,$data);

  if($popup_templete->all_status == 1)
  { 
    echo $contents;
  }     
}
else
{
  $popup_templete  = $this->db->query("SELECT * from first_access_popup where all_status_firstpopup=1")->row();
  
  $datas = array(
  '###ADMINNO###'=>$admin_number,
  '###COMPANYLOGO###'=>$site_logo,
  '###SITE NAME###'=>$site_name,
  );

  $first_acc_content  = $popup_templete->first_popup_template;
  $first_acc_popup    = strtr($first_acc_content,$datas);
  $first_popup_status = $popup_templete->all_status_firstpopup;

  if($first_popup_status == 1)
  {
    if($userdetails->first_ac_popup_status == 1)
    { 
      echo $first_acc_popup;
      ?>
      <script type="text/javascript">
        $(document).ready(function()
        {
          $('#first_acc_popup').modal('show');  
          $("#first_acc_popup").on("click", function(e)
          {
            $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>cashback/update_firstpoup_satus",
            data: {'poup_status':0},
            cache: false,
            success: function(result)
            {
              if(result!=1)
              {
                return false;
              }
              else
              {
                <?php $redirect_urlset =  base_url(uri_string());?>
                //window.location.href = '<?php echo $redirect_urlset; ?>';
                return false;          
              }             
            }
            });
            return false;
          }); 
        });  
      </script>
      <?php 
    }
  }
}
?>
<!-- End 17-11-16 -->

<!-- New code for popup details 18-11-16 -->
<?php include('popup_details.php'); ?>
<!-- End 18-11-16 -->




<script>
   $(function(){
        var x = 0;
        setInterval(function(){
            x-=1;
            $('.moving').css('background-position', x + 'px 0');
        }, 50);
    })
</script>

<script>

  $(function() {
      
    $('a[href*="#"]:not([href="#"])').click(function() {
    if ( $( this ).hasClass( "check" ) ){
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
        scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
    });
  }); 
</script>
<?php 
/*if(($log_menu_status == 1) || ($unlog_menu_status == 1))
{*/
  ?>
  <script>
    $(window).scroll(function(){
        'use strict';
        if ($(this).scrollTop() > 50){  
            $('.cbp-af-header').addClass("sticky");
        }
        else{
            $('.cbp-af-header').removeClass("sticky");
        }
    });
  </script>
  <?php
//}
?>  
<script type="text/javascript">
  function setupajax_login()
  { 
      
    var emails     = $('#emails').val();
    var passwords  = $('#passwrd').val();
    var signins    = $('#signin').val(); 
    var minhacheck = $('#minhacheck').val();
    //alert(minhacheck);die;
      $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>cashback/logincheck",
      data: {'email':emails,'password':passwords,'signin':signins},
      cache: false,
      success: function(result)
      {
        if(result!=1)
        {
          $('#newdis').html(result);
          return false;
        }
        else
        {
          if(minhacheck!='')
          {
            //alert("hai"); die;
            window.location.href = '<?php echo base_url();?>minha-conta';
            return false;
          }
          else
          {
            <?php $redirect_urlset =  base_url(uri_string());?>
            window.location.href = '<?php echo $redirect_urlset; ?>';
            return false;
          }  
        }             
      }
    });
    return false;
  }
  </script>
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/jquery.validate.min.js"></script> 
<script type="text/javascript">
/* form validation*/
 $(document).ready(function() {

         $("#regform").validate({
            rules: {

        user_email: {

                    required: true,

          email :true

                },

        user_pwd: {

                    required: true,

          minlength: 6

                },    

            },

            messages: {

        
        user_email: {

                      required: "Please enter your valid Emailid."
                    },

        user_pwd: {

                    required: "Please enter the password.",

          minlength: "Passwords must be minimum 6 characters."    

                },

        pwd_confirm: {

                    required: "Please confirm your password.",

          minlength: "Passwords must be minimum 6 characters.",

                },
            }
        });


    $('.first_popup').click(function(){
          
        $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>cashback/update_firstpoup_satus",
        data: {'poup_status':0},
        cache: false,
        success: function(result)
        {
          if(result!=1)
          {
            return false;
          }
          else
          {
            <?php $redirect_urlset =  base_url(uri_string());?>
            window.location.href = '<?php echo $redirect_urlset; ?>';
            return false;          
          }             
        }
      });
      return false;
      });
           
});
</script>

<script type="text/javascript">
//check email for  registration

function check_email()
{

  var email = $('#user_email').val();

  //alert(email);
if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
{
    $.ajax({

      type: 'POST',

      url: '<?php echo base_url();?>cashback/check_email',

      data:{'email':email},

       success:function(result){

        if(result.trim()==1)

        {

          $("#unique_name_error").css('color','#29BAB0');
          
           $("#unique_name_error").html('available.');

           $("input[name='register']").attr("disabled", false); 
        }

        else

        {

          $("#unique_name_error").css('color','#ff0000');
          $("#unique_name_error").css('font-size','14px');
          $("#unique_name_error").css('font-weight','bold');

          $("#unique_name_error").html('This email is already exists.'); 

          $("input[name='register']").attr("disabled", true); 

        }

      }

    });
}
  return false;

}

function valid_email()
{

  var emailid = $('#emailid').val();

if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailid))
{
    $.ajax({

      type: 'POST',

      url: '<?php echo base_url();?>cashback/check_vaild_email',

      data:{'email':emailid},

       success:function(result){

        if(result.trim()==1)

        {

          $("#unique_email_error").css('color','#29BAB0');
          

           $("#unique_email_error").html('available.');

        }

        else

        {

          $("#unique_email_error").css('color','#ff0000');

          $("#unique_email_error").html('This email is already exists.'); 


        }

      }

    });
}
  return false;

}

</script>

<?php

if(isset($_GET['store']))
{
if($_GET['store']=='login')	
{
?>
<script>
$('#login').modal('show');
</script>
<?php 
}
else
{
?>
<script>
$('#register').modal('show');
</script>
<?php
}
}
?>


<?php

if(($this->uri->segment(1) == 'minha-conta') || ($this->uri->segment(1) == 'extrato') || ($this->uri->segment(1) == 'loja-nao-avisou-compra') || ($this->uri->segment(1) == 'resgatar-compra-nao-avisada') || ($this->uri->segment(1) == 'loja-cancelou-minha-compra') || ($this->uri->segment(1) == 'resgate') || ($this->uri->segment(1) == 'indique-e-ganhe') || ($this->uri->segment(1) == 'saidas-para-loja'))
{
?>
<script>
$('#login').modal('show');
</script>
<?php 
}
?>

<!-- modal login,registration,forgot page popup files Start -->

  <script type="text/javascript"> 
    $(document).on('click','#signupclk',function()
  { 
    $('#login').modal('hide');
    $("#login").on('hide', function () {
    $('#register').modal('show');
    });
  });

  $(document).on('click','#signinclk',function()
  {
     $('#register').modal('hide');
    $("#register").on('hide', function () {
    $('#login').modal('show'); 
    });
  });

  $(document).on('click','#signupclks',function()
  {  
    $('#pingou').modal('hide');
    $("#pingou").on('hide', function () {
    $('#entrars').modal('show'); 
    });
  });

  $(document).on('click','#signinclks',function()
  { 
    $('#entrars').modal('hide');
    $("#entrars").on('hide', function () {
    $('#pingou').modal('show');
    });
  });

  $(document).on('click','#forgotclk',function()
  { 
    $('#login').modal('hide');
    $("#login").on('hide', function () {
    $('#forgot').modal('show');
    });
  });

  $(document).on('click','#forgotclick',function()
  { 
    $('#forgot').modal('hide');
    $("#forgot").on('hide', function () {
    $('#login').modal('show');
    });
  });


  </script>  
  <!-- Popup End -->
  
  <!-- Forgot password page Validation Start -->
  <script type="text/javascript">
    /* form validation*/
      $("#forget_password").validate({
            rules: {
        email: {
                     required: true,
            email :true
                }     
            },
            messages: {
        email: {
                   required: "Please enter  your valid Emailid."                  
               }
      }
        });
   
  </script>
  <!-- Forgot End -->

  <!-- New code fo search store and submit action -->
  <script type="text/javascript">
  function submit_form()
  {
    //var storeid_enc = $('#search').val();
    //alert(storeid);
    //var storeid = storeid_enc.replace(/ /g,"-");
    //var storeid = storeid.toLowerCase();
    var storeid = $('#search').val();
    var redirect_url = '<?php echo base_url();?>cupom-desconto/'+storeid;
    $('#storehead').val(storeid);     
    $('#dummyform').attr('action', redirect_url).submit();
    //window.location.href=redirect_url;
    return false;
  }
  </script>
  <!-- End -->

   
  <!-- New code for search content in top of the page 6-8-16 -->
<script type="text/javascript">
  $('#search_content').hide();
  $('#selsearch').each(function() {
    $(this).show(0).on('click', function(e) {
        e.preventDefault();
        $('#search_content').slideToggle('fast');
        $('#search').focus();
    });
});
</script>
<script type="text/javascript">
  $('#search_content1').hide();
  $('#selsearch1').each(function() {
    $(this).show(0).on('click', function(e) {
        e.preventDefault();
        $('#search_content1').slideToggle('fast');
        $('.searchs').focus();
    });
});
</script>
<!-- End -->

<script src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery-ui.js"></script>
<script type="text/javascript">
  //New code for search//
  
<?php $search_name =$this->front_model->search_name(); ?>
var avail = <?php echo $search_name; ?>;
jQuery(".search_header").autocomplete({

   source: avail,        
   select: function (event, ui) 
   {
          autoFocus: true;
          location.href = ui.item.the_link;

    $("#product_id").val(ui.item.product_id); 

   },

}).autocomplete( "instance" )._renderItem = function( ul, item ) {
var msg = '<a href="'+item.the_link+'"><div style="margin:8px; height:70px;" class="search_list"><span><div class="search_icon"><img width="150" height="70" class="img-response" src="<?php echo $this->front_model->get_img_url(); ?>uploads/affiliates/'+item.logo+'" /></div><strong style="margin-left:10px; text-transform: capitalize;">'+item.label+'</strong></span></div></a>';
return $( "<li>" )
.append( msg )
.appendTo( ul );

};
//End//
</script>
</body>
</html>
