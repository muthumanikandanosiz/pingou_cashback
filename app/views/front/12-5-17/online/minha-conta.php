<?php include('header.php'); ?>
            			
<!-- header content End -->
<style>
	.img-responsive.cls_topad {
    border-radius: 20px;
	}
	.error
	{
		color:#ff0000;
	}
	.required_field
	{
	 color:#ff0000;
	}
	.osiz_newcls
	{
		display: none;
    	float: right;
    	margin-bottom: 10px;
    	width: 438px;
	}
</style>
<!-- Main Content start -->
<?php  
$user_id     = $this->session->userdata('user_id');
$userdetails = $this->front_model->userdetails($user_id);
$password 	 = $userdetails->password;		
$user_email  = $userdetails->email;
$acc_page_status = $getadmindetails[0]->acc_page_status;
/*$myacc_banner_details  = $this->db->query("SELECT * from `sales_banner_status` where sales_banner_id =1")->row();
$myaccount_name          = $myacc_banner_details->minhaconta;
$myaccount_top_status    = $myacc_banner_details->minhaconta_top_status;
$myaccount_bot_status    = $myacc_banner_details->minhaconta_bottom_status; 
$bannerdetails           = $this->db->query("SELECT * from `sales_funnel` where sales_funnel_banner_id =1")->row();*/
?>
<!-- 
<div class="btn-group btn-breadcrumb">
	<a href="#" class="btn btn-info"><i class="glyphicon glyphicon-home"></i></a>
	<a href="#" class="btn btn-info">Snippets</a>
	<a href="#" class="btn btn-info">Breadcrumbs</a>
	<a href="#" class="btn btn-info">Info</a>
</div>
-->
<section class="cms wow fadeInDown accountsection">

  	<div class="container">
        
    	<div class="heading wow bounceIn">
      		<h2> My <span>Data</span> </h2>
     		<div class="heading_border_cms">
     			<span>
     				<img src="<?php echo $this->front_model->get_img_url();?>front/new/images/top_drop.png">
     			</span>
     		</div>
    	</div>
    	<div class="myac">
      		<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1" id="hide_fn">
		        <div class="my_account my_accblk">
		          	<div class="myacc-maintab">
		            	<!-- Nav tabs -->
			            <?php $this->load->view('front/user_menu'); ?>
			            <ul class="data-drop wow fadeInDown list-inline list-unstyled cls-greybgmenublk"  role="tablist">
				            <li id="profiles"  class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Dados do Perfil</a> </li>
				            <li id="payments"  class=""><a href="#payment"  aria-controls="payment"  role="tab" data-toggle="tab">Dados de Pagamento</a></li>
				            <li id="passwords" class=""><a href="#password" aria-controls="password" role="tab" data-toggle="tab">Mudar Senha</a></li>
				            <li id="notifys"   class=""><a href="#notify"   aria-controls="notify"   role="tab" data-toggle="tab">Notificação</a></li>
				            <?php
				            if($acc_page_status == 1)
				            {
				            	?>
				            	<style type="text/css">
				            		.my_account.my_accblk .data-drop {
				            			width: 91%;
				            			margin: 16px 0 0 5%;
				            		}
				            	</style>
				            	<li><a href="#account"  aria-controls="account"  role="tab" data-toggle="tab">Account</a></li>
			            		<?php 
				            }
				            ?>
			            </ul>
		          	</div>
		          <!-- Tab panes -->		          
		          	<!-- New code for add sales funnel banner details 31-10-16-->
		          	<?php 
			        include('top_banners.php');
          			?>
		          	<br>
		          	<!-- End 31-10-16 -->
		          	<div class="tab-content">

		          		<!-- My account details page content start-->
			            <div role="tabpanel" class="tab-pane active wow fadeInDown" id="profile">			              	
			              	<?php 
							$error = $this->session->flashdata('error');
							if($error!="")
							{
								echo '<div class="alert alert-warning">
								<button data-dismiss="alert" class="close">x</button>
								<strong>'.$error.'</strong></div>';
							}
							$success = $this->session->flashdata('success');
							$newsuccess = $this->session->flashdata('success_msg');
							if($newsuccess)
							{
								echo '<div class="alert alert-success">
								<button data-dismiss="alert" class="close">x</button>
								<strong>'.$newsuccess.' </strong></div>';
							}
							if($success!="")
							{
								echo '<div class="alert alert-success">
								<button data-dismiss="alert" class="close">x</button>
								<strong>'.$success.' </strong></div>';
							}

							//form begin
							$attributes = array('role'=>'form','name'=>'profile_form','id'=>'profile_form','method'=>'post','class'=>'form-horizontal comm-greybg');
							echo form_open('update-account',$attributes);
							?>
				                <div class="row wow fadeInDown accblk">
				                  	<h4>Basic data</h4>
					                <div class="row wow fadeInDown accblk">
					                    <div class="col-sm-6 col-xs-12">
					                      	<div class="form-group">
					                        	<label for="inputEmail3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Nome<!-- Name --> <span class="required"> * </span> </label>
					                        	<div class="col-sm-12 col-xs-12">
					                          		<input type="text" class="form-control comm-acctbox" id="first_name" name="first_name" value="<?php echo $results->first_name;?>">
					                        	</div>
					                      	</div>
					                    </div>
					                    <div class="col-sm-6 col-xs-12">
					                      	<div class="form-group">
					                        	<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Sobrenome<!-- Last Name --> <span class="required"> * </span> </label>
					                        	<div class="col-sm-12 col-xs-12">
					                         		<input type="text" class="form-control comm-acctbox" id="last_name" name="last_name" value="<?php echo $results->last_name;?>">
					                        	</div>
					                        </div>
					                    </div>
					                    <div class="col-sm-6 col-xs-12">
					                      	<div class="form-group">
					                        	<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel">Email <span class="required"> * </span> </label>
					                        	<div class="col-sm-12 col-xs-12">
					                          		<input type="text" readonly="" class="form-control comm-accemail" value="<?php echo $results->email;?>">
					                        	</div>
					                      	</div>
					                    </div>
					                   	<!--
					                    <div class="col-sm-6 col-xs-12">
					                      	<div class="form-group">
					                        	<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Sex <span class="required"> * </span> </label>
						                        <div class="col-sm-12 col-xs-12">
						                          	<div class="clearfix">
							                            <div class="fleft">
							                              	<select id="cd-dropdown sex" name='sex' class="cd-select valid">
							                                	<option value="0" <?php if($results->sex=='0'){ echo 'selected="selected"'; }?>></option>
											                    <option value="1" <?php if($results->sex=='1'){ echo 'selected="selected"'; }?>>Masculino</option>
											                    <option value="2" <?php if($results->sex=='2'){ echo 'selected="selected"'; }?>>Fêmea</option>
							                              	</select>
							                            </div>
						                          	</div>
						                        </div>
					                      	</div>
					                    </div>
					                    -->
					                    <div class="col-sm-6 col-xs-12">
					                      	<div class="form-group">
					                        	<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Sex <span class="required"> * </span> </label>
					                        	<div class="col-sm-12 col-xs-12">
							                        <div class="clearfix">
							                            <div class="select-style3">
								                            <select id="cd-dropdown sex" name='sex'>
								                            	<!-- <option value="0" selected>Select One</option> -->
								                            	<option value="0" <?php if($results->sex=='0'){ echo 'selected="selected"'; }?>></option>
								                                <option value="1" <?php if($results->sex=='1'){ echo 'selected="selected"'; }?>>Masculino</option>
											                    <option value="2" <?php if($results->sex=='2'){ echo 'selected="selected"'; }?>>Fêmea</option>
								                            </select>
							                            </div>
							                        </div>
						                        </div>
					                      	</div>
					                    </div>
					                    <div class="col-sm-6 col-xs-12">
						                    <div class="form-group">
						                        <label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Telefone <span class="required"> * </span> </label>
						                        <div class="col-sm-12 col-xs-12">
						                        	<input type="text" class="form-control comm-acctbox" id="contact_no" name="contact_no" onkeypress="return isNumber(event)" value="<?php echo $results->contact_no; ?>">
						                        </div>
						                        	<label id="contact_no_error" style="color:red; margin-left:15px; display:none;"></label>
						                    </div>
					                    </div>
					                    <div class="col-sm-6 col-xs-12">
						                    <div class="form-group">
						                        <label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Celular <span class="required"> * </span> </label>
						                        <div class="col-sm-12 col-xs-12">
						                        	<input type="text" class="form-control comm-acctbox" id="cell_no" name="cell_no" onkeypress="return isNumber(event)" value="<?php echo $results->celular_no; ?>">
						                        </div>
						                        <label id="cell_no_error" style="color:red; margin-left:15px; display:none;"></label>
						                    </div>
					                    </div>
					                    <?php 
					                    if($results->ifsc_code!='')
					                    {
					                    	?>
						                    <div class="col-sm-6 col-xs-12">
							                    <div class="form-group">
							                        <label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> CPF do titular <span class="required"> * </span> </label>
							                        <div class="col-sm-12 col-xs-12">
							                        	<input type="text" readonly="" class="form-control comm-acctbox" id="ifsc_code" name="ifsc_code" onkeypress="return isNumber(event)" value="<?php echo $results->ifsc_code; ?>">
							                        </div>
							                        <label id="ifsc_code_error" style="color:red; margin-left:15px; display:none;"></label>
							                    </div>
						                    </div>
						                    <?php 
						                }
						                ?>
					                </div>
				                </div>
				                <div class="row wow fadeInDown accblk">
				                  	<h4>address</h4>
				                  	<div class="row wow fadeInDown accblk">
					                    <div class="col-sm-6 col-xs-12">
						                    <div class="form-group">
						                        <label for="inputEmail3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Rua<!-- Street --> <span class="required"> * </span> </label>
						                        <div class="col-sm-12 col-xs-12">
						                          	<input type="text" class="form-control comm-acctbox" id="street" name="street" value="<?php echo $results->street; ?>">
						                        </div>
						                    </div>
					                    </div>
					                    <div class="col-sm-6 col-xs-12">
					                      	<div class="form-group">
					                        	<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Número da rua<!-- Street number --> <span class="required"> * </span> </label>
					                        	<div class="col-sm-12 col-xs-12">
					                          		<input type="text" class="form-control comm-acctbox" maxlength="6" onkeypress="return isNumber(event)" id="streetnumber" name="streetnumber" value="<?php echo $results->streetnumber; ?>">
					                        	</div>
					                      	</div>
					                    </div>

					                    <!-- New field 16-12-16 -->
					                    <div class="col-sm-6 col-xs-12">
						                    <div class="form-group">
						                        <label for="inputEmail3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Complemento<!-- complement --></label>
						                        <div class="col-sm-12 col-xs-12">
						                          	<input type="text" class="form-control comm-acctbox" maxlength="100" id="complemento" name="complemento" value="<?php echo $results->complemento; ?>">
						                        </div>
						                    </div>
					                    </div>
					                    <div class="col-sm-6 col-xs-12">
					                      	<div class="form-group">
					                        	<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Bairro<!-- Neighborhood --></label>
					                        	<div class="col-sm-12 col-xs-12">
					                          		<input type="text" class="form-control comm-acctbox" maxlength="100" id="bairro" name="bairro" value="<?php echo $results->bairro; ?>">
					                        	</div>
					                      	</div>
					                    </div>
					                    <!-- End 16-12-16 -->


					                    <div class="col-sm-6 col-xs-12">
					                      	<div class="form-group">
					                        	<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Cicade <!-- City --> <span class="required"> * </span> </label>
					                        	<div class="col-sm-12 col-xs-12">
					                          		<input type="text" class="form-control comm-acctbox" id="city" name="city" value="<?php echo $results->city; ?>">
					                        	</div>
					                      	</div>
					                    </div>
					                    <div class="col-sm-6 col-xs-12">
					                      	<div class="form-group">
					                        	<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Estado<!-- State --> <span class="required"> * </span> </label>
					                        	<div class="col-sm-12 col-xs-12">
					                          		<input type="text" class="form-control comm-acctbox" id="state" name="state" value="<?php echo $results->state; ?>">
					                        	</div>
					                      	</div>
					                    </div>
					                    <div class="col-sm-6 col-xs-12">
					                      	<div class="form-group">
					                        	<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Cep<!-- Zip Code --> <span class="required"> * </span> </label>
					                        	<div class="col-sm-12 col-xs-12">
					                          		<input type="text" class="form-control comm-acctbox" id="zipcode" name="zipcode" maxlength="9" onkeypress="return isNumber(event)" value="<?php echo $results->zipcode; ?>">
					                        	</div>
					                      	</div>
					                    </div>
					                    <?php 
					                    $selqry   	  = $this->db->query("SELECT * from country where id='$results->country'")->row(); 
										$country_name = $selqry->country_name;
					                    ?>
					                    <div class="col-sm-6 col-xs-12">
					                      	<div class="form-group">
					                        	<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Pais <!-- Country --><span class="required"> * </span> </label>
					                        	<div class="col-sm-12 col-xs-12">
					                          		<input type="text" class="form-control comm-acctbox" readonly id="country" name="countryname" value="<?php echo $country_name;?>">
					                          		<input type="hidden" class="form-control" readonly id="country" name="country" value="<?php echo $results->country;?>">
					                        	</div>
					                      	</div>
					                    </div>
				                	</div>
				                </div>
				                <div class="row wow fadeInDown accblk">
				                  	<div class="form-group">
				                    	<div class="col-sm-12 col-xs-12">
				                      		<center>
				                        		<input type="submit" id="save_changes" name="save_changes" value="Salvar Alterações" class="acc-commbtn">
				                      		</center>
				                    	</div>
				                  	</div>
				                </div>
				                <input type="hidden" name="user_id" id="user_id" value="<?php echo $results->user_id;?>">
				                <input type="hidden" name="save_changes" id="save_changes" value="Salvar Alterações">
			              	<?php echo form_close();?>
			            </div>
			            <!-- My account details page content End-->

			            <!-- sub menu tab content starts -->
			            
			            <!-- Payment data page content start -->
			            <div id="payment" role="tabpanel" class="tab-pane pay-tabblk wow fadeInDown">
			              	<?php
							//form begin
							$attributes = array('role'=>'form','name'=>'bankpayment_form','id'=>'bankpayment_form','method'=>'post','class'=>'form-horizontal');
							echo form_open('bankpayment',$attributes);
							?>
				                <div class="col-sm-6 col-xs-12 fn center-block wow fadeInDown accblk comm-greybg">
				                  	<h4>Insira seus dados bancários</h4><br>
				                  	<div class="row wow fadeInDown accblk">

					                    <div class="col-sm-12 col-xs-12">
					                    	<div class="clearfix">
                            					<div class="fleft">
					                    			<div class="form-group">
					                        			<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Nome do titular da conta </label>
					                        			<div class="col-sm-12 col-xs-12">
					                          				<input type="text"  class="form-control comm-accemail" name="act_holder" value="<?php echo $results->account_holder;?>" id="act_holder">
					                        			</div>
					                      			</div>
					                    		</div>
					                    	</div>
					                    </div>		
					                    <div class="col-sm-12 col-xs-12">
					                    	<div class="clearfix">
                            					<div class="fleft">
							                    	<div class="form-group">
							                        	<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> CPF do titular </label>
							                        	<div class="col-sm-12 col-xs-12">
							                          		<?php
							                          		if($results->ifsc_code!='')
							                          		{
							                          			?>
							                          			<input type="text" readonly="" class="form-control comm-accemail" maxlength="14" name="ifsc_code" id="ifsc_code" value="<?php echo $results->ifsc_code; ?>">	
							                          			<?php
							                          		}
							                          		else
							                          		{
							                          			?>
							                          			<input type="text"  class="form-control comm-accemail" maxlength="14" onblur='return check_cpf();' name="ifsc_code" id="ifsc_code" value="<?php echo $results->ifsc_code; ?>" autocomplete='off'>
							                          			<div id="unique_name_error"></div>
							                          			<?php 
							                          		} 
							                          		?>
							                          		
							                        	</div>
							                      	</div>
							                    </div>
							                </div>      	
					                    </div>
					                    <div class="col-sm-12 col-xs-12">
					                    	<div class="clearfix">
                            					<div class="fleft">
							                      	<div class="form-group">
							                        	<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Nome do banco </label>
								                        <div class="col-sm-12 col-xs-12">
								                          <div class="clearfix">
								                            <div class="select-style3">
								                              	<select id="cd-dropdown2 bank_name"  name="bank_name">
									                               	<option value="">Select One</option>
									                               	<?php
																 	foreach($notify as $newdata)
																	{
																 		?>							     
																	  	<option <?php if($newdata->bank_id==$results->bank_name){ echo 'selected="selected"'; } ?>value="<?=$newdata->bank_id?>"><?=$newdata->bank_name?></option>
										                           	 	<?php
																	}
																  	?>
								                              	</select>
								                            </div>
								                          </div>
								                        </div>
							                      	</div>
							                    </div>
							                </div>      	
					                    </div>
					                    <div class="col-sm-12 col-xs-12">
					                    	<div class="clearfix">
                            					<div class="fleft">
						                      		<div class="form-group">
						                        		<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Agência </label>
						                        		<div class="col-sm-12 col-xs-12">
						                          			<input type="text" class="form-control comm-accemail"  name="bank_brch_name" value="<?php echo $results->branch_name;?>" id="bank_brch_name">
						                        		</div>
						                      		</div>
						                      	</div>
						                    </div>  		
					                    </div>
					                    <div class="col-sm-12 col-xs-12">
					                    	<div class="clearfix">
                            					<div class="fleft">
							                      	<div class="form-group">
							                        	<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Número da conta </label>
							                        	<div class="col-sm-12 col-xs-12">
							                          		<input type="text" class="form-control comm-accemail" name="act_no" value="<?php echo $results->account_number;?>" id="act_no">
							                        	</div>
							                      	</div>
							                    </div>
							                </div>      	
					                    </div>
					                    <div class="col-sm-12 col-xs-12">
					                    	<div class="clearfix">
                            					<div class="fleft">
								                    <div class="form-group">
								                    	<label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Digite sua senha para confirmar </label>
								                        <div class="col-sm-12 col-xs-12">
								                        	<input type="password" class="form-control comm-accemail" name="con_pwd" id="con_pwd">
								                        </div>
								                    </div>
								                </div>
								            </div>        
					                    </div>
				                  	</div>
				                <div class="row wow fadeInDown accblk">
				                  <div class="form-group">
				                    <div class="col-sm-12 col-xs-12">
				                      <center>
				                        <input type="submit" name="save_chages" id="save_chages" value="Salvar Alterações" class="acc-commbtn">
				                      </center>
				                    </div>
				                  </div>
				                </div>
				                </div>
				                <input type="hidden" name="save_chages" id="save_chages" value="Salvar Alterações">
				                <input type="hidden" name="user_id" id="user_id" value="<?php echo $results->user_id;?>">
			              	<?php echo form_close();?>
			            </div>
			            <!-- Payment data page content End -->

			            <!-- Change password page content start -->
			            <?php 
			            if($password !='')
			            {	
			            	?>
			            	<div id="password" role="tabpanel" class="change-pwdblk tab-pane wow fadeInDown">
				              	<?php
								//form begin
								$attributes = array('role'=>'form','name'=>'account_form','id'=>'account_form','method'=>'post','class'=>'form-horizontal');
								echo form_open('change-password',$attributes);
								?>
				                <div class="col-sm-6 col-xs-12 fn center-block wow fadeInDown accblk comm-greybg">
				                  <h4>Change Password</h4>
				                  <div class="row wow fadeInDown accblk">
				                    
				                    <div class="col-sm-12 col-xs-12">
				                      <div class="form-group">
				                        <label for="inputEmail3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Senha antiga<!-- Current Password --> </label>
				                        <div class="col-sm-12 col-xs-12">
				                          <input type="password" class="form-control comm-acctbox" name="old_password" id="old_password" autocomplete='off'>
				                        </div>
				                      </div>
				                    </div>
				                    
				                    <div class="col-sm-12 col-xs-12">
				                      <div class="form-group">
				                        <label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Senha nova<!-- New Password --> </label>
				                        <div class="col-sm-12 col-xs-12">
				                          <input type="password" class="form-control comm-acctbox" name="new_password" id="new_password" autocomplete='off'>
				                        </div>
				                      </div>
				                    </div>
				                    
				                    <div class="col-sm-12 col-xs-12">
				                      <div class="form-group">
				                        <label for="inputPassword3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> Confirme a senha<!-- Confirm Password --> </label>
				                        <div class="col-sm-12 col-xs-12">
				                          <input type="password" class="form-control comm-accemail" name="confirm_password" id="confirm_password" autocomplete='off'>
				                        </div>
				                      </div>
				                    </div>

				                  </div>
				                <div class="row wow fadeInDown accblk">
				                  <div class="form-group">
				                    <div class="col-sm-12 col-xs-12">
				                      <center>
				                        <input type="submit" name="save" id="save" value="Save" class="acc-commbtn">
				                      </center>
				                    </div>
				                  </div>
				                </div>
				                </div>
				                <input type="hidden" name="save" id="save" value="Save">
				                <input type="hidden" name="user_id" id="user_id" value="<?php echo $results->user_id;?>">
				              	<?php echo form_close();?>
				            </div>
			            	<?php 
			            }
			            else
			            {
			            	?>
			            	<div id="password" role="tabpanel" class="change-pwdblk tab-pane wow fadeInDown">
				              	<?php
								//form begin
								$attributes = array('role'=>'form','name'=>'forget_password','id'=>'forget_password','method'=>'post','class'=>'form-horizontal');
								echo form_open('forgetpassword',$attributes);
								?>
				                <div class="col-sm-6 col-xs-12 fn center-block wow fadeInDown accblk comm-greybg">
				                  	<h4>Set Your Password</h4><br><br>
				                  	<div class="row wow fadeInDown accblk" style="margin-left: 0;">
					                    <div class="col-sm-12 col-xs-12">
					                      <div class="form-group">
					                        Currently, you access a Pingou using Your Social Account. 
					                        <br> So we need to create your password using your email.
					                        <b><?php echo $user_email; ?></b> 
					                      </div>
					                    </div>
				                  	</div>
				                  	<input type="hidden" class="form-control" name="email" id="email" value="<?php echo $user_email; ?>">
				                <div class="row wow fadeInDown accblk">
				                  <div class="form-group">
				                    <div class="col-sm-12 col-xs-12">
				                      <center>
				                        <input type="submit" name="reset" id="reset" value="Create Password" class="acc-commbtn">
				                      </center>
				                    </div>
				                  </div>
				                </div>
				                </div>
				                <input type="hidden" name="save" id="save" value="Save">
				                <input type="hidden" name="user_id" id="user_id" value="<?php echo $results->user_id;?>">
				              <?php echo form_close();?>
				            </div>
			            	<?php 
			            }
			            ?>
			            <!-- Change password page content End -->

			            <!-- Notification page content start -->
			            <div id="notify" role="tabpanel" class="notify-blk tab-pane wow fadeInDown">
			              	<?php 
			              	$attributes = array('role'=>'form','name'=>'account_form','id'=>'account_form','method'=>'post','class'=>'form-horizontal');
                    		echo form_open('notifymail-update',$attributes);
			                ?>
			                <div class="row wow fadeInDown accblk">
			                  	<div class="col-md-10 col-sm-12 col-xs-12 fn center-block">
			                    	<div class="col-md-12 col-sm-12 col-xs-12 wow slideInLeft accblk comm-greybg">
				                      	<div class="row">
				                      		<h4 class="col-sm-9 col-xs-9 pd-left-0 pd-right-0">notificação</h4>
				                      		<h4 class="col-sm-2 col-xs-3 pd-left-0 pd-right-0">Email</h4>
				                      	</div>
				                      	<div class="row clearfix">
				                        	<label for="inputEmail3" class="col-sm-9 col-xs-9 control-label comm-acclabel pd-left-0 pd-right-0"> Update about my cashbacks (Pending,Aprroved,Canceled) <span class="required"> * </span> </label>
				                        	<div class="col-md-2 col-sm-3 col-xs-3 pd-left-0 pd-right-0">
				                        		<div class="col-md-4 col-sm-4 col-xs-6">
				                            		<div class="checkbox clip-check check-primary">
				                              			<input type="checkbox" id="check1" <?php if($results->cashback_mail == 1) echo "checked"; ?> name="cashback" autocomplete='off'>
				                              				<label for="check1"> </label>
				                            		</div>
				                         		</div>
				                        	</div>	
				                      	</div>
				                      	<div class="row clearfix">
					                        <label for="inputEmail3" class="col-sm-9 col-xs-9 control-label comm-acclabel pd-left-0 pd-right-0"> Update about my Withdraws <span class="required"> * </span> </label>
					                        <div class="col-md-2 col-sm-3 col-xs-3 pd-left-0 pd-right-0">
					                          	<div class="col-md-4 col-sm-4 col-xs-6">
					                            	<div class="checkbox clip-check check-primary">
					                             	 	<input type="checkbox" id="check2" <?php if($results->withdraw_mail == 1) echo "checked"; ?>  name="withdraw" autocomplete='off'>
					                              		<label for="check2"> </label>
					                            	</div>
					                          	</div>
					                        </div>
				                      	</div>
				                      	<div class="row clearfix">
				                        	<label for="inputEmail3" class="col-sm-9 col-xs-9 control-label comm-acclabel pd-left-0 pd-right-0"> Update about Referral program (Bonus and friends indicated) <span class="required"> * </span> </label>
				                        	<div class="col-md-2 col-sm-3 col-xs-3 pd-left-0 pd-right-0">
				                          		<div class="col-md-4 col-sm-4 col-xs-6">
				                            		<div class="checkbox clip-check check-primary">
						                              <input type="checkbox" id="check3" <?php if($results->referral_mail == 1) echo "checked"; ?> name="referral" autocomplete='off'>
						                              <label for="check3"> </label>
						                            </div>
				                          		</div>
				                        	</div>
				                      	</div>
				                      	<div class="row clearfix">
					                        <label for="inputEmail3" class="col-sm-9 col-xs-9 control-label comm-acclabel pd-left-0 pd-right-0"> Update about Dripped money in your account (Pendente) <span class="required"> * </span> </label>
					                        <div class="col-md-2 col-sm-3 col-xs-3 pd-left-0 pd-right-0">
						                        <div class="col-md-4 col-sm-4 col-xs-6">
						                            <div class="checkbox clip-check check-primary">
						                              	<input type="checkbox" id="check4" <?php if($results->acbalance_mail == 1) echo "checked"; ?> name="acbalance" autocomplete='off'>
						                              	<label for="check4"> </label>
						                            </div>
						                        </div>
					                        </div>
				                      	</div>
				                      	<div class="row clearfix">
					                        <label for="inputEmail3" class="col-sm-9 col-xs-9 control-label comm-acclabel pd-left-0 pd-right-0"> Update about Support tickets <span class="required"> * </span> </label>
					                        <div class="col-md-2 col-sm-3 col-xs-3 pd-left-0 pd-right-0">
					                          	<div class="col-md-4 col-sm-4 col-xs-6">
					                            	<div class="checkbox clip-check check-primary">
					                              		<input type="checkbox" id="check5" <?php if($results->support_tickets == 1) echo "checked"; ?> name="support_tickets" autocomplete='off'>
					                              		<label for="check5"> </label>
					                            	</div>
					                          	</div>
				                        	</div>
				                      	</div>
				                      	<div class="row clearfix">
				                        	<label for="inputEmail3" class="col-sm-9 col-xs-9 control-label comm-acclabel pd-left-0 pd-right-0"> Newsletter of exclusive promotions and coupons of pingou <span class="required"> * </span> </label>
				                        	<div class="col-md-2 col-sm-3 col-xs-3 pd-left-0 pd-right-0">
				                         	 	<div class="col-md-4 col-sm-4 col-xs-6">
				                            		<div class="checkbox clip-check check-primary">
				                              			<input type="checkbox" id="check6" <?php if($results->newsletter_mail == 1) echo "checked"; ?> name="newsletter" autocomplete='off'>
				                              			<label id="check6" for="check6"> </label>
				                            		</div>
				                          		</div>
				                        	</div>
				                      	</div>
				                      	<div class="osiz_newcls" id="news_notify">Click in "Save settings" and from now on you'll receive Pingou newsletters </div>
				                      	<div class="row clearfix">
                       						<center>
			                        			<input type="submit" name="save" id="save"  class="acc-commbtn" value="Salvar dados">
			                    			</center>
			                			</div>
			                    	</div>
			                  	</div>
			                </div>
			                <input type="hidden" name="save" id="save" value="Salvar dados">
			                <input type="hidden" name="user_id" id="user_id" value="<?php echo $results->user_id;?>">
			                 
			                <?php echo form_close();?>
			            </div>
			            <!-- Notification page content End -->
			            <br>

			            <!-- new code for account page content start 17-3-17-->
			            <?php
			            if($acc_page_status == 1)
			            {
				            ?>
				            <div id="account" role="tabpanel" class="notify-blk tab-pane wow fadeInDown">
				              	<?php 
				              	$attributes = array('role'=>'form','name'=>'myaccount_form','id'=>'myaccount_form','method'=>'post','class'=>'form-horizontal');
	                    		echo form_open('myaccount-update',$attributes);
				                ?>
				                <div class="row wow fadeInDown accblk">
				                  	<div class="col-md-10 col-sm-12 col-xs-12 fn center-block">
				                    	<div class="col-md-12 col-sm-12 col-xs-12 wow slideInLeft accblk comm-greybg">
					                      	<div class="row">
					                      		<center><h4 class="">DELETE MY PINGOU ACCOUNT</h4></center>
					                      	</div>
					                      	<div class="row clearfix">
					                        	 <p>this function will delete you pingou account. Before processed there is a few things that you need to know.</p>
					                        	 <p>
					                        	 	Doing this you will loose all the cashbacks you currently have and you wont be able to recover it again.
					                        	 </p>
					                      	</div>
					                      	<div class="row clearfix">
	                       						<center>
				                        			<a name="save" id="save"  onclick="delete_acc();" class="acc-commbtn" style="cursor:pointer; padding: 8px 55px !important;">DELETE ACCOUNT</a>
				                    			</center>
				                			</div>
				                    	</div>
				                  	</div>
				                </div>
				                <input type="hidden" name="save" id="save" value="Salvar dados">
				                <input type="hidden" name="user_id" id="user_id" value="<?php echo $results->user_id;?>">
				                <?php echo form_close();?>
				            </div>
				            <?php
				        }     
			            ?>
			            <!-- account page content End 17-3-17-->




			            <!-- New code for Sales funnel banner details 31-10-16 -->
			            <?php
				        include('bottom_banners.php');
				        ?>
			            <!-- End 31-10-16 -->
                		<?php $this->load->view('front/my_earnings.php')?>
			            <!-- sub menu tab content ends -->
		          	</div>
		        </div>
      		</div>
    	</div>
    </div>
</section>

<!-- New code for success popup after withdraw request 16-3-17-->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  id="alert_popup">
	<div class="modal-dialog modal-sm">
	  <!-- Modal content-->
	  <div class="modal-content" style="padding: 20px 0;">
	    <div class="modal-body">
	    	<button type="button" class="close" data-dismiss="modal" style="margin-top: -10px;">×</button>
	    	<center>Are you sure? this action can't be Undone!</center>
	    </div>
	    <div class="accblk modal-footer">
	      <center>
	      	<a onclick="delete_now()" style="cursor:pointer;" class="acc-commbtn">Delete Now</a>
	      	<a href="<?php echo base_url();?>cupom" style="cursor:pointer;" class="acc-commbtn">Keep my Account</a>
	      </center>
	    </div>
	    <!-- 
	      <button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
	     -->
	  </div>
	</div>
</div>
<!-- End 16-3-17 -->

<!-- Main Content end -->
<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
<?php $admindetails = $this->front_model->getadmindetails_main();?>
<footer>
  <!-- ========================== -->
  <!-- FOOTER - FOOTER -->
  <!-- ========================== -->
  	<section class="footer-section innerfoot-sect">
	    <div class="container">
	      <div class="clearfix wow fadeInDown">
	      <div class="wow fadeInDown">
	        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
	          <h5>Help And Support</h5>
	          <ul class="footer-nav">
	            <li><a href="<?php echo base_url();?>contato"><i class="fa fa-caret-right"></i> Contact Us</a></li>
	            <li><a href="<?php echo base_url();?>perguntas-frequentes"><i class="fa fa-caret-right"></i> Perguntas Frequentes</a></li>
	            <?php 
	            $result = $this->front_model->sub_menu();
	            foreach($result as $view)
	            {
	            ?>
	              <li><a href="<?php echo base_url(); echo $view->cms_title; ?>"><i class="fa fa-caret-right"></i> <?php echo $view->cms_heading;?></a></li>
	            <?php 
	            }
	            ?>
	          </ul>
	        </div>
	        
	        <?php
	        if($user_id == '')
	        {
	        	?>
		        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
		          <h5>Email Newsletter</h5>
		          <ul class="footer-nav">
		            <li><a href="#"><i class="fa fa-caret-right"></i> Subscribe to recieve inspiration, ideas and news in your inbox.</a></li>
		            <li>
		              <div class="col-md-12 col-sm-12 col-xs-12 pd-left-0">
		              <input type="text" id="news_email" onkeypress="clears(2);" class="form-control tbox1" placeholder="Email Address">
		              <input type="button" class="tbtn1" value="send" onClick="email_news();">
		              </div>
		              &nbsp;
		              <span id="new_msg" style="color:red"></span>
		  			</li>
		          </ul>
		        </div>
		    	<?php 
		    }
		    ?>    
	        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
	          <h5>Top Categories</h5>
	          <ul class="footer-nav">
	            <?php 
	            $categories = $this->front_model->get_all_categories(8);
	            foreach($categories as $view)
	            {
	            ?> 
	            <li><a href="<?php echo base_url();?>products/<?php echo $view->category_url;?>"><i class="fa fa-caret-right"></i> <?php echo $view->category_name;?></a></li>
	            <?php
	            }
	            ?>
	          </ul>
	        </div>
	        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
	          <h5>Top Stores</h5>
	          <ul class="footer-nav">
	          <?php 
	          $stores = $this->front_model->get_all_stores(8);
	          foreach($stores as $view)
	          {
	          ?>
	            <li><a href="<?php echo base_url(); ?>cupom-desconto/<?php echo $view->affiliate_url?>"><i class="fa fa-caret-right"></i> <?php echo $view->affiliate_name; ?></a></li>
	          <?php
	          }
	          ?>
	          </ul>
	        </div>
	      </div>
	      </div>
	    </div>
  	</section>
  	<section class="copyright-section">
	    <div class="container">
	     <div class="row wow fadeInDown">
	      <div class="col-md-6 col-sm-6 col-xs-12 wow flipInX">
	        <p>Copyright &copy; <?php echo date('Y')?> <?php echo $admindetails->site_name;?>  |  All Rights Reserved </p>
	      </div>
	      <?php
	      $listing = $this->db->query("select * from admin")->row();
	      ?>
	      <div class="col-md-6 wow flipInX">
	        <ul class="list-socials pull-right">
	          <?php if($listing->admin_twitter) {?> <li><a href="<?php echo $listing->admin_twitter;?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
	          <?php if($listing->admin_fb)      {?> <li><a href="<?php echo $listing->admin_fb;?>">     <i class="fa fa-facebook"></i></a></li><?php } ?>
	          <?php if($listing->admin_gplus)   {?> <li><a href="<?php echo $listing->admin_gplus;?>">  <i class="fa fa-google-plus"></i></a></li><?php } ?>
	        </ul>
	      </div>
	     </div>
	    </div>
  	</section>
</footer>
 

<!-- Slider --> 
<script type="text/javascript">
$(function () { $("[data-toggle='tooltip']").tooltip(); });
</script>
<!-- Scripts queries --> 
 <?php include('js_scripts.php');?>	  

<!-- New code for popup details 19-11-16 -->
<?php include('popup_details.php'); ?>
<!-- End 19-11-16 -->
 



<script type="text/javascript">
	function delete_acc()
	{
		$('#alert_popup').modal('show');
	}

	function delete_now()
	{
		var user_id = $("#user_id").val();
		if(user_id)
	    {
	        $.ajax({
	            type: 'POST',
	            url: '<?php echo base_url();?>cashback/delete_account_details',
	            data:{'user_id':user_id},
	             success:function(result)
	             {
	                if(result)
	                {	 
			          	window.setTimeout(function(){
			          	window.location.href = "<?php echo base_url();?>logout";
			          	}, 300); 
	                }
	            }
	        });
	    }
	    return false;
	}
</script>




<script type="text/javascript">
	/* form validation*/
         $("#profile_form").validate({
	        
	        rules: 
	        {
				first_name: 
				{
                    required: true
                },
				last_name: 
				{
                    required: true
                },
				street: 
				{
                    required: true
                },
        		streetnumber:
        		{
                    required:true,
                  	// number:true
                   	//minlength: 5
                },  
          		sex:
          		{
		            required:true
		        },             
				city: 
				{
                    required: true
                },
				state: 
				{
                    required: true
                },
				zipcode: 
				{
                    required: true,
					 minlength: 9
                },
				country: 
				{
                    required: true
                },
				contact_no: 
				{
                    required: true,
					minlength: 10
                },
                cell_no: 
				{
                    required: true,
					minlength: 10
                }
							
            },
            messages: 
            {
				first_name: 
				{
                    required: "Insira seu primeiro nome."                    
                },
				last_name: 
				{
                    required: "Insira seu último nome."    
                },
				street:
				{
                    required: "Insira seu endereço."
                },
		        streetnumber: 
		        {
                    required: "Digitar apenas números.",
                    //minlength: "O número da rua deve ter pelo menos 5 dígitos."

                },
          		sex:
          		{
            		required:"selecione qualquer um."
		        },
				city: 
				{
                    required: "Insira sua cidade."
                },
				state: 
				{
                    required: "Insira seu estado."
                },
				zipcode: 
				{
                    required: "Insira seu Cep.",
					minlength: "O cep deve ter pelo menos 8 dígitos."
                },
				country: 
				{
                    required: "Selecione seu país."
                },
				contact_no: 
				{
                    required: "Insira um telefone para contato.",
					minlength: "Contact number must be minimum 10 digits."    
                },
				cell_no: 
				{
                    required: "Insira um Celular para contato.",
					minlength: "Celular number must be minimum 10 digits."    
                }
            },
			submitHandler: function (form) 
			{
				var cellno 	   = $('#cell_no').val();
				var contact_no = $('#contact_no').val();
				var is_error   = 0;
				//var contactlen = contact_no.length;
				//alert(contact_no.indexOf('_'));
				//alert(cellno.indexOf('_'));
				//alert(contact_no.indexOf('_'));
				if (contact_no.indexOf('_') < 13 && contact_no.indexOf('_')!= -1)
				{
					$('#contact_no_error').html('Contact number must be minimum 10 digits.').show();
					is_error = 1;
				}else{
					$('#contact_no_error').html('Contact number must be minimum 10 digits.').hide();
				}
				
				//alert(cellno.indexOf('_'));
				if (cellno.indexOf('_') < 13 && cellno.indexOf('_')!= -1)
				{
					$('#cell_no_error').html('Celular number must be minimum 10 digits.').show();
					is_error = 1;
				}else{
					$('#cell_no_error').html('Celular number must be minimum 10 digits.').hide();
				}
				//alert(is_error);
				if(is_error == 1)
				{
					return false;
				}
				else
				{
					return true;
				}
  			}	
        });
</script>
<script type="text/javascript">
 $("#account_form").validate({
	          rules: {
				old_password: {
                    required: true
                },
				new_password: {
                    required: true,
					minlength: 6,
                },
				
				confirm_password: {
                    required: true,
					minlength: 6,
					equalTo:'#new_password'
                }					
				
				
            },
            messages: {
				old_password: {
                    required: "Insira corretamente a senha antiga."                    
                },
				 new_password: {
                    required: "Insira a nova senha.",
					minlength: "A senha deve ter no mínimo 6 caracteres."    
                    
                },
				
				confirm_password: {
                   required: "Confirme a sua nova senha",
					minlength:"A senha deve ter no mínimo 6 caracteres.",
					equalTo : "A senhas devem coincidir."
                    
                } 
				
				
				
            }
				
});
</script>
<script type="text/javascript">
	/* form validation*/
	$("#bankpayment_form").validate({
	          rules: {
				act_holder: {
                    required: true
                },
				 bank_name: {
                    required: true
                },
				bank_brch_name: {
                    required: true,
                     number :true					
                },
				act_no: {
                    required: true,
                     number :true
					
                },
				 ifsc_code: {
                    required: true,
					minlength: 14
          // number :true
                },			
				con_pwd: {
                    required: true,
					minlength: 6,
					
                }
				
				
            },
            messages: {
				act_holder: {
                    required: "Digite o nome do titular da conta."                    
                },
				 bank_name: {
                   required: "Digite o nome do Banco."
                },
				 bank_brch_name: {
                    required: "Digite a agência.",
                    number : "Please Enter only number."
                    //maxlegth: "Zipcode must be maximum of 7 digits.",


                    
                },
				  act_no: {
                    required: "Digite o número da conta corrente.",
                    number : "Please Enter only number."
					    
                },
				ifsc_code: {
                    required: "ERRO."
                    
                },
				con_pwd: {
                    required: "Para sua segurança, confirme a sua senha.",
					minlength: "A senha deve ter no mínimo 6 caracteres."
					
                    
                }  
				
				
				
				
				
            }
				
});
</script>
<script type="text/javascript">
	/* form validation*/
	$("#cheque_form").validate({
	          rules: {
				cheque_full_name: {
                    required: true
                },
				 cheque_full_adr: {
                    required: true
                },
				cheque_city: {
                    required: true					
                },
				cheque_state: {
                    required: true
                },
				cheque_postal_code: {
                    required: true,
					minlength: 6
                },	
				cheque_contact_no: {
                    required: true
					//minlength: 10
                },	
				 cheque_confirm_pwd: {
                    required: true,
					minlength: 6
				} 
				
				
            },
            messages: {
				cheque_full_name: {
                    required: "Digite seu nome."                    
                },
				cheque_full_adr: {
                   required: "Digite seu endereço completo."
                },
				cheque_city: {
                    required: "Digite a sua cidade."
                    
                },
			    cheque_postal_code: {
					required: "Selecione seu estado."
					    
                },
				postal_code: {
                    required: "Digite seu cep.",
					minlength: "O cep deve ter 8 caracteres."
                    
                },
				cheque_contact_no: {
                    required: "Digite um telefone para contato."
				                 
                },
				cheque_confirm_pwd: {
                    required: "Confirme sua senha.",
					minlength: "A senha deve ter no mínimo 6 caracteres."
				} 
 				
		
            }
				
});
</script>

<style type="text/css">
	.tabela {
	            font-family: Verdana, Arial, Helvetica, sans-serif;
	            font-size: 10px;
	}
</style>

<!-- Format changes in cpf SATz -->
<script>
	function formatar(mascara, documento){
	  var i = documento.value.length;
	  var saida = mascara.substring(0,1);
	  var texto = mascara.substring(i)
	  
	  if (texto.substring(0,1) != saida){
	            documento.value += texto.substring(0,1);
	  }
	  
	}
</script>


<script type="text/javascript">
	function check_cpf()
	{
	    var ifsc_code = $('#ifsc_code').val();
	    if(ifsc_code)
	    {	
	    	$('#unique_name_error').html('');
	        $.ajax({

	            type: 'POST',
	            url: '<?php echo base_url();?>cashback/check_cpf',
	            data:{'ifsc_code':ifsc_code},
	            success:function(result)
	            {
	                if(result.trim()==1)
	                {
	                    $("#unique_name_error").css('color','#29BAB0');
	                    $("#unique_name_error").html('Available.');
	                }
	                else
	                {
	                    $("#unique_name_error").css('color','#ff0000');
	                    //$("#unique_name_error").html('Este é CPF já existe .');  
	                    $("#unique_name_error").html('Este é um CPF não é válido.');
	                    $('#ifsc_code').val('');
	                }
	            }
	        });
	    }
	    return false;
	}
</script>

<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/mask.js"></script>

<script type="text/javascript">
	 
	    $('#ifsc_code').mask('999.999.999-99', {reverse: true});
	    $('#zipcode').mask('99999-999',{reverse: false});
	 
</script>

<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.inputmask.extensions.js"></script>

<script type="text/javascript">
	
		contact = jQuery('#contact_no').inputmask('(99)9999[9]-9999');
		cellno  = jQuery('#cell_no').inputmask('(99)9999[9]-9999');

	    //s= jQuery('#streetnumber').inputmask('(99)9999[9]-9999');
	 
</script>
<?php 
/*if($results->newsletter_mail == 0)
{*/
	?>
	<script type="text/javascript">
		$('#check6').click(function() {
    if ($(this).is(':checked')) {
        $("#news_notify").show();
    } else {
        $("#news_notify").hide();
    }
});
	</script>
	<?php 
//}?>

<?php 	
if($user_id!='')
{

  $popup_templete  = $this->db->query("SELECT * from first_access_popup where all_status_firstpopup=1")->row();
  
  $datas = array(
  '###ADMINNO###'=>$admin_number,
  '###COMPANYLOGO###'=>$site_logo,
  '###SITE NAME###'=>$site_name,
  );

  $first_acc_content  = $popup_templete->first_popup_template;
  $first_acc_popup    = strtr($first_acc_content,$datas);
  $first_popup_status = $popup_templete->all_status_firstpopup;

  if($first_popup_status == 1)
  {
    if($userdetails->first_ac_popup_status == 1)
    { 
      echo $first_acc_popup;
      ?>
      <script type="text/javascript">
        $(document).ready(function()
        {
          $('#first_acc_popup').modal('show');  
          $("#first_acc_popup").on("click", function(e)
          {
            $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>cashback/update_firstpoup_satus",
            data: {'poup_status':0},
            cache: false,
            success: function(result)
            {
              if(result!=1)
              {
                return false;
              }
              else
              {
                <?php $redirect_urlset =  base_url(uri_string());?>
                //window.location.href = '<?php echo $redirect_urlset; ?>';
                return false;          
              }             
            }
            });
            return false;
          }); 
        });  
      </script>
      <?php 
    }
  }
}
?>

</body>

<style type="text/css">
	#alert_popup .acc-commbtn {
    font-size: 13px;
    padding-left: 15px;
    padding-right: 15px;
}
</style>
<script type="text/javascript">
	var hash = window.location.hash;
//console.log(hash);
//alert(hash);
var segments = hash.replace("#", "");

if(segments == 'payment')
{
	$("#payment").addClass('active');
	$("#payments").addClass('active');
	$("#profiles").removeClass('active');
	$("#profile").removeClass('active');
}
if(segments == 'profile')
{
	$("#profile").addClass('active');
	$("#profiles").addClass('active');
	//$("#profile").removeClass('active');
}
if(segments == 'password')
{
	$("#password").addClass('active');
	$("#passwords").addClass('active');
	$("#profiles").removeClass('active');
	$("#profile").removeClass('active');
}
if(segments == 'notify')
{
	$("#notify").addClass('active');
	$("#notifys").addClass('active');
	$("#profiles").removeClass('active');
	$("#profile").removeClass('active');
}
</script>
</html>
