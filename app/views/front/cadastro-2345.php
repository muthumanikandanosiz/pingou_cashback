<!-- header -->
<?php $this->load->view('front/header');?>
<!-- Header ends here -->
<style>
  .error {
    color:#ff0000;
  }
  .required_field {
    color:#ff0000;
  }
  .btn-resc1
  {
    border-radius: 10px;
    color: #fff;
    //float: right;
    font-size: 16px;
    font-weight: 600;
    margin-bottom: 0;
    margin-left: 46%;
    //margin-top: 7%;
    padding: 10px 25px;
    text-transform: capitalize;
  }
  .body-sign p
  {
    color: #8f8f8f !important;
  }
  </style>
 

<?php
  $cat_two_status   = $admindetails[0]->cat_two_status; 
  $cat_three_status = $admindetails[0]->cat_three_status;
  $cat_four_status  = $admindetails[0]->cat_four_status;
  $cat_five_status  = $admindetails[0]->cat_five_status; 

  $ref_settings       = $this->db->query("select * from referral_settings where ref_id=4")->row();
  $bellow_btn_content = $ref_settings->content_bellow_btn;
  $above_btn_content  = $ref_settings->content_above_btn;
?>
<div class="wrap-top">
  <div id="content">
    <div class="container">
      <section class="body-sign" style="height:50vh;">
        <br><br>
        <center>
          <?php echo $above_btn_content;?>
        </center>
        <br>
        <div class="newdivclass">
          <a style="color:#eee !important;" class="btn btn-resc1" data-toggle="modal" href="#register">Sign-Up</a>
        </div>
        <br>
         <center>
         <?php echo $bellow_btn_content;?>
        </center>
      </section>
      <input type="hidden" name="category_type" value="2345">
    </div>
  </div>
</div>

<!-- footer -->

<?php  
$this->load->view('front/site_intro');
$this->load->view('front/sub_footer');
?>
