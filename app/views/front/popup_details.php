<style type="text/css">
  .cls-for-popup .modal-dialog 
  {
    margin-left: 70% !important;
    /*top: 50% !important;*/
  }
.modal-dialog.in {
    bottom: 0;
    position: fixed;
    right: 15px;
    z-index:15;
}

@media screen and (max-width: 1270px) {
  
  .cls-for-popup .modal-dialog {
    margin-left: 70% !important;
    /*top: 41% !important;*/
}
} 
</style>
<div class="cls-for-popup">
<!-- New code for signle popup details 17-11-16-->
<?php
$user_id         = $this->session->userdata('user_id');
$userdetails     = $this->front_model->userdetails($user_id);
$popup_details   = $this->front_model->popupdetails();
$user_ip_address = $this->front_model->getUserIP();

$category_type  = $userdetails->referral_category_type;
$bonus_status   = $userdetails->bonus_benefit;
$login_type     = $userdetails->app_login;
$session_counts = $userdetails->popup_ses_count;

/*New code for popup session count details 20-11-16*/
$popup_ses_count  = $this->session->userdata('ses_popup_count');
$notlog_ses_count = $this->session->userdata('osiz_notlog_count');

/*End 20-11-16*/
if($user_id == '')
{
  $ses_count_details  = $this->db->query("SELECT * from notlogusers_ipaddress where ip_address='$user_ip_address'")->row('session_count');
  
  if($popup_details[0]->popup_type == 'not_log_user')
  {
    $not_log_popup_type       = $popup_details[0]->popup_type;
    $not_log_popup_status     = $popup_details[0]->popup_status;
    $not_log_session_count    = $popup_details[0]->session_count;
    $not_log_popup_content    = $popup_details[0]->popup_content;
  }  
} 
else
{
  
  $getadmindetails = $this->front_model->getadmindetails();  
  $popup_status    = $getadmindetails[0]->popup_log_users_status;

  if($popup_status == 1)
  {
    $popup_type = 'log_standard';
  }
  if($popup_status == 2)
  {
    if(($bonus_status == 0) && ($login_type == 0))
    {
      $popup_type = 'log_notusebonus_notuseapp'; 
    }
    if(($bonus_status == 1) && ($login_type == 0))
    {
      $popup_type = 'log_usebonus_notuseapp'; 
    }
    if(($bonus_status == 0) && ($login_type == 1))
    {
      $popup_type = 'log_notusebonus_logapp';
    }
    if(($bonus_status == 1) && ($login_type == 1))
    {
      $popup_type = 'log_usebonus_logapp';
    }
  }
}

$urldetails = $this->uri->segment(1);
if($user_id == '')
{
  $popupdetailss = $this->db->query("SELECT * from popup_details where popup_type='not_log_user'")->row(); 
  if(($popupdetailss->dont_show_status == 0) || ($popupdetailss->dont_show_status == 1 && !empty($urldetails)))
  {
      if($notlog_ses_count == 0)
      {
        if($not_log_popup_status != 0)
        {
          if($not_log_session_count > $ses_count_details)
          {
            //echo $not_log_popup_content;
            //echo $this->front_model->shortcut_details($not_log_popup_content);
            ?>
            <div class="modal-dialog" id="not_log_popup" style="width:360px !important;margin-bottom: 17px !important;"></div>
            <style type="text/css">
            .modal-open {
              overflow-y: scroll;
              padding:0px !important;
            }
            </style>
            <script type="text/javascript">
              $('#not_log_popup').modal('show');
              $('.modal-backdrop').css('display','none');
            </script>
            <?php
          }  
        }
      }
  }
  else
  {
  }
     
}
else
{
  $popupdetails = $this->db->query("SELECT * from popup_details where popup_type='$popup_type'")->row();
  if(($popupdetails->dont_show_status == 0) || ($popupdetails->dont_show_status == 1 && !empty($urldetails)))
  { 
    if($popupdetails->popup_status!= 0)
    {
      if($popupdetails->session_count != 0)
      {
        if($popupdetails->session_count > $session_counts)
        {  
          if($popup_ses_count == 0)
          { 
            //echo $popupdetails->popup_content;
            echo $this->front_model->shortcut_details($popupdetails->popup_content);
            ?>
            <style type="text/css">
            .modal-open {
              overflow-y: scroll;
              padding:0px !important;
            }
            </style>
            <script type="text/javascript">
              $('#log_popup').modal('show');
              $('.modal-backdrop').css('display','none');
            </script>
            <?php
          }  
        }  
      }
    }
  }    
}
?>
</div>
<!-- End 17-11-16 -->
<!-- New code for popup close session details 3-10-17-->
<script type="text/javascript">
$(document).on('click','.update_count',function()
{$.ajax({type:"POST",url:"<?php echo base_url();?>cashback/update_count_details",data:{},cache:!1,success:function(result)
{return!0}})});
$(document).on('click','.log_update_count',function()
{ $.ajax({type:"POST",url:"<?php echo base_url();?>cashback/log_update_count_details",data:{},cache:!1,success:function(result)
{return!0}})});
</script>
<!-- End 3-10-17 -->
