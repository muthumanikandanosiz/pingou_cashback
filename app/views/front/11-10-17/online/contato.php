<?php $this->load->view('front/header'); ?>
<!-- header content End -->

<?php
$user_id 	 = $this->session->userdata('user_id'); 
$userdetails = $this->front_model->userdetails($user_id);
$firstname 	 = $userdetails->first_name;
$lastname 	 = $userdetails->last_name;
$user_email  = $userdetails->email;

if($firstname == '' && $lastname == '')
{
	$username  = explode('@',$user_email);
	$user_name = $username[0];
}
else
{
	$user_name  = $firstname." ".$lastname;
}


$admin 	 = $admindetails[0];
/*  
$s        = str_replace('<br>',',',$admin->address);
$prepAddr = str_replace(' ','+',$s);                       
$geocode  = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($prepAddr).'&sensor=false');                        
$geo 	  = json_decode($geocode, true);  

if($geo['status'] = 'OK')
{
	@$lat  = $geo['results'][0]['geometry']['location']['lat'];
	@$long = $geo['results'][0]['geometry']['location']['lng'];
}*/
?>
<!-- stores
    ================================================== -->
<section class="cms wow fadeInDown">
  	<div class="container">
    	<div class="heading wow bounceIn">
      		<h2> Contact <span>Us</span></h2>
      		<div class="heading_border_cms">
      			<span>
      				<img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/top_drop.png">
      			</span>
      		</div>
    	</div>
    </div>
    <div class="">

    	<!-- ******Old Map Section****** --> 
        <!-- <div class="map-section section">
            <div class="container text-center">
                <h4>How to find us</h4>
                <p class="intro">Pellentesque efficitur lobortis massa eu sodales. Morbi eu ipsum euismod felis commodo porttitor quis sit amet leo. Sed lacinia eu ipsum sit amet tempus.</p>
                <div class="gmap-wrapper">
                    <div class="gmap" id="map"></div> 
                </div> 
            </div> 
        </div>-->

    	<div class="wow fadeInDown">
     		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3929.0337234024278!2d-51.12123738591039!3d-10.014073110141902!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1466406974786" width="100%" height="475" frameborder="0" style="border:0" allowfullscreen></iframe>
     	</div>
     	<div class="wow fadeInDown">
	      	<div class="contact container">
			  	<div class="row">
			 		<div class="col-md-4">
			  			<h2>Contact info</h2>
			  			<?php
						$s 		  = str_replace('<br>',',',$admin->address);
						$prepAddr = str_replace(' ','+',$s);                       
                       	$geocode  = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($prepAddr).'&sensor=false');                        
                       	$geo      = json_decode($geocode, true);  
					   
                        if ($geo['status'] = 'OK')
                        {
                        	@$lat  = $geo['results'][0]['geometry']['location']['lat'];
                        	@$long = $geo['results'][0]['geometry']['location']['lng'];
                        }
					    ?>
  						<table class="table">
  							<tr>
  								<th width="24%">Address :</th>
  								<td width="76%"><?php echo $admin->address;?> 
								<!-- <br> 101, sala 41. 
							    <br>
							    Bela Vista – São Paulo – SP – Brasil. <br>
								Cep: 01332-000 -->
								</td>
  							</tr>
  							<tr>
							  	<th>Phone :</th>
							    <td><?php echo $admin->contact_number;?></td>
							</tr>
						  	<tr>
							    <th>Email :</th>
							    <td><a href="mailto:<?php echo $admin->admin_email;?>"><?php echo $admin->admin_email;?></a></td>
						  	</tr>
  						</table>
  					</div>
   					<div class="col-md-8">
					   <h2>Contact info</h2>
					   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
						aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>

						<?php
						//$success = $this->session->flashdata('success');
						$success = $this->session->userdata('contact_success');
						if($success!="")
						{
							echo '<div class="alert alert-success">
							<button data-dismiss="alert" class="close">x</button>
							<strong>Success! </strong>'.$success.'</div>';			
						}
						$this->session->unset_userdata('contact_success'); 
						$attribute = array('role'=>'form','method'=>'post','id'=>'contact-form','class'=>'contact-form','novalidate'=>'novalidate','onSubmit'=>'return fillall();'); 
						echo form_open('contact-form',$attribute);
						?>
  							<?php 
  							if($user_id == '')
  							{
  								?>
  								<div class="form-group">
								  	<div class="row">
									  	<div class="col-md-6">
										    <label for="exampleInputPassword1">Your Name</label>
										    <input type="text" required="" minlength="2" class="form-control contact-tbox" name="name" id="name" aria-required="true" onClick="clears(2);">
									    </div>
									    <div class="col-md-6">
										    <label for="exampleInputPassword1">Your Email Address</label>
										    <input type="email" required="" class="form-control contact-tbox" name="email" id="email" aria-required="true" onClick="clears(3);">
									    </div>
								    </div>
								</div>
  								<?php 
  							}
  							else
  							{
  								?>
  								<input type="hidden" class="form-control" name="name"  id="name"  value="<?php echo $user_name; ?>">
								<input type="hidden" class="form-control" name="email" id="email" value="<?php echo $user_email; ?>">
  								<?php 
  							}
  							?> 
							<div class="form-group">
							  	<div class="row">
							    	<div class="col-md-12">
							    		<label for="exampleInputPassword1">Enter Your Message</label>
							   			<textarea required="" class="form-control contact-tarea" rows="7" name="message" id="message" aria-required="true" onClick="clears(4);"></textarea>
							    	</div>
							  	</div>
							</div>
						 	<center>
						 		<button class="btn btn-blu mar-bot-50 btn-contact" type="submit">SEND MESSAGE</button>
						  	</center>
						  	<div id="form-messages"></div>
						</form>
   					</div>
  				</div>
  			</div>
      	</div>     
    </div>
</section>
<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>
<!-- Footer Menu End-->
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/jquery.1.11.1.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script> 
<script type="text/javascript" src="<?php echo $this->front_model->get_css_js_url();?>front/js/map/gmaps.js"></script> 

<script>
var map;


    map = new GMaps({
        div: '#map',
        lat: <?php echo $lat;?>,
        lng: <?php echo $long;?>,
    });
    map.addMarker({
        lat: <?php echo $lat;?>,
        lng: <?php echo $long;?>,
        title: 'Address',      
        infoWindow: {
            content: 'India'
        }
        
    });

</script>

<script type="text/javascript">

function clears(val)
{
	if(val==1)
		$('#invite_mail').css('border', '');
	if(val==2)
		$('#name').css('border', '');
	if(val==3)
		$('#email').css('border', '');
	if(val==4)
		$('#message').css('border', '');		
	else
		$('#news_email').css('border', '');
} 

//Form validation

function fillall()
{
	var name = $('#name').val();
	var email = $('#email').val();
	var msg = $('#message').val();
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,3})?$/;
	if(name=='')
	{	
		$('#name').css("border","1px solid red");
		return false;
	}	
	if(email=='' || !emailReg.test(email))
	{	
		$('#email').css("border","1px solid red");
		return false;
	}	
	if(msg=='')
	{	
		$('#message').css('border', '1px solid red');
		return false;
	}	
}

</script>
 

