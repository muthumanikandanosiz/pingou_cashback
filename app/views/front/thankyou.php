<?php $this->load->view('front/header'); ?>
<section class="cms wow fadeInDown">
    <div class="container">
        <div class="heading wow bounceIn">
            <h2> Un <span>subscribe</span></h2>
            <div class="heading_border_cms">
                <span>
                    <img src="<?php echo $this->front_model->get_img_url(); ?>/front/new/images/top_drop.png">
                </span>
            </div>
        </div>
    </div>
    <div class="wrap-top">
        <div id="content">
            <div class="container">
                <section class="mid-sec">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="privacy">
                                    <center>
                                        <span>
                            		      You have unsubscribed successfully. To subscribe again or for more email settings, access <a href="<?php echo base_url();?>minha-conta"> MY ACCOUNT</a>  page
                                        </span>  
                                        <!-- <img src="http://www.ambajicreation.com/images/thanks.png"> -->
                                    </center>           
                                </div>        
                        </div>
                    </div>   
                </section>
            </div>
        </div>
    </div>
</section>
<?php
$this->load->view('front/site_intro');
$this->load->view('front/sub_footer');
?> 