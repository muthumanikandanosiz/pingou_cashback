<?php
	$this->load->dbutil();
	$xml  = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
	$xml .='<url>';
	$xml .='<loc>'.base_url().'</loc>';
	$xml .='<priority>.1.0.</priority>';
	$xml .='<changefreq>daily</changefreq>';
	$xml .='</url>'; 

	//Store page details 
	foreach($stores as $storename)
	{
		$xml .='<url>';
        $xml .='<loc>'.base_url().'cupom-desconto/'.$storename->affiliate_url.'</loc>';
        $xml .='<priority>1.0</priority>';
        $xml .='<changefreq>daily</changefreq>';
    	$xml .='</url>';
	}

    foreach($data_url as $url) 
    { 
  		$xml .='<url>';
        $xml .='<loc>'.base_url().$url.'</loc>';
        $xml .='<priority>0.9</priority>';
        $xml .='<changefreq>daily</changefreq>';
    	$xml .='</url>';
    } 

    //Categories page details
    foreach ($categories as $newcategories) 
    {
    	$xml .='<url>';
        $xml .='<loc>'.base_url().'ofertas/'.$newcategories->category_url.'</loc>';
        $xml .='<priority>0.8</priority>';
        $xml .='<changefreq>daily</changefreq>';
    	$xml .='</url>';
    }
    
    $xml .='<url>';
	$xml .='<loc>'.base_url().'contato</loc>';
	$xml .='<priority>0.7</priority>';
	$xml .='<changefreq>daily</changefreq>';
	$xml .='</url>';

    $xml .='<url>';
	$xml .='<loc>'.base_url().'howit-works</loc>';
	$xml .='<priority>0.7</priority>';
	$xml .='<changefreq>daily</changefreq>';
	$xml .='</url>';
	
	$xml .='<url>';
	$xml .='<loc>'.base_url().'perguntas-frequentes</loc>';
    $xml .='<priority>0.7</priority>';
	$xml .='<changefreq>daily</changefreq>';
	$xml .='</url>';

	//Cms page details
    foreach ($cms_url as $cmstitle)
    {
    	$xml .='<url>';
        $xml .='<loc>'.base_url().$cmstitle.'</loc>';
        $xml .='<priority>0.5</priority>';
        $xml .='<changefreq>daily</changefreq>';
    	$xml .='</url>';
    }


	$xml .='</urlset>';

	$sxe = new SimpleXMLElement($xml);
	//echo "<pre>";print_r($sxe); exit;
	$sxe->asXML("sitemap.xml");
?>