<!-- header -->
  <?php $this->load->view('front/header');?>
<!-- Header ends here -->
<style>
.error
{
	color:#ff0000;
}
.required_field
{
 color:#ff0000;
}
</style>

<div class="wrap-top">

<div id="content">

 

<div class="container">

<section class="body-sign">
			<div class="center-sign">
				
				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Reset Password</h2>
					</div>
					
					<div class="panel-body">
					<?php
					
					 //begin form
						$attribute = array('role'=>'form','name'=>'reset_password','id'=>'reset_password','method'=>'post');
						echo form_open('reset_password/',$attribute);
						
					?>
						<div class="form-group mb-lg">
													
								<label>New Password <span class="required_field">*</span></label>
								<div class="input-group input-group-icon">
									<input type="password" class="form-control input-lg" name="new_password"  id="new_password"autocomplete="off" >
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>
							<!--<input type="hidden" name="user_id" id="user_id" value="">
							<input type="hidden" name="email_id" id="email_id" value="">-->
							
							<div class="form-group mb-lg">
								<div class="clearfix">
									<label class="pull-left"> Confirm Password <span class="required_field">*</span></label>
								</div>
								<div class="input-group input-group-icon">
									<input type="password" class="form-control input-lg" name="confirm_password"  id="confirm_password" autocomplete="off" >
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="row">
								
								<div class="col-sm-8">
									<div class="checkbox-custom checkbox-default">
										<!--<input type="checkbox" name="rememberme" id="RememberMe">
										<label for="RememberMe">Remember Me</label>-->
									</div>
								</div>
								<div class="col-sm-4 text-right">
									<!--<button class="btn btn-danger hidden-xs pop" type="submit" name="signin">Sign In</button>-->
									<center><input type="submit"  class="btn btn-danger hidden-xs pop" name="Save" id="Save" value="Save"></center>
									<!--<button class="btn btn-block btn-lg visible-xs mt-lg btn-blue pop" type="submit" name="signin" >Sign In</button>-->
								</div>
							</div>

							<!--<span class="mt-lg mb-lg line-thru text-center text-uppercase">
								<span>or</span>
							</span>

							<p class="text-center">Don't have an account yet? 
							<?php
								 $attributes = array('class'=>'btn btn-blue btn-sm pop');
								echo anchor('register','Sign Up!',$attributes);?>
							</p>-->
						<?php
						
						//end form
						echo form_close();
						?>
					</div>
				</div>

			</div>
		</section>

</div>

</div>

</div>
<!-- footer -->
<?php $this->load->view('front/site_intro');?>
<?php $admindetails = $this->front_model->getadmindetails_main();?>
<footer>
  <!-- ========================== -->
  <!-- FOOTER - FOOTER -->
  <!-- ========================== -->
  <section class="footer-section innerfoot-sect">
    <div class="container">
      <div class="clearfix wow fadeInDown">
      <div class="wow fadeInDown">
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Help And Support</h5>
          <ul class="footer-nav">
            <li><a href="<?php echo base_url();?>contato"><i class="fa fa-caret-right"></i> Contact Us</a></li>
            <li><a href="<?php echo base_url();?>perguntas-frequentes"><i class="fa fa-caret-right"></i> Perguntas Frequentes</a></li>
            <?php 
            $result = $this->front_model->sub_menu();
            foreach($result as $view)
            {
            ?>
              <li><a href="<?php echo base_url(); echo $view->cms_title; ?>"><i class="fa fa-caret-right"></i> <?php echo $view->cms_heading;?></a></li>
            <?php 
            }
            ?>
          </ul>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Email Newsletter</h5>
          <ul class="footer-nav">
            <li><a href="#"><i class="fa fa-caret-right"></i> Subscribe to recieve inspiration, ideas and news in your inbox.</a></li>
            <li>
              <div class="col-md-12 col-sm-12 col-xs-12 pd-left-0">
              <input type="text" id="news_email" onkeypress="clears(2);" class="form-control tbox1" placeholder="Email Address">
              <input type="button" class="tbtn1" value="send" onClick="email_news();">
              </div>
              &nbsp;
              <span id="new_msg" style="color:red"></span>
            </li>
          </ul>
        </div>


        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Top Categories</h5>
          <ul class="footer-nav">
            <?php 
            $categories = $this->front_model->get_all_categories(8);
            foreach($categories as $view)
            {
            ?> 
            <li><a href="<?php echo base_url();?>products/<?php echo $view->category_url;?>"><i class="fa fa-caret-right"></i> <?php echo $view->category_name;?></a></li>
            <?php
            }
            ?>
          </ul>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Top Stores</h5>
          <ul class="footer-nav">
          <?php 
          $stores = $this->front_model->get_all_stores(8);
          foreach($stores as $view)
          {
          ?>
            <li><a href="<?php echo base_url(); ?>cupom-desconto/<?php echo $view->affiliate_url?>"><i class="fa fa-caret-right"></i> <?php echo $view->affiliate_name; ?></a></li>
          <?php
          }
          ?>
          </ul>
        </div>
      </div>
      </div>
    </div>
  </section>
  <section class="copyright-section">
    <div class="container">
     <div class="row wow fadeInDown">
      <div class="col-md-6 col-sm-6 col-xs-12 wow flipInX">
        <p>Copyright &copy; <?php echo date('Y')?> <?php echo $admindetails->site_name;?>  |  All Rights Reserved </p>
      </div>
      <?php
      $listing = $this->db->query("select * from admin")->row();
      ?>
      <div class="col-md-6 wow flipInX">
        <ul class="list-socials pull-right">
          <?php if($listing->admin_twitter) {?> <li><a href="<?php echo $listing->admin_twitter;?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
          <?php if($listing->admin_fb)      {?> <li><a href="<?php echo $listing->admin_fb;?>">     <i class="fa fa-facebook"></i></a></li><?php } ?>
          <?php if($listing->admin_gplus)   {?> <li><a href="<?php echo $listing->admin_gplus;?>">  <i class="fa fa-google-plus"></i></a></li><?php } ?>
        </ul>
      </div>
     </div>
    </div>
  </section>
</footer>
 

 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo $this->front_model->get_css_js_url();?>front/js/bootstrap.min.js"></script>
 <!-- Slider -->
 
<!-- Scripts queries -->
<?php //$this->load->view('front/js_scripts');?>	  
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/jquery.min.js"></script>
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/jquery.validate.min.js"></script>
<script type="text/javascript">
/* form validation*/
 $(document).ready(function() {
         $("#reset_password").validate({
	          rules: {
				new_password: {
                    required: true,
					minlength: 6
                },
				confirm_password: {
                    required: true,
					minlength: 6,
					equalTo:'#new_password'
                }
			},
            messages: {
				new_password: {
                    required: "Please enter the password.",
					minlength: "Passwords must be minimum 6 characters."    
                },
				confirm_password: {
                    required: "Please confirm your password.",
					minlength: "Passwords must be minimum 6 characters.",
					equalTo : "Please enter the same password."
                    
                }
			}
				
        });
});
</script>

<script type="text/javascript">
$(function () { $("[data-toggle='tooltip']").tooltip(); });
</script>

</body>
</html>
