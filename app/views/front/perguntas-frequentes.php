<?php 
$user_id = $this->session->userdata('user_id');

$meta_title = 'FAQ';
$meta_key = 'FAQ';
$meta_desc = 'FAQ';
//$content = $views->cms_content;
$heading = 'FAQ';
$cms_title = 'FAQ';

?>
<!-- Header content Start-->
<?php $admindetails = $this->front_model->getadmindetails_main();?>

<?php $this->load->view('front/header'); ?>
 <!-- header content End -->
<section class="cms wow fadeInDown">
  	<div class="container">
	    <div class="heading wow bounceIn">
	      <h2><span><?php echo $heading; ?></span></h2>
	      <div class="heading_border_cms"><span><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/top_drop.png"></span> </div>
	    </div>
	    <div class="about">
		    <div class="wow fadeInDown">
		      	<div id="accordion-faq" class="panel-group">	
		      		<?php
		      		$i=1;
					$faq_res = $this->db->query('SELECT * FROM `tbl_faq` where status=1')->result();
					foreach($faq_res as $result)
					{
						
						$string = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $result->faq_ans);
						?>
		       			<div class="panel panel-default">
				          	<div class="panel-heading">
					            <h4 class="panel-title">
					            	<a aria-expanded="false" class="collapsed" href="#collapseOne<?php echo $i;?>" data-parent="#accordion-faq" data-toggle="collapse">
					                	<?php echo $i;?> . <?php echo $result->faq_qn;?>  
					              	</a> 
					            </h4>
					        </div>
				          	<div aria-expanded="false" class="panel-collapse collapse" id="collapseOne<?php echo $i;?>" style="">
				            	<div class="panel-body">
					            	<p>
					                	<?php echo $string;?>
					              	</p>
					            </div>
					        </div>
	        			</div>
		     			<?php
						$i++;
					} 
		      		?>
		      	</div>
		    </div>
	    </div>
  	</div>
</section>

<?php $this->load->view('front/site_intro'); ?> 

<?php $this->load->view('front/sub_footer');?>

<script type="text/javascript">
	function clears(val)
{
	if(val==1)
		$('#email').css('border', '');
	else
		$('#news_email').css('border', '');
} 
 

 
function submit_user()
{	 
  var userids = document.getElementById('userids').value;
  var amount = document.getElementById('amount').value;
  
   
   if(amount!='')
  {
    $.ajax({
       type: "POST",
        url: '<?php echo base_url();?>cashback/onetime_benefit', 
        data: {userid: userids,amt:amount},
        dataType:"text", 
        success: 
          function(data)
          { 
            if(data==1)
            {

              $('#success').css("color","green");
              $('#success').text('Unic bônus added Successfully!');
              window.location.reload(true);
              location.href="<?php echo base_url();?>";

            }
            else
            {
              $('#success').css("color","red");
              $('#success').text('Unic bônus Not added Successfully');
            } 
          }
    });
  }
}
window.onpageshow = function(event) {
if (event.persisted) {
window.location.reload();
}
};
</script>
</body>
</html>
	