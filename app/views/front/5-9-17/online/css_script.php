<?php
$getadmindetailss = $this->front_model->getadmindetails(); 
$site_favicon = $getadmindetailss[0]->site_favicon;
?>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->front_model->get_img_url()."uploads/adminpro/".$site_favicon;?>">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet"> 

<link  type="text/css" rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/css/bootstrap.min.css">
<link  type="text/css" rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/css/carousel.css?name=<?php echo time();?>">
<link  type="text/css" rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/css/style.css?newname=<?php echo time();?>">
<link  type="text/css" rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/css/font-awesome.min.css" >
<link  type="text/css" rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/css/pre-pge.css">
<link  type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,600,500,700,800">
<link  type="text/css" rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/css/component.css" />
<link  type="text/css" rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/css/Selectstyle.css" />
<link  type="text/css" rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/css/bootstrap-datetimepicker.css">
<link  type="text/css" rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/css/dataTables.bootstrap.css">
<link  type="text/css" rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/css/icomoon_style.css">
<link  type="text/css" rel="stylesheet" href="<?php echo $this->front_model->get_css_js_url();?>front/css/jquerys-ui.css">
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/modernizr.custom.js"></script>


<!-- New code for onesignal popup styles 1-9-17 -->
<style type="text/css">
    #onesignal-popover-container.slide-down #onesignal-popover-dialog {
    width: 400px !important;
    }

    #onesignal-popover-container.slide-down {
    margin-right: 54% !important;
    top: 0 !important;
    }

    body #onesignal-popover-container #onesignal-popover-dialog .popover-button.secondary {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #222;
    border-radius: 4px;
    color: #222 !important;
    font-size: 13px;
    padding: 8px 15px;
    }
    
    body #onesignal-popover-container #onesignal-popover-dialog .popover-button.primary {
    border-radius: 4px;
    font-size: 13px;
    padding: 8px 15px;
    }
    
    body #onesignal-popover-container #onesignal-popover-dialog .popover-body-message {
    font-size: 14px;
    }

    body #onesignal-popover-container #onesignal-popover-dialog .popover-button.secondary, body #onesignal-popover-container #onesignal-popover-dialog .popover-button.primary {
    min-width: 126px;
    text-transform: initial;
    }
</style>
<!-- End 1-9-17 -->