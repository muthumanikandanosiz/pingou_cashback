<!-- header -->
  <?php $this->load->view('front/header');?>
<!-- Header ends here -->
<section class="cms wow fadeInDown">
  <div class="container">
      <div class="heading wow bounceIn">
          <h2> Un subscribe <span>Signup</span></h2>
          <div class="heading_border_cms">
            <span>
              <img src="<?php echo $this->front_model->get_img_url(); ?>/front/new/images/top_drop.png">
            </span>
          </div>
      </div>
  </div>
  <div class="wrap-top">
    <div id="content">
  	<?php
  		$complete = $this->session->flashdata('un_subscribe');
  		if($complete!="") {
  		echo '<div class="alert alert-success">
  		<button data-dismiss="alert" class="close">x</button>
  		<strong>'.$complete.'</strong></div>';
  	} ?>
     
      <div class="container">

        <section class="contact-section section section-on-bg">
          <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
          
          <?php 
  		if($verify_msg==1)
  		{
  			?>
  			<h3 class="heading text-center"> You have UnSubscribed from <?php echo $admindetails->site_name; ?> site </h3><br>
        <h4>if you want to resubscribe or You are here by mistake: </h4><br>
        <center><h4> Click the activation link in your mail to activate your account </h4> OR <br> <h4> Click Here to <a style="" href="#register" data-toggle="modal">REGISTER </a>Again to Activate your Account</h4></center>

  			<?php
  		}
  		?>
          </div> 
          </div> 
          </section>
      </div>
    </div>
  </div>
</section>  
<!-- footer -->

<?php 
$this->load->view('front/site_intro');
$this->load->view('front/sub_footer');
?>