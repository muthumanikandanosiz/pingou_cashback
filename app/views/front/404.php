<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title><?php $admindetails = $this->front_model->getadmindetails_main(); echo $admindetails->homepage_title;?> 404 - this page does not exist </title>
<style>

@import url(http://fonts.googleapis.com/css?family=Bree+Serif|Source+Sans+Pro:300,400);
*{
    maring: 0;
    padding: 0;
}
body{
    font-family: 'Source Sans Pro', sans-serif;
    background: #3f6eb3;
    color: #fff;
}
a:link{
    color: #1f3759;
    text-decoration: none;
}
a:active{
    color: #1f3759;
    text-decoration: none;
}
a:hover{
    color: #9fb7d9;
    text-decoration: none;
}
a:visited{
    color: #1f3759;
    text-decoration: none;
}

a.underline, .underline{
    text-decoration: none;
    font-weight: bold;
}

.bree-font{
    font-family: 'Bree Serif', serif;
}

#content{
    margin: 0 auto;
    width: 960px;
}

.clearfix:after {
    content: ".";
    display: block;
    clear: both;
    visibility: hidden;
    line-height: 0;
    height: 0;
}
 
.clearfix {
    display: block;
}

#logo {
    margin: 1em;
    float: left;
    display: bloack;
}
nav{
    float: right;
    display: block;
}
nav ul > li{
    list-style: none;
    float: left;
    margin: 0 2em;
    display: block;
}

#main-body{
    text-align: center;
}

.enormous-font{
    font-size: 10em;
    margin-bottom: 0em;
}
.big-font{
    font-size: 2em;
}
hr{
    width: 25%;
    height: 1px;
    background: #1f3759;
    border: 0px;
}
.navbar-wrapper, .navbar-inverse{width: 100%; margin-top: 0 !important;}
#main-body {
    color: #fff;
    margin: 18% 0 0;
    text-align: center;
}
.cls_maintainblk{margin-bottom: 4%;}
.cls_maintainblk img{width: 48%; height: 320px;}
</style>
<?php $this->load->view('front/css_script');?>
</head>
<body class="moving">
    <div class="navbar-wrapper index-headwrap wow fadeInDown">
      <div class="cbp-af-header">
        <div class="cbp-af-inner">
          <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
              <div class="tophead-resp-md">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
               <a href="javascript:;" id="selsearch1" class="select-style1 hidden-md hidden-sm"> </a>
                </div>

                <div class="row">
                  
                 
                      <a class="col-md-12 col-sm-12 col-xs-12 cls_padresno toplogo-md text-center" 
                      href="<?php echo base_url(); ?>">
                      <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/logo.png" class="center-block">
                      </a>
                   
                  
                </div>
              </div>  
            </div>
          </nav>
        </div>
      </div>
    </div>
    <div id="content">        
        <div id="main-body">
            <p class="enormous-font bree-font"> 404 </p>
            <p class="big-font"> Ops! Deu ruim, essa página não existe. </p>
            <hr>
            <p class="big-font"> Vamos voltar para a home <a href="<?php echo base_url();?>" class="underline">Home page</a> e seguir navegando?</p>
        </div>
    </div>
</body>
</html>
