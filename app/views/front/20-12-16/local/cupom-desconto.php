<?php $this->load->view('front/storeheader'); ?>
<style type="text/css">
  .storeblkmain{background: none;}
</style>
<?php 
$pagename          = $this->uri->segment(1);
if($this->input->get('ref'))
{
  $random_ref=$this->input->get('ref');
  $this->session->set_userdata('ses_random_ref',$random_ref);
}
$getadmindetails   = $this->front_model->getadmindetails();
$user_id           = $this->session->userdata('user_id');
$ratingRow         = $this->db->query("SELECT rating_number, FORMAT((total_points / rating_number),1) as average_rating FROM post_rating WHERE post_id = '$store_details->affiliate_id' AND status = 1")->row();
$store_names       = $store_details->affiliate_url;
$store_namesnew    = $store_details->affiliate_url;
$newcashback_ex_id = $this->session->userdata('cash_ex_id');
$cashback_details  = $this->session->userdata('link_name');
$expiry_dates      = $this->session->userdata('expiry_dates');
$cashback_web      = $this->session->userdata('cashback_web');
$affiliate_urls    = $this->session->userdata('affiliate_urls');
$expiry_dates      = strtotime($expiry_dates); 
$tday_dates        = strtotime(date('Y-m-d'));
$cash_query        = $this->db->query("SELECT * from cashback_exclusive where id='$newcashback_ex_id'")->row();

if($getadmindetails[0]->storecover_type =='coverimage')
{
  if($getadmindetails[0]->store_back_img_settings =='zoomimage')
  {
    ?>
    <section class="wow fadeInDown storeblkmain" style="background: rgba(0, 0, 0, 0) url(<?php echo $this->front_model->get_img_url();?>uploads/adminpro/<?php echo $getadmindetails[0]->storecover_image;?>) repeat-y scroll left center / cover;">
    <?php
  }
  else
  {
    ?>
    <section class="wow fadeInDown storeblkmain" style="background: rgba(0, 0, 0, 0) url(<?php echo $this->front_model->get_img_url();?>uploads/adminpro/<?php echo $getadmindetails[0]->storecover_image;?>);">
    <?php 
  }
}
if($getadmindetails[0]->storecover_type =='covercolor')
{
  ?>
  <section class="wow fadeInDown storeblkmain" style="background-color:<?php echo $getadmindetails[0]->storecover_color; ?> !important;">
  <?php
}
?>
  <div class="container">
    <div class="store-normalblk store-respblk">
      <div class="col-md-12 col-sm-12 col-xs-12 turn-shopblk clearfix">
        <div class="col-sm-3 col-xs-12 pd-left-0 pd-right-0">
          <?php 
          if($user_id=="")
          {
            ?>  
            <img src="<?php echo $this->front_model->get_img_url();?>uploads/affiliates/<?php echo $store_details->cover_photo;?>" alt="cupom-desconto-<?php echo $store_details->affiliate_name;?>" class="img-responsive shopdetblk1-logo"> 
            <?php
            $anew = '<a href="#myModal" data-toggle="modal" class="btn-loja-pos">';
          }
          else
          {
            ?>
            <img src="<?php echo $this->front_model->get_img_url();?>uploads/affiliates/<?php echo $store_details->cover_photo;?>" alt="cupom-desconto-<?php echo $store_details->affiliate_name;?>" class="img-responsive shopdetblk1-logo"> 
            <?php
            $anew = '<a href="'.$store_details->logo_url.'" target="_blank" class="btn-loja-pos">';
          }   
          ?>
        </div>
        <div class="col-md-4 col-sm-5 col-xs-12 pd-left-0 pd-right-0">
          <h4>
            <?php 
            if($store_details->cashback_percentage!='')
            {
              if($store_details->affiliate_cashback_type=="Percentage")
              {
                if($store_names == $affiliate_urls)
                { 
                  if($tday_dates <= $expiry_dates)
                  {
                    $cppercentage = $cashback_web."%";
                  }
                  else
                  {
                    $cppercentage = $store_details->cashback_percentage."%";
                  } 
                }
                else
                {
                  $cppercentage = $store_details->cashback_percentage."%";
                }
              }
              else
              {
                $cppercentage = "R$ ".$store_details->cashback_percentage;
              }
              ?> Ganhe <?php echo str_replace('.', ',', $cppercentage); ?> de volta <?php 
            }
            if($store_details->old_cashback!='')
            {
              ?>
              <span>(<?= $store_details->old_cashback;?>)</span>
              <?php
            }
            ?>
          </h4>
        </div>
        <?php
        $affid =  $store_details->affiliate_id; 
        ?>
        <div class="col-sm-4 col-xs-12">
          <?php 
          if($coupon_id == '')
          {
            if($user_id == '')
            {
              ?>
              <button data-toggle="modal" href="#login" class="turn-btn"> Ativar e ir para <?php echo $store_details->affiliate_name;?> </button>
              <?php 
            }
            else
            { 
              ?>
                <button class="turn-btn" onclick="window.open('<?php echo base_url().'ir-loja/'.$affid;?>');">Ativar e ir para <?php echo $store_details->affiliate_name;?></button>
                <?php
              }
          }
          else
          { 
            if($user_id == '')
            {
              ?>
              <button data-toggle="modal" href="#login" class="turn-btn"> Ativar e ir para <?php echo $store_details->affiliate_name;?> </button>
              <?php 
            }
            else
            { 
              ?>
              <button class="turn-btn" onclick="window.open('<?php echo base_url().'ir-loja/'.$affid;?>');">Ativar e ir para <?php echo $store_details->affiliate_name;?></button>
              <?php
            }
          }
          ?>
        </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12 cls-shopdetblk1">
        <div class="col-md-offset-3 col-sm-offset-0 col-xs-offset-0 col-md-9 col-sm-12 col-xs-12 pd-left-0 pd-right-0 cls-substorehead">
          <?php
          if($store_details->notify_desk<>'')
          {
            if($store_names == $affiliate_urls)
            {
              if($expiry_dates < $tday_dates)
              {
                ?>
                <div class="hidden-xs">
                <?php echo $cash_query->expiry_notify; ?>
                </div>
                <?php
              }
              else
              { 
                ?>
                <div class="hidden-xs"><?= $store_details->notify_desk; ?></div>
                <?php
              } 
            }
            else
            { 
              ?>
              <div class="hidden-xs">
              <?= $store_details->notify_desk; ?>
              </div>
              <?php
            } 
          }
          if($store_details->notify_mobile<>'')
          {
            ?>
            <div class="hidden-md hidden-sm">
              <?= $store_details->notify_mobile; ?>
            </div>
            <?php
          }
          if($store_details->notify_desk == '' ||  $store_details->notify_mobile == '')
          {
            ?>
            <style type="text/css">
            @media screen and (min-width: 992px){.turn-shopblk .shopdetblk1-logo{bottom:-55px;}}                
            </style>
            <?php 
          }
          ?>
        </div>          
        <div class="col-md-offset-3 col-sm-offset-0 col-xs-offset-0 col-md-2 col-sm-3 col-xs-12 pd-left-0 pd-right-0">
          <div class="cls-substorehead">
            <p>
              <?php 
              if($store_details->cashback_percentage!='')
              {
                ?>
                <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal_terms" style="color:#333 !important;">Regras e exceções</a> 
                <?php
              }
              ?>
            </p>
            <p class="hidden-xs">Tracking Speed : <?php echo $store_details->report_date;?> days</p>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12 pd-left-0 pd-right-0">
          <div class="cls-substorehead">
            <p>
              <a style="color:#333 !important;" href="javascript:void(0)" data-toggle="modal" data-target="#myModal_how_to_get">  Como funciona o dinheiro de volta? </a>
            </p>
            <p class="hidden-xs">Estimated Payment: <?php echo $store_details->retailer_ban_url;?> weeks</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-5 col-xs-12 share-blk pd-left-0 pd-right-0">
          <div class="cls-substorehead">
            <p>Share and Earn :</p>
            <ul class="list-inline list-unstyled">
              <li class="fb"><a href="#" class="hvr-pulse-shrink"> <i class="fa fa-facebook-square"></i> Share </a></li>
              <li class="gplus"><a href="#" class="hvr-pulse-shrink"> <span class="icon-google-plus"></span>Compartilhar</a></li>
              <li class="twit"><a href="#" class="hvr-pulse-shrink"> <i class="fa fa-twitter"></i> Tweet </a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
$affiliate_desc     = str_ireplace('<p>','',$store_details->affiliate_desc); 
$new_affiliate_desc = str_ireplace('</p>','',$affiliate_desc);
?>
<section class="wow fadeInDown storeblkmaincont">
    <div class="container">
      <?php
      if($user_id !='')
      {
        ?>
        <div class="col-md-9 col-sm-12 col-xs-12 fn center-block">
        <?php
      }
      else
      {
        ?>
        <div class="col-md-9 col-sm-12 col-xs-12">
        <?php
      }
      ?>
        <div class="comm-store-contblk">
          <h2>Cupom de Desconto <?php echo $store_details->affiliate_name;?></h2>
          <?php
          $userdetails             = $this->front_model->userdetails($user_id);
          $store_banner_details    = $this->db->query("SELECT * from `sales_banner_status` where sales_banner_id =1")->row();
          $store_name              = $store_banner_details->store;
          $store_top_status        = $store_banner_details->store_top_status;
          $store_bot_status        = $store_banner_details->store_bottom_status; 
          $category_type           = $userdetails->referral_category_type;
          $bonus_status            = $userdetails->bonus_benefit;
          $login_type              = $userdetails->app_login;
          $page_url                = '?utm_source=pingou&utm_medium=top-banner-'.$pagename.'-page&utm_campaign=';

          if($user_id == '')
          {
            if($store_top_status == 1)
            {
              $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
              $imagename     = explode('.',$bannerdetails[0]->image_name);
              $image_name    = $imagename[0];
              ?>
              <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
              </a>
              <?php 
            }
            if($store_top_status == 2)
            {
              $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
              $html_content  = $this->db->query("SELECT `not_log_html_settings` from `sales_funnel` where not_log_status !=0")->result();
              ?>
              <?php
              echo $html_content[0]->not_log_html_settings; 
            }
            if($store_top_status == 3)
            {
              $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0 order by RAND() limit 1")->result();
              $imagename     = explode('.',$bannerdetails[0]->image_name);
              $image_name    = $imagename[0];
              ?>
              <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
              </a>
              <?php 
            }
            if($store_top_status == 4)
            {
              $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0")->result();
              $html_content  = $this->db->query("SELECT `notlog_standard_html_settings` from `sales_funnel` where notlog_standard_status !=0")->result();
              ?>
              <?php
              echo $html_content[0]->notlog_standard_html_settings;
            }
          }
          else
          {
            if(($bonus_status == 0) && ($login_type == 0))
            {
              $conditiondetails = 'log_notusebonus_notuseapp';
              $html_name        = 'log_notusebonus_notuseapp_html_settings';
            }
            if(($bonus_status == 1) && ($login_type == 0))
            {
              $conditiondetails = 'log_usebonus_notuseapp';
              $html_name        = 'log_usebonus_notuseapp_html_settings';
            }
            if(($bonus_status == 0) && ($login_type == 1))
            {
              $conditiondetails = 'log_notusebonus_useapp';
              $html_name        = 'log_notusebonus_useapp_html_settings';
            }
            if(($bonus_status == 1) && ($login_type == 1))
            {
              $conditiondetails = 'log_usebonus_useapp';
              $html_name        = 'log_usebonus_useapp_html_settings';
            }

            if($store_top_status == 1)
            {
              $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='$conditiondetails' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
              $imagename     = explode('.',$bannerdetails[0]->image_name);
              $image_name    = $imagename[0];
              if($bannerdetails)
              {
                ?>
                <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                  <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
                </a>
                <?php 
              }  
            }
            if($store_top_status == 2)
            {
              $conditiondetails = $conditiondetails.'_status'; 
              $html_content     = $this->db->query("SELECT $html_name from `sales_funnel` where $conditiondetails!=0")->row($html_name);
              echo $html_content;
            }
            if($store_top_status == 3)
            {
              $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='log_standard' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
              $imagename     = explode('.',$bannerdetails[0]->image_name);
              $image_name    = $imagename[0];
              if($bannerdetails)
              {
                ?>
                <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                  <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
                </a>
                <?php     
              } 
            }
            if($store_top_status == 4)
            {              
              $html_content     = $this->db->query("SELECT `log_standard_html_settings` from `sales_funnel` where log_standard_status!=0")->row('log_standard_html_settings');
              echo $html_content;     
            } 
          }
          ?>
          <br>
          <?php 
          if($user_id =='')
          {
            ?>
            <div class='forum-content'>
              <div class='comments-space'><?php echo $new_affiliate_desc;?></div>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
      <div class="clearfix mar-bot-31">
        <?php
        if($user_id !='')
        {
          ?>
          <div class="col-md-9 col-sm-12 col-xs-12 fn center-block">
          <?php
        }
        else
        {
          ?>
          <div class="col-md-9 col-sm-12 col-xs-12"> 
          <?php
        }
        ?>
          <div class="comm-store-contblk">
              <?php
              $affid   =  $store_details->affiliate_id;
              $affname =  $store_details->affiliate_name;
              if($store_coupons!="")
              {
                ?>
                <div class="cls_store_lisr_on">
                  <?php
                  $kt=1;
                  $mi=1001;
                  foreach($store_coupons as $coupons)
                  {
                    $coupon_id    = $coupons->coupon_id;
                    $expiry_date  = $coupons->expiry_date;
                    $exp          = date('m/d/Y',strtotime($expiry_date));
                    $date         = DateTime::createFromFormat('m/d/Y', date('m/d/Y'));
                    $date1        = date_create($date->format('Y-m-d'));
                    $date         = DateTime::createFromFormat('m/d/Y', $exp);
                    $date2        = date_create($date->format('Y-m-d'));
                    $diff         = date_diff($date1,$date2);
                    $coupondate   =  $diff->format("%a dias");
                    $coupon_type  = $coupons->type;  
                    ?>
                    <div class="cls-storelistcontblk1">
                      <div class="cls-storelisthead1 clearfix">
                        <div class="col-md-11 col-sm-11 col-xs-10">
                          <?php
                          if($user_id=="")
                          { 
                            $hres      = '<a cuptype="'.$coupon_type.'" cupid="'.$coupon_id.'" href="#entrars" id="cid_'.$coupon_id.'" data-toggle="modal" class="newclr newnew1">';
                            $ss_button = '<a href="'.base_url().'cupom-desconto/'.$store_details->affiliate_name.'?oid='.$coupon_id.'" target="_blank;" data-id="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'" showcoupon_id="'.$coupon_id.'" class="btn btn-blue dsf">Ver cupom</a>';
                          }
                          else
                          {
                            if($coupon_type == 'Promotion')
                            {
                              $hres = '<a class="newclr" href="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'"  target="_blank">';
                            }
                            else
                            {
                              $hres =  '<a class="newclr" href="'.base_url().'cupom-desconto/'.$store_details->affiliate_name.'?oid='.$coupon_id.'" target="_blank" data-id="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'" show_id="'.$coupon_id.'">'; 
                            }
                            $ss_button = '<a href="'.base_url().'cupom-desconto/'.$store_details->affiliate_name.'?oid='.$coupon_id.'" target="_blank" data-id="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'" show_id="'.$coupon_id.'" class="btn btn-blue after_login"> Ver cupom LGN </a>';   
                          }
                          ?>
                          <h5>
                            <?php 
                              echo  $coupons->title;
                              if($store_details->cashback_percentage!="")
                              {
                                if($store_details->affiliate_cashback_type=="Percentage")
                                {
                                  if($store_names == $affiliate_urls)
                                  {
                                    if($tday_dates <= $expiry_dates)
                                    {
                                      $cppercentage = $cashback_web."%";
                                    } 
                                  }
                                  else
                                  {
                                    $cppercentage = $store_details->cashback_percentage."%";
                                  }
                                }
                                else
                                {
                                  $cppercentage = "R$. ".$store_details->cashback_percentage;
                                } 
                              }
                              if($cppercentage!="")
                              {
                                $admindetails = $this->front_model->getadmindetails_main();
                                echo " + ".$cppercentage." de volta " ;
                              }
                            ?>
                          </h5>
                          <?php $countchar = strlen($coupons->description);?>
                        </div>
                        <?php if(($coupons->coupon_options == 1) || ($coupons->coupon_options == 2))
                        {
                          ?>
                          <div class="col-md-1 col-sm-1 col-xs-2">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAeCAYAAAAo5+5WAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3BJREFUeNqMlnmIT1EUx+f3jLFkb2xDlrGF7GTfdzNmhuxM2SmKyBaKlCWUxlaWMn+IxNBkYiT7LmTJvmdJiMjO8Dn6vrq93pNbn7nzfu++7zvn3HPOfbH+OXfiIkYR6As9oQdUBw+ew3E4DPtzB9R9H/ZwLEJ4AsyDmvAGzsMLKICK0BKqwjtYAVm84Isr4AUEy0AubILHstYsTYWJMBkGQDK0hkOwHE6m7b1bK0q4PByQyCjoJnc/h3j0Ay7AMIXLXn4M8eQwYbOyDaTB9rj/HITgIFN3KA17EC/mCo+ADJhhGxKhMR5OQZ0Q8WuyvinM9zcvgfk2PIUuIYIW17HQQobYJh6TV/mI/vIXYm0202DbA1vYXru/LER0HGyEVo53SfJwJRQKrF8CFop0Tzn6Ec6GCI/WbBs1Fb7DFDgKC3XthuQ+k4VlYDx/esNV+BAi7FtUBTpa6JRiG/6xn6bV0VOqvIpYlOUID4XCcBFyFJ6wYXtVyoR/y5KwsRM6yPUPThFZkeRDjSizPb2hQsT9kvBIxTIdvsF6FUjZiCyqZkZ4enMTiQTjuw6smayC+vATboKfYt9DhC2X73oqYxPtGlhQTpVYAmbCLCgui4vqBfnuA+SxvbwR7PKUZg9hTkD4tYRnQ55iHNPazdAP3gaesaqzLrffk1sLoJ1K2h3vVAipundKnlmnexKwNoVpJKwmn1/51bTDGoj9aFUTsZFblctPgzcQbS6NS7A02N3GwAnYrVKO/U93Q7Q/0xGwkyTdb/iusJX1cPgEW+AyTIvI1VJgggd1MNh+dEb0ub8gPvBAW4legcWwRpgnt+RFkoqmjDbP1tWDZ/86mjIUCmuJDRXvLKXjJBiiYylHp0xtrFykAzbFFXItbgCV1MniVGW5IqYsyFZXC448tdJ9YRYPUzoVhDzYB65DM0gIuW/PJbKRSUHhwjrSt0Vs/iBZeg96hRxN1jvOQWZQ2PLzgdztIJca615llfdlhcXN89ZYOVz/m1EpXCe4whnKw7UwF746b09X87ZxWllRUdd2qkxDbI96hBnWyRVOVF84p+M/U/G03wcqU/xudlZ9wjxJJAxtlJKZ8rKFmxVjlAX+KJB1dgS9hBvOvd36/Er2uxviJ/U11Ng/jeKd1AoOS60zqkZ33FbjStOHSvD74u/4I8AAS4nzKRLPcIQAAAAASUVORK5CYII=" alt="currency" class="img-responsive center-block">
                          </div>
                          <?php 
                        }
                        ?>
                      </div>
                      <div class="cls-storelistcont1 clearfix">
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div id="forum-contents<?php echo $kt;?>" class='forum-contents'>
                            <div id="comments-spaces<?php echo $kt;?>" class='comments-spaces'>
                              <?php
                                $osiz_des_length = strlen($coupons->description); 
                                echo $substrings = substr($coupons->description, 0,70); 
                                if($osiz_des_length > 70) 
                                {
                                  echo "
                                  <span class='plus_click' style='float:right;font-size: 16px;position: absolute; cursor: pointer; right:10px; top:0px;'>
                                    <i class='fa fa-plus-square'></i>
                                    <i class='fa fa-minus-square' style='display:none;'></i>
                                  </span>";
                                }
                                ?>
                                <div class="full_desc" style="display:none;text-align:unset !important;"><?php echo substr($coupons->description, 70,$osiz_des_length);?></div>
                            </div>
                          </div>
                          <?php 
                          if($coupons->cashback_description!="")
                          {
                            ?>
                            <div class = "cls-store-grp">
                                <label> <span>Dica : </span><?php echo $coupons->cashback_description; ?></label>      
                            </div>
                            <?php
                          }
                          ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 pd-left-0 pd-right-0">
                          <p class="text-right mt15"> 
                            <?php 
                            if($user_id!="")
                            {
                              if($coupons->type == 'Promotion')
                              {?>
                                <a href="<?php echo base_url().'ir-loja/'.$affid.'/'.$coupon_id;?>" class="after_login"  target="_blank"><button class="btn-danger btn-coupon btn-couponactiv center-block"> Ativar desconto </button></a>
                                <?php
                              }
                              else
                              {
                              ?>  
                                <a class="after_login" href="<?php echo base_url();?>cupom-desconto/<?php echo $store_details->affiliate_url; ?>?oid=<?php echo $coupon_id;?>" target="_blank" data-id="<?php echo base_url();?>ir-loja/<?php echo $affid;?>/<?php echo $coupon_id;?>" show_id="<?php echo $coupon_id;?>">
                                <button class="btn-danger btn-coupon center-block btn1_<?php echo $coupon_id;?>"><small class="hvr-curl-bottom-right"> Ver cupom </small><span><?php echo $coupons->code;?></span></button>
                                </a>
                              <?php
                              }
                            }
                            else
                            {
                              if($coupons->type == 'Promotion')
                              {
                                ?>
                                <?php echo $hres;?><button class="btn-danger btn-coupon btn-couponactiv center-block">Ativar desconto</button></a>
                                <?php
                              }
                              else
                              {
                                echo $hres;?><button class="btn-danger btn-coupon center-block"><small class="hvr-curl-bottom-right">Usar cupom </small><span><?php echo $coupons->code;?></span> </button></a>
                                <?php
                              }
                            }
                            ?>
                          </p>
                          <br>                
                        </div>   
                      </div>
                    </div>
                    <?php
                    $kt++;
                    $mi++;
                  }
                  ?>  
                </div>
                <?php
              }
              
              if($user_id == '')
              {
                if($store_bot_status == 1)
                {
                  $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
                  $imagename     = explode('.',$bannerdetails[0]->image_name);
                  $image_name    = $imagename[0];
                  ?>
                  <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                    <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
                  </a>
                  <?php 
                }
                if($store_bot_status == 2)
                {
                  $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
                  $html_content  = $this->db->query("SELECT `not_log_html_settings` from `sales_funnel` where not_log_status !=0")->result();
                  ?>
                  <?php
                  echo $html_content[0]->not_log_html_settings; 
                }
                if($store_bot_status == 3)
                {
                  $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0 order by RAND() limit 1")->result();
                  $imagename     = explode('.',$bannerdetails[0]->image_name);
                  $image_name    = $imagename[0];
                  ?>
                  <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                    <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
                  </a>
                  <?php 
                }
                if($store_bot_status == 4)
                {
                  $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0")->result();
                  $html_content  = $this->db->query("SELECT `notlog_standard_html_settings` from `sales_funnel` where notlog_standard_status !=0")->result();
                  echo $html_content[0]->notlog_standard_html_settings;
                }
              }
              else
              {
                if(($bonus_status == 0) && ($login_type == 0))
                {
                  $conditiondetails = 'log_notusebonus_notuseapp';
                  $html_name        = 'log_notusebonus_notuseapp_html_settings';
                }
                if(($bonus_status == 1) && ($login_type == 0))
                {
                  $conditiondetails = 'log_usebonus_notuseapp';
                  $html_name        = 'log_usebonus_notuseapp_html_settings';
                }
                if(($bonus_status == 0) && ($login_type == 1))
                {
                  $conditiondetails = 'log_notusebonus_useapp';
                  $html_name        = 'log_notusebonus_useapp_html_settings';
                }
                if(($bonus_status == 1) && ($login_type == 1))
                {
                  $conditiondetails = 'log_usebonus_useapp';
                  $html_name        = 'log_usebonus_useapp_html_settings';
                }

                if($store_bot_status == 1)
                {
                  $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='$conditiondetails' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
                  $imagename     = explode('.',$bannerdetails[0]->image_name);
                  $image_name    = $imagename[0];
                  if($bannerdetails)
                  {
                    ?>
                    <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                      <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
                    </a>
                    <?php 
                  }  
                }
                if($store_bot_status == 2)
                {
                  $conditiondetails = $conditiondetails.'_status'; 
                  $html_content     = $this->db->query("SELECT $html_name from `sales_funnel` where $conditiondetails!=0")->row($html_name);
                  echo $html_content;  
                }
                if($store_bot_status == 3)
                {
                  $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='log_standard' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
                  $imagename     = explode('.',$bannerdetails[0]->image_name);
                  $image_name    = $imagename[0];
                  if($bannerdetails)
                  {
                    ?>
                    <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?><?php echo $page_url;?><?php echo $image_name;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
                      <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
                    </a>
                    <?php     
                  } 
                }
                if($store_bot_status == 4)
                {
                  $html_content     = $this->db->query("SELECT `log_standard_html_settings` from `sales_funnel` where log_standard_status!=0")->row('log_standard_html_settings');
                  echo $html_content;     
                }
              }
              ?>
              <br>
              <?php 
              if($user_id=='')
              { 
                if($expiry_coupons!="")
                {
                  ?> 
                  <h2>Unreliable Coupons</h2>      
                  <div class="cls_store_lisr_on">
                    <?php
                    $kt=1;
                    $mi=1001;
                    foreach($expiry_coupons as $coupons)
                    {

                      $coupon_id = $coupons->coupon_id;
                      $expiry_date = $coupons->expiry_date;
                      $exp = date('m/d/Y',strtotime($expiry_date));
                      $date = DateTime::createFromFormat('m/d/Y', date('m/d/Y'));
                      $date1 = date_create($date->format('Y-m-d'));
                      $date = DateTime::createFromFormat('m/d/Y', $exp);
                      $date2 = date_create($date->format('Y-m-d'));
                      $diff=date_diff($date1,$date2);
                      $coupondate =  $diff->format("%a dias");
                      ?>
                      <div class="cls-storelistcontblk1">
                        <div class="cls-storelisthead1 clearfix">
                          <div class="col-md-11 col-sm-11 col-xs-10">
                            <?php
                            if($user_id=="")
                            {
                              $hres =  '<a class="popup-text dsf newclr" href="'.base_url().'cupom-desconto/'.$store_details->affiliate_name.'?oid='.$coupon_id.'" target="_blank;" data-id="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'" showcoupon_id="'.$coupon_id.'">';
                              $ss_button = '<a href="'.base_url().'cupom-desconto/'.$store_details->affiliate_name.'?oid='.$coupon_id.'" target="_blank;" data-id="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'" showcoupon_id="'.$coupon_id.'" class="btn btn-blue dsf">Ver cupom</a>';
                            }
                            else
                            {
                              $hres =  '<a class="popup-text after_login newclr" href="'.base_url().'cupom-desconto/'.$store_details->affiliate_name.'?oid='.$coupon_id.'" target="_blank" data-id="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'" show_id="'.$coupon_id.'">';
                              $ss_button = '<a href="'.base_url().'cupom-desconto/'.$store_details->affiliate_name.'?oid='.$coupon_id.'" target="_blank" data-id="'.base_url().'ir-loja/'.$affid.'/'.$coupon_id.'" show_id="'.$coupon_id.'" onclick="return addRow();" class="btn btn-blue after_login"> Ver cupom LGN </a>';    
                            }
                            ?>
                            <h5>
                              <?php //echo $hres;?><?php echo $coupons->title?> 
                              <?php
                              if($store_details->cashback_percentage!="")
                              {
                                if($store_details->affiliate_cashback_type=="Percentage")
                                {
                                  if($store_names == $affiliate_urls)
                                  {
                                    if($tday_dates <= $expiry_dates)
                                    {
                                      $cppercentage = $cashback_web."%";
                                    } 
                                  }
                                  else
                                  {
                                    $cppercentage = $store_details->cashback_percentage."%";
                                  }
                                }
                                else
                                {
                                  $cppercentage = "Rs. ".$store_details->cashback_percentage;
                                } 
                              }
                              if($cppercentage!="")
                              {
                                $admindetails = $this->front_model->getadmindetails_main();
                              }
                              ?>
                            </h5>
                          </div>
                          <?php if(($coupons->coupon_options == 1) || ($coupons->coupon_options == 2))
                          {
                            ?>
                            <div class="col-md-1 col-sm-1 col-xs-2">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAeCAYAAAAo5+5WAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3BJREFUeNqMlnmIT1EUx+f3jLFkb2xDlrGF7GTfdzNmhuxM2SmKyBaKlCWUxlaWMn+IxNBkYiT7LmTJvmdJiMjO8Dn6vrq93pNbn7nzfu++7zvn3HPOfbH+OXfiIkYR6As9oQdUBw+ew3E4DPtzB9R9H/ZwLEJ4AsyDmvAGzsMLKICK0BKqwjtYAVm84Isr4AUEy0AubILHstYsTYWJMBkGQDK0hkOwHE6m7b1bK0q4PByQyCjoJnc/h3j0Ay7AMIXLXn4M8eQwYbOyDaTB9rj/HITgIFN3KA17EC/mCo+ADJhhGxKhMR5OQZ0Q8WuyvinM9zcvgfk2PIUuIYIW17HQQobYJh6TV/mI/vIXYm0202DbA1vYXru/LER0HGyEVo53SfJwJRQKrF8CFop0Tzn6Ec6GCI/WbBs1Fb7DFDgKC3XthuQ+k4VlYDx/esNV+BAi7FtUBTpa6JRiG/6xn6bV0VOqvIpYlOUID4XCcBFyFJ6wYXtVyoR/y5KwsRM6yPUPThFZkeRDjSizPb2hQsT9kvBIxTIdvsF6FUjZiCyqZkZ4enMTiQTjuw6smayC+vATboKfYt9DhC2X73oqYxPtGlhQTpVYAmbCLCgui4vqBfnuA+SxvbwR7PKUZg9hTkD4tYRnQ55iHNPazdAP3gaesaqzLrffk1sLoJ1K2h3vVAipundKnlmnexKwNoVpJKwmn1/51bTDGoj9aFUTsZFblctPgzcQbS6NS7A02N3GwAnYrVKO/U93Q7Q/0xGwkyTdb/iusJX1cPgEW+AyTIvI1VJgggd1MNh+dEb0ub8gPvBAW4legcWwRpgnt+RFkoqmjDbP1tWDZ/86mjIUCmuJDRXvLKXjJBiiYylHp0xtrFykAzbFFXItbgCV1MniVGW5IqYsyFZXC448tdJ9YRYPUzoVhDzYB65DM0gIuW/PJbKRSUHhwjrSt0Vs/iBZeg96hRxN1jvOQWZQ2PLzgdztIJca615llfdlhcXN89ZYOVz/m1EpXCe4whnKw7UwF746b09X87ZxWllRUdd2qkxDbI96hBnWyRVOVF84p+M/U/G03wcqU/xudlZ9wjxJJAxtlJKZ8rKFmxVjlAX+KJB1dgS9hBvOvd36/Er2uxviJ/U11Ng/jeKd1AoOS60zqkZ33FbjStOHSvD74u/4I8AAS4nzKRLPcIQAAAAASUVORK5CYII=" alt="currency" class="img-responsive center-block">
                            </div>
                            <?php 
                          }
                          ?>
                        </div>
                        <?php $countchar = strlen($coupons->description);?>
                        <div class="cls-storelistcont1 clearfix">
                          <div class="col-md-8 col-sm-8 col-xs-12">
                            <div id="forum-contents<?php echo $kt;?>" class='forum-contents'>
                              <div id="comments-spaces<?php echo $kt;?>" class='comments-spaces'>
                                <?php
                                  $osiz_des_length = strlen($coupons->description); 
                                  echo $substrings = substr($coupons->description, 0,70); 
                                  if($osiz_des_length > 70) 
                                  {
                                    echo "
                                    <span class='plus_click' style='float:right;font-size: 16px;position: absolute; cursor: pointer; right:10px; top:0px;'>
                                      <i class='fa fa-plus-square'></i>
                                      <i class='fa fa-minus-square' style='display:none;'></i>
                                    </span>";
                                  }
                                  ?>
                                  <div class="full_desc" style="display:none;text-align:unset !important;"><?php echo substr($coupons->description, 70,$osiz_des_length);?></div>
                              </div>
                            </div>
                            <?php 
                            if($coupons->cashback_description!="")
                            { 
                              ?>
                              <div class = "cls-store-grp">
                                <label> <span>Dica : </span><?php echo $coupons->cashback_description; ?></label>                                
                              </div>
                              <?php
                            }
                            ?>
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12 pd-left-0 pd-right-0">
                            <p class="text-right mt15">
                              <?php 
                              if($user_id=="")
                              {
                                if($coupons->type=='Promotion')
                                {
                                  ?>
                                  <a class="after_login" href="<?php echo base_url();?>cupom-desconto/<?php echo $store_details->affiliate_url; ?>?oid=<?php echo $coupon_id;?>" target="_blank;" data-id="<?php echo base_url();?>ir-loja/<?php echo $affid;?>/<?php echo $coupon_id;?>" show_id="<?php echo $coupon_id;?>"><button class="btn-danger btn-coupon btn-couponactiv center-block"> Ativar desconto </button></a>
                                  <?php
                                }
                                else
                                {
                                ?>  
                                  <a class="after_login" href="<?php echo base_url();?>cupom-desconto/<?php echo $store_details->affiliate_url; ?>?oid=<?php echo $coupon_id;?>" target="_blank" data-id="<?php echo base_url();?>ir-loja/<?php echo $affid;?>/<?php echo $coupon_id;?>" show_id="<?php echo $coupon_id;?>"><button class="btn-danger btn-coupon btn-couponactiv center-block"> Ativar desconto </button>
                                  <button class="btn btn-orange btn2_<?php echo $coupon_id;?>" style="display:none"><?php echo $coupons->code;?></button></a>
                                  <?php
                                }
                              }
                              ?>
                            </p> 
                          </div>   
                        </div>
                      </div>
                      <?php
                      $mi++;
                      $kt++;
                    }  
                    ?> 
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <a id="more_button" href="javascript:void(0);" class="uppercase full-width btn btn-lg center-block">load more</a> 
                      <a id="more_button_null" style="display:none" class="uppercase full-width btn btn-lg center-block">Sorry! No more results found</a>
                    </div>
                  </div>
                  <?php
                }
                else
                {
                  ?>
                  <div class="store-title_red"><h2 class="mar-no mar-bot20">No Expiry Coupons available at this time</h2></div>
                  <?php
                }
              }
              ?>  
              <br>
              <?php 
              if($user_id =='')
              {
                ?>
                <p><?php echo $store_details->how_to_get_this_offer; ?></p>
                <?php
              }
              ?>
          </div>
        </div>
        <?php
        if($user_id=='')
        {
          ?>
          <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="store-sidebarcont comm-store-contblk">
            <h2>Rating</h2>
            <center>
              <input type="hidden" class="baseurls" value="<?php echo base_url();?>front/">
              <input name="rating"  value="0" id="rating_star" type="hidden" postID="<?php echo $store_details->affiliate_id; ?>" storename='<?php echo $store_details->affiliate_name;?>' />
              <div class="overall-rating">(Average Rating <span id="avgrat"><?php if($ratingRow->average_rating == '') { echo '0';} else { echo $ratingRow->average_rating; } ?>/5</span>
              - <span id="totalrat"><?php if($ratingRow->rating_number == ''){ echo '0';} else { echo $ratingRow->rating_number;} ?></span>  rating)</span>
              </div>
            </center>
          </div>
          <div class="store-sidebarcont comm-store-contblk">
            <h2>Lojas Relacionadas</h2>
            <input type="hidden" id="counts" name="cat_counts" value="<?php echo $storescount;?>">
            <input type="hidden" id="new_rel_counts" name="new_rel_counts" value="<?php echo $rel_storescount;?>">
            <?php
            if($storecategories)
            {
              $store_categories      = $storecategories;
              $split_categories      = explode(",",$store_categories);
              $counts                = count($split_categories);
              echo '<span id="myList">';
              foreach($split_categories  as $splitcategories)
              { 
                $ref_cat_details = $this->front_model->get_related_categories($splitcategories);
                $store_name      =  $ref_cat_details->affiliate_url;
                $store_names     =  $ref_cat_details->affiliate_name;
                if($store_name!=$store_details->affiliate_url)
                { 
                  echo
                  '<ul class="list-unstyled">
                    <li>
                      <span class="links cl-effect-5">
                        <a href="'.$store_name.'" class="clsreadblk">
                          <span data-hover="Cupom Desconto '.$store_names.'">Cupom Desconto '.$store_names.'</span>
                        </a>
                      </span>
                    </li>
                  </ul>';
                }   
              }
              echo '</span>';
              echo '<div id="loadMore">Ver Todas</div>
              <div id="showLess" style="display:none">Ver Menos</div>';     
            }
            else if($rel_store_detail)
            {
              $store_categories      = $rel_store_detail;
              $split_categories      = explode(",",$store_categories);
              $counts                = count($split_categories);
              echo '<span id="myList">';
              foreach($split_categories  as $splitcategories)
              { 
                $ref_cat_details = $this->front_model->get_related_categories($splitcategories);
                $store_name      =  $ref_cat_details->affiliate_url;
                $store_names     =  $ref_cat_details->affiliate_name;
                if($store_name!=$store_details->affiliate_url)
                { 
                  echo
                  '<ul class="list-unstyled">
                    <li>
                      <span class="links cl-effect-5">
                        <a href="'.$store_name.'" class="clsreadblk">
                          <span data-hover="Cupom Desconto '.$store_names.'">Cupom Desconto '.$store_names.'</span>
                        </a>
                      </span>
                    </li>
                  </ul>';
                }   
              }
              echo '</span>';
              echo '<div id="loadMore1">Ver Todas</div>
              <div id="showLess1" style="display:none">Ver Menos</div>';  
            }
            else
            {
              echo '<h5><center>Related stores not found</center></h5>';
            }
            ?>         
          </div>
          <?php $related_details   = $getadmindetails[0]->rel_store_details; ?>
          <div class="store-sidebarcont comm-store-contblk">
            <h2>Store details</h2>
            <span>
              <?php 
              if($related_details!='')
              {
                 echo ucfirst($related_details); 
              }
              else
              {
                echo ucfirst("<center><b>Records are Not found</b></center>");  
              }
              ?>
            </span>    
          </div>
          </div>
          <?php
        }
        ?>
      </div> 
    </div>
    <input type="hidden" name="pagenum" id="pagenum" value="2">
    <input type="hidden" name="store_name" id="store_name" value="<?php echo $store_details->affiliate_url?>">
    <input type="hidden" name="coupon_id" value="<?php echo $store_details->affiliate_id; ?>"> 
    <div class="modal fade" id="myModal_how_to_get" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Como funciona o dinheiro de volta? </h4>
          </div>
          <hr>
          <div class="modal-body">
            <p class="txt">
              <strong>Passo 1:</strong> Cadastre-se grátis no <?php $admindetails = $this->front_model->getadmindetails_main(); echo $admindetails->site_name; ?>. &amp; login<br><br>
              <strong>Passo 2:</strong> Faça login e clique na oferta desejada em <?php echo $store_details->affiliate_name;?>. 
              <br><br>
              <strong>Passo 3:</strong> Compre normalmente <?php echo $store_details->affiliate_name?>. <br><br>
              <strong>Passo 4:</strong> Seu dinheiro de volta aparecerá no seu extrato em até 2 dias úteis.
            </p> 
          </div>
          <hr>     
        </div>
      </div>
    </div>   
    <div class="modal fade" id="myModal_terms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
            <h4 class="modal-title" id="myModalLabel">Regras & Exceções </h4>
          </div>
          <hr>
          <div class="modal-body">
            <?php
              if($store_details->terms_and_conditions)
              {
                echo $store_details->terms_and_conditions;
              } 
            ?> 
            <hr>     
          </div>
        </div>
      </div>
    </div>  
</section>
<?php $this->load->view('front/site_intro'); ?> 
<!-- Footer menu start -->
<footer>
  <section class="footer-section innerfoot-sect">
    <div class="container">
      <div class="clearfix wow fadeInDown">
      <div class="wow fadeInDown">
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Help And Support</h5>
          <ul class="footer-nav">
            <li><a href="<?php echo base_url();?>contato"><i class="fa fa-caret-right"></i> Contact Us</a></li>
            <li><a href="<?php echo base_url();?>cms/perguntas-frequentes"><i class="fa fa-caret-right"></i> Perguntas Frequentes</a></li>
            <?php 
            $result = $this->front_model->sub_menu();
            foreach($result as $view)
            {
            ?>
              <li><a href="<?php echo base_url();?>cms/<?php echo $view->cms_title; ?>"><i class="fa fa-caret-right"></i> <?php echo $view->cms_heading;?></a></li>
            <?php 
            }
            ?>
          </ul>
        </div>
        <?php 
        if($user_id== '')
        {
          ?>
          <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Email Newsletter</h5>
          <ul class="footer-nav">
            <li><a href="#"><i class="fa fa-caret-right"></i> Subscribe to recieve inspiration, ideas and news in your inbox.</a></li>
            <li>
              <div class="col-md-12 col-sm-12 col-xs-12 pd-left-0">
              <input type="text" id="news_email" onkeypress="clears(2);" class="form-control tbox1" placeholder="Email Address">
              <input type="button" class="tbtn1" value="send" onClick="email_news();">
              </div>
              &nbsp;
              <span id="new_msg" style="color:red"></span>
            </li>
          </ul>
          </div>
          <?php
        }
        ?>
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Top Categories</h5>
          <ul class="footer-nav">
            <?php 
            $categories = $this->front_model->get_all_categories(8);
            foreach($categories as $view)
            {
            ?> 
            <li><a href="<?php echo base_url();?>ofertas/<?php echo $view->category_url;?>"><i class="fa fa-caret-right"></i> <?php echo $view->category_name;?></a></li>
            <?php
            }
            ?>
          </ul>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Top Stores</h5>
          <ul class="footer-nav">
          <?php 
          $stores = $this->front_model->get_all_stores(8);
          foreach($stores as $view)
          {
          ?>
            <li><a href="<?php echo base_url(); ?>cupom-desconto/<?php echo $view->affiliate_url?>"><i class="fa fa-caret-right"></i> <?php echo $view->affiliate_name; ?></a></li>
          <?php
          }
          ?>
          </ul>
        </div>
      </div>
      </div>
    </div>
  </section>
  <section class="copyright-section">
    <div class="container">
     <div class="row wow fadeInDown">
      <div class="col-md-6 col-sm-6 col-xs-12 wow flipInX">
        <p>Copyright &copy; <?php echo date('Y')?> <?php echo $admindetails->site_name;?>  |  All Rights Reserved </p>
      </div>
      <?php
      $listing = $this->db->query("select * from admin")->row();
      ?>
      <div class="col-md-6 wow flipInX">
        <ul class="list-socials pull-right">
          <?php if($listing->admin_twitter) {?> <li><a href="<?php echo $listing->admin_twitter;?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
          <?php if($listing->admin_fb)      {?> <li><a href="<?php echo $listing->admin_fb;?>">     <i class="fa fa-facebook"></i></a></li><?php } ?>
          <?php if($listing->admin_gplus)   {?> <li><a href="<?php echo $listing->admin_gplus;?>">  <i class="fa fa-google-plus"></i></a></li><?php } ?>
        </ul>
      </div>
     </div>
    </div>
  </section>
</footer>
<!-- Start a new code for codeigniter minify method 28-11-16-->
<?php 
$this->minify->add_js(array('jquery.1.11.1.min.js'))->add_js('bootstrap.min.js,jquery.validate.min.js');
$this->minify->add_js(array('jquery-ui.js'))->add_js('rating.js');
$js_minify = $this->minify->deploy_js();
?>
<script src="<?php echo $this->front_model->get_css_js_url();?>front/minify/scripts.min.js"></script>
<!-- End 28-11-16 -->
<?php include('store_footer.php'); ?>
<!-- New code for popup details 18-11-16 -->
<?php include('popup_details.php'); ?>
<!-- End 18-11-16 -->