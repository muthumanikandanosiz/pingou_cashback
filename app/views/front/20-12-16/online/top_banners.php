
<?php  
$user_id                 = $this->session->userdata('user_id');
$userdetails             = $this->front_model->userdetails($user_id);
$pagename                = $this->uri->segment(1);
$bannerplace_details     = $this->db->query("SELECT * from `sales_banner_status` where sales_banner_id =1")->row();
$category_type           = $userdetails->referral_category_type;
$bonus_status            = $userdetails->bonus_benefit;
$login_type              = $userdetails->app_login;


if($pagename == 'cupom')
{
  $page_top_status         = $bannerplace_details->topcashback_top_status;
  $page_bot_status         = $bannerplace_details->topcashback_bottom_status; 
}
if($pagename == 'ofertas')
{
  $page_top_status         = $bannerplace_details->category_top_status;
  $page_bot_status         = $bannerplace_details->category_bottom_status;
}
/*if($pagename == 'cupom-desconto')
{
  $page_top_status         = $bannerplace_details->store_top_status;
  $page_bot_status         = $bannerplace_details->store_bottom_status;
}*/
if($pagename == 'barato')
{
  $page_top_status         = $bannerplace_details->barato_top_status;
  $page_bot_status         = $bannerplace_details->barato_bottom_status; 
}
if($pagename == 'minha-conta')
{
  $page_top_status         = $bannerplace_details->minhaconta_top_status;
  $page_bot_status         = $bannerplace_details->minhaconta_bottom_status;
}
if($pagename == 'extrato')
{
  $page_top_status         = $bannerplace_details->extrato_top_status;
  $page_bot_status         = $bannerplace_details->extrato_bottom_status; 
}
if($pagename == 'resgate')
{
  $page_top_status         = $bannerplace_details->resgate_top_status;
  $page_bot_status         = $bannerplace_details->resgate_bottom_status; 
}
if($pagename == 'saidas-para-loja')
{
  $page_top_status         = $bannerplace_details->clickhistory_top_status;
  $page_bot_status         = $bannerplace_details->clickhistory_bottom_status;
}

//echo $page_top_status;
if($user_id == '')
{
  if($page_top_status == 1)
  {
    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
    ?>
    <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
      <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
    </a>
    <?php 
  }
  if($page_top_status == 2)
  {
    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlogusers' and banner_status!=0 order by RAND() limit 1")->result();
    $html_content  = $this->db->query("SELECT `not_log_html_settings` from `sales_funnel` where not_log_status !=0")->result();
    ?>
    <!-- <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
      <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
    </a> -->
    <?php
    echo $html_content[0]->not_log_html_settings; 
  }
  if($page_top_status == 3)
  {
    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0 order by RAND() limit 1")->result();
    ?>
    <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
      <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
    </a>
    <?php 
  }
  if($page_top_status == 4)
  {
    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='notlog_standard' and banner_status!=0")->result();
    $html_content  = $this->db->query("SELECT `notlog_standard_html_settings` from `sales_funnel` where notlog_standard_status !=0")->result();
    ?>
    <!-- <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
      <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
    </a> -->
    <?php
    echo $html_content[0]->notlog_standard_html_settings;
  }
}
else
{
  if(($bonus_status == 0) && ($login_type == 0))
  {
    $conditiondetails = 'log_notusebonus_notuseapp';
    $html_name        = 'log_notusebonus_notuseapp_html_settings';
  }
  if(($bonus_status == 1) && ($login_type == 0))
  {
    $conditiondetails = 'log_usebonus_notuseapp';
    $html_name        = 'log_usebonus_notuseapp_html_settings';
  }
  if(($bonus_status == 0) && ($login_type == 1))
  {
    $conditiondetails = 'log_notusebonus_useapp';
    $html_name        = 'log_notusebonus_useapp_html_settings';
  }
  if(($bonus_status == 1) && ($login_type == 1))
  {
    $conditiondetails = 'log_usebonus_useapp';
    $html_name        = 'log_usebonus_useapp_html_settings';
  }

  if($page_top_status == 1)
  {
    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='$conditiondetails' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
    if($bannerdetails)
    {
      ?>
      <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
        <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
      </a>
      <?php 
    }  
  }
  if($page_top_status == 2)
  {
    
    //$bannerdetails    = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='$conditiondetails' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0")->result();
    $conditiondetails = $conditiondetails.'_status';
    $html_content     = $this->db->query("SELECT $html_name from `sales_funnel` where $conditiondetails!=0")->row($html_name);
    //print_r($bannerdetails);
    //if($bannerdetails)
    //{
      ?>
     <!--  <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
        <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
      </a> -->
      <?php
      echo $html_content;//$html_content[0]->log_notusebonus_notuseapp_html_settings;
    //}  
  }
  if($page_top_status == 3)
  {
    $bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='log_standard' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0 order by RAND() limit 1")->result();
    if($bannerdetails)
    {
      ?>
      <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
        <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
      </a>
      <?php     
    } 
  }
  if($page_top_status == 4)
  {
    //$bannerdetails = $this->db->query("SELECT * from `sales_funnel_banners` where `banner_condition_details`='log_standard' AND (banner_cat_type='$category_type' OR banner_cat_type='all') AND banner_status!=0")->result();
    $html_content     = $this->db->query("SELECT `log_standard_html_settings` from `sales_funnel` where log_standard_status!=0")->row('log_standard_html_settings');
    //if($bannerdetails)
    //{
      ?>
      <!-- <a target="blank" href="<?php echo $bannerdetails[0]->banner_img_url;?>" class="click_count" bannerid="<?php echo $bannerdetails[0]->banner_id;?>" >
        <img class="img-responsive cls_topad" src="<?php echo $this->front_model->get_img_url().'uploads/banners/'.$bannerdetails[0]->image_name; ?>">
      </a> -->
      <?php
      echo $html_content;     
    //} 
  }  
}
 