<?php $this->load->view('front/header'); ?>
<!-- header content End -->

<!-- stores -->
<section class="cms wow fadeInDown">
  <div class="container">
    <div class="heading wow bounceIn">
      <h2>Category</h2>
      <div class="heading_border_cms">
        <span>
          <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/top_drop.png">
        </span>
      </div>
    </div>
    <!-- stores -->
    <br>
    <?php 
    $user_id                = $this->session->userdata('user_id');
    $userdetails            = $this->front_model->userdetails($user_id);
    ?>
    <br><br>
    <section class="cashback" >
      <div class="categories">
        <div class="row">
          <?php 
          $categories = $this->front_model->get_all_categories();
          $kt = 1;
          foreach($categories as $view)
          {
            $category_id     = $view->category_id; 
            $category_image  = $view->category_img;
            $category_name   = $view->category_url;
            $category_names  = $view->category_name;
            ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="catcoup-container">
                  <div class="cate coup"> 
                    <?php if($category_image!='') 
                    {
                    ?>
                      <img src="<?php echo $this->front_model->get_img_url();?>uploads/img/<?php echo $category_image;?>" style="height:265px!important; max-width:none!important;" width="265" class="img-responsive center-block">
                    <?php
                    }
                    else
                    {?>
                      <img src="<?php echo $this->front_model->get_img_url();?>front/new/images/c1.png" class="img-responsive center-block">
                    <?php
                    } 
                    ?>
                    <div class="overlay">
                      <div class="cat_content"> 
                        <a class="cat_btn1" href="<?php echo base_url();?>cupom-desconto/<?php echo $category_name;?>">
                          <span> <?php echo $category_names;?> </span>
                        </a> 
                        <a class="strlnk md-trigger md-setperspective menulink" data-modal="modal-<?php echo $category_id;?>">
                          <span>
                            <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/link.png">
                          </span>
                        </a> 
                      </div>
                    </div>
                  </div>
                  <div class="cat-coup-mask">
                    <a class="cat_btn" href="#">
                      <span><?php echo $category_names;?></span>
                    </a>
                  </div>
                </div>
            </div>
            <div class="md-modal md-effect-<?php echo $view->category_id;?>" id="modal-<?php echo $view->category_id;?>">
                <div class="md-content">
                  <h3>About <?php echo $view->category_name;?></h3>                
                  <div>
                    <center>
                      <p><?php echo $view->category_desc; ?></p>                         
                      <button class="md-close btn btn-primary">CLOSE</button>
                    </center>
                  </div>
                </div>
            </div>
            <?php 
          }
          ?>
        </div>
        <div> 
        </div>
      </div>
    </section>
    
  </div>
</section>

<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>
<!-- Footer Menu End-->