<?php $this->load->view('front/header') ?>
<!-- Header ends here -->
<section class="cms wow fadeInDown">
  <div class="container">
    <div class="heading wow bounceIn">
      <h2> Refer <span>Friends</span></h2>
      <div class="heading_border_cms">
        <span>
          <img src="<?php echo $this->front_model->get_img_url(); ?>/front/new/images/top_drop.png">
        </span>
      </div>
    </div>
  </div>
  <section class="cms wow fadeInDown">      
    <div class="container">
      <section class="profile-sec">
        <div class="row" id="page-user-profile">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-12">
                <!-- <ul class="nav nav-tabs ul-edit responsive hidden-xs hidden-sm">
                  <li class=""><a href="myaccount.html"> Account Settings</a></li>
                  <li class=""><a href="myearnings.html">  My Earnings</a></li>
                  <li class=""><a href="payment.html">  Payment</a></li>
                  <li class=""><a href="missing-cashback.html">  Missing Cashback?</a></li>
                  <li class="active"><a href="<?php echo base_url(); ?>refer_friends">  Refer Friends</a></li>
                  <li class=""><a href="referral-network.html">  Referral Network</a></li>
                  <li class=""><a href="testimonials.html">  Testimonials</a></li>
                </ul> -->
                <div class="tab-content" id="generalTabContent">
                  <div class="row">
                    <div class="col-md-12">
                      <h4 class="mar-bot20"> Refer Friends </h4>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lacinia dictum sapien, a egestas elit lobortis vel. Sed sapien dui, congue et pharetra eleifend, finibus a augue. Phasellus lacus ligula, vestibulum sed nunc vel, placerat congue leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dignissim metus quam.</p>


                   <div class="cls_ref_frndblk">   
                      <div class="refer-link clearfix">
                        <div class="row">
                          <div class="col-md-4 col-sm-4 col-xs-12">
                            <h4 class="clr-wht fnt-sz-15">Your unique referral link:</h4>
                          </div>
                          <div class="col-md-4 col-sm-5 col-xs-12">
                            <?php
                            $result = $this->front_model->refer_friends();
                    				if($result)
                    				{
                    					foreach($result as $views)
                    					{
                    				    ?>                
                        				<input type="text" class="form-control" value="<?php echo base_url().'?ref='.$views->random_code; ?>">
                        				<input type="hidden" id="random_code" value="<?php echo $views->random_code; ?>">
                                <?php
                              }
                        		}
                            ?>  
                          </div>
                          <div class="col-md-4 col-sm-3 col-xs-12">
                            <h4 class="clr-wht fnt-sz-15"><strong>Friends referred: <?php echo count($this->front_model->referral_network());?></strong></h4>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="fw inviter" id="RefFrndScroll">
                          	<h4 class="text-center text-danger">Import contacts from email or social media</h4>
                            <p class="text-center">Personal details are not saved and all information entered
                             is secure and encrypted</p>
                      		  <p id="error" class="text-center" style="color:red;"></p>
                            <p id="success" class="text-center"></p>

                           <div class="fl fw mails_list clearfix">
                            <ul class="fl fw">
          				            <li class="fl">
                              <input type="checkbox" name="chech" title="Select/Deselect all" id="" 
                              name="toggle_all" onClick="selectAll(this)" class="fl">
                              <strong>Select All</strong>
                              </li>
                            </ul>
                            </div>
                    				<div class="fl fw mails_list">
                      				<ul class="fl fw clearfix">
                              <?php
                      				
                              if($user_contacts!='')
                      				{
                      				  
                               /* $i=0;
                                $Row_Count = count($user_contacts);*/

                                foreach($user_contacts as $views)
                      					{
                      					  /*$i++;
                                  $Mod = $i % 10;
                                  if($Mod == 1)
                                  {*/
                                    ?>
                                    
                      					    
                        						<?php
                                  //}
                                    if($views->email !='')
                                    {
                                      ?>

                                      <li class="fl">
                                        <input type="checkbox" class="fl contact_mail" value="<?php echo $views->email; ?>" id="check" name="email[]" ><!-- </li> -->
                                        <!--<li class="fl txt"><?php echo $views->displayName; ?></li>-->
                                        <!-- <li class="fl txt"> -->
                                        <span>
                                        <?php  echo $views->email; ?>  
                                        </span>                                      
                                      </li>
                      					     <?php
                                    }
                                  /*if($Mod == 0 || $i == $Row_Count)
                                  {*/
                                    ?>
                                    
                                    
                                    <?php 
                                 // }  
                                }
                              }
                              ?>
                              </ul>
                    				</div>
                    				<textarea rows="5" class="form-control" id="mail_text" style="display:none;">Hi,
                              I've been using <?php $admindetails = $this->front_model->getadmindetails_main(); echo $admindetails->site_name; ?> to save money on websites I use anyway like Flipkart, Yatra, Jabong and more. I thought this would be useful for you too. 
                              It's free to join and you can easily save Rs 10,000 every year. You've got nothing to lose by trying it!  
                              Enjoy!
                              <?php 
                              if($result)
                              {
                              	foreach($result as $views)
                              	{ 
                              		echo $views->first_name.' '.$views->last_name;
                              	} 
                              }
                              else
                              {
                              	echo "No results Found";
                              }
                              ?>
                            </textarea>
                          </div>
                        </div>
                      </div>
                      </div>

                       <div class="clearfix">
                        <input type="submit" value="Send invites" onClick="send_mail();" name="send" 
                        class="fl btn btn-blu pull-right">
                      </div> 

                    </div>
                  </div>
                </div>
                <!-- <div class="panel-group responsive visible-xs visible-sm" id="collapse-undefined">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#collapse-undefined" href="#collapse-tab-activity"><i class="fa fa-bolt"></i>&nbsp;
                        Activity</a></h4>
                    </div>
                    <div id="collapse-tab-activity" class="panel-collapse collapse" style="height: 0px;">
                      <div class="panel-body"></div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#collapse-undefined" href="#collapse-tab-edit"><i class="fa fa-edit"></i>&nbsp;
                        Edit Profile</a></h4>
                    </div>
                    <div id="collapse-tab-edit" class="panel-collapse collapse in" style="">
                      <div class="panel-body"></div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#collapse-undefined" href="#collapse-tab-messages"><i class="fa fa-envelope-o"></i>&nbsp;
                        Messages</a></h4>
                    </div>
                    <div id="collapse-tab-messages" class="panel-collapse collapse" style="height: 0px;">
                      <div class="panel-body"></div>
                    </div>
                  </div>
                </div> -->
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </section>
</section>

<?php $this->load->view('front/site_intro');?>  
<?php $this->load->view('front/sub_footer');?>



<script type="text/javascript">
//Start Email subscribe

function email_sub()
{
	var email = $("#email").val();
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,3})?$/;
	if(!email || !emailReg.test(email))
		$('#email').css('border', '2px solid red');
	else
	{
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>cashback/email_subscribe/",
			data: {'email':email},
			success: function(msg)
			{
				if(msg==1)
				{
					$('#msg').text('Activated Successfully');
					$('#email').css('border', '');
				}
				else
				{
					$('#msg').text('Already Activated');
					$('#email').css('border', '');
				}	
			}
		});
	}	
}
/*function email_news()
{
	var email = $("#news_email").val();
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,3})?$/;
	if(!email || !emailReg.test(email))
	{
		$('#news_email').css('border', '2px solid red');
	}	
	else
	{
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>cashback/email_subscribe/",
			data: {'email':email},
			success: function(msg)
			{
				if(msg==1)
				{
					$('#new_msg').text('Activated Successfully');
					$('#news_email').css('border', '');
				}
				else
				{
					$('#new_msg').text('Already Activated');
					$('#news_email').css('border', '');
				}	
			}
		});
	}	
}*/

function clears(val)
{
	if(val==1)
		$('#invite_mail').css('border', '');
	else
		$('#news_email').css('border', '');
} 

//End Email subscribe

//Friends Invite Mail
function send_mail()
{
	 var random = $('#random_code').val();
     var text = $('#mail_text').val();
	 var result = "";
   var arrresult = Array(); 
     $('input.contact_mail:checked').each(function (e) {
 		      arrresult[e] = $(this).val();
     });
  result = arrresult.toString();   
	if(result == '')
		$('#error').text('No Email Selected');
	else
	{
		$.ajax({
		type: "post",
		url: "<?php echo base_url(); ?>cashback/invite_mail/",
		data: {'email':result,'random':random,'mail_text':text},
		success: function()
		{
       
          $(':checkbox:checked').removeAttr('checked');
          $('#success').text('Mail Send Successfully');
          window.setTimeout(function(){
          window.location.href = "<?php echo base_url();?>minha-conta";
          }, 300);
		}
		});
	}	
}

function MyPopUpWin(url, width, height) {
    var leftPosition, topPosition;
    //Allow for borders.
    leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
    //Allow for title and status bars.
    topPosition = (window.screen.height / 2) - ((height / 2) + 50);
    //Open the window.
    window.open(url, "Window2",
    "status=no,height=" + height + ",width=" + width + ",resizable=yes,left="
    + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY="
    + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no");
}

function selectAll(source) {
		checkboxes = document.getElementsByName('email[]');
		var n=checkboxes.length;
		//alert(n);
		for(var i=0;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
	}
   


</script>