<div class="modal fade wow bounceIn" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:block;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header login-popuphead"> <a class="login_tit"><span aria-hidden="true"><img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/login_tit.png"></span></a>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i> </span></button>
        <h4 class="modal-title" id="myModalLabel">Login</h4>
      </div>
      <?php
      $redirect_urlstring =  uri_string();
      if($redirect_urlstring=="")
      {
        $redirect_urlstring = 'index';
      }
      $redirect_endcede = insep_encode($redirect_urlstring);
      ?>
      <div class="modal-body login-popupbody">
        <?php
        //begin form
        $attribute = array('role'=>'form','name'=>'login_form','id'=>'login_form', 'onSubmit'=>'return setupajax_login();', 'autocomplete'=>'off','method'=>'post');
        echo form_open('chk-invalid',$attribute);           
        
        if($this->uri->segment(1) == 'cupom-desconto') 
        {
          if($cashback_details !='')
          {
            ?>
            <!-- New code for session concept 18-7-16 --> 
            <input type="hidden" name="new_cash_ex_id"   id="new_cash_ex_id"    value="<?php echo $new_cash_ex_id; ?>">
            <input type="hidden" name="cashback_details" id="cashback_details"  value="<?php echo $cashback_details; ?>">
            <input type="hidden" name="expiry_dates"     id="expiry_dates"      value="<?php echo $expiry_dates; ?>">
            <input type="hidden" name="cashback_web"     id="cashback_web"      value="<?php echo $cashback_web; ?>">
            <!-- End --> 
            <?php
          }
        }
        ?>
          <input type="hidden" name="pagename" value="<?php echo $this->uri->segment(1)?>">
          <input type="hidden" name="signin" value="signin" id="signin" />
          <div class="row fb-block">
            <div class="center-block pad-no">
              <center>
                <a class="facebook" href="<?php echo base_url();?>HAuth/register/Facebook/<?php echo $redirect_endcede;?><?php echo $extra_urls; ?>"> <i class="fa fa-facebook"></i> Login with Facebook </a>
              </center>
              <br>
              <center>
                <a class="google" href="<?php echo base_url();?>HAuth/register/Google/<?php echo $redirect_endcede;?><?php echo $extra_urls; ?>"><i class="fa fa-google"></i>Login with Gmail</a>
              </center>
              <br>
            </div>
          </div>
          <div class="row or-blk">
            <div class="form-or"> <span>or</span> </div>
          </div>
          <center><span id="newdis" style="color:red; font-weight:bold;"> </span></center>
          <div class="form-group enve"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/envelop_icon.png">
            <input type="email" required name="emails" class="form-control" id="emails" placeholder="Enter your Email ">
          </div>
          <div class="form-group lock"> <img src="<?php echo $this->front_model->get_img_url(); ?>front/new/images/lock_icon.png">
            <input type="password" required name="passwrd" class="form-control" id="passwrd" placeholder="Enter your Password">
          </div>
          <center>
            <input type="submit" name="signin" id="signin" class="btn btn-signin" value="Sign in">
          </center>
          <div class="form-group">
            <div class="checkbox mar_30">
              <input type="checkbox" id="checkbox2" class="styled">
              <label for="checkbox2"> Remember Me </label>
              <span class="pull-right forgot"><a href="#forgot" data-toggle="modal" id="forgotclk">Forgot Password ?</a></span> </div> <!--  data-toggle="modal" data-target="#forgot" -->
          </div>
        <?php echo form_close();?>
      </div>
      <div class="modal-footer">
        <div class="form-group"> <span class="pull-left need"><a href="#">Need New Account ?</a></span> <span class="pull-right signup"><a href="#register" data-toggle="modal" id="signupclk">Sign up</a></span> </div>
      </div>
    </div>
  </div>
    </div>