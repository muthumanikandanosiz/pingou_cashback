<!-- header -->
  <?php $this->load->view('front/header');?>
<!-- Header ends here -->
<section class="cms wow fadeInDown">
<div class="container">
      <div class="heading wow bounceIn">
          <h2> Verify <span>Account</span></h2>
          <div class="heading_border_cms">
            <span>
              <img src="<?php echo $this->front_model->get_img_url(); ?>/front/new/images/top_drop.png">
            </span>
          </div>
      </div>
    </div>
<div class="wrap-top">
  <div id="content">
   
	<?php
		$complete = $this->session->flashdata('complete');
		if($complete!="") {
		echo '<div class="alert alert-success">
		<button data-dismiss="alert" class="close">x</button>
		<strong>'.$complete.'</strong></div>';
	} ?>
  
    <div class="container">

      <section class="contact-section section section-on-bg">
        <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <?php 
		if($verify_msg==1)
		{
			?>
			<h3 class="heading text-center"> Account has been activated Sucessfully</h3>
			<?php
		}
		else
		{
			?>
			<h3 class="heading text-center"> Account has been Not Verified</h3>
			<?php
		}
		?>
        </div> 
        </div> 
        </section>
    </div>
  </div>
</div>
</section>
<!-- footer -->

<?php 
$this->load->view('front/site_intro');
$this->load->view('front/sub_footer');
?>