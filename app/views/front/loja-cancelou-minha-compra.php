<?php $this->load->view('front/header'); ?>
<!-- header content End --> 
<style>
.text-primary
{
  background: #4daed9 none repeat scroll 0 0;
  border-radius: 20px;
  color: #fff;
  font-weight: 500;
  margin-bottom: 0;
  padding: 7px 15px;
}

.butn_clr_success
{
  background: #68c668 none repeat scroll 0 0;
  border-radius: 20px;
  color: #fff;
  font-weight: 500;
  margin-bottom: 0;
  padding: 5px 15px;
}
.butn_clr_danger
{
  background: #ff4242 none repeat scroll 0 0;
  border-radius: 20px;
  color: #fff;
  font-weight: 500;
  margin-bottom: 0;
  padding: 5px 15px;
}

.error
{
  color:#ff0000;
}
.required_field
{
 color:#ff0000;
}

.view_ticket label:before 
{
  content: attr(data-name);
  left: 0;
  position: absolute;
  top: 0;
}
.view_ticket label 
{
  border-top: 1px solid #e6e6e6;
  font-size: 12px;
  line-height: 30px;
  padding-left: 170px;
  position: relative;
}

.btn{
  white-space: normal!important;
}
a.tooltips 
{
  position: relative;
  display: inline;
}
a.tooltips span 
{
  background: #3da0d5 none repeat scroll 0 0;
  border-radius: 2px;
  color: #ffffff;
  line-height: 30px;
  min-height: 30px;
  min-width: 100%;
  position: absolute;
  text-align: center;
  visibility: hidden;
  width: 250px!important;
  word-wrap: break-word;
}
a.tooltips span:after 
{
  content: '';
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -8px;
  width: 0; height: 0;
  border-top: 8px solid #3da0d5;
  border-right: 8px solid transparent;
  border-left: 8px solid transparent;
}
a:hover.tooltips span 
{
  visibility: visible;
  bottom: 30px;
  left: 50%;
  margin-left: -76px;
  z-index: 999;
}
/*}*/
.table-responsive {
  overflow-x: unset;
}

div.selector, div.selector span, div.checker span, div.radio span, div.uploader, div.uploader span.action, div.button, div.button span
  {background-image:none;}
  .col-xs-6 {width: 47% !important; float: left;min-height:1px;padding-left: 12px;padding-right: 12px;position: relative;}
  select.input-sm { width:60px !important;}
  .row {margin-left: -12px;margin-right: -12px;}
  .pagination {border-radius: 4px;margin: 20px 0;padding-left: 0;}
  .dataTables_filter label::after {content: '' !important;}

</style>

<!-- Main Content start -->
<?php $user_id = $this->session->userdata('user_id');?>
<section class="cms wow fadeInDown">
    <div class="container">
      <div class="heading wow bounceIn">
          <h2>
            faltando <span> Aprovação</span>
          </h2>
        <div class="heading_border_cms">
          <span>
            <img src="<?php echo $this->front_model->get_img_url();?>front/new/images/top_drop.png">
          </span>
        </div>
      </div>
      <div class="myac">
        <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1" id="hide_fn">
            <div class="my_account my_accblk">
              <div class="myacc-maintab">
                  <!-- Nav tabs -->
                  <?php $this->load->view('front/user_menu'); ?>
              </div>
              <!-- Tab panes -->
              <div class="tab-content"><br>
                  <h4 class="mar-bot20">faltando Aprovação</h4>
                      
                    <?php 
                    $error = $this->session->flashdata('error');
                    if($error!="")
                    {
                      echo '<div class="alert alert-warning">
                      <button data-dismiss="alert" class="close">x</button>
                      '.$error.'</div>';
                    }
                    
                    $success = $this->session->flashdata('success');
                    if($success!="")
                    {
                      echo '<div class="alert alert-success">
                      <button data-dismiss="alert" class="close">x</button>
                      '.$success.'</div>';      
                    }
                    ?>
                    <!-- <h4 class="mar-top20 mar-bot20">Extrato</h4>  -->
                    
                      <br><br>
                <div role="tabpanel" id="acc2">
                  <!--account table section starts-->
                  <div class="row wow fadeInDown">
                    <div class="col-md-12 col-sm-12">
                      <div class="a">
                        <?php 
                        if(empty($results))
                        {
                          echo "<center>Aqui você poderá acompanhar o status de todos os tickets de suporte que forem realizados.</center><br>";
                        } 
                        else
                        {
                          ?>
                          <table cl id="sample_teste1" class="display zui-table zui-table-rounded table acc-table1">
                            <thead>
                              <tr>
                                <th>Transaction <br>Date</th>
                                <th>Store</th>
                                <th>Cashback</th>
                                <th>Transaction Amount</th>
                                <th>Status</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>  
                              <?php
                              if($results!='')
                              {
                                $kss=1;
                                //foreach($results as $res)
                                //{     
                                ?>
                                  <!-- <tr>
                                    <td><?php echo date("d/m/y",strtotime($res->transaction_date));?></td>
                                    <td><?php echo $res->affiliate_id;?></td>
                                    <td>R$ <?php echo $this->front_model->currency_format($res->cashback_amount);?></td>
                                    <td>R$ <?php echo $this->front_model->currency_format($res->transaction_amount);?></td>
                                    <?php
                                    $Count_Missing_Approval = $this->front_model->missing_approval_count($res->reference_id);
                                    ?>
                                    <td><a href="#" class="text-danger" style="font-weight: 500;"> cancelado </a></td>
                                    <?php
                                    $getadmindetails = $this->front_model->getadmindetails();
                                    $site_logo = $getadmindetails[0]->site_logo;
                                    if($Count_Missing_Approval == 0)
                                    {
                                      ?>
                                      <td  class="label-txt"><a href="<?php echo base_url();?>add-missing-approval/<?php echo base64_encode($res->cashback_id);?>"><label class="text-primary">Contestar Cancelamento</label></a> </td>
                                      <?php
                                    }
                                    else
                                    {   
                                      ?>
                                      <td>Cancelamento jã contestadado</td>
                                      <?php
                                    }
                                    ?>
                                  </tr> -->
                                  <?php
                                  //$kss++;
                                //} 
                              }
                              ?>
                            </tbody>  
                          </table>
                          <?php
                        }
                        ?>
                      </div>
                    </div>
                  </div>
                  <br>
                  <!--account table section ends-->
                </div>
                <br>
                <?php $this->load->view('front/my_earnings.php')?>
              </div>
            </div>
        </div>
      </div>
    </div>
</section>  
<!-- Main Content end -->

<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
<!-- Footer menu start -->
<?php $this->load->view('front/sub_footer');?>
<style type="text/css">
 .dataTables_processing
{
  display:none !important;
}
</style>
 <link href="<?php echo $this->front_model->get_css_js_url(); ?>assets/css/pagination.css" rel="stylesheet" />
