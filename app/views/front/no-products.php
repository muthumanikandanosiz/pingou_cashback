<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

 	<title>No Products Found</title>
	<meta name="Description" content=""/>    
    <meta name="keywords" content="" />    
    
<!-- Bootstrap -->
<?php $this->load->view('front/css_script'); ?>	

   <link rel="stylesheet" href="<?php echo base_url();?>front/css/pre-pge.css" type="text/css">

</head>

<body>
<?php $this->load->view('front/header'); ?>

<!-- Header ends here -->

<div class="wrap-top">
  <div id="content">
   <div class="page-intro" style="margin-top: 0px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ol class="breadcrumb">
                                <li><i class="fa fa-home pad-rht"></i><a href="<?php echo base_url();?>">Home</a></li>
                               <!-- <li class=""><a href="<?php echo base_url();?>myaccount">Products</a></li>-->
                                  <!--<li class="active"><a href="<?php echo base_url();?>my_payments">My Payments</a></li>-->
                             <!--   <li class="active">Add Missing Cashback</li>-->
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
    <div class="container">
    
      <section class="coupon-section mar40">
      
      
       
       <div class="row">
       
       <div class="col-md-3">
                    <aside class="sidebar-left store-title">
                     <h3 class="mar-no mar-bot20"><i class="fa fa-ticket"></i>Categories</h3>
                        <ul class="nav nav-tabs nav-stacked nav-coupon-category nav-coupon-category-left">
                      
                           
                            <?php 
								$categories = $this->front_model->get_all_categories(10);
								foreach($categories as $view)
								{
									$category_name = $view->category_name; 
								?>
									<li><a href="<?php echo base_url();?>products<?php echo $view->category_url?>"><i class="fa fa-arrow-circle-right"></i> <?php echo $view->category_name; ?></a>
                  </li>
								<?php 
								} ?>
                          
                        </ul>
                        
                        
                        
                        
                    </aside>

                </div>
       
       <div class="col-md-9">
       
       <div class="store-title_red"><h3 class="mar-no mar-bot20">No Records Found for Your Search</h3></div>
      
       
       
       
      
       </div>
       
       </div> 
                
         
        </section>
        
        
    </div>
  </div>
</div>

<footer>
  <?php
//sub footer
	$this->load->view('front/sub_footer');
	
//Footer
	$this->load->view('front/site_intro');	

?>
</footer>

<!-- FAQ --->




<?php $this->load->view('front/js_scripts');?>



 <!-- contact page specific js starts -->
 
    <script type="text/javascript" src="<?php echo base_url(); ?>front/js/map/jquery.validate.min.js"></script>       
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>front/js/map/gmaps.js"></script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>front/js/map/map.js"></script>

<!-- Slider --> 

<script type="text/javascript">
$(function () { $("[data-toggle='tooltip']").tooltip(); });

</script>


<script type="application/javascript">

function toggle_st(num)
{
	$('.toggle'+num).toggle('slow');
	return false;	
}
</script> 



</body>
</html>
