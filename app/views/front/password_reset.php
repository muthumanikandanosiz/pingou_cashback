<!-- header -->
  <?php $this->load->view('front/header');?>
<!-- Header ends here -->
<style>
.error
{
	color:#ff0000;
}
.required_field
{
 color:#ff0000;
}
</style> 


<section class="cms wow fadeInDown">
  	<div class="container">
	    <div class="heading wow bounceIn">
	      <h2><span>Reset </span>Password</h2>
	      <div class="heading_border_cms"><span><img src="<?php echo $this->front_model->get_css_js_url(); ?>/front/new/images/top_drop.png"></span> </div>
	    </div>
	    <div class="col-md-6 col-sm-12 col-xs-12 fn center-block">
		    <div class="wow fadeInDown">
				<div class="panel-body">
			        <?php
					$error = $this->session->flashdata('error');
					if($error!="")
					{
						echo '<div class="alert alert-danger">
						<button data-dismiss="alert" class="close">x</button>
						<strong>Error! </strong>'.$error.'</div>';
					}  
					$success = $this->session->flashdata('success');
					if($success!="")
					{
						echo '<div class="alert alert-success">
						<button data-dismiss="alert" class="close">x</button>
						<strong>Success! </strong>'.$success.'</div>';
					}

				 	//begin form
					$attribute = array('role'=>'form','name'=>'reset_password','id'=>'reset_password','method'=>'post');
					echo form_open('passwordreset/',$attribute); 
					?>
			            <input type="hidden" name="user_id" value="<?php echo $user_id;?>" id="" >
						<div class="form-group mb-lg">						
							<label>New Password <span class="required_field">*</span></label>
							<div class="input-group input-group-icon">
								<input type="password" class="form-control input-lg" style="width:100%" name="new_password"  id="new_password"autocomplete="off" >
								<span class="input-group-addon">
									<span class="icon icon-lg">
										<i class="fa fa-lock"></i>
									</span>
								</span>
							</div>
						</div>
						<div class="form-group mb-lg">
							<div class="clearfix">
								<label class="pull-left"> Confirm Password <span class="required_field">*</span></label>
							</div>
							<div class="input-group input-group-icon">
								<input type="password" class="form-control input-lg" style="width:100%" name="confirm_password"  id="confirm_password" autocomplete="off" >
								<span class="input-group-addon">
									<span class="icon icon-lg">
										<i class="fa fa-lock"></i>
									</span>
								</span>
							</div>
						</div>
						<div class="row"> 
							<div class="col-sm-4 accblk">
								<input type="submit"  class="acc-commbtn btncls" name="Save" id="Save" value="Save"></center>
							</div>
						</div>
					<?php echo form_close();?>
				</div>
			</div>
	    </div>
  	</div>
</section>
<!-- footer -->
<?php $this->load->view('front/site_intro');?>
<?php $this->load->view('front/sub_footer');?>

<script type="text/javascript">
/* form validation*/
         $("#reset_password").validate({
	          rules: {
				new_password: {
                    required: true,
					minlength: 6
                },
				confirm_password: {
                    required: true,
					minlength: 6,
					equalTo:'#new_password'
                }
			},
            messages: {
				new_password: {
                    required: "Please enter the password.",
					minlength: "Passwords must be minimum 6 characters."    
                },
				confirm_password: {
                    required: "Please confirm your password.",
					minlength: "Passwords must be minimum 6 characters.",
					equalTo : "Please enter the same password."
                    
                }
			}
				
        });
</script>
