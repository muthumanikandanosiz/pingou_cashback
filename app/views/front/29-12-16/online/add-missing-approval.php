<?php $this->load->view('front/header'); ?>
<!-- header content End -->
<style type="text/css">
  .table-responsive {
    overflow-x: unset;
}
  .error
{
  color:#ff0000;
}
.required_field
{
 color:#ff0000;
}
.error_b {
    border: 1px solid #dd4b39 !important;
}
#errors_set
{
  color:red;
}
  .btn-file > input {
    cursor: pointer;
    direction: ltr;
    font-size: 23px;
    height: 100%;
    margin: 0;
    opacity: 0;
    position: absolute;
    right: 0;
    top: 0;
    width: 100%;
}
.btn-blue {
    background: #3da0d5 none repeat scroll 0 0;
    color: #fff;
}
.butn_clr_danger
{
  background: #ff4242 none repeat scroll 0 0;
  border-radius: 20px;
  color: #fff;
  font-weight: 500;
  margin-bottom: 0;
  padding: 5px 15px;
}
.newbackclr
{
  background: #f5f5f5 none repeat scroll 0 0 !important;  
  border-radius: 15px;
}

</style>
<!-- Main Content start -->
<?php $user_id = $this->session->userdata('user_id');?>

<section class="cms wow fadeInDown">
  <div class="container">
    <div class="heading wow bounceIn">
      <h2>
      faltando <span> Aprovação</span>
      </h2>
      <div class="heading_border_cms">
        <span>
          <img src="<?php echo $this->front_model->get_img_url();?>front/new/images/top_drop.png">
        </span>
      </div>
    </div>
    <div class="myac">
      <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1" id="hide_fn">
        <div class="my_account my_accblk">
          <div class="myacc-maintab">
            <!-- Nav tabs -->
            <?php $this->load->view('front/user_menu'); ?>
            <!-- <ul class="data-drop wow fadeInDown list-inline list-unstyled cls-greybgmenublk" id="dataSubMain" role="tablist">
              <li class="active"> <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Dados do Perfil</a> </li>
              <li> <a href="#payment" aria-controls="payment" role="tab" data-toggle="tab">Dados de Pagamento</a> </li>
              <li> <a href="#password" aria-controls="password" role="tab" data-toggle="tab">Mudar Senha</a> </li>
              <li> <a href="#notify" aria-controls="notify" role="tab" data-toggle="tab">Notificação</a> </li>
            </ul> -->
          </div>
          <!-- Tab panes -->
          <div class="tab-content"><br>
            
            <?php 
            $error = $this->session->flashdata('error');
            if($error!="")
            {
              echo '<div class="alert alert-warning">
              <button data-dismiss="alert" class="close">x</button>
              '.$error.'</div>';
            }
            $success = $this->session->flashdata('success');
            if($success!="")
            {
              echo '<div class="alert alert-success">
              <button data-dismiss="alert" class="close">x</button>
              '.$success.'</div>';      
            }
            ?>

            <div role="tabpanel" id="acc2" class="newbackclr">
             
              <div class="clearfix wow fadeInDown">
                <div class="col-md-12 col-sm-12">
                 <br>
                 <h4 class="mar-bot20">faltando Aprovação</h4>
                  <br>
                  <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                              <tr>
                                <th>Transaction Date</th>
                                <th>Store</th>
                                <th>Cashback</th>
                                <th>Transaction Amount</th>
                                <th>Status</th>
                              </tr>
                                <?php
                                if($results!='')
                                {
                                  $kss=1;
                                  foreach($results as $res)
                                  {
                                    ?>
                                    <tr>
                                      <td><?php echo date("d-m-Y",strtotime($res->transaction_date));?></td>
                                      <td><?php echo $res->affiliate_id;?></td>
                                      <td>R$ <?php echo $this->front_model->currency_format($res->transaction_amount);?></td>
                                      <td>R$ <?php echo $this->front_model->currency_format($res->cashback_amount);?></td>
                                      <td><a href="#" class="butn_clr_danger"> cancelado </a></td>
                                      <?php
                                      $getadmindetails = $this->front_model->getadmindetails();
                                      $site_logo = $getadmindetails[0]->site_logo;
                                      ?>
                                    </tr>
                                    <?php            
                                    $kss++;
                                  }
                                }
                              ?>
                    </table>
                   </div>
                    <br>
                    <div class="col-sm-10 fn center-block">
                    <form class="form-horizontal serial_missing_approval" id="serial_missing_approval" enctype="multipart/form-data" action="<?php echo base_url();?>missing-approval-submit" method="post" onSubmit="return serial_missing_approval_submit()"   role="form">
                              <div class="panel-group">
                                <fieldset>
                                  <!-- Text input-->
                                  <div class="form-group">
                                    <!-- <label class="col-sm-3 control-label" for="textinput">Nº do Pedido <span class="clr-red fnt-sz-13">* </span> </label> -->
                                    <div class="col-sm-12">
                                      <input id="transaction_reference"  placeholder="Nº do Pedido"  class="form-control error_b" type="text" value="" name="transaction_reference">
                                      <p class="help-block"><em>Comentário</em></p>
                                    </div>
                                  </div>

                                  <input type="hidden" name="store" value="<?= $res->affiliate_id;?>">
                                  <input type="hidden" name="transaction_amount" value="<?= $res->transaction_amount;?>">
                                  <input type="hidden" name="ordervalue" value="<?= $res->transaction_amount;?>">
                                  <input type="hidden" name="cashback_reference" value="<?= $res->reference_id;?>">

                                  <!-- Text input-->
                                  <div class="form-group">
                                    <input type="hidden" name="coupon_id" id="coupon_id" value="0"><!-- ALTEREI INSERI-->
                                    <!-- <label class="col-sm-3 control-label" for="textinput">Cupom utilizado</label> -->
                                    <div class="col-sm-12">
                                      <input id="coupon_used" type="text" class="form-control" placeholder="Cupom utilizado"  name="coupon_used">
                                    </div>
                                  </div>

                                  <!-- Text input-->
                                  <div class="form-group">
                                    <div class="col-sm-12">
                                      <input id="ssn" maxlength="54" class="form-control detail error_b" name="details" placeholder="____-____-____-____-____-____-____-____-____-_____-_____"></input> 
                                      <p class="help-block"><em> Nº de 44 dígitos. As lojas costumam enviar a NFE por email. </em></p>
                                    </div>
                                  </div>

                                  <!-- Text input-->
                                  <div class="form-group">
                                    <!-- <label class="col-sm-3 control-label" for="textinput">Anexar arquivo:</label> -->
                                    <div class="col-sm-7">
                                      <span class="btn btn-blue btn-file">
                                      <span class="fileupload-new">Anexar Nota Fiscal</span>
                                        <input id="ticket_attachment" class="col-md-4" type="file" value="" name="ticket_attachment" onchange="CheckFileType(this)" accept="image/*">
                                      </span>
                                      <span id="errors_set"></span>
                                    </div>
                                  </div>

                                  <!-- New code for phone number details 3-5-16 -->
                                  <div class="form-group"> 
                                    <div class="col-sm-12">
                                      <input id="phone_no" onkeypress="return isNumber(event)" type="text" class="form-control" placeholder="Phone number to contact"  name="phone_no">
                                    </div>
                                  </div>
                                  <!-- End -->
                            
                                  <div class="form-group">
                                    <div class="col-sm-12">
                                      
                                         Some content goes here 
                                      
                                    </div>
                                  </div>
                                    
                                  <div class="clearfix mar-bot">
                                    <div class="colmd-12">
                                      <div class="checkbox">
                                        <label>
                                          <input id="terms_conditions" class="error_b" type="checkbox" value="Yes" name="terms_conditions">Have read and understood the terms and conditions.
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                  
                                  <div class="form-group">
                                    <div class="col-sm-12">
                                      <button type="submit" name="save" value="save" onClick="return serial_missing_approval_submit();" class="btn btn-blue">Submit</button>
                                    </div>
                                  </div>
                                </fieldset>
                                <input type="hidden" name="hid_click_id" value="0" id="hid_click_id"></input>
                              </div>
                    </form>
                  </div>
                </div>
              </div>
              <br>
              <!--account table section ends-->
            </div>
            <br>
            <?php $this->load->view('front/my_earnings.php')?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  
<!-- Main Content end -->

<!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
<!-- Footer menu start -->
<?php //$this->load->view('front/sub_footer');?>

<?php $admindetails = $this->front_model->getadmindetails_main();?>
<footer>
  <!-- ========================== -->
  <!-- FOOTER - FOOTER -->
  <!-- ========================== -->
  <section class="footer-section innerfoot-sect">
    <div class="container">
      <div class="clearfix wow fadeInDown">
      <div class="wow fadeInDown">
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Help And Support</h5>
          <ul class="footer-nav">
            <li><a href="<?php echo base_url();?>contato"><i class="fa fa-caret-right"></i> Contact Us</a></li>
            <li><a href="<?php echo base_url(); ?>faq"><i class="fa fa-caret-right"></i> FAQ</a></li>
            <?php 
            $result = $this->front_model->sub_menu();
            foreach($result as $view)
            {
            ?>
              <li><a href="<?php echo base_url(); echo $view->cms_title; ?>"><i class="fa fa-caret-right"></i> <?php echo $view->cms_heading;?></a></li>
            <?php 
            }
            ?>
          </ul>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Email Newsletter</h5>
          <ul class="footer-nav">
            <li><a href="#"><i class="fa fa-caret-right"></i> Column Two Item</a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Top Categories</h5>
          <ul class="footer-nav">
            <?php 
            $categories = $this->front_model->get_all_categories(8);
            foreach($categories as $view)
            {
            ?> 
            <li><a href="<?php echo base_url();?>products/<?php echo $view->category_url;?>"><i class="fa fa-caret-right"></i> <?php echo $view->category_name;?></a></li>
            <?php
            }
            ?>
          </ul>
        </div>
         <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Top Stores</h5>
          <ul class="footer-nav">
          <?php 
          $stores = $this->front_model->get_all_stores(8);
          foreach($stores as $view)
          {
          ?>
            <li><a href="<?php echo base_url(); ?>cupom-desconto/<?php echo $view->affiliate_url?>"><i class="fa fa-caret-right"></i> <?php echo $view->affiliate_name; ?></a></li>
          <?php
          }
          ?>
          </ul>
        </div>
      </div>
      </div>
    </div>
  </section>
  <section class="copyright-section">
    <div class="container">
     <div class="row wow fadeInDown">
      <div class="col-md-6 col-sm-6 col-xs-12 wow flipInX">
        <p>Copyright &copy; <?php echo date('Y')?> <?php echo $admindetails->site_name;?>  |  All Rights Reserved </p>
      </div>
      <?php
      $listing = $this->db->query("select * from admin")->row();
      ?>
      <div class="col-md-6 wow flipInX">
        <ul class="list-socials pull-right">
          <?php if($listing->admin_twitter) {?> <li><a href="<?php echo $listing->admin_twitter;?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
          <?php if($listing->admin_fb)      {?> <li><a href="<?php echo $listing->admin_fb;?>">     <i class="fa fa-facebook"></i></a></li><?php } ?>
          <?php if($listing->admin_gplus)   {?> <li><a href="<?php echo $listing->admin_gplus;?>">  <i class="fa fa-google-plus"></i></a></li><?php } ?>
        </ul>
      </div>
     </div>
    </div>
  </section>
</footer>
</body>
</html>


<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/jquery.1.11.1.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/bootstrap.min.js"></script>
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/owl-carousel.js"></script>
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/jquery-ui.js"></script>
<!-- Scripts queries -->
 
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/jquery.validate.js"></script>
<script>
  if (top.location != location) 
  {
      top.location.href = document.location.href ;
   }
    
    $(function(){
      window.prettyPrint && prettyPrint();
    });

</script>

<script type="text/javascript">

/* form validation*/

 $(document).ready(function() {

         $("#serial_missing_approval").validate({

            rules: {

        transaction_reference: {

                    required: true

                },

        coupon_used: {

                    required: true

                },

        ssn: {

                    required: true

          

                },
/*
        ticket_attachment: {

                    required: true


                },
*/
        phone_no: {

                    required: true
},

terms_conditions: {

                    required: true
},


},

            messages: {

        transaction_reference: {

                    required: "Please enter Transaction Reference."                    

                },

        coupon_used: {

                    required: "Please enter the Coupon Id."

                    

                },

        ssn: {

                    required: "Please enter details."

                    

                },
/*
        ticket_attachment: {

                    required: "Please attach the reference file.",
   

                },
                */

        phone_no: {

                    required: "Please enter Phone Number.",

           },

           terms_conditions: {

                    required: "Please accept our policy.",

           },

}

        

        });

});

  


function final_step(stepid)
{
  $('#hid_click_id').val(stepid);
  $('#err_hidden').val(3);
  $("#collapse-3").attr('class', 'panel-collapse collapse');  
  $("#collapse-4").attr('class', 'panel-collapse collapse in'); 
  $("#collapse-4").attr('style', ''); 
}

function CheckFileType(ctlFileName) {
    strFileName = ctlFileName.value;
    var strFN = new String(strFileName);
    var aryFN = Array();
    aryFN = strFN.split(".");
    var strExt = new String(aryFN[aryFN.length-1]);
    strExt = strExt.toLowerCase();
    if (strExt !== 'pdf' && strExt !== 'png' && strExt !== 'jpg' && strExt !== 'jpeg'
        && strExt !== 'gif' && strExt !== 'pdf') {
        ctlFileName.value='';
        show_errors(ctlFileName,'Selecione uma imagem (jpg, png)');
        return false;
    }
    
    // 5 MB  = 5242880
    if(ctlFileName.files[0].size > 5242880) {
      ctlFileName.value='';
      show_errors(ctlFileName,'Esse arquivo é muito grande, o máximo suportado é 5mb.');
      return false;
    }
    
    hide_errors(ctlFileName);
    return true;
}   
function show_errors(obj,error){
  //$("#errors_set").addClass('error_b');
  $("#errors_set").html(error);
}
function hide_errors(obj){
 // $("#errors_set").removeClass('error_b');
  $("#errors_set").html('');
}


function serial_missing_approval_submit()
{
  var err_hidden = $('#err_hidden').val();
  if(err_hidden==3)
  {
    $("#serial_missing_approval").submit();
    return true;
  }
  else
  {
    $("#collapse-4").attr('class', 'panel-collapse collapse');  
    $("#collapse-1").attr('class', 'panel-collapse collapse in');
    $("#collapse-1").attr('style', '');     
    ShowTransactionDateError();
    return false;
  }
}
 /*Number format with - symbol after 4 digits and phone number field validation 3-5-16*/ 
  $('#ssn').keyup(function() {
        var val = this.value.replace(/\D/g, '');
        var newVal = '';
        while (val.length > 4) {
          newVal += val.substr(0, 4) + '-';
          val = val.substr(4);
        }
        newVal += val;
        this.value = newVal;
    });

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

/*End code*/

  
  </script>
