<?php $this->load->view('front/header'); ?>
<!-- header content End -->
<style type="text/css">
	.btn-file > input {
    cursor: pointer;
    direction: ltr;
    font-size: 23px;
    height: 100%;
    margin: 0;
    opacity: 0;
    position: absolute;
    right: 0;
    top: 0;
    width: 100%;
}
.btn-blue {
    background: #3da0d5 none repeat scroll 0 0;
    color: #fff;
}
 
.error
{
	color:#ff0000 !important;
}
.required_field
{
 color:#ff0000 !important;
}
.error_b {
    border: 1px solid #dd4b39 !important;
}
#errors_set
{
	color:red !important;
}
</style>


<!-- Main Content start -->

<section class="cms wow fadeInDown">
  	<div class="container">
   		<div class="heading wow bounceIn">
      		<h2>Adicionar <span>um Ticket </span></h2>
	    	<div class="heading_border_cms">
	    		<span>
	    			<img src="<?php echo $this->front_model->get_img_url();?>front/new/images/top_drop.png">
	    		</span>
	    	</div>
    	</div>
    	<div class="about my_account my_accblk">
      		<div class="wow fadeInDown resc-mainbuyblk">
		        <h2>Seu dinheiro de volta não apareceu?</h2>
		        <p>As vezes pode acontecer de a loja não nos avisar da sua compra. Mas deixa que a gente resolve isso! Adicione uma solicitação que vamos entrar em contato com a loja e recuperar sua compra.</p>
      		</div>
      		<br>
      		<br>
      		<?php 
			$error = $this->session->flashdata('error');
			if($error!="")
			{
				echo '<div class="alert alert-danger">
				<button data-dismiss="alert" class="close">x</button>
				'.$error.'</div>';
			}
			$success = $this->session->flashdata('success');
			if($success!="")
			{
				echo '<div class="alert alert-success">
				<button data-dismiss="alert" class="close">x</button>
				'.$success.'</div>';
			}
			?>
	     	<div class="row wow fadeInDown">
	        	<form class="form-horizontal serial_missing_cashback" id="serial_missing_cashback" enctype="multipart/form-data" action="missing-Cashback-submit" method="post" onSubmit="return serial_missing_cashback_submit()" role="form">
	          		<div class="accblk rescuepurchaseblk fn center-block col-md-10 col-sm-10 col-xs-12">
			            <div class="comm-greybg clearfix">
                    <div class="col-sm-6 col-xs-12">
			              	<div class="form-group newerr">
				                <label for="inputEmail3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> date of purchase </label>
				                <div class="col-sm-12 col-xs-12">
				                  	<div class="cls_post_subhead">
				                    	<div class='input-group date'>  <!--id='datetimepicker1'-->
				                      		<input type='text' value="<?php echo date("d/m/Y");?>" data-date-format="dd/mm/yyyy" id="dp2" class="form-control" />
				                      		<span class="input-group-addon">
				                      			<span class="glyphicon glyphicon-calendar"></span>
				                      		</span>
				                      	</div>
				                  	</div>
				                </div>
			              	</div>
			              </div>
			              <span class="err"></span>
			              <div class="col-sm-6 col-xs-12">
			              	<div class="form-group">
			                	<label for="inputEmail3" class="col-sm-12 col-xs-12 control-label comm-acclabel"> store name </label>
			                	<div class="col-sm-12 col-xs-12">
			                  		<div class="clearfix">
			                    		<div class="select-style3">		<!-- id="cd-dropdown" class="cd-select" -->
			                      			<select onChange="admSelectCheck(148);" id="store" name="store" class="cd-select"><!-- id="store" -->
						                    	<option id="admOption" value="">--Selecione--</option>
						                    </select>
			                    		</div>
			                  		</div>
			                	</div>
			              	</div>
			              </div>
			              <input type="hidden" name="report_date" id="report_dates" value="">
			              <span id="haihai" style="display:none;">
			            	  <b>This store can take upto 10 days to give us the information about your purchase.<br>They are still on time. check the date on the <a href="<?php echo base_url()?>/saidas-para-loja">Click history page</a></b>
			              </span>
                    
			              <div class="" id="admDivCheck" style="display:none; padding-top:110px;">
                      <div class="panel-heading" id="collapse-4">
                        <p class="panel-title">Só precisamos de mais alguns detalhes</p>
                        <div class="panel-body">
                          <fieldset>
                            <div class="form-group">
                              <!-- <label class="col-sm-3 control-label" for="textinput">Nº do Pedido <span class="clr-red fnt-sz-13">* </span> </label> -->
                              <div class="col-sm-9">
	                              <input id="transaction_reference"  placeholder="Nº do Pedido"  class="form-control error_b" type="text" value="" name="transaction_reference">
	                              <p class="help-block"><em>Comentário</em></p>
                              </div>
                            </div>
                            <div class="form-group">
                            <!-- <label class="col-sm-3 control-label" for="textinput">Valor total do pedido <span class="clr-red fnt-sz-13">* </span></label> -->
                              <div class="col-sm-9">
                                <input id="ordervalue" placeholder="Valor total do pedido"  class="form-control error_b" type="text" value="" name="ordervalue">
                                <p class="help-block"> <em> Sem o valor do frete </em></p>
                              </div>
                            </div>
                            <div class="form-group">
                              <input type="hidden" name="coupon_id" id="coupon_id" value="0">
                              <!-- <label class="col-sm-3 control-label" for="textinput">Cupom utilizado</label> -->
                              <div class="col-sm-9">
                                <input id="coupon_used" type="text" class="form-control" placeholder="Cupom utilizado"  name="coupon_used">
                              </div>
                            </div>
                            <div class="form-group">
	                          <!-- <label class="col-sm-3 control-label" for="textinput">Nº da Nota Fiscal Eletrônica <span class="clr-red fnt-sz-13">* </span></label> -->
	                            <div class="col-sm-9">
	                              <!-- Number format seperated by - ymbol after 4 digits 3-6-16 -->
		                            <input type="text" id="ssn" maxlength="54" id="ssn" class="form-control detail error_b" name="details" placeholder="____-____-____-____-____-____-____-____-____-_____-_____"> 
		                            <p class="help-block">
		                              <em> Nº de 44 dígitos. As lojas costumam enviar a NFE por email. </em>
		                            </p>
	                            </div>
                            </div>
                            <div class="form-group">
	                          <!-- <label class="col-sm-3 control-label" for="textinput">Anexar arquivo:</label> -->
	                            <div class="col-sm-7">
	                              <span class="btn btn-blue btn-file">
	                              <span class="fileupload-new">Anexar Nota Fiscal</span>
	                                <input id="ticket_attachment" class="col-md-4" type="file" value="" name="ticket_attachment" onchange="CheckFileType(this)" accept="image/*">
	                              </span>
	                              <span id="errors_set"></span>
	                            </div>
                            </div>
                            <!-- New code for phone number details 3-5-16 -->
                            <div class="form-group"> 
                              <div class="col-sm-9">
                                <input id="phone_no" onkeypress="return isNumber(event)" type="text" class="form-control" placeholder="Phone number to contact"  name="phone_no">
                              </div>
                            </div>
                            <!-- End -->
                            <div class="form-group">
                              <div class="col-sm-10 col-sm-offset-1">
	                              <ul class="list">
		                              <li>Retailers do not accept missing cashbacks older than 10 days</li>
		                              <li>Missing Cashback Tickets may take 30-45 days to get resolved and show in your Cashback Account</li>
		                              <li>Incorrect Information may lead to Cancellation of Cashback. Please do not raise multiple tickets for the same transaction. This may also lead to the Retailer declining the enquiry</li>
		                              <li>Cashback added from a Missing ticket may take more than 90 days to Confirm.</li>
		                              <li>Your Cashback may be updated at the time of confirmation</li>
		                              <li>While we shall try our best to resolve and get your missing cashback approved the retailer's decision is final</li>
	                              </ul>
	                            </div>
                            </div>
                            <div class="form-group">
                              <div class="colmd-11 col-md-offset-1">
                                <div class="checkbox">
                                  <label>
                                    <input id="terms_conditions" class="error_b" type="checkbox" value="Yes" name="terms_conditions">Have read and understood the terms and conditions.
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-10">
                                <button type="submit" name="save" value="save" onClick="return serial_missing_cashback_submit();" class="btn btn-blue">Submit</button>
                              </div>
                            </div>
                          </fieldset>
                          <input type="hidden" name="hid_click_id" value="0" id="hid_click_id">
                        </div>
                      </div>
                    </div>
			            </div>
                </div>  
        		</form>
      		</div>
	      	<?php $this->load->view('front/my_earnings');?>
    	</div>
  	</div>
</section>
<input type="hidden" name="err_hidden" id="err_hidden" value="<?php echo date("d/m/Y");?>">
<!-- Main Content end -->

 <!-- Pop up pages start -->
<?php $this->load->view('front/site_intro'); ?> 
<!-- Popup End -->
 
<!-- Footer menu start -->
<?php //$this->load->view('front/sub_footernew');?>
<?php $admindetails = $this->front_model->getadmindetails_main();?>
<footer>
  <!-- ========================== -->
  <!-- FOOTER - FOOTER -->
  <!-- ========================== -->
  <section class="footer-section innerfoot-sect">
    <div class="container">
      <div class="clearfix wow fadeInDown">
      <div class="wow fadeInDown">
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Help And Support</h5>
          <ul class="footer-nav">
            <li><a href="<?php echo base_url();?>contato"><i class="fa fa-caret-right"></i> Contact Us</a></li>
            <li><a href="<?php echo base_url(); ?>faq"><i class="fa fa-caret-right"></i> FAQ</a></li>
            <?php 
            $result = $this->front_model->sub_menu();
            foreach($result as $view)
            {
            ?>
              <li><a href="<?php echo base_url(); echo $view->cms_title; ?>"><i class="fa fa-caret-right"></i> <?php echo $view->cms_heading;?></a></li>
            <?php 
            }
            ?>
          </ul>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Email Newsletter</h5>
          <ul class="footer-nav">
            <li><a href="#"><i class="fa fa-caret-right"></i> Column Two Item</a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Top Categories</h5>
          <ul class="footer-nav">
            <?php 
            $categories = $this->front_model->get_all_categories(8);
            foreach($categories as $view)
            {
            ?> 
            <li><a href="<?php echo base_url();?>products/<?php echo $view->category_url;?>"><i class="fa fa-caret-right"></i> <?php echo $view->category_name;?></a></li>
            <?php
            }
            ?>
          </ul>
        </div>
         <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Top Stores</h5>
          <ul class="footer-nav">
          <?php 
          $stores = $this->front_model->get_all_stores(8);
          foreach($stores as $view)
          {
          ?>
            <li><a href="<?php echo base_url(); ?>cupom-desconto/<?php echo $view->affiliate_url?>"><i class="fa fa-caret-right"></i> <?php echo $view->affiliate_name; ?></a></li>
          <?php
          }
          ?>
          </ul>
        </div>
      </div>
      </div>
    </div>
  </section>
  <section class="copyright-section">
    <div class="container">
     <div class="row wow fadeInDown">
      <div class="col-md-6 col-sm-6 col-xs-12 wow flipInX">
        <p>Copyright &copy; <?php echo date('Y')?> <?php echo $admindetails->site_name;?>  |  All Rights Reserved </p>
      </div>
      <?php
      $listing = $this->db->query("select * from admin")->row();
      ?>
      <div class="col-md-6 wow flipInX">
        <ul class="list-socials pull-right">
          <?php if($listing->admin_twitter) {?> <li><a href="<?php echo $listing->admin_twitter;?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
          <?php if($listing->admin_fb)      {?> <li><a href="<?php echo $listing->admin_fb;?>">     <i class="fa fa-facebook"></i></a></li><?php } ?>
          <?php if($listing->admin_gplus)   {?> <li><a href="<?php echo $listing->admin_gplus;?>">  <i class="fa fa-google-plus"></i></a></li><?php } ?>
        </ul>
      </div>
     </div>
    </div>
  </section>
</footer>
<!-- Footer Menu End--> 
<!-- Scripts queries --> 
<?php //$this->load->view('front/js_scripts');?>

<script src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/jquery.1.11.1.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/bootstrap.min.js"></script>
<script src="<?php echo $this->front_model->get_css_js_url(); ?>front/js/owl-carousel.js"></script>
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/jquery-ui.js"></script>
<!-- Scripts queries --> 
<script src="<?php echo $this->front_model->get_css_js_url();?>front/js/jquery.validate.js"></script>	 

<!-- New code for popup details 19-11-16 -->
<?php include('popup_details.php'); ?>
<!-- End 19-11-16 -->
<!-- Add a ticket page date and storename releated scripts Start 17-8-16-->

<script>
if (top.location != location) 
{
  top.location.href = document.location.href ;
}
$(function(){
  window.prettyPrint && prettyPrint();
});

var changed_date = null;
$(document).ready(function() {    
  $('#ordervalue').keyup(function(e) {      
    if(e.keyCode != 9) {
      var acc_no = $('#ordervalue').val();
      patt=/^[0-9\,]*$/i;
      result = patt.test(acc_no);
      result == false ? $('#ordervalue').val(acc_no.replace(/[^0-9\,]/ig,'')) : null;
    }
  });  
});
 
$(function() 
{ 
  $("#dp2").datepicker({
    maxDate: new Date(), 
    minDate: new Date(2013, 6, 12),
    dateFormat: 'dd-mm-yy',
    onSelect: function(dateText, inst) 
    {
      // alert(dateText);
      if(dateText >= (new Date() - 3*24*60*60*1000)) 
      {
        alert('Please allow upto 72 hours for your cashback to track. If it does not track in your <?php $admindetails = $this->front_model->getadmindetails_main(); echo $admindetails->site_name; ?> account within 72 hours, you should raise a Missing Cashback Ticket. Our customer care team shall help resolve this for you at the earliest.');
        $('#store').empty();
        $('#store').append($('<option>', { value: "", text : "--Select--" }));
        //$("#details_section").children("ul.child").remove();
        // $('#exit_id').val('');
        return false;
      }  
      hideTransationerror();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>cashback/ajaxcall",
        data :'date='+dateText,
        cache: false,
        dataType:"JSON",
        success: function(result)
        {
          if(result!="")
          {
            $('#err_hidden').val(1);
            var s=0;
            $.each(result, function() {
              var items = result[s];      
              $("#store").append("<option value=\""+items['store_name']+"\">"+items['store_name']+"</option>");
              s++;
            });
            /*$("#collapse-1").attr('class', 'panel-collapse collapse'); ALTEREI exclui */
            /*$("#collapse-2").attr('class', 'panel-collapse collapse in');ALTEREI exclui */          
            return false;
          }
          else
          {
            ShowTransactionDateError();
            $('#err_hidden').val(0);
            return false;
          }             
        }
      });
    } 
  });
  $( document ).ajaxStart(function() {
  $('#page_loading').show();
  });
});
  
function ShowTransactionDateError() 
{
  $("#dp2").next('p.error').remove();
  $(".newerr").after("<p class='txt error'>Ops.. Não vimos nenhum clique da sua conta nesta data. Por favor selecione uma data válida</p>");
  $('#store').empty();
  $('#err_hidden').val(0);
  $('#store').append($('<option>', { value: "", text : "--Select--" }));
  $('#tbl1 tr:not(:first)').remove();
}

function hideTransationerror()
{
  $(".newerr").next('p.error').remove();
}
/*ALTEREI inseri a função abaixo*/
function admSelectCheck(nameSelect)
{
    /*New code for store details 4-5-16*/
  var store_name = $('#store').val();
  $.ajax ({
      type: "POST",
      url: "<?php echo base_url();?>cashback/storedetail",
      data :'storename='+store_name,
      cache: false,
      dataType:"JSON",
      success: function(result)    /*faz aparecer o calendario*/
      {   
        //alert(result);
        /*Report date */
        var newnew      = result[0]['report_date'];
        //alert(newnew);
        document.getElementById('report_dates').value = newnew;
        /*Report date end*/

        /*purchase Date*/
        var strdate    = $('#dp2').val();
        /*puchase date end*/

        /*Change a date format into Y-m-d format*/
        var d    = strdate.slice(0, 10).split('-');   
        var newd = d[2] +'-'+ d[1] +'-'+ d[0];
        
        /*today date*/
          var d = new Date();
          var month = d.getMonth()+1;
          var day = d.getDate();
          var today = d.getFullYear() + '-' +
          ((''+month).length<2 ? '0' : '') + month + '-' +
          ((''+day).length<2 ? '0' : '') + day;

          /*convert a stringtotime format in today date*/
          var todaydate = Date.parse(today);
          /*End*/
        /*today date end*/
        
        var someDate    = new Date(newd);
        var newnew      = parseInt(newnew);
        someDate.setDate(someDate.getDate()+newnew); //number  of days to add
        var dateFormated = someDate.toISOString().substr(0,10);
        //alert(dateFormated);
        var expirydate = Date.parse(dateFormated);
       

        //alert(todaydate);
         //alert(expirydate);
        /*New code for date comparision*/
          if(todaydate >= expirydate)
          {

            if(nameSelect)
            {
              admOptionValue = document.getElementById("admOption").value;
              if(admOptionValue != nameSelect.value)
              {
                document.getElementById("admDivCheck").style.display = "block";
                $('#err_hidden').val(3);
                $('#hid_click_id').val(nameSelect);
              }
              else
              {
                document.getElementById("admDivCheck").style.display = "none";
              }
            }
            else
            {
              document.getElementById("admDivCheck").style.display = "none";
            } 
          }
          else
          {
            $("#haihai").show();
          } 
          /*end*/


      }
      });   
 
}

function get_click_details_ajax(click_store)
{
  var dateText = $("#dp2").val();
  $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>cashback/get-clicked-store-details",
            data :'date='+dateText+'&click_store='+click_store,
            cache: false,
            success: function(result)    /*faz aparecer o calendario*/
            {
              if(result!="")
              {
                $('#err_hidden').val(3);
                 $('#tbl1 tr').last().after(result);
                /*$("#collapse-2").attr('class', 'panel-collapse collapse');ALTEREI removi*/
                $("#collapse-3").attr('class', 'panel-collapse collapse in'); 
                $("#collapse-3").attr('style', ''); 
                
                return false;
              }
              else
              {
                ShowTransactionDateError();
                $('#err_hidden').val(0);
                return false;
              }             
            }
          });
}

function final_step(stepid)
{
  $('#hid_click_id').val(stepid);
  $('#err_hidden').val(3);
  $("#collapse-3").attr('class', 'panel-collapse collapse');  
  $("#collapse-4").attr('class', 'panel-collapse collapse in'); 
  $("#collapse-4").attr('style', ''); 
}

function CheckFileType(ctlFileName) {
    strFileName = ctlFileName.value;
    var strFN = new String(strFileName);
    var aryFN = Array();
    aryFN = strFN.split(".");
    var strExt = new String(aryFN[aryFN.length-1]);
    strExt = strExt.toLowerCase();
    if (strExt !== 'pdf' && strExt !== 'png' && strExt !== 'jpg' && strExt !== 'jpeg'
        && strExt !== 'gif' && strExt !== 'pdf') {
        ctlFileName.value='';
        show_errors(ctlFileName,'Selecione uma imagem (jpg, png)');
        return false;
    }
    
    // 5 MB  = 5242880
    if(ctlFileName.files[0].size > 5242880) {
      ctlFileName.value='';
      show_errors(ctlFileName,'Esse arquivo é muito grande, o máximo suportado é 5mb.');
      return false;
    }
    
    hide_errors(ctlFileName);
    return true;
}   
function show_errors(obj,error){
  //$("#errors_set").addClass('error_b');
  $("#errors_set").html(error);
}
function hide_errors(obj){
 // $("#errors_set").removeClass('error_b');
  $("#errors_set").html('');
}


function serial_missing_cashback_submit()
{
  
  var err_hidden = $('#err_hidden').val();
  //alert(err_hidden);
  if(err_hidden==3)
  {
    $("#serial_missing_cashback").submit();
    return true;
  }
  else
  { 
    $("#collapse-4").attr('class', 'panel-collapse collapse');  
    $("#collapse-1").attr('class', 'panel-collapse collapse in');
    $("#collapse-1").attr('style', '');     
    ShowTransactionDateError();
    return false;
  }
}
 /*Number format with - symbol after 4 digits and phone number field validation 3-5-16*/ 
  $('#ssn').keyup(function() {
        var val = this.value.replace(/\D/g, '');
        var newVal = '';
        while (val.length > 4) {
          newVal += val.substr(0, 4) + '-';
          val = val.substr(4);
        }
        newVal += val;
        this.value = newVal;
    });

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

/*End code*/
</script>
<!-- Add a ticket page date and storename releated scripts End 17-8-16-->
</body>
</html>