<?php $this->load->view('front/header');?>
<!-- Header ends here -->

<div class="wrap-top">
  <div id="content">
	<?php
		$complete = $this->session->flashdata('complete');
		if($complete!="") {
		echo '<div class="alert alert-success">
		<button data-dismiss="alert" class="close">x</button>
		<strong>'.$complete.'</strong></div>';
	} ?>
    <div class="container">

      <section class="contact-section section section-on-bg">
        <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        
        <h3 class="heading text-center"> Thank You </h3>
        
        <?php 
        $activate = $admindetails->activate_method;
        if($activate == 1) 
        { 
         ?> 
         <div class="privacy">
            <p>Your registration is now completed Successfully!.</p>
            <ul>
              <li>Your account is activated Successfully.</li>
            </ul>
            <br>
        </div>
        <?php
        }
        else
        {
        ?>
        <div class="privacy">
            <p>Your registration is now completed Successfully!.</p>
            <ul>
              <li>A mail has been sent to your email address.</li>
              <li>Click the activation link in your mail to activate your account.</li>
              
            </ul><br>
        </div>
        <?php
        }
        ?>
        </div> 
        </div> 
        </section>
    </div>
  </div>
</div><br><br>
<?php 
$this->load->view('front/site_intro');
$this->load->view('front/sub_footer');
?>