<?php $admindetails = $this->front_model->getadmindetails_main();
$user_id     = $this->session->userdata('user_id');
$userdetails = $this->front_model->userdetails($user_id); 
?>
<footer>
  <!-- ========================== -->
  <!-- FOOTER - FOOTER -->
  <!-- ========================== -->
  <section class="footer-section innerfoot-sect">
    <div class="container">
      <div class="clearfix wow fadeInDown">
      <div class="wow fadeInDown">
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Help And Support</h5>
          <ul class="footer-nav">
            <li><a href="<?php echo base_url();?>contato"><i class="fa fa-caret-right"></i> Contact Us</a></li>
            <li><a href="<?php echo base_url();?>perguntas-frequentes"><i class="fa fa-caret-right"></i> Perguntas Frequentes</a></li>
            <?php 
            $result = $this->front_model->sub_menu();
            foreach($result as $view)
            {
            ?>
              <li><a href="<?php echo base_url();?><?php echo $view->cms_title; ?>"><i class="fa fa-caret-right"></i> <?php echo $view->cms_heading;?></a></li>
            <?php 
            }
            ?>
          </ul>
        </div>
        <?php 
        if($user_id== '')
        {
          ?>
          <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Email Newsletter</h5>
          <ul class="footer-nav">
            <li><a href="#"><i class="fa fa-caret-right"></i> Subscribe to recieve inspiration, ideas and news in your inbox.</a></li>
            <li>
              <div class="col-md-12 col-sm-12 col-xs-12 pd-left-0">
              <input type="text" id="news_email" onkeypress="clears(2);" class="form-control tbox1" placeholder="Email Address">
              <input type="button" class="tbtn1" value="send" onClick="email_news();">
              </div>
              &nbsp;
              <span id="new_msg" style="color:red"></span>
            </li>
          </ul>
          </div>
          <?php
        }
        ?>  


        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Top Categories</h5>
          <ul class="footer-nav">
            <?php 
            $categories = $this->front_model->get_all_categories(8);
            foreach($categories as $view)
            {
            ?> 
            <li><a href="<?php echo base_url();?>ofertas/<?php echo $view->category_url;?>"><i class="fa fa-caret-right"></i> <?php echo $view->category_name;?></a></li>
            <?php
            }
            ?>
          </ul>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInLeft pad-no">
          <h5>Top Stores</h5>
          <ul class="footer-nav">
          <?php 
          $stores = $this->front_model->get_all_stores(8);
          foreach($stores as $view)
          {
          ?>
            <li><a href="<?php echo base_url(); ?>cupom-desconto/<?php echo $view->affiliate_url?>"><i class="fa fa-caret-right"></i> <?php echo $view->affiliate_name; ?></a></li>
          <?php
          }
          ?>
          </ul>
        </div>
      </div>
      </div>
    </div>
  </section>
  <section class="copyright-section">
    <div class="container">
     <div class="row wow fadeInDown">
      <div class="col-md-6 col-sm-6 col-xs-12 wow flipInX">
        <p>Copyright &copy; <?php echo date('Y')?> <?php echo $admindetails->site_name;?>  |  All Rights Reserved </p>
      </div>
      <?php
      $listing = $this->db->query("select * from admin")->row();
      ?>
      <div class="col-md-6 wow flipInX">
        <ul class="list-socials pull-right">
          <?php if($listing->admin_twitter) {?> <li><a href="<?php echo $listing->admin_twitter;?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
          <?php if($listing->admin_fb)      {?> <li><a href="<?php echo $listing->admin_fb;?>">     <i class="fa fa-facebook"></i></a></li><?php } ?>
          <?php if($listing->admin_gplus)   {?> <li><a href="<?php echo $listing->admin_gplus;?>">  <i class="fa fa-google-plus"></i></a></li><?php } ?>
        </ul>
      </div>
     </div>
    </div>
  </section>
</footer>
<?php $this->load->view('front/js_scripts');?>
<?php 
 
//print_r($popup_templete);
$admin_number  = $admindetails->contact_number;
$site_logo     = $this->front_model->get_img_url()."uploads/adminpro/".$admindetails->site_logo;
$site_name     = $admindetails->site_name;

if($user_id == '')
{
  $popup_templete = $this->db->query("SELECT * from exit_popup where all_status=1")->row(); 
  $url            = base_url();
  $all_content    = $popup_templete->popup_template;
 

  $data = array(
  '###ADMINNO###'=>$admin_number,
  '###COMPANYLOGO###'=>$site_logo,
  '###SITE NAME###'=>$site_name,
  );
  
  $contents = strtr($all_content,$data);
  
  if($popup_templete->all_status == 1)
  {
    ?>
    <!-- <div class="modal fade cus_modal" id="myModal" counts="0" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none!important;">
      <div class="modal-dialog" style="width:604px !important;">
        <div class="modal-content" id="newcontent" style="height:545px;">
          <div class="modal-header">
            <button aria-hidden="true"  data-dismiss="modal" class="close cls_popup" type="button">×</button>
          </div>
          <div class="modal-body-default">
            <center> -->
            <?php 
              //echo $contents;
            /*New code for shortcut details content 26-5-17*/
            echo $this->front_model->shortcut_details($contents);//$newcontent  = strtr($contents,$short_data);
            /*End 26-5-17*/
            ?>
            <!-- </center>              
          </div>
        </div>
      </div>
    </div> -->
    <?php
  }   
}
?>

<!-- new code for First access popup details 20-3-17 -->
<?php 
$popup_templete    = $this->db->query("SELECT * from first_access_popup where all_status_firstpopup=1")->row(); 
$first_acc_content = $popup_templete->first_popup_template;

$datas = array(
  '###ADMINNO###'=>$admin_number,
  '###COMPANYLOGO###'=>$site_logo,
  '###SITE NAME###'=>$site_name,
  );

$first_acc_popup    = strtr($first_acc_content,$datas);
$first_popup_status = $popup_templete->all_status_firstpopup;

/*New code for shortcut details content 26-5-17*/
//$first_acc_popup  = strtr($first_acc_popup,$short_data);
/*End 26-5-17*/


if($user_id!='')
{
  if($first_popup_status == 1)
  {
    if($userdetails->first_ac_popup_status == 1)
    { 
      echo $this->front_model->shortcut_details($first_acc_popup);
      ?>
      <script type="text/javascript">
          
          $('#first_acc_popup').modal('show');  
          $("#first_acc_popup").on("click", function(e)
          {
            $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>cashback/update_firstpoup_satus",
            data: {'poup_status':0},
            cache: false,
            success: function(result)
            {
              if(result!=1)
              {
                return false;
              }
              else
              {
                <?php $redirect_urlset =  base_url(uri_string());?>
                //window.location.href = '<?php echo $redirect_urlset; ?>';
                return false;          
              }             
            }
            });
            return false;
          }); 
      </script>
      <style type="text/css">
        .modal-backdrop.in 
        {
          display: block !important;
        }
      </style>
      <?php 
      $data = array(
        'first_ac_popup_status' => 0,
      );

      $this->db->where('user_id',$user_id);
      $update_qry = $this->db->update('tbl_users',$data);
    }
  }
}

?>
<!-- End 20-3-17 -->


<!-- New code for popup details 18-11-16 -->
<?php include('popup_details.php'); ?>
<!-- End 18-11-16 -->
</body>
</html>


