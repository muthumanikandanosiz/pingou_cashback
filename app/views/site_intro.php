<div class="cls_bot_footer">
  <div class="wrap-top">
    <div class="container">
      <div class="row">
        <div class="container">
          <div class="col-md-2 col-sm-2 col-xs-12">
            <div class="cls_footer_text_logo"><img src="<?php echo base_url(); ?>front/images/footr_logo.png" alt=""/></div>
          </div>

          <div class="col-md-8 col-sm-8 col-xs-12 cls_footr_bot">
            <h4>A Little About <?php $admis = $this->front_model->getadmindetails_main(); echo $admis->site_name; ?></h4>
            <p><?php 
            $cms_Details = $this->front_model->cms_content('about-us');

            $big=  $cms_Details[0]->cms_content;
            echo $small = substr($big, 0, 591);
            ?> </p>
            <div class="cls_copy_right"> © <?php $admis = $this->front_model->getadmindetails_main(); echo $admis->site_name; ?> - All Rights Reserved.
            </div>
          </div>

          <div class="col-md-2 col-sm-2 col-xs-12">
            <div class="cls_mar-top11"><img src="<?php echo base_url(); ?>front/images/footer_right_logo.png" alt=""/></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="pingou" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> <a class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></a>
        <h4 class="modal-title text-center" id="myModalLabel"> Login or Create an Account</h4>
      </div>
      <div class="modal-body">
        <div class="account-login">
          <div class="col-md-6 no-left-margin">
            <div class="registered-users bot-shadow">
              <div class="content">
                <div id="login">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-wrap">
                        <h1>Log in with your email account</h1>
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12">
                         	  <?php $redirect_urlstring =  uri_string();
                    				if($redirect_urlstring=="")
                    				{
                    				  $redirect_urlstring = 'index';
                    				}
                    				$redirect_endcede = insep_encode($redirect_urlstring);
                    				?>
                            <p class="text-center"> <a class="btn btn-social btn-sm btn-google-plus" href="<?php echo base_url();?>HAuth/register/Google/<?php echo $redirect_endcede;?>"> <i class="fa fa fa-google-plus"></i>
                            Sign in with Google Plus </a> <a class="btn btn-social btn-sm btn-facebook" href="<?php echo base_url();?>HAuth/register/Facebook/<?php echo $redirect_endcede;?>"> <i class="fa fa fa-facebook fb"></i>
                            Sign in with Facebook </a></p>
                            <!--<div class="buttons-set">
                            <center> <a href="<?php echo base_url()?>register"> <button type="button" title="Create an Account" class="btn btn-warning"> Signup Start Earning </button></a></center>
                            </div>-->
                          </div>
                        </div>
                        <div class="signin-or">
                          <hr class="hr-or">
                          <span class="span-or">or</span>
                        </div>
                        <?php
					               //begin form
              						$attribute = array('role'=>'form','name'=>'login_form','id'=>'login_form', 'onSubmit'=>'return setupajax_login();', 'autocomplete'=>'off','method'=>'post');
              						echo form_open('chk_invalid',$attribute);						
              					?>
                        <input type="hidden" name="signin" value="signin" id="signin" />
                          <div class="form-group">
                           <center><span id="userstatus" style="color:red; font-weight:bold;"> </span></center>
                            <label for="email" class="sr-only">Email<span class="required_field">*</span></label>
                            <input type="email" required id="email" class="form-control input-lg" placeholder="somebody@example.com" name="email" autocomplete="off"  >
                          </div>
                          <div class="form-group">
                            <label for="key" class="sr-only">Password<span class="required_field">*</span></label>
                            <input type="password" required class="form-control input-lg" name="pwd" autocomplete="off" placeholder="Password" >
                          </div>
                          <div class="checkbox"> 
                            <span  style="border:none;"class="character-checkbox" onclick=""></span> <input type="checkbox" name="rememberme" id="RememberMe"><span class="label">Remember Me</span> 
                          </div>
                          <input type="submit"  class="btn btn-custom btn-lg btn-block" name="signin" id="signin" value="Sign In">
                        <?php echo form_close();?>
                        <a href="<?php echo base_url()?>forgetpassword" class="forget">Forgot your password?</a>
                        <hr>
                      </div>
                    </div>
                    <!-- /.col-xs-12 --> 
                  </div>
                  <!-- /.row --> 
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 no-right-margin">
            <div class="new-user">
              <div class="content">
                <div class="section-line">
                  <h4 class="text-capitalize">Join India's No.1 Cashback & Coupon website</h4>
                  <ul class="list-unstyled clearfix login-list">
                    <li> <a href=""> <i class="fa fa-arrow-circle-right pad-rht"></i> Join FREE in under 15 seconds</a></li>
                    <li> <a href=""> <i class="fa fa-arrow-circle-right pad-rht"></i> We pay you CASHBACK on top of Coupons,
                      making us better than any other coupon site!</a></li>
                    <li> <a href=""> <i class="fa fa-arrow-circle-right pad-rht"></i>Save money at Snapdeal, Myntra, Yebhi,
                      Yatra & 500 more top brands</a></li>
                  </ul>
                  <?php //$num = rand(1000000,9999999); //?ref=<?php echo $num;?>
                  <div class="buttons-set">
                    <a href="<?php echo base_url()?>register"> <button class="btn btn-warning" title="Create an Account" type="button"> Signup Start Earning </button></a>
                  </div>
                  <hr>
              
                  <h3 class="clr-blu">SAVE MONEY at 500+ brands -<small> why pay more when you can pay less? </small></h3>
                  <ul class="list-inline clearfix mar40">
              
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-1.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-2.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-3.png" class="img-thumbnail mar-bot" width="70"></a></li>                   
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-4.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-5.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-6.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-7.png" class="img-thumbnail mar-bot" width="70"></a></li>                    
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-8.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer"> </div>
      <!--New registration Code End -->

    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dalog --> 
</div>

<!--New code for refferal link to register a users 20-6-16-->
<div id="pingous" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
  <div class="modal-dialog" style="width:400px;">
    <div class="modal-content">
      <div class="modal-header"> <a class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></a>
        <!--<h4 class="modal-title text-center" id="myModalLabel"> Create an Account</h4>-->
      </div>
       <div class="modal-body"> 
        <div class="account-login">
          <div class="col-md-12 no-left-margin">
            <div class="registered-users bot-shadow">
              <div class="content">
                <div id="login">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-wrap">
                        <h1>Free Registration</h1>
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12">
                            <?php $redirect_urlstring =  uri_string();
                            if($redirect_urlstring=="")
                            {
                              $redirect_urlstring = 'index';
                            }
                            $redirect_endcede = insep_encode($redirect_urlstring);
                            ?>
                            <p class="text-center">
                              <a class="btn btn-social btn-sm btn-google-plus" href="<?php echo base_url();?>HAuth/register/Google/<?php echo $redirect_endcede;?>"> <i class="fa fa fa-google-plus"></i> Connect with Google Plus</a><br><br>
                              <a class="btn btn-social btn-sm btn-facebook" href="<?php echo base_url();?>HAuth/register/Facebook/<?php echo $redirect_endcede;?>"> <i class="fa fa fa-facebook fb"></i> Connect with Facebook </a>
                            </p>
                            <!--<div class="buttons-set">
                            <center> <a href="<?php echo base_url()?>register"> <button type="button" title="Create an Account" class="btn btn-warning"> Signup Start Earning </button></a></center>
                            </div>-->
                          </div>
                        </div>
                        <div class="signin-or">
                          <hr class="hr-or">
                          <span class="span-or">or</span>
                        </div>
                        <?php
                         //begin form
                          $attribute = array('role'=>'form','name'=>'regform','id'=>'regform','method'=>'post');
                          echo form_open('register',$attribute);           
                        ?>
                        
                        <input type="hidden" name="pagename" value="<?php echo $this->uri->segment(1)?>">
                        <input type="hidden" name="uni_id" value="<?php echo $_REQUEST['ref'];?>">
                        <input type="hidden" name="register" value="register" id="register"/>
                        
                        
                          <div class="form-group">
                           <center><span id="userstatus" style="color:red; font-weight:bold;"> </span></center>
                            <label for="email" class="sr-only">Email<span class="required_field">*</span></label>
                            <input type="email" class="form-control input-lg" placeholder="Type your e-mail" name="user_email" id="user_email" onblur="return check_email();" autocomplete="off">
                            <div id="unique_name_error"></div>
                          </div>
                          <div class="form-group">
                            <label for="key" class="sr-only">Password<span class="required_field">*</span></label>
                            <input type="password" class="form-control input-lg" placeholder="Create a Password" name="user_pwd" id="user_pwd" autocomplete="off">
                          </div>
                          <!--<div class="checkbox"> 
                            <span  style="border:none;"class="character-checkbox" onclick=""></span> <input type="checkbox" name="rememberme" id="RememberMe"><span class="label">Remember Me</span> 
                          </div>-->
                          <input type="submit"  class="btn btn-custom btn-lg btn-block" name="register" id="register" value="Sign Up">
                          <div class="form-group">
                              <center>On sign-up you accepts the <a target="_blank" href="<?php echo base_url();?>cms/terms-of-service" class="clr-blu"> terms of use</a></center>
                           <br>
                            <center>
                              Already have an account?<?php echo '<a href="#entrar" data-toggle="modal" id="signinclk"  class="clr-blu"> Sign-in </a>'; ?>
                            </center>
                          </div>
                          <?php echo form_close();?>
                        <!--<a href="<?php echo base_url()?>forgetpassword" class="forget">Forgot your password?</a>-->
                        <hr>
                      </div>
                    </div>
                    <!-- /.col-xs-12 --> 
                  </div>
                  <!-- /.row --> 
                </div>
              </div>
            </div>
          </div>
          <!--<div class="col-md-6 no-right-margin">
            <div class="new-user">
              <div class="content">
                <div class="section-line">
                  <h4 class="text-capitalize">Join India's No.1 Cashback & Coupon website</h4>
                  <ul class="list-unstyled clearfix login-list">
                    <li> <a href=""> <i class="fa fa-arrow-circle-right pad-rht"></i> Join FREE in under 15 seconds</a></li>
                    <li> <a href=""> <i class="fa fa-arrow-circle-right pad-rht"></i> We pay you CASHBACK on top of Coupons,
                      making us better than any other coupon site!</a></li>
                    <li> <a href=""> <i class="fa fa-arrow-circle-right pad-rht"></i>Save money at Snapdeal, Myntra, Yebhi,
                      Yatra & 500 more top brands</a></li>
                  </ul>
                  <?php //$num = rand(1000000,9999999); //?ref=<?php echo $num;?>
                  <div class="buttons-set">
                    <a href="<?php echo base_url()?>register"> <button class="btn btn-warning" title="Create an Account" type="button"> Signup Start Earning </button></a>
                  </div>
                  <hr>
              
                  <h3 class="clr-blu">SAVE MONEY at 500+ brands -<small> why pay more when you can pay less? </small></h3>
                  <ul class="list-inline clearfix mar40">
              
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-1.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-2.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-3.png" class="img-thumbnail mar-bot" width="70"></a></li>                   
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-4.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-5.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-6.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-7.png" class="img-thumbnail mar-bot" width="70"></a></li>                    
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-8.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    
                  </ul>
                </div>
              </div>
            </div>
          </div>-->
        </div>
      </div>

      <div class="modal-footer"> </div>
      <!--New registration Code End -->

    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dalog --> 
</div>
<!--End new register menu-->

<!-- New code for New login Popup menu 20-6-16-->
<input type="hidden" name="pagenames" value="<?php echo $this->uri->segment(1)?>">
<?php
                    $storename     = $this->uri->segment(2); 
                    $selqry        = $this->db->query("SELECT * from affiliates where affiliate_url='$storename'")->row(); 
                    $aff_id        = $selqry->affiliate_id;
                    $aff_name      = $selqry->affiliate_name;
                    $types         = $selqry->affiliate_cashback_type;
                    if($types == 'Flat')
                    {
                      $cashbackper   = 'R$'. $selqry->cashback_percentage;
                    }
                    else
                    {
                      $cashbackper   = $selqry->cashback_percentage .'%';
                    }
                    //$cashbackper   = $selqry->cashback_percentage;
                    $store_coupons = $this->front_model->get_coupons_from_store($storename,20);
                    foreach($store_coupons as $coupons)
                    {
                      $coupon_id = $coupons->coupon_id;
                    }
                    //$num = rand(1000000,9999999); //?ref=<?php echo $num;
?>
<div id="entrar" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
<?php 
  if($this->uri->segment(1) == 'cupom_desconto') 
  {
  ?>
  <div class="modal-dialog">
  <?php
  }
  else
  {
  ?>
  <div class="modal-dialog"  style="width:400px;">
  <?php
  }
  ?>
    <div class="modal-content">
      <div class="modal-header"> 
        <a class="btn btn-default" data-dismiss="modal" id="enter_close">
          <span class="glyphicon glyphicon-remove"></span>
        </a>
        <?php 
  if($this->uri->segment(1) == 'cupom_desconto') 
  {
  ?>
        <h4 class="modal-title text-center" id="myModalLabel"> Besides saving with coupon,</h4>
        <h3 class="modal-title text-center" id="myModalLabel">would you like to Receive <?php echo $cashbackper; ?> of cashback?</h3>
        
        <div id="item-wrapper">
            <a href="#" id="item-1" data-panel="panel-1"><img src="<?php echo base_url();?>front/images/store-img-1.png" class="imgcls"></a><br><br>
          <div id="panel-1" class="panels">
            <p>
            <?php echo $this->uri->segment(2);?> Pay to advertise on  and we share that money with you.Learn<a href="<?php echo base_url();?>como-funciona"> How it works</a></p>
          </div>
       </div>

  <?php
  }
  ?>     
      </div>
      
       <div class="modal-body"> 
       
        <div class="account-login">
        <?php 
          if($this->uri->segment(1) == 'cupom_desconto') 
          {
          ?>
         <div class="col-md-6 no-left-margin">
          <?php
          }
          else
          {
          ?>
          <div class="col-md-12 no-left-margin">
          <?php
          }
          ?>
            <div class="registered-users bot-shadow">
              <div class="content">
                <div id="login">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-wrap">
                        <h1>Login</h1>
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12">
                            <?php $redirect_urlstring =  uri_string();
                            if($redirect_urlstring=="")
                            {
                              $redirect_urlstring = 'index';
                            }
                            $redirect_endcede = insep_encode($redirect_urlstring);
                            ?>
                            <p class="text-center">
                              <a class="btn btn-social btn-sm btn-google-plus" href="<?php echo base_url();?>HAuth/register/Google/<?php echo $redirect_endcede;?>"> <i class="fa fa fa-google-plus"></i> Connect with Google Plus</a><br><br>
                              <a class="btn btn-social btn-sm btn-facebook" href="<?php echo base_url();?>HAuth/register/Facebook/<?php echo $redirect_endcede;?>"> <i class="fa fa fa-facebook fb"></i> Connect with Facebook </a>
                            </p>
                            <!--<div class="buttons-set">
                            <center> <a href="<?php echo base_url()?>register"> <button type="button" title="Create an Account" class="btn btn-warning"> Signup Start Earning </button></a></center>
                            </div>-->
                          </div>
                        </div>
                        <div class="signin-or">
                          <hr class="hr-or">
                          <span class="span-or">or</span>
                        </div>
                        <?php
                         //begin form
                          $attribute = array('role'=>'form','name'=>'login_form','id'=>'login_form', 'onSubmit'=>'return setupajax_login();', 'autocomplete'=>'off','method'=>'post');
                          echo form_open('chk_invalid',$attribute);           
                        ?>
                        <input type="hidden" name="pagename" value="<?php echo $this->uri->segment(1)?>">
                        <input type="hidden" name="signin" value="signin" id="signin" />
                          <div class="form-group">
                           <center><span id="newdis" style="color:red; font-weight:bold;"> </span></center>
                            <label for="email" class="sr-only">Email<span class="required_field">*</span></label>
                            <input type="email" required id="emails" class="form-control input-lg" placeholder="your email" value="" name="emails" autocomplete="off"  >
                          </div>
                          <div class="form-group">
                            <label for="key" class="sr-only">Password<span class="required_field">*</span></label>
                            <input type="password" id="passwrd" required class="form-control input-lg" name="passwrd" autocomplete="off" value="" placeholder="your Password" >
                          </div>
                          <!--<div class="checkbox"> 
                            <span  style="border:none;"class="character-checkbox" onclick=""></span> <input type="checkbox" name="rememberme" id="RememberMe"><span class="label">Remember Me</span> 
                          </div>-->
                          <input type="submit"  class="btn btn-custom btn-lg btn-block" name="signin" id="signin" value="Sign-In">
                          <div class="form-group">
                              <center><a href="<?php echo base_url()?>forgetpassword" class="forget">Forgot your password?</a></center>
                           <br>
                            <center>
                              Don't have an account? <?php echo '<a href="#pingous" data-toggle="modal" id="signupclk" class="clr-blu"> Sign-Up </a>'; ?>
                            </center>
                          </div>
                          <?php echo form_close();?>
                        <!--<a href="<?php echo base_url()?>forgetpassword" class="forget">Forgot your password?</a>-->
                        <hr>
                      </div>
                    </div>
                    <!-- /.col-xs-12 --> 
                  </div>
                  <!-- /.row --> 
                </div>
              </div>
            </div>
          </div>
          <?php if($this->uri->segment(1) == 'cupom_desconto')
          {?>
            <div class="col-md-6 no-right-margin">
            <div class="new-user">
              <div class="content">
                <div class="section-line">
                  
                  <!--<ul class="list-unstyled clearfix login-list">
                    <li> <a href=""> <i class="fa fa-arrow-circle-right pad-rht"></i> Join FREE in under 15 seconds</a></li>
                    <li> <a href=""> <i class="fa fa-arrow-circle-right pad-rht"></i> We pay you CASHBACK on top of Coupons,
                      making us better than any other coupon site!</a></li>
                    <li> <a href=""> <i class="fa fa-arrow-circle-right pad-rht"></i>Save money at Snapdeal, Myntra, Yebhi,
                      Yatra & 500 more top brands</a></li>
                  </ul>-->

                  <div class="buttons-set">
                    <center><a href="<?php echo base_url().'ir_loja/'.$aff_id.'/'.$coupon_id ?>" target='_blank'<button class="btn btn-warning" title="Create an Account" type="button"> VISIT <?php echo strtoupper($aff_name);?> WITHOUT COUPON </button></a></center>
                  </div>
                  
                  <!--<hr>
                  <h3 class="clr-blu">SAVE MONEY at 500+ brands -<small> why pay more when you can pay less? </small></h3>
                  <ul class="list-inline clearfix mar40">
              
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-1.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-2.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-3.png" class="img-thumbnail mar-bot" width="70"></a></li>                   
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-4.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-5.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-6.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-7.png" class="img-thumbnail mar-bot" width="70"></a></li>                    
                    <li><a href=""><img src="<?php echo base_url();?>front/images/store-img-8.png" class="img-thumbnail mar-bot" width="70"></a></li>
                    
                  </ul>-->
                </div>
              </div>
            </div>
          </div>  
          <?php
          }
          ?>
        </div>
      </div>
      <div class="modal-footer"> </div>
      <!--New registration Code End -->
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dalog --> 
</div>

<!--End New code for login popup menu 20-6-16-->
<style type="text/css">
  .imgcls
  {
    float: right;
    height: 35px;
    margin-right: 13%;
    width: 100px;
  }

  .buttons-set
  {
     margin: 50% 10% 0 18% !important;
      
  }
 
 
.panels {
    display: none;
    background: #ccc;
    //min-height: 200px;
    //width: 48%;
    float: right;
    //margin-right: 20px;
}
#panel-2.panels {
    background: #ffcc00;
}
li a {
    display: block;
}
h2 {
    margin: 0;
    padding: 0;
}

</style>
<script type="text/javascript">
$('#item-wrapper a').mouseenter(function () {
    console.log($(this).data('panel'));
    if ($(this).data('panel')) {
        $('.panels').hide();
        $('#' + $(this).data('panel')).fadeIn();
    }
    });
$('#item-wrapper').mouseleave(function () {
    $('.panels').fadeOut();
});

</script>