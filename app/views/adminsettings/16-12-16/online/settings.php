<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php
  $admin_details   = $this->admin_model->get_admindetails();
  ?>
  <title>Admin Settings | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>

  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php $this->load->view('adminsettings/header'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
    <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
          <div class="span12">
            <!-- <h3 class="page-title">
            Admin Settings
            </h3>
            <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
					      <span class="divider">&nbsp;</span>
              </li>
              <li>
						  	<?php echo anchor('adminsettings/settings','Settings'); ?>
							  <span class="divider-last">&nbsp;</span>
              </li>
            </ul> -->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
              <div class="span12">
                <!-- BEGIN SAMPLE FORM widget-->
                <div class="widget">
                  <div class="widget-title">
                    <h4><i class="icon-cog"></i> Admin Settings</h4>
                    <span class="tools">
                      <a href="javascript:;" class="icon-chevron-down"></a>
                      <!--<a href="javascript:;" class="icon-remove"></a>-->
                    </span>
                  </div><br>

					    <span>
                <span class="required_field"> &nbsp;&nbsp;&nbsp;*</span> marked fields are mandatory.
              </span>
              <br>
              <div class="widget-body form">
    					  <?php 
        					$error = $this->session->flashdata('error');
                  if($error!="")
                  {
        						echo '<div class="alert alert-error">
        						<button data-dismiss="alert" class="close">x</button>
        						<strong>Error! </strong>'.$error.'</div>';
        					}
        				  $success = $this->session->flashdata('success');
        				  
                  if($success!="") 
                  {
        						echo '<div class="alert alert-success">
        						<button data-dismiss="alert" class="close">x</button>
        						<strong>Success! </strong>'.$success.'</div>';			
        				  }
                ?>
                <!-- BEGIN FORM-->
      					<?php
      						$attribute = array('role'=>'form','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data','onSubmit' =>'return validation();' ); 
      						echo form_open('adminsettings/settingsupdate',$attribute);
    						?>
                        	
                <input type="hidden" name="admin_id" id="admin_id" value="<?php echo $admin_id; ?>">
                <input type="hidden" name="hidden_img" id="hidden_img" value="<?php echo $admin_logo; ?>">
                <input type="hidden" name="hidden_site_logo" id="hidden_site_logo" value="<?php echo $site_logo; ?>">
                <input type="hidden" name="hidden_site_favicon" id="hidden_site_favicon" value="<?php echo $site_favicon; ?>">

                <div class="control-group">
                  <label class="control-label">Site Name <span class="required_field">*</span></label>
                  <div class="controls">
                    <input type="text" class="span6" name="site_name" id="site_name" value="<?php echo $site_name; ?>" required />
                  </div>
                </div>
						   
						    <div class="control-group">
                  <label class="control-label">Site URL <span class="required_field">*</span></label>
                    <div class="controls">
                      <input type="text" class="span6" name="site_url" id="site_url" value="<?php echo $site_url; ?>" required />
                    </div>
                </div>
                           
                <div class="control-group">
                  <label class="control-label">Homepage Title <span class="required_field">*</span></label>
                    <div class="controls">
                      <textarea name="homepage_title" id="homepage_title" required class="span6"><?php echo $homepage_title; ?></textarea>
                    </div>
                </div>
						  
						    <div class="control-group">
                  <label class="control-label">Meta Keyword<span class="required_field">*</span></label>
                  <div class="controls">
								    <textarea name="meta_keyword" id="meta_keyword" required class="span6"><?php echo $meta_keyword; ?></textarea>
                  </div>
                </div>
						 
						    <div class="control-group">
                  <label class="control-label">Meta Description <span class="required_field">*</span></label>
                  <div class="controls">
								    <textarea name="meta_description" id="meta_description" required class="span6"><?php echo $meta_description; ?></textarea>
                  </div>
                </div> 

                <div class="control-group">
                  <label class="control-label">Username <span class="required_field">*</span></label>
                  <div class="controls">
                    <input type="text" class="span6" name="username" id="username" value="<?php echo $admin_username; ?>" required />
                  </div>
                </div>
						    
                <div class="control-group">
                  <label class="control-label">Email Address <span class="required_field">*</span></label>
                    <div class="controls">
                    <input type="email" class="span6" name="email_notify" id="email_notify" value="<?php echo $email_notify; ?>" required email/>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Email Notification sender<span class="required_field">*</span></label>
                  <div class="controls">
                    <input type="email" class="span6" name="email" id="email" value="<?php echo $admin_email; ?>" required email/>
                  </div>
                </div>

						    <div class="control-group">
                  <label class="control-label">Admin Avatar</label>
                  <div class="controls">
                    <input type="file" class="span6" name="admin_logo" id="admin_logo" /><br>
								    <span>Note: Admin Avatar should be in (250 * 250) in size</span><br>
								    <img src="<?php echo $this->admin_model->get_img_url(); ?>uploads/adminpro/<?php echo $admin_logo; ?>" width="250" height="250">
                  </div>
                </div>
						   
						    <div class="control-group">
                  <label class="control-label">Site Logo</label>
                  <div class="controls">
                    <input type="file" class="span6" name="site_logo" id="site_logo" /><br>
								    <span>Note: Admin Avatar should be in (216 * 79) in size</span><br>
								    <img src="<?php echo $this->admin_model->get_img_url(); ?>uploads/adminpro/<?php echo $site_logo; ?>" width="150" height="250">	
                  </div>
                </div>
						   
						    <div class="control-group">
                  <label class="control-label">Site Favicon</label>
                  <div class="controls">
                    <input type="file" class="span6" name="site_favicon" id="site_favicon" /><br>
								    <span>Note: Site Favicon should be in (50 * 50) in size</span><br>
								    <img src="<?php echo $this->admin_model->get_img_url(); ?>uploads/adminpro/<?php echo $site_favicon; ?>" width="50" height="50">
                  </div>
                </div>
						   
						    <div class="control-group">
                  <label class="control-label">Minimum Withdraw <span class="required_field">*</span><br>  </label>
                  <div class="controls">
                    <input type="text" class="span6" name="minimum_cashback" id="minimum_cashback" value="<?php echo $minimum_cashback; ?>" required /> (in Rs.)
                  </div>
                </div>
    
    				    <div class="control-group">
                  <label class="control-label">Unic bônus <span class="required_field">*</span><br>  </label>
                  <div class="controls">
                    <input type="text" class="span6" name="benefit_bonus" id="benefit_bonus" value="<?php echo preg_replace('/\./', ',',$benefit_bonus); ?>" required /> (in Rs.)
                  </div>
                </div>

						    <div class="control-group">
                  <label class="control-label">Facebook URL <span class="required_field"></span></label>
                  <div class="controls">
                    <input type="text" class="span6" name="fb_url" id="fb_url" value="<?php echo $admin_fb; ?>"  />
                  </div>
                </div>
					   
						    <div class="control-group">
                  <label class="control-label">Twitter URL <span class="required_field"></span></label>
                  <div class="controls">
                    <input type="text" class="span6" name="twitter_url" id="twitter_url" value="<?php echo $admin_twitter; ?>"  />
                  </div>
                </div>
					   
						    <div class="control-group">
                  <div class="control-group">
                    <label class="control-label">Google+ URL <span class="required_field"></span></label>
                    <div class="controls">
                      <input type="text" class="span6" name="gplus_url" id="gplus_url" value="<?php echo $admin_gplus; ?>"  />
                    </div>
                  </div>
                  
                  <label class="control-label">Instagram URL <span class="required_field"></span></label>
                  <div class="controls">
                    <input type="text" class="span6" name="admin_instagram" id="admin_instagram" value="<?php echo $admin_instagram; ?>"  />
                  </div>
                </div>
                           
                <div class="control-group">
                  <label class="control-label">Pinterest URL <span class="required_field"></span></label>
                  <div class="controls">
                    <input type="text" class="span6" name="admin_pintrust" id="admin_pintrust" value="<?php echo $admin_pintrust; ?>" />
                  </div>
                </div>           
						   
						    <div class="control-group">
                  <label class="control-label">Contact Number <span class="required_field">*</span></label>
                  <div class="controls">
                    <input type="text" class="span6" name="contact_number" id="contact_number" value="<?php echo $contact_number; ?>" required />
                  </div>
                </div>
						   
						    <div class="control-group">
                  <label class="control-label">Site Mode</label>
                  <div class="controls">
								    <select name="site_mode" class="span6">
									    <option value="1" <?php if($site_mode=='1'){ echo 'selected="selected"'; }?>>Live Mode</option>
									    <option value="0" <?php if($site_mode=='0'){ echo 'selected="selected"'; }?>>Under Maintenance</option>
								    </select>
                  </div>
                </div>
                           
                <div class="control-group">
                  <label class="control-label">Google analytics <span class="required_field">*</span></label>
                  <div class="controls">
								    <textarea name="google_analytics" id="google_analytics" required  class="span6"><?php echo $google_analytics; ?></textarea>
                  </div>
                </div>                      
                           
                <div class="control-group">
                  <label class="control-label">Google Login Id <span class="required_field">*</span></label>
                  <div class="controls">
								    <textarea name="google_key" id="google_key" required class="span6"><?php echo $google_key; ?></textarea>
                  </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label">Google Secret Key <span class="required_field">*</span></label>
                  <div class="controls">
								    <textarea name="google_secret" id="google_secret" required class="span6"><?php echo $google_secret; ?></textarea>
                  </div>
                </div>
						    
                <!-- face book App Id & secret key  seetha -->
						    <div class="control-group">
                  <label class="control-label">Facebook Login Id <span class="required_field">*</span></label>
                  <div class="controls">
								    <textarea name="facebook_key" id="facebook_key" required class="span6"><?php echo $facebook_key; ?></textarea>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">facebook Secret Key <span class="required_field">*</span></label>
                  <div class="controls">
								    <textarea name="facebook_secret" id="facebook_secret" required class="span6"><?php echo $facebook_secret; ?></textarea>
                  </div>
                </div>
                
                <!-- Yahoo App Id & secret key -->
                <div class="control-group">
                  <label class="control-label">Yahoo Login Id <span class="required_field">*</span></label>
                  <div class="controls">
                    <textarea name="yahoo_key" id="yahoo_key" required class="span6"><?php echo $yahoo_key; ?></textarea>
                  </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label">Yahoo Secret Key <span class="required_field">*</span></label>
                  <div class="controls">
                    <textarea name="yahoo_secret" id="yahoo_secret" required class="span6"><?php echo $yahoo_secret; ?></textarea>
                  </div>
                </div>    

                <!-- New code for hotmail configuration 6-9-16 -->
                <div class="control-group">
                  <label class="control-label">Hotmail Client Id <span class="required_field">*</span></label>
                  <div class="controls">
                    <textarea name="hotmail_key" id="hotmail_key" required class="span6"><?php echo $hotmail_key; ?></textarea>
                  </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label">Hotmail Secret Key <span class="required_field">*</span></label>
                  <div class="controls">
                    <textarea name="hotmail_secret" id="hotmail_secret" required class="span6"><?php echo $hotmail_secret; ?></textarea>
                  </div>
                </div>       
                <!-- End -->

                           
                <div class="control-group">
                  <label class="control-label">Blog URL <span class="required_field">*</span></label>
                  <div class="controls">
								    <textarea name="blog_url" id="blog_url" required class="span6"><?php echo $blog_url; ?></textarea>
                  </div>
                </div>
                           
                <div class="control-group">
                  <label class="control-label">Enable Blog <span class="required_field">*</span></label>
                  <div class="controls">
								    <select name="enable_blog" class="span6">
								   	  <option value="1" <?php if($enable_blog=='1'){ echo 'selected="selected"'; }?>>Enable</option>
									    <option value="0" <?php if($enable_blog=='0'){ echo 'selected="selected"'; }?>>Disable</option>
								    </select>
                  </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label">Enable Shopping <span class="required_field">*</span></label>
                  <div class="controls">
								    <select name="enable_shopping" class="span6">
    							    <option value="1" <?php if($enable_shopping=='1'){ echo 'selected="selected"'; }?>>Enable</option>
    								  <option value="0" <?php if($enable_shopping=='0'){ echo 'selected="selected"'; }?>>Disable</option>
    								</select>
                  </div>
                </div>
                  
                <div class="control-group">
                  <label class="control-label">Address</label>
                  <div class="controls">
                    <textarea class="span6" name="address" id="address" rows="5" required ><?php echo $address; ?></textarea>
                  </div>
                </div>         
                
                <!-- seetha-->
						    <input type="hidden" name="enable_slider" id="enable_slider" value="0">
                 
                  
                <!-- SATz Time picker -->
                <!-- New Session format start 12-7-16 -->
                <?php
                 //echo $ses_datetime;
                $newformat = explode(':', $ses_datetime);
                //print_r($newformat);
                $days    = $newformat['0'];
                $hours   = $newformat['1'];
                $minutes = $newformat['2'];
                ?>

                <div class="control-group">
                    <label class="control-label">Session Alive Time</label>
                    <div class="controls">

                    <select style="width:8%;" name="days">
                      <option value="">Days</option>
                      <?php 
                      for($i=0;$i<=31;$i++)
                      {
                        ?>
                        <option <?php if($days==$i){ echo 'selected="selected"'; } ?>value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php 
                      }
                      ?>  
                    </select>
                    <select style="width:10%;" name="hours">
                      <option value="">Hours</option>
                      <?php for($i=0;$i<=23;$i++)
                      {
                        ?>
                        <option <?php if($hours==$i){ echo 'selected="selected"'; } ?>value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php 
                      }
                      ?>
                    </select>
                    <select style="width:11%;" name="minutes">
                      <option value="">Minutes</option>
                      <?php for($i=30;$i<=59;$i++)
                      {
                        ?>
                        <option <?php if($minutes==$i){ echo 'selected="selected"'; } ?>value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php 
                      }
                      ?>
                    </select> 
                    <!-- <input type="text" id="time" data-format="D:HH:mm" data-template="D : HH : mm" name="ses_datetime" value="<?=$ses_datetime?>"> -->
                    </div>
                </div>
                <!-- New format end 12-7-16 --> 
                <!-- SATz Time picker -->
                <!--Pilaventhiran 02/05/2016 End-->
                <!--End-->

                <div class="control-group">
                  <label class="control-label">Expiry Date of Coupon Field</label>
                  <div class="controls">
                    <input type="text" name="coupon_expiry_date" value="<?php echo date('d-m-Y',strtotime($coupon_exp_date)); ?>" id="coupon_expiry_date" required class="span4 date-picker">
                  </div>
                </div>
                  
                 
           
                <!--End-->
                <div class="form-actions">
                  <input type="submit" name="save" value="Save Changes" class="btn btn-success">
                </div>
						   
  				      <?php echo form_close(); ?>
                <!--</form>-->
                <!-- END FORM-->
              </div>
            </div>
                  <!-- END SAMPLE FORM widget-->
          </div>
        </div>
      </div>
         <!-- END PAGE CONTAINER-->
    </div>
      <!-- END PAGE -->  
  </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
  <!-- SATz -->
  <!-- Time picker -->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>front/js/moment.js"></script>   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>front/js/combodate.js"></script>
  <script>
  $(function(){
      $('#time').combodate({
          firstItem: 'name', //show 'hour' and 'minute' string at first item of dropdown
          minuteStep: 1
      });  
  });
  </script>



<!-- SATz  -->
   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
    function change_background(background)
    {
      if(background=='color') 
      {
        $("#back_image").hide();
        $("#back_color").show();
      }
      if(background=='image') 
       {
        $("#back_color").hide();
        $("#back_image").show();
      }
    }

    function change_coverphoto(cover)
    {
      if(cover=='covercolor') 
      {
        $("#coverimage").hide();
        $("#covercolor").show();
      }
      if(cover=='coverimage') 
      {
        $("#covercolor").hide();
        $("#coverimage").show();
      }
    }
   
</script>
     <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>
                            