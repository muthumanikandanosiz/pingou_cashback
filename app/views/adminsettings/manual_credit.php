<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>manual Credit| <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>

	<!--seetha--> 
	<link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/boostrap-select/bootstrap-combined.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/boostrap-select/bootstrap-select.min.css" media="screen" > 	
	<!--seetha--> 
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN THEME CUSTOMIZER-->
            <!--<div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
            <span class="settings">
            <span class="text">Theme:</span>
            <span class="colors">
            <span class="color-default" data-style="default"></span>
            <span class="color-gray" data-style="gray"></span>
            <span class="color-purple" data-style="purple"></span>
            <span class="color-navy-blue" data-style="navy-blue"></span>
            </span>
            </span>adminsettings
            </div>-->
            <!-- END THEME CUSTOMIZER-->
            <!-- <h3 class="page-title">
            Manual Credit Details
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
						    <span class="divider">&nbsp;</span>
              </li>
              <li>
							  <?php echo anchor('adminsettings/manual_credit',' Manual Credit'); ?>
							  <span class="divider">&nbsp;</span>
              </li>
					    <li>
							  Store Details<span class="divider-last">&nbsp;</span>
              </li>
            </ul> -->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN SAMPLE FORM widget-->
            <div class="widget">
                <div class="widget-title">
                  <h4><i class="icon-file"></i>  Manual Credit</h4>
                  <span class="tools">
                     <a href="javascript:;" class="icon-chevron-down"></a>
                     <!--<a href="javascript:;" class="icon-remove"></a>-->
                  </span>
                </div>
                <br>
					      <span><span class="required_field"> &nbsp;&nbsp;&nbsp;*</span> marked fields are mandatory.</span><br>
					      <?php
        				if($action=="new")
                {
        				
                  ?>
                  <div class="widget-body form">
          				  <?php 
          				  $error = $this->session->flashdata('error');
          				  if($error!="")
                    {
            					echo '<div class="alert alert-error">
            					<button data-dismiss="alert" class="close">x</button>
            					<strong>Error! </strong>'.$error.'</div>';
          				  }
                    
          					$success = $this->session->flashdata('success');
          					if($success!="") 
                    {
          					  echo '<div class="alert alert-success">
          					 	<button data-dismiss="alert" class="close">x</button>
          						<strong>Success! </strong>'.$success.'</div>';
          					}
                    ?>
                    <!-- BEGIN FORM-->
                    <!--<form action="#" class="form-horizontal">-->
  						      <?php
        						$sess_affiliate_name = $this->session->flashdata('affiliate_name');
        						$sess_affiliate_status = $this->session->flashdata('affiliate_status');
        						$sess_affiliate_type = $this->session->flashdata('affiliate_type');

        						$attribute = array('role'=>'form','method'=>'post','id'=>'affiliates_a','class'=>'form-horizontal', 'enctype'=>'multipart/form-data'); 
        						echo form_open('adminsettings/manual_credit',$attribute);
  						      ?>
  						      
                    <div class="control-group extra_hidden">
                      <label class="control-label">Amount<span class="required_field">*</span></label>
                      <div class="controls">
                        Rs: <input type="text" onKeyPress="bluramount(this.value);" class="span5" name="transation_amount" id="transation_amount" value="" placeholder="" required />
                      </div>
                    </div>
                    <?php
  						      $get_allusers = $this->admin_model->get_allusers();
  						      ?>
                             
                    <div class="control-group extra_hidden">
                      <label class="control-label " >User Email<span class="required_field">*</span></label>
                      <div class="controls">
                        <select name="user_id" required class="span6 selectpicker" data-live-search="true"> 
                          <?php
                    			foreach($get_allusers as $users)
                    			{
                    				?>							     
                    				<option <?php if($passing_userid==$users->user_id){ echo "selected=selected";}?> value="<?php echo $users->user_id;?>"><?php echo $users->email;?></option>
                    	      <?php
                    			}
                    			?>
                        </select>
                      </div>
                    </div>
                             
                    <div class="control-group extra_hidden">
                      <label class="control-label">Payment Type<span class="required_field">*</span></label>
                      <div class="controls">
                        <select name="transation_reason" id="transation_reason" required class="span6"> 								  					     
                          <option  value="Credit Account">Credit Account</option>
                          <option  value="Cashback">Cashback</option>
                          <option  value="Referal Payment">Referal Payment</option>
                        </select>
                      </div>
                    </div>
                    <div class="control-group extra_hidden">
                      <label class="control-label">Payment Status<span class="required_field">*</span></label>
                      <div class="controls">
                        <select name="status" id="transation_reason" required class="span6">                                 
                          <option  value="Pending">Pending</option>
                          <option  value="Approved">Approved</option>
                          <option  value="Canceled">Canceled</option>
                        </select>
                      </div>
                    </div>

  						      <!-- Added Section 28/11/14 -->
  						      <div class="form-actions">
                      <input type="submit" name="save" value="Submit" class="btn btn-success">
                    </div>
  						      <?php echo form_close(); ?>
                    <!--</form>-->
                    <!-- END FORM-->
                  </div>
					        <?php
                }
                else if($action=="edit")
                {
                  ?>
                  <div class="widget-body form">
                    <?php 
                    $error = $this->session->flashdata('error');
                    if($error!="") 
                    {
                      echo '<div class="alert alert-error">
                      <button data-dismiss="alert" class="close">x</button>
                      <strong>Error! </strong>'.$error.'</div>';
                    }
                    $success = $this->session->flashdata('success');
                    if($success!="") 
                    {
                      echo '<div class="alert alert-success">
                      <button data-dismiss="alert" class="close">x</button>
                      <strong>Success! </strong>'.$success.'</div>';
                    }
                    ?>
                    <!-- BEGIN FORM-->
                    <!--<form action="#" class="form-horizontal">-->
                    <?php
                    $attribute = array('role'=>'form','method'=>'post','id'=>'affiliates_a','class'=>'form-horizontal', 'enctype'=>'multipart/form-data'); 
                    echo form_open('adminsettings/manual_credit',$attribute);
                    ?>

                    <div class="control-group">
                      <label class="control-label">User Id</label>
                      <div class="controls">
                        <label class="span6"><?php echo $user_id; ?></label>
                      </div>
                    </div>
                    <?php 
                    $username  = $this->admin_model->user_names($user_id);
                    $usernames = $username->first_name." ".$username->last_name;
                    ?>
                    <div class="control-group">
                      <label class="control-label">User name</label>
                      <div class="controls">
                        <label class="span6"><?php echo $usernames; ?></label>
                      </div>
                    </div>
                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
                    <div class="control-group">
                      <?php $emailid = $this->admin_model->view_user($user_id);
                      foreach($emailid as $get)
                      {
                        $user_email = $get->email;
                      }
                      ?>
                      <input type="hidden" name="cashback_id" value="<?php echo $cashback_id;?>"/>
                      <input type="hidden" name="transation_status" value="<?php echo $transation_status;?>"/>
                      <input type="hidden" name="trans_id" value="<?php echo $trans_id;?>"/>
                      <input type="hidden" name="transation_amount" value="<?php echo $transation_amount;?>"/>
                      <label class="control-label">User Email</label>
                      <div class="controls">
                        <label name="email" class="span6"><?php echo $user_email; ?></label>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Report Update Id</label>
                      <div class="controls">
                        <label class="span6"><?php echo $report_update_id; ?></label>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Payment Type</label>
                      <div class="controls">
                        <label class="span6"><?php echo $transation_reason; ?></label>
                      </div>
                    </div>
                    
                    <div class="control-group">
                      <label class="control-label">Transaction Amount</label>
                      <div class="controls">
                        <label class="span6"><?php echo $this->admin_model->currency_format($transation_amount); ?></label>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Transaction Date</label>
                      <div class="controls">
                        <label class="span6"><?php echo date('d/m/Y',strtotime($transaction_date)); ?></label> 
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Transaction Date</label>
                      <div class="controls">
                        <label class="span6"><?php echo date('d/m/Y',strtotime($transation_date)); ?></label>
                      </div>
                    </div>
                    
                    <div class="control-group">
                      <label class="control-label">Reference Id</label>
                      <div class="controls">
                        <label class="span6"><?php echo $transaction_id; ?></label>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Table</label>
                      <div class="controls">
                        <label class="span6"><?php echo $table; ?></label>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Details Id</label>
                      <div class="controls">
                        <label class="span6"><?php echo $details_id; ?></label>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">New Txn Id</label>
                      <div class="controls">
                        <label class="span6"><?php echo $new_txn_id; ?></label>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Ref User Tracking Id</label>
                      <div class="controls">
                        <label class="span6"><?php echo $ref_user_tracking; ?></label>
                      </div>
                    </div>


                    <hr>
                    <div class="control-group"><br>
                      <label class="control-label">Current Status</label>
                      <div class="controls">
                        <?php  $user_status = $transation_status;?>
                        <select name="status" required>
                          <option value="Pending" <?php if($user_status=='Pending'){ echo 'selected="selected"'; } ?>>Pending</option>
                          <option value="Approved" <?php if($user_status=='Approved' || $user_status=='Paid'){ echo 'selected="selected"'; } ?>>Approved</option>
                          <option value="Canceled" <?php if($user_status=='Canceled'){ echo 'selected="selected"'; } ?>>Canceled</option>
                        </select>
                      </div>
                    </div>
                          
                    <div class="form-actions">
                      <input type="submit" name="saves" value="Submit" class="btn btn-success">
                    </div>
                    <?php echo form_close(); ?>
                    <!--</form>-->
                    <!-- END FORM-->
                  </div>
                  <?php
                }
                ?>
            </div>
            <!-- END SAMPLE FORM widget-->
          </div>
        </div>
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->  
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
  <!-- END FOOTER -->
  <!-- BEGIN JAVASCRIPTS -->
  <!-- Load javascripts at bottom, this will reduce page load time -->
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
	<script>
    $(document).ready(function() {       
    // initiate layout and plugins
    App.init();
    });
  </script>
  <script>
    function changetypes(method)
    {
    	if(method=='Direct')
    	{
    		$('.extra_hidden').show();
    		$('#hidden_sub').hide();
    		
    	}
    	else
    	{
    		$('.extra_hidden').hide();
    		$('#hidden_sub').show();
    	}
    }
 
    function bluramount(max_shipping_time)
    {
    	max_shipping_time = max_shipping_time.replace(/[^0-9\.]/g,'');
    	if(max_shipping_time.split('.').length>2) 
    	max_shipping_time = max_shipping_time.replace(/\.+$/,"");
    	$('#transation_amount').val(max_shipping_time);
     }
  </script>
  <!-- seetha 19.8.15-->
  <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>-->
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/boostrap-select/bootstrap-select.min.js"></script>
  <script>
  $(document).ready(function(e) 
  { 
  	$('.selectpicker').selectpicker();
  });
  </script>
  <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>