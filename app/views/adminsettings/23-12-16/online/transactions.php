<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Payments | <?php echo $admin_details->site_name; ?> Admin</title>
  <?php $this->load->view('adminsettings/script'); ?>
  <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <!-- <h3 class="page-title">
              Payments
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
                <span class="divider">&nbsp;</span>
              </li>
              <li><?php echo anchor('adminsettings/transactions','Payments'); ?>
                <span class="divider-last">&nbsp;</span>
              </li>
            </ul> -->
            <!-- END PAGE TITLE & BREADCRUMB-->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <!-- BEGIN ADVANCED TABLE widget-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN EXAMPLE TABLE widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i> Payments</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                </span>
              </div>
              <div class="widget-body">
                <?php 
                $error = $this->session->flashdata('error');
                if($error!="") 
                {
                  echo '<div class="alert alert-error">
                  <button data-dismiss="alert" class="close">x</button>
                  <strong>Error! </strong>'.$error.'</div>';
                }
                $success = $this->session->flashdata('success');
                if($success!="")
                {
                  echo '<div class="alert alert-success">
                  <button data-dismiss="alert" class="close">x</button>
                  <strong>Success! </strong>'.$success.'</div>';      
                }
                ?>
                <form id="form2" action="" method="post" name="form2">
                  <table class="table table-striped table-bordered" id="sample_1">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th style=""><center><input type="checkbox" id="check_b" class="check_b" onchange="checkAll(this)" name="chk[]" /></center></th>
                        <th><center>User ID</center></th>
                        <th><center>User Name</center></th>
                        <th><center>User Email</center></th>
                        <th><center>Report Update Id</center></th>
                        <th><center>Payment Type</center></th>
                        <th><center>Amount</center></th>
                        <th><center>Transation Date</center></th>
                        <th><center>Transaction Date</center></th>
                        <th><center>Reference ID</center></th>
                        <th><center>Table</center></th>
                        <th><center>Details Id</center></th>
                        <th><center>New Txn Id</center></th>
                        <th><center>Ref User tracking Id</center></th>
                        <th><center>Status</center></th>
                        <th><center>Edit</center></th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      
                      if($coupons)
                      {
                        $k=0;
                        foreach($coupons as $coupon)
                        {
                          $transation_id = $coupon->transation_id;
                          
                          ?>
                          <tr class="odd gradeX">
                            <td><?php echo $k; ?></td>
                            <td><center><input type="checkbox"  class="check_b" name="chkbox[<?php echo $coupon->trans_id;?>]" /></center></td> 
                            <td><?php echo $coupon->user_id;?></td>
                            <?php
                            if($this->admin_model->user_name($coupon->user_id))
                            {
                              ?>
                              <td>
                                <a class="user" href="<?php echo base_url();?>adminsettings/view_user/<?php echo $coupon->user_id;?>">
                                <?php echo $this->admin_model->user_name($coupon->user_id); ?></a>
                              </td>
                              <?php
                            }
                            else
                            {
                              ?>
                              <td><a class="user" href="javascript:void(0)">Unknown User</a></td>
                              <?php
                            }
                            ?>
                            <td><?php echo $this->admin_model->user_email($coupon->user_id);?></td>
                            <td><?php echo $coupon->report_update_id;?></td>
                            <td><?php if($coupon->transation_reason == 'Cashback'){ echo 'Cashback'; } else if($coupon->transation_reason == 'Credit Account'){ echo 'Credit Account'; } else { echo 'Referal Payment'; } ?></td>
                            <td>Rs.<?php echo $this->admin_model->currency_format($coupon->transation_amount); ?></td>
                            <td><?php echo date('d/m/Y',strtotime($coupon->transation_date)); ?></td>
                            <td><?php echo date('d/m/Y',strtotime($coupon->transaction_date)); ?></td>
                            <td><?php echo $transation_id; ?></td>

                            <td><?php echo $coupon->table; ?></td>
                            <td><?php echo $coupon->details_id; ?></td>
                            <td><?php echo $coupon->new_txn_id; ?></td>
                            <td><?php echo $coupon->ref_user_tracking_id; ?></td>
                            
                            <td><center><?php
                              if($coupon->transation_status=='Approved' || $coupon->transation_status=='Paid')
                              {
                                ?>
                                <span class="btn btn-success" >Approved</span>
                                <?php
                              }
                              else
                              {
                                ?>
                                <span class="btn btn-danger" ><?php echo $coupon->transation_status;?></span>
                                <?php
                              }
                              ?></center>
                            </td>
                            <td>
                              <center>
                              <?php if($coupon->transation_reason != 'Cashback'){ echo anchor('adminsettings/edittransactions/'.$coupon->trans_id,'<i class="icon-pencil"></i>'); } else{ ?><center><span class="label label-info">Cashback</span></center><?php } ?>
                              </center>
                            </td>
                            
                          </tr>
                          <?php
                        $k++;
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                  <input type="hidden" name="hidd" value="hidd">
                  <input id="GoUpdate" class="btn btn-warning" type="submit" value="Delete Transactions" name="GoUpdate">
                </form>
              </div>
            </div>
            <!-- END EXAMPLE TABLE widget-->
          </div>
        </div>
        <!-- END ADVANCED TABLE widget-->
        <!-- END PAGE CONTENT-->
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
 
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>   
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
  <script>
    jQuery(document).ready(function() {
      // initiate layout and plugins
      App.init();
    });
  </script>
  <script type="text/javascript">
    function confirmDelete(m)  // Confirm before delete coupon..
    {
      if(!confirm(m))
      {
        return false;
      }
      else
      {
        return true;
      }
    }
  </script>
  <script>
    $(document).ready(function() {
    $(".check_b").attr("style", "opacity: 1;");
      });
    function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
    }
  </script>
</body>
<!-- END BODY -->
</html>

                            