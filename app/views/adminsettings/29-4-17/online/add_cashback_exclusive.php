<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Casahback Exclusive | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>

	<link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/boostrap-select/bootstrap-select.min.css" media="screen" > 	
	<link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php $this->load->view('adminsettings/header'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
    <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
     <?php $this->load->view('adminsettings/sidebar'); ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
          <!-- BEGIN PAGE HEADER-->   
          <div class="row-fluid">
              <div class="span12">
                <!-- END THEME CUSTOMIZER-->
               <!--  <h3 class="page-title">Add Cashback Exclusive</h3> -->
                <!-- <ul class="breadcrumb">
                  <li>
                    <a href="http://localhost/pingou/adminsettings/dashboard"><i class="icon-home"></i></a>
                    <span class="divider">&nbsp;</span>
                  </li>
                  <li>
                    <a href="http://localhost/pingou/adminsettings/cashback_exclusive">Cashback Exclusive</a>
                    <span class="divider">&nbsp;</span>
                  </li>
                  <li>
                    <a href="http://localhost/pingou/adminsettings/add_cashback_exclusive">Add Cashback Exclusive</a>
                    <span class="divider-last">&nbsp;</span>
                  </li>
                </ul> -->
              </div>
          </div>
          <!-- END PAGE HEADER-->
          <!-- BEGIN PAGE CONTENT-->
          <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM widget-->
                  <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-file"></i>Add Cashback Exclusive</h4>
                        <span class="tools">
                           <a href="javascript:;" class="icon-chevron-down"></a>
                           <!--<a href="javascript:;" class="icon-remove"></a>-->
                        </span>
                    </div>
                    <br>
					          <span>
                      <span class="required_field"> &nbsp;&nbsp;&nbsp;*</span> marked fields are mandatory.
                    </span>
                    <br>
					          <div class="widget-body form">
                        <?php
                        $get_allusers = $this->admin_model->get_allusers(); 
              					$error = $this->session->flashdata('error');
              					if($error!="") 
                        {
              						echo '<div class="alert alert-error">
              						<button data-dismiss="alert" class="close">x</button>
              						<strong>Error! </strong>'.$error.'</div>';
              					}
                        
                        $success = $this->session->flashdata('success');
              					if($success!="")
                        {
              						echo '<div class="alert alert-success">
              					  <button data-dismiss="alert" class="close">x</button>
              						<strong>Success! </strong>'.$success.'</div>';
              					}
                        ?>
                        <!-- BEGIN FORM-->
            						<?php
            						$attribute = array('role'=>'form','method'=>'post','id'=>'cash_exclusive','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
            						echo form_open('adminsettings/add_cashback_exclusive',$attribute);
            						?>
						
                          <div class="control-group">
                            <label class="control-label">Link Name<span class="required_field">*</span></label>
                            <div class="controls">
                              <input type="text" class="span6" name="link_name" id="link_name" value="" placeholder="" required />  ex: ?pro=name-blog-Netshoes
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label " >User Email<span class="required_field">*</span>
                            </label>
                            <div class="controls">
                              <select name="user_id" required class="span6 selectpicker" data-live-search="true"><?php
                							 	foreach($get_allusers as $users)
                								{
                							 	  ?>							     
                								  <option <?php if($passing_userid==$users->user_id){ echo "selected=selected";}
                                  ?> value="<?php echo $users->email;?>"><?php echo $users->email;?></option>
                	                <?php
                								}
                							  ?>
                              </select>
                            </div>
                          </div>
                          <?php
                          $aff = $this->admin_model->affiliates();
                          ?>
                          <div class="control-group">
                            <label class="control-label">Store Name </label>
                            <div class="controls">
                              <select name="affiliate_name" class="span6">
                                <?php
                                foreach($aff as $res)
                                {
                                  ?>                  
                                  <option value="<?php echo $res->affiliate_name;?>"><?php echo $res->affiliate_name;?></option>
                                  <?php
                                }
                                ?>
                              </select>
                            </div>
                          </div>   
                          <div class="control-group">
                            <label class="control-label">Cashback Type</label>
                            <div class="controls">
                              <select name="affiliate_cashback_type" class="span6">
                                <option value="Percentage">Percentage</option>
                                <option value="Flat">Flat</option>
                                <!-- <option value="Desativado">Desativado</option> -->
                              </select>
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">Cashback for user <span class="required_field">*</span></label>
                            <div class="controls">
                              <input type="text" required class="span6" name="cashback_percentage" id="cashback_percentage" value="<?php if($sess_cashback_percentage!=""){ echo $sess_cashback_percentage;  }?>" />  
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">Cashback for user in the Android App <span class="required_field">*</span></label>
                            <div class="controls">
                              <input type="text" required class="span6" name="cashback_percent_android" id="cashback_percent_android" />  
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">Content of the store Extra tracking Paramenter <span class="required_field">*</span></label>
                            <div class="controls">
                              <textarea class="span6" required name="content_extra_param" id="content_extra_param" ></textarea>
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">Content of the store Extra tracking Paramenter in ANDROID APP </label>
                            <div class="controls">
                              <textarea class="span6" required name="content_extra_param_android" id="content_extra_param__android" ></textarea>
                            </div>
                          </div>

                          <!-- new code for cashback_exclusive new url details 1-8-16.-->
                          <div class="control-group">
                            <label class="control-label">Analytics Information <span class="required_field">*</span></label>
                            <div class="controls">
                              <input type="text" required class="span6" name="analytics_info" id="analytics_info" />  
                            </div>
                          </div>
                          <!-- End -->


                          <div class="control-group">
                            <label class="control-label">Start Date <span class="required_field">*</span></label>
                            <div class="controls">
                              <input type="text" required class="datepicker" name="start_date" placeholder="From Day (mm/dd/aaaa)" value="" id="startdatepicker">
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">End Date <span class="required_field">*</span></label>
                            <div class="controls">
                              <input type="text" required class="datepicker" name="end_date"   placeholder="To Day (mm/dd/aaaa)" value="" id="enddatepicker">
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">Notification For Users (After the link Expires)</label>
                              <div class="controls">
                                <textarea rows="5" class="span6" name="notify_desk" id="notify_desk" ></textarea>
                              </div>
                          </div>

                          <!-- new code for notification for users destop and mobile description details 21-4-17 -->
                          <div class="control-group">
                            <label class="control-label">Notification on Desktop(Store)</label>
                              <div class="controls">
                                <textarea rows="5" class="span6" name="desk_notification" id="desk_notification" ></textarea>
                              </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">Notification on Mobile/APP(Store)</label>
                              <div class="controls">
                                <textarea rows="5" class="span6" name="mob_notification" id="mob_notification" ></textarea>
                              </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">Redirect Page Notification Desktop</label>
                              <div class="controls">
                                <textarea rows="5" class="span6" name="desk_redir_notify" id="desk_redir_notify" ></textarea>
                              </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">Redirect Page Notification Mobile/APP</label>
                              <div class="controls">
                                <textarea rows="5" class="span6" name="mob_redir_notify" id="mob_redir_notify" ></textarea>
                              </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">Old cashback <span class="required_field">*</span></label>
                            <div class="controls">
                              <input type="text" required class="span6" name="old_cashback" id="old_cashback" />  
                            </div>
                          </div>
                          <!-- end 21-4-17 -->

                          <br>
                          <br>
                          <!-- New code 22-4-17 -->
                          <div class="control-group">
                            <label class="control-label" style="width:220px !important;">Click count on exclusive Link: </label>
                            <div class="controls" style="margin-top: 6px;">
                            -  
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label" style="width:220px !important;">Sign-ups made through the link: </label>
                            <div class="controls" style="margin-top: 6px;">
                            -  
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label" style="width:220px !important;">Conversion rate: </label>
                            <div class="controls" style="margin-top: 6px;">
                            -  
                            </div>
                          </div>

                          <div class="control-group">
                            <input type="checkbox" name="valid_purchase_status" id="valid_purchase_status" class="span6" value="1" style="opacity: 0;">
                            Valid only for user that never made a purchase in pingou
                          </div>
                          <div class="control-group">
                            <input type="checkbox" name="campaign_email_status" id="campaign_email_status" class="span6" value="1" style="opacity: 0;">
                            Send email to user when campaign finishes (expiration date)
                          </div>
                          <div class="control-group">
                            <input type="checkbox" name="valid_num_status" id="valid_num_status" class="span6" value="1" style="opacity: 0;">
                            Valid only if the number 
                            <input type="text" name="five_digit_number" id="five_digit_number" style="width:100px;">&nbsp;&nbsp; there is at most &nbsp;&nbsp;
                            <input type="text" name="digits_counts" id="digits_counts" style="width:60px;">&nbsp;&nbsp; times in five-digits column of the table "cashback" 
                          </div>
                          <div class="control-group">
                            <input type="checkbox" name="indique_status" id="indique_status" class="span6" value="1" style="opacity: 0;">
                            Display campaign statistics in the user /indique-e-ganhe page 
                          </div>

                          <div class="control-group">
                            <label class="control-label">Campaign Name<br>(USER SIDE)<span class="required_field">*</span></label>
                            <div class="controls">
                              <input type="text" required class="span6" name="campaign_name" id="campaign_name" />  
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">Pingou email<br>(USER SIDE)<span class="required_field">*</span></label>
                            <div class="controls">
                              <input type="text" required class="span6" name="pingou_mail" id="pingou_mail" />  
                            </div>
                          </div>

                          <div class="control-group">
                            <label class="control-label">Campaign Description</label>
                              <div class="controls">
                                <textarea rows="5" class="span6" name="campaign_desc" id="campaign_desc" ></textarea>
                              </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">Campaign Comments</label>
                              <div class="controls">
                                <textarea rows="5" class="span6" name="campaign_comments" id="campaign_comments" ></textarea>
                              </div>
                          </div>
                          <!-- End 22-4-17 -->
  						            <div class="form-actions">
                            <input type="submit" name="save" value="Submit" class="btn btn-success">
                          </div>

						            <?php echo form_close(); ?>
                        <!--</form>-->
                        <!-- END FORM-->
                    </div>
                  </div>
                  <!-- END SAMPLE FORM widget-->
                </div>
          </div>
        </div>
        <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
    </div>
   
  <?php $this->load->view('adminsettings/footer'); ?>
 
   
      <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
   </script>
 <script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){


$(function () {
    $('#startdatepicker, #enddatepicker').datepicker({
        beforeShow: customRange,
        dateFormat: "dd/mm/yy",
        firstDay: 1,
        changeFirstDay: false
    });
});

function customRange(input) {
    var min = null, // Set this to your absolute minimum date
        dateMin = min,
        dateMax = null,
        dayRange = 30; // Restrict the number of days for the date range
    
    if ($('#select1').val() === '2') {
        if (input.id === "startdatepicker") {
            if ($("#enddatepicker").datepicker("getDate") != null) {
                dateMax = $("#enddatepicker").datepicker("getDate");
                dateMin = $("#enddatepicker").datepicker("getDate");
                dateMin.setDate(dateMin.getDate() - dayRange);
                if (dateMin < min) { dateMin = min; }
            } else {  }
        } else if (input.id === "enddatepicker") {
            dateMin = $("#startdatepicker").datepicker('getDate');
            dateMax = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() + 30);
            if ($('#startdatepicker').datepicker('getDate') != null) {
                var rangeMax = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() + dayRange);
                if (rangeMax < dateMax) { dateMax = rangeMax; }
            }
        }
    } else if ($('#select1').val() != '2') {
        if (input.id === "startdatepicker") {
            if ($('#enddatepicker').datepicker('getDate') != null) {
                dateMin = null;
            } else {  }
        } else if (input.id === "enddatepicker") {
            dateMin = $('#startdatepicker').datepicker('getDate');
            dateMax = null;
            if ($('#startdatepicker').datepicker('getDate') != null) { dateMax = null; }
        }
    }
    return {
        minDate: dateMin,
        maxDate: dateMax
    };
}

$('.datepicker').datepicker('widget').delegate('.ui-datepicker-close', 'mouseup', function() {
    var inputToBeCleared = $('.datepicker').filter(function() { 
      return $(this).data('pickerVisible') == true;
    });    
    $(inputToBeCleared).val('');
});
});//]]>  

</script>




<script>
function bluramount(max_shipping_time)
 {
  max_shipping_time = max_shipping_time.replace(/[^0-9\.]/g,'');
  if(max_shipping_time.split('.').length>2) 
  max_shipping_time = max_shipping_time.replace(/\.+$/,"");
  $('#transation_amount').val(max_shipping_time);
 }
</script>
<script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/boostrap-select/bootstrap-select.min.js"></script>
<script>
$(document).ready(function(e) 
{ 
  $('.selectpicker').selectpicker();
});
</script>
   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>