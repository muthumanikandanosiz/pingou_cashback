<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <?php $admin_details = $this->admin_model->get_admindetails(); ?>
   <title>Referrals | <?php echo $admin_details->site_name; ?> Admin</title>
   <?php $this->load->view('adminsettings/script'); ?>

   <link href="<?php echo base_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/uniform/css/uniform.default.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php $this->load->view('adminsettings/header'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
    <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->
      	<div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                    Referrals > Category Three
                    </h3>
                    <ul class="breadcrumb">
                       <li>
                           <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
						   <span class="divider">&nbsp;</span>
                       </li>
                       <li><?php echo anchor('adminsettings/referrals','Referrals'); ?>
						<span class="divider">&nbsp;</span>
					   </li>
					   <li> 
							Category Three<span class="divider-last">&nbsp;</span>
                       </li>
                   </ul>
                  <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <!-- BEGIN ADVANCED TABLE widget-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> Category Three</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>
                        <div class="widget-body form">
						 	
						 	<?php 
							$error = $this->session->flashdata('error');
							if($error!="") 
							{
								echo '<div class="alert alert-error">
								<button data-dismiss="alert" class="close">x</button>
								<strong>Error! </strong>'.$error.'</div>';
							}
								$success = $this->session->flashdata('success');
								if($success!="")
								{
									echo '<div class="alert alert-success">
									<button data-dismiss="alert" class="close">x</button>
									<strong>Success! </strong>'.$success.'</div>';			
								}
							 
							$attribute = array('role'=>'form','method'=>'post','class'=>'form-horizontal'); 
							echo form_open('adminsettings/updatecategorythree',$attribute);
							?>
                        	<br><br>

	                          	<div class="control-group"> 
	                               	<b>Referral by percentage</b>
								  	<select required="" style="width:10%;margin-left:1%;" name="refpercentage">
									  <option value="0" <?php if($ref_by_percentage=='0'){ echo 'selected="selected"'; }?>>De active</option>
									  <option value="1" <?php if($ref_by_percentage=='1'){ echo 'selected="selected"'; }?>>Active</option>
								    </select>
	                               	<b style="margin-left:2%;">Referral Cashback</b>
								  	<input type="text" required style="width:10%; margin-left:3%;" value="<?php echo $ref_cashback; ?>" name="refcashback" id="refcashback" onkeypress="return isNumberKey(event)">
	                                %
	                                <b style="margin-left:2%;">Valid for</b>
	                                <input type="text" style="width:10%; margin-left:1%;" required name="validmonth" value="<?php echo $valid_months; ?>" id="validmonth" onkeypress="return isNumber(event)">
	                                <b style="margin-left:1%;">Days after the registration date.</b> <!-- Month -->
	                            </div>
	                            <hr style="border-width:4px !important;"></br>


	                            <div class="control-group"> 
	                               	<b>Referral by Rate</b>
								  	<select required="" style="width:10%;margin-left:5%;"  name="refbyrate">
									  <option value="0" <?php if($ref_by_rate=='0'){ echo 'selected="selected"'; }?>>De active</option>
									  <option value="1" <?php if($ref_by_rate=='1'){ echo 'selected="selected"'; }?>>Active</option>
								    </select>
	                               	<b style="margin-left:2%;">Referral Cashback R$</b>
								  	<input type="text" style="width:10%; margin-left:1%;" name="refcashback_rate" value="<?php echo $ref_cashback_rate;?>" id="refcashback_rate" required  onkeypress="return isNumberKey(event)">	                                
	                                <b style="margin-left:1%;">Just when the friend requests his first withdraw.</b>
	                            </div>
	                            <hr style="border-width:4px !important;"><br>


	                           <div class="control-group"> 
	                               	<b>Bonus by Referral Rate</b>&nbsp;
								  	<select required="" style="width:10%;" name="bonus_rate">
									  <option value="0" <?php if($bonus_by_ref_rate=='0'){ echo 'selected="selected"'; }?>>De active</option>
									  <option value="1" <?php if($bonus_by_ref_rate=='1'){ echo 'selected="selected"'; }?>>Active</option>
								    </select>
	                               	<b style="margin-left:2%;">Referral Cashback R$</b>&nbsp;
								  	<input type="text" style="width:10%; margin-left:1%;"  value="<?php echo $ref_cashback_rate_bonus;?>" name="refcashback_rate_bonus" id="refcashback_rate_bonus" required  onkeypress="return isNumberKey(event)">
	                                
	                                <b style="margin-left:2%;">Just when </b>
	                                <input type="text" style="width:10%; margin-left:1%;" value="<?php echo $friends_count;?>"  name="friends_count" id="friends_count" required  onkeypress="return isNumber(event)">
	                                <b style="margin-left:1%;">referred friends have successfully</b><br><b style="margin-left:78%;"> requested their first withdraws. </b>
	                            </div>
	                            <br>
	                            
	                            <center><b>This bonus is valid only ONE time per account. If a user ever has received this bonus, he may never received it again.</b></center> 
	                            <hr style="border-width:4px !important;"> <br>
	                            <input type="hidden" name="cat_type" id="cat_type" value="<?php echo $cat_type; ?>">

<script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
                              
                              <div class="control-group" style="margin-left:10%;"> 
                                <b>Category Description</b>
                                <div style="margin-left:16%;">
                                  <textarea required="" id="cat_description" rows="3" name="cat_description" class="span6 ckeditor"><?php echo $cat_description;?></textarea>
                                </div>
	                            </div><br><br>
                              
                              <div class="control-group" style="margin-left:10%;">
                                <b>Users in this category</b>
                                <input type="email" style="margin-left:1%; width:39%;" placeholder="Email_address" value=""  name="usermail" id="usermail">
                                <input type="button" name="addsuer" onclick="submit_user()"  value="Add user" class="btn btn-success">
                                <span id="success"></span>
                              </div><br> 
                              <b style="margin-left:10%;">Current Users (by Id Number)</b><br>
                              <br>
                              <div id="show" style="width: 36%; margin-left: 25%;">
                                <table class="table table-striped table-bordered dataTable" style="width:auto !important;">
                                  <?php 
                                  $i=0;$Row_Count = count($users);
                                  
                                  foreach($users as $user)
                                  {$i++;
                                    
                                    $Mod= $i % 12;
                                    

                                    if($Mod == 1){
                                      ?>
                                      <tr>
                                      <?php
                                    }
                                  ?>  
                                      <td><?php echo $user->user_id; ?>&nbsp;&nbsp;&nbsp;<?php
                                          $confirm = array("class"=>"confirm-dialog","onclick"=>"return confirmDelete('Do you want to delete this user?');");
                                          echo anchor('adminsettings/delete_user/'.$user->user_id,'<i class="icon-trash"></i>',$confirm); ?>
                                      </td>
                                  <?php 
                                  if($Mod == 0 || $i == $Row_Count){

                                    ?>
                                    </tr>
                                    <?php
                                  }
                                  }
                                  ?> 
                                </table>   
                              </div> 
                              <br><br>
                              <center><input type="submit" name="save" value="Save Changes" class="btn btn-success">
                           		</center>



						    <?php echo form_close(); ?>
                         
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>
            <!-- END ADVANCED TABLE widget-->
            <!-- END PAGE CONTENT-->
        </div>
         <!-- END PAGE CONTAINER-->
      	</div>
      <!-- END PAGE -->
    </div>
     
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
   <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo base_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>   
   <script src="<?php echo base_url(); ?>assets/js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->   
   <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
    <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
   </script>
 
</body>
<!-- END BODY -->
</html>
<script>
        $(document).ready(function() {
                setInterval(function() {
                        
                        $('#show').val();
                }, 2000);
        });
</script>
<script>    
function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function submit_user()
{
  var name = document.getElementById('usermail').value;
  var cat_type = document.getElementById('cat_type').value;
  
  if(name=='')
  { 
    $('#usermail').css("border","2px solid red");
  }
  else if(name!='')
  {
    $('#usermail').css("border","none");
    $.ajax({
       type: "POST",
        url: '<?php echo base_url();?>adminsettings/add_user', 
        data: {mailid: name,cat_type:cat_type},
        dataType:"text", 
        success: 
          function(data)
          { 
            if(data==1)
            {
              $('#usermail').val('');
              $('#success').css("color","green");
              $('#success').text('User added Successfully!');
              location.reload(true);
            }
            else
            {
              $('#success').css("color","red");
              $('#success').text('InValid Email id');
            } 
          }
    });
  }
}
</script>
