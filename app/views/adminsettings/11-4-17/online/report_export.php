<!DOCTYPE html>
<html lang="en"> 
<head>
  <meta charset="utf-8" />
  <?php 
    
    $admin_details = $this->admin_model->get_admindetails(); 
    $bank_details  = $this->admin_model->bank_details();
    //print_r($bank_details);
  ?>
  <title>Export Report | <?php echo $admin_details->site_name; ?> Admin</title>
    <?php $this->load->view('adminsettings/script'); ?>

       <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"  />
       <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
       <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
       <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
       <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
       <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
       <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
       <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
       <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
       <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
       <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
       <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
       <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />
</head>
<style type="text/css">
  .btn.btn-success {
    margin-left: 10px;
}
</style>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
  <!-- BEGIN SIDEBAR -->
  <?php $this->load->view('adminsettings/sidebar'); ?>
  <!-- END SIDEBAR -->
  <!-- BEGIN PAGE -->  
    <div id="main-content">
    <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
      <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
          <!--div span12 start-->
          <div class="span12" style="margin-top:13px;"> 
            <!-- <h3 class="page-title">
              Export Reports
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
			          <span class="divider">&nbsp;</span>
              </li>
              <li>
        				<?php echo anchor('adminsettings/bulk_store','Export Reports'); ?>
        				<span class="divider-last">&nbsp;</span>
              </li>
            </ul> -->
            <span style="float:left;">
              <a href="<?php echo base_url();?>adminsettings/reports" class="btn btn-success">View Report</a> &nbsp;
            </span>
          </div>
          <!--div span12 End-->
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
          <div class="span12">
          <!-- BEGIN SAMPLE FORM widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-file"></i> Export Reports</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                </span>
              </div> 
				      
          		<?php
        			if($action=="new")
              {
        			?>
                <div class="widget-body form">
               	  <?php 
            	    $error = $this->session->flashdata('error');
            		  if($error!="")
                  {
              			echo '<div class="alert alert-error">
              		  <button data-dismiss="alert" class="close">x</button>
              			<strong>Error! </strong>'.$error.'</div>';
            			}
                  
                  $success = $this->session->flashdata('success');
            			if($success!="")
                  {
            				echo '<div class="alert alert-success">
            				<button data-dismiss="alert" class="close">x</button>
            				<strong>Success! </strong>'.$success.'</div>';
            			}
                  ?>
                  <!-- BEGIN FORM-->
  						    <!-- <h4>1. Choose what report you want to export</h4><br> -->
                  <div class="row" style="margin-left:0px; !important; ">
                    <div class="span12">
                      <!-- <ul class="nav nav-tabs ul-edit responsive" style="border-bottom:none;">
                        <li class="active" style="width:14%; margin-left:	0px;"><a data-toggle="tab" href="#tab-pending-cashback" class="btn btn-success1">Cashback</a></li>
                        <li style="margin-left:10px; width:16%;" class=""><a data-toggle="tab" href="#tab-missing-cashback" class="btn btn-success1">Missing Cashback</a> </li>
                        <li style="margin-left:10px; width:16%;" class=""><a data-toggle="tab" href="#tab-withdraws" class="btn btn-success1">Withdraws</a></li>
                        <li style="margin-left:10px; width:16%;" class=""><a data-toggle="tab" href="#tab-payments" class="btn btn-success1">Payments</a></li>
                        <li style="margin-left:10px; width:16%;" class=""><a data-toggle="tab" href="#tab-subscribes" class="btn btn-success1">Subscribers</a></li>
                        <li style="margin-left:10px; width:16%;" class=""><a data-toggle="tab" href="#tab-users" class="btn btn-success1">Users</a></li>
                      </ul> --> 
                         
  						  
                     <!--  <h4>2. Choose the filders to generate export</h4><br> -->
                      <div class="row" style="margin-left:0px; !important; ">
                        <div class="span12">
                          <div class="tab-content">
                          <!--Pending Cashback Menu Start -->
                            <?php 
                            if($export_type =="cashback")
                            {
                              ?>
                              <h3> Export Cashback details Report</h3><br>
                              <h4> Choose the filders to generate export</h4><br> 
                              <?php
                              //form begin
                              $attributes = array('role'=>'form','name'=>'profile_form','id'=>'profile_form','method'=>'post','class'=>'form-horizontal');
                              echo form_open('adminsettings/export_report',$attributes);
                              ?>  
                                <div class="span3" style="">
                                  <h4>Stores</h4>
                                  <div class="form-group">
                                    <select name="store_name" class="form-control" id="store_name">
                                      <option value="All">All</option>
                                      <?php 
                                      //print_r($cashback);
                                      foreach($cashback as $newcashback) 
                                      {
                                      ?>
                                        <option value="<?php echo $newcashback->affiliate_name ?>"><?php echo $newcashback->affiliate_name ?></option>
                                      <?php
                                      }
                                      ?>
                                    </select>
                                  </div>  
                                </div> 

                                <div class="span4" style="">
                                  <h4>Times Intervals</h4>
                                  <div class="form-group">
                                    <input type="text" class="datepicker form-control hasDatepicker" name="start_date" placeholder="From Day (mm/dd/aaaa)" value="" id="startdatepicker"><br><br>
                                    <input type="text" class="datepicker form-control hasDatepicker" name="end_date"   placeholder="To Day (mm/dd/aaaa)" value="" id="enddatepicker">
                                  </div>
                                </div>

                                <div class="span4">
                                  <h4>Specfic User Id's</h4>
                                  <div class="form-group">
                                    <textarea id="user_id" class="span6" rows="3" style="width:100%;" name="user_id"></textarea>
                                  </div>
                                </div>
                                <!-- status details added 11-7-16 -->
                                <div class="span3">
                                  <h4 style="float:right; margin-right:200px">Status</h4>
                                  <div class="form-group" style="float:right; margin-right:35px">
                                    <select name="status" id="status">
                                      <option value="All">All</option> 
                                      <option value="Canceled">Canceled</option>
                                      <option value="Completed">Completed</option>
                                      <option value="Pending">Pending</option>
                                    </select>
                                  </div>
                                </div>
                                <!-- end -->

                                <input type="hidden" name="type" value="pending_cashback">
                                <div class="span12" style="margin-left: 56% !important; margin-top:10%;">
                                  <input type="submit" class="btn btn-success" value="Download Reports" name="save">
                                  <input type="submit" class="btn btn-success" value="Download Reports in XLS" name="savexls">
                                </div>
                              <?php echo form_close();?>
                                <!-- form End -->
                              <?php 
                            }
                            ?>
                          <!--Pending cashback Menu End-->  

                          <!--Missing cashback Menu start-->
                            <?php 
                            if($export_type=="missing_cashback")
                            {
                              ?>
                              <h3> Export Missing Cashback details Report</h3><br>
                              <h4> Choose the filders to generate export</h4><br> 
                              <!-- <div class="tab-pane fade" id="tab-missing-cashback"> -->
                                <?php
                                //form begin
                                $attributes = array('role'=>'form','name'=>'profile_form','id'=>'profile_form','method'=>'post','class'=>'form-horizontal');
                                echo form_open('adminsettings/export_report',$attributes);
                                ?>  
                                <div class="span3" style="">
                                  <h4>Stores</h4>
                                  <div class="form-group">
                                    <select name="store_name" class="form-control" id="store_name">
                                      <option value="All">All</option>
                                      <?php 
                                      //print_r($cashback);
                                      foreach($cashback as $newcashback)
                                      {
                                      ?>
                                        <option value="<?php echo $newcashback->affiliate_name ?>"><?php echo $newcashback->affiliate_name ?></option>
                                      <?php
                                      }
                                      ?>
                                    </select>
                                  </div>  
                                </div>

                                <div class="span3">
                                  <h4>Specfic User Id's</h4>
                                  <div class="form-group">
                                    <textarea id="user_id" class="span6" rows="3" style="width:90%;" name="user_id"></textarea>
                                  </div>
                                </div>

                                <div class="span3">
                                  <h4>Status</h4>
                                  <div class="form-group">
                                    <select name="status" id="status">
                                      <option value="All">All</option> 
                                      <option value="0">Success</option>
                                      <option value="1">Cancelled</option>
                                      <option value="2">Send to Retailer</option>
                                      <option value="3">Pending</option>
                                    </select>
                                  </div>
                                </div>  
                                
                                <div class="span3" style="margin-left:1%;">
                                  <h4>Times Intervals</h4>
                                  <div class="form-group">
                                    <input type="text" class="datepicker form-control hasDatepicker" name="start_date" placeholder="From Day (mm/dd/aaaa)" value="" id="startdatepicker"><br><br>
                                    <input type="text" class="datepicker form-control hasDatepicker" name="end_date"   placeholder="To Day (mm/dd/aaaa)" value="" id="enddatepicker">
                                  </div>
                                </div>
                                <br>     
                                <input type="hidden" name="type" value="missing_cashback">
                                <div class="span12" style="margin-left: 56% !important; margin-top:10%;">
                                  <input type="submit" class="btn btn-success" value="Download Reports" name="save">
                                    <input type="submit" class="btn btn-success" value="Download Reports in XLS" name="savexls">
                                </div>
                                <?php echo form_close();?>
                              </div>
                              <?php 
                            }
                            ?>
                          <!--Missing cashback menu End-->

                          <!-- Withdraw Menu Start-->
                            <?php 
                            if($export_type=="withdraw")
                            {
                              ?>
                              <h3> Export Withdraw details Report</h3><br>
                              <h4> Choose the filders to generate export</h4><br> 
                              <!-- <div class="tab-pane fade" id="tab-withdraws"> -->
                                <?php
                                //form begin
                                $attributes = array('role'=>'form','name'=>'profile_form','id'=>'profile_form','method'=>'post','class'=>'form-horizontal');
                                echo form_open('adminsettings/export_report',$attributes);
                                ?>                            
                                  <div class="span3">
                                    <h4>Status</h4>
                                    <div class="form-group">
                                    <select name="status" id="status"> 
                                      <option value="All">All</option>
                                      <option value="requested">Request</option>
                                      <option value="processing">Processing</option>
                                      <option value="completed">Completed</option>
                                      <option value="cancelled">Cancelled</option>
                                    </select>
                                    </div>
                                  </div>
                                  <div class="span3">
                                    <h4>Bank Name</h4>
                                    <div class="form-group">
                                    <select name="bank_name" id="bank_name"> 
                                      <option value="All">All</option>
                                       <?php 
                                        //print_r($bank_details);
                                        
                                        foreach($bankdetails as $newbank_details) { ?>
                                          <option value="<?php echo $newbank_details->bank_name; ?>"><?php echo $newbank_details->bank_name; ?></option>
                                        <?php } ?>
                                    </select>
                                    </div>
                                  </div>
                                  <div class="span3">
                                    <h4>Times Intervals</h4>
                                    <div class="form-group">
                                      <input type="text" class="datepicker form-control hasDatepicker" name="start_date" placeholder="From Day (mm/dd/aaaa)" value="" id="startdatepicker">
                                    </div>
                                  </div>

                                  <!--<div class="span3" style="">
                                    <h4>Bank to be deposited</h4>
                                      <div class="form-group">
                                        <select name="bank_name" class="form-control" id="bank_name">
                                        <?php 
                                        foreach($bankdetails as $newdata) { ?>
                                          <option value="<?php echo $newdata->bank_id?>"><?php echo $newdata->bank_name?></option>
                                        <?php } ?>
                                        </select>
                                      </div>  
                                  </div>-->

                                  <div class="span3" style="margin-left:1%;">
                                    <h4>Times Intervals</h4>
                                    <div class="form-group">
                                      <input type="text" class="datepicker form-control hasDatepicker" name="end_date" placeholder="To day (mm/dd/aaaa)" value="" id="enddatepicker">
                                    </div>
                                  </div>
                                  <input type="hidden" name="type" value="withdraws">
                                  <div class="span12" style="margin-left: 56% !important; margin-top:10%;">
                                    <input type="submit" class="btn btn-success" value="Download Reports" name="save">
                                  
                                    <input type="submit" class="btn btn-success" value="Download Reports in XLS" name="savexls">
                                  </div>
                                <?php echo form_close();?>  
                              <!-- </div> -->
                              <?php 
                            }
                            ?>
                          <!-- Withdraw Menu End-->

                              <!--Payment Menu Start-->
                            <?php 
                            if($export_type=="payment")
                            {
                              ?>
                              <h3> Export Payment details Report</h3><br>
                              <h4> Choose the filders to generate export</h4><br> 
                              <!-- <div class="tab-pane fade" id="tab-payments"> -->
                                <?php
                                //form begin
                                $attributes = array('role'=>'form','name'=>'profile_form','id'=>'profile_form','method'=>'post','class'=>'form-horizontal');
                                echo form_open('adminsettings/export_report',$attributes);
                                ?>  
                                <div class="span3">
                                  <h4>Transaction Status</h4>
                                  <div class="form-group">
                                    <select name="status" id="status"> 
                                      <option value="all">All</option>
                                      <option value="Approved">Approved</option>
                                      <option value="Canceled">Canceled</option>
                                      <option value="Pending">Pending</option>
                                      <option value="Paid">Paid</option>
                                      <option value="Progress">Progress</option>
                                    </select>
                                  </div>
                                </div> 

                                <div class="span3">
                                  <h4>Specfic User Id's</h4>
                                  <div class="form-group">
                                    <textarea id="user_id" class="span6" rows="3" style="width:90%;" name="user_id"></textarea>
                                  </div>
                                </div> 

                                <div class="span3" style="margin-left:1%;">
                                  <h4>Transaction Reason</h4>
                                  <div class="form-group">
                                    <input type="text" class="form-control" name="trans_reason" value="" autocomplete='off'>
                                  </div>
                                </div>

                                <!--<div class="span3" style="">
                                  <h4>Store</h4>
                                  <div class="form-group">
                                      <select name="storename" class="form-control" id="bank_name">
                                        <?php foreach($cashback as $newcashback) { ?>
                                        <option value="<?php echo $newcashback->affiliate_name?>"><?php echo $newcashback->affiliate_name?></option>
                                        <?php } ?>
                                      </select>
                                    </div>  
                                  </div>-->
       
                                  <div class="span3" style="margin-left:1%;">
                                    <h4>Times Intervals</h4>
                                    <div class="form-group">
                                     <input type="text" class="datepicker form-control hasDatepicker" name="start_date" placeholder="From Day (mm/dd/aaaa)" value="" id="startdatepicker"><br><br>
                                      <input type="text" class="datepicker form-control hasDatepicker" name="end_date"   placeholder="To Day (mm/dd/aaaa)" value="" id="enddatepicker"><br><br>
                                    </div>
                                  </div>

                                  <div class="span3" style="margin-left:1%;">
                                    <h4>Mode</h4>
                                    <div class="form-group">
                                      <select name="mode" id="mode"> 
                                        <option value="all">All</option>
                                        <option value="Credited">Credited</option>
                                        <option value="Debited">Debited</option>
                                    </select>
                                    </div>
                                  </div> 
                                  <input type="hidden" name="type" value="payments">
                                  <div class="span12" style="margin-left: 56% !important; margin-top:10%;">
                                    <input type="submit" class="btn btn-success" value="Download Reports" name="save">
                                 
                                    <input type="submit" class="btn btn-success" value="Download Reports in XLS" name="savexls">
                                  </div>
                                  <?php echo form_close();?>
                              </div>
                              <?php 
                            }
                            ?>
                          <!-- Payment menu End-->

                          <!-- Subscribes menu start-->

                            <?php 
                            if($export_type=="subscribers")
                            {
                              ?>
                              <h3> Export Subscribers details Report</h3><br>
                              <h4> Choose the filders to generate export</h4><br>
                                <?php
                                //form begin
                                $attributes = array('role'=>'form','name'=>'profile_form','id'=>'profile_form','method'=>'post','class'=>'form-horizontal');
                                echo form_open('adminsettings/export_report',$attributes);
                                ?>  
                                  <div class="span3">
                                    <h4>Time interval</h4>
                                    <div class="form-group">
                                     <input type="text" class="datepicker form-control hasDatepicker" name="start_date" placeholder="From Day (mm/dd/aaaa)" value="" id="startdatepicker"><br><br>
                                      <input type="text" class="datepicker form-control hasDatepicker" name="end_date"   placeholder="To Day (mm/dd/aaaa)" value="" id="enddatepicker"><br><br>
                                    </div>
                                  </div>

                                  <div class="span3">
                                    <h4>Subscribers status</h4>
                                    <div class="form-group">
                                      <select name="status" id="status"> 
                                        <option value="all">All</option>
                                        <option value="1">Active</option>
                                        <option value="0">In-active</option>
                                    </select>
                                    </div>
                                  </div>
                                  <input type="hidden" name="type" value="subscribers">
                                  <div class="span12" style="margin-left: 56% !important; margin-top:10%;">
                                    <input type="submit" class="btn btn-success" value="Download Reports" name="save">
                                    <input type="submit" class="btn btn-success" value="Download Reports in XLS" name="savexls">
                                  </div>
                                <?php echo form_close();?>
                              </div>
                              <?php 
                            }
                            ?>  

                          <!--Subscribes menu page end-->
                          
                          <!-- SATz Users-->
                              
                          <!-- New changes for reports export for users detials 6-5-16--> 
                            <?php 
                            if($export_type=="users")
                            {
                              ?>    
                                <h3> Export User details Report</h3><br>
                                <h4> Choose the filders to generate export</h4><br>
                                
                                <?php
                                //form begin
                                $attributes = array('role'=>'form','name'=>'profile_form','id'=>'profile_form','method'=>'post','class'=>'form-horizontal');
                                echo form_open('adminsettings/export_report',$attributes);
                                ?>  
                                 
                                  <div class="span3">
                                    <h4>Account status</h4>
                                    <div class="form-group">
                                      <select name="acc_status" id="acc_status"> 
                                        <option value="All">All</option>
                                        <option value="1">Active</option>
                                        <option value="0">Deactive</option>
                                    </select>
                                    </div>
                                  </div>

                                  <div class="span3">
                                    <h4>Referral Category</h4>
                                    <div class="form-group">
                                    	<select name="category_type" id="category_type">
                                    	  <option value="All">All</option>
                                        <option value="1">CategoryOne</option>
                                        <option value="2">CategoryTwo</option>
                                        <option value="3">CategoryThree</option>
                                        <option value="4">CategoryFour</option>
                                        <option value="5">CategoryFive</option>
                                      </select>
                                      <input type="hidden" name="type" value="users">      
                                    </div>
                                  </div>

                                  <div class="span3">
                                    <h4>Login using App</h4>
                                    <div class="form-group">
                                      <select name="login_app" id="login_app"> 
                                        <option value="All">All</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                      </select>
                                    </div>
                                  </div>
                                   
                                  <div class="span3">
                                    <h4>Available balance is Zero?</h4>
                                    <div class="form-group">
                                      <select name="bal_status" id="bal_status"> 
                                        <option value="All">All</option>
                                        <option value="0">Yes</option>
                                        <option value="2">No</option>
                                      </select>
                                    </div>
                                  </div>
                                
                                
                                  <div class="span3" style="margin-left:0 !important; margin-top: 20px;">
                                    <h4>Newsletter Email</h4>
                                    <div class="form-group">
                                      <select name="news_status" id="news_status"> 
                                        <option value="All">All</option>
                                        <option value="1">Active</option>
                                        <option value="0">Deactive</option>
                                      </select>
                                    </div>
                                  </div>
                                   
                                  <div class="span3" style="margin-top: 20px;">
                                    <h4>Register Via Referral</h4>
                                    <div class="form-group">
                                      <select name="ref_status" id="ref_status"> 
                                        <option value="All">All</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                      </select>
                                    </div>
                                  </div>
                                   
                                  <div class="span3" style="margin-top: 20px;">
                                    <h4>Bank information</h4>
                                    <div class="form-group">
                                      <select name="bank_status" id="bank_status"> 
                                        <option value="All">All</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                      </select>
                                    </div>
                                  </div>
                                 
                                  <input type="hidden" name="type" value="users">
                         
                                  <div class="span12" style="margin-left: 37.2% !important; margin-top:10%;">
                                    <input type="submit" class="btn btn-success" value="Download Reports" name="save">
                                   
                                    <input type="submit" class="btn btn-success" value="Download Reports in XLS" name="savexls">
                                   
                                    <input type="submit" class="btn btn-success" value="Download only users emails in csv" name="userdetail">
                                  </div>
                                <?php echo form_close();?>  
                              <?php 
                            }
                            ?>  
                             
                          <!-- SATz users end --> 
                          </div>
                        </div>
                      </div>  
                    </div>      
                  </div>
                </div>  
				      <?php
              } 
              ?>
            </div>
                  <!-- END SAMPLE FORM widget-->
          </div>
        </div>
      </div>
         <!-- END PAGE CONTAINER-->
    </div>
      <!-- END PAGE -->  
  </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
    <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
   </script>
 <script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){


$(function () {
    $('#startdatepicker, #enddatepicker').datepicker({
        beforeShow: customRange,
        dateFormat: "dd/mm/yy",
        firstDay: 1,
        changeFirstDay: false
    });
});

function customRange(input) {
    var min = null, // Set this to your absolute minimum date
        dateMin = min,
        dateMax = null,
        dayRange = 30; // Restrict the number of days for the date range
    
    if ($('#select1').val() === '2') {
        if (input.id === "startdatepicker") {
            if ($("#enddatepicker").datepicker("getDate") != null) {
                dateMax = $("#enddatepicker").datepicker("getDate");
                dateMin = $("#enddatepicker").datepicker("getDate");
                dateMin.setDate(dateMin.getDate() - dayRange);
                if (dateMin < min) { dateMin = min; }
            } else {  }
        } else if (input.id === "enddatepicker") {
            dateMin = $("#startdatepicker").datepicker('getDate');
            dateMax = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() + 30);
            if ($('#startdatepicker').datepicker('getDate') != null) {
                var rangeMax = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() + dayRange);
                if (rangeMax < dateMax) { dateMax = rangeMax; }
            }
        }
    } else if ($('#select1').val() != '2') {
        if (input.id === "startdatepicker") {
            if ($('#enddatepicker').datepicker('getDate') != null) {
                dateMin = null;
            } else {  }
        } else if (input.id === "enddatepicker") {
            dateMin = $('#startdatepicker').datepicker('getDate');
            dateMax = null;
            if ($('#startdatepicker').datepicker('getDate') != null) { dateMax = null; }
        }
    }
    return {
        minDate: dateMin,
        maxDate: dateMax
    };
}

$('.datepicker').datepicker('widget').delegate('.ui-datepicker-close', 'mouseup', function() {
    var inputToBeCleared = $('.datepicker').filter(function() { 
      return $(this).data('pickerVisible') == true;
    });    
    $(inputToBeCleared).val('');
});
});//]]>  

</script>
   <!-- END JAVASCRIPTS -->
<style>
li .btn-success1
{
  height:40px; background: #87bb33;padding-bottom: 0px !important; border-radius: 4px 4px 4px 4px !important;
padding-top: 0px !important; line-height:40px !important;
}
</style>   
</body>
<!-- END BODY -->
</html>
