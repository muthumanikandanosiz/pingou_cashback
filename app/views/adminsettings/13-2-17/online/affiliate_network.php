<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Affiliate Network | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>
  <link href="<?php echo base_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/uniform/css/uniform.default.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <?php $api_details = $this->db->query("SELECT * from affiliates_list where `affiliate_network`='".$affiliate_name."'")->result(); 
    //echo "<pre>"; print_r($api_details); 
    ?>
      
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
          <div class="span12">
            <h3 class="page-title">
              <?php echo ucfirst($affiliate_name); ?> API
            </h3>
            <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
						    <span class="divider">&nbsp;</span>
              </li>
              <li><?php echo anchor('adminsettings/affiliate_network','Affiliate Network'); ?>
							  <span class="divider-last">&nbsp;</span>
					    </li>
            </ul>
            <?php //echo anchor('adminsettings/addaffiliate_list','<button style="float:right" class="btn btn-success">Add Affiliate Network</button>'); ?> 
            <!-- END PAGE TITLE & BREADCRUMB-->
          <span style="float:left;margin-bottom:3%; margin-top:3%;">
            <!-- 
            <select>
              <option name=""></option>
              <option name=""></option>
              <option name=""></option>
            </select>
            -->
            <a onClick="return fun_import('<?php echo $api_details[0]->api_key; ?>','<?php echo $api_details[0]->networkid; ?>','<?php echo $api_details[0]->status; ?>','<?php echo date("Y-m-d"); ?>','<?php echo date(DATE_RFC822);?>');" title="Import Coupons" name="import" id="import" class="btn btn-success" href="javascript:void(0);">Synchronize with <?php echo ucfirst($affiliate_name); ?></a> &nbsp;
          </span>
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <!-- BEGIN ADVANCED TABLE widget-->
        
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN EXAMPLE TABLE widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i> <?php echo ucfirst($affiliate_name); ?> API</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                </span>
              </div>

              <div class="widget-body">
    						<span class="msg"></span>
    						<?php 
      					$error = $this->session->flashdata('error');
      					if($error!="") 
                {
      						echo '<div class="alert alert-error">
      						<button data-dismiss="alert" class="close">x</button>
      						<strong>Error! </strong>'.$error.'</div>';
      					}
    						$success = $this->session->flashdata('success');
    						if($success!="") 
                {
    							echo '<div class="alert alert-success">
    							<button data-dismiss="alert" class="close">x</button>
    							<strong>Success! </strong>'.$success.'</div>';			
    						} 
						    ?>
                <form id="form2" action="" method="post" name="form2">
                    <table class="table table-striped table-bordered" id="sample_1">
                      <thead>
                        <tr>
                          <th><center>#</center></th>
                          <th><center>Store Name </center></th> 
                          <th class="hidden-phone"><center>Start Date </center></th>
                          <th class="hidden-phone"><center>Expires On </center></th>
    				              <th class="hidden-phone"><center>Title </center></th>
                          <th class="hidden-phone"><center>Link</center></th>
                          <th class="hidden-phone"><center>Code</center></th>
                          <th class="hidden-phone"><center>Description</center></th>
                          <th class="hidden-phone"><center>Actions</center></th>
                          <!-- <th class="hidden-phone">Delete</th> -->
                        </tr>
                      </thead>
                      <tbody>
          							<?php
          							$k=0;
          							if($affiliate_network)
                        {
            							foreach($affiliate_network as $affiliate)
                          {
            							  $k++;
            							  ?>
                            <tr class="odd gradeX">
                              <td><center><?php echo $k;?></center></td>
            									<td><center><?php echo $affiliate->offer_name; ?></center></td>
            									<td><center><?php echo date('d-m-Y', strtotime($affiliate->start_date)); ?></center></td>
                              <td><center><?php echo $affiliate->expiry_date; ?></center></td>
                              <td><center><?php echo $affiliate->title; ?></center></td>
                              <td><center><a href="<?php echo $affiliate->offer_page;?>">Link</a></center></td>
                              <td><center><?php echo $affiliate->code; ?></center></td>
                              <td><center><?php echo $affiliate->description; ?></center></td>
      									       
                              <!-- <td class="hidden-phone"><center>
      									        <img src="<?php echo base_url();?>assets/img/loadaing.gif" id="import_loader" style="display:none;">
      									        <a href="javascript:void(0);"  name="import" id="import" onClick="return fun_import('<?php echo $affiliate->api_key; ?>','<?php echo $affiliate->networkid; ?>','<?php echo $affiliate->status; ?>','<?php echo date("Y-m-d"); ?>','<?php echo date(DATE_RFC822);?>');" title="Import Coupons"><i class="icon-cloud-download"></i></a></center>
                              </td> -->
      								         
      									      <!-- <td class="hidden-phone"><center>
        									      <img src="<?php echo base_url();?>assets/img/loadaing.gif" id="report_loader" style="display:none;">
        									      <a href="javascript:void(0);"  name="report" id="report" onClick="return fun_fetchreport('<?php echo $affiliate->api_key; ?>','<?php echo $affiliate->networkid; ?>','<?php echo $affiliate->status; ?>','<?php echo date("Y-m-d"); ?>','<?php echo date(DATE_RFC822) ;?>');"  title="Upload reports"><i class="icon-upload"></i></i></a></center>
                              </td> -->
      									      
                              <!-- <td>
              									<?php
              									$status = $affiliate->status;
              										if($status=='1'){
              											echo 'Activated';
              										}else{
              											echo 'De-activated';
              										}
              									?>
      									      </td> -->
                              <td>
                                <?php
                                $confirm = array("class"=>"confirm-dialog","onclick"=>"return confirmDelete('Do you want to change this approval status?');");    
                                echo anchor('adminsettings/api_change_approval/'.$affiliate->coupon_id.'/'.$affiliate->coupon_status.'/'.$affiliate->api_name,'<i class="icon-exchange"></i>',$confirm); ?>&nbsp;
                                <?php $attr =array('title'=>'Edit');
              									echo anchor('adminsettings/editcoupon/'.$affiliate->coupon_id.'/'.$affiliate->api_name,'<i class="icon-pencil"></i>',$attr); ?>            									            
                                <?php $confirm = array("class"=>"confirm-dialog",'title'=>'Delete',"onclick"=>"return confirmDelete('Do you want to delete this affiliate detail?');");   
                                echo anchor('adminsettings/delete_affiliate/'.$affiliate->coupon_id,'<i class="icon-trash"></i>',$confirm); ?> 
                              </td>
                              <!-- <td class="center hidden-phone">
              									<?php
              									$confirm = array("class"=>"confirm-dialog",'title'=>'Delete',"onclick"=>"return confirmDelete('Do you want to delete this affiliate detail?');");		
              									echo anchor('adminsettings/delete_affiliate/'.$affiliate->id,'<i class="icon-trash"></i>',$confirm); ?>
            									</td> -->
                            </tr>
      								      <?php
                          }
                        }
                        ?>
                      </tbody>
                    </table>						
        						<input type="hidden" name="tracking_id" value="<?php echo $affiliate->tracking_id;?>" id="tracking_id">
                    <input type="hidden" name="api_name" value="<?php echo $affiliate_name;?>" id="api_name">
                    <input type="hidden" name="hidd" value="hidd">
                    <!-- <input id="GoUpdate" class="btn btn-warning" type="submit" value="Delete Affiliate Networks" name="GoUpdate"> -->
                </form>
              </div>
            </div>
            <!-- END EXAMPLE TABLE widget-->
          </div>
        </div>
        <!-- END ADVANCED TABLE widget-->
        <!-- END PAGE CONTENT-->
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
  <!-- END FOOTER -->
  <!-- BEGIN JAVASCRIPTS -->
  <!-- Load javascripts at bottom, this will reduce page load time -->
  <script src="<?php echo base_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>   
  <script src="<?php echo base_url(); ?>assets/js/jquery.blockui.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->   
  <!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/data-tables/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/data-tables/DT_bootstrap.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
  <script>
     jQuery(document).ready(function() {
         // initiate layout and plugins
         App.init();
      });
   </script>
<script type="text/javascript">
function confirmDelete(m)  // Confirm before delete cms..
{
	if(!confirm(m))
	{
		return false;
	}
	else
	{
		return true;
	}
}
</script>
<script>
      $(document).ready(function() {
		$(".check_b").attr("style", "opacity: 1;");
      });
	  function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }
 </script>
<script>
function fun_import(secretkey,connectid,status,date)
{
  var api_name = $('#api_name').val();
	//$('#import').hide();
	$('#import_loader').show();
	if(status==1)
	{
		var tracking_id =$('#tracking_id').val();
		$.ajax({
			type:'POST',
			dataType: 'JSON',
			url: '<?php echo base_url();?>adminsettings/get_coupons/'+secretkey+'/'+connectid+'/'+date+'/'+api_name,
			data:'secretkey='+secretkey+'&connectid='+connectid+'&tracking_id='+tracking_id+'&status='+status+'&date='+date+'&api_name='+api_name,
			success:function(results){				
				$('.msg').html('<div class="alert alert-success"><button data-dismiss="alert" class="close">x</button><strong>'+results['success']+'</strong></div>');
				$('#import_loader').hide();
				//$('#import').show();
				$("html, body").animate({ scrollTop: 0 }, "slow");
				setTimeout(function(){ window.location="<?php echo base_url();?>adminsettings/affiliate_network/"+api_name; }, 3000);
		
			}
		});
	}
	else {
		$('.msg').html('<div class="alert alert-error"><button data-dismiss="alert" class="close">x</button><strong>Affiliate Network is De-activated.</strong></div>');
        $('#import_loader').hide();
		//$('#import').show();		
	}
	
}
</script>
<!-- upload the report automatically 
 seetha 21.09.2015-->
 <script>
function fun_fetchreport(apikey,netwrkid,status)
{
	//alert(apikey);
	$('#report').hide();
	$('#report_loader').show();
	 if(status==1)
	 {
		$.ajax({
			type:'POST',
			dataType: 'JSON',
			url: '<?php echo base_url();?>adminsettings/fetch_report/'+apikey+'/'+netwrkid,
			data:'apikey='+apikey+'&netwrkid='+netwrkid+'&status='+status,
			success:function(results){				
				$('.msg').html('<div class="alert alert-success"><button data-dismiss="alert" class="close">x</button><strong>'+results['success']+'</strong></div>');	
				$('#report_loader').hide();
				$('#report').show();
				$("html, body").animate({ scrollTop: 0 }, "slow");
		
			}
		});
	}
	else {
		$('.msg').html('<div class="alert alert-error"><button data-dismiss="alert" class="close">x</button><strong>Affiliate Network is De-activated.</strong></div>');
		$('#report_loader').hide();
		$('#report').show();
	}
	
}
</script>
</body>
<!-- END BODY -->
</html>