<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Content | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>

  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
      <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN THEME CUSTOMIZER-->
            <!--<div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
            <span class="settings">
            <span class="text">Theme:</span>
            <span class="colors">
            <span class="color-default" data-style="default"></span>
            <span class="color-gray" data-style="gray"></span>
            <span class="color-purple" data-style="purple"></span>
            <span class="color-navy-blue" data-style="navy-blue"></span>
            </span>
            </span>
            </div>-->
            <!-- END THEME CUSTOMIZER-->
            <!-- <h3 class="page-title">
            Exit Popup
            </h3>
            <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
						    <span class="divider">&nbsp;</span>
              </li>
              <li>
							  <?php echo anchor('adminsettings/exit_popup','Exit Popup'); ?>
							  <span class="divider">&nbsp;</span>
              </li>
					    <li>
							  Exit Popup <span class="divider-last">&nbsp;</span>
              </li>
            </ul> -->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN SAMPLE FORM widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-file"></i> Unic Bonus Features For Extension</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                  <!--<a href="javascript:;" class="icon-remove"></a>-->
                </span>
              </div>
              <br>
				      <span><span class="required_field"> &nbsp;&nbsp;&nbsp;*</span> marked fields are mandatory.</span><br>
              <div class="widget-body form">
      					<?php 
      					$error = $this->session->flashdata('error');
      					if($error!="") 
                {
      						echo '<div class="alert alert-error">
      						<button data-dismiss="alert" class="close">x</button>
      						<strong>Error! </strong>'.$error.'</div>';
  					    }
    						$success = $this->session->flashdata('success');
    						$sess_subject = $this->session->flashdata('subject');
    						if($success!="") 
                {
    							echo '<div class="alert alert-success">
    							<button data-dismiss="alert" class="close">x</button>
    							<strong>Success! </strong>'.$success.'</div>';
    						}
                ?>
                <!-- BEGIN FORM-->
  						  <?php
  							$attribute = array('role'=>'form','name'=>'unic_bonus','method'=>'post','id'=>'unic_bonus','class'=>'form-horizontal'); 
  							echo form_open('adminsettings/update_unic_bonus',$attribute);
  						  ?>
  						  <h4>Content for Not Logged Users</h4>
                <br>
                 
    					  <input type="hidden" name="content_id" id="content_id" value="<?php echo $content_id; ?>">
                
    					  <div class="control-group">
                  <label class="control-label">Not logged Content <span class="required_field">*</span></label>
                  <div class="controls">
                    <textarea class="span6 ckeditor" name="not_log_content" id="not_log_content" ><?php echo $not_log_content; ?></textarea>
                  </div>
                </div>
               <!--  <div class="control-group">
                  <label class="control-label">Status <span class="required_field">*</span></label>
                  <div class="controls">
                    <select  name="all_status" id="all_status" required class="span6">
                      <option value="0" <?php if($all_status=='0'){ echo 'selected="selected"'; }?>>Disabled</option>
                      <option value="1" <?php if($all_status=='1'){ echo 'selected="selected"'; }?>>Enabled</option>
                    </select>
                  </div>
                </div> -->
                <br>
                <h4>Content For Logged Users</h4><br>

                <div class="control-group">
                  <label class="control-label">Logged  Content <span class="required_field">*</span></label>
                  <div class="controls">
                    <textarea class="span6 ckeditor" name="logged_content" id="logged_content" ><?php echo $logged_content; ?></textarea>
                  </div>
                </div>
               <!--  <div class="control-group">
                  <label class="control-label">Status <span class="required_field">*</span></label>
                  <div class="controls">
                    <select  name="all_status" id="all_status" required class="span6">
                      <option value="0" <?php if($all_status=='0'){ echo 'selected="selected"'; }?>>Disabled</option>
                      <option value="1" <?php if($all_status=='1'){ echo 'selected="selected"'; }?>>Enabled</option>
                    </select>
                  </div>
                </div> -->   
  							<div class="form-actions">
                  <input type="submit" name="save" value="Save Changes" class="btn btn-success">
                </div>
  						  <?php echo form_close(); ?>
                <!-- END FORM-->
              </div>
				    </div>
            <!-- END SAMPLE FORM widget-->
          </div>
        </div>
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->  
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
  <!-- END FOOTER -->
  <!-- BEGIN JAVASCRIPTS -->
  <!-- Load javascripts at bottom, this will reduce page load time -->
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
   </script>

   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>