<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php
  $admin_details   = $this->admin_model->get_admindetails();
  ?>
  <title>Admin Settings | <?php echo $admin_details->site_name; ?> Admin</title>
  <?php $this->load->view('adminsettings/script'); ?>

  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
          <div class="span12">
            <!-- <h3 class="page-title">
            Layout Settings
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
                <span class="divider">&nbsp;</span>
              </li>
              <li>Marketing<span class="divider">&nbsp;</span></li>
              <li>
                <?php echo anchor('adminsettings/layout','Layout'); ?>
                <span class="divider-last">&nbsp;</span>
              </li>
            </ul> -->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
          <div class="span12">
                <!-- BEGIN SAMPLE FORM widget-->
                <div class="widget">
                  <div class="widget-title">
                    <h4><i class="icon-cog"></i> Layout Settings</h4>
                    <span class="tools">
                      <a href="javascript:;" class="icon-chevron-down"></a>
                      <!--<a href="javascript:;" class="icon-remove"></a>-->
                    </span>
                  </div><br>
                  <span>
                    <span class="required_field"> &nbsp;&nbsp;&nbsp;*</span> marked fields are mandatory.
                  </span>
                  <br>
                  <div class="widget-body form">
                    <?php 
                      $error = $this->session->flashdata('error');
                      if($error!="")
                      {
                        echo '<div class="alert alert-error">
                        <button data-dismiss="alert" class="close">x</button>
                        <strong>Error! </strong>'.$error.'</div>';
                      }
                      $success = $this->session->flashdata('success');
                      if($success!="") 
                      {
                        echo '<div class="alert alert-success">
                        <button data-dismiss="alert" class="close">x</button>
                        <strong>Success! </strong>'.$success.'</div>';      
                      }
                    ?>
                    <!-- BEGIN FORM-->
                    <?php
                      $attribute = array('role'=>'form','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data','onSubmit' =>'return validation();' ); 
                      echo form_open('adminsettings/layoutupdate',$attribute);
                    ?>


                      <!--New code for background image and color settings for index page-->
                      <font color="red">*Front end background settings*</font>
                      <br><br>
                      <div class="control-group">
                        <label class="control-label">Frontend Background <span class="required_field">*</span></label>
                        <div class="controls">
                          <select name="backtype" id="backtype" required class="span6"> <!--onChange="change_background(this.value);"-->
                            <option value="image" <?php if($background_type=='image'){ echo 'selected="selected"'; }?>>Image</option>
                            <option value="color" <?php if($background_type=='color'){ echo 'selected="selected"'; }?>>Color</option>
                          </select>
                        </div>
                      </div>
                      <div class="control-group" id="back_image">
                        <label class="control-label">Frontend Background Image</label>
                        <div class="controls">
                          <input type="file" class="span6" name="background_image" id="background_image" /><br>
      								    <span>Note: Frontend Background Image should be in (150 * 150) in size</span><br>
      								    <img src="<?php echo $this->admin_model->get_img_url(); ?>uploads/adminpro/<?php echo $background_image; ?>" width="150" height="250">
                        </div>
                      </div>
                      <div class="control-group" id="back_color"> <!-- style="display:none"-->
                        <label class="control-label">Frontend Background Color <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="text" name="background_color" id="background_color" required value="<?php echo $background_color;?>"  class="span6">
                        </div>
                      </div>
                      <!--End-->

                      <br>
                      <!--New code for background image and color settings in store page-->
                      <font color="red">*Store page cover photo settings*</font>
                      <br><br>
                      <div class="control-group">
                          <label class="control-label">Store page Header Background <span class="required_field">*</span></label>
                          <div class="controls">
                            <select  name="covertype" id="covertype" required class="span6"> <!--onChange="change_coverphoto(this.value);"-->
                              <option value="coverimage" <?php if($storecover_type=='coverimage'){ echo 'selected="selected"'; }?>>Image</option>
                              <option value="covercolor" <?php if($storecover_type=='covercolor'){ echo 'selected="selected"'; }?>>Color</option>
                            </select>
                          </div>
                      </div>
                      <div class="control-group" id="coverimage">
                          <label class="control-label">Store page Background Image</label>
                          <div class="controls">
                            <input type="file" class="span6" name="cover_image" id="cover_image" /><br>
                            <span>Note: Store page Background Image should be in (150 * 150) in size</span><br>
                            <img src="<?php echo $this->admin_model->get_img_url(); ?>uploads/adminpro/<?php echo $storecover_image; ?>" width="150" height="250">
                          </div>
                      </div>

                      <!-- new code for background image settings in store page 18-8-16-->
                      <div class="control-group">
                          <label class="control-label">Settings of the Store page background image <span class="required_field">*</span></label>
                          <div class="controls">
                            <select  name="back_img_type" id="back_img_type" required class="span6">
                              <option value="zoomimage" <?php if($store_back_img_settings=='zoomimage'){ echo 'selected="selected"'; }?>>Extend the image to the screen size</option>
                              <option value="repeatimage" <?php if($store_back_img_settings=='repeatimage'){ echo 'selected="selected"'; }?>>Repeat image (side by side) while maintaining the orginal size </option>
                            </select>
                          </div>
                      </div>
                      <!-- End 18-8-16-->


                      <div class="control-group" id="covercolor"> <!-- style="display:none"-->
                          <label class="control-label">Store page Background Color <span class="required_field">*</span></label>
                          <div class="controls">
                            <input type="text" name="cover_color" id="cover_color" required value="<?php echo $storecover_color;?>" class="span6">
                          </div>
                      </div>
                      <!--End-->

                      <!--New code for background image and color settings in topcashback page 26-04-16-->

                      <font color="red">*Top cashback page cover photo settings*</font>
                      <br><br>
                      <div class="control-group">
                        <label class="control-label">Topcashback page Header Background <span class="required_field">*</span></label>
                        <div class="controls">
                          <select  name="topcashbacktype" id="topcashbacktype" required class="span6"> <!--onChange="change_coverphoto(this.value);"-->
                            <option value="topcashback_image" <?php if($topcashback_type=='topcashback_image'){ echo 'selected="selected"'; }?>>Image</option>
                            <option value="topcashback_color" <?php if($topcashback_type=='topcashback_color'){ echo 'selected="selected"'; }?>>Color</option>
                          </select>
                        </div>
                      </div>
                      <div class="control-group" id="coverimage">
                        <label class="control-label">Top cashback page Background Image</label>
                        <div class="controls">
                          <input type="file" class="span6" name="topcashbackimage" id="topcashbackimage" /><br>
                          <span>Note: Top cashback page Background Image should be in (150 * 150) in size</span><br>
                          <img src="<?php echo $this->admin_model->get_img_url(); ?>uploads/adminpro/<?php echo $topcashback_image; ?>" width="150" height="250">
                        </div>
                      </div>
                      <!-- new code for background image settings in top-cashback page 19-8-16-->
                      <div class="control-group">
                        <label class="control-label">Settings of the Top cashback page background image <span class="required_field">*</span></label>
                        <div class="controls">
                          <select  name="top_back_img_type" id="top_back_img_type" required class="span6">
                            <option value="top_zoomimage" <?php if($top_back_img_settings == 'top_zoomimage'){ echo 'selected="selected"'; }?>>Extend the image to the screen size</option>
                            <option value="top_repeatimage" <?php if($top_back_img_settings == 'top_repeatimage'){ echo 'selected="selected"'; }?>>Repeat image (side by side) while maintaining the orginal size </option>
                          </select>
                        </div>
                      </div>
                      <!-- End 18-8-16-->
                      <div class="control-group" id="covercolor"> <!-- style="display:none"-->
                        <label class="control-label">Top cashback page Background Color <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="text" name="topcashbackcolor" id="topcashbackcolor" required value="<?php echo $topcashback_color;?>" class="span6">
                        </div>
                      </div>
                      <div class="control-group" id="coverimage">
                        <label class="control-label">Image of Top cashback</label>
                        <div class="controls">
                          <input type="file" class="span6" name="imagetopcashback" id="imagetopcashback" /><br>
                          <img src="<?php echo $this->admin_model->get_img_url(); ?>uploads/adminpro/<?php echo $image_topcashback; ?>" width="150" height="250">
                        </div>
                      </div>
                      <!--Pilaventhiran 02/05/2016 Start-->
                      <div class="control-group">
                          <label class="control-label">Notification on Desktop(Store)</label>
                          <div class="controls">
                            <textarea class="span6 ckeditor" name="notify_desk" id="notify_desk" ><?php echo $notify_desk; ?></textarea>
                          </div>
                      </div>

                      <!-- meta image property for social share (facebook) 11-11-16 -->
                      <div class="control-group">
                        <label class="control-label">Meta image for Pingou Site</label>
                        <div class="controls">
                          <input type="file" class="span6" name="pingou_img" id="pingou_img" /><br>
                          <!-- <span>Note: Admin Avatar should be in (250 * 250) in size</span> --><br>
                          <img src="<?php echo $this->admin_model->get_img_url(); ?>uploads/adminpro/<?php echo $admin_details->pingou_meta_image; ?>" width="150" height="250">
                        </div>
                      </div>
                 
                      <div class="control-group">
                        <label class="control-label">Meta image for Pingou Store page</label>
                        <div class="controls">
                          <input type="file" class="span6" name="store_img" id="store_img" /><br>
                          <!-- <span>Note: Admin Avatar should be in (216 * 79) in size</span> --><br>
                          <img src="<?php echo $this->admin_model->get_img_url(); ?>uploads/adminpro/<?php echo $admin_details->storepage_meta_image; ?>" width="150" height="250"> 
                        </div>
                      </div>
                      <!-- End 11-11-16 -->

                      <!-- New code for account tab status trigger 17-3-17-->
                      <div class="control-group">
                        <label class="control-label">Account page status</label>
                        <div class="controls">
                            <select  name="acc_page_status" id="acc_page_status" required class="span6">
                              <option value="0" <?php if($acc_page_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                              <option value="1" <?php if($acc_page_status=='1'){ echo 'selected="selected"'; }?>>Enable </option>
                            </select>
                          </div>
                      </div>
                      <!-- end 17-3-17 -->
                      
                      
                      <input type="hidden" name="pingou_meta_img" id="pingou_meta_img" value="<?php echo $admin_details->pingou_meta_image; ?>">
                      <input type="hidden" name="store_meta_img" id="store_meta_img" value="<?php echo $admin_details->storepage_meta_image; ?>">
                      <input type="hidden" name="username" id="username" value="<?php echo $admin_username; ?>">
                      <input type="hidden" name="admin_id" id="admin_id" value="<?php echo $admin_id; ?>">
                      <input type="hidden" name="hidden_site_background_image"  id="hidden_site_background_image"  value="<?php echo $background_image; ?>">
                      <input type="hidden" name="hidden_store_cover_image"      id="hidden_store_cover_image"      value="<?php echo $storecover_image; ?>">
                      <input type="hidden" name="hidden_site_topcashback_image" id="hidden_site_topcashback_image" value="<?php echo $topcashback_image; ?>">
                      <input type="hidden" name="hidden_site_image_topcashback" id="hidden_site_image_topcashback" value="<?php echo $image_topcashback; ?>">

                      <div class="form-actions">
                        <input type="submit" name="save" value="Save Changes" class="btn btn-success">
                      </div>
                   
                    <?php echo form_close(); ?>
                    <!--</form>-->
                    <!-- END FORM-->
                  </div>
                </div>
                  <!-- END SAMPLE FORM widget-->
          </div>
        </div>
      </div>
         <!-- END PAGE CONTAINER-->
    </div>
      <!-- END PAGE -->  
  </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
  <!-- SATz -->
  <!-- Time picker -->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>front/js/moment.js"></script>   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>front/js/combodate.js"></script>
  <script>
  $(function(){
      $('#time').combodate({
          firstItem: 'name', //show 'hour' and 'minute' string at first item of dropdown
          minuteStep: 1
      });  
  });
  </script>



<!-- SATz  -->
   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
    function change_background(background)
    {
      if(background=='color') 
      {
        $("#back_image").hide();
        $("#back_color").show();
      }
      if(background=='image') 
       {
        $("#back_color").hide();
        $("#back_image").show();
      }
    }

    function change_coverphoto(cover)
    {
      if(cover=='covercolor') 
      {
        $("#coverimage").hide();
        $("#covercolor").show();
      }
      if(cover=='coverimage') 
      {
        $("#covercolor").hide();
        $("#coverimage").show();
      }
    }
   
</script>
     <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>
                            