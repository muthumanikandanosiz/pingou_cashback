<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Cashback Exclusive | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>

  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
    <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
      <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
          <div class="span12" style="margin-top:13px;">
            <!-- END THEME CUSTOMIZER-->
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
           <!--  <h3 class="page-title">
              Cashback Exclusive
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
			     	    <span class="divider">&nbsp;</span>
              </li>
              <li><?php echo anchor('adminsettings/cashback_exclusive','Cashback Exclusive'); ?>
							  <span class="divider-last">&nbsp;</span>
					    </li>
            </ul> -->
            <span style="float:left;">
              <a href="<?php echo base_url();?>adminsettings/add_cashback_exclusive" class="btn btn-success">Add Cashback Exclusive</a> &nbsp;
            </span>
            <!-- END PAGE TITLE & BREADCRUMB-->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <!-- BEGIN ADVANCED TABLE widget-->
        <div class="row-fluid">
          <div class="span12">
          <!-- BEGIN EXAMPLE TABLE widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i> Cashback Exclusive</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                </span>
              </div>
              <div class="widget-body">
  						  <?php 
      					$error = $this->session->flashdata('error');
      					if($error!="")
                {
      					  echo '<div class="alert alert-error">
      					  <button data-dismiss="alert" class="close">x</button>
      					  <strong>Error! </strong>'.$error.'</div>';
      					}
      					$success = $this->session->flashdata('success');
      					if($success!="")
                {
      						echo '<div class="alert alert-success">
      						<button data-dismiss="alert" class="close">x</button>
      						<strong>Success! </strong>'.$success.'</div>';			
      					}
                ?>
                <form id="form2" action="" method="post" name="form2">
                  <table class="table table-striped table-bordered" id="sample_1">
                    <thead>
                      <tr>
                        <th>#</th>
                        <!-- <th style=""> <input type="checkbox" id="check_b" class="check_b" onchange="checkAll(this)" name="chk[]" /></th> -->
                        <th class="hidden-phone">Link Name</th> 
                        <th class="hidden-phone">User email</th> 
                        <th class="hidden-phone">Store Name</th>
                        <!-- <th class="hidden-phone">Cashback Type</th>-->
                        <th class="hidden-phone">Cashback on web</th>
                        <!-- <th class="hidden-phone">Cashback on App</th> -->
                        <th class="hidden-phone">Extra parameter in Web</th>
                        <!-- <th class="hidden-phone">Extra parameter in App</th> -->
                        <th class="hidden-phone">Date_added</th>
                        <th class="hidden-phone">Start Date</th>
                        <th class="hidden-phone">End Date</th>
                      <!--   <th class="hidden-phone">Expiry Notification</th> -->
                        <th class="hidden-phone">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
        							<?php
        							$k=0;
        							if($cas_exe)
                      {
          							foreach($cas_exe as $newcas_exe)
                        {
            							$k++;
            							?>
                          <tr class="odd gradeX" style="" >
                            <td style=""><?php echo $k; ?></td>
                            <!-- <td><input type="checkbox"  class="check_b" name="chkbox[<?php echo $affiliate->affiliate_id;?>]" /></td> -->
                            <td><?php echo $newcas_exe->link_name; ?></td>
                            <td><?php echo $newcas_exe->user_email; ?></td>
                            <td><?php echo $newcas_exe->store_name; ?></td>
                            <!--  <td><?php echo $newcas_exe->cashback_type; ?></td> -->
                            <td><?php echo $newcas_exe->cashback_web; ?></td>
                            <!-- <td><?php echo $newcas_exe->cashback_app; ?></td> -->
                            <td><?php echo $newcas_exe->extra_param_web; ?></td>
                            <!-- <td><?php echo $newcas_exe->extra_param_app; ?></td> -->
                            <td><?php echo $newcas_exe->date_added; ?></td>
                            <td><?php echo $newcas_exe->start_date; ?></td>
                            <td><?php echo $newcas_exe->expirydate; ?></td>
                            <!-- <td><?php echo $newcas_exe->start_date; ?></td> -->
                            
                            <td>
        									    <?php echo anchor('adminsettings/edit_cashback_exclusive/'.$newcas_exe->id,'<i class="icon-pencil"></i>'); ?>
        									   &nbsp;|&nbsp;
        									    <?php
        									    $confirm = array("class"=>"confirm-dialog","onclick"=>"return confirmDelete('Do you want to delete this cashback detail?');");		
            									echo anchor('adminsettings/delete_cashback_exclusive/'.$newcas_exe->id,'<i class="icon-trash"></i>',$confirm); ?>
        									  </td>
                          </tr>          
    								      <?php
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                  <input type="hidden" name="hidd" value="hidd">
                  <!-- <input id="GoUpdate" class="btn btn-warning" type="submit" value="Update Retailers" name="GoUpdate"> -->
                </form>
              </div>
            </div>
            <!-- END EXAMPLE TABLE widget-->
          </div>
        </div>
        <!-- END ADVANCED TABLE widget-->
        <!-- END PAGE CONTENT-->
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
  </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
   <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->   
   <!--<script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
   
   <script>
      jQuery(document).ready(function() {
         // initiate layout and plugins
         App.init();
      });
   </script>
<script type="text/javascript">
function confirmDelete(m)  // Confirm before delete cms..
{
	if(!confirm(m))
	{
		return false;
	}
	else
	{
		return true;
	}
}
</script>
<script>
      $(document).ready(function() {
		$(".check_b").attr("style", "opacity: 1;");
      });
	  function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }
 
 
   </script>
</body>
<!-- END BODY -->
</html>
                            