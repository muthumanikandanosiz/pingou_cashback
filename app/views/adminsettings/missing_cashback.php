<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> 
<html lang="en"> 
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Missing cashback</title>
	<?php $this->load->view('adminsettings/script'); ?>

  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/uniform/css/uniform.default.css" />
  <style>
    #sample_1 th
    {
	   text-align:center !important;
	  }
  </style>
  <style>
    div.selector, div.selector span, div.checker span, div.radio span, div.uploader, div.uploader span.action, div.button, div.button span
    {background-image:none;}
    .col-xs-6 {width: 47% !important; float: left;min-height:1px;padding-left: 12px;padding-right: 12px;position: relative;}
    select.input-sm { width:60px !important;}
    .row {margin-left: -12px;margin-right: -12px;}
    .pagination {border-radius: 4px;margin: 20px 0;padding-left: 0;}
  </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
          <div class="span12">
            <!--BEGIN THEME CUSTOMIZER-->
            <!--
              <div id="theme-change" class="hidden-phone">
                <i class="icon-cogs"></i>
                <span class="settings">
                  <span class="text">Theme:</span>
                  <span class="colors">
                    <span class="color-default" data-style="default"></span>
                    <span class="color-gray" data-style="gray"></span>
                    <span class="color-purple" data-style="purple"></span>
                    <span class="color-navy-blue" data-style="navy-blue"></span>
                  </span>
                </span>
              </div>-->
             <!-- END THEME CUSTOMIZER-->
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
           <!--  <h3 class="page-title">
              Missing Cashback
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
			          <span class="divider">&nbsp;</span>
              </li>
              <li><?php echo anchor('adminsettings/missing_cashback','Missing Cashbacks'); ?>
				        <span class="divider-last">&nbsp;</span>
		          </li>
            </ul> -->
            <span style="float:left; margin-top: 15px;">
              <a href="<?php echo base_url();?>adminsettings/report_export/missing_cashback" class="btn btn-success">Export CSV</a> &nbsp;
              <a href="<?php echo base_url();?>adminsettings/report_update/missing_cashback" class="btn btn-success">Update CSV</a> &nbsp;
            </span>
            <!-- END PAGE TITLE & BREADCRUMB-->
         </div>
        </div>
          <!-- END PAGE HEADER-->
          <!-- BEGIN PAGE CONTENT-->
          <!-- BEGIN ADVANCED TABLE widget-->
          <div class="row-fluid">
            <div class="span12">
              <!-- BEGIN EXAMPLE TABLE widget-->
              <div class="widget">
                <div class="widget-title">
                  <h4><i class="icon-reorder"></i> Missing Cashbacks</h4>
                  <span class="tools">
                    <a href="javascript:;" class="icon-chevron-down"></a>
                  </span>
                </div>
                <div class="widget-body">
      						<?php 
      					  $error = $this->session->flashdata('error');
      					  if($error!="")
                  {
        						echo '<div class="alert alert-error">
        						<button data-dismiss="alert" class="close">x</button>
        						<strong>Error! </strong>'.$error.'</div>';
      					  }
      				    $success = $this->session->flashdata('success');
          				if($success!="") 
                  {
        						echo '<div class="alert alert-success">
    								<button data-dismiss="alert" class="close">x</button>
    								<strong>Success! </strong>'.$success.'</div>';			
          				} 
                  ?>
                  <form id="form2" action="" method="post" name="form2">
                    <table class="table table-striped table-bordered" id="sample_teste1">  <!-- sample_1 -->
                      <thead>
                        <tr>
                          <!-- <th>S.No.</th> -->
                            <th style=""><center><input type="checkbox" id="check_b" class="check_b grpall" name="chk[]" /></center></th> <!-- onchange="checkAll(this)"  -->
                            <th>Cashback Id</th>
                            <th>Missing reason</th>
                            <th>User Id</th>
                            <th>User Name</th>
                            <th>User Email</th>
                            <th>Transac Date</th>
                            <th>Retailer Name</th>
                            <th>Transac Amt(Rs.)</th>  
                            <th>Cashback</th>                 
                            <th class="hidden-phone">Status</th>
                            <th class="hidden-phone">Status update Date</th>
                            <th class="hidden-phone">View</th>
                            <th class="hidden-phone">Delete</th>
                            <th>Transaction Ref Id</th> 
                        </tr>
                    </thead>
                    <tbody>
        							<?php
        							/*$k=0;
        							foreach($allmissing_cashbacks as $res)
                      {
          							$k++;
          							$user_details = $this->admin_model->view_user($res->user_id);
          							$fname = $user_details[0]->first_name;
          							$lname = $user_details[0]->last_name;
          							$usname = $fname." ".$lname;
          							$details = $res->status;

                        $affiliate_name = $this->db->query("SELECT affiliate_url FROM affiliates where affiliate_name='$res->retailer_name'")->result();
                         
                        $affiliate_url  = $affiliate_name[0]->affiliate_url;
          							?>
                        <tr class="odd gradeX">
                         
                          <td><center><input type="checkbox"  class="check_b" name="chkbox[<?php echo $res->cashback_id;?>]" /></center></td> 
                          
                          <td><?php echo $res->cashback_id;?></td>
                          <td><?php echo $res->missing_reason; if($res->missing_reason == 'Missing Approval') { echo "<br>ref:".$res->cashback_reference;} ?></td>
                          <td><?php echo $res->user_id;?></td>
                          <td><?php echo $usname;?></td>
                          <td><a href="<?php echo base_url().'adminsettings/view_user/'.$res->user_id ?>" target="_blank"><?php echo $user_details[0]->email;?></a></td>
  									      <td><?php echo $res->trans_date;?></td>
                          <td><a href="<?php echo base_url().'adminsettings/missing_cashback/'.$affiliate_url ?>" target="_blank"><?php echo $res->retailer_name;?></a></td>
                          <td><center><?php echo $this->admin_model->currency_format($res->transation_amount);?></center></td>
                          <td><?php echo $res->ordervalue;?></td>
                          <?php 
      										switch($details)
      										{
      											case 0:
      											?>
      											 <td><a href="" class="btn btn-success btn-xs pop"> Completed </a></td>
      											<?php
      											break;
      											case 1:
      											?>
      											 <td><a href="" class="btn btn-danger btn-xs pop"> Cancelled </a></td>
      											<?php
      											break;
      											case 2:
      											?>
      											<td><a href="" class="btn btn-danger btn-xs pop"> Sent to retailer </a></td>
      											<?php
      											break;
      											case 3:
      											?>
      											 <td><a href="" class="btn btn-danger btn-xs pop"> Created </a></td>
      											<?php
      											break;
                            case 4:
                            ?>
                             <td><a href="" class="btn btn-danger btn-xs pop"> Completed ticket only </a></td>
                            <?php
                            break;
      										}
  									      ?>
                          <td><?php echo $res->status_update_date;?></td>
                          <td class="hidden-phone"><center>
  									        <?php echo anchor('adminsettings/view_missing_cashback/'.$res->cashback_id,'<i class="icon-eye-open"></i>'); ?></center>
  									      </td>
                          <td class="center hidden-phone"><center>
          									<?php
          									$confirm = array("class"=>"confirm-dialog","onclick"=>"return confirmDelete('Do you want to delete this Cashback?');");
          									echo anchor('adminsettings/delete_missing_cashback/'.$res->cashback_id,'<i class="icon-trash"></i>',$confirm); ?></center>
          								</td>
                          <td><?php echo $res->transaction_ref_id;?></td>
                        </tr>
  								      <?php
                      }*/
                      ?>
                    </tbody>
                  </table>
                  <input type="hidden" name="hidd" value="hidd">
                  <input id="GoUpdate" class="btn btn-warning" type="submit" value="Delete Orders" name="GoUpdate">
                </form>
              </div>
            </div>
            <!-- END EXAMPLE TABLE widget-->
          </div>
        </div>
        <!-- END ADVANCED TABLE widget-->
        <!-- END PAGE CONTENT-->
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>   
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->   

  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script> 

  <script type="text/javascript">
    jQuery(document).ready(function() 
    {
       // initiate layout and plugins
       App.init();
    });
  </script>

  <script>

    $(document).ready(function() 
    {
      $(".check_b").attr("style", "opacity: 1;");
    });

    $('input.grpall').click(function()
    {
      if($(this).is(':checked') == true)
      {
        $(this).attr('checked','checked');
        $('input.chksingle').each(function(){
        $(this).attr('checked','checked');
        }) 
      }
      else
      {
        $(this).removeAttr('checked');
        $('input.chksingle').each(function(){
        $(this).removeAttr('checked');
        }) 
      }
      //return false;
    });
  </script>


  <script type="text/javascript">
    function confirmDelete(m)  // Confirm before delete cms..
    {
    	if(!confirm(m))
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }
  </script>

  <script>
     /* $(document).ready(function() {
		$(".check_b").attr("style", "opacity: 1;");
      });
	  function checkAll(ele) 
    {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }*/
 
 
   </script>
<script type="text/javascript">
  $(document).ready(function() {
$('#sample_teste1').DataTable({
"processing": true,
"serverSide": true,
"columnDefs": [{
"targets": 0,
"orderable": false
}],
"ajax": {
"url": "<?php echo site_url('adminsettings/newmissing_cashback')?>",
"data": {
//"totalrecords": "<?php echo $iTotal; ?>"
"user_id":"<?php echo $this->uri->segment(4); ?>",
"aff_name":"<?php echo $this->uri->segment(3); ?>"
}
}
});
});
</script>


</body>
<!-- END BODY -->
</html>
<style type="text/css">
 .dataTables_processing
{
  display:none !important;
}
</style>
 <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/css/pagination.css" rel="stylesheet" />