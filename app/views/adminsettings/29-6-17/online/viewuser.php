<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <?php $admin_details = $this->admin_model->get_admindetails(); ?>
   <title>User Details | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>

   <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />
<style type="text/css">
   .form-actions 
   {
      border-top: 0px !important;
   }
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php $this->load->view('adminsettings/header'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('adminsettings/sidebar'); ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM widget-->
                  <div class="widget">
                     <div class="widget-title">
                        <h4><i class="icon-cog"></i> User Details</h4>
                        <span class="tools">
                           <a href="javascript:;" class="icon-chevron-down"></a>
                           <!--<a href="javascript:;" class="icon-remove"></a>-->
                        </span>
                     </div>
                     <div class="widget-body form">
                        <?php 
         					$error = $this->session->flashdata('error');
         					if($error!="") 
                        {
         						echo '<div class="alert alert-error">
         						<button data-dismiss="alert" class="close">x</button>
         						<strong>Error! </strong>'.$error.'</div>';
         					}
                        $success = $this->session->flashdata('success');
         					if($success!="")
                        {
         					   echo '<div class="alert alert-success">
         						<button data-dismiss="alert" class="close">x</button>
         						<strong>Success! </strong>'.$success.'</div>';			
         					}
                        ?>
                        <!-- BEGIN FORM-->
      						<?php
                        $ref_details  = $this->db->query("SELECT * from referral_settings where ref_id!=''")->result();
                        
                        foreach($ref_details as $newref_details)
                        {
                           $ref_ids     = $newref_details->ref_id;
                        }
                        if($user_detail)
                        {
      							foreach($user_detail as $user) 
                           {
         							$attribute = array('role'=>'form','method'=>'post','class'=>'form-horizontal'); 
         							echo form_open('adminsettings/userupdate',$attribute);
         						   
                              $total_withdraw   = $this->db->query("SELECT COUNT(*) as counting FROM `withdraw` where user_id='$user->user_id'");
                              $total_with_count = $total_withdraw->row('counting');
                              
                              $total_click_ss   = $this->db->query("SELECT COUNT(*) as counting FROM `click_history` where user_id='$user->user_id'");
                              $total_click      = $total_click_ss->row('counting');

                              $approve_cashback_details = $this->db->query("SELECT COUNT(*) as approve_count from cashback where `user_id`=$user->user_id AND `status`='Completed'");
                              $cancel_cashback_details  = $this->db->query("SELECT COUNT(*) as cancel_count  from cashback where `user_id`=$user->user_id AND `status`='Canceled'");
                              $pending_cashback_details = $this->db->query("SELECT COUNT(*) as pending_count from cashback where `user_id`=$user->user_id AND `status`='Pending'");
                              $total_cashback_details   = $this->db->query("SELECT COUNT(*) as total_count   from cashback where `user_id`=$user->user_id"); 

                              $approve_ticket_details   = $this->db->query("SELECT COUNT(*) as approve_count from missing_cashback where `user_id`=$user->user_id AND `status`=0");
                              $cancel_ticket_details    = $this->db->query("SELECT COUNT(*) as cancel_count  from missing_cashback where `user_id`=$user->user_id AND `status`=1");
                              $pending_ticket_details   = $this->db->query("SELECT COUNT(*) as pending_count from missing_cashback where `user_id`=$user->user_id AND `status`=3");
                              $total_ticket_details     = $this->db->query("SELECT COUNT(*) as total_count   from missing_cashback where `user_id`=$user->user_id"); 


                              $top_fivestores           = $this->db->query("SELECT count(click_id) as clickcount,store_name FROM click_history where `user_id`=$user->user_id group by store_name order by count(click_id) desc limit 0,5")->result(); 
                              //echo "<pre>"; print_r($top_fivestores); 
                              if($user->profile != '') 
                              {
                                 $profile_pic = $user->profile;
                              }
                              else
                              {
                                 $profile_pic = $this->admin_model->get_img_url().'uploads/img/default_pro.jpg';  
                              }
                              ?>
                              

                              <!-- new code for edit user info 4-4-17 -->
                              <h4 class="showdetail">Edit User Information <a id="showhide" style="cursor:pointer; text-decoration:none;" onclick="showhide(); return false;"><span class="showstatus">(Show)</span></a></h4>
                              <h4 class="hidedetail" style="display:none;">Edit User Information <a id="hideshow" style="cursor:pointer; text-decoration:none;" onclick="hideshow(); return false;"><span class="hidestatus">(Hide)</span></a></h4>
                              <div class="control-group userdetail" style="display:none;">
                                 <select id="userdetails" class="span4 selectpicker" name="userdetails">
                                    <option value="first_name">First Name</option>
                                    <option value="last_name">Last Name</option>
                                    <option value="email">Email address</option>
                                    <option value="contact_no">Contact Number</option>
                                    <option value="celular_no">Celular Nubmer</option>
                                    <option value="street">Street</option>
                                    <option value="streetnumber">Street Number</option>
                                    <option value="complemento">Complemento</option>
                                    <option value="bairro">Bairro</option>
                                    <option value="city">City</option>
                                    <option value="state">State</option>
                                    <option value="country">Country</option>
                                    <option value="zipcode">Zipcode</option>
                                    <option value="ifsc_code">IFSC Code</option>
                                    <option value="account_holder">Account Holder</option>
                                    <option value="bank_name">Bank Name</option>
                                    <option value="branch_name">Branch Name</option>
                                    <option value="account_number">Account Number</option>
                                 </select>
                                 <span id="other_details">
                                    <input type="text" id="editfield" onblur="" maxlength="" autocomplete="off" name="editfield" class="editfield" style="margin-left:3%;">
                                 </span>
                                 <span id="ifsc_details" style="display:none;">
                                    <input type="text" autocomplete="off" value="" id="ifsc_code" name="ifsc_code" onblur="return check_cpf();" maxlength="14" style="margin-left:3%;" class="form-control comm-accemail">      
                                 </span>
                                 <a onclick="update_now()" class="btn btn-success">UPDATE NOW</a> &nbsp; <span id="unique_name_error"></span>
                                 <p style="margin-left:35%;">You are Updating : <span class="fieldname"></span> Field</p>
                              </div>
                              <!-- End 4-4-17 -->

                              <div class="span6">
                                 <div class="control-group">
                                    <label class="control-label">Profile Image</label>
                                    <div class="controls">
                                       <label class="span6"><img src="<?php echo $profile_pic; ?>" alt="<?php echo $profile_pic; ?>" width='150' height='50'></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">User Id</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->user_id; ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Full Name</label>
                                    <div class="controls">
         								      <label class="span6"><?php echo $user->first_name.' '.$user->last_name; ?></label>
                                    </div>
                                 </div>
   						            <input type="hidden" name="user_id" id="user_id" value="<?php echo $user->user_id; ?>">
   						            <div class="control-group">
                                    <label class="control-label">Email Address</label>
                                    <div class="controls">
                                      <label class="span6"><?php echo $user->email; ?></label>
                                    </div>
                                 </div>
   						            <div class="control-group">
                                    <label class="control-label">Contact Number</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->contact_no; ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Celular Number</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->celular_no; ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Referral Category</label>
                                    <div class="controls">
                                       <!-- <label class="span6"><?php echo "Category ". $user->referral_category_type; ?></label> -->
                                       <select id="ref_category" class="span6 selectpicker" name="ref_category">
                                          <?php
                                          for($i=0;$i<=$ref_ids;$i++)
                                          {
                                             ?>
                                             <option <?php if($user->referral_category_type==$i){ echo 'selected="selected"'; } ?>value="<?php echo $i;?>"><?php echo $i;?></option>
                                             <?php 
                                          }       
                                          ?>
                                       </select>
                                    </div>
                                 </div>
                                 <br>
                                 <!-- new code for cashback details for user 3-4-17-->
                                 <h4><b>Cashback statistics for this user</b></h4>
                                 <table class="table table-striped table-bordered" id="sample_teste1">
                                    <thead>
                                       <tr>
                                          <th>Approved</th>
                                          <th>Canceled</th>
                                          <th>Pending</th>
                                          <th>Total</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr>
                                          <td><?php echo $approve_cashback_details->row('approve_count');?></td>
                                          <td><?php echo $cancel_cashback_details->row('cancel_count');?></td>
                                          <td><?php echo $pending_cashback_details->row('pending_count');?></td>
                                          <td><?php echo $total_cashback_details->row('total_count');?></td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 <!-- End 3-4-17 -->
                                 <!-- new code for support tickets details for user 3-4-17-->
                                 <br>
                                 <h4><b>Support tickets statistics for this user</b></h4>
                                 <table class="table table-striped table-bordered" id="sample_teste1">
                                    <thead>
                                       <tr>
                                          <th>Approved</th>
                                          <th>Canceled</th>
                                          <th>Created/Pending</th>
                                          <th>Total</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr>
                                          <td><?php echo $approve_ticket_details->row('approve_count');?></td>
                                          <td><?php echo $cancel_ticket_details->row('cancel_count');?></td>
                                          <td><?php echo $pending_ticket_details->row('pending_count');?></td>
                                          <td><?php echo $total_ticket_details->row('total_count');?></td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 <!-- End 3-4-17 -->
                                 <br>
                                 <!-- new code for support tickets details for user 3-4-17-->
                                 <br>
                                 <h4><b>Top stores of this user(Based on click number on History)</b></h4>
                                 <?php 
                                 if($top_fivestores)
                                 {
                                    ?>
                                    <table class="table table-striped table-bordered" id="sample_teste1">
                                       <thead>
                                          <tr>
                                             <?php 
                                             foreach($top_fivestores as $newtop_fivestores)
                                             {
                                                ?>
                                                <th><?php echo $newtop_fivestores->store_name; ?></th>
                                                <?php 
                                             }
                                             ?>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr>
                                             <?php 
                                             foreach($top_fivestores as $newnewtop_fivestores)
                                             {
                                                ?>
                                                <td><?php echo $newnewtop_fivestores->clickcount ;?></td>
                                                <?php 
                                             }
                                             ?>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <?php 
                                 }
                                 else
                                 {  
                                    ?>
                                    <table class="table table-striped table-bordered" id="sample_teste1">
                                       <thead>
                                          <tr>
                                             <th>Store Names Not found</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr> 
                                             <td>Stores Click Counts not available</td>  
                                          </tr>
                                       </tbody>
                                    </table>
                                    <?php 
                                 }
                                 ?>
                                 <!-- End 3-4-17 -->

                                 <br>
                                 <div class="control-group">
                                    <label class="control-label">Refered By</label>
                                    <div class="controls">
                                       <label class="span6"><?php if($user->ref_user_cat_type == 0) { echo "User not refered";} else { echo "Category ". $user->ref_user_cat_type . " User"; } ?></label>
                                    </div>
                                 </div>

                                 <!-- referral click count details 20-4-17 -->
                                 <div class="control-group">
                                    <label class="control-label"> Referral Clicks </label>
                                    <div class="controls">
                                      <?php echo $user->reflink_click_counts; ?>
                                    </div>
                                 </div>
                                 <!-- End 20-4-17 -->

                                 <div class="control-group">
                                    <label class="control-label"><!-- User Current --> Balance </label>
                                    <div class="controls">
                                       R$ <?php echo $this->admin_model->currency_format($user->balance); ?>
                                    </div>
                                 </div>
                                 
                                 <div class="control-group">
                                    <label class="control-label">No of Withdrawals </label>
                                    <div class="controls">
                                       <?php echo $total_with_count; ?>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">No of Clicks </label>
                                    <div class="controls">
                                       <?php echo $total_click; ?>
                                    </div>
                                 </div>

            						   <div class="control-group">
                                    <label class="control-label">Date Added</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo date('d/m/Y',strtotime($user->date_added)); ?></label>
                                    </div>
                                 </div>   
   						   
            						   <div class="control-group">
                                    <label class="control-label">Sex</label>
                                    <div class="controls">
                                       <label class="span6"><?php if($user->sex == 1) { echo "Masculino"; } else if($user->sex == 2) { echo "Fêmea";} else { echo ""; } ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Random Code</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->random_code; ?></label>
                                    </div>
                                 </div>
                              </div> 
                                
                              <div class="span5">

                                 <h4><b>Contact Details</b><h4><br>

                                 <div class="control-group">
                                    <label class="control-label">Street</label>
                                    <div class="controls">
                                      <label class="span6"><?php echo $user->street; ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Street Number</label>
                                    <div class="controls">
                                      <label class="span6"><?php echo $user->streetnumber; ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Complemento</label>
                                    <div class="controls">
                                      <label class="span6"><?php echo $user->complemento; ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Bairro</label>
                                    <div class="controls">
                                      <label class="span6"><?php echo $user->bairro; ?></label>
                                    </div>
                                 </div>
                        
                                 <div class="control-group">
                                    <label class="control-label">City</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->city; ?></label>  
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">State</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->state; ?></label>
                                    </div>
                                 </div>
                        
                                 <div class="control-group">
                                    <label class="control-label">Country</label>
                                    <div class="controls">
                                       <label class="span6">
                                          <?php                      
                                             echo $country = $this->admin_model->get_country($user->country);
                                          ?>
                                       </label>
                                    </div>
                                 </div>
                        
                                 <div class="control-group">
                                    <label class="control-label">Zipcode</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->zipcode; ?></label>
                                    </div>
                                 </div>
                                 <!-- <div class="control-group">
                                    <label class="control-label">Current Status</label>
                                    <div class="controls">
                                      <label class="span6"><?php if($user->status == 1) { echo "Active"; } else { echo "De-Active"; } ?></label>
                                    </div>
                                 </div> -->
                                 
                                 <div class="control-group"><br>
                                    <label class="control-label">Current Status</label>
                                    <div class="controls">
                                       <?php  $user_status = $user->status;?>
                                       <select name="status" required>
                                         <option value="0" <?php if($user_status=='0'){ echo 'selected="selected"'; } ?>>De active</option>
                                         <option value="1" <?php if($user_status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
                                       </select>
                                    </div>
                                 </div>

                                 <br>
            							<h4><b>Bank Details</b></h4>
            							<br>
            						   <div class="control-group">
                                    <label class="span3">IFSC Code </label>
                                    <div class="controls">
                                       <label class="span6"><?php if($user->ifsc_code!=""){
                                          echo $user->ifsc_code;
                                          } else {
                                          echo 'Not Provided.';
                                          } ?>
                                       </label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="span3">Account Holder </label>
                                    <div class="controls">
                                       <label class="span6"><?php
                                          if($user->account_holder!=""){
                                             echo $user->account_holder;
                                          } else {
                                             echo 'Not Provided.';
                                          } ?>
                                       </label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="span3">Bank Name </label>
                                    <div class="controls">
               								<label class="span6"><?php if($user->bank_name!=""){
                  								echo $user->bank_name;
                  								} else {
                  								echo 'Not Provided.';
                  								} ?>
                                       </label>
                                    </div>
                                 </div>         						   
            						   <div class="control-group">
                                    <label class="span3">Branch Name</label>
                                    <div class="controls">
               								<label class="span6" style="word-wrap: break-word;"><?php if($user->branch_name!=""){
                  								echo $user->branch_name;
                  								} else {
                  								echo 'Not Provided.';
                  								} ?>
                                       </label>
                                    </div>
                                 </div>
            						   <div class="control-group">
                                    <label class="span3">Account Number </label>
                                    <div class="controls">
            								   <label class="span6"><?php if($user->account_number!=""){
            									echo $user->account_number;
            								   }
                                       else
                                       {
            									   echo 'Not Provided.';
            								   }
                                       ?>
                                       </label>
                                    </div>
                                 </div>
            						   
                                 <h4><b>Other Info</b><h4><br>

                                 <div class="control-group">
                                    <label class="control-label">Refer</label>
                                    <div class="controls">
                                      <label class="span6"><?php echo $user->refer; ?></label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Cashback Mail</label>
                                    <div class="controls">
                                      <!-- <label class="span6"><?php if($user->cashback_mail == 1) { echo "Active";} else { echo "De-Active"; } ?></label> -->
                                       <select id="cashback_mail" class="span6 selectpicker" name="cashback_mail">
                                          <option <?php if($user->cashback_mail==0){ echo 'selected="selected"'; } ?>value="0"><?php echo "De-Active";?></option>
                                          <option <?php if($user->cashback_mail==1){ echo 'selected="selected"'; } ?>value="1"><?php echo "Active";?></option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Withdraw Mail</label>
                                    <div class="controls">
                                       <!-- <label class="span6"><?php if($user->withdraw_mail == 1) { echo "Active";} else { echo "De-Active"; } ?></label> -->
                                       <select id="withdraw_mail" class="span6 selectpicker" name="withdraw_mail">
                                          <option <?php if($user->withdraw_mail==0){ echo 'selected="selected"'; } ?>value="0"><?php echo "De-Active";?></option>
                                          <option <?php if($user->withdraw_mail==1){ echo 'selected="selected"'; } ?>value="1"><?php echo "Active";?></option>
                                       </select>  
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Referral Mail</label>
                                    <div class="controls">
                                       <!-- <label class="span6"><?php if($user->referral_mail == 1) { echo "Active";} else { echo "De-Active"; } ?></label> -->
                                       <select id="referral_mail" class="span6 selectpicker" name="referral_mail">
                                          <option <?php if($user->referral_mail==0){ echo 'selected="selected"'; } ?>value="0"><?php echo "De-Active";?></option>
                                          <option <?php if($user->referral_mail==1){ echo 'selected="selected"'; } ?>value="1"><?php echo "Active";?></option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Newsletter Mail</label>
                                    <div class="controls">
                                       <!-- <label class="span6"><?php if($user->newsletter_mail == 1) { echo "Active";} else { echo "De-Active"; } ?></label>   -->
                                       <select id="newsletter_mail" class="span6 selectpicker" name="newsletter_mail">
                                          <option <?php if($user->newsletter_mail==0){ echo 'selected="selected"'; } ?>value="0"><?php echo "De-Active";?></option>
                                          <option <?php if($user->newsletter_mail==1){ echo 'selected="selected"'; } ?>value="1"><?php echo "Active";?></option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Support Tickets</label>
                                    <div class="controls">
                                       <!-- <label class="span6"><?php if($user->support_tickets == 1) { echo "Active";} else { echo "De-Active"; } ?></label> -->
                                       <select id="support_tickets" class="span6 selectpicker" name="support_tickets">
                                          <option <?php if($user->support_tickets==0){ echo 'selected="selected"'; } ?>value="0"><?php echo "De-Active";?></option>
                                          <option <?php if($user->support_tickets==1){ echo 'selected="selected"'; } ?>value="1"><?php echo "Active";?></option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Acbalance Mail</label>
                                    <div class="controls">
                                       <!-- <label class="span6"><?php if($user->acbalance_mail == 1) { echo "Active";} else { echo "De-Active"; } ?></label> -->
                                       <select id="acbalance_mail" class="span6 selectpicker" name="acbalance_mail">
                                          <option <?php if($user->acbalance_mail==0){ echo 'selected="selected"'; } ?>value="0"><?php echo "De-Active";?></option>
                                          <option <?php if($user->acbalance_mail==1){ echo 'selected="selected"'; } ?>value="1"><?php echo "Active";?></option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Bonus Benefit</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->bonus_benefit; ?></label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Referral Amt</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->referral_amt; ?></label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">App Login</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->app_login; ?></label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Unic Bonus Code</label>
                                    <div class="controls">
                                       <label class="span6"><?php if($user->unic_bonus_code == '') {echo "-"; } else {echo $user->unic_bonus_code;} ?></label>
                                    </div>
                                 </div>
                              </div>
                              <div class="span11">
                                 <div class="control-group">
                                       <label class="control-label">Comments for User</label>
                                       <div class="controls">
                                          <textarea class="span12 ckeditor" name="user_review" id="user_review" ><?php echo $user->user_reviews; ?></textarea>
                                       </div>
                                 </div>
                              </div>

         						   <!-- <hr>
                              <div class="control-group"><br>
                                 <label class="control-label">Current Status</label>
                                 <div class="controls">
         							      <?php  $user_status = $user->status;?>
            								<select name="status" required>
            								  <option value="0" <?php if($user_status=='0'){ echo 'selected="selected"'; } ?>>De active</option>
            								  <option value="1" <?php if($user_status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
            							   </select>
                                 </div>
                              </div>
                              -->
                              
                              <div class="form-actions">
                                 <input type="submit" name="save" value="Save Changes" class="btn btn-success" style="margin-left:15%; margin-top:5%;">
                              </div>
						   
						   <?php echo form_close(); ?>
						   <?php } } ?>
                        <!--</form>-->
                        <!-- END FORM-->
                     </div>
                  </div>
                  <!-- END SAMPLE FORM widget-->
               </div>
            </div>
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->

   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
      
   
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
   

   <script type="text/javascript">
   /*function check_cpf()
   {
      var ifsc_code = $('#ifsc_code').val();
      if(ifsc_code)
      {
         $('#unique_name_error').html('');
         $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>adminsettings/check_cpf',
            data:{'ifsc_code':ifsc_code},
            success:function(result)
            {
               if(result.trim()==1)
               {
                  $("#unique_name_error").css('color','#29BAB0');
                  $("#unique_name_error").html('Available.');
                  return true;
               }
               else
               {
                  $("#unique_name_error").css('color','#ff0000');
                  //$("#unique_name_error").html('Este é CPF já existe .');  
                  $("#unique_name_error").html('Este é um CPF não é válido.');
                  $('#ifsc_code').val('');
                  
               }
            }
         });
      }
      return false; 
   }*/
   </script>

   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
   </script>
   <!-- END JAVASCRIPTS -->   
   <script type="text/javascript">
   function showhide()
   {
      $('.showdetail').hide();
      $('.hidedetail').show();
      $('.userdetail').show();
   }
   function hideshow()
   {
      $('.showdetail').show();
      $('.hidedetail').hide();
      $('.userdetail').hide();
   }
   
   /*Onselect select box script 4-4-17*/
   $("#userdetails").change(function()
   {
      var str = "";
      var selectval = $("#userdetails").val();
      if(selectval == 'ifsc_code')
      {
         //$('#editfield').attr('onblur','return check_cpf();');
         //$('#editfield').attr('maxlength',14);
         //$('#editfield').attr('id','ifsc_code'); 
         $('#other_details').hide();
         $('#ifsc_details').show();
         $('#ifsc_code').attr('class','editfield');
      }
       
      $( "#userdetails option:selected" ).each(function() 
      {
         str += $(this).text() + " ";
      });

      $( ".fieldname" ).text(str);
   })
   .change();

   /*User details update script 4-4-17*/
   function update_now()
   {
      var selectname = $('#userdetails').val();
      var user_id    = $('#user_id').val();
      var text_value = $('.editfield').val();

      if(selectname == 'email')
      {
         if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(text_value))
         {  
            $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>adminsettings/check_email',
            data:{'email':text_value},
               success:function(result)
               {
                  if(result.trim()==1)
                  {
                     $("#unique_name_error").css('color','#29BAB0'); 
                     $("#unique_name_error").html('available.');

                     $.ajax({
                        type: 'POST',
                        url: '<?php echo base_url();?>adminsettings/update_user_details',
                        data:{'text_value':text_value,'user_id':user_id,'selectname':selectname},
                           success:function(result)
                           {
                              if(result.trim()==1)
                              {
                                 $(".editfield").val('');
                                 $("#unique_name_error").css('color','#29BAB0'); 
                                 $("#unique_name_error").html('Successfully Updated.');
                                 return true;
                              }
                              else
                              {
                                $("#unique_name_error").css('color','#ff0000');
                                $("#unique_name_error").css('font-size','14px');
                                $("#unique_name_error").css('font-weight','bold');
                                $("#unique_name_error").html('Updated not successfully.'); 
                              }
                           }
                       });
                  }
                  else
                  {
                    $("#unique_name_error").css('color','#ff0000');
                    $("#unique_name_error").css('font-size','14px');
                    $("#unique_name_error").css('font-weight','bold');
                    $("#unique_name_error").html('This email is already exists.'); 
                    return false;
                  }
               }
           });
         } 
      }
      else if(selectname == 'ifsc_code')
      {
         var ifsc_code = $('#ifsc_code').val();
         if(ifsc_code)
         {
            $('#unique_name_error').html('');
            $.ajax({
               type: 'POST',
               url: '<?php echo base_url();?>adminsettings/check_cpf',
               data:{'ifsc_code':ifsc_code},
               success:function(result)
               {  
                  if(result.trim()==1)
                  {
                     $("#unique_name_error").css('color','#29BAB0');
                     $("#unique_name_error").html('Available.');

                     $.ajax({
                     type: 'POST',
                     url: '<?php echo base_url();?>adminsettings/update_user_details',
                     data:{'text_value':ifsc_code,'user_id':user_id,'selectname':selectname},
                       
                        success:function(result)
                        {
                           if(result.trim()==1)
                           {
                              $(".editfield").val('');
                              $("#unique_name_error").css('color','#29BAB0'); 
                              $("#unique_name_error").html('Successfully Updated.');
                              return true;
                           }
                           else
                           {
                             $("#unique_name_error").css('color','#ff0000');
                             $("#unique_name_error").css('font-size','14px');
                             $("#unique_name_error").css('font-weight','bold');
                             $("#unique_name_error").html('Updated not successfully.');
                             return false; 
                           }
                        }
                    });
                  }
                  else if(result.trim()==2)
                  {
                     $("#unique_name_error").css('color','#ff0000');
                     $("#unique_name_error").css('font-size','14px');
                     $("#unique_name_error").css('font-weight','bold');
                     $("#unique_name_error").html('Already exist.'); 
                     return false;
                  }
                  else
                  {
                     $("#unique_name_error").css('color','#ff0000');
                     //$("#unique_name_error").html('Este é CPF já existe .');  
                     $("#unique_name_error").html('Este é um CPF não é válido.');
                     $('#ifsc_code').val('');
                     return false;
                     
                  }
               }
            });
         }
      }
      else
      {  
         $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>adminsettings/update_user_details',
            data:{'text_value':text_value,'user_id':user_id,'selectname':selectname},
               success:function(result)
               {
                  if(result.trim()==1)
                  {
                     $(".editfield").val('');
                     $("#unique_name_error").css('color','#29BAB0'); 
                     $("#unique_name_error").html('Successfully Updated.');
                     return true;
                  }
                  else
                  {
                    $("#unique_name_error").css('color','#ff0000');
                    $("#unique_name_error").css('font-size','14px');
                    $("#unique_name_error").css('font-weight','bold');
                    $("#unique_name_error").html('Updated not successfully.'); 
                  }
               }
           });
      }
   } 
  /*End*/
   </script>

   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/mask.js"></script>
   <script type="text/javascript">
      $('#ifsc_code').mask('999.999.999-99', {reverse: true});
   </script>

</body>
<!-- END BODY -->
</html>