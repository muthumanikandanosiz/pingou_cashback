<!DOCTYPE html>
<html lang="en"> 
<head>
   <meta charset="utf-8" />
   <?php $admin_details = $this->admin_model->get_admindetails(); 
         $bank_details  = $this->admin_model->bank_details();
         //print_r($bank_details);
   ?>
   <title>Export Report | <?php echo $admin_details->site_name; ?> Admin</title>
   <?php $this->load->view('adminsettings/script'); ?>

     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"/>
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />

<style type="text/css">
.fixed-top #container {
margin-top: 40px !important;
}

.box-head> h4{
  font-size:14px;
  font-weight: bold;
}
#tab-extrato_page > h4 {
    font-size: 22px;
}
.sub_form {
    font-size: 21px;
}
</style>]
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php $this->load->view('adminsettings/header'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
    <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('adminsettings/sidebar'); ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
              <div class="span12"> 
                  <!-- <h3 class="page-title">
                    User Information
                  </h3>
                   <ul class="breadcrumb">
                      <li>
                          <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
                          <span class="divider">&nbsp;</span>
                      </li>
                      <li>
                          <?php echo anchor('adminsettings/user_nformation',' User Information'); ?>
                          <span class="divider-last">&nbsp;</span>
                       </li>
                    </ul> -->
                    <span style="float:right">
                    <!--   <a href="<?php echo base_url();?>adminsettings/reports" class="btn btn-success">View Report</a> &nbsp;  -->
                    </span>
              </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
          <div class="row-fluid">
            <div class="span12">
                  <!-- BEGIN SAMPLE FORM widget-->
              <div class="widget">
                  <div class="widget-title">
                    <h4><i class="icon-file"></i> User Information</h4>
                    <span class="tools">
                      <a href="javascript:;" class="icon-chevron-down"></a>
                    </span>
                  </div> 
                  <br>
                  <?php
             //     if($action=="new")
             //SATz     {
                  ?>
                    <div class="widget-body form">
                      <?php 
                      $error = $this->session->flashdata('error');
                      if($error!="") {
                        echo '<div class="alert alert-error">
                        <button data-dismiss="alert" class="close">x</button>
                        <strong>Error! </strong>'.$error.'</div>';
                      }?>
                      <?php
                      $success = $this->session->flashdata('success');
                      if($success!="") {
                        echo '<div class="alert alert-success">
                        <button data-dismiss="alert" class="close">x</button>
                        <strong>Success! </strong>'.$success.'</div>';
                      }?>
                    <!-- BEGIN FORM-->
             
                   
                    <div class="row" style="margin-left:0px; !important; ">
                      <div class="span12">
                        <ul class="nav nav-tabs ul-edit responsive" style="border-bottom:none;">
                          <li class="active" style="width:14%; margin-left: 0px;"><a data-toggle="tab" href="#tab-extrato_page" class="btn btn-success1">Extrato page</a></li>
                          <li style="margin-left:10px; width:16%;" class=""><a data-toggle="tab" href="#tab-missing-cashback" class="btn btn-success1">Missing Cashback</a> </li>
                          <li style="margin-left:10px; width:16%;" class=""><a data-toggle="tab" href="#tab-resgate" class="btn btn-success1">resgate page</a></li>
                          
                          
                        </ul> 
                      
                      <br><br>  

                    
                      <div class="row" style="margin-left:0px; !important; ">
                      <div class="span12">
                        <div class="tab-content">

                        <!--Extrato Menu Start -->

                        <?php 
                   
foreach($userinfo as $u){

  



                   /*
                        if($pending_cashabck =="extrato")
                                            {
                                      */
                        ?>

                          <div class="tab-pane fade active in" id="tab-extrato_page"> 

                        <h4>EXTRATO PAGE INFORMATIONS:</h4>
                          <?php
                          //form begin
                          $attributes = array('role'=>'form','name'=>'profile_form','id'=>'profile_form','method'=>'post','class'=>'form-horizontal');
                          echo form_open('adminsettings/user_information_update',$attributes);
                          ?>  
                          <h3 class="sub_form">Cashback Items</h3><br>
                            
                           <div class="clearfix"> 
                           <div class="span4 box-head">
                              <h4>Pending</h4>
                              <div class="form-group">
                                <textarea id="ex_cash_pending" class="span6" rows="3" style="width:100%;" name="ex_cash_pending"><?=$u->ex_cash_pending?></textarea>
                              </div>
                            </div>
                           
                           
                           
                           <div class="span4 box-head">
                              <h4>Cancelled</h4>
                              <div class="form-group">
                                <textarea id="ex_cash_cancelled" class="span6" rows="3" style="width:100%;" name="ex_cash_cancelled"><?=$u->ex_cash_cancelled?></textarea>
                              </div>
                            </div>
                           


                           <div class="span4 box-head">
                              <h4>Approved</h4>
                              <div class="form-group">
                                <textarea id="ex_cash_approved" class="span6" rows="3" style="width:100%;" name="ex_cash_approved"><?=$u->ex_cash_approved?></textarea>
                              </div>
                            </div>
                            
                            </div>

                            <div class="clearfix">
                    <h3 class="sub_form">Refferal item</h3>
                     
                     <div class="inner-refer">
                     <h6>Referal Payment</h6>
                     </div>
                       <div class="span4 box-head" style="margin-left: 0;">
                              <h4>Pending</h4>
                              <div class="form-group">
                                <textarea id="ex_ref_pending" class="span6" rows="3" style="width:100%;" name="ex_ref_pending"><?=$u->ex_ref_pending?></textarea>
                              </div>
                            </div>
                           
                           
                           
                           <div class="span4 box-head">
                              <h4>Cancelled</h4>
                              <div class="form-group">
                                <textarea id="ex_ref_cancelled" class="span6" rows="3" style="width:100%;" name="ex_ref_cancelled"><?=$u->ex_ref_cancelled?></textarea>
                              </div>
                            </div>
                           
                           
                                                   
                           



                            <div class="span4 box-head">
                              <h4>Approved</h4>
                              <div class="form-group">
                                <textarea id="ex_ref_approved" class="span6" rows="3" style="width:100%;" name="ex_ref_approved"><?=$u->ex_ref_approved?></textarea>
                              </div>
                            </div>
</div>




              <div class="clearfix"> 
                         



                            <!-- item2-->
                              <div class="inner-refer">
                            <h6>Referral Cashback amount</h6>
                            </div>
<div class="span4 box-head" style="margin-left: 0;">
                              <h4>Pending</h4>
                              <div class="form-group">
                                <textarea id="ex_ref_pending_sec" class="span6" rows="3" style="width:100%;" name="ex_ref_pending_sec"><?=$u->ex_ref_pending_sec?></textarea>
                              </div>
                            </div>
                          
                           
                           
                           
                           <div class="span4 box-head">
                              <h4>Cancelled</h4>
                              <div class="form-group">
                                <textarea id="ex_ref_cancelled_sec" class="span6" rows="3" style="width:100%;" name="ex_ref_cancelled_sec"><?=$u->ex_ref_cancelled_sec?></textarea>
                              </div>
                            </div>
                           
                           
                                                   
                           



                            <div class="span4 box-head">
                              <h4>Approved</h4>
                              <div class="form-group">
                                <textarea id="ex_ref_approved_sec" class="span6" rows="3" style="width:100%;" name="ex_ref_approved_sec"><?=$u->ex_ref_approved_sec?></textarea>
                              </div>
                            </div>
                            </div>

<div class="clearfix"> 
<!-- item3 -->
  <div class="inner-refer">
<h6>Referral Bonus for Category five User</h6>
</div>
<div class="span4 box-head" style="margin-left: 0;">
                              <h4>Pending</h4>
                              <div class="form-group">
                                <textarea id="ex_ref_pending_third" class="span6" rows="3" style="width:100%;" name="ex_ref_pending_third"><?=$u->ex_ref_pending_third?></textarea>
                              </div>
                            </div>
                           
                           
                           
                           <div class="span4 box-head">
                              <h4>Cancelled</h4>
                              <div class="form-group">
                                <textarea id="ex_ref_cancelled_third" class="span6" rows="3" style="width:100%;" name="ex_ref_cancelled_third"><?=$u->ex_ref_cancelled_third?></textarea>
                              </div>
                            </div>
                           
                           
                                                   
                           



                            <div class="span4 box-head">
                              <h4>Approved</h4>
                              <div class="form-group">
                                <textarea id="ex_ref_approved_third" class="span6" rows="3" style="width:100%;" name="ex_ref_approved_third"><?=$u->ex_ref_approved_third?></textarea>
                              </div>
                            </div>


</div>  
       
                            
                            <!-- option 2-->
                            <div class="clearfix">
                            
                         <h3 class="sub_form">Missing Cashback credit:</h3><br />     



                            <div class="span4 box-head"  style="margin-left: 0;">
                              <h4>Approved</h4>
                              <div class="form-group">
                                <textarea id="ex_missing_cash_approved" class="span6" rows="3" style="width:100%;" name="ex_missing_cash_approved"><?=$u->ex_missing_cash_approved?></textarea>
                              </div>
                            </div>

                            </div>

                            <div class="clearfix">
       
                            
                            
                            <!-- option 3 -->
                            
                            <h3 class="sub_form">Credit Account</h3><br />     


                        
                      
                        <div class="span4 box-head" style="margin-left: 0;" >
                              <h4>Pending</h4>
                              <div class="form-group">
                                <textarea id="ex_credit_pending" class="span6" rows="3" style="width:100%;" name="ex_credit_pending"><?=$u->ex_credit_pending?></textarea>
                              </div>
                            </div>
                           
                           
                           
                           <div class="span4 box-head">
                              <h4>Cancelled</h4>
                              <div class="form-group">
                                <textarea id="ex_credit_cancelled" class="span6" rows="3" style="width:100%;" name="ex_credit_cancelled"><?=$u->ex_credit_cancelled?></textarea>
                              </div>
                            </div>
                           
                             <div class="span4 box-head">
                              <h4>Approved</h4>
                              <div class="form-group">
                                <textarea id="ex_credit_approved" class="span6" rows="3" style="width:100%;" name="ex_credit_approved"><?=$u->ex_credit_approved?></textarea>
                              </div>
                            </div>

                            </div>
       
         
                            <input type="hidden" name="type" value="extrato">
                            <div class="span12" style="margin-left: 78% !important; margin-top:10%;">
                              <input type="submit" class="btn btn-success" value="Save Changes" name="save">
                            </div>
                          <?php echo form_close();?>
                          </div>
                        <?php 
      //                  }
                        ?>
                        <!--Pending cashback Menu End-->  

                        <!--Missing cashback Menu start-->

                        <?php 
                     //   if($missing_cashabck=="missing_cashabck")
                     //    {
                        ?>
                          <div class="tab-pane fade" id="tab-missing-cashback">

                          <?php
                          //form begin
                          $attributes = array('role'=>'form','name'=>'profile_form','id'=>'profile_form','method'=>'post','class'=>'form-horizontal');
                          echo form_open('adminsettings/user_information_update',$attributes);
                          ?>  
                              <h4>MISSING CASHBACK PAGE INFORMATIONS:</h4><br>
                            
                           <div class="clearfix"> 
                           <div class="span3">
                              <h4>Created</h4>
                              <div class="form-group">
                                <textarea id="missing_cash_created" class="span6" rows="3" style="width:100%;" name="missing_cash_created"><?=$u->missing_cash_created?></textarea>
                              </div>
                            </div>
                           
                           
                           
                           <div class="span3">
                              <h4>Sent to retailer</h4>
                              <div class="form-group">
                                <textarea id="missing_cash_sentretailer" class="span6" rows="3" style="width:100%;" name="missing_cash_sentretailer"><?=$u->missing_cash_sentretailer?></textarea>
                              </div>
                            </div>
                           


                           <div class="span3">
                              <h4>Cancelled</h4>
                              <div class="form-group">
                                <textarea id="missing_cash_cancelled" class="span6" rows="3" style="width:100%;" name="missing_cash_cancelled"><?=$u->missing_cash_cancelled?></textarea>
                              </div>
                            </div>
                            


                                <div class="span3">
                              <h4>Completed</h4>
                              <div class="form-group">
                                <textarea id="missing_cash_completed" class="span6" rows="3" style="width:100%;" name="missing_cash_completed"><?=$u->missing_cash_completed?></textarea>
                              </div>
                            </div>
                           

                            </div>
                            
                            <!-- option 2-->

                            <input type="hidden" name="type" value="missing_cashabck">
                            <div class="span12" style="margin-left: 78% !important; margin-top:10%;">
                              <input type="submit" class="btn btn-success" value="Save Changes" name="save">
                            </div>

                          <?php echo form_close();?>
                          </div>
                        <?php 
                    //    }
                        ?>
                        <!--Missing cashback menu End-->

                        <!-- Withdraw Menu Start-->

                        <?php 
                  //      if($withdraws=="resgate")
                   //     {
                        ?>
                          <div class="tab-pane fade" id="tab-resgate">
                          
                          <?php
                          //form begin
                          $attributes = array('role'=>'form','name'=>'profile_form','id'=>'profile_form','method'=>'post','class'=>'form-horizontal');
                          echo form_open('adminsettings/user_information_update',$attributes);
                          ?>                            
                         
       <h4>RESGATE PAGE INFORMATIONS:</h4><br>
                            
                           <div class="clearfix"> 
                           <div class="span3">
                              <h4>Requested</h4>
                              <div class="form-group">
                                <textarea id="resgate_requested" class="span6" rows="3" style="width:100%;" name="resgate_requested"><?=$u->resgate_requested?></textarea>
                              </div>
                            </div>
                           
                           
                           
                           <div class="span3">
                              <h4>Processing</h4>
                              <div class="form-group">
                                <textarea id="resgate_processing" class="span6" rows="3" style="width:100%;" name="resgate_processing"><?=$u->resgate_processing?></textarea>
                              </div>
                            </div>
                           


                           <div class="span3">
                              <h4>Completed</h4>
                              <div class="form-group">
                                <textarea id="resgate_completed" class="span6" rows="3" style="width:100%;" name="resgate_completed"><?=$u->resgate_completed?></textarea>
                              </div>
                            </div>
                            


                                <div class="span3">
                              <h4>Cancelled</h4>
                              <div class="form-group">
                                <textarea id="resgate_cancelled" class="span6" rows="3" style="width:100%;" name="resgate_cancelled"><?=$u->resgate_cancelled?></textarea>
                              </div>
                            </div>
                           

                            </div>
                       



                           <input type="hidden" name="type" value="resgate">
                            <div class="span12" style="margin-left: 78% !important; margin-top:10%;">
                              <input type="submit" class="btn btn-success" value="Save Changes" name="save">
                            </div>
                          <?php echo form_close();?>  
                          </div>

                        <?php 
                   //     }

                   }
                        ?>
                        <!-- Withdraw Menu End-->

                        </div>
                      </div>
                    </div>  
                    </div>      
                    </div>
                  </div>  
                  <?php
               // SATz   } 
                  ?>

              </div>
                  <!-- END SAMPLE FORM widget-->
            </div>
          </div>
        </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
    <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
   </script>
 <script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){


$(function () {
    $('#startdatepicker, #enddatepicker').datepicker({
        beforeShow: customRange,
        dateFormat: "dd/mm/yy",
        firstDay: 1,
        changeFirstDay: false
    });
});

function customRange(input) {
    var min = null, // Set this to your absolute minimum date
        dateMin = min,
        dateMax = null,
        dayRange = 30; // Restrict the number of days for the date range
    
    if ($('#select1').val() === '2') {
        if (input.id === "startdatepicker") {
            if ($("#enddatepicker").datepicker("getDate") != null) {
                dateMax = $("#enddatepicker").datepicker("getDate");
                dateMin = $("#enddatepicker").datepicker("getDate");
                dateMin.setDate(dateMin.getDate() - dayRange);
                if (dateMin < min) { dateMin = min; }
            } else {  }
        } else if (input.id === "enddatepicker") {
            dateMin = $("#startdatepicker").datepicker('getDate');
            dateMax = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() + 30);
            if ($('#startdatepicker').datepicker('getDate') != null) {
                var rangeMax = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() + dayRange);
                if (rangeMax < dateMax) { dateMax = rangeMax; }
            }
        }
    } else if ($('#select1').val() != '2') {
        if (input.id === "startdatepicker") {
            if ($('#enddatepicker').datepicker('getDate') != null) {
                dateMin = null;
            } else {  }
        } else if (input.id === "enddatepicker") {
            dateMin = $('#startdatepicker').datepicker('getDate');
            dateMax = null;
            if ($('#startdatepicker').datepicker('getDate') != null) { dateMax = null; }
        }
    }
    return {
        minDate: dateMin,
        maxDate: dateMax
    };
}

$('.datepicker').datepicker('widget').delegate('.ui-datepicker-close', 'mouseup', function() {
    var inputToBeCleared = $('.datepicker').filter(function() { 
      return $(this).data('pickerVisible') == true;
    });    
    $(inputToBeCleared).val('');
});
});//]]>  

</script>
   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>
<style>

li .btn-success1
{
  height:40px; background: #87bb33;padding-bottom: 0px !important; border-radius: 4px 4px 4px 4px !important;
padding-top: 0px !important; line-height:40px !important;
}

</style>