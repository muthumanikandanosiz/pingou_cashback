<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <?php $admin_details = $this->admin_model->get_admindetails(); ?>
   <title>User Details | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>

   <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />
<style type="text/css">
   .form-actions 
   {
      border-top: 0px !important;
   }
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php $this->load->view('adminsettings/header'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('adminsettings/sidebar'); ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM widget-->
                  <div class="widget">
                     <div class="widget-title">
                        <h4><i class="icon-cog"></i> User Details</h4>
                        <span class="tools">
                           <a href="javascript:;" class="icon-chevron-down"></a>
                           <!--<a href="javascript:;" class="icon-remove"></a>-->
                        </span>
                     </div>
                     <div class="widget-body form">
         					<?php 
         					$error = $this->session->flashdata('error');
         					if($error!="") 
                        {
         						echo '<div class="alert alert-error">
         						<button data-dismiss="alert" class="close">x</button>
         						<strong>Error! </strong>'.$error.'</div>';
         					}
                        $success = $this->session->flashdata('success');
         					if($success!="")
                        {
         					   echo '<div class="alert alert-success">
         						<button data-dismiss="alert" class="close">x</button>
         						<strong>Success! </strong>'.$success.'</div>';			
         					}
                        ?>
                        <!-- BEGIN FORM-->
      						<?php
      						if($user_detail)
                        {
      							foreach($user_detail as $user) 
                           {
         							$attribute = array('role'=>'form','method'=>'post','class'=>'form-horizontal'); 
         							echo form_open('adminsettings/userupdate',$attribute);
         						   
                              $total_withdraw   = $this->db->query("SELECT COUNT(*) as counting FROM `withdraw` where user_id='$user->user_id'");
                              $total_with_count = $total_withdraw->row('counting');
                              if($user->profile != '') 
                              {
                                 $profile_pic = $user->profile;
                              }
                              else
                              {
                                 $profile_pic = $this->admin_model->get_img_url().'uploads/img/default_pro.jpg';  
                              }
                              ?>
                              <div class="span6">
                                 <div class="control-group">
                                    <label class="control-label">Profile Image</label>
                                    <div class="controls">
                                       <label class="span6"><img src="<?php echo $profile_pic; ?>" alt="<?php echo $profile_pic; ?>" width='150' height='50'></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">User Id</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->user_id; ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Full Name</label>
                                    <div class="controls">
         								      <label class="span6"><?php echo $user->first_name.' '.$user->last_name; ?></label>
                                    </div>
                                 </div>
   						            <input type="hidden" name="user_id" id="user_id" value="<?php echo $user->user_id; ?>">
   						            <div class="control-group">
                                    <label class="control-label">Email Address</label>
                                    <div class="controls">
                                      <label class="span6"><?php echo $user->email; ?></label>
                                    </div>
                                 </div>
   						            <div class="control-group">
                                    <label class="control-label">Contact Number</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->contact_no; ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Celular Number</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->celular_no; ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Referral Category</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo "Category ". $user->referral_category_type; ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Refered By</label>
                                    <div class="controls">
                                       <label class="span6"><?php if($user->ref_user_cat_type == 0) { echo "User not refered";} else { echo "Category ". $user->ref_user_cat_type . " User"; } ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label"><!-- User Current --> Balance </label>
                                    <div class="controls">
                                       R$ <?php echo $this->admin_model->currency_format($user->balance); ?>
                                    </div>
                                 </div>
                                 
                                 <div class="control-group">
                                    <label class="control-label">No of Withdrawals </label>
                                    <div class="controls">
                                       <?php echo $total_with_count; ?>
                                    </div>
                                 </div>
            						   <div class="control-group">
                                    <label class="control-label">Date Added</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo date('d/m/Y',strtotime($user->date_added)); ?></label>
                                    </div>
                                 </div>   
   						   
            						   <div class="control-group">
                                    <label class="control-label">Sex</label>
                                    <div class="controls">
                                       <label class="span6"><?php if($user->sex == 1) { echo "Masculino"; } else if($user->sex == 2) { echo "Fêmea";} else { echo ""; } ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Random Code</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->random_code; ?></label>
                                    </div>
                                 </div>
            						   
            						   <h4><b>Contact Details</b><h4><br>

                                 <div class="control-group">
                                    <label class="control-label">Street</label>
                                    <div class="controls">
                                      <label class="span6"><?php echo $user->street; ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Street Number</label>
                                    <div class="controls">
                                      <label class="span6"><?php echo $user->streetnumber; ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Complemento</label>
                                    <div class="controls">
                                      <label class="span6"><?php echo $user->complemento; ?></label>
                                    </div>
                                 </div>

                                 <div class="control-group">
                                    <label class="control-label">Bairro</label>
                                    <div class="controls">
                                      <label class="span6"><?php echo $user->bairro; ?></label>
                                    </div>
                                 </div>
                        
                                 <div class="control-group">
                                    <label class="control-label">City</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->city; ?></label>  
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">State</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->state; ?></label>
                                    </div>
                                 </div>
                        
                                 <div class="control-group">
                                    <label class="control-label">Country</label>
                                    <div class="controls">
                                       <label class="span6">
                                          <?php                      
                                             echo $country = $this->admin_model->get_country($user->country);
                                          ?>
                                       </label>
                                    </div>
                                 </div>
                        
                                 <div class="control-group">
                                    <label class="control-label">Zipcode</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->zipcode; ?></label>
                                    </div>
                                 </div>
                                 <!-- <div class="control-group">
                                    <label class="control-label">Current Status</label>
                                    <div class="controls">
                                      <label class="span6"><?php if($user->status == 1) { echo "Active"; } else { echo "De-Active"; } ?></label>
                                    </div>
                                 </div> -->
                                 
                                 <div class="control-group"><br>
                                    <label class="control-label">Current Status</label>
                                    <div class="controls">
                                       <?php  $user_status = $user->status;?>
                                       <select name="status" required>
                                         <option value="0" <?php if($user_status=='0'){ echo 'selected="selected"'; } ?>>De active</option>
                                         <option value="1" <?php if($user_status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
                                       </select>
                                    </div>
                                 </div>
                              </div>   
                              <div class="span6">   
            							<h5 class="page-title">	Bank Details </h5>
            							<br>
            						   <div class="control-group">
                                    <label class="span3">IFSC Code </label>
                                    <div class="controls">
                                       <label class="span6"><?php if($user->ifsc_code!=""){
                                          echo $user->ifsc_code;
                                          } else {
                                          echo 'Not Provided.';
                                          } ?>
                                       </label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="span3">Account Holder </label>
                                    <div class="controls">
                                       <label class="span6"><?php
                                          if($user->account_holder!=""){
                                             echo $user->account_holder;
                                          } else {
                                             echo 'Not Provided.';
                                          } ?>
                                       </label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="span3">Bank Name </label>
                                    <div class="controls">
               								<label class="span6"><?php if($user->bank_name!=""){
                  								echo $user->bank_name;
                  								} else {
                  								echo 'Not Provided.';
                  								} ?>
                                       </label>
                                    </div>
                                 </div>         						   
            						   <div class="control-group">
                                    <label class="span3">Branch Name</label>
                                    <div class="controls">
               								<label class="span6" style="word-wrap: break-word;"><?php if($user->branch_name!=""){
                  								echo $user->branch_name;
                  								} else {
                  								echo 'Not Provided.';
                  								} ?>
                                       </label>
                                    </div>
                                 </div>
            						   <div class="control-group">
                                    <label class="span3">Account Number </label>
                                    <div class="controls">
            								   <label class="span6"><?php if($user->account_number!=""){
            									echo $user->account_number;
            								   }
                                       else
                                       {
            									   echo 'Not Provided.';
            								   }
                                       ?>
                                       </label>
                                    </div>
                                 </div>
            						   
                                 <h4><b>Other Info</b><h4><br>

                                 <div class="control-group">
                                    <label class="control-label">Refer</label>
                                    <div class="controls">
                                      <label class="span6"><?php echo $user->refer; ?></label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Cashback Mail</label>
                                    <div class="controls">
                                      <label class="span6"><?php if($user->cashback_mail == 1) { echo "Active";} else { echo "De-Active"; } ?></label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Withdraw Mail</label>
                                    <div class="controls">
                                       <label class="span6"><?php if($user->withdraw_mail == 1) { echo "Active";} else { echo "De-Active"; } ?></label>  
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Referral Mail</label>
                                    <div class="controls">
                                       <label class="span6"><?php if($user->referral_mail == 1) { echo "Active";} else { echo "De-Active"; } ?></label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Newsletter Mail</label>
                                    <div class="controls">
                                       <label class="span6"><?php if($user->newsletter_mail == 1) { echo "Active";} else { echo "De-Active"; } ?></label>  
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Support Tickets</label>
                                    <div class="controls">
                                       <label class="span6"><?php if($user->support_tickets == 1) { echo "Active";} else { echo "De-Active"; } ?></label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Acbalance Mail</label>
                                    <div class="controls">
                                       <label class="span6"><?php if($user->acbalance_mail == 1) { echo "Active";} else { echo "De-Active"; } ?></label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Bonus Benefit</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->bonus_benefit; ?></label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Referral Amt</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->referral_amt; ?></label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">App Login</label>
                                    <div class="controls">
                                       <label class="span6"><?php echo $user->app_login; ?></label>
                                    </div>
                                 </div>
                                 <div class="control-group">
                                    <label class="control-label">Unic Bonus Code</label>
                                    <div class="controls">
                                       <label class="span6"><?php if($user->unic_bonus_code == '') {echo "-"; } else {echo $user->unic_bonus_code;} ?></label>
                                    </div>
                                 </div>
                              </div>

         						   <!-- <hr>
                              <div class="control-group"><br>
                                 <label class="control-label">Current Status</label>
                                 <div class="controls">
         							      <?php  $user_status = $user->status;?>
            								<select name="status" required>
            								  <option value="0" <?php if($user_status=='0'){ echo 'selected="selected"'; } ?>>De active</option>
            								  <option value="1" <?php if($user_status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
            							   </select>
                                 </div>
                              </div>
                              -->
                                   
                              <div class="form-actions">
                                 <input type="submit" name="save" value="Save Changes" class="btn btn-success" style="margin-left:15%; margin-top:44%;">
                              </div>
						   
						   <?php echo form_close(); ?>
						   <?php } } ?>
                        <!--</form>-->
                        <!-- END FORM-->
                     </div>
                  </div>
                  <!-- END SAMPLE FORM widget-->
               </div>
            </div>
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
   </script>
   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>