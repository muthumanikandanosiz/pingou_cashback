<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Admin Payment Settings | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>

   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"/>
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
          <!-- BEGIN PAGE HEADER-->   
          <div class="row-fluid">
              <div class="span12">
                <!-- END THEME CUSTOMIZER-->
                <!-- <h3 class="page-title">
                  Amazon S3 Settings
                </h3>
                <ul class="breadcrumb">
                  <li>
                    <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
						        <span class="divider">&nbsp;</span>
                  </li>
                  <li>
							      <?php echo anchor('adminsettings/amazon_s3_settings','Amazon S3 Settings'); ?>
							      <span class="divider-last">&nbsp;</span>
                  </li>
                </ul> -->
              </div>
          </div>
          <!-- END PAGE HEADER-->
          <!-- BEGIN PAGE CONTENT-->
          <div class="row-fluid">
            <div class="span12">
              <!-- BEGIN SAMPLE FORM widget-->
              <div class="widget">
                <div class="widget-title">
                        <h4><i class="icon-cog"></i>  Amazon S3 Settings</h4>
                        <span class="tools">
                           <a href="javascript:;" class="icon-chevron-down"></a>
                           <!--<a href="javascript:;" class="icon-remove"></a>-->
                        </span>
                </div>
					      <br>
					      <span><span class="required_field"> &nbsp;&nbsp;&nbsp;*</span> marked fields are mandatory.</span><br>
                <div class="widget-body form">
              					<?php 
              					$error = $this->session->flashdata('error');
              					if($error!="") 
                        {
              					  echo '<div class="alert alert-error">
              						<button data-dismiss="alert" class="close">x</button>
              						<strong>Error! </strong>'.$error.'</div>';
              					}
                				$success = $this->session->flashdata('success');
                				if($success!="") {
                						echo '<div class="alert alert-success">
                								<button data-dismiss="alert" class="close">x</button>
                								<strong>Success! </strong>'.$success.'</div>';			
                				}
                        ?>
                        <!-- BEGIN FORM-->
                        <!--<form action="#" class="form-horizontal">-->
            						<?php
            							$attribute = array('role'=>'form','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
            							echo form_open('adminsettings/amazon_s3_settings',$attribute);
            						
                        ?>
                                    	
                          <!-- New code for AWS to store a images.css,js..... setting for AWS 28-7-16-->
                  
                          <font color="red">*AWS S3 Storage settings*</font>
                          <br><br>
                          (when a file is uploaded to the media library(jpg,bmp,png,gif,PDF and all other), Copy it to S3 but also keep a copy on the server)<br>
                          <br>

                          <div class="control-group">
                            <label class="control-label">Copy Files to Pingoubucket on S3 <span class="required_field">*</span></label>
                            <div class="controls">
                              <select  name="copy_status" id="copy_status" required class="span2">
                                <option value="0" <?php if($copy_files_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                                <option value="1" <?php if($copy_files_status=='1'){ echo 'selected="selected"'; }?>>Enable</option>
                              </select>
                            </div>
                          </div>
                          
                          <hr>
                          (For media library files that have been copied to S3, rewrite the URL's so that they are saved from S3/Cloudfront instead of your server)
                          <br><br>
                         
                          <div class="control-group">
                            <label class="control-label">Rewrite the file URL's <span class="required_field">*</span></label>
                            <div class="controls">
                              <select  name="url_status" id="url_status" required class="span2">
                                <option value="0" <?php if($file_url_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                                <option value="1" <?php if($file_url_status=='1'){ echo 'selected="selected"'; }?>>Enable</option>
                              </select>
                              <span style="margin-left:20px;">Config file Url's : </span> &nbsp;
                              <input type="text" name="file_url" value="<?php echo $config_file_url;?>" required class="span6">
                              <p style="float:right; margin-right:11%;">(eg:https://www.static.pingou.com.br OR https://pingoubucket.s3.amazonaws.com)</p>
                            </div>
                          </div>

                          <span style="margin-left:10%;">(eg:If the image URL of the file was:  &nbsp;&nbsp;&nbsp;   https://www.pingou.gmeec.com.br/uploads/adminpro/pingou3.png</span> <br> 
                          <span style="margin-left:10%;">after Enable the image URL will be          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   https://www.static.pingou.gmeec.com.br/uploads/adminpro/pingou3.png)</span>
                          <hr>
                          (Copy assets to S3 and rewrite URLs for enqueued CSS & JS scripts. Files will be scanned every 5 minutes, new and changed files will be uploaded to S3 and missing files will be removed. PS: But also keep a copy on the server.)<br><br>
                  
                          <div class="control-group">
                            <label class="control-label">Copy & Serve + Automatic Scanning <span class="required_field">*</span></label>
                            <div class="controls">
                              <select  name="autoscan_status" id="autoscan_status" required class="span2">
                                <option value="0" <?php if($copy_autoscan_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                                <option value="1" <?php if($copy_autoscan_status=='1'){ echo 'selected="selected"'; }?>>Enable</option>
                              </select>
                              <span style="margin-left:20px;">Config file Url's : </span> &nbsp;
                              <input type="text" name="autoscan_url" value="<?php echo $config_autoscan_url;?>" required class="span6">
                            </div>
                          </div>
                          
                          PS: Minify: Reduce CSS and JS file size by removing unnecessary data<br><br>
                          
                          <div class="control-group">
                            <label class="control-label">Manual Scan Now (CSS & JS)<span class="required_field">*</span></label>
                            <div class="controls">
                              <button name="manual_scan" id="manual_scan" class="manual_scan">Scan Now</button>&nbsp;<span class="success"></span><br>
                            </div>
                          </div> 

                          <hr>
                          (Using amazon EalastiCache, you can add an in-memory caching layer to your infrastructure in a matter of minutes by using the AWS Management Console.)<br><br>
                          
                          <div class="control-group">
                            <label class="control-label">System Memory Cache <span class="required_field">*</span></label>
                            <div class="controls">
                              <select  name="cache_status" id="cache_status" required class="span2">
                                <option value="0" <?php if($sys_memory_cache_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                                <option value="1" <?php if($sys_memory_cache_status=='1'){ echo 'selected="selected"'; }?>>Enable</option>
                              </select>
                              <span style="margin-left:20px;">Config the AWS ElastiCache cluster : </span>&nbsp;
                              <input type="text" name="config_ecache" value="<?php echo $config_elasticache;?>" required class="span6">
                            </div>
                          </div>   
                          <!-- End -->
                          
                          <div class="form-actions">
                            <input type="submit" name="save" value="Save Changes" class="btn btn-success">
                          </div>
						            
                        <?php echo form_close(); ?>
                        <!--</form>-->
                        <!-- END FORM-->
                </div>
              </div>
              <!-- END SAMPLE FORM widget-->
            </div>
          </div>
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->  
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
   </script>
   <!-- END JAVASCRIPTS -->   
  <script type="text/javascript">
  $('.manual_scan').click(function(){
    $.ajax({
      type: "POST",
      url: '<?php echo base_url();?>adminsettings/upload_fileon_s3',
      data:{},
      success:function(result)
      {
        if(result == 1)
        {
          $('.success').css("color","green");
          $('.success').text('Files are Updated Successfully!');
          setTimeout(function(){
          $('.success').text('');
        }, 3000);
        }
      }
    }); 
    return false; 
  });
  </script>
</body>
<!-- END BODY -->
</html>
                            