<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>CMS | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>
  <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
  <link rel="stylesheet" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
      <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
              <div class="span12">
              </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
              <div class="span12">
                <!-- BEGIN SAMPLE FORM widget-->
                <div class="widget">
                  <div class="widget-title">
                    <h4><i class="icon-file"></i> CMS</h4>
                    <span class="tools">
                      <a href="javascript:;" class="icon-chevron-down"></a>
                      <!--<a href="javascript:;" class="icon-remove"></a>-->
                    </span>
                  </div>
        					<?php
        					if($action=="new")
                  {
        					  ?>
                    <div class="widget-body form">
            					<?php 
            					$error = $this->session->flashdata('error');
            					if($error!="")
                      {
              					echo '<div class="alert alert-error">
              					<button data-dismiss="alert" class="close">x</button>
              					<strong>Error! </strong>'.$error.'</div>';
            					} 
              				$success = $this->session->flashdata('success');
              				if($success!="") 
                      {
              				  echo '<div class="alert alert-success">
              					<button data-dismiss="alert" class="close">x</button>
              					<strong>Success! </strong>'.$success.'</div>';			
              				} 
                      ?>
                      <!-- BEGIN FORM-->
                      <!--<form action="#" class="form-horizontal">-->
						          <?php
          							$attribute = array('role'=>'form','name'=>'cms','method'=>'post','id'=>'change_pwd','enctype'=>'multipart/form-data','class'=>'form-horizontal','onSubmit'=>'return fillall();'); 
          							echo form_open('adminsettings/addcms',$attribute);
          						?>
                      
                      <div class="control-group">
                        <label class="control-label">CMS Url slag</label>
                        <div class="controls">
                          <input type="text" class="span6" name="url_slag" id="url_slag" value="" required />
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label">Page Title</label>
                        <div class="controls">
                          <input type="text" class="span6" name="page_title" id="page_title" value="" required />
                        </div>
                      </div>
				      			  
                      <div class="control-group">
                        <label class="control-label">Meta Title</label>
                        <div class="controls">
                          <input type="text" class="span6" name="meta_title" id="meta_title" value="" required />
                        </div>
                      </div>
						   
                      <div class="control-group">
                        <label class="control-label">Meta Keyword</label>
                        <div class="controls">
                          <input type="text" class="span6" name="meta_keyword" id="meta_keyword" value="" required />
                        </div>
                      </div>
                      <!-- New code 22-12-16 -->
                      <div class="control-group">
                        <label class="control-label">Meta Type</label>
                        <div class="controls">
                          <input type="text" class="span6" name="meta_type" id="meta_type" value="" required />
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Meta Site Name</label>
                        <div class="controls">
                          <input type="text" class="span6" name="meta_sitename" id="meta_sitename" value="" required />
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Meta Site Url</label>
                        <div class="controls">
                          <input type="text" class="span6" name="meta_url" id="meta_url" value="" required />
                        </div>
                      </div>
                      
                      <div class="control-group">
                        <label class="control-label">Meta Image <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="file" class="" name="meta_image" id="meta_image" required accept="image/*" />
                          <br/><span id="error_file"></span>
                        </div>
                      </div>
                      <!-- End 22-12-16 -->

                      <!-- New code for Cms page 26-4-17 -->
                      <div class="control-group">
                        <label class="control-label">CMS Image One <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="file" class="" name="cms_img_one" id="cms_img_one" required accept="image/*" />
                          <br/><span id="error_file"></span>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">CMS Image Two <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="file" class="" name="cms_img_two" id="cms_img_two" required accept="image/*" />
                          <br/><span id="error_file"></span>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">CMS Image Three <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="file" class="" name="cms_img_three" id="cms_img_three" required accept="image/*" />
                          <br/><span id="error_file"></span>
                        </div>
                      </div>
                       <div class="control-group">
                        <label class="control-label">CMS Image Four <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="file" class="" name="cms_img_four" id="cms_img_four" required accept="image/*" />
                          <br/><span id="error_file"></span>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Tag Status <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="radio" class="span3 ptype" name="tag_status" value="index-follow">Index/Follow &nbsp;
                          <input type="radio" class="span3 ptype" name="tag_status" value="noindex-nofollow">No Index/No Follow &nbsp;
                          <input type="radio" class="span3 ptype" name="tag_status" value="noindex-follow"> No Index/Follow &nbsp;
                          <input type="radio" class="span3 ptype" name="tag_status" value="index-nofollow"> Index/No Follow<br>
                        </div>
                      </div> 
                      <!-- End 26-4-17 -->

						      
                      <div class="control-group">
                        <label class="control-label">Meta Description</label>
                        <div class="controls">
                          <textarea class="span6" name="meta_description" rows="5" id="meta_description" required></textarea>	
                        </div>
                      </div>

						          <div class="control-group">
                        <label class="control-label">CMS content</label>
                        <div class="controls">
                          <textarea class="span6 ckeditor" name="cms_content" id="cms_content" ></textarea>
                        </div>
                      </div>
                           
                      <!-- <div class="control-group">
                        <label class="control-label">CMS Position</label>
                        <div class="controls">
                          <select name="cms_position" required>								 
					                  <option value="footer">Footer</option>
                            <option value="header">Header</option>
				                  </select>
                        </div>
                      </div> -->
                          
						          <div class="control-group">
                        <label class="control-label">CMS Status</label>
                        <div class="controls">
                          <select name="cms_status" required>
            							  <option value="0">De active</option>
            							  <option value="1">Active</option>
            						  </select>
                        </div>
                      </div>
						   
                      <div class="form-actions">
                        <input type="submit" name="save" value="Submit" class="btn btn-success">
                      </div>
						          <?php echo form_close(); ?>
                      <!--</form>-->
                      <!-- END FORM-->
                    </div>
					          <?php
                  }

						      if($action=="edit")
                  {
						        ?>
                    <div class="widget-body form">
            					<?php 
            					$error = $this->session->flashdata('error');
            					if($error!="")
                      {
            						echo '<div class="alert alert-error">
            						<button data-dismiss="alert" class="close">x</button>
            						<strong>Error! </strong>'.$error.'</div>';
            					}  
              				$success = $this->session->flashdata('success');
              				if($success!="") 
                      {
            						echo '<div class="alert alert-success">
            						<button data-dismiss="alert" class="close">x</button>
            						<strong>Success! </strong>'.$success.'</div>';			
            				  }
                      ?>
                      <!-- BEGIN FORM-->
                      <!--<form action="#" class="form-horizontal">-->
          						<?php
          							$attribute = array('role'=>'form','name'=>'cms','method'=>'post','id'=>'update_form','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
          							echo form_open('adminsettings/updatecms',$attribute);
          						?>
                      
                      <input type="hidden" name="cms_id" id="cms_id" value="<?php echo $cms_id; ?>">

                      <div class="control-group">
                        <label class="control-label">CMS Url slag</label>
                        <div class="controls">
                          <input type="text" class="span6" name="url_slag" id="url_slag" value="<?php echo $url_slag; ?>" required />
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label">Page Title</label>
                        <div class="controls">
                          <input type="text" class="span6" name="page_title" id="page_title" value="<?php echo $cms_heading; ?>" required />
                        </div>
                      </div>
						   
                      <div class="control-group">
                        <label class="control-label">Meta Title</label>
                        <div class="controls">
                          <input type="text" class="span6" name="meta_title" id="meta_title" value="<?php echo $cms_metatitle; ?>" required />
                        </div>
                      </div>
						   
                      <div class="control-group">
                        <label class="control-label">Meta Keyword</label>
                        <div class="controls">
                          <input type="text" class="span6" name="meta_keyword" id="meta_keyword" value="<?php echo $cms_metakey; ?>" required />
                        </div>
                      </div>
						          
                      <!-- New code 22-12-16 -->
                      <div class="control-group">
                        <label class="control-label">Meta Type</label>
                        <div class="controls">
                          <input type="text" class="span6" name="meta_type" id="meta_type" value="<?php echo $meta_type; ?>" required />
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label">Meta Site Name</label>
                        <div class="controls">
                          <input type="text" class="span6" name="meta_sitename" id="meta_sitename" value="<?php echo $meta_sitename; ?>" required />
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label">Meta Site Url</label>
                        <div class="controls">
                          <input type="text" class="span6" name="meta_url" id="meta_url" value="<?php echo $meta_url; ?>" required />
                        </div>
                      </div>
                      
                      <div class="control-group">
                        <label class="control-label">Meta Image <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="file" class="" name="meta_image" id="meta_image" accept="image/*" />
                          
                          <br/><span id="error_file"></span>
                          <img src="<?php echo $this->admin_model->get_img_url();?>uploads/adminpro/<?php echo $meta_image; ?>" width="180" height="35">
                        </div>
                      </div>

                      <!-- End 22-12-16 -->

                      <!-- New code for Cms page 26-4-17 -->
                      <div class="control-group">
                        <label class="control-label">CMS Image One <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="file" class="" name="cms_img_one" id="cms_img_one"  accept="image/*" />
                          <br/><span id="error_file"></span>
                          <img src="<?php echo $this->admin_model->get_img_url();?>uploads/adminpro/<?php echo $cms_img_one; ?>" width="180" height="35">
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">CMS Image Two <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="file" class="" name="cms_img_two" id="cms_img_two" accept="image/*" />
                          <br/><span id="error_file"></span>
                          <img src="<?php echo $this->admin_model->get_img_url();?>uploads/adminpro/<?php echo $cms_img_two; ?>" width="180" height="35">
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">CMS Image Three <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="file" class="" name="cms_img_three" id="cms_img_three" accept="image/*" />
                          <br/><span id="error_file"></span>
                          <img src="<?php echo $this->admin_model->get_img_url();?>uploads/adminpro/<?php echo $cms_img_three; ?>" width="180" height="35">
                        </div>
                      </div>
                       <div class="control-group">
                        <label class="control-label">CMS Image Four <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="file" class="" name="cms_img_four" id="cms_img_four"  accept="image/*" />
                          <br/><span id="error_file"></span>
                          <img src="<?php echo $this->admin_model->get_img_url();?>uploads/adminpro/<?php echo $cms_img_four; ?>" width="180" height="35">
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Tag Status <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="radio" class="span3 ptype" name="tag_status" <?php if($tag_status=="index-follow"){ echo 'checked="checked"';} ?>value="index-follow">Index/Follow &nbsp;
                          <input type="radio" class="span3 ptype" name="tag_status" <?php if($tag_status=="noindex-nofollow"){ echo 'checked="checked"';} ?>value="noindex-nofollow">No Index/No Follow &nbsp;
                          <input type="radio" class="span3 ptype" name="tag_status" <?php if($tag_status=="noindex-follow"){ echo 'checked="checked"';} ?>value="noindex-follow"> No Index/Follow &nbsp;
                          <input type="radio" class="span3 ptype" name="tag_status" <?php if($tag_status=="index-nofollow"){ echo 'checked="checked"';} ?>value="index-nofollow"> Index/No Follow<br>
                        </div>
                      </div> 
                      <!-- End 26-4-17 -->

                      <div class="control-group">
                        <label class="control-label">Meta Description</label>
                        <div class="controls">
                          <textarea class="span6" name="meta_description" rows="5" id="meta_description" required><?php echo $cms_metadesc; ?></textarea>	
                        </div>
                      </div>

						          <div class="control-group">
                        <label class="control-label">CMS content</label>
                        <div class="controls">
                          <textarea class="span6 ckeditor" name="cms_content" id="cms_content" ><?php echo $cms_content; ?></textarea>
                        </div>
                      </div>
                           
                      <!-- <div class="control-group">
                        <label class="control-label">CMS Position</label>
                        <div class="controls">
                          <select name="cms_position" required>								 
								            <option <?php if($cms_position=='footer'){ echo 'selected="selected"'; } ?> value="footer">Footer</option>
                            <option <?php if($cms_position=='header'){ echo 'selected="selected"'; } ?> value="header">Header</option>
							            </select>
                        </div>
                      </div> -->
						   
        					    <div class="control-group">
                        <label class="control-label">CMS Status</label>
                        <div class="controls">
                          <select name="cms_status" required>
            							  <option value="0" <?php if($cms_status=='0'){ echo 'selected="selected"'; } ?>>De active</option>
            							  <option value="1" <?php if($cms_status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
          						    </select>
                        </div>
                      </div>
                      <input type="hidden" name="hidden_meta_img" id="hidden_meta_img" value="<?php echo $meta_image; ?>">
                      <input type="hidden" name="hidden_cms_img_one" id="hidden_cms_img_one" value="<?php echo $cms_img_one; ?>">   
                      <input type="hidden" name="hidden_cms_img_two" id="hidden_cms_img_two" value="<?php echo $cms_img_two; ?>">   
                      <input type="hidden" name="hidden_cms_img_three" id="hidden_cms_img_three" value="<?php echo $cms_img_three; ?>">      
                      <input type="hidden" name="hidden_cms_img_four" id="hidden_cms_img_four" value="<?php echo $cms_img_four; ?>">   

                      <div class="form-actions">
                        <input type="submit" name="save" value="Save Changes" class="btn btn-success">
                      </div>
						          <?php echo form_close(); ?>
                      <!--</form>-->
                      <!-- END FORM-->
                    </div>
					          <?php
                  }
                  ?>
                </div>
                <!-- END SAMPLE FORM widget-->
              </div>
        </div>
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->  
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
  <!-- END FOOTER -->
  <!-- BEGIN JAVASCRIPTS -->
  <!-- Load javascripts at bottom, this will reduce page load time -->
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckfinder/ckfinder.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
  <script>
    jQuery(document).ready(function() {       
    // initiate layout and plugins
      App.init();
    });

    CKEDITOR.replace( 'cms_content', {
      height: 300,
      // Configure your file manager integration. This example uses CKFinder 3 for PHP.
      filebrowserBrowseUrl: '<?php echo base_url();?>assets/assets/ckfinder/ckfinder.html',
      filebrowserImageBrowseUrl: '<?php echo base_url();?>assets/assets/ckfinder/ckfinder.html?type=Images',
      filebrowserFlashBrowseUrl: '<?php echo base_url();?>assets/assets/ckfinder/ckfinder.html?Type=Flash',
      filebrowserUploadUrl: '<?php echo base_url();?>assets/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
      filebrowserImageUploadUrl: '<?php echo base_url();?>assets/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
      filebrowserFlashUploadUrl: '<?php echo base_url();?>assets/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });
    CKFinder.setupCKEditor( editor, '../' );


  </script>
  <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>