<!DOCTYPE html>
<html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>New Coupon | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>

  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
  <!--<link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />-->
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />

  <!-- css for selectbox -->
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/boostrap-select/bootstrap-combined.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/boostrap-select/bootstrap-select.min.css" media="screen" > 
  <!-- End -->
  <style type="text/css">
    .bootstrap-select.btn-group .dropdown-menu.inner 
    {
      height: 200px;
      overflow-y: scroll;
    }
    .dropdown-menu
    {
      z-index: 99 !important;
    }

    .btn-group.bootstrap-select.span6
    {
      width:260px !important;
    }
    .ptype
    {
      opacity: 1 !important;
      width:15px !important;
    }
    .newptype
    {
      opacity: 1 !important;
      width:15px !important;
    }

  </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
          <div class="span12">
            <!-- <h3 class="page-title">
            New Coupon
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
		        	  <span class="divider">&nbsp;</span>
              </li>
              <li>
							  <?php echo anchor('adminsettings/addcoupon','New Coupon'); ?>
							  <span class="divider-last">&nbsp;</span>
              </li>
            </ul> -->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
          <div class="span12">
          <!-- BEGIN SAMPLE FORM widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-file"></i> New Coupon</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                  <!--<a href="javascript:;" class="icon-remove"></a>-->
                </span>
              </div>
              <br>
					    <span><span class="required_field"> &nbsp;&nbsp;&nbsp;*</span> marked fields are mandatory.</span><br>
              <?php
					    if($action=="new")
              {
					      ?>
                <div class="widget-body form">
        					<?php 
        					$error = $this->session->flashdata('error');
        					if($error!="") 
                  {
          					echo '<div class="alert alert-error">
          					<button data-dismiss="alert" class="close">x</button>
          					<strong>Error! </strong>'.$error.'</div>';
        					}
        					$success = $this->session->flashdata('success');
        					if($success!="") 
                  {
        					  echo '<div class="alert alert-success">
        						<button data-dismiss="alert" class="close">x</button>
        						<strong>Success! </strong>'.$success.'</div>';
        					}
                  ?>
                  <!-- BEGIN FORM-->
						      <?php
      						$attribute = array('role'=>'form','name'=>'addcoupon','method'=>'post','id'=>'addcoupon','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
      						echo form_open('adminsettings/addcoupon',$attribute);
      						?>
              
                    <div class="control-group extra_hidden">
                      <label class="control-label">Store Name <span class="required_field">*</span></label>
                      <div class="controls">
                        <?php
                      	$aff_list  = $this->admin_model->affiliates();
                      	?>
                        <select id="offer_name" class="span6 selectpicker addone" name="offer_name" data-live-search="true" onchange="Choose_store(this)">
                          <?php
        								  foreach($aff_list as $stores)
                          {
                            ?>
                            <option  value="<?php echo $stores->affiliate_name;?>"><?php echo $stores->affiliate_name;?></option>
                            <?php
                          }
                          ?>
                        </select>
                        <span class="small">eg : amazon</span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Date</label>
                      <div class="controls">
        							  <input type="text" style="margin-right:17px;" name="start_date" id="start_date" value="<?php echo date('d-m-Y');?>" required class="span4 date-picker">
                        <input type="text" name="expiry_date" id="expiry_date" class="span4 date-picker">
                        </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Title <span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="text" name="title" id="title" required class="span8">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Description <span class="required_field">*</span></label>
                      <div class="controls">
        							<textarea name="description" id="description" required class="span8"></textarea>  
                      </div>
                    </div>
                    
                    <div class="control-group">
                      <label class="control-label">Type <span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="radio"  class="span3 ptype" checked="checked" name="type" id="promotion" value="1" />Promotion &nbsp;
                        <input type="radio"  class="span3 ptype" name="type" id="coupon" value="2" />Coupon &nbsp;
                        <input type="text"   style="width: 344px;" name="code"  class="coupons" id="coupons" class="span6"> &nbsp;
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Nota do Editor <span class="required_field">*</span></label>
                      <div class="controls">
        							  <textarea name="cashback_description" id="cashback_description" class="span8"></textarea>  
                      </div>
                      <span class="small" style="margin-left: 180px;">Default : Get additional upto [Retailler Cashback] Cashback from <?php echo $admin_details->site_name; ?></span>
                    </div>
                    <!-- New code for url types 12-10-16 -->
                    <div class="control-group">
                      <div class="controls">
                        <input type="radio" class="span3 newptype types" name="url_type" id="paste"    value="1" />Paste &nbsp; <!-- checked="checked" -->
                        <input type="radio" class="span3 newptype types" name="url_type" id="standard" value="2" />Standard &nbsp;
                        <input type="radio" class="span3 newptype types" name="url_type" id="deeplink" value="3" />DeepLink &nbsp;
                        <input type="text"  name="deeplink_url"   style="width: 220px;" class="deeplink_url" id="deeplink_url" class="span6"> 
                        <div name="save"    id="deeplink_details" class="btn btn-success">Go</div>
                      </div>
                    </div>
                    <!-- End -->
        					  <div class="control-group">
                      <label class="control-label">URL <span class="required_field">*</span></label>
                      <div class="controls">
                        <textarea name="offer_page" id="offer_page" rows="5" required class="span8"></textarea>
                      </div>
                    </div>       
                    <div class="control-group">
                      <label class="control-label">Coupons Options </label>
                      <div class="controls">
                        <input type="radio" class="span3 newptype" name="coupon_options" id="featured" value="1" />Featured &nbsp;
                        <input type="radio" class="span3 newptype" name="coupon_options" id="featured" value="2" />Exclusive
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Tracking Extra Param<span class="required_field">*</span> </label>
                      <div class="controls">
                        <input type="text" name="Tracking"  id="Tracking" required class="span8">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Extra Tracking Param<span class="required_field">*</span> </label>
                      <div class="controls">
                        <input type="text" name="extra_tracking"  id="extra_tracking" required class="span8">
                      </div>
                    </div>
                                 
                    <input type="hidden" class="span3" name="featured" id="featured" value="0" />
                    <input type="hidden" class="span3" name="exclusive" id="exclusive" value="0" />             
        					  <input type="hidden" name="coupon_type" id="coupon_type" value="Other">

                    <div class="form-actions">
                      <input type="submit" name="save" value="Submit" class="btn btn-success">
                    </div>
						   
						      <?php echo form_close(); ?>
                  <!-- END FORM-->
                </div>
					      <?php
              } 
              ?>
					 
  					  <?php
  					  if($action=="edit")
              {
  					    ?>
                <div class="widget-body form">
    					    <?php 
      					  $error = $this->session->flashdata('error');
      					  if($error!="") 
                  {
        						echo '<div class="alert alert-error">
        						<button data-dismiss="alert" class="close">x</button>
        						<strong>Error! </strong>'.$error.'</div>';
      					  }
      						$success = $this->session->flashdata('success');
      						if($success!="") 
                  {
      						  echo '<div class="alert alert-success">
      							<button data-dismiss="alert" class="close">x</button>
      							<strong>Success! </strong>'.$success.'</div>';
      						} 
                  ?>
                  <!-- BEGIN FORM-->
    						  <?php
    							$attribute = array('role'=>'form','name'=>'addcoupon','method'=>'post','id'=>'addcoupon','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
    							echo form_open('adminsettings/updatecoupon',$attribute);
    						  ?>

    						  <input type="hidden" name="coupon_id" id="coupon_id" value="<?php echo $coupon_id; ?>">

                  <div class="control-group extra_hidden">
                    <label class="control-label">Offer Name <span class="required_field">*</span></label>
                    <div class="controls">
    								  <?php
                      $aff_list  = $this->admin_model->affiliates();
                      ?>
                      <select id="offer_name" class="span6 selectpicker" required="" name="offer_name" data-live-search="true" onchange="Choose_store(this)">
                        <?php
                        foreach($aff_list as $stores)
                        {
                          ?>
                          <option <?php if($stores->affiliate_name==$offer_name){echo 'selected="selected"';}?>  value="<?php echo $stores->affiliate_name;?>"><?php echo $stores->affiliate_name;?></option>
                          <?php
                        }
                        ?>
                      </select>
                      <span class="small">eg : amazon</span>
                    </div>
                  </div>

                  <div class="control-group">
                    <label class="control-label">Date</label>
                    <div class="controls">
                      <input type="text" style="margin-right:17px;" name="start_date" id="start_date" value="<?php echo $start_date; ?>" required class="span4 date-picker">
                      <input type="text" name="expiry_date" id="expiry_date" class="span4 date-picker" value="<?php echo $expiry_date; ?>">
                    </div>
                  </div>
    						  <div class="control-group">
                    <label class="control-label">Title <span class="required_field">*</span></label>
                    <div class="controls">
    								  <input type="text" name="title" id="title" required class="span8" value="<?php echo $title; ?>">
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Description <span class="required_field">*</span></label>
                    <div class="controls">
                      <textarea name="description" id="description" rows="5" required class="span8"><?php echo $description; ?></textarea>
                      
                    </div>
                  </div>             
                  <div class="control-group">
                    <label class="control-label">Type <span class="required_field">*</span></label>
                    <div class="controls">
                      <input type="radio" class="span3 ptype"   name="type" id="promotion" <?php if($type=='Promotion'){echo "checked='checked'";}?> value="1" />Promotion &nbsp;
                      <input type="radio" class="span3 ptype"   name="type" id="coupon"    <?php if($type=='Coupon'){echo "checked='checked'";}?> value="2" />Coupon &nbsp;
                      <input type="text" class="coupons" name="code" id="code" value="<?php echo $code; ?>" style="width: 345px;"> &nbsp;
                    </div>
                  </div>            
    						  <div class="control-group">
                    <label class="control-label">Cashback Description <span class="required_field">*</span></label>
                    <div class="controls">
    								  <textarea name="cashback_description" id="cashback_description" class="span8"><?php echo $cashback_description; ?></textarea>
                    </div>
                    <span class="small" style="margin-left: 180px;">Default : Get additional upto [Retailler Cashback] Cashback from <?php echo $admin_details->site_name; ?></span>
                  </div>
                  <!-- New code for url types 12-10-16 -->
                  <div class="control-group">
                    <div class="controls">
                      <input type="radio" class="span3 newptype types" name="url_type" id="paste"    <?php if($url_link_type=='Paste'){echo "checked='checked'";}?>    value="1" />Paste &nbsp;
                      <input type="radio" class="span3 newptype types" name="url_type" id="standard" <?php if($url_link_type=='Standard'){echo "checked='checked'";}?> value="2" />Standard &nbsp;
                      <input type="radio" class="span3 newptype types" name="url_type" id="deeplink" <?php if($url_link_type=='DeepLink'){echo "checked='checked'";}?> value="3" />DeepLink &nbsp;
                      <input type="text"  name="deeplink_url"   style="width: 220px;" class="deeplink_url" id="deeplink_url" value="<?php echo $deep_url;?>"> 
                      <div name="save"    id="deeplink_details" class="btn btn-success">Go</div>
                    </div>
                  </div>
                  <!-- End -->
    						  <div class="control-group">
                    <label class="control-label">URL <span class="required_field">*</span></label>
                    <div class="controls">
                      <textarea name="offer_page" id="offer_page" rows="5" required class="span8"><?php echo $offer_page; ?></textarea>
                    </div>
                  </div>         
                  <div class="control-group">
                    <label class="control-label">Coupons Options </label>
                    <div class="controls">
                      <input type="radio" class="span3 ptype" name="coupon_options" id="featured" <?php if($coupon_options=="1"){ echo 'checked="checked"';} ?> value="1" />Featured &nbsp;
                      <input type="radio" class="span3 ptype" name="coupon_options" id="featured" <?php if($coupon_options=="2"){ echo 'checked="checked"';} ?> value="2" />Exclusive
                    </div>
                  </div>        
                  <?php
                  if($api_name== 'zanox')
                  {
                    $Tracking    = $admindetails->zanox_tracking;
                    $ex_tracking = $admindetails->zanox_extra_tracking;
                  }
                  if($api_name == 'cityads')
                  {
                    $Tracking    = $admindetails->cityads_tracking;
                    $ex_tracking = $admindetails->cityads_extra_tracking;
                  }
                  if($api_name== 'rakuten')
                  {
                    $Tracking    = $admindetails->rakuten_tracking;
                    $ex_tracking = $admindetails->rakuten_extra_tracking;
                  }
                  if($api_name == 'afilio')
                  {
                    $Tracking    = $admindetails->afilio_tracking;
                    $ex_tracking = $admindetails->afilio_extra_tracking;
                  }
                  if($api_name == 'lomadee')
                  {
                    $Tracking    = $admindetails->lomadee_tracking;
                    $ex_tracking = $admindetails->lomadee_extra_tracking;
                  }
                  ?>              
                  <div class="control-group">
                    <label class="control-label">Tracking Extra Param<span class="required_field">*</span> </label>
                    <div class="controls">
                      <input type="text" name="Tracking"  id="Tracking" value="<?php echo $Tracking;?>" required class="span8">
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Extra Tracking Param<span class="required_field">*</span> </label>
                    <div class="controls">
                      <input type="text" name="extra_tracking"  id="extra_tracking" value="<?php echo $ex_tracking;?>" required class="span8">
                    </div>
                  </div>
                  
                  <input type="hidden" class="span3" name="api_name" id="api_name" value="<?php echo $api_name;?>" />
                  <input type="hidden" class="span3" name="featured" id="featured" value="0" />
                  <input type="hidden" class="span3" name="exclusive" id="exclusive" value="0" />

                  <div class="form-actions">
                    
                    <?php 
                    if($api_name)
                    {
                      ?>
                      <input type="submit" name="save_coupon" value="Save Changes" class="btn btn-success">  
                      <input type="submit" name="approve_coupon" value="Save Changes AND APPROVE COUPON" class="btn btn-success" style="">
                      <?php 
                    }
                    else
                    { 
                      ?>
                      <input type="submit" name="save" value="Save Changes" class="btn btn-success">
                      <?php 
                    }
                    ?>
                  </div>

                  
    						   
    						   <?php echo form_close(); ?>
                            <!-- END FORM-->
                         </div>
    					 <?php } ?>
                      </div>
                    <!-- END SAMPLE FORM widget-->
                 </div>
              </div>
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
    <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
	  
	  function change_coupon_status(nowstar)
	  {
		 if(nowstar=='Coupon') 
		 {
			 $("#hidesh").show();
		 }
		 else
		 {
			 $("#hidesh").hide();
		 }
	  }
   </script>
<script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/boostrap-select/bootstrap-select.min.js"></script>
<script>

window.onload = function() {
  document.getElementById("selectinput").focus();
};

jQuery(document).ready(function($) {
    $('.addone').addClass('open');
})



$(document).ready(function(e) 
{ 
  $('.selectpicker').selectpicker();

  $('.coupons').click(function()
{
  //alert("ahua");
  var objpnt = $(this).parents('.controls');
  var valu    = $(this).val();
  objpnt.find('input.ptype').removeAttr('checked');
  //$('input[type="radio"]').removeAttr('checked');
  if(valu=='')
  {
    $('input#coupon').attr('checked',true);
  }
  else
  {
    $('input#promotion').attr('checked',true);
  }
});
 

/*New code 14-10-16*/  
$('.types').click(function()
{
  var type       = $(this).val();
  var store_name = $('#offer_name').val();
  if(type == 2)
  {
    $.ajax({  
    url:"<?php echo base_url(); ?>index.php/cashback/coupon_url_details",
    data: "store_name="+ store_name,     
    type:"POST",
    success:function(data)
    {
      if(data!='')
      {
        $('#offer_page').val(data);
      }  
    }
    });
  }
  else
  {
    $('#offer_page').val('');
  }
});

/*New code for deeplink functionalities 15-10-16*/
$('#deeplink_details').click(function()
{
  
  var linkname = $("#deeplink_url").val();
  if(linkname != '')
  {
    $.ajax({  
    url:"<?php echo base_url(); ?>index.php/cashback/deeplink_urldetails",
    data: "link_name="+ linkname,     
    type:"POST",
    success:function(data)
    {
      if(data!='')
      {
        $('#offer_page').val('');
        $('#offer_page').val(data);
      }  
    }
    });
  } 
});


});
function Choose_store(data) 
{

  var store_name =  data.value; 
  $.ajax({  
    url:"<?php echo base_url(); ?>index.php/cashback/store_param_details",
    data: "storename="+store_name,     
    type:"POST",
    success:function(data)
    {
      if(data!='')
      {
        var arrdata = data.split('&&');
        $('#Tracking').val(arrdata[0]);
        $('#extra_tracking').val(arrdata[1]);
      }  
    }
  });

  var type       = $('.types:checked').val();
  var store_name = $('#offer_name').val();
  if(type == 2)
  {
    $.ajax({  
    url:"<?php echo base_url(); ?>index.php/cashback/coupon_url_details",
    data: "store_name="+ store_name,     
    type:"POST",
    success:function(data)
    {
      if(data!='')
      {
        $('#offer_page').val(data);
      }  
    }
    });
  }  
}


/*End 14-10-16*/ 
</script>
   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>