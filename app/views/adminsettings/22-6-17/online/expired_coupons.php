<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Expired Premium Coupons | <?php echo $admin_details->site_name; ?> Admin</title> 
	<?php $this->load->view('adminsettings/script'); ?>
  <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />

  <style>
    #sample_1 th
    {
     text-align:center !important;
    }
  </style>
  <style>
    div.selector, div.selector span, div.checker span, div.radio span, div.uploader, div.uploader span.action, div.button, div.button span
    {background-image:none;}
    .col-xs-6 {width: 47% !important; float: left;min-height:1px;padding-left: 12px;padding-right: 12px;position: relative;}
    select.input-sm { width:60px !important;}
    .row {margin-left: -12px;margin-right: -12px;}
    .pagination {border-radius: 4px;margin: 20px 0;padding-left: 0;}
  </style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN THEME CUSTOMIZER-->
            <!--<div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
            <span class="settings">
            <span class="text">Theme:</span>
            <span class="colors">
            <span class="color-default" data-style="default"></span>
            <span class="color-gray" data-style="gray"></span>
            <span class="color-purple" data-style="purple"></span>
            <span class="color-navy-blue" data-style="navy-blue"></span>
            </span>
            </span>
            </div>-->
            <!-- END THEME CUSTOMIZER-->
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
           <!--  <h3 class="page-title">
            Expired Premium Coupons
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
						    <span class="divider">&nbsp;</span>
              </li>
              <li><?php echo anchor('adminsettings/expired_coupons','Expired Premium Coupons'); ?>
							  <span class="divider-last">&nbsp;</span>
					    </li>
            </ul> -->
            <span style="float:left; margin-top:13px;">
              <a class="btn btn-success" href="<?php echo base_url();?>adminsettings/download_coupons/expired">Download Expired Coupons</a> &nbsp;
            </span>
            <!-- END PAGE TITLE & BREADCRUMB-->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <!-- BEGIN ADVANCED TABLE widget-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN EXAMPLE TABLE widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i>Expired Premium Coupons</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                </span>
              </div>
              <div class="widget-body">
    						<?php 
    					  $error = $this->session->flashdata('error');
    					  if($error!="")
                {
      						echo '<div class="alert alert-error">
      						<button data-dismiss="alert" class="close">x</button>
      						<strong>Error! </strong>'.$error.'</div>';
      					}
      					$success = $this->session->flashdata('success');
      					if($success!="") 
                {
      					  echo '<div class="alert alert-success">
      						<button data-dismiss="alert" class="close">x</button>
      						<strong>Success! </strong>'.$success.'</div>';			
    						} 
                //$store_name = $this->uri->segment(3);
                //if(!$store_name)
               // {
                  ?>
                  <form id="form2" action="" method="post" name="form2">
                    <table class="table table-striped table-bordered" id="sample_teste1">
                      <thead>
                        <tr>
                          <!-- <th><center>#</center></th>-->
                          <th style=""><center><input type="checkbox" id="check_b" class="check_b" onchange="checkAll(this)" name="chk[]" /></center></th>
                          <th><center>Store Name</center></th> 
                          <th><center>Coupon Name</center></th> 
                          <th><center>Original Price</center></th>
                          <th><center>Coupon Price</center></th>
                          <th><center>Coupon Code</center></th>
                          <th><center>Category</center></th>
                          <!-- <th><center>Quantity</center></th>  -->
                          <!--<th><center>Offer Name</center></th>-->
                          <th><center>Start Date</center></th> 
                          <th><center>Expires Date</center></th>
                          <th><center>Edit</center></th>
                          <th class="hidden-phone"><center>Delete</center></th>
                        </tr>
                      </thead>
                      <tbody>
        							  <?php
          							/*$k=0;
          							if($shoppingcoupons)
                        {
            							foreach($shoppingcoupons as $coupon)
                          {
              							$k++;
                            $store_name  = $this->admin_model->get_affiliate($coupon->store_name);
                            $storename   = $store_name[0]->affiliate_name;
                            $category_id = $coupon->category;
                            $cat_details = $this->admin_model->get_premium_category($category_id);
              							?>
                            <tr class="odd gradeX">
                              <!--  <td><center><?php echo $k; ?></center></td>-->
        									    <td><center><input type="checkbox"  class="check_b" name="chkbox[<?php echo $coupon->shoppingcoupon_id;?>]" /></center></td>  
                              <td><a target="_blank" href="<?php echo base_url();?>adminsettings/shoppingcoupons/<?php echo $store_name[0]->affiliate_url;?>"><?php echo $storename;?></a></td>
                              <td><center><a target="_blank" href="<?php echo base_url();?>barato/<?php echo $cat_details[0]->category_url; ?>/<?php echo $coupon->seo_url; ?>"><?php echo $coupon->offer_name; ?></a></center></td>
        									    <td><?php echo "R$ ".$this->admin_model->currency_format($coupon->price);?></td>
                              <td><center><?php echo "R$ ".$this->admin_model->currency_format($coupon->amount); ?></center></td> 
                              <td><?php echo $coupon->coupon_code;?></td>
                              <td><center><?php echo $coupon->category; ?></center></td>
        									    <!-- <td>
                                <center><?php $quantity = $this->admin_model->get_countshopcoupon($coupon->shoppingcoupon_id);
          									    if($quantity > 0)
                                { 
          										    echo $quantity;
          									    }
                                else
                                {
              										echo 0;
              									}
              									?>
                                </center>
                              </td> -->
        									    <!--<td><center><?php echo $coupon->offer_name; ?></center></td>-->	
        									    <td><center><?php echo date('d-M-Y',strtotime($coupon->date_added)); ?></center></td>
        									    <td><center><?php echo date('d-M-Y',strtotime($coupon->expiry_date)); ?></center></td>
        									    <td><center><?php echo anchor('adminsettings/edit_shoppingcoupon/'.$coupon->shoppingcoupon_id,'<i class="icon-pencil"></i>'); ?></center></td>
                              <td class="center hidden-phone"><center>
              									<?php
              									$confirm = array("class"=>"confirm-dialog","onclick"=>"return confirmDelete('Do you want to delete this coupon details?');");
              									echo anchor('adminsettings/delete_exp_shoppingcoupon/'.$coupon->shoppingcoupon_id,'<i class="icon-trash"></i>',$confirm); ?></center>
            									</td>
                            </tr>
            								<?php
                          }
                        }*/
                        /*else
                        {
                          ?>
            							<tr>
            								<td colspan="7"><center>No shopping coupons found.</center></td>
            							</tr>
            							<?php
                        }*/
                        ?>
                      </tbody>
                    </table>
                    <input type="hidden" name="hidd" value="hidd">
                    <input id="GoUpdate" class="btn btn-warning" type="submit" value="Delete Coupons" name="GoUpdate">
                  </form>
                  <?php 
                /*}
                else
                {
                  ?>  
                  <form id="form2" action="" method="post" name="form2">
                    <table class="table table-striped table-bordered" id="sample_1">
                      <thead>
                        <tr>
                          <th><center><input type="checkbox" id="check_b" class="check_b grpall" name="chk[]" onchange="checkAll(this)" /></center></th> <!-- onchange="checkAll(this)" -->
                          <th><center>Coupon ID</center></th>
                          <th><center>Store</center></th>
                          <th><center>Title</center></th>
                          <th><center>Code</center></th>
                          <th><center>Offers</center></th>
                          <th><center>Promo ID</center></th>
                          <th><center>1st Track</center></th>
                          <th><center>2st Track</center></th>
                          <th><center>Expiry Date</center></th>
                          <th><center>Edit</center></th>
                          <th><center>Delete</center></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $m=0;
                        if($shoppingcoupons)
                        {
                          foreach($shoppingcoupons as $coupon)
                          {
                            $m++;
                            $store_name    = $this->admin_model->get_affiliate_details($coupon->offer_name);
                            $affilaite_url = $store_name[0]->affiliate_url;
                            ?>
                            <tr class="odd gradeX">
                              <!-- <td><center><?php echo $m; ?></center></td> --> 
                              <td><center><input type="checkbox"  class="check_b" name="chkbox[<?php echo $coupon->coupon_id;?>]" /></center></td>  
                              <td><?php echo $coupon->coupon_id;?></td>
                              <td><a target="_blank" href="<?php echo base_url();?>cupom-desconto/<?php echo $affilaite_url;?>"><?php echo $coupon->offer_name;?></td>
                              <td><center><a target="_blank" href="<?php echo $coupon->offer_page; ?>"><?php echo $coupon->title; ?></a></center></td>
                              <td><?php echo $coupon->code;?></td>
                              <td></td>
                              <td><center><?php echo $coupon->type; ?></center></td> 
                              <td><center><?php echo $coupon->Tracking; ?></center></td>
                              <td><center><?php echo $coupon->extra_tracking_param; ?></center></td>
                              <td><center><?php echo date('d/m/Y',strtotime($coupon->expiry_date)); ?></center></td>
                              <td><center><?php echo anchor('adminsettings/editcoupon/'.$coupon->coupon_id,'<i class="icon-pencil"></i>'); ?></center></td>
                              <td class="center hidden-phone"><center>
                                <?php
                                $confirm = array("class"=>"confirm-dialog","onclick"=>"return confirmDelete('Do you want to delete this coupon details?');");
                                echo anchor('adminsettings/deletecoupon/'.$coupon->coupon_id,'<i class="icon-trash"></i>',$confirm); ?></center>
                              </td>
                            </tr>
                            <?php
                          }
                        }
                        ?>
                      </tbody>
                    </table>
                    <input type="hidden" name="hidd" value="hidd">
                    <input id="GoUpdate" class="btn btn-warning" type="submit" value="Delete Coupons" name="GoUpdate">
                  </form>
                  <?php 
                }*/
                ?>
              </div>
            </div>
            <!-- END EXAMPLE TABLE widget-->
          </div>
        </div>
        <!-- END ADVANCED TABLE widget-->
        <!-- END PAGE CONTENT-->
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
  <!-- END FOOTER -->
  <!-- BEGIN JAVASCRIPTS -->
  <!-- Load javascripts at bottom, this will reduce page load time -->
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>   
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->   
  <!-- <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>-->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script> 

  <script>
     jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();
     });
  </script>
  <script type="text/javascript">
    /*function confirmDelete(m)  // Confirm before delete coupon..
    {
    	if(!confirm(m))
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }*/
  </script>
  <script>
    $(document).ready(function() 
    {
		$(".check_b").attr("style", "opacity: 1;");
      });
	  function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
    }
  </script>
</body>
<!-- END BODY -->
</html>
<script type="text/javascript">

   function confirmDelete()  // Confirm before delete cms..
    {
      var m = "Do you want to delete this expiry coupon record?";
      if(!confirm(m))
      {
        return false;
      }
      else
      {
        return true;
      }
    }

  $(document).ready(function() {
$('#sample_teste1').DataTable({
"processing": true,
"serverSide": true,
"columnDefs": [{
"targets": 0,
"orderable": false
}],
"ajax": {
"url": "<?php echo site_url('adminsettings/newexpiry_coupons')?>",
"data": {
//"totalrecords": "<?php echo $iTotal; ?>"
"user_id":"<?php echo $this->uri->segment(4); ?>",
"aff_name":"<?php echo $this->uri->segment(3); ?>"
}
}
});
});
</script>


</body>
<!-- END BODY -->
</html>
<style type="text/css">
 .dataTables_processing
{
  display:none !important;
}
</style>
 <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/css/pagination.css" rel="stylesheet" />
