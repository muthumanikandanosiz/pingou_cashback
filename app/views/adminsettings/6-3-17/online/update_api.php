<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Update API | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>

  <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
  <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
      <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
          <div class="span12" style="margin-top:13px;">
            <!-- <span style="float:left">
              <a class="btn btn-success" href="<?php echo base_url();?>networks/update_api">Home</a> &nbsp;
            </span> -->
            <span style="float:left">
              <a class="btn btn-success" href="<?php echo base_url();?>networks/update_api/zanox">Zanox</a> &nbsp;
            </span>
            <span style="float:left">
              <a class="btn btn-success" href="<?php echo base_url();?>networks/update_api/lomadee">Lomadee</a> &nbsp;
            </span>
            <span style="float:left">
              <a class="btn btn-success" href="<?php echo base_url();?>networks/update_api/rakuten">Rakuten</a> &nbsp;
            </span>
            <span style="float:left">
              <a class="btn btn-success" href="<?php echo base_url();?>networks/update_api/cityads">Cityads</a> &nbsp;
            </span>
            <span style="float:left">
              <a class="btn btn-success" href="<?php echo base_url();?>networks/update_api/afilio">Afilio</a> &nbsp;
            </span>
            <!-- BEGIN THEME CUSTOMIZER-->
            <!--<div id="theme-change" class="hidden-phone">
                <i class="icon-cogs"></i>
                <span class="settings">
                  <span class="text">Theme:</span>
                  <span class="colors">
                    <span class="color-default" data-style="default"></span>
                    <span class="color-gray" data-style="gray"></span>
                    <span class="color-purple" data-style="purple"></span>
                    <span class="color-navy-blue" data-style="navy-blue"></span>
                  </span>
                </span>
              </div>-->
            <!-- END THEME CUSTOMIZER-->
            <!-- <h3 class="page-title">Upload Reports</h3>
            <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
						    <span class="divider">&nbsp;</span>
              </li>
              <li>
							  <?php echo anchor('adminsettings/bulkcoupon','Upload Reports'); ?>
							  <span class="divider-last">&nbsp;</span>
              </li>
            </ul> -->
             
          </div>
        </div>
        <?php 
        $api_name  = $this->uri->segment(3);
        ?>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
          <div class="span12">
          <!-- BEGIN SAMPLE FORM widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-file"></i> <?php echo $api_name; ?> API Reports Update</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                  <!--<a href="javascript:;" class="icon-remove"></a>-->
                </span>
              </div>
              <br>
					    <span><span class="required_field"> &nbsp;&nbsp;&nbsp;*</span> marked field is mandatory.</span><br>
                <?php 
                if($api_name == 'zanox')
                {
                  ?>
                  <div class="widget-body form">
      					    <?php 
      					    $error = $this->session->flashdata('error');
      					    if($error!="") 
                    {
      						    echo '<div class="alert alert-error">
      						    <button data-dismiss="alert" class="close">x</button>
      						    <strong>Error! </strong>'.$error.'</div>';
      					    } 
                		$success = $this->session->flashdata('success');
  						      if($success!="")
                    {
  								    echo '<div class="alert alert-success">
  									  <button data-dismiss="alert" class="close">x</button>
  									  <strong>Success! </strong>'.$success.'</div>';
  						      }
                    ?>
                    <!-- BEGIN FORM-->
                    <form id="form2" action="" method="post" name="form2" class="form-horizontal">
                      <?php
                      //$attribute = array('role'=>'form','name'=>'bulkreport','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
                      //echo form_open('adminsettings/upload_apireports',$attribute);
                      ?>
                      <div class="control-group">
                        <label class="control-label">Report Date </label>
                        <div class="controls">
                          <input type="text" required name="reportdate" id="reportdate" class="span4 date-picker">
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label">Date Type </label>
                        <div class="controls">
                          <input type="radio" class="span1 newptype types" name="date_type" id="modified_date" checked="checked" value="modified_date" />Modified_date &nbsp;
                          <input type="radio" class="span1 newptype types" name="date_type" id="tracking_date"                   value="tracking_date" />Tracking_date &nbsp;
                          <input type="radio" class="span1 newptype types" name="date_type" id="review_state_changed_date"        value="review_state_changed_date" />Review_state_changed_date
                        </div>
                      </div>
                      <div class="control-group extra_hidden">
                        <label class="control-label">Program <span class="required_field">*</span></label>
                        <div class="controls">
                          <?php $aff_list  = $this->admin_model->get_affiliates_zanox_Id(); ?>
                          <select id="offer_name" class="span6 selectpicker addone" name="offer_name" data-live-search="true" onchange="Choose_store(this)">
                            <option value="all">All</option>
                            <?php
                            foreach($aff_list as $stores)
                            {
                              ?>
                              <option  value="<?php echo $stores->zanox_pgm_id;?>"><?php echo $stores->affiliate_name;?></option>
                              <?php
                            }
                            ?>
                          </select>
                          <span class="small">eg : amazon</span>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">AdSpace </label>
                        <div class="controls">
                          <input type="radio" class="span1" name="adspace" id="adspace" value="2149716" checked="checked" />Pingou (2149716) &nbsp;
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Status In Zanox </label>
                        <div class="controls">
                          <input type="radio" class="span1 status" name="zanox_status" id="zanox_status1" value="all" />All &nbsp;
                          <input type="radio" class="span1 status" name="zanox_status" id="zanox_status2" value="confirmed" />Confirmed &nbsp;
                          <input type="radio" class="span1 status" name="zanox_status" id="zanox_status3" value="open" />Open &nbsp;
                          <input type="radio" class="span1 status" name="zanox_status" id="zanox_status4" value="approved" />Approved &nbsp;
                          <input type="radio" class="span1 status" name="zanox_status" id="zanox_status5" value="rejected" />Rejected &nbsp;
                        </div>
                      </div>
                      <div class="control-group" id="newstatus" style="display:none;">
                        <div class="controls">
                          <input type="radio" class="span1" name="newstatus" id="pending"  value="0"/>Pending &nbsp;
                          <input type="radio" class="span1" name="newstatus" id="complete" value="1"/>Completed
                        </div>
                      </div>
                    
                      <div class="control-group">
                        <label class="control-label">Upload Report <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="submit" class="btn btn-success" value="Update Reports" name="update_apireports" id="update_apireports">
                        </div>
                      </div>
                      <input type="hidden" name="uploadtype" value="Report_upload">
                      <input type="hidden" name="api_name"   value="<?php echo $api_name;?>">
                    </form>
                  </div>  
                  <?php 
                }
                if($api_name == 'lomadee')
                {
                  ?>
                  <div class="widget-body form">
                    <?php 
                    $error = $this->session->flashdata('error');
                    if($error!="") 
                    {
                      echo '<div class="alert alert-error">
                      <button data-dismiss="alert" class="close">x</button>
                      <strong>Error! </strong>'.$error.'</div>';
                    } 
                    $success = $this->session->flashdata('success');
                    if($success!="")
                    {
                      echo '<div class="alert alert-success">
                      <button data-dismiss="alert" class="close">x</button>
                      <strong>Success! </strong>'.$success.'</div>';
                    }
                    ?>
                    <!-- BEGIN FORM-->
                    <form id="form2" action="" method="post" name="form2" class="form-horizontal">
                      <?php
                      //$attribute = array('role'=>'form','name'=>'bulkreport','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
                      //echo form_open('adminsettings/upload_apireports',$attribute);
                      ?>
                      <div class="control-group">
                        <label class="control-label">Report Start Date </label>
                        <div class="controls">
                          <input type="text" required name="report_startdate" id="reportdate" class="span4 date-picker">
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Report End Date </label>
                        <div class="controls">
                          <input type="text" required name="report_enddate" id="reportdate" class="span4 date-picker">
                        </div>
                      </div>
                      <!-- <div class="control-group extra_hidden">
                        <label class="control-label">Program <span class="required_field">*</span></label>
                        <div class="controls">
                          <?php $aff_list  = $this->admin_model->get_affiliates_lomadee_Id(); ?>
                          <select id="offer_name" class="span6 selectpicker addone" name="offer_name" data-live-search="true" onchange="Choose_store(this)">
                            <option value="all">All</option>
                            <?php
                            foreach($aff_list as $stores)
                            {
                              ?>
                              <option  value="<?php echo $stores->lomadee_pgm_id;?>"><?php echo $stores->affiliate_name;?></option>
                              <?php
                            }
                            ?>
                          </select>
                          <span class="small">eg : amazon</span>
                        </div>
                      </div> -->
                      <div class="control-group">
                        <label class="control-label">Publisher Id </label>
                        <div class="controls">
                          <input type="radio" class="span1" name="adspace" id="adspace" value="22679375" checked="checked" />Pingou (22679375) &nbsp;
                        </div>
                      </div>
                      
                      <div class="control-group">
                        <label class="control-label">Status </label>
                        <div class="controls">
                          <input type="radio" class="span1 status" name="lomadee_status" id="lomadee_status1" value="0" checked="checked"/>Pending &nbsp;
                          <input type="radio" class="span1 status" name="lomadee_status" id="lomadee_status2" value="1" />Confirmed &nbsp;
                          <input type="radio" class="span1 status" name="lomadee_status" id="lomadee_status3" value="2" />Canceled &nbsp;
                        </div>
                      </div>
                       
                    
                      <div class="control-group">
                        <label class="control-label">Upload Report <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="submit" class="btn btn-success" value="Update Reports" name="update_apireports" id="update_apireports">
                        </div>
                      </div>
                      <input type="hidden" name="uploadtype" value="Report_upload">
                      <input type="hidden" name="api_name"   value="<?php echo $api_name;?>">
                    </form>
                  </div>  
                  <?php 
                }
                if($api_name == 'rakuten')
                {
                  ?>
                  <div class="widget-body form">
                    <?php 
                    $error = $this->session->flashdata('error');
                    if($error!="") 
                    {
                      echo '<div class="alert alert-error">
                      <button data-dismiss="alert" class="close">x</button>
                      <strong>Error! </strong>'.$error.'</div>';
                    } 
                    $success = $this->session->flashdata('success');
                    if($success!="")
                    {
                      echo '<div class="alert alert-success">
                      <button data-dismiss="alert" class="close">x</button>
                      <strong>Success! </strong>'.$success.'</div>';
                    }
                    ?>
                    <!-- BEGIN FORM-->
                    <form id="form2" action="" method="post" name="form2" class="form-horizontal">
                      <?php
                      //$attribute = array('role'=>'form','name'=>'bulkreport','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
                      //echo form_open('adminsettings/upload_apireports',$attribute);
                      ?>
                      <div class="control-group">
                        <label class="control-label">Report Date </label>
                        <div class="controls">
                          <input type="text" required name="reportdate" id="reportdate" class="span4 date-picker">
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label">Date Type </label>
                        <div class="controls">
                          <input type="radio" class="span1 newptype types" name="date_type" id="modified_date" checked="checked" value="modified_date" />Modified_date &nbsp;
                          <input type="radio" class="span1 newptype types" name="date_type" id="tracking_date"                   value="tracking_date" />Tracking_date &nbsp;
                          <input type="radio" class="span1 newptype types" name="date_type" id="review_state_changed_date"        value="review_state_changed_date" />Review_state_changed_date
                        </div>
                      </div>
                      <div class="control-group extra_hidden">
                        <label class="control-label">Program <span class="required_field">*</span></label>
                        <div class="controls">
                          <?php $aff_list  = $this->admin_model->get_affiliates_rakuten_Id(); ?>
                          <select id="offer_name" class="span6 selectpicker addone" name="offer_name" data-live-search="true" onchange="Choose_store(this)">
                            <option value="all">All</option>
                            <?php
                            foreach($aff_list as $stores)
                            {
                              ?>
                              <option  value="<?php echo $stores->zanox_pgm_id;?>"><?php echo $stores->affiliate_name;?></option>
                              <?php
                            }
                            ?>
                          </select>
                          <span class="small">eg : amazon</span>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">AdSpace </label>
                        <div class="controls">
                          <input type="radio" class="span1" name="adspace" id="adspace" value="2149716" checked="checked" />Pingou (2149716) &nbsp;
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Status In Zanox </label>
                        <div class="controls">
                          <input type="radio" class="span1 status" name="zanox_status" id="zanox_status1" value="all" />All &nbsp;
                          <input type="radio" class="span1 status" name="zanox_status" id="zanox_status2" value="confirmed" />Confirmed &nbsp;
                          <input type="radio" class="span1 status" name="zanox_status" id="zanox_status3" value="open" />Open &nbsp;
                          <input type="radio" class="span1 status" name="zanox_status" id="zanox_status4" value="approved" />Approved &nbsp;
                          <input type="radio" class="span1 status" name="zanox_status" id="zanox_status5" value="rejected" />Rejected &nbsp;
                        </div>
                      </div>
                      <div class="control-group" id="newstatus" style="display:none;">
                        <div class="controls">
                          <input type="radio" class="span1" name="newstatus" id="pending"  value="0"/>Pending &nbsp;
                          <input type="radio" class="span1" name="newstatus" id="complete" value="1"/>Completed
                        </div>
                      </div>
                    
                      <div class="control-group">
                        <label class="control-label">Upload Report <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="submit" class="btn btn-success" value="Update Reports" name="update_apireports" id="update_apireports">
                        </div>
                      </div>
                      <input type="hidden" name="uploadtype" value="Report_upload">
                      <input type="hidden" name="api_name"   value="<?php echo $api_name;?>">
                    </form>
                  </div>  
                  <?php 
                }
                if($api_name == 'cityads')
                {
                  ?>
                  <div class="widget-body form">
                    <?php 
                    $error = $this->session->flashdata('error');
                    if($error!="") 
                    {
                      echo '<div class="alert alert-error">
                      <button data-dismiss="alert" class="close">x</button>
                      <strong>Error! </strong>'.$error.'</div>';
                    } 
                    $success = $this->session->flashdata('success');
                    if($success!="")
                    {
                      echo '<div class="alert alert-success">
                      <button data-dismiss="alert" class="close">x</button>
                      <strong>Success! </strong>'.$success.'</div>';
                    }
                    ?>
                    <!-- BEGIN FORM-->
                    <form id="form2" action="" method="post" name="form2" class="form-horizontal">
                      <?php
                      //$attribute = array('role'=>'form','name'=>'bulkreport','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
                      //echo form_open('adminsettings/upload_apireports',$attribute);
                      ?>
                      <div class="control-group">
                        <label class="control-label">Report Start Date </label>
                        <div class="controls">
                          <input type="text" required name="report_startdate" id="reportdate" class="span4 date-picker">
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Report End Date </label>
                        <div class="controls">
                          <input type="text" required name="report_enddate" id="reportdate" class="span4 date-picker">
                        </div>
                      </div>

                      <div class="control-group extra_hidden">
                        <label class="control-label">Program <span class="required_field">*</span></label>
                        <div class="controls">
                          <?php $aff_list  = $this->admin_model->get_affiliates_cityads_Id(); ?>
                          <select id="offer_name" class="span6 selectpicker addone" name="offer_name" data-live-search="true" onchange="Choose_store(this)">
                            <option value="all">All</option>
                            <?php
                            foreach($aff_list as $stores)
                            {
                              ?>
                              <option  value="<?php echo $stores->cityads_pgm_id;?>"><?php echo $stores->affiliate_name;?></option>
                              <?php
                            }
                            ?>
                          </select>
                          <span class="small">eg : amazon</span>
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label">Date Type </label>
                        <div class="controls">
                          <input type="radio" class="span1 newptype types" name="date_type" id="conversion" value="conversion" checked="checked" />Conversion &nbsp;
                          <input type="radio" class="span1 newptype types" name="date_type" id="order_upload" value="order_upload" />Order upload &nbsp;
                        </div>
                      </div>
                      
                      <!-- <div class="control-group">
                        <label class="control-label">Status</label>
                        <div class="controls">
                          <input type="radio" class="span1 status" name="cityads_status" id="cityads_status1" value="all" checked="checked" />All &nbsp;
                          <input type="radio" class="span1 status" name="cityads_status" id="cityads_status2" value="open" />Open &nbsp;
                          <input type="radio" class="span1 status" name="cityads_status" id="cityads_status3" value="rejected" />Rejected &nbsp;
                          <input type="radio" class="span1 status" name="cityads_status" id="cityads_status4" value="approved" />Approved &nbsp;
                        </div>
                      </div> -->

                      <div class="control-group">
                        <label class="control-label">Upload Report <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="submit" class="btn btn-success" value="Update Reports" name="update_apireports" id="update_apireports">
                        </div>
                      </div>
                      <input type="hidden" name="uploadtype" value="Report_upload">
                      <input type="hidden" name="api_name"   value="<?php echo $api_name;?>">
                    </form>
                  </div>  
                  <?php 
                }
                if($api_name == 'afilio')
                {
                  ?>
                  <div class="widget-body form">
                    <?php 
                    $error = $this->session->flashdata('error');
                    if($error!="") 
                    {
                      echo '<div class="alert alert-error">
                      <button data-dismiss="alert" class="close">x</button>
                      <strong>Error! </strong>'.$error.'</div>';
                    } 
                    $success = $this->session->flashdata('success');
                    if($success!="")
                    {
                      echo '<div class="alert alert-success">
                      <button data-dismiss="alert" class="close">x</button>
                      <strong>Success! </strong>'.$success.'</div>';
                    }
                    ?>
                    <!-- BEGIN FORM-->
                    <form id="form2" action="" method="post" name="form2" class="form-horizontal">
                      <?php
                      //$attribute = array('role'=>'form','name'=>'bulkreport','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
                      //echo form_open('adminsettings/upload_apireports',$attribute);
                      ?>
                      <div class="control-group">
                        <label class="control-label">Report Start Date </label>
                        <div class="controls">
                          <input type="text" required name="report_startdate" id="reportdate" class="span4 date-picker">
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Report End Date </label>
                        <div class="controls">
                          <input type="text" required name="report_enddate" id="reportdate" class="span4 date-picker">
                        </div>
                      </div>

                      <div class="control-group extra_hidden">
                        <label class="control-label">Program <span class="required_field">*</span></label>
                        <div class="controls">
                          <?php $aff_list  = $this->admin_model->get_affiliates_afilio_Id(); 
                          //print_r($aff_list);
                          ?>
                          
                          <select id="offer_name" class="span6 selectpicker addone" name="offer_name" data-live-search="true" onchange="Choose_store(this)">
                            <option value="all">All</option>
                            <?php
                            foreach($aff_list as $stores)
                            {
                              //$cat_name = $this->db->query("SELECT * from afilio_category_name_details where category_id=$stores->afilio_pgm_id AND status=1")->row('category_name');
                              ?>
                              <option  value="<?php echo $stores->afilio_pgm_id;?>"><?php echo $stores->affiliate_name;?></option>
                              <?php
                            }
                            ?>
                            <!-- <option value="382">Centauro</option> -->
                          </select>
                          <span class="small">eg : amazon</span>
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label">Date Type </label>
                        <div class="controls">
                          <input type="radio" class="span1 newptype types" name="date_type" id="validation"  checked="checked" value="modified_date" />Validation &nbsp;
                          <input type="radio" class="span1 newptype types" name="date_type" id="transaction" value="tracking_date" />Transaction &nbsp;
                          <!-- <input type="radio" class="span1 newptype types" name="date_type" id="review_state_changed_date"        value="review_state_changed_date" />Review_state_changed_date -->
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label">SiteId </label>
                        <div class="controls">
                          <input type="radio" class="span1" name="siteid" id="siteid" value="47777" checked="checked" />Pingou (47777) &nbsp;
                        </div>
                      </div>
                      <!-- <div class="control-group">
                        <label class="control-label">Status </label>
                        <div class="controls">
                          <input type="radio" class="span1 af_status" name="afilio_status" id="afilio_status1" value="all" />All &nbsp;
                          <input type="radio" class="span1 af_status" name="afilio_status" id="afilio_status2" value="pending" />Pendente &nbsp;
                          <input type="radio" class="span1 af_status" name="afilio_status" id="afilio_status3" value="refuse" />Recusado &nbsp;
                          <input type="radio" class="span1 af_status" name="afilio_status" id="afilio_status4" value="completed" />Aceito &nbsp;
                        </div>
                      </div> -->
                      <div class="control-group" id="afiliostatus" style="display:none;">
                        <div class="controls">
                          <input type="radio" class="span1" name="aff_newstatus" id="pending"  value="pending"/>pending in pingou&nbsp;
                          <input type="radio" class="span1" name="aff_newstatus" id="complete" value="complete"/>completed in pingou
                        </div>
                      </div>
                    
                      <div class="control-group">
                        <label class="control-label">Upload Report <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="submit" class="btn btn-success" value="Update Reports" name="update_apireports" id="update_apireports">
                        </div>
                      </div>
                      <input type="hidden" name="uploadtype" value="Report_upload">
                      <input type="hidden" name="api_name"   value="<?php echo $api_name;?>">
                    </form>
                  </div>  
                  <?php 
                }
                ?>
                <!-- END FORM-->
                
            </div>
                  <!-- END SAMPLE FORM widget-->
          </div>
        </div>
      </div>
         <!-- END PAGE CONTAINER-->
    </div>
      <!-- END PAGE -->  
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
  <!-- END FOOTER -->
  <!-- BEGIN JAVASCRIPTS -->
  <!-- Load javascripts at bottom, this will reduce page load time -->
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
  <script>
    jQuery(document).ready(function() {       
    // initiate layout and plugins
    App.init();
    });
  </script>
  <script type="text/javascript">
    function myFunction()
    {
      var reportdate = $('#reportdate').val();
      if(reportdate == '')
      {
         
        $('#error_file').css('color','red').text('Please Select a Report Date.');
        return false;
      }
      return true;
    }
    function ValidateFileUpload() 
    {
  	  var avatar = $("#bulkcoupon").val();
  	  var ext = avatar.split('.').pop().toLowerCase();
  	  //alert(ext);
  	  if(ext == '')
      {
  		  $('#error_file').css('color','red').text('Choose your upload file.');
  		  return false;
  	  }
      else if(ext != 'csv')
      {	
  		  $('#error_file').css('color','red').text('Invalid File Format.');
  		  return false;
  	  }
    }

    function date_status(nowstar)
    {
     
      $("#hidesh1").hide();
      $("#hidesh2").hide();
      $("#zanoxdate").hide();
      if((nowstar=='lomadee') || (nowstar=='cityads') || (nowstar=='afilio')) 
      {
       $("#hidesh1").show();
       $("#hidesh2").show();
      }
      if(nowstar=='zanox')
      {
        $("#zanoxdate").show();
      }
      if(nowstar=='rakuten')
      {
        $("#hidesh1").hide();
        $("#hidesh2").hide();  
      }
    }
    
  </script>
<script type="text/javascript">
  $('.status').click(function()
{
  var status = $(this).val();
  //var status = $("#zanox_status").val();
  if(status=='all' || status=='approve')
  {
     $("#newstatus").show();
  }
  else
  {
     $("#newstatus").hide();
  }
});

$('.af_status').click(function()
{
  var status = $(this).val();
  //var status = $("#zanox_status").val();
  if(status=='completed')
  {
     $("#afiliostatus").show();
  }
  else
  {
     $("#afiliostatus").hide();
  }
});  
</script>

  <!-- END JAVASCRIPTS -->   
  </body>
  <!-- END BODY -->
</html>