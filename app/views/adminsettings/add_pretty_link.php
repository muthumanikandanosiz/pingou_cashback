<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Retailer | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
</head>
<style type="text/css">
  .mar-lno{margin-left:0 !important;}
  .form-horizontal .controls {
    margin-left: 0 !important; 
}
.form-actions {
    border-top: none !important;
   }
   .controls img {
    height:140px !important;
   }
</style>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
          <div class="span12">
            <!-- END THEME CUSTOMIZER-->
            <!-- <h3 class="page-title">
            Pretty Link Details
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
						    <span class="divider">&nbsp;</span>
              </li>
              <li>
							  <?php echo anchor('adminsettings/pretty_link','Pretty link'); ?>
							  <span class="divider">&nbsp;</span>
              </li>
					    <li>
							  Pretty Link Details<span class="divider-last">&nbsp;</span>
              </li>
            </ul> -->
            <br>
            <span style="float:left;">
              <a href="<?php echo base_url();?>adminsettings/pretty_link" style="padding: 4px 6px !important;"class="btn btn-success">View Pretty Link</a> &nbsp;
              <!-- <a href="<?php echo base_url();?>adminsettings/bulk_store" class="btn btn-success">Import Retailers</a> -->
            </span>
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN SAMPLE FORM widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-file"></i> Pretty Link</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                  <!--<a href="javascript:;" class="icon-remove"></a>-->
                </span>
              </div>
              <br>
					    <span><span class="required_field"> &nbsp;&nbsp;&nbsp;*</span> marked fields are mandatory.</span><br>
					    <?php
    					if($action=="new")
              {
    					  ?>
                <div class="widget-body form">
        					<?php 
        					$error = $this->session->flashdata('error');
        					if($error!="") 
                  {
        					  echo '<div class="alert alert-error">
        						<button data-dismiss="alert" class="close">x</button>
        						<strong>Error! </strong>'.$error.'</div>';
        					}
        					$success = $this->session->flashdata('success');
        					if($success!="") 
                  {
        						echo '<div class="alert alert-success">
        						<button data-dismiss="alert" class="close">x</button>
        						<strong>Success! </strong>'.$success.'</div>';
        					}
                  ?>
                  <!-- BEGIN FORM-->
                  <!--<form action="#" class="form-horizontal">-->
  						    <?php
      						
      						$sess_pretty_name  = $this->session->flashdata('affiliate_name');
      						$sess_income_url   = $this->session->flashdata('logo_url');
      						$sess_external_url = $this->session->flashdata('meta_keyword');
      						$sess_status       = $this->session->flashdata('meta_description');
  							  
                  $attribute = array('role'=>'form','method'=>'post','id'=>'change_pwd','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
  							  echo form_open('adminsettings/add_pretty_link',$attribute);
  					 	    ?>
                
                    <div class="control-group">
                      <label class="control-label">Pretty Link Name <span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="text" class="span6" name="pretty_link_name" id="pretty_link_name" value="<?php if($sess_pretty_name!=""){ echo $sess_pretty_name;  }?>" required />
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">Incoming  URL <span class="required_field">*</span></label>
                      <div class="controls">
                        <span class="span3" style="font-size:15px;"><?php echo base_url();?></span><input type="text" class="span5" name="income_url" id="income_url" value="<?php if($sess_income_url!=""){ echo $sess_income_url;  }?>" required style="width: 25.5%;"/>
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">External URL <span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="url" class="span6" name="external_url" id="external_url" value="<?php if($sess_external_url!=""){ echo $sess_external_url;  }?>" required />
                      </div>
                    </div>
                    <!-- New code for header tag details 26-4-17 --> 
                    <div class="control-group">
                      <label class="control-label">Tag Status <span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="radio" class="span3 ptype" name="tag_status" value="index-follow">Index/Follow &nbsp;
                        <input type="radio" class="span3 ptype" name="tag_status" value="noindex-nofollow">No Index/No Follow &nbsp;
                        <input type="radio" class="span3 ptype" name="tag_status" value="noindex-follow"> No Index/Follow &nbsp;
                        <input type="radio" class="span3 ptype" name="tag_status" value="index-nofollow"> Index/No Follow<br>
                      </div>
                    </div> 
                    <!-- <div class="control-group">
                      <label class="control-label">Meta Properties<span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="radio" class="span3 ptype" name="meta_properties" value="index-follow">Extrack them from the "External URL"  &nbsp;
                        <input type="radio" class="span3 ptype" name="meta_properties" value="noindex-nofollow">Set informations now
                      </div>
                    </div> --> 

                    <!-- New code for met property details 25-7-17 -->
                    <div class="control-group">
                      <label class="control-label">Meta image for Pretty Link</label>
                      <div class="controls">
                        <input type="file" class="span6" name="pretty_link_img" id="pretty_link_img" required /><br>
                        <!-- <span>Note: Admin Avatar should be in (250 * 250) in size</span> --><br>
                        <!-- <img src="<?php echo $this->admin_model->get_img_url(); ?>uploads/adminpro/<?php echo $admin_details->pingou_meta_image; ?>" width="150" height="250"> -->
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Pretty meta title <span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="text" class="span6" name="pretty_link_title" id="pretty_link_title" value="" required />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Pretty meta description <span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="text" class="span6" name="pretty_link_desc" id="pretty_link_desc" value="" required />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Pretty meta siteurl <span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="text" class="span6" name="pretty_link_siteurl" id="pretty_link_siteurl" value="" required />
                      </div>
                    </div>
                    <!-- End 25-7-17 -->


                    <!-- End 26-4-17 -->
                    <div class="control-group">
                      <label class="control-label">Status</label>
                      <div class="controls">
                        <select name="status" class="span6">
                          <option value="1">Active</option>
                          <option value="0">De active</option>
                        </select>
                      </div>
                    </div>
                  <!-- End -->                         
						      
                  <div class="form-actions">
                    <input type="submit" name="save" value="Submit" class="btn btn-success">
                  </div>
						      <?php echo form_close(); ?>
                  <!--</form>-->
                  <!-- END FORM-->
                </div>
					      <?php
              }
              ?>
					    <?php
						  if($action=="edit")
              {
						    ?>
                <div class="widget-body form">
  					      <?php
                  $error = $this->session->flashdata('error');
  					      if($error!="") 
                  {
        						echo '<div class="alert alert-error">
        						<button data-dismiss="alert" class="close">x</button>
        						<strong>Error! </strong>'.$error.'</div>';
        					}  
          				$success = $this->session->flashdata('success');
          				if($success!="") 
                  {
          					echo '<div class="alert alert-success">
          					<button data-dismiss="alert" class="close">x</button>
          					<strong>Success! </strong>'.$success.'</div>';
          				}
                  ?>
                  <!-- BEGIN FORM-->
  						    <?php
  							  $attribute = array('role'=>'form','name'=>'faq','method'=>'post','id'=>'update_form','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
  							  echo form_open('adminsettings/update_pretty_link',$attribute);
  						    ?>

                    <div class="control-group">
                      <label class="control-label">Pretty Link Name <span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="text" class="span6" name="pretty_link_name" id="pretty_link_name" value="<?php echo $pretty_link_name; ?>" required />
                      </div>
                    </div>
                  
                    <div class="control-group">
                      <label class="control-label">Incoming URL <span class="required_field">*</span></label>
                      <div class="controls">
                        <span class="span3" style="font-size:15px;"><?php echo base_url();?></span><input type="text" class="span5" name="income_url" id="income_url" value="<?php echo $income_url; ?>" required style="width: 25.5%;"/>
                      </div>
                    </div>
                    
                    <div class="control-group">
                      <label class="control-label">External URL <span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="text" class="span6" name="external_url" id="external_url" value="<?php echo $external_url; ?>" required />
                      </div>
                    </div>

                    <!-- New code for header tag details 26-4-17 --> 
                    <div class="control-group">
                      <label class="control-label">Tag Status <span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="radio" class="span3 ptype" name="tag_status" <?php if($tag_status=="index-follow"){ echo 'checked="checked"';} ?>value="index-follow">Index/Follow &nbsp;
                        <input type="radio" class="span3 ptype" name="tag_status" <?php if($tag_status=="noindex-nofollow"){ echo 'checked="checked"';} ?>value="noindex-nofollow">No Index/No Follow &nbsp;
                        <input type="radio" class="span3 ptype" name="tag_status" <?php if($tag_status=="noindex-follow"){ echo 'checked="checked"';} ?>value="noindex-follow"> No Index/Follow &nbsp;
                        <input type="radio" class="span3 ptype" name="tag_status" <?php if($tag_status=="index-nofollow"){ echo 'checked="checked"';} ?>value="index-nofollow"> Index/No Follow<br>
                      </div>
                    </div> 
                    
                    <!-- <div class="control-group">
                      <label class="control-label">Meta Properties<span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="radio" class="span3 ptype" name="meta_properties" <?php if($meta_properties=="0"){ echo 'checked="checked"';} ?> value="0">Extrack them from the "External URL"  &nbsp;
                        <input type="radio" class="span3 ptype" name="meta_properties" <?php if($meta_properties=="1"){ echo 'checked="checked"';} ?> value="1">Set informations now
                      </div>
                    </div> --> 
                    <!-- End 26-4-17 -->


                    <!-- New code for met property details 25-7-17 -->
                    <div class="control-group">
                      <label class="control-label">Meta image for Pretty Link</label>
                      <div class="controls">
                        <input type="file" class="span6" name="pretty_link_img" id="pretty_link_img" /><br>
                        <!-- <span>Note: Admin Avatar should be in (250 * 250) in size</span> --><br>
                        <img src="<?php echo $this->admin_model->get_img_url(); ?>uploads/adminpro/<?php echo $meta_image; ?>" width="150" height="250" style="margin-left: 160px;">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Pretty meta title <span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="text" class="span6" name="pretty_link_title" id="pretty_link_title" value="<?php echo $meta_title; ?>" required />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Pretty meta description <span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="text" class="span6" name="pretty_link_desc" id="pretty_link_desc" value="<?php echo $meta_desc; ?>" required />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Pretty meta siteurl <span class="required_field">*</span></label>
                      <div class="controls">
                        <input type="text" class="span6" name="pretty_link_siteurl" id="pretty_link_siteurl" value="<?php echo $meta_url; ?>" required />
                      </div>
                    </div>
                    <!-- End 25-7-17 -->

                    <div class="control-group">
                      <label class="control-label">Status</label>
                      <div class="controls">
                        <select name="status" class="span6">
                          <option value="1" <?php if($pretty_link_status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
                          <option value="0" <?php if($pretty_link_status=='0'){ echo 'selected="selected"'; } ?>>De-active</option>
                        </select>
                      </div>
                    </div>
                    <!-- End -->
    						    <input type="hidden" name="pretty_link_id" id="pretty_link_id" value="<?php echo $pretty_link_id; ?>">
                    <input type="hidden" name="hidden_meta_img" id="hidden_meta_img" value="<?php echo $meta_image; ?>">        
  						      <div class="form-actions">
                      <input type="submit" name="save" value="Save Changes" class="btn btn-success">
                    </div>

						      <?php echo form_close(); ?>
                </div>
					      <?php
              }
              ?>
            </div>
            <!-- END SAMPLE FORM widget-->
          </div>
        </div>
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->  
  </div>
   
   
    
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
  <!-- END FOOTER -->
  <!-- BEGIN JAVASCRIPTS -->
  <!-- Load javascripts at bottom, this will reduce page load time -->
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
	<script>
    $(document).ready(function() 
    {       
      // initiate layout and plugins
      App.init();
    });
  </script>
 <!--  <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/css/jquery-checktree.css" rel="stylesheet" type="text/css">
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery.min.js"></script> 
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-checktree.js"></script>  -->
  <script>
  $('#tree').checktree();
  </script>
  <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>