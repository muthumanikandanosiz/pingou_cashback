<!DOCTYPE html>
<html lang="en"> 
<head>
   <meta charset="utf-8" />
   <?php $admin_details = $this->admin_model->get_admindetails(); ?>
   <title>Export Report | <?php echo $admin_details->site_name; ?> Admin</title>
   <?php $this->load->view('adminsettings/script'); ?>

     <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
     <link rel="stylesheet" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
     <link rel="stylesheet" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php $this->load->view('adminsettings/header'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
    <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('adminsettings/sidebar'); ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
              <div class="span12" style="margin-top:13px;"> 
                  <!-- <h3 class="page-title">
                     Update Reports
                  </h3> -->
                   <!-- <ul class="breadcrumb">
                      <li>
                          <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
                          <span class="divider">&nbsp;</span>
                      </li>
                      <li>
                          <?php echo anchor('adminsettings/bulk_store','Export Reports'); ?>
                          <span class="divider-last">&nbsp;</span>
                       </li>
                    </ul> -->
                    <span style="float:left;">
                      <a href="<?php echo base_url();?>adminsettings/reports" class="btn btn-success">View Report</a> &nbsp;
                    </span>
              </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
          <div class="row-fluid">
            <div class="span12">
                  <!-- BEGIN SAMPLE FORM widget-->
              <div class="widget">
                  <div class="widget-title">
                    <h4><i class="icon-file"></i> Export Reports</h4>
                    <span class="tools">
                      <a href="javascript:;" class="icon-chevron-down"></a>
                    </span>
                  </div> 
                   
                  <?php
                  if($action=="new")
                  {
                  ?>
                    <div class="widget-body form">
                      <?php 
                      $error = $this->session->flashdata('error');
                      if($error!="") {
                        echo '<div class="alert alert-error">
                        <button data-dismiss="alert" class="close">x</button>
                        <strong>Error! </strong>'.$error.'</div>';
                      }?>
                      <?php
                      $success = $this->session->flashdata('success');
                      if($success!="") {
                        echo '<div class="alert alert-success">
                        <button data-dismiss="alert" class="close">x</button>
                        <strong>Success! </strong>'.$success.'</div>';
                      }?>
                    <!-- BEGIN FORM-->
                
                    <!-- s -->
                    <div class="row" style="margin-left:0px; !important; ">
                      <div class="span12">
                        <!-- <ul class="nav nav-tabs ul-edit responsive" style="border-bottom:none;">
                          <li class="active" style="width:18%; margin-left:30px;"><a data-toggle="tab" href="#tab-pending-cashback" class="btn btn-success1">Pending Cashback</a></li>
                          <li style="margin-left:10px; width:18%;" class=""><a data-toggle="tab" href="#tab-missing-cashback" class="btn btn-success1">Missing Cashback</a> </li>
                          <li style="margin-left:10px; width:18%;" class=""><a data-toggle="tab" href="#tab-withdraws" class="btn btn-success1">Withdraws</a></li>
                          <li style="margin-left:10px; width:18%;" class=""><a data-toggle="tab" href="#tab-payments" class="btn btn-success1">Payments</a></li>
                          
                        </ul> 
                      
                      <br><br>  --> 
                     
                      <!-- <h4>2. Choose the filders to generate export</h4><br> -->
                      <div class="row" style="margin-left:0px; !important; ">
                      <div class="span12">
                        <div class="tab-content">

                        <!--Pending Cashback Menu Start -->

                        <?php 
                        if($update_type == "pending_cashback")
                        {
                          ?>
                          <h3>Update Pending Cashback Details</h3>
                          <?php
                          //form begin
                          $attributes = array('role'=>'form','name'=>'profile_form','id'=>'profile_form','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data','onsubmit'=>"return Validate(this);");
                          echo form_open('adminsettings/update_report',$attributes);
                          ?>  
                          <div class="control-group" style="margin-top: 5%; margin-bottom:8% !important;">          
                            <div class="controls"> 
                              <input type="text" id="uploadFile" name="" style="width:41%; margin-left:15%;" class="span6" placeholder="Click to select the .csv file from your computer" disabled="disabled"/>
                                  <div class="fileUpload btn btn-success">
                                    <span>Select the file</span>
                                    <input id="uploadBtn" required  name="reports" type="file" class="upload"/>
                                  </div>&nbsp;&nbsp;&nbsp;&nbsp;<a style="color: rgb(135, 206, 4);" href="<?php echo base_url(); ?>uploads/csv_examples/pending_cashback.csv"><img align="absmiddle" src="<?php echo base_url(); ?>front/images/csv.png"> example.csv</a>
                            </div> 
                            <span id="error_file"></span>
                            <br>
                            <p style="margin-left:47%;color:red">Supported File Formats : .csv </p>       
                          </div>
                          <input type="hidden" name="type" value="pending_cashback">
                          <div style="margin-left: 35% !important;" class="span12">
                            <input type="submit" name="save" value="Click to Upload the Report/Table" class="btn btn-success">
                          </div>
                          <?php echo form_close();?>
                          <?php 
                        }
                        ?>

                        <?php 
                        if($update_type == "missing_cashback")
                        {
                          ?>
                          <h3>Update Missing Cashback Details</h3>
                          <!-- <div class="tab-pane fade" id="tab-missing-cashback"> --> 
                          <?php
                          //form begin
                          $attributes = array('role'=>'form','name'=>'profile_form','id'=>'profile_form','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data','onsubmit'=>"return Validate(this);");
                          echo form_open('adminsettings/update_report',$attributes);
                          ?>  
                          <div class="control-group" style="margin-top: 5%; margin-bottom:8% !important;">          
                            <div class="controls"> 
                              <input type="text" id="uploadFile1" name="" style="width:41%; margin-left:15%;" class="span6" placeholder="Click to select the .csv file from your computer" disabled="disabled"/>
                              <div class="fileUpload btn btn-success">
                                <span>Select the file</span>
                                <input id="uploadBtn1" required  name="reports" type="file" class="upload"/>
                              </div>&nbsp;&nbsp;&nbsp;&nbsp;<a style="color: rgb(135, 206, 4);" href="<?php echo base_url(); ?>uploads/csv_examples/missing_cashback.csv"><img align="absmiddle" src="<?php echo base_url(); ?>front/images/csv.png"> example.csv</a>
                            </div>
                            <span id="error_file"></span>
                            <br>
                            <p style="margin-left:47%;color:red">Supported File Formats : .csv </p>
                          </div>
                          <input type="hidden" name="type" value="missing_cashback">
                          <div style="margin-left: 35% !important;" class="span12">
                            <input type="submit" name="save" value="Click to Upload the Report/Table" class="btn btn-success">
                          </div>
                          <?php echo form_close();?>
                          <!-- </div> -->
                          <?php 
                        }
                        ?>

                        <?php 
                        if($update_type == "withdraw")
                        {
                          ?>
                          <h3>Update Withdraw Details</h3>
                          <!-- <div class="tab-pane fade" id="tab-withdraws"> --> 
                          <?php
                          //form begin
                          $attributes = array('role'=>'form','name'=>'profile_form','id'=>'profile_form','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data','onsubmit'=>"return Validate(this);");
                          echo form_open('adminsettings/update_report',$attributes);
                          ?>  
                          <div class="control-group" style="margin-top: 5%; margin-bottom:8% !important;">          
                            <div class="controls"> 
                                    <input type="text" id="uploadFile2" name="" style="width:41%; margin-left:15%;" class="span6" placeholder="Click to select the .csv file from your computer" disabled="disabled"/>
                                        <div class="fileUpload btn btn-success">
                                          <span>Select the file</span>
                                          <input id="uploadBtn2" required  name="reports" type="file" class="upload"/>
                                        
                                        </div>&nbsp;&nbsp;&nbsp;&nbsp;<a style="color: rgb(135, 206, 4);" href="<?php echo base_url(); ?>uploads/csv_examples/withdraw.csv"><img align="absmiddle" src="<?php echo base_url(); ?>front/images/csv.png"> example.csv</a>
                            </div>
                            <br>
                            <p style="margin-left:47%;color:red">Supported File Formats : .csv </p>
                          </div>
                          <input type="hidden" name="type" value="withdraws">
                          <div style="margin-left: 35% !important;" class="span12">
                            <input type="submit" name="save" value="Click to Upload the Report/Table" class="btn btn-success">
                          </div>
                          <?php echo form_close();?>
                          <!-- </div> -->
                          <?php 
                        }
                        ?>

                        <?php 
                        if($update_type == "payment")
                        {
                          ?>
                          <h3>Update Payment Details</h3>
                          <!-- <div class="tab-pane fade" id="tab-payments">  -->
                          <?php
                          //form begin
                          $attributes = array('role'=>'form','name'=>'profile_form','id'=>'profile_form','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data','onsubmit'=>"return Validate(this);");
                          echo form_open('adminsettings/update_report',$attributes);
                          ?>  
                          <div class="control-group" style="margin-top: 5%; margin-bottom:8% !important;">          
                            <div class="controls"> 
                              <input type="text" id="uploadFile3" name="" style="width:41%; margin-left:15%;" class="span6" placeholder="Click to select the .csv file from your computer" disabled="disabled"/>
                              <div class="fileUpload btn btn-success">
                                <span>Select the file</span>
                                <input id="uploadBtn3" required  name="reports" type="file" class="upload"/>
                              </div>&nbsp;&nbsp;&nbsp;&nbsp;<a style="color: rgb(135, 206, 4);" href="<?php echo base_url(); ?>uploads/csv_examples/payments.csv"><img align="absmiddle" src="<?php echo base_url(); ?>front/images/csv.png"> example.csv</a>
                            </div>
                            <span id="error_file"></span>
                            <br>
                            <p style="margin-left:47%;color:red">Supported File Formats : .csv </p>
                          </div>
                          <input type="hidden" name="type" value="payments">
                          <div style="margin-left: 35% !important;" class="span12">
                            <input type="submit" name="save" value="Click to Upload the Report/Table" class="btn btn-success">
                          </div>
                          <?php echo form_close();?>
                          <!-- </div> -->
                          <?php 
                        }
                        ?>
                    </div>      
                    </div>
                  </div> 
                    </div>      
                    </div>
                  </div>  
                  <?php
                  } 
                  ?>

              </div>
                  <!-- END SAMPLE FORM widget-->
            </div>
          </div>
        </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
    <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
   </script>
 
<script>

var _validFileExtensions = [".csv"];    
function Validate(oForm) {
    var arrInputs = oForm.getElementsByTagName("input");
    for (var i = 0; i < arrInputs.length; i++) {
        var oInput = arrInputs[i];
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
                
                if (!blnValid) {
                    alert("Allowed a " + _validFileExtensions.join(", ") + " formats only");
                    return false;
                }
            }
        }
    }
  
    return true;
}

document.getElementById("uploadBtn").onchange = function () {

    var newnew = this.value.replace("C:\\fakepath\\", "");

    document.getElementById("uploadFile").value = newnew;
};
document.getElementById("uploadBtn1").onchange = function () {

     var newnew1 = this.value.replace("C:\\fakepath\\", "");
    document.getElementById("uploadFile1").value = newnew1;
};
document.getElementById("uploadBtn2").onchange = function () {
    
     var newnew2 = this.value.replace("C:\\fakepath\\", "");
    document.getElementById("uploadFile2").value = newnew2;
};
document.getElementById("uploadBtn3").onchange = function () {

     var newnew3 = this.value.replace("C:\\fakepath\\", "");
    document.getElementById("uploadFile3").value = newnew3;
};
</script>
   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>
<style>


.fileUpload {
    position: relative;
    overflow: hidden;
    margin-bottom: 10px;
    margin-left: 20px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}

li .btn-success1
{
  height:40px; background: #87bb33;padding-bottom: 0px !important; border-radius: 4px 4px 4px 4px !important;
padding-top: 0px !important; line-height:40px !important;
}

</style>