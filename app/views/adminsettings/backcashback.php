<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Cashback | <?php echo $admin_details->site_name; ?> Admin</title>
  <?php $this->load->view('adminsettings/script'); ?>
  <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <style>
    div.selector, div.selector span, div.checker span, div.radio span, div.uploader, div.uploader span.action, div.button, div.button span
    {
      background-image:none
    }
  </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN THEME CUSTOMIZER-->
            <!--<div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
            <span class="settings">
            <span class="text">Theme:</span>
            <span class="colors">
            <span class="color-default" data-style="default"></span>
            <span class="color-gray" data-style="gray"></span>
            <span class="color-purple" data-style="purple"></span>
            <span class="color-navy-blue" data-style="navy-blue"></span>
            </span>
            </span>
            </div>-->
            <!-- END THEME CUSTOMIZER-->
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
           <!--  <h3 class="page-title">
            Cashback
            </h3> -->
           <!--  <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
						    <span class="divider">&nbsp;</span>
              </li>
              <li><?php echo anchor('adminsettings/cashback','Cashback'); ?>
							  <span class="divider-last">&nbsp;</span>
					    </li>
            </ul> -->
            <span style="float:left;margin-top: 15px;"> 
              <a href="<?php echo base_url();?>adminsettings/reports" class="btn btn-success">Reports</a> &nbsp;
              <!-- <a href="<?php echo base_url();?>adminsettings/stores"   class="btn btn-success">Stores</a> -->
            </span> 
            <!-- END PAGE TITLE & BREADCRUMB-->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <!-- BEGIN ADVANCED TABLE widget-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN EXAMPLE TABLE widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i> Cashback</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                </span>
              </div>
              <div class="widget-body">
						    <?php 
					      $error = $this->session->flashdata('error');
					      if($error!="")
                {
      						echo '<div class="alert alert-error">
      						<button data-dismiss="alert" class="close">x</button>
      						<strong>Error! </strong>'.$error.'</div>';
      					} 
      					$success = $this->session->flashdata('success');
      					if($success!="") 
                {
      						echo '<div class="alert alert-success">
      						<button data-dismiss="alert" class="close">x</button>
      					  <strong>Success! </strong>'.$success.'</div>';			
      					}
                ?>
						    <form id="form2" action="" method="post" name="form2">
                  <table class="table table-striped table-bordered table-hover" id="dynamic-table">
                    <thead>
                      <tr>
                        <th class="center">#</th>
									      <th class="center"><input type="checkbox" id="check_b" class="check_b" onchange="checkAll(this)" name="chk[]" /></th><!-- seetha-->
                        <th class="center">User Id</th>
                        <th class="center">User Name</th>
                        <th class="center">User Email</th>
                        <th class="center">Store</th>
                        <th class="center">Cashback</th>
                        <th class="center"> Transc Amt</th>
                        <th class="center">Transaction Date</th>
                        <th class="center">Status</th>
                        <th class="center">Edit</th>
                        <th class="center">Report Update ID</th>
                        <th class="center">Status Date Updated</th>
                        <th class="center">Referral</th>
                        <th class="center">txn_id</th>
                        <th class="center">new_txn_id</th>
                        <th class="center">Plataform</th>
                        <th class="center">Delete</th>
                      </tr>
                    </thead>
                  </table>
      						<input type="hidden" name="hidd" value="hidd"><!-- seetha-->
      						<input id="GoUpdate" class="btn btn-warning" type="submit" value="Delete Cashback" name="GoUpdate">
      					</form>
              </div>
            </div>
            <!-- END EXAMPLE TABLE widget-->
          </div>
        </div>
        <!-- END ADVANCED TABLE widget-->
        <!-- END PAGE CONTENT-->
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
  <!-- END FOOTER -->
  <!-- BEGIN JAVASCRIPTS -->
  <!-- Load javascripts at bottom, this will reduce page load time -->
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>   
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->   
  <!--<script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>-->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
  
  <!-- New js files 17-12-16 -->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>

  <!-- End 17-12-16 -->


  <script>
      jQuery(document).ready(function() {
         // initiate layout and plugins
         App.init();
      });
   </script>
   <script type="text/javascript">
function confirmDelete(m)  // Confirm before delete cashback..
{
	if(!confirm(m))
	{
		return false;
	}
	else
	{
		return true;
	}
}
</script>
<!-- seetha-->
<script>
	  $(document).ready(function() {
		$(".check_b").attr("style", "opacity: 1;");
	   
    $('#dynamic-table').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
      "url": "<?php echo site_url('adminsettings/newcashback')?>",
      "data": {
      //"totalrecords": "<?php echo $iTotal; ?>"
      }
      }
    });


    });
	
    function checkAll(ele) {    //multiple delete cashback
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }
</script>
</body>
<!-- END BODY -->
</html>
