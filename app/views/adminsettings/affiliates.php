<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Retailers | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>
  <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <style type="text/css">  
    .all_des > a {
        float: left;
        margin-left: 3px;
        margin-right: 4px;
    }
    .all_des 
    {
      width: 100px;
    }
    .all_des .span12 
    {
      float: left !important;
      width: 62px !important;
    }
    div.selector, div.selector span, div.checker span, div.radio span, div.uploader, div.uploader span.action, div.button, div.button span
    {
      background-image:none
    }

    /*New style for datatable related css 17-12-16*/
    .col-xs-6 {width: 47% !important; float: left;min-height:1px;padding-left: 12px;padding-right: 12px;position: relative;}
    select.input-sm { width:60px !important;}
    .row {margin-left: -12px;margin-right: -12px;}
    .pagination {border-radius: 4px;margin: 20px 0;padding-left: 0;}
    /*End 17-12-16*/
  </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <!-- <h3 class="page-title">
            Retailers
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
						    <span class="divider">&nbsp;</span>
              </li>
              <li><?php echo anchor('adminsettings/affiliates','Retailers'); ?>
							  <span class="divider-last">&nbsp;</span>
					    </li>
            </ul> -->
            <span style="float:left;margin-top: 15px;"> <!--  margin-left:-14%;  -->
              <a href="<?php echo base_url();?>adminsettings/addaffiliate" class="btn btn-success">Add Retailer</a> &nbsp;
              <a href="<?php echo base_url();?>adminsettings/bulk_store"   class="btn btn-success">Import Retailers</a> &nbsp;
              <a href="<?php echo base_url();?>adminsettings/site_affiliates"   class="btn btn-success">Direct Affiliates</a>
            </span>
            <span style="float:right; margin-right: -7px; margin-top: 15px;">
              <p class="badge badge-info">Featured</p> &nbsp;//
              <p class="badge badge-success">Ret.Week</p> //
              <p class="badge badge-important">Both</p> &nbsp;
            </span>
            <!-- END PAGE TITLE & BREADCRUMB-->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <!-- BEGIN ADVANCED TABLE widget-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN EXAMPLE TABLE widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i> Retailers</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                </span>
              </div>
              <div class="widget-body">
    						<?php 
    					  $error = $this->session->flashdata('error');
    					  if($error!="") 
                {
    						  echo '<div class="alert alert-error">
    						  <button data-dismiss="alert" class="close">x</button>
    						  <strong>Error! </strong>'.$error.'</div>';
    					  }
    						$success = $this->session->flashdata('success');
    						if($success!="") 
                {
    							echo '<div class="alert alert-success">
    							<button data-dismiss="alert" class="close">x</button>
    							<strong>Success! </strong>'.$success.'</div>';			
    						}
                ?>     
                <!-- 
                <div class="row-fluid">
                  <div class="span3">
                    <table width="100%" cellspacing="2" cellpadding="3" border="0" align="center">
                      <tbody>
                        <tr>
                          <td valign="middle" align="left" class="tb2">Featured </td>
                          <td valign="middle" align="right" class="stat_s"><span class="badge badge-info">&nbsp;</span></td>
                        </tr>
                        <tr>
                          <td valign="middle" align="left" class="tb2">Retailer Of The Week </td>
                          <td valign="middle" align="right" class="stat_s"><span class="badge badge-success">&nbsp;</span></td>
                        </tr>
                        <tr>
                          <td valign="middle" align="left" class="tb2">Featured &amp; Retailer Of The Week </td>
                          <td valign="middle" align="right" class="stat_s"><span class="badge badge-important">&nbsp;</span></td>
                        </tr>
                        <tr>
                          <td colspan="2"><div class="sline"></div></td>
                        </tr>
                      </tbody>
                    </table>
						      </div>
                </div>
                -->
                <br>
                <form id="form2" action="" method="post" name="form2">
                  <table class="table table-striped table-bordered" id="sample_teste1">
                    <thead>
                      <tr>
                        <th><center><input type="checkbox" id="check_b" class="check_b grpall" name="chk[]" /></center></th>
                        <th style="">ID</th>
                        <th class="hidden-phone">Retailer Logo</th>
                        <th>Retailer Name (Status)</th> 
                        <th class="hidden-phone">E</th>
                        <th class="hidden-phone">Coupons</th>
                        <th class="hidden-phone">CashB</th>
                        <th class="hidden-phone">Old CB</th>
                        <th>Visits</th>
                        <th>Categ</th> 
                        <th>No Days</th> 
                        <th class="hidden-phone">Pays in</th>
                        <th class="hidden-phone">1st Track</th>
                        <th class="hidden-phone">2st Track</th>
                        <th class="hidden-phone">Web</th>
                        <th class="hidden-phone">Android</th>
                        <th class="hidden-phone">Actions</th>
                      </tr>
                    </thead>
                  </table>
                  <input type="hidden" name="hidd" value="hidd">
                  <input id="GoUpdate" class="btn btn-warning" type="submit" value="Update Retailers" name="GoUpdate">
                </form>
              </div>
            </div>
            <!-- END EXAMPLE TABLE widget-->
          </div>
        </div>
        <!-- END ADVANCED TABLE widget-->
        <!-- END PAGE CONTENT-->
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
  <!-- END FOOTER -->
  <!-- BEGIN JAVASCRIPTS -->
  <!-- Load javascripts at bottom, this will reduce page load time -->
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>   
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->   
  <!--<script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>-->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
  
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script> 

  <script>
    jQuery(document).ready(function() {
    // initiate layout and plugins
    App.init();
    });
  </script>
  <script type="text/javascript">
    function confirmDelete()  // Confirm before delete coupon..
    {
      var m = "Do you want to delete this affiliate detail?";
      if(!confirm(m))
      {
        return false;
      }
      else
      {
        return true;
      }
    }
  </script>
  <script>
    $(document).ready(function() 
    {
    	$(".check_b").attr("style", "opacity: 1;");
    });

    	  /*function checkAll(ele) {
          
         var checkboxes = document.getElementsByTagName('input');
         if (ele.checked) {
             for (var i = 0; i < checkboxes.length; i++) {
                 if (checkboxes[i].type == 'checkbox') {
                     checkboxes[i].checked = true;
                 }
             }
         } else {
             for (var i = 0; i < checkboxes.length; i++) {
                 console.log(i)
                 if (checkboxes[i].type == 'checkbox') {
                     checkboxes[i].checked = false;
                 }
             }
         }
        }*/
  $('input.grpall').click(function()
  {
    if($(this).is(':checked') == true)
    {
      $(this).attr('checked','checked');
      $('input.chksingle').each(function(){
      $(this).attr('checked','checked');
      }) 
    }
    else
    {
      $(this).removeAttr('checked');
      $('input.chksingle').each(function(){
      $(this).removeAttr('checked');
      }) 
    }
    //return false;
 });
  </script>

  <script type="text/javascript">

    $(document).ready(function() 
    {
      $('#sample_teste1').DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [{
      "targets": 0,
      "orderable": false
      }],
      "ajax": {
      "url": "<?php echo site_url('adminsettings/newaffiliates')?>",
      "data": {
      //"totalrecords": "<?php echo $iTotal; ?>"
      //"coupon_name": "<?php echo $coupon_name; ?>"
      }
      }
      });
    });
  </script>

</body>
<!-- END BODY -->
</html>
<style type="text/css">
 .dataTables_processing
{
  display:none !important;
}
</style>
<link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/css/pagination.css" rel="stylesheet" />
                            