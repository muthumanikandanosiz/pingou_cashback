<?php 
$role_id 	 = $this->session->userdata('admin_id');
$user_access = $this->session->userdata('user_access');
?>

<style type="text/css">
	.sidebar-menu .fontcls
	{
		font-family:'Arial';
	}
	.cls_doun_hc{
		line-height: 60px !important;
	}
	#sidebar > ul > li > a.bew_io {
	display: table-cell;
	height: 45px;
	vertical-align: middle;
	}
	.cls_doun_hc span.icon-box{
		margin-right: 0!important;

	}
	.cls_dou_icon{
		float: left !important;
	    padding: 10px !important;
	}
	.cls_dou_icon a{ 
		padding-left:0 !important;
		text-decoration: none;
	}
	.cls_new_cin i{
		background:#32C2CD;
		height:25px;
		width:25px;
		line-height:41px;/*line-height:25px;*/
		text-align:center;
		padding:3px;
		border-radius:50%;
	}
	.cls_sjm {
	    background: rgba(0, 0, 0, 0.2) none repeat scroll 0 0 !important;
	    margin-right: 10px;
	    padding: 20px !important;
	}
	.bew_c {
	    padding: 0 7px 0 0 !important;
	}
	ul.sidebar-menu span.icon-box {
	    margin-right: 10px;
	    padding: 20px 25px;
	}

	#sidebar > ul > li.cls_new_cin a.cls1 {
	    background: rgba(0, 0, 0, 0) none repeat scroll 0 0 !important;
	}
	.bew_c {
	    background: rgba(0, 0, 0, 0.1) none repeat scroll 0 0 !important;
	    display: table-cell;
	}
	.bew_io i{
		background:transparent;
	}
	.bew_c span {
	    margin-top: 15px;
			margin-left:0px !important;
	}

	.cls_pad_l {
	    padding-left: 4px !important;
	}
	.bew_c {
	    background: rgba(0, 0, 0, 0.1) none repeat scroll 0 0 !important;
	    display: table-cell !important;
	}
	.cls_new_cin a{
		padding-left:12px;
	}
	.ul.sidebar-menu span.icon-box{
		height: 40px;
		padding: 20px 25px;
	}


	#sidebar ul {
	    margin-top: 50px;
	    width: 235px;
	}
	#sidebar {
	    position: absolute;
	    width: 235px;
	}
	#main-content {
	    margin-left: 235px;
	    margin-top: 0;
	    min-height: 800px;
	}
	.coupon_api
	{
		margin-bottom: -43px !important;
	    margin-top: 10%;
	    text-align: center;
	}
</style>

<div id="sidebar" class="nav-collapse collapse">
	<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
	<div class="sidebar-toggler hidden-phone"></div>
	<?php
		// $dashb = 'adminsettings/dashboard';
	?>
	<?php if(in_array('15',$user_access) OR $role_id == '1'){ ?>
	<div class="coupon_api">
		<a href="<?php echo base_url();?>adminsettings/affiliate_network/zanox">Z</a>&nbsp;&nbsp;
		<a href="<?php echo base_url();?>adminsettings/affiliate_network/cityads">C</a>&nbsp;&nbsp;
		<a href="<?php echo base_url();?>adminsettings/affiliate_network/rakuten">R</a>&nbsp;&nbsp;
		<a href="<?php echo base_url();?>adminsettings/affiliate_network/afilio">A</a>&nbsp;&nbsp;
		<a href="<?php echo base_url();?>adminsettings/affiliate_network/lomadee">L</a>&nbsp;&nbsp;
		<a href="<?php echo base_url();?>adminsettings/backup">B</a>&nbsp;&nbsp;
	</div>
	<?php } ?>

	<ul class="sidebar-menu">

		<!-- Coupons Menu -->
		<?php if(in_array('1',$user_access) OR $role_id == '1'){ ?>
		<li class="cls_sideacc">
			<span class="cls"> 
				<a href="<?php echo base_url();?>adminsettings/bulkcoupon" class="icon-box">
					<i class="newicon-upload2"></i>
				</a>
			</span>	
			<span class="cls">
				<a href="<?php echo base_url();?>adminsettings/addcoupon" class="icon-box">
					<i class="newicon-plus-circle"></i>
				</a>
			</span>	
			<span class="icon-box">
				<a class="" href="<?php echo base_url();?>adminsettings/coupons">
					<i class="icon-tags"></i>&nbsp;<span class="fontcls">Coupons</span>
				</a>
			</span>	
		</li>
		<?php } ?>

		<!-- Retailers Menu -->
		<?php if(in_array('2',$user_access) OR $role_id == '1'){ ?>
		<li class="cls_sideacc"> 
			<span class="cls"> 
				<a href="" class="icon-box"> <!-- <?php echo base_url();?>adminsettings/bulk_store -->
					<i class="newicon-cog"></i><!-- upload2 -->
				</a>
			</span>
			<span class="cls"> 
				<a href="<?php echo base_url();?>adminsettings/addaffiliate" class="icon-box">
					<i class="newicon-plus-circle"></i>
				</a>
			</span>	
			<span class="icon-box">
				<a class="" href="<?php echo base_url();?>adminsettings/affiliates">
					<i class="newicon-store"></i>&nbsp;<span class="fontcls">Retailers</span>
				</a>
			</span>
		</li>
		<?php } ?>

		<!-- Members Menu -->
		<?php if(in_array('3',$user_access) OR $role_id == '1'){ ?>
		<li class="cls_sideacc">
			<span class="cls">
				<a href="<?php echo base_url();?>adminsettings/click_history" class="icon-box">
					<i class=" newicon-clock3"></i>
				</a>
			</span>
			<span class="cls">
				<a href="<?php echo base_url();?>adminsettings/report_export/users" class="icon-box">
					<i class="newicon-enter-down2"></i>
				</a>
			</span>
			<span class="icon-box">
				<a href="<?php echo base_url();?>adminsettings/users" class="">
					<i class="newicon-users2"></i>&nbsp; <span class="fontcls">Members</span>
				</a>
			</span>	
		</li>
		<?php } ?>

		<!-- Cashback Menu -->
		<?php if(in_array('4',$user_access) OR $role_id == '1'){ ?>
		<li class="cls_sideacc">
			<span class="cls">
				<a href="<?php echo base_url();?>adminsettings/report_upload" class="icon-box">
					<i class="newicon-upload2"></i>
				</a>
			</span>
			<span class="cls">
				<a href="<?php echo base_url();?>adminsettings/report_export/cashback" class="icon-box">
					<i class="newicon-enter-down2"></i>
				</a>
			</span>
			<span class="icon-box">
				<a href="<?php echo base_url();?>adminsettings/cashback" class="">
					<i class="newicon-bag"></i>&nbsp;<span class="fontcls">Cashbacks</span> 
				</a>
			</span>	
		</li>
		<?php } ?>

		<!-- Pending Cashback Menu -->
		<?php if(in_array('5',$user_access) OR $role_id == '1'){ ?>
		<li class="cls_sideacc">
			<span class="cls">
				<a href="<?php echo base_url();?>adminsettings/report_update/pending_cashback" class="icon-box">
					<i class="newicon-sync"></i>
				</a>
			</span>
			<span class="cls">	
				<a href="<?php echo base_url();?>networks/update_api/zanox" class="icon-box">
				 	<i class="newicon-site-map"></i>
				</a>
			</span>	
			<span class="icon-box">
				<a href="<?php echo base_url();?>adminsettings/pending_cashback" class="">
					<i class="newicon-bag2"></i><span class="fontcls">Pending Cashbacks</span>
				</a>
			</span>
		</li>
		<?php } ?>

		<!-- Missing Cashback Menu -->
		<?php if(in_array('6',$user_access) OR $role_id == '1'){ ?>
        <li class="cls_sideacc">
	        <span class="cls">
	        	<a href="<?php echo base_url();?>adminsettings/report_update/missing_cashback" class="icon-box">
					<i class="newicon-sync"></i> 
				</a>
			</span>
			<span class="cls">
				<a href="<?php echo base_url();?>adminsettings/report_export/missing_cashback" class="icon-box">
				 	<i class="newicon-enter-down2"></i>
				</a>
			</span>	
			<span class="icon-box">
				<a href="<?php echo base_url();?>adminsettings/missing_cashback" class="">
					<i class="newicon-mailbox-full"></i> <span class="fontcls">Missing Cashbacks</span>
				</a>
			</span>	 
		</li>
		<?php } ?>

		<!-- Withdraw Menu -->
		<?php if(in_array('7',$user_access) OR $role_id == '1'){ ?>
		<li class="cls_sideacc">
			<span class="cls">
				<a href="<?php echo base_url();?>adminsettings/report_update/withdraw" class="icon-box">
					<i class="newicon-sync"></i>
				</a>
			</span>
			<span class="cls">
				<a href="<?php echo base_url();?>adminsettings/report_export/withdraw" class="icon-box">
				 	<i class="newicon-enter-down2"></i>
				</a>
			</span>	
			<span class="icon-box">
				<a href="<?php echo base_url();?>adminsettings/withdraw" class="">
					<i class="newicon-wallet"></i>&nbsp;<span class="fontcls">Withdraw</span>
				</a>
			</span>	
		</li>
		<?php } ?>
       	
        <!-- Payment Menu -->
        <?php if(in_array('8',$user_access) OR $role_id == '1'){ ?>
        <li class="cls_sideacc">
        	<span class="cls">
	        	<a href="<?php echo base_url();?>adminsettings/report_update/payment" class="icon-box">
					<i class="newicon-sync"></i> 
				</a>
			</span>
			<span class="cls">	
				<a href="<?php echo base_url();?>adminsettings/report_export/payment" class="icon-box">
				 	<i class="newicon-enter-down2"></i>
				</a>
			</span>	
			<span class="icon-box">
				<a href="<?php echo base_url();?>adminsettings/transactions" class="">
					<i class="newicon-receipt"></i>&nbsp;<span class="fontcls">Payments</span> 
				</a>
			</span>		
		</li>
		<?php } ?>

		<!-- new code for Marketing Menu and sub menu details 6-10-16 -->
		<?php if(in_array('9',$user_access) OR $role_id == '1'){ ?>
    	<li class="has-sub cls_new_cin cls_doun_hc" <?php 
    		if((strpos($_SERVER['REQUEST_URI'],'adminsettings/layout') == true)       		 || (strpos($_SERVER['REQUEST_URI'],'adminsettings/cashback_exclusive') == true) || 
    		(strpos($_SERVER['REQUEST_URI'],'adminsettings/sales_funnel') == true) 			 || (strpos($_SERVER['REQUEST_URI'],'adminsettings/popup') == true) ||
			(strpos($_SERVER['REQUEST_URI'],'adminsettings/pretty_link') == true)   		 || (strpos($_SERVER['REQUEST_URI'],'adminsettings/ads') == true) ||
			(strpos($_SERVER['REQUEST_URI'],'adminsettings/add_cashback_exclusive') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/edit_cashback_exclusive') == true) ||
			(strpos($_SERVER['REQUEST_URI'],'adminsettings/editads') == true) 				 || (strpos($_SERVER['REQUEST_URI'],'adminsettings/delete_cashback_exclusive') == true))	    	
	    	{      	
				echo 'open'; 
			}
			?> >
			
			<span class="icon-box cls_pad_l cls_dou_icon"> 
				<a href="<?php echo base_url();?>adminsettings/cashback_exclusive" class="">
					<i class="newicon-coin-dollar"></i>
				</a>
			</span> 
		
		
		 	<span class="icon-box cls_dou_icon">
		 		<a href="<?php echo base_url();?>adminsettings/layout" class=" ">
		 			<i class="newicon-sun"></i>
		 		</a>
		 	</span>
		
			<a href="javascript:;" class="cls1 bew_io">
				<i class="newicon-diamond3"></i>&nbsp;<span class="fontcls">Marketing</span>
				<span class="arrow" <?php 
					if((strpos($_SERVER['REQUEST_URI'],'adminsettings/layout') == true)       	     || (strpos($_SERVER['REQUEST_URI'],'adminsettings/cashback_exclusive') == true) || 
		    		(strpos($_SERVER['REQUEST_URI'],'adminsettings/sales_funnel') == true) 		     || (strpos($_SERVER['REQUEST_URI'],'adminsettings/popup') == true) 			   ||
					(strpos($_SERVER['REQUEST_URI'],'adminsettings/pretty_link') == true)   		 || (strpos($_SERVER['REQUEST_URI'],'adminsettings/ads') == true) ||
					(strpos($_SERVER['REQUEST_URI'],'adminsettings/add_cashback_exclusive') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/edit_cashback_exclusive') == true) ||
					(strpos($_SERVER['REQUEST_URI'],'adminsettings/editads') == true) 				 || (strpos($_SERVER['REQUEST_URI'],'adminsettings/delete_cashback_exclusive') == true) ||
					(strpos($_SERVER['REQUEST_URI'],'adminsettings/addbanner') == true)				 || (strpos($_SERVER['REQUEST_URI'],'adminsettings/banners') == true) ||
					(strpos($_SERVER['REQUEST_URI'],'adminsettings/exit_popup') == true)             || (strpos($_SERVER['REQUEST_URI'],'adminsettings/content') == true) || 
					(strpos($_SERVER['REQUEST_URI'],'adminsettings/sales_funnel_settings') == true)  || (strpos($_SERVER['REQUEST_URI'],'adminsettings/first_access_popup') == true)) 
					{
						echo 'open'; 
					}
					?>>
				</span>
			</a>
			<ul class="sub" <?php 
				if((strpos($_SERVER['REQUEST_URI'],'adminsettings/layout') == true)       	     || (strpos($_SERVER['REQUEST_URI'],'adminsettings/cashback_exclusive') == true) || 
		    	(strpos($_SERVER['REQUEST_URI'],'adminsettings/sales_funnel') == true) 		     || (strpos($_SERVER['REQUEST_URI'],'adminsettings/popup') == true) 			   ||
				(strpos($_SERVER['REQUEST_URI'],'adminsettings/pretty_link') == true)   		 || (strpos($_SERVER['REQUEST_URI'],'adminsettings/ads') == true) || 
				(strpos($_SERVER['REQUEST_URI'],'adminsettings/add_cashback_exclusive') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/edit_cashback_exclusive') == true) ||
				(strpos($_SERVER['REQUEST_URI'],'adminsettings/editads') == true)				 || (strpos($_SERVER['REQUEST_URI'],'adminsettings/delete_cashback_exclusive') == true) ||
				(strpos($_SERVER['REQUEST_URI'],'adminsettings/addbanner') == true)				 || (strpos($_SERVER['REQUEST_URI'],'adminsettings/banners') == true) ||
				(strpos($_SERVER['REQUEST_URI'],'adminsettings/exit_popup') == true)			 || (strpos($_SERVER['REQUEST_URI'],'adminsettings/content') == true) ||
				(strpos($_SERVER['REQUEST_URI'],'adminsettings/sales_funnel_settings') == true)  || (strpos($_SERVER['REQUEST_URI'],'adminsettings/first_access_popup') == true))
				{
				 	echo 'style="display:block;"'; 
				}
				?>>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/layout">Layout</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/cashback_exclusive">Cashback Exclusive</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/sales_funnel">Sales Funnel</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/sales_funnel_settings">Sales Funnel Settings</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/popup">Popup</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/pretty_link">Preety Link Redirect</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/exit_popup">Exit Popup</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/first_access_popup">First access Popup</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/ads">Ads</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/banners">Banners</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/content">Content</a></li>
			</ul>
		</li>  
		<?php } ?>
        <!-- end 6-10-16--> 

        <!-- Referrals Menu -->
        <?php if(in_array('10',$user_access) OR $role_id == '1'){ ?>
		<li class="has-sub cls_new_cin cls_doun_hc" <?php 
			if((strpos($_SERVER['REQUEST_URI'],'adminsettings/referrals') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/category') == true)){
			echo 'open'; } ?> 
			>
			<span class="icon-box cls_pad_l cls_dou_icon">
				<a href="<?php echo base_url();?>adminsettings/pending_referral" class="">
					<i class="newicon-notification"></i>
				</a>
			</span>
			<span class="icon-box cls_dou_icon">	
				<a href="<?php echo base_url();?>adminsettings/referrals" class="">
				 	<i class="newicon-file-preview"></i>
				</a>
			</span>	

			<a href="javascript:;" class="cls1 bew_io">
				<i class="newicon-group-work"></i>&nbsp;<span class="fontcls">Referrals</span>
				<span class="arrow <?php 
				if((strpos($_SERVER['REQUEST_URI'],'adminsettings/referrals') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/pending_referral') == true)|| (strpos($_SERVER['REQUEST_URI'],'adminsettings/category') == true)){
				echo 'open'; } ?>"></span>
			</a>
			<ul class="sub" <?php 
				if((strpos($_SERVER['REQUEST_URI'],'adminsettings/referrals') == true)|| (strpos($_SERVER['REQUEST_URI'],'adminsettings/pending_referral') == true)|| (strpos($_SERVER['REQUEST_URI'],'adminsettings/category') == true)){
				echo 'style="display:block;"'; } ?>>
				<li><a class="" href="<?php echo base_url();?>adminsettings/referrals">View All</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/category/one">Category ONE(Standard)</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/category/two">Category TWO</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/category/three">Category THREE</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/category/four">Category FOUR</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/category/five">Category FIVE</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/category/six">Category SIX</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/category/seven">Category SEVEN</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/category/eight">Category EIGHT</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/category/nine">Category NINE</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/category/ten">Category TEN</a></li>
           		<!-- <li><a class="" href="<?php echo base_url();?>adminsettings/pending_referral">Pending Referrals</a></li> -->
			</ul>
		</li>
		<?php } ?>

		<!-- Premium Coupons Menu -->
		<?php if(in_array('11',$user_access) OR $role_id == '1'){ ?>
		<li class="has-sub cls_new_cin cls_doun_hc" <?php 
			if((strpos($_SERVER['REQUEST_URI'],'adminsettings/add_shoppingcoupon') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/shoppingcoupons') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/edit_shoppingcoupon') == true) ||(strpos($_SERVER['REQUEST_URI'],'adminsettings/upload_coupons') == true)){
			echo 'open'; } ?>>
			
			<span class="icon-box cls_pad_l cls_dou_icon">
			<a href="<?php echo base_url();?>adminsettings/upload_coupons" class="">
				<i class="newicon-upload2"></i> 
			</a>
			</span>
			<span class="icon-box cls_dou_icon">
			<a href="<?php echo base_url();?>adminsettings/add_shoppingcoupon" class="">
			 	<i class="newicon-plus-circle"></i>
			</a>
			</span>

			<a href="javascript:;" class="cls1 bew_io">
				<i class="newicon-tags"></i>&nbsp;<span class="fontcls">Premium&nbsp;coupon</span>
				<span class="arrow <?php 
				if((strpos($_SERVER['REQUEST_URI'],'adminsettings/add_shoppingcoupon') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/shoppingcoupons') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/edit_shoppingcoupon') == true) ||(strpos($_SERVER['REQUEST_URI'],'adminsettings/upload_coupons') == true)){
				echo 'open'; } ?>"></span>
			</a>
			<ul class="sub" <?php 
				if((strpos($_SERVER['REQUEST_URI'],'adminsettings/add_shoppingcoupon') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/shoppingcoupons') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/edit_shoppingcoupon') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/expired_coupons') == true) ||(strpos($_SERVER['REQUEST_URI'],'adminsettings/upload_coupons') == true)){
				echo 'style="display:block;"'; } ?>>
				<?php //if(in_array('42',$sub_access) OR $role_id == '1'){ ?>
				<li><a class="" href="<?php echo base_url();?>adminsettings/upload_coupons">Upload Coupons</a></li>
           		<li><a class="" href="<?php echo base_url();?>adminsettings/add_shoppingcoupon">Add New</a></li>
           		<?php //} ?><?php //if(in_array('43',$sub_access) OR $role_id == '1'){ ?>
                <li><a class="" href="<?php echo base_url();?>adminsettings/shoppingcoupons">Active Coupons</a></li>
                <?php //} ?><?php //if(in_array('44',$sub_access) OR $role_id == '1'){ ?>
                <li><a class="" href="<?php echo base_url();?>adminsettings/expired_coupons">Expired Coupons</a></li>
                <?php //} ?><?php //if(in_array('45',$sub_access) OR $role_id == '1'){ ?>
                <li><a class="" href="<?php echo base_url();?>adminsettings/orders">Orders</a></li>
                <?php //} ?><?php //if(in_array('46',$sub_access) OR $role_id == '1'){ ?>
                <li><a class="" href="<?php echo base_url();?>adminsettings/reviews">Reviews</a></li>
                <?php //} ?>
			</ul>
		</li>
		<?php } ?>

		<!-- CMS Menu -->
		<?php if(in_array('12',$user_access) OR $role_id == '1'){ ?>	 
		<li class="cls_sideacc <?php 
			if((strpos($_SERVER['REQUEST_URI'],'adminsettings/addcms') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/cms') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/editcms') == true)){
			echo 'open'; } ?>">

			<span class="cls">
				<!-- <a href="" class="icon-box">
					<i class="newicon-earth"></i> 
				</a> -->
				<a href="<?php echo base_url();?>adminsettings/faqs" class="icon-box">
				 	<i class="newicon-bubble-question"></i>
				</a>

			</span>
			<span class="cls">
				<a href="<?php echo base_url();?>adminsettings/addcms" class="icon-box">
				 	<i class="newicon-plus-circle"></i>
				</a>
			</span>	

			<a href="<?php echo base_url();?>adminsettings/cms" class="cls" style="background: transparent none repeat scroll 0% 0% ! important;">
				<i class="newicon-files" style="padding:0 5px;"></i>&nbsp;Cms Pages
			</a>
		</li>
		<?php } ?>

		<!-- Dasboard Menu-->
		<?php if(in_array('13',$user_access) OR $role_id == '1'){ ?>
		<li class="has-sub cls_new_cin cls_doun_hc" <?php 
			if((strpos($_SERVER['REQUEST_URI'],'adminsettings/dashboard') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/settings') == true) || 
			   (strpos($_SERVER['REQUEST_URI'],'adminsettings/change_password') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/user_information') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/sub_admin') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/payment_settings') == true) ||
			   (strpos($_SERVER['REQUEST_URI'],'adminsettings/cashback_exclusive') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/amazon_s3_settings') == true) || (strpos($_SERVER['REQUEST_URI'],'networks/upload_apireport') == true) || (strpos($_SERVER['REQUEST_URI'],'networks/upload_apicoupons') == true) ||
			   (strpos($_SERVER['REQUEST_URI'],'adminsettings/manual_credit') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/categories') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/editcategory') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/premium_categories') == true)||
			   (strpos($_SERVER['REQUEST_URI'],'adminsettings/edit_premium_categories') == true))
			{
				echo 'open'; 
			}
			?>>
			<span class="icon-box cls_pad_l cls_dou_icon">
				<a href="<?php echo base_url();?>adminsettings/manual_credit" class="">
					<i class="newicon-cash-dollar"></i>
				</a>
			</span>
			<span class="icon-box cls_dou_icon">	
				<a href="<?php echo base_url();?>adminsettings/settings" class="">
				 	<i class="newicon-cog"></i>
				</a>
			</span>
			<a href="javascript:;" class="cls1 bew_io">
				<i class="icon-dashboard"></i>&nbsp;<span class="fontcls">Dashboard</span>
				<span class="arrow <?php 
					if( /*(strpos($_SERVER['REQUEST_URI'],'adminsettings/dashboard') == true)*/ /*|| (strpos($_SERVER['REQUEST_URI'],'adminsettings/ads') == true) 			 || (strpos($_SERVER['REQUEST_URI'],'adminsettings/editads') == true) 			 || */
					    (strpos($_SERVER['REQUEST_URI'],'adminsettings/settings')  == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/change_password') == true)  || (strpos($_SERVER['REQUEST_URI'],'adminsettings/user_information') == true)   ||
					    (strpos($_SERVER['REQUEST_URI'],'adminsettings/sub_admin') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/payment_settings') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/cashback_exclusive') == true) ||
					    (strpos($_SERVER['REQUEST_URI'],'adminsettings/add_cashback_exclusive') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/edit_cashback_exclusive') == true) ||(strpos($_SERVER['REQUEST_URI'],'adminsettings/delete_cashback_exclusive') == true) ||
					    (strpos($_SERVER['REQUEST_URI'],'networks/upload_apireport') == true) || (strpos($_SERVER['REQUEST_URI'],'networks/upload_apicoupons') == true) ||
					    (strpos($_SERVER['REQUEST_URI'],'adminsettings/manual_credit') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/categories') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/editcategory') == true) ||
					    (strpos($_SERVER['REQUEST_URI'],'adminsettings/premium_categories') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/edit_premium_categories') == true)|| (strpos($_SERVER['REQUEST_URI'],'adminsettings/addcategory') == true) ||
				    	(strpos($_SERVER['REQUEST_URI'],'adminsettings/stores') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/bank_details') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/add_bankdetails') == true)||
				    	(strpos($_SERVER['REQUEST_URI'],'adminsettings/edit_bankdetails') == true))
					    {
							echo 'open'; 
						}
					?>">
				</span>
			</a>
			
			<ul class="sub" <?php 
				if(/*(strpos($_SERVER['REQUEST_URI'],'adminsettings/dashboard') == true)*/ /*|| (strpos($_SERVER['REQUEST_URI'],'adminsettings/ads') == true)			 || (strpos($_SERVER['REQUEST_URI'],'adminsettings/editads') == true) || */
				    (strpos($_SERVER['REQUEST_URI'],'adminsettings/settings')  == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/change_password') == true)  || (strpos($_SERVER['REQUEST_URI'],'adminsettings/user_information') == true) ||
				    (strpos($_SERVER['REQUEST_URI'],'adminsettings/sub_admin') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/payment_settings') == true)  ||
				    (strpos($_SERVER['REQUEST_URI'],'adminsettings/amazon_s3_settings') == true) || (strpos($_SERVER['REQUEST_URI'],'networks/upload_apireport') == true) || (strpos($_SERVER['REQUEST_URI'],'networks/upload_apicoupons') == true) ||
				    (strpos($_SERVER['REQUEST_URI'],'adminsettings/manual_credit') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/categories') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/editcategory') == true) || 
				    (strpos($_SERVER['REQUEST_URI'],'adminsettings/premium_categories') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/edit_premium_categories') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/addcategory') == true) ||
				    (strpos($_SERVER['REQUEST_URI'],'adminsettings/stores') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/bank_details') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/add_bankdetails') == true)||
				    (strpos($_SERVER['REQUEST_URI'],'adminsettings/edit_bankdetails') == true))
					{
						echo 'style="display:block;"'; 
					}
					?>>
				<li><a class="" href="<?php echo base_url();?>adminsettings/dashboard">Dashboard</a></li>
				<li><a class="" href="<?php echo base_url();?>adminsettings/settings">Settings</a></li>
				<li><a class="" href="<?php echo base_url();?>adminsettings/categories">Categories Normal</a></li>
				<li><a class="" href="<?php echo base_url();?>adminsettings/premium_categories">Categories Premium</a></li>
	            <li><a class="" href="<?php echo base_url();?>networks/upload_apireport">Aff ntwk Upload Reports</a></li>
				<!--
				<li><a class="" href="<?php echo base_url();?>networks/upload_apicoupons">Aff ntwk Upload Coupons</a></li>
				-->	
				<li><a class="" href="<?php echo base_url();?>adminsettings/manual_credit"> Manual Credit</a></li>
		        <li><a class="" href="<?php echo base_url();?>adminsettings/user_information">User Information</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/bank_details">Bank Details</a></li>

                <?php 
                if($role_id==1)
                { 
                	?>
                	<li><a class="" href="<?php echo base_url();?>adminsettings/sub_admin">Sub Admin</a></li>
                	<?php
                }
                ?>	
                <li><a class="" href="<?php echo base_url();?>adminsettings/amazon_s3_settings">Amazon S3 Settings</a></li>
		        <li><a class="" href="<?php echo base_url();?>adminsettings/payment_settings">Payment Settings</a></li>
		        <li><a class="" href="<?php echo base_url();?>adminsettings/change_password">Change Password</a></li>
			</ul>
		</li>
		<?php } ?>

		<!-- Email Templates Menu -->
		<?php if(in_array('14',$user_access) OR $role_id == '1'){ ?>
		<li class="has-sub cls_new_cin cls_doun_hc" <?php 
			if(strpos($_SERVER['REQUEST_URI'],'adminsettings/email_template') == true){
			echo 'open'; } ?>>
			<span class="icon-box cls_pad_l cls_dou_icon">
				<a href="<?php echo base_url();?>adminsettings/subscribers" class="">
					<i class="newicon-envelope"></i> 
				</a>
			</span>
			<span class="icon-box cls_dou_icon">
				<a href="<?php echo base_url();?>adminsettings/report_export/subscribers" class="">
				 	<i class="newicon-enter-down2"></i>
				</a>
			</span>	

			<a href="javascript:;" class="cls1 bew_io">
				<i class="newicon-inbox2"></i> <span class="fontcls">Email Templates</span>
				<span class="arrow <?php 
				if((strpos($_SERVER['REQUEST_URI'],'adminsettings/email_template') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/subscribers') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/report_export/subscribers') == true)) {
				echo 'open'; } ?>"></span>
			</a>
			<ul class="sub" <?php 
				if((strpos($_SERVER['REQUEST_URI'],'adminsettings/email_template') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/subscribers') == true) || (strpos($_SERVER['REQUEST_URI'],'adminsettings/report_export/subscribers') == true)) {
				echo 'style="display:block;"'; } ?>>
				
           		<li><a class="" href="<?php echo base_url();?>adminsettings/email_template/1">User Registration</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/2">Forget Password</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/4">Pending Referral Mail</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/5">Refer Friends Mail</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/7">Contato Mail</a></li>
				<li><a class="" href="<?php echo base_url();?>adminsettings/email_template/8">Approve Cashback Mail</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/9">Approve Referral Mail</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/10">Pending Cashback Mail</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/11">Cashback canceled Mail</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/12">Referral canceled Mail</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/13">Missing Cashback Request</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/14">Missing CashB Status</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/17">Withdraw Request Mail</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/15">Withdraw status</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/16">Available to withdraw</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/18">Social Signup</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/19">Canceled referral cashback</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/20">Pending referral cashback</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/21">Referral category congrats</a></li>
                <li><a class="" href="<?php echo base_url();?>adminsettings/email_template/22">Campaign Notification Mail</a></li>
				<li><a class="" href="<?php echo base_url();?>adminsettings/compose_newsletter">Send Newsletter</a></li>
			</ul>
		</li>
		<?php } ?>
			
		<!-- Faq menu -->
		<!-- <li class="cls_sideacc <?php 
			if((strpos($_SERVER['REQUEST_URI'],'adminsettings/addfaqs') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/faqs') == true)||(strpos($_SERVER['REQUEST_URI'],'adminsettings/editfaq') == true)){
			echo 'open'; } ?>">
			
			<span class="cls">
				<a href="<?php echo base_url();?>perguntas-frequentes" class="icon-box">
					<i class="newicon-flag2"></i>
				</a>
			</span>
			<span class="cls">
				<a href="<?php echo base_url();?>adminsettings/addfaqs" class="icon-box">
				 	<i class="newicon-plus-circle"></i>
				</a>
			</span>
			<a href="<?php echo base_url();?>adminsettings/faqs" class="cls" style="background: transparent none repeat scroll 0% 0% ! important;">
				<i class="newicon-bubble-question" style="padding:0 5px;"></i> FAQ
			</a>
		</li> -->
	</ul>
	<!-- END SIDEBAR MENU -->
</div>
