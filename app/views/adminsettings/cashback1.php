<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Cashback | <?php echo $admin_details->site_name; ?> Admin</title>
  <?php $this->load->view('adminsettings/script'); ?>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />

  <style>
    div.selector, div.selector span, div.checker span, div.radio span, div.uploader, div.uploader span.action, div.button, div.button span
    {
      background-image:none
    }

    /*New style for datatable related css 17-12-16*/
    .col-xs-6 {width: 47% !important; float: left;min-height:1px;padding-left: 12px;padding-right: 12px;position: relative;}
    select.input-sm { width:60px !important;}
    .row {margin-left: -12px;margin-right: -12px;}
    .pagination {border-radius: 4px;margin: 20px 0;padding-left: 0;}
    /*End 17-12-16*/
  </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN THEME CUSTOMIZER-->
            <!--<div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
            <span class="settings">
            <span class="text">Theme:</span>
            <span class="colors">
            <span class="color-default" data-style="default"></span>
            <span class="color-gray" data-style="gray"></span>
            <span class="color-purple" data-style="purple"></span>
            <span class="color-navy-blue" data-style="navy-blue"></span>
            </span>
            </span>
            </div>-->
            <!-- END THEME CUSTOMIZER-->
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
           <!--  <h3 class="page-title">
            Cashback
            </h3> -->
           <!--  <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
						    <span class="divider">&nbsp;</span>
              </li>
              <li><?php echo anchor('adminsettings/cashback','Cashback'); ?>
							  <span class="divider-last">&nbsp;</span>
					    </li>
            </ul> -->
            <span style="float:left;margin-top: 15px;"> 
              <a href="<?php echo base_url();?>adminsettings/reports" class="btn btn-success">Reports</a> &nbsp;
              <!-- <a href="<?php echo base_url();?>adminsettings/stores"   class="btn btn-success">Stores</a> -->
            </span> 
            <!-- END PAGE TITLE & BREADCRUMB-->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <!-- BEGIN ADVANCED TABLE widget-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN EXAMPLE TABLE widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i> Cashback</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                </span>
              </div>
              <div class="widget-body">
						    <?php 
					      $error = $this->session->flashdata('error');
					      if($error!="")
                {
      						echo '<div class="alert alert-error">
      						<button data-dismiss="alert" class="close">x</button>
      						<strong>Error! </strong>'.$error.'</div>';
      					} 
      					$success = $this->session->flashdata('success');
      					if($success!="") 
                {
      						echo '<div class="alert alert-success">
      						<button data-dismiss="alert" class="close">x</button>
      					  <strong>Success! </strong>'.$success.'</div>';			
      					}
                ?>
						    <form id="form2" action="" method="post" name="form2">
                  <table class="table table-striped table-bordered" id="sample_teste">
                    <thead>
                      <tr>
                        <th>#</th>
									      <th><center><input type="checkbox" id="check_b" class="check_b" onchange="checkAll(this)" name="chk[]" /></center></th><!-- seetha-->
                        <th><center>User Id</center></th>
                        <th><center>User Name</center></th>
                        <th><center>User Email</center></th>
                        <th><center>Store</center></th>
                        <th><center>Cashback</center></th>
                        <th><center>Transc Amt</center></th>
                        
                        <!-- <th><center>Voucher Name</center></th> -->
                        <th><center>Transaction Date</center></th>
                        <th><center>Status</center></th>
                        <th><center>Edit</center></th>

                        <th><center>Report Update ID</center></th>
                        <th><center>Status Date Updated</center></th>
                        <th><center>Referral</center></th>
                        <th><center>txn_id</center></th>
                        <th><center>new_txn_id</center></th>
                        <th><center>Plataform</center></th>

                        <th><center>Delete</center></th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
      						<input type="hidden" name="hidd" value="hidd"><!-- seetha-->
      						<input id="GoUpdate" class="btn btn-warning" type="submit" value="Delete Cashback" name="GoUpdate">
      					</form>
              </div>
            </div>
            <!-- END EXAMPLE TABLE widget-->
          </div>
        </div>
        <!-- END ADVANCED TABLE widget-->
        <!-- END PAGE CONTENT-->
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
  <!-- END FOOTER -->
  <!-- BEGIN JAVASCRIPTS -->
  <!-- Load javascripts at bottom, this will reduce page load time -->
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>   
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->   
  <!--<script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>-->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
  
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>

  <script>
      jQuery(document).ready(function() {
         // initiate layout and plugins
         App.init();
      });
   </script>
   <script type="text/javascript">
function confirmDelete(m)  // Confirm before delete cashback..
{
	if(!confirm(m))
	{
		return false;
	}
	else
	{
		return true;
	}
}
</script>
<!-- seetha-->
<script>
	  $(document).ready(function() {
		$(".check_b").attr("style", "opacity: 1;");
	  });
	function checkAll(ele) {    //multiple delete cashback
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }
</script>

<script type="text/javascript">

 
$(document).ready(function() {
$('#sample_teste').DataTable( {
"processing": true,
"serverSide": true,
"ajax": {
"url": "<?php echo site_url('adminsettings/newcashback')?>",
"data": {
//"totalrecords": "<?php echo $iTotal; ?>"
}
}

} );
} );
</script>
</body>
<!-- END BODY -->
</html>
<style>
.dataTables_processing
{
  display:none !important;
}
.pagination {
    padding-left: 0;
    margin: 0;
    border-radius: 4px
}

.pager li,
.pagination>li {
    display: inline
}

.pagination>li>a,
.pagination>li>span {
    position: relative;
    float: left;
    padding: 6px 12px;
    line-height: 1.42857143;
    text-decoration: none;
    color: #337ab7;
    background-color: #fff;
    border: 1px solid #ddd;
    margin-left: -1px
}

.pagination>li:first-child>a,
.pagination>li:first-child>span {
    margin-left: 0;
    border-bottom-left-radius: 4px;
    border-top-left-radius: 4px
}

.pagination>li:last-child>a,
.pagination>li:last-child>span {
    border-bottom-right-radius: 4px;
    border-top-right-radius: 4px
}

.pagination>li>a:focus,
.pagination>li>a:hover,
.pagination>li>span:focus,
.pagination>li>span:hover {
    z-index: 2;
    color: #23527c;
    background-color: #eee;
    border-color: #ddd
}

.pagination>.active>a,
.pagination>.active>a:focus,
.pagination>.active>a:hover,
.pagination>.active>span,
.pagination>.active>span:focus,
.pagination>.active>span:hover {
    z-index: 3;
    color: #999;
    background-color: #f5f5f5;
    //border-color: #337ab7;
    cursor: default
}

.pagination>.disabled>a,
.pagination>.disabled>a:focus,
.pagination>.disabled>a:hover,
.pagination>.disabled>span,
.pagination>.disabled>span:focus,
.pagination>.disabled>span:hover {
    color: #777;
    background-color: #fff;
    border-color: #ddd;
    cursor: not-allowed
}

.pagination-lg>li>a,
.pagination-lg>li>span {
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333
}

.pagination-lg>li:first-child>a,
.pagination-lg>li:first-child>span {
    border-bottom-left-radius: 6px;
    border-top-left-radius: 6px
}

.pagination-lg>li:last-child>a,
.pagination-lg>li:last-child>span {
    border-bottom-right-radius: 6px;
    border-top-right-radius: 6px
}

.pagination-sm>li>a,
.pagination-sm>li>span {
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5
}

.badge,
.close,
.label {
    line-height: 1
}

.pagination-sm>li:first-child>a,
.pagination-sm>li:first-child>span {
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px
}

.pagination-sm>li:last-child>a,
.pagination-sm>li:last-child>span {
    border-bottom-right-radius: 3px;
    border-top-right-radius: 3px
}

.pager {
    padding-left: 0;
    margin: 20px 0;
    list-style: none;
    text-align: center
}

.pager li>a,
.pager li>span {
    display: inline-block;
    padding: 5px 14px;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 15px
}

.pager li>a:focus,
.pager li>a:hover {
    text-decoration: none;
    background-color: #eee
}

.pager .next>a,
.pager .next>span {
    float: right
}

.pager .previous>a,
.pager .previous>span {
    float: left
}

.pager .disabled>a,
.pager .disabled>a:focus,
.pager .disabled>a:hover,
.pager .disabled>span {
    color: #777;
    background-color: #fff;
    cursor: not-allowed
}
</style>