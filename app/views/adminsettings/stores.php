<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
  <head>
	<meta charset="utf-8" />
	<?php $admin_details = $this->admin_model->get_admindetails(); ?>
    <title>Stores | <?php echo $admin_details->site_name; ?> Admin</title>
	  <?php $this->load->view('adminsettings/script'); ?>
    <style>
      .fileUpload 
      {
        position: relative;
        overflow: hidden;
        margin: 10px;
      }
      .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
      }
    </style>
    <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />
  </head>
  <!-- END HEAD -->
  <!-- BEGIN BODY -->
  <body class="fixed-top">
    <!-- BEGIN HEADER -->
    <?php $this->load->view('adminsettings/header'); ?>
    <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->
    <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
          <div class="row-fluid">
            <div class="span12">    
              <!-- <h3 class="page-title">Category</h3>
                <ul class="breadcrumb">
                  <li><?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?><span class="divider">&nbsp;</span></li>
                  <li>
          				  <?php echo anchor('adminsettings/stores','Stores'); ?>
          					<span class="divider">&nbsp;</span>
                  </li>
					        <li>
						        Store Details<span class="divider-last">&nbsp;</span>
                  </li>
                </ul> -->
            </div>
          </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
          <div class="row-fluid">
            <div class="span12">
              <!-- BEGIN SAMPLE FORM widget-->
              <div class="widget">
                <div class="widget-title">
                  <h4><i class="icon-file"></i> Store</h4>
                  <span class="tools">
                    <a href="javascript:;" class="icon-chevron-down"></a>
                    <!--<a href="javascript:;" class="icon-remove"></a>-->
                  </span>
                </div> <br>
					      <span>
                  <span class="required_field"> &nbsp;&nbsp;&nbsp;*</span> marked fields are mandatory.
                </span><br>
        					

                <?php
        		  	//if($action=="new")
                //{
        				?>
                  <div class="widget-body form">
          					<?php 
          					$error = $this->session->flashdata('error');
          					if($error!="") {
          					echo '<div class="alert alert-error">
          					<button data-dismiss="alert" class="close">x</button>
          					<strong>Error! </strong>'.$error.'</div>';
          					}
                    ?>
        					  <?php
            				$success = $this->session->flashdata('success');
            				if($success!="") {
            						echo '<div class="alert alert-success">
            								<button data-dismiss="alert" class="close">x</button>
            								<strong>Success! </strong>'.$success.'</div>';			
            				} 
            				$attribute = array('role'=>'form','name'=>'store','method'=>'post','id'=>'change_pwd','class'=>'form-horizontal','enctype'=>'multipart/form-data','onsubmit'=>"return Validate(this);"); 
            				echo form_open('adminsettings/stores',$attribute);
            				?>
                           
                      <div class="control-group">
                        <label class="control-label">Category Name <span class="required_field">*</span></label>
                        <div class="controls">
                        <!--<input type="text" name="offer_name" id="offer_name" required class="span6">-->
                          <?php
                          $aff_list  = $this->admin_model->categories();
                          ?>
                          <select id="category_id" class="span6" name="category_name" onchange="runcheck();">
                            <?php
                            foreach($aff_list as $stores)
                            {
                            ?>
                              <option value="<?php echo $stores->category_id;?>"><?php echo $stores->category_name;?></option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                                 
                      <div class="control-group">
                        <label class="control-label">Do you want to show related stores <span class="required_field">*</span></label>
                        <div class="controls">
                          <select id="status" class="span6" required="" name="status">
                            <option value="active">Active</option>
                            <option value="inactive">In Active</option>
                          </select>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Number of stores show in this area <span class="required_field">*</span></label>
                        <div class="controls">
                          <select id="counts" class="span6" required="" name="counts">
                            <?php for($i=1;$i<=20;$i++)
                            {
                            ?>
                              <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Choose the stores to show in this area <span class="required_field">*</span></label>    
                        <div class="controls">
                          <label id="newnew"><h5></h5></label>
                        </div>
                      </div>                            
                      <div class="form-actions">
                        <input type="submit" name="save" value="Submit" class="btn btn-success">
                      </div>
						        <?php
                    echo form_close();
                    ?>
                        <!--</form>-->
                        <!-- END FORM-->
                  </div>
					      <?php
                //}
                ?>
              </div>
                  <!-- END SAMPLE FORM widget-->
            </div>
          </div>
        </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
    </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
    <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   
   
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
   
   <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/css/jquery-checktree.css" rel="stylesheet" type="text/css">

   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
   </script>

   <!-- END JAVASCRIPTS -->   
</body>
</html>
<!-- END BODY -->
<script type="text/javascript">

function onSubmit() 
{ 
  var fields = $("input[name='categorys_list[]']").serializeArray(); 
  if (fields.length == 0) 
  { 
    alert('Please select atleast one store'); 
    // cancel submit
    return false;
  } 
  else 
  { 
    if(fields.length == 1)
    {
      alert(fields.length + " store selected");
    }
    else
    {
      alert(fields.length + " stores selected");
    } 
     
  }
}

// register event on form, not submit button
$('#change_pwd').submit(onSubmit)

function runcheck()
{

  var categoryid = $('#category_id').val();
   
    $.ajax({
      type: 'POST',
      url: '<?php echo base_url();?>adminsettings/cat_details',
      data:{'category_id':categoryid},
       success:function(data){
        
        if(data)
        {
            $("#newnew").html(data);
             
        }
        else
        {
                
          $("#newnew").html('category is not avaliable.');
          
        }
      }
    });
  return false;
}  
</script> 
