<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php
  $admin_details   = $this->admin_model->get_admindetails();
  ?>
  <title>Admin Settings | <?php echo $admin_details->site_name; ?> Admin</title>
  <?php $this->load->view('adminsettings/script'); ?>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
          <div class="span12">
            <!-- <h3 class="page-title">
            Popup Settings
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
                <span class="divider">&nbsp;</span>
              </li>
              <li>Marketing<span class="divider">&nbsp;</span></li>
              <li>
                <?php echo anchor('adminsettings/popup','Sales Funnel'); ?>
                <span class="divider-last">&nbsp;</span>
              </li>
            </ul> -->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN SAMPLE FORM widget-->
            <div class="widget">
                  <div class="widget-title">
                    <h4><i class="icon-cog"></i> Popup Settings</h4>
                    <span class="tools">
                      <a href="javascript:;" class="icon-chevron-down"></a>
                      <!--<a href="javascript:;" class="icon-remove"></a>-->
                    </span>
                  </div><br>
                  <span>
                    <span class="required_field"> &nbsp;&nbsp;&nbsp;*</span> marked fields are mandatory.
                  </span>
                  <br>
                  <div class="widget-body form">
                    <?php 
                    $error = $this->session->flashdata('error');
                    if($error!="")
                    {
                      echo '<div class="alert alert-error">
                      <button data-dismiss="alert" class="close">x</button>
                      <strong>Error! </strong>'.$error.'</div>';
                    }
                    $success = $this->session->flashdata('success');
                    if($success!="") 
                    {
                      echo '<div class="alert alert-success">
                      <button data-dismiss="alert" class="close">x</button>
                      <strong>Success! </strong>'.$success.'</div>';      
                    }
                    ?>
                    <!-- BEGIN FORM-->
                    <?php
                      $attribute = array('role'=>'form','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data','onSubmit' =>'return validation();' ); 
                      echo form_open('adminsettings/popupupdate',$attribute);
                    ?>
                      
                    

                      <?php 
                      /*Popup For not logged users details*/
                      $popup_details1 = $this->db->query("select * from popup_details where popup_id=1")->result();
                      foreach($popup_details1 as $popupdetails1)
                      {
                        ?>
                        <br>
                        <div class="notloguser detail_banners">
                          <h4><b>Popup for not logged user settings  </b></h4><br>
                          <div class="control-group">
                            <div class="controls">
                              <select  name="not_log_usr_status" id="not_log_usr_status" required class="span6">
                                <option value="0" <?php if($popupdetails1->popup_status=='0'){ echo 'selected="selected"'; }?>>Disabled</option>
                                <option value="1" <?php if($popupdetails1->popup_status=='1'){ echo 'selected="selected"'; }?>>Enabled</option>
                                <!-- <option value="2" <?php if($popupdetails1->popup_status=='2'){ echo 'selected="selected"'; }?>>Standard popup Settings</option>
                                <option value="3" <?php if($popupdetails1->popup_status=='3'){ echo 'selected="selected"'; }?>>Popup funnel Settings</option> -->
                              </select>
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">No of sessions to show the popup to each user</label>
                            <div class="controls">
                              <input type="text" class="span6 sessiontiming" name="sessiontiming" id="sessiontiming" value="<?php echo $popupdetails1->session_count;?>" /><br>
                            </div>
                          </div>

                          <div class="control-group">
                             <div class="controls">
                                <input type="checkbox" name="notlogusr_status" class="notlogusr_status" id="notlogusr_status" value="<?php if($popupdetails1->dont_show_status == 1) { echo 1; }else { echo 0;}?>" <?php if($popupdetails1->dont_show_status=="1"){ echo 'checked="checked"';} ?>/><div style="margin-left: 20px; margin-top: -18px; font-size:14px;">Do not show popup on homepage</div>
                            </div>
                          </div>

                          <div class="control-group">
                            <label class="control-label">Reset Counting</label>
                            <div class="controls">
                              <input type="button" class="btn btn-success notlog_usr_reset" name="notlog_usr_reset" id="notlog_usr_reset" onclick="return confirmDelete(this,'','');" value="YES" />&nbsp;<span class="success"></span><br>
                              
                            </div> 
                          </div>
                          <div class="control-group">
                            <label class="control-label">Popup Content</label>
                            <div class="controls">
                              <textarea class="span6 ckeditor" name="not_log_popupcontent" id="not_log_popupcontent" ><?php echo $popupdetails1->popup_content;?></textarea>
                            </div>
                          </div>
                          <input type="hidden" class="span6 popup_id1" name="popup_id1" id="popup_id1" value="<?php echo $popupdetails1->popup_id;?>" />
                        </div><br>
                        <?php
                      }
                      /*End*/

                      /*Popup for Logged users 15-11-16*/
                      $popup_details11 = $this->db->query("select `popup_log_users_status` from admin where admin_id=1")->row();
                      ?>
                      <h4><b>Popup for logged users settings  </b></h4><br>
                          <div class="control-group">
                            <div class="controls">
                              <select  name="log_usr_status" id="log_usr_status" required class="span6">
                                <option value="0" <?php if($popup_details11->popup_log_users_status=='0'){ echo 'selected="selected"'; }?>>Disabled</option>
                                <!-- <option value="1" <?php if($popup_details11->popup_log_users_status=='1'){ echo 'selected="selected"'; }?>>Enabled</option> -->
                                <option value="1" <?php if($popup_details11->popup_log_users_status=='1'){ echo 'selected="selected"'; }?>>Enabled, Standard popup Settings</option>
                                <option value="2" <?php if($popup_details11->popup_log_users_status=='2'){ echo 'selected="selected"'; }?>>Enabled, Popup funnel Settings</option>
                              </select>
                            </div>
                          </div>
                      <?php 
                      /*End 15-11-16*/



                      /*Popup For Logged users Not use bonus benefit and havent logged in using Android App*/
                      $popup_details2 = $this->db->query("select * from popup_details where popup_id=2")->result();
                      foreach($popup_details2 as $popupdetails2)
                      {
                        ?>
                        <br>
                        <div class="log_notusebonus_notuseapp detail_banners">
                          
                          <h4><b>Popup Funnel) For logged users that havent used "Unic Bonus" Feature And Havent Logged in Uisng Android App  </b></h4><br>
                          <div class="control-group">
                            <div class="controls">
                              <select  name="log_notusebonus_notuseapp_status" id="log_notusebonus_notuseapp_status" required class="span6">
                                <option value="0" <?php if($popupdetails2->popup_status=='0'){ echo 'selected="selected"'; }?>>Disabled</option>
                                <option value="1" <?php if($popupdetails2->popup_status=='1'){ echo 'selected="selected"'; }?>>Enabled</option>
                                <!-- <option value="2" <?php if($popupdetails2->popup_status=='2'){ echo 'selected="selected"'; }?>>Standard popup Settings</option>
                                <option value="3" <?php if($popupdetails2->popup_status=='3'){ echo 'selected="selected"'; }?>>Popup funnel Settings</option> -->
                              </select>
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">No of sessions to show the popup to each user</label>
                            <div class="controls">
                              <input type="text" class="span6 log_notusebonus_notuseapp_session" name="log_notusebonus_notuseapp_session" id="log_notusebonus_notuseapp_session" value="<?php echo $popupdetails2->session_count;?>" /><br>
                            </div>
                          </div>
                          
                          <div class="control-group">
                             <div class="controls">
                                <input type="checkbox" name="logusr_notusebonus_notuseapp_status" class="logusr_notusebonus_notuseapp_status" id="logusr_notusebonus_notuseapp_status" value="<?php if($popupdetails2->dont_show_status == 1) { echo 1; }else { echo 0;}?>" <?php if($popupdetails2->dont_show_status=="1"){ echo 'checked="checked"';} ?>/><div style="margin-left: 20px; margin-top: -18px; font-size:14px;">Do not show popup on homepage</div>
                            </div>
                          </div>

                          <div class="control-group">
                            <label class="control-label">Reset Counting</label>
                            <div class="controls">
                              <input type="button" class="btn btn-success log_notusebonus_notuseapp_reset" name="log_notusebonus_notuseapp_reset" id="log_notusebonus_notuseapp_reset"  onclick="return confirmDelete(this,0,0);" value="YES" />&nbsp;<span class="success"></span><br>
                            </div>
                          </div>

                          <div class="control-group">
                            <label class="control-label">Popup Content</label>
                            <div class="controls">
                              <textarea class="span6 ckeditor" name="log_notusebonus_notuseapp_content" id="log_notusebonus_notuseapp_content" ><?php echo $popupdetails2->popup_content;?></textarea>
                            </div>
                          </div>
                          <input type="hidden" class="span6 popup_id2" name="popup_id2" id="popup_id2" value="<?php echo $popupdetails2->popup_id;?>" />
                        </div><br>
                        <?php
                      }
                      /*End*/

                      /*Popup for Logged users use bonus benefit and havent logged in using Android App*/
                      $popup_details3 = $this->db->query("select * from popup_details where popup_id=3")->result();
                      foreach($popup_details3 as $popupdetails3)
                      {
                        ?>
                        <br>
                        <div class="log_usebonus_notuseapp detail_banners">
                          
                          <h4><b>Popup Funnel) For logged users that Already used "Unic Bonus" Feature And Havent Logged in Uisng Android App  </b></h4><br>
                          <div class="control-group">
                            <div class="controls">
                              <select  name="log_usebonus_notuseapp_status" id="log_usebonus_notuseapp_status" required class="span6">
                                <option value="0" <?php if($popupdetails3->popup_status=='0'){ echo 'selected="selected"'; }?>>Disabled</option>
                                <option value="1" <?php if($popupdetails3->popup_status=='1'){ echo 'selected="selected"'; }?>>Enabled</option>
                                <!-- <option value="2" <?php if($popupdetails3->popup_status=='2'){ echo 'selected="selected"'; }?>>Standard popup Settings</option>
                                <option value="3" <?php if($popupdetails3->popup_status=='3'){ echo 'selected="selected"'; }?>>Popup funnel Settings</option> -->
                              </select>
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">No of sessions to show the popup to each user</label>
                            <div class="controls">
                              <input type="text" class="span6 log_usebonus_notuseapp_session" name="log_usebonus_notuseapp_session" id="log_usebonus_notuseapp_session" value="<?php echo $popupdetails3->session_count;?>" /><br>
                            </div>
                          </div>
                          
                          <div class="control-group">
                             <div class="controls">
                                <input type="checkbox" name="logusr_usebonus_notuseapp_status" class="logusr_usebonus_notuseapp_status" id="logusr_usebonus_notuseapp_status" value="<?php if($popupdetails3->dont_show_status == 1) { echo 1; }else { echo 0;}?>" <?php if($popupdetails3->dont_show_status=="1"){ echo 'checked="checked"';} ?>/><div style="margin-left: 20px; margin-top: -18px; font-size:14px;">Do not show popup on homepage</div>
                            </div>
                          </div>

                          <div class="control-group">
                            <label class="control-label">Reset Counting</label>
                            <div class="controls">
                              <input type="button" class="btn btn-success log_usebonus_notuseapp_reset" name="log_usebonus_notuseapp_reset" id="log_usebonus_notuseapp_reset" onclick="return confirmDelete(this,1,0);" value="YES" />&nbsp;<span class="success"></span><br>
                            </div>
                          </div>

                          <div class="control-group">
                            <label class="control-label">Popup Content</label>
                            <div class="controls">
                              <textarea class="span6 ckeditor" name="log_usebonus_notuseapp_content" id="log_usebonus_notuseapp_content" ><?php echo $popupdetails3->popup_content;?></textarea>
                            </div>
                          </div>
                          <input type="hidden" class="span6 popup_id3" name="popup_id3" id="popup_id3" value="<?php echo $popupdetails3->popup_id;?>" />
                        </div><br>
                        <?php
                      }
                      /*End*/

                      /*Popup for Logged users not use bonus benefit and logged in using Android App*/
                      $popup_details4 = $this->db->query("select * from popup_details where popup_id=4")->result();
                      foreach($popup_details4 as $popupdetails4)
                      {
                        ?>
                        <br>
                        <div class="log_notbonus_useapp detail_banners">
                          
                          <h4><b>Popup Funnel) For logged users that havent used "Unic Bonus" Feature And Already Logged in Uisng Android App  </b></h4><br>
                          <div class="control-group">
                            <div class="controls">
                              <select  name="log_notbonus_useapp_status" id="log_notbonus_useapp_status" required class="span6">
                                <option value="0" <?php if($popupdetails4->popup_status=='0'){ echo 'selected="selected"'; }?>>Disabled</option>
                                <option value="1" <?php if($popupdetails4->popup_status=='1'){ echo 'selected="selected"'; }?>>Enabled</option>
                                <!-- <option value="2" <?php if($popupdetails4->popup_status=='2'){ echo 'selected="selected"'; }?>>Standard popup Settings</option>
                                <option value="3" <?php if($popupdetails4->popup_status=='3'){ echo 'selected="selected"'; }?>>Popup funnel Settings</option> -->
                              </select>
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">No of sessions to show the popup to each user</label>
                            <div class="controls">
                              <input type="text" class="span6 log_notbonus_useapp_session" name="log_notbonus_useapp_session" id="log_notbonus_useapp_session" value="<?php echo $popupdetails4->session_count;?>" /><br>
                            </div>
                          </div>
                          

                          <div class="control-group">
                             <div class="controls">
                                <input type="checkbox" name="logusr_notusebonus_useapp_status" class="logusr_notusebonus_useapp_status" id="logusr_notusebonus_useapp_status" value="<?php if($popupdetails4->dont_show_status == 1) { echo 1; }else { echo 0;}?>" <?php if($popupdetails4->dont_show_status=="1"){ echo 'checked="checked"';} ?>/><div style="margin-left: 20px; margin-top: -18px; font-size:14px;">Do not show popup on homepage</div>
                            </div>
                          </div>

                          <div class="control-group">
                            <label class="control-label">Reset Counting</label>
                            <div class="controls">
                              <input type="button" class="btn btn-success log_notbonus_useapp_reset" name="log_notbonus_useapp_reset" id="log_notbonus_useapp_reset" onclick="return confirmDelete(this,0,1);" value="YES" />&nbsp;<span class="success"></span><br>
                            </div>
                          </div>

                          <div class="control-group">
                            <label class="control-label">Popup Content</label>
                            <div class="controls">
                              <textarea class="span6 ckeditor" name="log_notbonus_useapp_content" id="log_notbonus_useapp_content" ><?php echo $popupdetails4->popup_content;?></textarea>
                            </div>
                          </div>
                          <input type="hidden" class="span6 popup_id4" name="popup_id4" id="popup_id4" value="<?php echo $popupdetails4->popup_id;?>" />
                        </div><br>
                        <?php
                      }
                      /*End*/

                      /*Popup for Logged users use bonus benefit and  logged in using Android App*/
                      $popup_details5 = $this->db->query("select * from popup_details where popup_id=5")->result();
                      foreach($popup_details5 as $popupdetails5)
                      {
                        ?>
                        <br>
                        <div class="log_usebonus_useapp detail_banners">
                          
                          <h4><b>Popup Funnel) For logged users that Already used "Unic Bonus" Feature And Already Logged in Uisng Android App  </b></h4><br>
                          <div class="control-group">
                            <div class="controls">
                              <select  name="log_usebonus_useapp_status" id="log_usebonus_useapp_status" required class="span6">
                                <option value="0" <?php if($popupdetails5->popup_status=='0'){ echo 'selected="selected"'; }?>>Disabled</option>
                                <option value="1" <?php if($popupdetails5->popup_status=='1'){ echo 'selected="selected"'; }?>>Enabled</option>
                                <!-- <option value="2" <?php if($popupdetails5->popup_status=='2'){ echo 'selected="selected"'; }?>>Standard popup Settings</option>
                                <option value="3" <?php if($popupdetails5->popup_status=='3'){ echo 'selected="selected"'; }?>>Popup funnel Settings</option> -->
                              </select>
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">No of sessions to show the popup to each user</label>
                            <div class="controls">
                              <input type="text" class="span6 log_usebonus_useapp_session" name="log_usebonus_useapp_session" id="log_usebonus_useapp_session" value="<?php echo $popupdetails5->session_count;?>" /><br>
                            </div>
                          </div>
                          
                          <div class="control-group">
                             <div class="controls">
                                <input type="checkbox" name="logusr_usebonus_useapp_status" class="logusr_usebonus_useapp_status" id="logusr_usebonus_useapp_status" value="<?php if($popupdetails5->dont_show_status == 1) { echo 1; }else { echo 0;}?>" <?php if($popupdetails5->dont_show_status=="1"){ echo 'checked="checked"';} ?>/><div style="margin-left: 20px; margin-top: -18px; font-size:14px;">Do not show popup on homepage</div>
                            </div>
                          </div>

                          <div class="control-group">
                            <label class="control-label">Reset Counting</label>
                            <div class="controls">
                              <input type="button" class="btn btn-success log_usebonus_useapp_reset" name="log_usebonus_useapp_reset" id="log_usebonus_useapp_reset" onclick="return confirmDelete(this,1,1);" value="YES" />&nbsp;<span class="success"></span><br>
                            </div>
                          </div>

                          <div class="control-group">
                            <label class="control-label">Popup Content</label>
                            <div class="controls">
                              <textarea class="span6 ckeditor" name="log_usebonus_useapp_content" id="log_usebonus_useapp_content" ><?php echo $popupdetails5->popup_content;?></textarea>
                            </div>
                          </div>
                          <input type="hidden" class="span6 popup_id5" name="popup_id5" id="popup_id5" value="<?php echo $popupdetails5->popup_id;?>" />
                        </div><br>
                        <?php
                      }
                      /*End*/

                      /*Standard Popup for All Logged users*/
                      $popup_details6 = $this->db->query("select * from popup_details where popup_id=6")->result();
                      foreach($popup_details6 as $popupdetails6)
                      {
                        ?>
                        <br>
                        <div class="stnd_all_log_users detail_banners">
                          
                          <h4><b>Standard Popup) For All logged users </b></h4><br>
                          <div class="control-group">
                            <div class="controls">
                              <select  name="stnd_all_log_users_status" id="stnd_all_log_users_status" required class="span6">
                                <option value="0" <?php if($popupdetails6->popup_status=='0'){ echo 'selected="selected"'; }?>>Disabled</option>
                                <option value="1" <?php if($popupdetails6->popup_status=='1'){ echo 'selected="selected"'; }?>>Enabled</option>
                                <!-- <option value="2" <?php if($popupdetails6->popup_status=='2'){ echo 'selected="selected"'; }?>>Standard popup Settings</option>
                                <option value="3" <?php if($popupdetails6->popup_status=='3'){ echo 'selected="selected"'; }?>>Popup funnel Settings</option> -->
                              </select>
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">No of sessions to show the popup to each user</label>
                            <div class="controls">
                              <input type="text" class="span6 stnd_all_log_users_session" name="stnd_all_log_users_session" id="stnd_all_log_users_session" value="<?php echo $popupdetails6->session_count;?>" /><br>
                            </div>
                          </div>
                          
                          <div class="control-group">
                             <div class="controls">
                                <input type="checkbox" name="alllogusr_status" class="alllogusr_status" id="alllogusr_status" value="<?php if($popupdetails6->dont_show_status == '1') { echo '1'; } else { echo '0'; }?>" <?php if($popupdetails6->dont_show_status=="1"){ echo 'checked="checked"';} ?>/><div style="margin-left: 20px; margin-top: -18px; font-size:14px;">Do not show popup on homepage</div>
                            </div>
                          </div>

                          <div class="control-group">
                            <label class="control-label">Reset Counting</label>
                            <div class="controls">
                              <input type="button" class="btn btn-success stnd_all_log_users_reset" name="stnd_all_log_users_reset" id="stnd_all_log_users_reset" onclick="return confirmDelete(this,2,2);" value="YES" />&nbsp;<span class="success"></span><br>
                            </div>
                          </div>

                          <div class="control-group">
                            <label class="control-label">Popup Content</label>
                            <div class="controls">
                              <textarea class="span6 ckeditor" name="stnd_all_log_users_content" id="stnd_all_log_users_content" ><?php echo $popupdetails6->popup_content;?></textarea>
                            </div>
                          </div>
                          <input type="hidden" class="span6 popup_id6" name="popup_id6" id="popup_id6" value="<?php echo $popupdetails6->popup_id;?>" />
                        </div><br>
                        <?php
                      }
                      /*End*/
                      ?>
            
                      <input type="hidden" name="admin_id" id="admin_id" value="<?php echo $admin_details->admin_id; ?>">
                      
                      <div class="form-actions">
                        <input type="submit" name="save" value="Save Changes" class="btn btn-success">
                      </div>
                    <?php echo form_close(); ?>
                    <!--</form>-->
                    <!-- END FORM-->
                  </div>
            </div>
            <!-- END SAMPLE FORM widget-->
          </div>
        </div>
      </div>
         <!-- END PAGE CONTAINER-->
    </div>
      <!-- END PAGE -->  
  </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
  <!-- SATz -->
  <!-- Time picker -->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>front/js/moment.js"></script>   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>front/js/combodate.js"></script>
  
  <script>
    function confirmDelete(ele,bonus,log_type)  // Confirm before Reset the session count for notusebonus and notuseapp..
    {
      var objparant = $(ele).parents('.controls');
      var m ='Do you want to Reset this Count?';
      if(!confirm(m))
      {
        return false;
      }
      else
      {

        $.ajax({
        type: "POST",
        url: '<?php echo base_url();?>adminsettings/update_session_count',
        data:{'bonus':bonus,'log_type':log_type},
        success:function(result)
        {
          objparant.find('.success').css("color","green");
          objparant.find('.success').text('Count Resetted!');
          setTimeout(function(){
          objparant.find('.success').text('');
        }, 3000);
        }
        }); 
        return false; 
      }
    }
    
    $('.notlogusr_status').click(function(){
       if($(this).is(':checked') == true)
       {
        $(this).val(1);
       }
       else
       {
        $(this).val(0);
       }
    });
    $('.logusr_notusebonus_notuseapp_status').click(function(){
       if($(this).is(':checked') == true)
       {
        $(this).val(1);
       }
       else
       {
        $(this).val(0);
       }
    });
    $('.logusr_usebonus_notuseapp_status').click(function(){
       if($(this).is(':checked') == true)
       {
        $(this).val(1);
       }
       else
       {
        $(this).val(0);
       }
    });
    $('.logusr_notusebonus_useapp_status').click(function(){
       if($(this).is(':checked') == true)
       {
        $(this).val(1);
       }
       else
       {
        $(this).val(0);
       }
    });
    $('.logusr_usebonus_useapp_status').click(function(){
       if($(this).is(':checked') == true)
       {
        $(this).val(1);
       }
       else
       {
        $(this).val(0);
       }
    });
    $('.alllogusr_status').click(function(){
       if($(this).is(':checked') == true)
       {
        $(this).val(1);
       }
       else
       {
        $(this).val(0);
       }
    });
    

  </script>
<!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>
                              