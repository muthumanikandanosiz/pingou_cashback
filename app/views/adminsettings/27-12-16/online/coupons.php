
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Coupons | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>
  <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
    <style>
    div.selector, div.selector span, div.checker span, div.radio span, div.uploader, div.uploader span.action, div.button, div.button span
    {
      background-image:none
    }

    /*New style for datatable related css 17-12-16*/
    .col-xs-6 {width: 47% !important; float: left;min-height:1px;padding-left: 12px;padding-right: 12px;position: relative;}
    select.input-sm { width:60px !important;}
    .row {margin-left: -12px;margin-right: -12px;}
    .pagination {border-radius: 4px;margin: 20px 0;padding-left: 0;}
    /*End 17-12-16*/
  </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
          <div class="span12" style="margin-top:13px;">
            <span style="float:right">
              <p class="badge badge-info">Featured</p> &nbsp;//
              <!-- <p class="badge badge-success">Ret.Week</p> // -->
              <p class="badge badge-important">Exclusive</p> &nbsp;
            </span>
            <span style="float:left">
              <a class="btn btn-success" href="<?php echo base_url();?>adminsettings/download_free_coupons/active">Download Coupons</a> &nbsp;
            </span>
            <!-- END PAGE TITLE & BREADCRUMB-->
          </div>
        </div>
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN EXAMPLE TABLE widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i> Coupons</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                </span>
              </div>
              <div class="widget-body">
    						<?php 
    					  $error = $this->session->flashdata('error');
    					  if($error!="")
                {
      						echo '<div class="alert alert-error">
      						<button data-dismiss="alert" class="close">x</button>
      						<strong>Error! </strong>'.$error.'</div>';
    					  }
    						$success = $this->session->flashdata('success');
    						if($success!="")
                {
    						  echo '<div class="alert alert-success">
    							<button data-dismiss="alert" class="close">x</button>
    							<strong>Success! </strong>'.$success.'</div>';			
    						} 
						    ?>
                <form id="form2" action="" method="post" name="form2">
                    <table class="table table-striped table-bordered" id="sample_teste1">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th style=""><input type="checkbox" id="check_b" class="check_b" onchange="checkAll(this)" name="chk[]" /></th>
                          <th><center>Coupon ID</center></th>
                          <th><center>Store</center></th>
                          <th><center>Title</center></th>
                          <th><center>Code</center></th>
                          <th><center>Offers</center></th>
                          <th><center>Promo ID</center></th>
                          <th><center>Expiry Date</center></th>
                          <th><center>Edit</center></th>
                          <th><center>Delete</center></th>
                        </tr>
                      </thead>
                    </table>
                    <input type="hidden" name="hidd" value="hidd">
                    <input id="GoUpdate" class="btn btn-warning" type="submit" value="Delete Coupons" name="GoUpdate">
                </form>
              </div>
            </div>
            <!-- END EXAMPLE TABLE widget-->
          </div>
        </div>
        <!-- END ADVANCED TABLE widget-->
        <!-- END PAGE CONTENT-->
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
  

  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>

  <script>
    jQuery(document).ready(function() {
    // initiate layout and plugins
    App.init();
    });
  </script>
<script type="text/javascript">
function confirmDelete()  // Confirm before delete coupon..
{
	var m = "Do you want to delete this coupon detail?";
  if(!confirm(m))
	{
		return false;
	}
	else
	{
		return true;
	}
}
</script>
<script>
      $(document).ready(function() {
		$(".check_b").attr("style", "opacity: 1;");
      });
	  function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }
 

$(document).ready(function() {
$('#sample_teste1').DataTable( {
"processing": true,
"serverSide": true,
"ajax": {
"url": "<?php echo site_url('adminsettings/newcoupons')?>",
"data": {
//"totalrecords": "<?php echo $iTotal; ?>"
}
}
});
});
 
</script>

</body>
<!-- END BODY -->
</html>
 <style type="text/css">
 .dataTables_processing
{
  display:none !important;
}
</style>
<link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/css/pagination.css" rel="stylesheet" />
