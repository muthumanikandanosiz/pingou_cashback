<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php
  $admin_details   = $this->admin_model->get_admindetails();
  ?>
  <title>Admin Settings | <?php echo $admin_details->site_name; ?> Admin</title>
  <?php $this->load->view('adminsettings/script'); ?>

  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
          <div class="span12">
           <!--  <h3 class="page-title">
            Sales Funnel Settings
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
                <span class="divider">&nbsp;</span>
              </li>
              <li>Marketing<span class="divider">&nbsp;</span></li>
              <li>
                <?php echo anchor('adminsettings/sales_funnel','Sales Funnel'); ?>
                <span class="divider-last">&nbsp;</span>
              </li>
            </ul> -->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN SAMPLE FORM widget-->
            <div class="widget">
                  <div class="widget-title">
                    <h4><i class="icon-cog"></i> Sales Funnel Settings</h4>
                    <span class="tools">
                      <a href="javascript:;" class="icon-chevron-down"></a>
                      <!--<a href="javascript:;" class="icon-remove"></a>-->
                    </span>
                  </div><br>
                  <span>
                    <span class="required_field"> &nbsp;&nbsp;&nbsp;*</span> marked fields are mandatory.
                  </span>
                  <br>
                  <div class="widget-body form">
                    <?php 
                    $error = $this->session->flashdata('error');
                    if($error!="")
                    {
                      echo '<div class="alert alert-error">
                      <button data-dismiss="alert" class="close">x</button>
                      <strong>Error! </strong>'.$error.'</div>';
                    }
                    $success = $this->session->flashdata('success');
                    if($success!="") 
                    {
                      echo '<div class="alert alert-success">
                      <button data-dismiss="alert" class="close">x</button>
                      <strong>Success! </strong>'.$success.'</div>';      
                    }
                    ?>
                    <!-- BEGIN FORM-->
                    <?php
                      $attribute = array('role'=>'form','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data','onSubmit' =>'return validation();' ); 
                      echo form_open('adminsettings/sales_funnel_settings_update',$attribute);
                    ?>
                      <div class="control-group">
                        <label class="control-label">New user Account activation method <span class="required_field">*</span></label>
                        <div class="controls">
                          <select  name="activate_method" id="activate_method" required class="span6">
                            <option value="0" <?php if($activate_method=='0'){ echo 'selected="selected"'; }?>>Activate via Email confirmation</option>
                            <option value="1" <?php if($activate_method=='1'){ echo 'selected="selected"'; }?>>Activate without Email confirmation</option>
                          </select>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Notification for UNLOGGED Users <span class="required_field">*</span></label>
                        <div class="controls">
                          <select  name="unlog_status" id="unlog_status" required class="span6">
                            <option value="1" <?php if($unlog_status=='1'){ echo 'selected="selected"'; }?>>Enable</option>
                            <option value="0" <?php if($unlog_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                          </select>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Content of Notification bar for UNLOGGED users</label>
                        <div class="controls">
                          <textarea class="span6 ckeditor" name="unlog_content" id="unlog_content" ><?php echo $unlog_content; ?></textarea>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Notification for LOGGED Users <span class="required_field">*</span></label>
                        <div class="controls">
                          <select  name="log_status" id="log_status" required class="span6">
                            <option value="1" <?php if($log_status=='1'){ echo 'selected="selected"'; }?>>Enable</option>
                            <option value="0" <?php if($log_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                          </select>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Float menu for UNLOGGED Users <span class="required_field">*</span></label>
                        <div class="controls">
                          <select  name="unlog_usr_status" id="unlog_usr_status" required class="span6">
                            <option value="0" <?php if($unlog_menu_status=='0'){ echo 'selected="selected"'; }?>>Disabled</option>
                            <option value="1" <?php if($unlog_menu_status=='1'){ echo 'selected="selected"'; }?>>Enabled, only to the menu </option>
                            <option value="2" <?php if($unlog_menu_status=='2'){ echo 'selected="selected"'; }?>>Enabled, to the menu and the notification bar </option>
                          </select>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Float menu for LOGGED Users <span class="required_field">*</span></label>
                        <div class="controls">
                          <select  name="log_usr_status" id="log_usr_status" required class="span6">
                            <option value="0" <?php if($log_menu_status=='0'){ echo 'selected="selected"'; }?>>Disabled</option>
                            <option value="1" <?php if($log_menu_status=='1'){ echo 'selected="selected"'; }?>>Enabled, only to the menu</option>
                            <option value="2" <?php if($log_menu_status=='2'){ echo 'selected="selected"'; }?>>Enabled, to the menu and the notification bar </option>
                          </select>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Notification bar background color <span class="required_field">*</span></label>
                        <div class="controls">
                          <input type="text" class="span6" name="notify_color" id="notify_color" value="<?php echo $notify_color; ?>" required />
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Related Store details</label>
                        <div class="controls">
                          <textarea class="span6 ckeditor" name="rel_store_details" id="rel_store_details" ><?php echo $rel_store_details; ?></textarea>
                          <!-- <textarea class="span6" name="rel_store_details" id="rel_store_details" rows="5" required ><?php echo $rel_store_details; ?></textarea> -->
                        </div>
                      </div> 

                        
                      <h4>PLACES TO DISPLAY THE BANNER</h4>
                      <h5><b>Important! this feature should only be displyed in desktops.(that is: Screen sizes larger that 750px)....  </b></h4><br>
                      
                      <div class="control-group">
                        <label class="control-label"><?php echo $topcashback;?></label>
                        <div class="controls">
                          <select  name="topcash_top_status" id="topcash_top_status" required class="span6">
                            <option value="0" <?php if($top_top_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($top_top_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($top_top_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($top_top_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category</option>
                            <option value="4" <?php if($top_top_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select> &nbsp;Between tag H1 and stores grid
                        </div>
                      </div>
                      <div class="control-group">
                        <div class="controls">
                          <select  name="topcash_bot_status" id="topcash_bot_status" required class="span6">
                            <option value="0" <?php if($top_bot_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($top_bot_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($top_bot_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($top_bot_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category</option>
                            <option value="4" <?php if($top_bot_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select> &nbsp;After the stores grid
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label"><?php echo $category;?></label>
                        <div class="controls">
                          <select  name="cat_top_status" id="cat_top_status" required class="span6">
                            <option value="0" <?php if($cat_top_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($cat_top_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($cat_top_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($cat_top_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category</option>
                            <option value="4" <?php if($cat_top_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select> &nbsp;Between tag H1 and caetgories grid
                        </div>
                      </div>
                      <div class="control-group">
                        <div class="controls">
                          <select  name="cat_bot_status" id="cat_bot_status" required class="span6">
                            <option value="0" <?php if($cat_bot_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($cat_bot_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($cat_bot_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($cat_bot_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category</option>
                            <option value="4" <?php if($cat_bot_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select> &nbsp;After the categories grid
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label"><?php echo $store;?></label>
                        <div class="controls">
                          <select  name="store_top_status" id="store_top_status" required class="span6">
                            <option value="0" <?php if($store_top_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($store_top_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($store_top_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($store_top_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category </option>
                            <option value="4" <?php if($store_top_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select> &nbsp;Between the tag H1 and the active coupons list 
                        </div>
                      </div>
                      <div class="control-group">
                        <div class="controls">
                          <select  name="store_bot_status" id="store_bot_status" required class="span6">
                            <option value="0" <?php if($store_bot_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($store_bot_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($store_bot_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($store_bot_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category</option>
                            <option value="4" <?php if($store_bot_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select> &nbsp; Immediately after the active coupons list
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label"><?php echo $barato;?></label>
                        <div class="controls">
                          <select  name="barato_top_status" id="barato_top_status" required class="span6">
                            <option value="0" <?php if($barato_top_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($barato_top_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($barato_top_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($barato_top_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category </option>
                            <option value="4" <?php if($barato_top_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML bo content</option>
                          </select> &nbsp; After tag H1
                        </div>
                      </div>
                      <div class="control-group">
                        <div class="controls">
                          <select  name="barato_bot_status" id="barato_bot_status" required class="span6">
                            <option value="0" <?php if($barato_bot_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($barato_bot_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($barato_bot_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($barato_bot_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category</option>
                            <option value="4" <?php if($barato_bot_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select> &nbsp; Between product description and related products data 
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label"><?php echo $minhaconta;?></label>
                        <div class="controls">
                          <select  name="minha_top_status" id="minha_top_status" required class="span6">
                            <option value="0" <?php if($myacc_top_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($myacc_top_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($myacc_top_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($myacc_top_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category </option>
                            <option value="4" <?php if($myacc_top_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select>&nbsp; Between submenu content area
                        </div>
                      </div>
                      <div class="control-group">
                        <div class="controls">
                          <select  name="minha_bot_status" id="minha_bot_status" required class="span6">
                            <option value="0" <?php if($myacc_bot_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($myacc_bot_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($myacc_bot_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($myacc_bot_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category</option>
                            <option value="4" <?php if($myacc_bot_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select>&nbsp; Between content and my earnings box
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label"><?php echo $extrato;?></label>
                        <div class="controls">
                          <select  name="extrato_top_status" id="extrato_top_status" required class="span6">
                            <option value="0" <?php if($extrato_top_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($extrato_top_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($extrato_top_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($extrato_top_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category </option>
                            <option value="4" <?php if($extrato_top_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select>&nbsp; Between menu and search table box
                        </div>
                      </div>
                      <div class="control-group">
                        <div class="controls">
                          <select  name="extrato_bot_status" id="extrato_bot_status" required class="span6">
                            <option value="0" <?php if($extrato_bot_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($extrato_bot_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($extrato_bot_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($extrato_bot_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category</option>
                            <option value="4" <?php if($extrato_bot_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select>&nbsp; Between table and my earning box
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label"><?php echo $resgate;?></label>
                        <div class="controls">
                          <select  name="resgate_top_status" id="resgate_top_status" required class="span6">
                            <option value="0" <?php if($resgate_top_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($resgate_top_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($resgate_top_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($resgate_top_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category </option>
                            <option value="4" <?php if($resgate_top_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select>&nbsp; Between the menu and the withdraw area
                        </div>
                      </div>
                      <div class="control-group">
                        <div class="controls">
                          <select  name="resgate_bot_status" id="resgate_bot_status" required class="span6">
                            <option value="0" <?php if($resgate_bot_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($resgate_bot_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($resgate_bot_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($resgate_bot_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category</option>
                            <option value="4" <?php if($resgate_bot_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select>&nbsp; Between table and my earning box
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label"><?php echo $clickhistory;?></label>
                        <div class="controls">
                          <select  name="click_top_status" id="click_top_status" required class="span6">
                            <option value="0" <?php if($clkhis_top_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($clkhis_top_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($clkhis_top_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($clkhis_top_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category </option>
                            <option value="4" <?php if($clkhis_top_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select>&nbsp; Between menu and search table box
                        </div>
                      </div>
                      <div class="control-group">
                        <div class="controls">
                          <select  name="click_bot_status" id="click_bot_status" required class="span6">
                            <option value="0" <?php if($clkhis_bot_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($clkhis_bot_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($clkhis_bot_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($clkhis_bot_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category</option>
                            <option value="4" <?php if($clkhis_bot_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select>&nbsp; Between table and my earning box
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label"><?php echo $categorystore;?></label>
                        <div class="controls">
                          <select  name="catstore_top_status" id="catstore_top_status" required class="span6">
                            <option value="0" <?php if($catstore_top_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($catstore_top_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($catstore_top_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($catstore_top_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category </option>
                            <option value="4" <?php if($catstore_top_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select>&nbsp; After tag H1
                        </div>
                      </div>
                      <div class="control-group">
                        <div class="controls">
                          <select  name="catstore_bot_status" id="catstore_bot_status" required class="span6">
                            <option value="0" <?php if($catstore_bot_status=='0'){ echo 'selected="selected"'; }?>>Disable</option>
                            <option value="1" <?php if($catstore_bot_status=='1'){ echo 'selected="selected"'; }?>>Enable, Sales funnel Random Image banner between the uploaded to the same referral category</option>
                            <option value="2" <?php if($catstore_bot_status=='2'){ echo 'selected="selected"'; }?>>Enable, Sales funnel HTML box content</option>
                            <option value="3" <?php if($catstore_bot_status=='3'){ echo 'selected="selected"'; }?>>Enable, Standard content Random Image banner between the uploaded to the same referral category</option>
                            <option value="4" <?php if($catstore_bot_status=='4'){ echo 'selected="selected"'; }?>>Enable, Standard content HTML box content</option>
                          </select>&nbsp;After stores grid
                        </div>
                      </div>

                      <input type="hidden" name="sales_funnel_banner_id" id="sales_funnel_banner_id" value="<?php echo $bannerdetail->sales_funnel_banner_id; ?>">
                      <input type="hidden" name="sales_banner_id" id="sales_banner_id" value="<?php echo $sales_banner_id; ?>">

                      <input type="hidden" name="username" id="username" value="<?php echo $admin_username; ?>">
                      <input type="hidden" name="admin_id" id="admin_id" value="<?php echo $admin_id; ?>">
                      <div class="form-actions">
                        <input type="submit" name="save" value="Save Changes" class="btn btn-success">
                      </div>
                    <?php echo form_close(); ?>
                    <!--</form>-->
                    <!-- END FORM-->
                  </div>
            </div>
            <!-- END SAMPLE FORM widget-->
          </div>
        </div>
      </div>
         <!-- END PAGE CONTAINER-->
    </div>
      <!-- END PAGE -->  
  </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   

   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckfinder/ckfinder.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
  <!-- SATz -->
  <!-- Time picker -->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>front/js/moment.js"></script>   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>front/js/combodate.js"></script>
  
  <script>
    $(function()
    {
      /*New code for add,update,delete a multiple image banner details 4-11-16*/
      $(document).delegate('.deletebanner','click',function()
      {
        var objpnt    = $(this).parents('.detail_banners');
        var banner_id = $(this).attr('bid');
        var cond_type = $(this).attr('ctype');  
        $.ajax({
              type: "POST",
              url: '<?php echo base_url();?>adminsettings/delete_bannerdetails',
              data:{'banner_id':banner_id},
              success:function(result)
              {
                //alert("hai"); 

                if(result==1)
                {
                  $.ajax({
                    type: "POST",
                    url: '<?php echo base_url();?>adminsettings/get_bannerdetails',
                    data:  {cat_type:cond_type},
                    dataType:"text", 
                    success: 
                    function(results)
                    {
                        objpnt.find('.newbanners').replaceWith(results);
                    }
                    });   
                }

              }
            }); 
            return false;   
      })

      $(document).delegate('.updatebanner_status','click',function()
      {
       
        var objpnt    = $(this).parents('.detail_banners');
        var banner_id = $(this).attr('bid');
        var cond_type = $(this).attr('ctype');  
        var status    = $(this).attr('status');

        $.ajax({
              type: "POST",
              url: '<?php echo base_url();?>adminsettings/update_bannerdetails',
              data:{'banner_id':banner_id,'status':status},
              success:function(result)
              {
                //alert("hai"); 

                if(result==1)
                {
                  $.ajax({
                    type: "POST",
                    url: '<?php echo base_url();?>adminsettings/get_bannerdetails',
                    data:  {cat_type:cond_type},
                    dataType:"text", 
                    success: 
                    function(results)
                    {
                        objpnt.find('.newbanners').replaceWith(results);
                    }
                    });   
                }

              }
            }); 
            return false;   
      })

      $(document).delegate('.submit_banners','click',function()
      {
        var objpnt       = $(this).parents('.detail_banners');
        var imagedetails = objpnt.find(".bannerimages").val();
        var image_name   = objpnt.find(".bannerimages"); 
        var img_name     = image_name[0].files[0];
        var banner_url   = objpnt.find('.bannerurl').val();
        var ref_category = objpnt.find('.refcat_type').val();
        var cond_type    = objpnt.find('.condition_type').val();

        if(typeof ref_category === 'undefined')
        {
          ref_category = 0;
        }
        
        var formData = new FormData();
        formData.append('img_name',img_name);
        formData.append('bannerurl',banner_url);
        formData.append('cat_type', ref_category);
        formData.append('condtype', cond_type);

        
         
        if(banner_url == '')
        {
          objpnt.find('.bannerurl').css("border","2px solid red");
          return false;
        }

        if(imagedetails == '')
        { 
          alert('Please Upload a Image');
          return false;
        }
        if(imagedetails!='' && banner_url!='')
        {
          objpnt.find('.bannerurl').css("border","none");
          $.ajax({
             type: "POST",
              url: '<?php echo base_url();?>adminsettings/add_bannerdetails', 
              data: formData,
              cache: false,
              contentType: false,
              processData: false,
              success: 
              function(data)
              { 
                if(data==1)
                {
                  
                  $.ajax({
                  type: "POST",
                  url: '<?php echo base_url();?>adminsettings/get_bannerdetails',
                  data:  {cat_type:cond_type},
                  dataType:"text", 
                  success: 
                  function(results)
                  {
                      objpnt.find('.newbanners').replaceWith(results);
                  }
                  }); 
                  objpnt.find('.bannerimages').val('');
                  objpnt.find('.bannerurl').val('');
                  objpnt.find('.refcat_type').val('');
                }   
              }
          });
        }
      })
      /*End 4-11-16*/
      $('#time').combodate({
        firstItem: 'name', //show 'hour' and 'minute' string at first item of dropdown
        minuteStep: 1
      });  

    });
 
  
    jQuery(document).ready(function() {       
      // initiate layout and plugins
      App.init();
    });

    CKEDITOR.replace( 'unlog_content', {
      // Configure your file manager integration. This example uses CKFinder 3 for PHP.
      filebrowserBrowseUrl: '<?php echo base_url();?>assets/assets/ckfinder/ckfinder.html',
      filebrowserImageBrowseUrl: '<?php echo base_url();?>assets/assets/ckfinder/ckfinder.html?type=Images',
      filebrowserFlashBrowseUrl: '<?php echo base_url();?>assets/assets/ckfinder/ckfinder.html?Type=Flash',
      filebrowserUploadUrl: '<?php echo base_url();?>assets/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
      filebrowserImageUploadUrl: '<?php echo base_url();?>assets/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
      filebrowserFlashUploadUrl: '<?php echo base_url();?>assets/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });

    CKEDITOR.replace( 'rel_store_details', {
      // Configure your file manager integration. This example uses CKFinder 3 for PHP.
      filebrowserBrowseUrl: '<?php echo base_url();?>assets/assets/ckfinder/ckfinder.html',
      filebrowserImageBrowseUrl: '<?php echo base_url();?>assets/assets/ckfinder/ckfinder.html?type=Images',
      filebrowserFlashBrowseUrl: '<?php echo base_url();?>assets/assets/ckfinder/ckfinder.html?Type=Flash',
      filebrowserUploadUrl: '<?php echo base_url();?>assets/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
      filebrowserImageUploadUrl: '<?php echo base_url();?>assets/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
      filebrowserFlashUploadUrl: '<?php echo base_url();?>assets/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });
    
    CKFinder.setupCKEditor( editor, '../' );

    function change_background(background)
    {
      if(background=='color') 
      {
        $("#back_image").hide();
        $("#back_color").show();
      }
      if(background=='image') 
       {
        $("#back_color").hide();
        $("#back_image").show();
      }
    }

    function change_coverphoto(cover)
    {
      if(cover=='covercolor') 
      {
        $("#coverimage").hide();
        $("#covercolor").show();
      }
      if(cover=='coverimage') 
      {
        $("#covercolor").hide();
        $("#coverimage").show();
      }
    }
  </script>
<!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>
                              