<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Retailer | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>

  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
</head>
<style type="text/css">
  .mar-lno{margin-left:0 !important;}
  .form-horizontal .controls {
    margin-left: 0 !important; 
}
.form-actions {
    border-top: none !important;
   }
   .controls img {
    height:140px !important;
   }
</style>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
          <div class="span12">
            <!-- END THEME CUSTOMIZER-->
            <!-- <h3 class="page-title">
            Retailer Details
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
						    <span class="divider">&nbsp;</span>
              </li>
              <li>
							  <?php echo anchor('adminsettings/affiliates','Retailer'); ?>
							  <span class="divider">&nbsp;</span>
              </li>
					    <li>
							  Retailers Details<span class="divider-last">&nbsp;</span>
              </li>
            </ul> -->
            <br>
            <span style="float:right; margin-right: 77%;">
              <a href="<?php echo base_url();?>adminsettings/affiliates" style="padding: 4px 6px !important;"class="btn btn-success">View Retailer</a> &nbsp;
              <a href="<?php echo base_url();?>adminsettings/bulk_store" class="btn btn-success">Import Retailers</a>
            </span>
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN SAMPLE FORM widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-file"></i> Retailer</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                  <!--<a href="javascript:;" class="icon-remove"></a>-->
                </span>
              </div>
              <br>
					    <span><span class="required_field"> &nbsp;&nbsp;&nbsp;*</span> marked fields are mandatory.</span><br>
					    <?php
    					if($action=="new")
              {
    					  ?>
                <div class="widget-body form">
        					<?php 
        					$error = $this->session->flashdata('error');
        					if($error!="") 
                  {
        					  echo '<div class="alert alert-error">
        						<button data-dismiss="alert" class="close">x</button>
        						<strong>Error! </strong>'.$error.'</div>';
        					}
        					$success = $this->session->flashdata('success');
        					if($success!="") 
                  {
        						echo '<div class="alert alert-success">
        						<button data-dismiss="alert" class="close">x</button>
        						<strong>Success! </strong>'.$success.'</div>';
        					}
                  ?>
                  <!-- BEGIN FORM-->
                  <!--<form action="#" class="form-horizontal">-->
  						    <?php
      						$sess_affiliate_desc      = $this->session->flashdata('affiliate_desc');
      						$sess_affiliate_name      = $this->session->flashdata('affiliate_name');
      						$sess_logo_url            = $this->session->flashdata('logo_url');
      						$sess_meta_keyword        = $this->session->flashdata('meta_keyword');
      						$sess_meta_description    = $this->session->flashdata('meta_description');
      						$sess_cashback_percentage = $this->session->flashdata('cashback_percentage');
  							  
                  $attribute = array('role'=>'form','method'=>'post','id'=>'change_pwd','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
  							  echo form_open('adminsettings/addaffiliate',$attribute);
  					 	    ?>
               
                  <div class="span5">
                    <div class="span12 mar-lno">
                      <label class="control-label span12">Retailer Name <span class="required_field">*</span></label>
      						    <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="affiliate_name" id="affiliate_name" value="<?php if($sess_affiliate_name!=""){ echo $sess_affiliate_name;  }?>" required />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="span12 mar-lno">
                      <label class="control-label span12">Retailer URL <span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <input type="url" class="span12" name="logo_url" id="logo_url" value="<?php if($sess_logo_url!=""){ echo $sess_logo_url;  }?>" required />
                          </div>
                        </div>
                      </div>
                    </div>

                    <!-- New code for Add a affiliate url details 30-11-16 -->
                     <div class="span12 mar-lno">
                      <label class="control-label span12">Retailer URL slug<span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="url_slug" id="url_slug" value="<?php if($url_slug!=""){ echo $url_slug;  }?>" required />
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End 30-11-16-->


                    <div class="span12 mar-lno">
                      <label class="control-label span12">Meta Keyword <span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="meta_keyword" id="meta_keyword" value="<?php if($sess_meta_keyword!=""){ echo $sess_meta_keyword;  }?>" required />
                          </div>
                        </div>
                      </div> 
                    </div>   
                    <div class="span12 mar-lno">
                      <label class="control-label span12">Meta Description <span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <textarea class="span12" name="meta_description"  rows="4" id="meta_description" required><?php if($sess_meta_description!=""){ echo $sess_meta_description; }?></textarea> 
                          </div>
                        </div>
                      </div>  
                    </div> 
                    <div class="span12 mar-lno">
                      <label class="control-label span12">Cashback Type</label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <select name="affiliate_cashback_type" class="span12">
                              <option>Select</option>
                              <option value="Percentage">Percentage</option>
                              <option value="Flat">Flat</option>
                              <option value="Desativado">Desativado</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>  
                    <div class="span6 mar-lno">
                      <label class="control-label span12">Cashback for user </label>
                      <div class="span12 mar-lno" style="width: 140% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span6" name="cashback_percentage" id="cashback_percentage" value="<?php if($sess_cashback_percentage!=""){ echo $sess_cashback_percentage;  }?>" />  
                          </div>
                        </div>
                      </div>  
                    </div>
                    <!--Pilaventhiran 03/05/2016 START-->
                    <div class="span6 mar-lno">
                      <label class="control-label span12" style="width: 240px !important; margin-left: -30px;">Cashback for user in the Android App</label>
                      <div class="span12 mar-lno" style="margin-left: -30px !important; width: 285px !important;">  
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span10" name="cashback_percent_android" id="cashback_percent_android" />  
                          </div>
                        </div>
                      </div>  
                    </div>  
                    <div class="span6 mar-lno">
                      <label class="control-label span12" style="width: 88% !important;">Content Extra tracking web</label>
                      <div class="span12 mar-lno" style="width: 140% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span6" name="content_extra_param" id="content_extra_param" >
                          </div>
                        </div>
                      </div>    
                    </div>
                    <div class="span6 mar-lno">
                      <label class="control-label span12" style="width: 240px !important; margin-left: -20px; "> Content Extra tracking ANDROID</label>
                      <div class="span12 mar-lno" style="margin-left: -30px !important; width: 285px !important;">
                        <div class="control-group">
                            <div class="controls">
                              <input type="text" class="span10" name="content_extra_param_android" id="content_extra_param_android" >
                            </div>
                        </div>
                      </div>
                    </div>  
                    <div class="span6 mar-lno">
                      <label class="control-label span12" style="width: 88% !important;">Old Cashback</label>
                      <div class="span12 mar-lno" style="width: 140% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span6" name="old_cashback" id="old_cashback" >
                          </div>
                        </div>
                      </div>
                    </div>    
                    <div class="span6 mar-lno">
                      <label class="control-label span12" style="width: 240px !important; margin-left: -30px;">CashB for user Android(content)</label>
                      <div class="span12 mar-lno" style="margin-left: -30px !important; width: 285px !important;">
                        <div class="control-group">
                            <div class="controls">
                              <input type="text" class="span10" name="cashback_content_android" id="cashback_content_android" >
                            </div>
                        </div>
                      </div>
                    </div>
                    <!--Pilaventhiran 03/05/2016 START-->
                    <div class="span6 mar-lno">
                      <label class="control-label span12" style="width: 92% !important;">Days store know about trans</label>
                      <div class="span12 mar-lno" style="width: 140% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span6" name="report_date" id="report_date" required="" />
                          </div>
                        </div>
                      </div>  
                    </div>           
                    <!--Pilaventhiran 03/05/2016 END-->
                    <!-- new code for estimated days for store details 18-8-16 start -->
                    <div class="span5 mar-lno">
                      <label class="control-label span12" style="width: 240px !important; margin-left: -10px;">Estimated Payment</label>
                      <div class="span12 mar-lno" style="margin-left: -10px !important; width: 260px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span10" name="retailer_ban_url" id="retailer_ban_url" > 
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- end 18-8-16 -->
                    <div class="span6 mar-lno" style="margin-top:15px;">
                        <div class="control-group">
                          <div class="controls span9">
                            <input type="checkbox" name="featured" id="featured" value="1" />
                            <label style="float: right; margin-right: 60px; margin-top: 4px;">Featured </label>
                          </div>
                        </div>
                    </div>  
                    <div class="span6 mar-lno" style="float:left; margin-top:15px;">
                      <div class="control-group">
                        <div class="controls">
                          <input type="checkbox" name="store_of_week" id="store_of_week" value="1" style="margin-left: -10px;" />
                          <label style="float: right; margin-right: 50px; margin-top: 4px;">Retailer Of The Week </label>
                        </div>
                      </div>
                    </div>
                    <div class="span12 mar-lno">
                      <label class="control-label span12">Sidebar URL<span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="sidebar_image_url" rows="5" id="sidebar_image_url" >
                          </div>
                        </div>
                      </div>
                    </div>    
                    <!-- New code for Turn and go to shop button extra param url settings 30-7-16 -->
                    <div class="span12" style="float:right;margin-top:15px;">Settings for parameteres for "Turn and Go to Shop Button"</div>
                    <div class="span6 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 78% !important;">Tracking parameter<span class="required_field">*</span></label>
                      <div class="span10 mar-lno" style="width: 190% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span6" name="tracking_param" id="tracking_param" required />
                          </div>
                        </div> 
                      </div>
                    </div>
                    <div class="span6 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 240px !important;">Extra Tracking Parameter</label>
                      <div class="span12 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="ex_tracking_param" id="ex_tracking_param" > 
                          </div>
                        </div>
                      </div>
                    </div>    
                    <!-- End -->
                    <!-- New field for coupon page tracking and extra parameter 12-10-16 -->
                    <div class="span12" style="float:right;margin-top:15px;">Settings for standard parameteres to appear in Coupon page</div>
                    <div class="span6 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 78% !important;">Tracking parameter<span class="required_field">*</span></label>
                      <div class="span12 mar-lno" style="width: 190% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span6" name="coupon_tracking_param" id="tracking_param" required />
                          </div>
                        </div> 
                      </div>
                    </div> 
                    <div class="span5 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 240px !important;">Extra Tracking Parameter</label>
                      <div class="span10 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="coupon_ex_tracking_param" id="ex_tracking_param" > 
                          </div>
                        </div>
                      </div>
                    </div>    
                    <!-- End -->
                    <div class="span6 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 78% !important;">Retailer Logo </label>
                      <div class="span12 mar-lno" style="width: 200% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="file" class="span6" name="affiliate_logo" id="affiliate_logo" />
                          </div>
                        </div>  
                      </div>
                      <span style="float: right;width:200px;">Note: Logo size (150 * 93) </span>
                    </div>  
                    <!--Pilaventhiran 03/05/2016 START-->
                    <div class="span5 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 240px !important;">Cover Photo <span class="required_field">*</span></label>
                      <div class="span10 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="file" class="span12" name="cover_photo" id="cover_photo" />
                          </div>
                        </div>  
                      </div>
                      <span style="float:left;width:200px;">Note: Cover Photo size (150 * 93)</span>
                    </div>  
                    <!--Pilaventhiran 03/05/2016 END-->
                    <div class="span6 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 81% !important;">Retailer Banner Images <span class="required_field">*</span></label>
                      <div class="span12 mar-lno" style="width: 200% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="file" class="span6" name="coupon_image[]" id="coupon_image[]" multiple class="span6">
                          </div>
                        </div>
                      </div>  
                      <span style="float: right;width:200px;">Note: image size (840 * 280)</span>
                    </div>
                    <div class="span5 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 240px !important;">Sidebar Image <span class="required_field">*</span></label>
                      <div class="span12 mar-lno" style=" width: 240px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="file" class="span12" name="sidebar_image" id="sidebar_image"/><br>
                          </div>
                        </div>
                      </div>  
                      <span style="float:left;width:200px;">Note: image size (180 * 35) </span>
                    </div>
                    <div class="span12 mar-lno">
                      <label class="control-label span12">Retailer Category <span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <?php
                          $category_list =  $this->admin_model->categories();
                          ?>
                          <div class="controls">
                            <div class="span12" id="tree_width" style="">
                              <ul id="tree" class="checktree-root">
                                <?php
                                foreach($category_list as $cate)
                                {
                                  ?>
                                  <li>
                                    <label>
                                    <input  name="categorys_list[]" value="<?php echo $cate->category_id;?>" type="checkbox"><?php echo $cate->category_name;?></label>
                                    <?php
                                    $cateid = $cate->category_id;
                                    $sub_category_list =  $this->admin_model->sub_categories($cateid);
                                    if($sub_category_list)
                                    {
                                      foreach($sub_category_list as $subcate)
                                      {
                                        ?>
                                        <ul>                                            
                                          <li>
                                            <label><input type="checkbox" name="size_<?php echo $cateid;?>[]" value="<?php echo $subcate->sun_category_id;?>"><?php echo $subcate->sub_category_name;?></label>
                                          </li>                       
                                        </ul>
                                        <?php
                                      }
                                    }
                                    ?>
                                  </li>
                                  <?php
                                }
                                ?>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br>

                    <div class="span12"><b>Program Id's In each Affiliate Network</b></div>
                    <div class="span6 mar-lno" style="margin-right:14px;">
                      <label class="control-label span12" style="width: 240px !important;">Zanox</label>
                      <div class="span10 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="zanox_pgm_id" id="zanox_pgm_id" value=""> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="span5 mar-lno" style="">
                      <label class="control-label span12" style="width: 240px !important;">CityAds</label>
                      <div class="span10 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="cityads_pgm_id" id="cityads_pgm_id" value=""> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="span6 mar-lno" style=" margin-right:14px;">
                      <label class="control-label span12" style="width: 240px !important;">Lomadee</label>
                      <div class="span10 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="lamadee_pgm_id" id="lamadee_pgm_id" value=""> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="span5 mar-lno">
                      <label class="control-label span12" style="width: 240px !important;">Rakuten</label>
                      <div class="span10 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="rakuten_pgm_id" id="rakuten_pgm_id" value=""> 
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="span5 mar-lno">
                      <label class="control-label span12" style="width: 240px !important;">Afilio</label>
                      <div class="span10 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="afilio_pgm_id" id="afilio_pgm_id" value=""> 
                          </div>
                        </div>
                      </div>
                    </div>



                    <div class="span12 mar-lno">
                      <label class="control-label span12">Retailer Status</label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <select name="affiliate_status" class="span12">
                              <option value="1">Active</option>
                              <option value="0">De active</option>
                            </select>
                          </div>
                        </div>
                      </div>    
                    </div> 
                  </div>       

                  <!--Pilaventhiran 03/05/2016 Start-->
                  <div class="span7">

                    <div class="span12 mar-lno">
                      <label class="control-label span12" style="width: 190px !important;">Notification on Desktop(Store)</label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <textarea class="span12 ckeditor" name="notify_desk" id="notify_desk" ></textarea>
                          </div>
                        </div>
                      </div>
                    </div>    
                    <!--Pilaventhiran 13/05/2016 START-->
                    <div class="span12 mar-lno">
                      <label class="control-label span12" style="width: 290px !important;">Notification on Mobile/APP(Store)</label>
                      <div class="span12 mar-lno">
                        <div class="control-group notify">
                          <div class="controls">
                            <textarea class="span12 ckeditor" name="notify_mobile" id="notify_mobile" ></textarea>
                          </div>
                        </div>
                      </div>
                    </div>    
                    <!--Pilaventhiran 13/05/2016 END-->
                    <div class="span12 mar-lno">
                      <label class="control-label span12" style="width: 190px !important;">Redirect Page Notification</label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <textarea class="span12 ckeditor" name="redir_notify" id="redir_notify" ></textarea>
                          </div>
                        </div>
                      </div>    
                    </div>
                    <div class="span12 mar-lno">
                      <label class="control-label span12" style="width: 190px !important;">Retailer Description <span class="required_field">*</span></label>  
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <textarea class="span12 ckeditor" name="affiliate_desc" id="affiliate_desc">
                              <?php 
                              if($sess_affiliate_desc!="")
                              {
                                echo $sess_affiliate_desc;
                              }               
                              ?>
                            </textarea>
                          </div>
                        </div>
                      </div>
                    </div>    
                    <div class="span12 mar-lno">
                      <label class="control-label span12" style="width: 190px !important;">How to Get this Offer<span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <textarea class="span12 ckeditor" name="how_to_get_this_offer" id="how_to_get_this_offer"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>    
                    <div class="span12 mar-lno">
                      <label class="control-label span12" style="width: 190px !important;">Terms & Conditions  <span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <textarea class="span12 ckeditor" name="terms_and_conditions" id="terms_and_conditions"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>    
                    <!-- New code related store bellow details 1-6-16.-->
                    <div class="span12 mar-lno">
                      <label class="control-label span12" style="width: 190px !important;">Related stores details</label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <textarea class="span12 ckeditor" name="related_details" id="related_details" ></textarea>
                          </div>
                        </div>
                      </div>
                    </div>    
                    <!-- End -->
                  </div>                          
						      <!-- Added Section 28/11/14 -->
						      <div class="form-actions">
                    <input type="submit" name="save" value="Submit" class="btn btn-success">
                  </div>
						      <?php echo form_close(); ?>
                  <!--</form>-->
                  <!-- END FORM-->
                </div>
					      <?php
              }
              ?>
					    <?php
						  if($action=="edit")
              {
						    ?>
                <div class="widget-body form">
  					      <?php
                  $error = $this->session->flashdata('error');
  					      if($error!="") 
                  {
        						echo '<div class="alert alert-error">
        						<button data-dismiss="alert" class="close">x</button>
        						<strong>Error! </strong>'.$error.'</div>';
        					}  
          				$success = $this->session->flashdata('success');
          				if($success!="") 
                  {
          					echo '<div class="alert alert-success">
          					<button data-dismiss="alert" class="close">x</button>
          					<strong>Success! </strong>'.$success.'</div>';
          				}
                  ?>
                  <!-- BEGIN FORM-->
  						    <?php
  							  $attribute = array('role'=>'form','name'=>'faq','method'=>'post','id'=>'update_form','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
  							  echo form_open('adminsettings/updateaffiliate',$attribute);
  						    ?>
                  
                  <div class="span5">

                    <div class="span12 mar-lno">
                      <label class="control-label span12">Retailer Name <span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="affiliate_name" id="affiliate_name" value="<?php echo $affiliate_name; ?>" required />
                          </div>
                        </div>
                      </div>
                    </div> 
                    <div class="span12 mar-lno">
                      <label class="control-label span12">Retailer URL<span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <input type="url" class="span12" name="logo_url" id="logo_url" value="<?php echo $logo_url;?>" required />
                          </div>
                        </div>  
                      </div>
                    </div>
                    
                    <!-- New code for Add a affiliate url details 30-11-16 -->
                     <div class="span12 mar-lno">
                      <label class="control-label span12">Retailer URL slug<span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="url_slug" id="url_slug" value="<?php echo $url_slug;?>" required />
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End 30-11-16-->

                    <div class="span12 mar-lno">
                      <label class="control-label span12">Meta Keyword <span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="meta_keyword" id="meta_keyword" value="<?php echo $meta_keyword; ?>" required />
                          </div>
                        </div>
                      </div>  
                    </div>
                    <div class="span12 mar-lno">
                      <label class="control-label span12">Meta Description <span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="meta_description" id="meta_description" required value="<?php echo $meta_description; ?>"> 
                          </div>
                        </div>
                      </div>  
                    </div>      
                    <div class="span12 mar-lno">
                      <label class="control-label span12">Cashback Type</label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <select name="affiliate_cashback_type" class="span12">
                              <option>Select</option>
                              <option <?php if($affiliate_cashback_type=='Percentage'){ echo 'selected="selected"'; } ?> value="Percentage">Percentage</option>
                              <option <?php if($affiliate_cashback_type=='Flat'){ echo 'selected="selected"'; } ?> value="Flat">Flat</option>
                            </select>
                          </div>
                        </div>
                      </div>  
                    </div>
                    <div class="span6 mar-lno">
                      <label class="control-label span12">Cashback for user </label>
                      <div class="span12 mar-lno" style="width: 140% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span6" name="cashback_percentage" id="cashback_percentage" value="<?php echo $cashback_percentage; ?>" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--Pilaventhiran 03/05/2016 START-->
                    <div class="span6 mar-lno">
                      <label class="control-label span12" style="width: 240px !important; margin-left: -30px;">Cashback for user in the Android App</label>
                      <div class="span12 mar-lno" style="margin-left: -30px !important; width: 285px !important;">  
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span10" name="cashback_percent_android" id="cashback_percent_android" value="<?= $cashback_percent_android;?>"/>  
                          </div>
                        </div>
                      </div>  
                    </div>
                    <div class="span6 mar-lno">
                      <label class="control-label span12" style="width: 88% !important;">Content Extra tracking web</label>
                      <div class="span12 mar-lno" style="width: 140% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span6" name="content_extra_param" id="content_extra_param" value="<?php echo $content_extra_param; ?>">
                          </div>
                        </div>
                      </div>    
                    </div>
                    <div class="span6 mar-lno">
                      <label class="control-label span12" style="width: 240px !important; margin-left: -20px; "> Content Extra tracking ANDROID</label>
                      <div class="span12 mar-lno" style="margin-left: -30px !important; width: 285px !important;">
                        <div class="control-group">
                            <div class="controls">
                              <input type="text" class="span10" name="content_extra_param_android" id="content_extra_param__android" value="<?php echo $content_extra_param_android;?>" >
                            </div>
                        </div>
                      </div>
                    </div>    
                    <div class="span6 mar-lno">
                      <label class="control-label span12" style="width: 88% !important;">Old Cashback</label>
                      <div class="span12 mar-lno" style="width: 140% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span6" name="old_cashback" id="old_cashback" value="<?php echo $old_cashback; ?>">
                          </div>
                        </div>
                      </div>
                    </div>    
                    <div class="span6 mar-lno">
                      <label class="control-label span12" style="width: 240px !important; margin-left: -30px;">CashB for user Android(content)</label>
                      <div class="span12 mar-lno" style="margin-left: -30px !important; width: 285px !important;">
                        <div class="control-group">
                            <div class="controls">
                              <input type="text" class="span10" name="cashback_content_android" id="cashback_content_android" value="<?= $cashback_content_android;?>">
                            </div>
                        </div>
                      </div>
                    </div>
                    <!--Pilaventhiran 03/05/2016 START-->
                    <div class="span6 mar-lno">
                      <label class="control-label span12" style="width: 92% !important;">Days store know about trans</label>
                      <div class="span12 mar-lno" style="width: 140% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span6" name="report_date" id="report_date" value="<?= $report_date;?>" required="" />
                          </div>
                        </div>
                      </div>  
                    </div>           
                    <!--Pilaventhiran 03/05/2016 END-->
                    <!-- new code for estimated days for store details 18-8-16 start -->
                    <div class="span5 mar-lno">
                      <label class="control-label span12" style="width: 240px !important; margin-left: -10px;">Estimated Payment</label>
                      <div class="span12 mar-lno" style="margin-left: -10px !important; width: 260px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span10" name="retailer_ban_url" id="retailer_ban_url" value="<?php echo $retailer_ban_url; ?>"> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- end 18-8-16 -->   
                    <div class="span6 mar-lno" style="margin-top:15px;">
                        <div class="control-group">
                          <div class="controls span9">
                            <input type="checkbox" name="featured" id="featured" value="1" <?php if($featured=="1"){ echo 'checked="checked"';} ?>/>
                            <label style="float: right; margin-right: 60px; margin-top: 4px;">Featured </label>
                          </div>
                        </div>
                    </div>  
                    <div class="span6 mar-lno" style="float:left; margin-top:15px;">
                      <div class="control-group">
                        <div class="controls">
                          <input type="checkbox" name="store_of_week" id="store_of_week" value="1" style="margin-left: -10px;" <?php if($store_of_week=="1"){ echo 'checked="checked"';} ?>/>
                          <label style="float: right; margin-right: 50px; margin-top: 4px;">Retailer Of The Week </label>
                        </div>
                      </div>
                    </div>

                    <div class="span12 mar-lno">
                      <label class="control-label span12">Sidebar URL<span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="sidebar_image_url" rows="5" id="sidebar_image_url" value="<?php echo $sidebar_image_url; ?>">
                          </div>
                        </div>
                      </div>
                    </div>

                    <!-- New code for Turn and go to shop button extra param url settings 30-7-16 -->
                    <div class="span12" style="float:right;margin-top:15px;">Settings for parameteres for "Turn and Go to Shop Button"</div>
                    <div class="span6 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 78% !important;">Tracking parameter<span class="required_field">*</span></label>
                      <div class="span10 mar-lno" style="width: 190% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span6" name="tracking_param" id="tracking_param" value="<?php echo $tracking_param; ?>" required />
                          </div>
                        </div> 
                      </div>
                    </div>
                    <div class="span6 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 240px !important;">Extra Tracking Parameter</label>
                      <div class="span12 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="ex_tracking_param" id="ex_tracking_param" value="<?php echo $ex_tracking_param;?>"> 
                          </div>
                        </div>
                      </div>
                    </div>    
                    <!-- End -->  

                    <!-- New field for coupon page tracking and extra parameter 12-10-16 -->
                    <div class="span12" style="float:right;margin-top:15px;">Settings for standard parameteres to appear in Coupon page</div>
                    <div class="span6 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 78% !important;">Tracking parameter<span class="required_field">*</span></label>
                      <div class="span12 mar-lno" style="width: 190% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span6" name="coupon_tracking_param" id="tracking_param" required value="<?php echo $coupon_track_param;?>" />
                          </div>
                        </div> 
                      </div>
                    </div> 
                    <div class="span5 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 240px !important;">Extra Tracking Parameter</label>
                      <div class="span10 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="coupon_ex_tracking_param" id="ex_tracking_param" value="<?php echo $coupon_ex_track_param;?>"> 
                          </div>
                        </div>
                      </div>
                    </div>    
                    <!-- End -->

                    <div class="span6 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 78% !important;">Retailer Logo </label>
                      <div class="span12 mar-lno" style="width: 200% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="file" class="span6" name="affiliate_logo" id="affiliate_logo" /><br>
                            <img src="<?php echo $this->admin_model->get_img_url();?>uploads/affiliates/<?php echo $affiliate_logo; ?>" width="180" height="35">
                          </div>
                        </div>  
                      </div>
                      <span style="float: right;width:200px;">Note: Logo size (150 * 93) </span>
                    </div>  
                    <!--Pilaventhiran 03/05/2016 START-->
                    <div class="span5 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 240px !important;">Cover Photo <span class="required_field">*</span></label>
                      <div class="span10 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="file" class="span12" name="cover_photo" id="cover_photo" />
                            <br>
                            <img src="<?php echo $this->admin_model->get_img_url();?>uploads/affiliates/<?php echo $cover_photo; ?>" width="180" height="35">
                          </div>
                        </div>  
                      </div>
                      <span style="float:left;width:200px;">Note: Cover Photo size (150 * 93)</span>
                    </div>  
                    <!--Pilaventhiran 03/05/2016 END-->
                    <div class="span6 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 81% !important;">Retailer Banner Images <span class="required_field">*</span></label>
                      <div class="span12 mar-lno" style="width: 200% !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="file" class="span6" name="coupon_image[]" id="coupon_image[]" multiple class="span6">
                            <br> 
                            <?php
                            $coupon_image = explode(',',$coupon_image);
                            if($coupon_image[0])
                            {
                              foreach($coupon_image as $get)
                              {
                                ?>
                                <img src="<?php echo $this->admin_model->get_img_url(); ?>uploads/store_banner/<?php echo $get; ?>" width="185" height="35">
                                <?php
                              }
                            }
                            $coupon_image = implode(',',$coupon_image);
                            ?>
                          </div>
                        </div>
                      </div>  
                      <span style="float: right;width:200px;">Note: image size (840 * 280)</span>
                    </div>

                    <div class="span5 mar-lno" style="margin-top:15px;">
                      <label class="control-label span12" style="width: 240px !important;">Sidebar Image <span class="required_field">*</span></label>
                      <div class="span12 mar-lno" style=" width: 240px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="file" class="span12" name="sidebar_image" id="sidebar_image"/><br>
                            <?php
                            if($sidebar_image)
                            {
                              ?>
                              <img src="<?php echo $this->admin_model->get_img_url();?>uploads/sidebar_image/<?php echo $sidebar_image; ?>" width="180" height="35">
                              <?php
                            }
                            ?>
                          </div>
                        </div>
                      </div>  
                      <span style="float:left;width:200px;">Note: image size (180 * 35) </span>
                    </div>

                    <div class="span12 mar-lno" style="margin-bottom:20px;">
                      <label class="control-label span12">Retailer Category <span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <?php
                          $category_list =  $this->admin_model->categories();
                          ?>
                          <div class="controls">
                            <div class="span12" id="tree_width" style="">
                              <ul id="tree" class="checktree-root">
                                <?php
                                $sel_category = explode(",",$store_categorys);
                                foreach($category_list as $cate)
                                {
                                  $subcatelevelid = '';
                                  ?>
                                  <li>
                                    <label>
                                    <input name="categorys_list[]" value="<?php echo $cate->category_id;?>" <?php if(in_array($cate->category_id,$sel_category)){ echo 'checked="checked"';} ?> type="checkbox"><?php echo $cate->category_name;?></label>
                                    <?php
                                    $cateid = $cate->category_id;
                                    //get sub levels
                                    $affiliate_category_list =  $this->admin_model->get_updated_categories($affiliate_id,$cateid);
                                    //convert into array
                                    if($affiliate_category_list)
                                    {
                                      foreach($affiliate_category_list as $sublevel)
                                      {
                                        $subcatelevelid[] =  $sublevel->sub_category_id;
                                      }
                                    }
                                    $sub_category_list =  $this->admin_model->sub_categories($cateid);
                                    if($sub_category_list)
                                    {
                                      foreach($sub_category_list as $subcate)
                                      {
                                        ?>
                                        <ul>                                            
                                          <li>
                                            <label><input type="checkbox" name="size_<?php echo $cateid;?>[]" <?php if($subcatelevelid!='') {if(in_array($subcate->sun_category_id,$subcatelevelid)){ echo 'checked="checked"';}} ?>  value="<?php echo $subcate->sun_category_id;?>"><?php echo $subcate->sub_category_name;?></label>
                                          </li>                       
                                        </ul>
                                        <?php
                                      }
                                    }
                                    ?>
                                  </li>
                                  <?php
                                }
                                ?>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br>
                     <br><br>
                    <div class="span12"><b>Program Id's In each Affiliate Network</b></div>
                    <div class="span6 mar-lno" style="margin-right:14px;">
                      <label class="control-label span12" style="width: 240px !important;">Zanox</label>
                      <div class="span10 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="zanox_pgm_id" id="zanox_pgm_id" value="<?php echo $zanox_pgm_id;?>"> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="span5 mar-lno" style="">
                      <label class="control-label span12" style="width: 240px !important;">CityAds</label>
                      <div class="span10 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="cityads_pgm_id" id="cityads_pgm_id" value="<?php echo $cityads_pgm_id;?>"> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="span6 mar-lno" style=" margin-right:14px;">
                      <label class="control-label span12" style="width: 240px !important;">Lomadee</label>
                      <div class="span10 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="lamadee_pgm_id" id="lamadee_pgm_id" value="<?php echo $lamadee_pgm_id;?>"> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="span5 mar-lno">
                      <label class="control-label span12" style="width: 240px !important;">Rakuten</label>
                      <div class="span10 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="rakuten_pgm_id" id="rakuten_pgm_id" value="<?php echo $rakuten_pgm_id;?>"> 
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="span5 mar-lno">
                      <label class="control-label span12" style="width: 240px !important;">Afilio</label>
                      <div class="span10 mar-lno" style="width: 206px !important;">
                        <div class="control-group">
                          <div class="controls">
                            <input type="text" class="span12" name="afilio_pgm_id" id="afilio_pgm_id" value="<?php echo $afilio_pgm_id;?>"> 
                          </div>
                        </div>
                      </div>
                    </div>



                    <div class="span12 mar-lno">
                      <label class="control-label span12">Retailer Status</label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <select name="affiliate_status" class="span12">
                              <option value="1" <?php if($affiliate_status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
                              <option value="0" <?php if($affiliate_status=='0'){ echo 'selected="selected"'; } ?>>De active</option>
                            </select>
                          </div>
                        </div>
                      </div>    
                    </div>  
                  </div> 

                  <div class="span7">
                    <div class="span12 mar-lno">
                      <label class="control-label span12" style="width: 190px !important;">Notification on Desktop(Store)</label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <textarea class="span12 ckeditor" name="notify_desk" id="notify_desk" ><?php echo $notify_desk; ?></textarea>
                          </div>
                        </div>
                      </div>
                    </div>    
                    <!--Pilaventhiran 13/05/2016 START-->
                    <div class="span12 mar-lno">
                      <label class="control-label span12" style="width: 290px !important;">Notification on Mobile/APP(Store)</label>
                      <div class="span12 mar-lno">
                        <div class="control-group notify">
                          <div class="controls">
                            <textarea class="span12 ckeditor" name="notify_mobile" id="notify_mobile" ><?php echo $notify_mobile; ?></textarea>
                          </div>
                        </div>
                      </div>
                    </div>    
                    <!--Pilaventhiran 13/05/2016 END-->
                    <div class="span12 mar-lno">
                      <label class="control-label span12" style="width: 190px !important;">Redirect Page Notification</label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <textarea class="span12 ckeditor" name="redir_notify" id="redir_notify" ><?php echo $redir_notify; ?></textarea>
                          </div>
                        </div>
                      </div>    
                    </div>
                    <div class="span12 mar-lno">
                      <label class="control-label span12" style="width: 190px !important;">Retailer Description <span class="required_field">*</span></label>  
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <textarea class="span12 ckeditor" name="affiliate_desc" id="affiliate_desc">
                              <?php echo $affiliate_desc; ?>
                            </textarea>
                          </div>
                        </div>
                      </div>
                    </div>    
                    <div class="span12 mar-lno">
                      <label class="control-label span12" style="width: 190px !important;">How to Get this Offer<span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <textarea class="span12 ckeditor" name="how_to_get_this_offer" id="how_to_get_this_offer"><?php echo $how_to_get_this_offer; ?></textarea>
                          </div>
                        </div>
                      </div>
                    </div>    
                    <div class="span12 mar-lno">
                      <label class="control-label span12" style="width: 190px !important;">Terms & Conditions  <span class="required_field">*</span></label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <textarea class="span12 ckeditor" name="terms_and_conditions" id="terms_and_conditions"><?php echo $terms_and_conditions; ?></textarea>
                          </div>
                        </div>
                      </div>
                    </div>    
                    <!-- New code related store bellow details 1-6-16.-->
                    <div class="span12 mar-lno">
                      <label class="control-label span12" style="width: 190px !important;">Related stores details</label>
                      <div class="span12 mar-lno">
                        <div class="control-group">
                          <div class="controls">
                            <textarea class="span12 ckeditor" name="related_details" id="related_details" ><?php echo $related_details; ?></textarea>
                          </div>
                        </div>
                      </div>
                    </div>    
                    <!-- End -->
                  </div>             
 
    						  <input type="hidden" name="affiliate_id" id="affiliate_id" value="<?php echo $affiliate_id; ?>">
    						  <input type="hidden" name="hidden_img" id="hidden_img" value="<?php echo $affiliate_logo; ?>">
                  <input type="hidden" name="hidden__cover_photo" id="hidden__cover_photo" value="<?php echo $cover_photo; ?>">
						      <input type="hidden" name="sidebar_image_hid" id="sidebar_image_hid" value="<?php echo $sidebar_image; ?>">
                  <input type="hidden" name="hidden_coupon_image" id="hidden_coupon_image" value="<?php echo $coupon_image; ?>">
						    
                  <!--
                  <div class="control-group">
                    <label class="control-label">Name of the store Extra tracking Paramenter</label>
                    div class="controls">
                      <textarea class="span6" name="name_extra_param" id="name_extra_param" ><?php echo $name_extra_param; ?></textarea>
                    </div>
                  </div>
                  -->         
						      <div class="form-actions">
                    <input type="submit" name="save" value="Save Changes" class="btn btn-success">
                  </div>
						      <?php echo form_close(); ?>
                </div>
					      <?php
              }
              ?>
            </div>
            <!-- END SAMPLE FORM widget-->
          </div>
        </div>
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->  
  </div>
   
   
    
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
  <!-- END FOOTER -->
  <!-- BEGIN JAVASCRIPTS -->
  <!-- Load javascripts at bottom, this will reduce page load time -->
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
	<script>
    $(document).ready(function() 
    {       
      // initiate layout and plugins
      App.init();
    });
  </script>
  <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/css/jquery-checktree.css" rel="stylesheet" type="text/css">
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery.min.js"></script> 
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-checktree.js"></script> 
  <script>
  $('#tree').checktree();
  </script>
  <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>