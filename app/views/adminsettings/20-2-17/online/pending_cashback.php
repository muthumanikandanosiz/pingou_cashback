<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Pending Cashback | <?php echo $admin_details->site_name; ?> Admin</title>
  <?php $this->load->view('adminsettings/script'); ?>
  <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <style>
    div.selector, div.selector span, div.checker span, div.radio span, div.uploader, div.uploader span.action, div.button, div.button span
    {
      background-image:none
    }
    .col-xs-6 {width: 47% !important; float: left;min-height:1px;padding-left: 12px;padding-right: 12px;position: relative;}
    select.input-sm { width:60px !important;}
    .row {margin-left: -12px;margin-right: -12px;}
    .pagination {border-radius: 4px;margin: 20px 0;padding-left: 0;}
  </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN THEME CUSTOMIZER-->
            <!--<div id="theme-change" class="hidden-phone">
              <i class="icon-cogs"></i>
              <span class="settings">
                <span class="text">Theme:</span>
                <span class="colors">
                  <span class="color-default" data-style="default"></span>
                  <span class="color-gray" data-style="gray"></span>
                  <span class="color-purple" data-style="purple"></span>
                  <span class="color-navy-blue" data-style="navy-blue"></span>
                </span>
              </span>
            </div>-->
            <!-- END THEME CUSTOMIZER-->
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <!-- <h3 class="page-title">
            Pending Cashback
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
                <span class="divider">&nbsp;</span>
              </li>
              <li><?php echo anchor('adminsettings/pending_cashback','Pending Cashback'); ?>
                <span class="divider-last">&nbsp;</span>
              </li>
            </ul> -->
            <span style="float:left; margin-top: 15px;">
              <a href="<?php echo base_url();?>adminsettings/report_update/pending_cashback" class="btn btn-success">Update Cashbacks CSV</a> &nbsp;
            </span>
            <!-- END PAGE TITLE & BREADCRUMB-->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <!-- BEGIN ADVANCED TABLE widget-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN EXAMPLE TABLE widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-money"></i> Pending Cashback</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                </span>
              </div>
              <div class="widget-body">
                <?php 
                $error = $this->session->flashdata('error');
                if($error!="")
                {
                  echo '<div class="alert alert-error">
                  <button data-dismiss="alert" class="close">x</button>
                  <strong>Error! </strong>'.$error.'</div>';
                }
                
                $success = $this->session->flashdata('success');
                if($success!="") 
                {
                  echo '<div class="alert alert-success">
                  <button data-dismiss="alert" class="close">x</button>
                  <strong>Success! </strong>'.$success.'</div>';      
                }
                ?>
                <form id="form2" action="" method="post" name="form2">
                  <table class="table table-striped table-bordered" id="sample_teste1">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th style=""><center><input type="checkbox" id="check_b" class="check_b grpall"  name="chk[]" /></center></th> <!-- onchange="checkAll(this)" -->
                        <th><center>User Id</center></th>
                        <th><center>User Name</center></th>
                        <th><center>User Email</center></th>
                        <th><center>Store</center></th>
                        <th><center>Transaction Amount</center></th>
                        <th><center>Cashback</center></th>
                        <th><center>Date</center></th>
                        <th><center>Approve</center></th>
                        <th><center>Cancel</center></th>
                        <th><center>Report Update Id</center></th>
                        <th><center>Referral</center></th>
                        <th><center>Txn Id</center></th>
                        <th><center>New Txn Id</center></th>
                        <th><center>Delete</center></th>
                      </tr>
                      </thead>
                    </table>

                    <input type="hidden" name="hidd" value="hidd"><!-- seetha-->
                    <input id="" class="btn btn-warning" type="submit" value="Delete Cashback" name="deletecashback">
                    <input id="" class="btn btn-success" type="submit" value="Approve Cashback" name="approvecashback">
                    <input id="" class="btn btn-danger" type="submit" value="Cancel Cashback" name="cancelcashback">
                  </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>
            <!-- END ADVANCED TABLE widget-->
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
   <?php $this->load->view('adminsettings/footer'); ?>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>   
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->   
   <!--<script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>

   <script>
      jQuery(document).ready(function() {
         // initiate layout and plugins
         App.init();
      });
   </script>
   <script type="text/javascript">
function confirmDelete1()  // Confirm before delete cashback..
{
  var m = "Do you want to Approve this cashback?"; 
  if(!confirm(m))
  {
    return false;
  }
  else
  {
    return true;
  }
}
function confirmDelete2()  // Confirm before delete cashback..
{
  var m = "Do you want to Cancel this cashback?"; 
  if(!confirm(m))
  {
    return false;
  }
  else
  {
    return true;
  }
}
function confirmDelete3()  // Confirm before delete cashback..
{
  var m = "Do you want to delete this cashback detail?"; 
  if(!confirm(m))
  {
    return false;
  }
  else
  {
    return true;
  }
}

</script>
<!-- seetha-->
<script>
    $(document).ready(function() {
    $(".check_b").attr("style", "opacity: 1;");
    });
  /*function checkAll(ele) {    //multiple delete cashback
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }*/

  $('input.grpall').click(function(){

  if($(this).is(':checked') == true)
  {
    $(this).attr('checked','checked');
    $('input.chksingle').each(function(){
    $(this).attr('checked','checked');
    }) 
  }
  else
  {
    $(this).removeAttr('checked');
    $('input.chksingle').each(function(){
    $(this).removeAttr('checked');
    }) 
  }
  //return false;
 });

$(document).ready(function() {
$('#sample_teste1').DataTable( {
"processing": true,
"serverSide": true,
"columnDefs": [{
"targets": 1,
"orderable": false
}],
"ajax": {
"url": "<?php echo site_url('adminsettings/newpending_cashback')?>",
"data": {
//"totalrecords": "<?php echo $iTotal; ?>"
}
}

});
});
</script>
</body>
<!-- END BODY -->
</html>
<style type="text/css">
 .dataTables_processing
{
  display:none !important;
}
</style>
<link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/css/pagination.css" rel="stylesheet" />
