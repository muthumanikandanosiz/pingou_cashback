<!DOCTYPE html>
<html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<?php $admin_details = $this->admin_model->get_admindetails(); ?>
	<title>Members | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/fancybox/source/jquery.fancybox.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/uniform/css/uniform.default.css" />
  <style>
    #sample_1 th
    {
	   text-align:center !important;
	  }
  </style>
  <style>
    div.selector, div.selector span, div.checker span, div.radio span, div.uploader, div.uploader span.action, div.button, div.button span
    {background-image:none;}
    .col-xs-6 {width: 47% !important; float: left;min-height:1px;padding-left: 12px;padding-right: 12px;position: relative;}
    select.input-sm { width:60px !important;}
    .row {margin-left: -12px;margin-right: -12px;}
    .pagination {border-radius: 4px;margin: 20px 0;padding-left: 0;}
  </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
  <!-- BEGIN HEADER -->
  <?php $this->load->view('adminsettings/header'); ?>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <!-- <h3 class="page-title">
            Members
            </h3> -->
            <!-- <ul class="breadcrumb">
              <li>
                <?php echo anchor('adminsettings/dashboard','<i class="icon-home"></i>'); ?>
  	  				  <span class="divider">&nbsp;</span>
              </li>
              <li><?php echo anchor('adminsettings/users','Members'); ?>
							  <span class="divider-last">&nbsp;</span>
					    </li>
            </ul> -->
            <span style="float:left; margin-top: 15px;">
              <a href="<?php echo base_url();?>adminsettings/click_history" class="btn btn-success">Click History</a> &nbsp;
              <a class="btn btn-success" href="<?php echo base_url();?>adminsettings/download_users">Export Users</a> &nbsp;
              <a class="btn btn-success" href="<?php echo base_url();?>adminsettings/download_users/csv">Export Users CSV</a> 
            </span>
            <!-- END PAGE TITLE & BREADCRUMB-->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <!-- BEGIN ADVANCED TABLE widget-->
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN EXAMPLE TABLE widget-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i> Members</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                </span>
              </div>
              <div class="widget-body">
  						  <?php 
  					    $error = $this->session->flashdata('error');
      					if($error!="") 
                {
      						echo '<div class="alert alert-error">
      						<button data-dismiss="alert" class="close">x</button>
      						<strong>Error! </strong>'.$error.'</div>';
      					}
      				  $success = $this->session->flashdata('success');
      				  if($success!="") 
                {
      					  echo '<div class="alert alert-success">
      						<button data-dismiss="alert" class="close">x</button>
      						<strong>Success! </strong>'.$success.'</div>';			
      				  } 
      				  ?>
                <form id="form2" name="form2" method="post" action="">
                  <table class="table table-striped table-bordered" id="sample_teste1">
                    <thead>
                      <tr>
                        <th><input type="checkbox" id="check_b" class="check_b grpall" name="chk[]" /></th> <!-- onchange="checkAll(this)"  -->
                        <th>Id</th>
                        <th>Name (Status)</th>
                        <th>Email Address</th>
                        <th>Balance<!--  (in Rs.) --></th>
                        <th>C</th>
                        <th>N.Wd</th>
                        <th>Clicks</th>
                        <th class="hidden-phone">Contact No</th>
                        <th class="hidden-phone">IFSC Code</th>
                        <th class="hidden-phone">Ref</th>
                        <th class="hidden-phone">Cat</th>
                        <th class="hidden-phone">R_amt</th>
                        <th class="hidden-phone">App</th>
                        <th class="hidden-phone">U.B</th>
                        <th class="hidden-phone">News</th>
                        <th class="hidden-phone">R.Code</th>
                        <th class="hidden-phone">signup</th>
                        <th class="hidden-phone">D</th>
                      </tr>
                    </thead>
                  </table>
                  <input type="hidden" value="hidd" name="hidd">
    					    <input id="GoUpdate" class="btn btn-warning" type="submit" name="GoUpdate" value="Delete Users">
                </form>
              </div>
            </div>
            <!-- END EXAMPLE TABLE widget-->
          </div>
        </div>
        <!-- END ADVANCED TABLE widget-->
        <!-- END PAGE CONTENT-->
      </div>
      <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php $this->load->view('adminsettings/footer'); ?>
  <!-- END FOOTER -->
  <!-- BEGIN JAVASCRIPTS -->
  <!-- Load javascripts at bottom, this will reduce page load time -->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->   
  <!--   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>-->
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
  
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script> 

  <script>
    jQuery(document).ready(function() {
    // initiate layout and plugins
    App.init();
    });
  </script>
  <script type="text/javascript">
    function confirmDelete()  // Confirm before delete cms..
    {
    	var m = "Do you want to delete this User?";
      if(!confirm(m))
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }
  </script>
  <script>
    $(document).ready(function() 
    {
		  $(".check_b").attr("style", "opacity: 1;");
    });
	  /*function checkAll(ele) 
    {
      var checkboxes = document.getElementsByTagName('input');
      if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
      } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
    }*/

  $('input.grpall').click(function(){

  if($(this).is(':checked') == true)
  {
    $(this).attr('checked','checked');
    $('input.chksingle').each(function(){
    $(this).attr('checked','checked');
    }) 
  }
  else
  {
    $(this).removeAttr('checked');
    $('input.chksingle').each(function(){
    $(this).removeAttr('checked');
    }) 
  }
  //return false;
 });

$(document).ready(function() {
$('#sample_teste1').DataTable( {
"processing": true,
"serverSide": true,
"columnDefs": [{
"targets": 0,
"orderable": false
}],
"ajax": {
"url": "<?php echo site_url('adminsettings/newusers')?>",
"data": {
//"totalrecords": "<?php echo $iTotal; ?>"
}
}
});
});
  </script>
</body>
<!-- END BODY -->
</html>
<style type="text/css">
 .dataTables_processing
{
  display:none !important;
}
</style>
 <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/css/pagination.css" rel="stylesheet" />