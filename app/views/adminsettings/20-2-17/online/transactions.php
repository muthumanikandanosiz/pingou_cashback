<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Payments | <?php echo $admin_details->site_name; ?> Admin</title>
  <?php $this->load->view('adminsettings/script'); ?>
  <link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
  <style>
    div.selector, div.selector span, div.checker span, div.radio span, div.uploader, div.uploader span.action, div.button, div.button span
    {
      background-image:none
    }
    .col-xs-6 {width: 47% !important; float: left;min-height:1px;padding-left: 12px;padding-right: 12px;position: relative;}
    select.input-sm { width:60px !important;}
    .row {margin-left: -12px;margin-right: -12px;}
    .pagination {border-radius: 4px;margin: 20px 0;padding-left: 0;}
  </style>
</head>
<body class="fixed-top">
  <?php $this->load->view('adminsettings/header'); ?>
  <div id="container" class="row-fluid">
    <?php $this->load->view('adminsettings/sidebar'); ?>
    <div id="main-content">
      <div class="container-fluid">
        <div class="row-fluid">
          <div class="span12">
            <!-- BEGIN EXAMPLE TABLE widget-->
            <span style="float:left; margin-top: 15px;">
              <a href="<?php echo base_url();?>adminsettings/report_export/payment" class="btn btn-success">Export CSV</a> &nbsp;
              <a href="<?php echo base_url();?>adminsettings/report_update/payment" class="btn btn-success">Update CSV</a> &nbsp;
            </span>
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i> Payments</h4>
                <span class="tools">
                  <a href="javascript:;" class="icon-chevron-down"></a>
                </span>
              </div>
              <div class="widget-body">
                <?php 
                $error = $this->session->flashdata('error');
                if($error!="") 
                {
                  echo '<div class="alert alert-error">
                  <button data-dismiss="alert" class="close">x</button>
                  <strong>Error! </strong>'.$error.'</div>';
                }
                $success = $this->session->flashdata('success');
                if($success!="")
                {
                  echo '<div class="alert alert-success">
                  <button data-dismiss="alert" class="close">x</button>
                  <strong>Success! </strong>'.$success.'</div>';      
                }
                ?>
                <form id="form2" action="" method="post" name="form2">
                  <table class="table table-striped table-bordered" id="sample_teste1">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th style=""><center><input type="checkbox" id="check_b" class="check_b grpall"  name="chk[]" /></center></th> <!-- onchange="checkAll(this)" -->
                        <th><center>User ID</center></th>
                        <th><center>User Name</center></th>
                        <th><center>User Email</center></th>
                        <th><center>Payment Type</center></th>
                        <th><center>Amount</center></th>
                        <th><center>Transation Date</center></th>
                        <th><center>Transaction Date</center></th>
                        <th><center>Reference ID</center></th>
                        <th><center>Report Update Id</center></th>
                        <!-- <th><center>Table</center></th>
                        <th><center>Details Id</center></th> -->
                        <th><center>New Txn Id</center></th>
                        <th><center>Ref User tracking Id</center></th>
                        <th><center>Status</center></th>
                        <th><center>Edit</center></th>
                      </tr>
                    </thead>
                  </table>
                  <input type="hidden" name="hidd" value="hidd">
                  <input id="GoUpdate" class="btn btn-warning" type="submit" value="Delete Transactions" name="GoUpdate">
                </form>
              </div>
            </div>
            <!-- END EXAMPLE TABLE widget-->
          </div>
        </div>
      </div>
    </div>
    <!-- END PAGE -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php //$this->load->view('adminsettings/footer'); ?>
 
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>   
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
  <!-- ie8 fixes -->
  <!--[if lt IE 9]>
  <script src="js/excanvas.js"></script>
  <script src="js/respond.js"></script>
  <![endif]-->   
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.js"></script>
  <script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
  
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>

  <script>
    jQuery(document).ready(function() {
      // initiate layout and plugins
      App.init();
    });
  </script>
  <script type="text/javascript">
    function confirmDelete()  // Confirm before delete coupon..
    {
      if(!confirm(m))
      {
        return false;
      }
      else
      {
        return true;
      }
    }
  </script>
  <script>
    $(document).ready(function() {
    $(".check_b").attr("style", "opacity: 1;");
      });
    /*function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
    }*/

 $('input.grpall').click(function(){

  if($(this).is(':checked') == true)
  {
    $(this).attr('checked','checked');
    $('input.chksingle').each(function(){
    $(this).attr('checked','checked');
    }) 
  }
  else
  {
    $(this).removeAttr('checked');
    $('input.chksingle').each(function(){
    $(this).removeAttr('checked');
    }) 
  }
  //return false;
 });

$(document).ready(function() {
    
$('#sample_teste1').DataTable( {

"processing": true,
"serverSide": true,
"columnDefs": [{
"targets": 1,
"orderable": false
}],
"ajax": {
"url": "<?php echo site_url('adminsettings/newtransactions')?>",
"data": {
//"totalrecords": "<?php echo $iTotal; ?>"
}
}

});
});
  
  </script>
</body>
<!-- END BODY -->
</html>
<style type="text/css">
 .dataTables_processing
{
  display:none !important;
}
</style>
<link href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/css/pagination.css" rel="stylesheet" />
                            