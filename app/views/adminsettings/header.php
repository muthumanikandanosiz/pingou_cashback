<style>
.cls-titles
{
	color:#fff;
	font-size: 25px;
	display: block;
	font-weight: normal !important;
	margin: 8px 0px 0px 0px;
	text-transform: capitalize;
}
.error
{
	color:red;
}
.na-img > li > a > img 
{     
    height: 31px!important; 
   	max-width: none;
   	width: 39px!important;;
}
</style>
<div id="header" class="navbar navbar-inverse navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="navbar-inner">
		<div class="container-fluid">
			<!-- BEGIN LOGO -->
			<?php
				echo anchor('adminsettings/index','<img src="'.$this->admin_model->get_img_url().'assets/img/logo.png">',array('class' => 'brand'));
			?>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
			</a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<div id="top_menu" class="nav notify-row">
				<!-- BEGIN NOTIFICATION -->
				<ul class="nav top-menu">
					<!-- BEGIN SETTINGS -->
					<li class="dropdown">
					<?php
						echo anchor('adminsettings/settings','<i class="icon-cog"></i>',array('class'=>'dropdown-toggle element','data-placement'=>'bottom','data-toggle'=>'tooltip','data-original-title'=>'Settings'));
					?>
					</li>

					<li class="cls-titles"><center>
						<?php
						$pagename = $this->uri->segment(2);
						$arr_name = array('bulkcoupon'=>'Upload Coupon','addcoupon'=>'New Coupon','coupons'=>'Coupons','addaffiliate'=>'Retailer Details','affiliates'=>'Retailers','bulk_store'=>'Upload Retailers',
										 'site_affiliates'=>'Direct Affiliates','site_addaffiliate'=>'Direct Affiliates Details','click_history'=>'Click History','report_export'=>'Export Reports','reports'=>'Reports',
										 'users'=>'Members','report_upload'=>'Upload Reports','cashback'=>'Cashback','editcoupon'=>'Edit coupon details','editaffiliate'=>'Edit Affiliate details','view_report'=>'Report Details','view_user'=>'User Details',
										 'manual_credit'=>'Manual Credit Details','editcashback'=>'Cashback Details','report_update'=>'Update Reports',
										 'pending_cashback'=>'Pending Cashback','missing_cashback'=>'Missing Cashback','view_missing_cashback'=>'Missing Cashback',
										 'withdraw'=>'Withdraw','editwithdraw'=>'Edit withdraw details','transactions'=>'Payments','edittransactions'=>'Edit transaction details',
										 'cashback_exclusive'=>'Cashback Exclusive','add_cashback_exclusive'=>'Add cashback exclusive details','edit_cashback_exclusive'=>'Edit cashback exclusive details','layout'=>'Layout Settings',
										 'sales_funnel'=>'Sales Funnel Settings','popup'=>'Popup Settings','pretty_link'=>'Pretty Link Details','add_pretty_link'=>'Add pretty link details','edit_pretty_link'=>'Edit pretty link details',
										 'exit_popup'=>'Exit Popup settings','ads'=>'Ads management','editads'=>'Edit Ads Details','banners'=>'Banner details','editbanner'=>'Edit banner details',
										 'addbanner'=>'Add banner details','pending_referral'=>'Pending Referral','referrals'=>'Referral Details','upload_coupons'=>'Upload Premium Coupons','shoppingcoupons'=>'Premium Coupons','edit_shoppingcoupon'=>'Edit Premium coupon details',
										 'add_shoppingcoupon'=>'Add Premium coupon details','expired_coupons'=>'Expired Premium Coupons','orders'=>'Premium Coupon Orders','reviews'=>'Reviews','addcms'=>'Add CMS content','cms'=>'CMS contents',
										 'editcms'=>'Edit CMS content','settings'=>'Admin Settings','categories'=>'Categories','addcategory'=>'Add category Details','sub_categories'=>'Sub Category details','add_sub_category'=>'Add Sub Category details','editcategory'=>'Edit Category details',
										 'stores'=>'Add store category details','premium_categories'=>'Premium Categories','editpremiumcategory'=>'Edit premiumcategory details','upload_apireport'=>'Upload API report details','upload_apicoupons'=>'Upload API coupons details','user_information'=>'User information details','bank_details'=>'Bank details',
										 'edit_bankdetails'=>'Edit Bank details','add_bankdetails'=>'Add Bank details','sub_admin'=>'Sub admin details','amazon_s3_settings'=>'Amazon S3 Settings','payment_settings'=>'Admin Payment Settings','change_password'=>'Change Password','subscribers'=>'Subscribers',
										 'email_template'=>'Email Template','compose_newsletter'=>'Compose Newsletter','addfaqs'=>'Add FAQ details','faqs'=>'View FAQ','editfaq'=>'Edit FAQ details','dashboard'=>'Dashboard'
										 );

					 	echo $arr_name[$pagename];
					 	if(($this->uri->segment(2) == 'category') && ($this->uri->segment(3) == 'one'))
					 		echo "Referrals > Category One";
					 	if(($this->uri->segment(2) == 'category') && ($this->uri->segment(3) == 'two'))
					 		echo "Referrals > Category Two";
					 	if(($this->uri->segment(2) == 'category') && ($this->uri->segment(3) == 'three'))
					 		echo "Referrals > Category Three";
					 	if(($this->uri->segment(2) == 'category') && ($this->uri->segment(3) == 'four'))
					 		echo "Referrals > Category Four";
					 	if(($this->uri->segment(2) == 'category') && ($this->uri->segment(3) == 'five'))
					 		echo "Referrals > Category Five";
					 	if(($this->uri->segment(2) == 'category') && ($this->uri->segment(3) == 'six'))
					 		echo "Referrals > Category Six";
					 	if(($this->uri->segment(2) == 'category') && ($this->uri->segment(3) == 'seven'))
					 		echo "Referrals > Category Seven";
					 	if(($this->uri->segment(2) == 'category') && ($this->uri->segment(3) == 'eight'))
					 		echo "Referrals > Category Eight";
					 	if(($this->uri->segment(2) == 'category') && ($this->uri->segment(3) == 'nine'))
					 		echo "Referrals > Category Nine";
					 	if(($this->uri->segment(2) == 'category') && ($this->uri->segment(3) == 'ten'))
					 		echo "Referrals > Category Ten";

					 	if(($this->uri->segment(2) == 'affiliate_network') && ($this->uri->segment(3) == 'zanox'))
					 		echo "Zanox API";
					 	if(($this->uri->segment(2) == 'affiliate_network') && ($this->uri->segment(3) == 'cityads'))
					 		echo "Cityads API";
					 	if(($this->uri->segment(2) == 'affiliate_network') && ($this->uri->segment(3) == 'rakuten'))
					 		echo "Rakuten API";
					 	if(($this->uri->segment(2) == 'affiliate_network') && ($this->uri->segment(3) == 'afilio'))
					 		echo "Afilio API";
					 	if(($this->uri->segment(2) == 'affiliate_network') && ($this->uri->segment(3) == 'lomadee'))
					 		echo "Lomadee API";
					 	?>



					 	</center></li>
					<!-- END SETTINGS -->
					<!-- BEGIN NOTIFICATION DROPDOWN -->
					<!--<li class="dropdown" id="header_notification_bar">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-bell-alt"></i>
							<span class="badge badge-warning">1</span>
						</a>
						<ul class="dropdown-menu extended notification">
							<li>
								<p>You have 1 new notification</p>
							</li>
							<li>
								<a href="#">See all notifications</a>
							</li>
						</ul>
					</li>-->
					<!-- END NOTIFICATION DROPDOWN -->
				</ul>
			</div>
				<!-- END  NOTIFICATION -->
			<div class="top-nav ">
				<ul class="nav na-img pull-right top-menu" >
					<!-- BEGIN SUPPORT -->
					<!--<li class="dropdown mtop5">

						<a class="dropdown-toggle element" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="Chat">
							<i class="icon-comments-alt"></i>
						</a>
						</li>
						<li class="dropdown mtop5">
							<a class="dropdown-toggle element" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="Help">
								<i class="icon-headphones"></i>
							</a>
						</li>-->
					<!-- END SUPPORT -->
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<?php
					/*$admlogo = $this->admin_model->getadmindetails();
					if($admlogo)
					{
						foreach($admlogo as $adm)
						{
							$main_admin_logo = $adm->admin_logo;
						}
					}*/
					 
					$admlogo = $this->admin_model->getadmindetails_session();
					if($admlogo)
					{
						foreach($admlogo as $adm)
						{
							$main_admin_logo = $adm->admin_logo;
						}
					}
					
					//$bugname = $this->config->item('bucket_name');
					//$bugname = 'pingou.gmeec.com.br';
					?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<!-- <img src="https://s3-sa-east-1.amazonaws.com/<?php echo $bugname;?>/uploads/adminpro/<?php echo $main_admin_logo; ?>" width="70px" height="50px"> -->
							<img src="<?php echo $this->admin_model->get_img_url(); ?>uploads/adminpro/<?php echo $main_admin_logo; ?>" alt="admin logo" width="70px" height="50px">
							<span class="username"><?php echo $this->session->userdata('admin_username'); ?></span>
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<!--<li><a href="#"><i class="icon-user"></i> My Profile</a></li>
							<li><a href="#"><i class="icon-tasks"></i> My Tasks</a></li>
							<li><a href="#"><i class="icon-calendar"></i> Calendar</a></li>
							<li class="divider"></li>-->
							<!--<li><a href="login.html"><i class="icon-key"></i> Log Out</a></li>-->

							<?php 
							$main=$this->session->userdata('admin_id'); ?>
						 	<?php if($main ==1 )
						 	{ 
					 			?>
								<li><?php echo anchor('adminsettings/settings','<i class="icon-cog"></i> Settings'); ?></li>
								<?php
							}
							?>
							<li><?php echo anchor('adminsettings/logout','<i class="icon-key"></i> Log Out'); ?></li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOP NAVIGATION MENU -->
			</div>
		</div>
	</div>
	<!-- END TOP NAVIGATION BAR -->
</div>