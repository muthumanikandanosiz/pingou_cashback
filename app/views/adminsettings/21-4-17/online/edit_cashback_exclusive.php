<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <?php $admin_details = $this->admin_model->get_admindetails(); ?>
  <title>Casahback Exclusive | <?php echo $admin_details->site_name; ?> Admin</title>
	<?php $this->load->view('adminsettings/script'); ?>
	
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/boostrap-select/bootstrap-select.min.css" media="screen">   
    <link type="text/css"  type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/css/uniform.default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.css" />    
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/data-tables/DT_bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php $this->load->view('adminsettings/header'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
     <?php $this->load->view('adminsettings/sidebar'); ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
              <div class="span12">
              <!-- END THEME CUSTOMIZER-->
               <!--  <h3 class="page-title">
                  Edit Cashback Exclusive
                </h3>
                <ul class="breadcrumb" style="background-color:none !important;">
                  <li>
                    <a href="http://localhost/pingou/adminsettings/dashboard"><i class="icon-home"></i></a>
                    <span class="divider">&nbsp;</span>
                  </li>
                  <li>
                    <a href="http://localhost/pingou/adminsettings/cashback_exclusive">Cashback Exclusive</a>
                    <span class="divider">&nbsp;</span>
                  </li>
                  <li>
                    <a href="http://localhost/pingou/adminsettings/edit_cashback_exclusive">Edit Cashback Exclusive</a>
                    <span class="divider-last">&nbsp;</span>
                  </li>

                </ul> -->
              </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM widget-->
                  <div class="widget">
                     <div class="widget-title">
                        <h4><i class="icon-file"></i>Edit Cashback Exclusive</h4>
                        <span class="tools">
                           <a href="javascript:;" class="icon-chevron-down"></a>
                           <!--<a href="javascript:;" class="icon-remove"></a>-->
                        </span>
                      </div>
                      <br>
					            <span>
                        <span class="required_field"> &nbsp;&nbsp;&nbsp;*</span> marked fields are mandatory.
                      </span>
                      <br>
					            <div class="widget-body form">
                         <?php
                        $get_allusers = $this->admin_model->get_allusers();
                        ?>
              					<?php 
              					$error = $this->session->flashdata('error');
              					if($error!="") 
                        {
              						echo '<div class="alert alert-error">
              						<button data-dismiss="alert" class="close">x</button>
              						<strong>Error! </strong>'.$error.'</div>';
              					}
                        
                        $success = $this->session->flashdata('success');
              					if($success!="")
                        {
              						echo '<div class="alert alert-success">
              					  <button data-dismiss="alert" class="close">x</button>
              						<strong>Success! </strong>'.$success.'</div>';
              					}
                        ?>
                        <!-- BEGIN FORM-->
            						<?php
            						$attribute = array('role'=>'form','method'=>'post','id'=>'cash_exclusive','class'=>'form-horizontal','enctype'=>'multipart/form-data'); 
            						echo form_open('adminsettings/edit_cashback_exclusive',$attribute);
            						?>
						
                        <div class="control-group">
                          <label class="control-label">Link Name<span class="required_field">*</span></label>
                          <div class="controls">
                            <input type="text" class="span6" name="link_name" id="link_name" value="<?php echo $link_name;?>" placeholder="" required />  ex: ?pro=name-blog-Netshoes
                          </div>
                        </div>
                        <?php //echo $user_email; ?>
                        <div class="control-group">
                          <label class="control-label " >User Email<span class="required_field">*</span>
                          </label>
                          <div class="controls">
                            <select name="user_id" required class="span6 selectpicker" data-live-search="true"><?php

              							 	foreach($get_allusers as $users)
              								{
              							 	  ?>							     
              								  <option <?php if($user_email==$users->email){ echo "selected=selected";}
                                ?> value="<?php echo $users->email;?>"><?php echo $users->email;?></option>
              	                <?php
              								}
              							  ?>
                            </select>
                          </div>
                        </div>
                        <?php
                        $aff = $this->admin_model->affiliates();
                        ?>
                        <div class="control-group">
                          <label class="control-label">Store Name </label>
                          <div class="controls">
                            <select name="affiliate_name" class="span6">
                              <?php
                              foreach($aff as $res)
                              {
                                ?>                  
                                <option <?php if($store_name==$res->affiliate_name){ echo "selected=selected";}?> value="<?php echo $res->affiliate_name;?>"><?php echo $res->affiliate_name;?></option>
                                <?php
                              }
                              ?>
                            </select>
                          </div>
                        </div>   
                        <div class="control-group">
                          <label class="control-label">Cashback Type</label>
                          <div class="controls">
                            <select name="affiliate_cashback_type" class="span6">
                              <option value="Percentage">Percentage</option>
                              <!-- <option value="Flat">Flat</option>
                              <option value="Desativado">Desativado</option> -->
                            </select>
                          </div>
                        </div>
                        <div class="control-group">
                          <label class="control-label">Cashback for user <span class="required_field">*</span></label>
                          <div class="controls">
                            <input type="text" required class="span6" name="cashback_percentage" id="cashback_percentage" value="<?php echo $cashback_web; ?>" />  
                          </div>
                        </div>
                        <div class="control-group">
                          <label class="control-label">Cashback for user in the Android App <span class="required_field">*</span></label>
                          <div class="controls">
                            <input type="text" required class="span6" name="cashback_percent_android" id="cashback_percent_android" value="<?php echo $cashback_app; ?>" />  
                          </div>
                        </div>
                        <div class="control-group">
                          <label class="control-label">Content of the store Extra tracking Paramenter <span class="required_field">*</span></label>
                          <div class="controls">
                            <textarea class="span6" required name="content_extra_param" id="content_extra_param"  ><?php echo $extra_param_web; ?></textarea>
                          </div>
                        </div>
                        <div class="control-group">
                          <label class="control-label">Content of the store Extra tracking Paramenter in ANDROID APP </label>
                          <div class="controls">
                            <textarea class="span6" required name="content_extra_param_android" id="content_extra_param__android" ><?php echo $extra_param_app; ?></textarea>
                          </div>
                        </div>
                        <!-- new code for cashback_exclusive new url details 1-8-16.-->
                        <div class="control-group">
                          <label class="control-label">Analytics Information <span class="required_field">*</span></label>
                          <div class="controls">
                            <input type="text" required class="span6" name="analytics_info" id="analytics_info" value="<?php echo $analytics_info; ?>" />  
                          </div>
                        </div>
                        <!-- End -->
                        <div class="control-group">
                          <label class="control-label">Start Date <span class="required_field">*</span></label>
                          <div class="controls">
                            <input type="text" required class="datepicker form-control hasDatepicker" name="start_date" placeholder="From Day (mm/dd/aaaa)" value="<?php echo $start_date; ?>" id="startdatepicker">
                          </div>
                        </div>
                        <div class="control-group">
                          <label class="control-label">End Date <span class="required_field">*</span></label>
                          <div class="controls">
                            <input type="text" required class="datepicker form-control hasDatepicker" name="end_date"   placeholder="To Day (mm/dd/aaaa)" value="<?php echo $expirydate; ?>" id="enddatepicker">
                          </div>
                        </div>
                        <div class="control-group">
                          <label class="control-label">Notification For Users (After the link Expires)</label>
                            <div class="controls">
                              <textarea rows="5" class="span6" name="notify_desk" id="notify_desk" ><?php echo $expiry_notify; ?></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="cashback_id" value="<?php echo $cashback_id;?>">
						            <div class="form-actions">
                          <input type="submit" name="update" value="Update" class="btn btn-success">
                        </div>
						   
						   <?php echo form_close(); ?>
                        <!--</form>-->
                        <!-- END FORM-->
                     </div>
                     </div>
                  <!-- END SAMPLE FORM widget-->
               </div>
            </div>
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   
  <?php $this->load->view('adminsettings/footer'); ?>
 
    <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery-1.8.2.min.js"></script>    
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/js/scripts.js"></script>
   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
   </script>
 <script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){


$(function () {
    $('#startdatepicker, #enddatepicker').datepicker({
        beforeShow: customRange,
        dateFormat: "dd/mm/yy",
        firstDay: 1,
        changeFirstDay: false
    });
});

function customRange(input) {
    var min = null, // Set this to your absolute minimum date
        dateMin = min,
        dateMax = null,
        dayRange = 30; // Restrict the number of days for the date range
    
    if ($('#select1').val() === '2') {
        if (input.id === "startdatepicker") {
            if ($("#enddatepicker").datepicker("getDate") != null) {
                dateMax = $("#enddatepicker").datepicker("getDate");
                dateMin = $("#enddatepicker").datepicker("getDate");
                dateMin.setDate(dateMin.getDate() - dayRange);
                if (dateMin < min) { dateMin = min; }
            } else {  }
        } else if (input.id === "enddatepicker") {
            dateMin = $("#startdatepicker").datepicker('getDate');
            dateMax = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() + 30);
            if ($('#startdatepicker').datepicker('getDate') != null) {
                var rangeMax = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate() + dayRange);
                if (rangeMax < dateMax) { dateMax = rangeMax; }
            }
        }
    } else if ($('#select1').val() != '2') {
        if (input.id === "startdatepicker") {
            if ($('#enddatepicker').datepicker('getDate') != null) {
                dateMin = null;
            } else {  }
        } else if (input.id === "enddatepicker") {
            dateMin = $('#startdatepicker').datepicker('getDate');
            dateMax = null;
            if ($('#startdatepicker').datepicker('getDate') != null) { dateMax = null; }
        }
    }
    return {
        minDate: dateMin,
        maxDate: dateMax
    };
}

$('.datepicker').datepicker('widget').delegate('.ui-datepicker-close', 'mouseup', function() {
    var inputToBeCleared = $('.datepicker').filter(function() { 
      return $(this).data('pickerVisible') == true;
    });    
    $(inputToBeCleared).val('');
});
});//]]>  

</script>




<script>
function bluramount(max_shipping_time)
 {
  max_shipping_time = max_shipping_time.replace(/[^0-9\.]/g,'');
  if(max_shipping_time.split('.').length>2) 
  max_shipping_time = max_shipping_time.replace(/\.+$/,"");
  $('#transation_amount').val(max_shipping_time);
 }
</script>
<script src="<?php echo $this->admin_model->get_css_js_url(); ?>assets/assets/boostrap-select/bootstrap-select.min.js"></script>
<script>
$(document).ready(function(e) 
{ 
  $('.selectpicker').selectpicker();
});
</script>
   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>