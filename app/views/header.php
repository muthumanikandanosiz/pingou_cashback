<?php
$ip_address 	 	 = $_SERVER['REMOTE_ADDR'];
$getadmindetails	 = $this->front_model->getadmindetails();  
$logo 				 = $getadmindetails[0]->site_logo;
$blog_url  			 = $getadmindetails[0]->blog_url;
$site_mode 			 = $getadmindetails[0]->site_mode;
$background_image 	 = $getadmindetails[0]->background_image;
$background_color 	 = $getadmindetails[0]->background_color;
$background_type 	 = $getadmindetails[0]->background_type;

$unlog_menu_status 	 = $getadmindetails[0]->unlog_menu_status;
$log_menu_status 	 = $getadmindetails[0]->log_menu_status;
$log_content   		 = $getadmindetails[0]->log_content;
$unlog_content 		 = $getadmindetails[0]->unlog_content;
$log_status   		 = $getadmindetails[0]->log_status;
$unlog_status 		 = $getadmindetails[0]->unlog_status;
$notify_color        = $getadmindetails[0]->notify_color;
$site_favicon 		 = $getadmindetails[0]->site_favicon;
$ip 				 = $_SERVER['REMOTE_ADDR'];
$unique_visits 		 = $this->front_model->unique_visits($ip);
$user_id 			 = $this->session->userdata('user_id'); 
$userdetails 		 = $this->front_model->userdetails($user_id);

$firstname  		 = $userdetails->first_name;
$lastname   		 = $userdetails->last_name;
$emailid    		 = $userdetails->email;
$oldbalance 		 = number_format(round($userdetails->balance,2),2);
$balance    		 = preg_replace('/\./', ',', $oldbalance);
$usernames  		 = ucfirst($firstname)." ".ucfirst($lastname);

 
if($balance == '')
{
	$balance = '0.00';
} 
//Available balance details//

$cashback_count = $this->front_model->pending_cashback($user_id);
 
$referal_count 	= $this->front_model->pending_referral($user_id);
            
			$oldtotal_amt = number_format(round(($cashback_count + $referal_count),2),2); 
			$total_amt 	  = preg_replace('/\./', ',',$oldtotal_amt);

			if($total_amt == '0')
			{
				$total_amt = "0,00";
			}
			 
			 
//End//

if($firstname =='' && $lastname =='')
{	
	
	$emails    = explode('@', $emailid);
	$usernames = $emails[0];	
}
$username  = substr($usernames,0,15);
//$usernamesasa = $userdetails[0]->first_name;
if($site_mode==0)
{	
		redirect('cashback/under_maintance','refresh');
}

echo $google_analytics = $getadmindetails[0]->google_analytics;
?>
<?php if($background_type == 'image'){?>
<style type="text/css">
body {
	background:url(<?php echo base_url();?>uploads/adminpro/<?php echo $background_image;?>); 
	//background-repeat:repeat;
	//background-size: 100% 100%;
}	
</style>
<?php 
}
if($background_type == 'color')
{?>
<style type="text/css">
body {
	background-color:<?php echo $background_color;?>;
}	
</style>
<?php } ?>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()."uploads/adminpro/".$site_favicon;?>">
<style>
body {
	font-family: 'PT Sans', sans-serif;
	font-size:13px;
	line-height:20px;
}

.ui-menu.ui-widget.ui-widget-content{
	background: #fff none repeat scroll 0 0!important;

    height: 292px!important;
    /*left: 469px!important;*/	
    overflow-x: hidden!important;
    padding: 0!important;
    position: relative!important;
    /*top: -4463.2px!important;*/
    width: 257px!important;
    overflow:scroll;
    border-bottom-left-radius: 6px;
    border-bottom-right-radius: 7px;
    box-shadow: 0 1px 4px rgba(0, 0, 0, 0.15);
    z-index:1050;

}
.ui-menu-item {
    list-style: outside none none!important;
     margin-bottom: 15px!important;
     border-bottom: 1px solid #eee;
}
#ui-id-51 strong {
    margin-left: 21px!important;
}
#ui-id-51 img{
	width:45%!important;
}
 
</style>
<?php 
if($user_id == '')
{
	if($unlog_menu_status == 1)
	{
	?>
		<style type="text/css">
		/*Code for disply hidden based code*/
		.newnews
		{
			display: none;
		}
		.main{
			position: fixed;
			top: 0;
			width: 100%;
			z-index: 10;
		}
		
		/*
		code for (position) based menu
		.main{
			position: fixed;
			top: 0;
			width: 100%;
			z-index: 10;
			padding-top: 50px;
		}*/
		</style>
	<?php
	}
	if($unlog_menu_status == 2)
	{?>
		<style type="text/css">
		/*Code for disply hidden based code*/
		.main{
			position: fixed;
			top: 0;
			width: 100%;
			z-index: 10;
			padding-top: 60px;
		}
		.newnews
		{	
			position: fixed;
			width: 100%;
		}


		/*
		code for (position) based menu
		.main{
			position: fixed;
			top: 0;
			width: 100%;
			z-index: 10;
			padding-top: 50px;
		}
		.newnews
		{	
			position: fixed;
			width: 100%;
		}*/
		</style>
	<?php
	}
	if($unlog_menu_status == 0)
	{?>
		<style type="text/css">
		
		/*Code for disply hidden based code*/
		.main
		{
			display: none;
		}
		.newnew
		{
			display: none;	
		}

		/*
		code for (position) based menu

		.main{
			position: relative;
			top: 0;
			width: 100%;
			z-index: 10;
		}
		.newnews
		{	
			position: relative;
			width: 100%;
		}*/
		</style>
	<?php 
	}

	if(($unlog_status == 0) && ($unlog_menu_status == 1))	
	{?>
		<style type="text/css">
			.top-main-area
			{
    			background: #61b2de none repeat scroll 0 0;
    		}
    		.main 
    		{
    			padding-top: 8px;
			}
		</style>
	<?php
	}
	if(($unlog_status == 0) && ($unlog_menu_status == 2))	
	{?>
		<style type="text/css">
			.top-main-area
			{
    			background: #61b2de none repeat scroll 0 0;
    		}
    		.main 
    		{
    			padding-top: 8px;
			}
		</style>
	<?php
	} 
//}	
/*else
{
	if($log_menu_status == 1)
	{

	}
	else if($log_menu_status == 2)
	{

	}
	else
	{

	}*/
}
if($user_id !='')
{
	if($log_menu_status == 1)
	{
	?>
		<style type="text/css">
		/*Code for disply hidden based code*/
		.newnews
		{
			display: none;
		}
		.main{
			position: fixed;
			top: 0;
			width: 100%;
			z-index: 10;
		}
		
		/*
		code for (position) based menu
		.main{
			position: fixed;
			top: 0;
			width: 100%;
			z-index: 10;
			padding-top: 50px;
		}*/
		</style>
	<?php
	}
	if($log_menu_status == 2)
	{?>
		<style type="text/css">
		/*Code for disply hidden based code*/
		.main{
			position: fixed;
			top: 0;
			width: 100%;
			z-index: 10;
			padding-top: 60px;
		}
		.newnews
		{	
			position: fixed;
			width: 100%;
		}


		/*
		code for (position) based menu
		.main{
			position: fixed;
			top: 0;
			width: 100%;
			z-index: 10;
			padding-top: 50px;
		}
		.newnews
		{	
			position: fixed;
			width: 100%;
		}*/
		</style>
	<?php
	}
	if($log_menu_status == 0)
	{?>
		<style type="text/css">
		
		/*Code for disply hidden based code*/
		.main
		{
			display: none;
		}
		.newnew
		{
			display: none;	
		}

		/*
		code for (position) based menu

		.main{
			position: relative;
			top: 0;
			width: 100%;
			z-index: 10;
		}
		.newnews
		{	
			position: relative;
			width: 100%;
		}*/
		</style>
	<?php 
	}	

	if(($log_status == 0) && ($log_menu_status == 1))	
	{?>
		<style type="text/css">
			.top-main-area
			{
    			background: #61b2de none repeat scroll 0 0;
    		}
    		.main 
    		{
    			padding-top: 8px;
			}
		</style>
	<?php
	}
	if(($log_status == 0) && ($log_menu_status == 2))	
	{?>
		<style type="text/css">
			.top-main-area
			{
    			background: #61b2de none repeat scroll 0 0;
    		}
    		.main 
    		{
    			padding-top: 8px;
			}
		</style>
	<?php
	} 
	 
//}	
/*else
{
	if($log_menu_status == 1)
	{

	}
	else if($log_menu_status == 2)
	{

	}
	else
	{

	}*/
}
?>
<header>
		 <div class="top-main-area text-center newnew newnews">
            <div class="wrap-top">

             <!--  ALTEREI Logo era aqui --> 
            <!--New code for Notification bar content 16-4-16-->
            <?php 
            $log_content   = $getadmindetails[0]->log_content;
            $unlog_content = $getadmindetails[0]->unlog_content;
            $log_status    = $getadmindetails[0]->log_status;
            $unlog_status  = $getadmindetails[0]->unlog_status;
            $url           = base_url()."#pingous";
            $data = array(
			   		'###URL###'=>$url);
            $content=strtr($unlog_content,$data);

		            	if($user_id!='')
		            	{	
		            		if($log_status == 1)
		            		{
		            			if($notify_color !='')
		            			{	
		            			?>
				            		<style type="text/css">
				            		.newnew
				            		{
				            			background: <?php echo $notify_color;?>
				            		}
				            		</style>
			        				<b><?php echo $log_content; ?></b>
		        				<?php
		        				} 	
		            		}	
		            	} 
			            if($user_id == '')
			            {	
			            	if($unlog_status == 1)
		            		{
		            			if($notify_color !='')
		            			{	
		            			?>
				            		<style type="text/css">
				            		.newnew
				            		{
				            			background:<?php echo $notify_color;?>
				            		}
				            		</style>
					        		<b><?php echo $content;?></b>
			        			<?php
			        			}
			        		}
			        	} 	
           			?>
            
            	</div>
            </div> 
             
        	
              
                <!--  ALTEREI teste -->     
                
            
  <div class="main">
    <div class="wrap-top-old">
      <div class="row">
        <div class="col-md-12"> 
          
          <div id="mgmenu1" class="mgmenu_container">
           <!-- ALTEREI logotipo INICIO -->
           <div class="header-left">
                 <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>uploads/adminpro/<?php echo $logo;?>" alt="pingou-cupom-desconto" > </a>
           </div>
           <!-- ALTEREI logotipo FINAL -->
           
           <!-- ALTEREI SEARCH INICIO -->
<div class="header-middle">                             
                             
<?php
  if((strpos($_SERVER['REQUEST_URI'],'cashback/shopping') == true))
  {
	  ?>
      <form class="search-area form-group search-area-white search-style" action="<?php echo base_url();?>cashback/change_location" method="POST">
            <div class="wrap-top">
                <div class="row">
                    <!--<div class="col-md-11 clearfix">
                        <label><i class="fa fa-search"></i><span>I am searching for</span>
                        </label>
                        <div class="search-area-division search-area-division-input">
                          
                            <input type="text" required id="store_nametype" class="typeahead form-control" name="store" value="<?php  if(isset($_POST['storehead'])){echo $storehead;}?>" data-provide="typeahead">
                        </div>
                    </div>-->
                    <div class="col-md-11 clearfix">
                         <div class="search-area-division search-area-division-input">

                             <input type="text" required id="Location" placeholder="Pesquise Lojas ou Ofertas" class="form-control" name="location" value="<?php if($this->session->userdata('cityname'))
																{
																	echo $cityname = $this->session->userdata('cityname');
																} ?>" data-provide="typeahead">
                             
                        
                        </div>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-block btn-white search-btn pop" type="submit">
                        <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </form>
      <?php
  }
  else
  {	 
	  if(isset($_POST['storehead']))
	  {
		$storehead  = $_POST['storehead'];
	  } 
	  ?>
      <!--<form class="search-area form-group search-area-white search-style" method="get" onsubmit="return submit_form();">
            <div class="wrap-top">
                <div class="row">
                    <div class="col-md-11 clearfix">
                        <div class="search-area-division search-area-division-input">
                            <input type="text" placeholder="Aliexpress, Submarino, Netshoes" required id="store_nametype" class="typeahead form-control" name="store" value="<?php  if(isset($_POST['storehead'])){echo $storehead;}?>" data-provide="typeahead">
                        </div>
                    </div>
                    <div class="col-md-1 search-botao">
                        <button class="btn btn-block btn-white search-btn pop" type="submit">
                        <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </form>-->

        <!--New search form-->

        <form id="search_mini_form" class="search-area form-group search-area-white search-style"  action="" method="get">
			<div class="wrap-top">
 				<div class="row">
                    <div class="col-md-11 clearfix">
                        <div class="search-area-division search-area-division-input">
							<input id="search" type="text" name="store" class="form-control search_header" value="<?php if(isset($_POST['storehead'])){echo $storehead;}?>" placeholder="Aliexpress, Submarino, Netshoes" />
							<input id="catsearch" type="hidden" name="cat" />
						</div>
                    </div>	
					<div class="col-md-1 search-botao">
							 
							 <button type="button" class="btn btn-block btn-white search-btn pop"><i class="fa fa-search"></i>
							</button> 
					</div>
					<div id="search_autocomplete" class="search-autocomplete"></div>
					</div>				
			</div>
		</form>

        <!--End-->

        <form id="dummyform" method="post">
          <input type="hidden" name="storehead" id="storehead" value="">
        </form>
      <?php
  }
  
?>

</div>
		
	
	                           <!-- ALTEREI SEARCH final -->
                               
                               
            <!-- ALTEREI LOGIN-INICIO" -->
             <ul class="login-register navbar-right">
                           <!-- ALTEREI REMOVI: <li class="shopping-cart" id="cart"><a href="#" data-toggle="dropdown" id="dropdownMenu2"><i class="fa fa-shopping-cart"></i>My Cart</a> -->
                           
                           
                           
                             <!-- negocio de carrinho - INICIO -->  
                           <!--  
                            <XXXXXXX?php
							$cart =  $this->front_model->getuser_cart();
							?>
                            <ul aria-labelledby="dropdownMenu2" role="menu" class="dropdown-menu">
                            <!-- No Items Found-->
                            <!--	<li>
                            <XXXXXXXX?php 
							$sub_total=0; $i=0; 
							if($cart) {
								?>
                                   
										<table class="table hcart">
											<tbody>
                                
                                <XXXXXXX?php
								$s=1; 
								$new_check_out='';
								foreach($cart as $carts) 
								{  $i++;
									$product_details=$this->front_model->details($carts->product_id);
									if($product_details->remain_coupon_code!='')
									{
										$new_check_out=1;
									$db_coupon_image=$product_details->coupon_image;
									$exp_db_coupon_image=explode(",",$db_coupon_image);  	
									$maximum_count=explode(",",$product_details->remain_coupon_code);
							 ?>
                            
											<tr id="">
												<td class="text-center">
														<a href="javascript:void(0)">
														<img class="img-thumbnail img-responsive" width="80" height="80" title="image" alt="image" src="<XXXXXXX?php echo base_url(); ?>uploads/premium/<XXXXXX?php echo $exp_db_coupon_image[0]; ?>">
													</a>
												</td>
												<td class="text-left">
													<a href="javascript:void(0)">
														<XXXXXX?php echo substr($product_details->offer_name,0,9)."...";   ?>
													</a>
												</td>
												<td class="text-right">x 1</td>
												<td class="text-right">Rs.<XXXXXX?php echo $product_details->amount ?></td>
												<!--<td class="text-center">
													<a href="#" onclick="return delete_cart_header(<XXXXXX?php echo $carts->id;  ?>,<XXXXXX?php echo $s;?>);">
                                                    
														<i class="fa fa-times"></i>
													</a>
												</td>-->
									<!--		</tr>
											
										<XXXXXX?php 
											$sub_total+=$product_details->amount*$carts->quantity;?>
                                        <XXXXXX?php }
										else
										{
											$this->db->query('DELETE from premium_cart where id='.$carts->id);
										}
										
										$s++;}?>
                                        </tbody></table>
									</li>
                             
									<li>
										<table class="table table-bordered total">
											<tbody>
											<!--	<tr>
													<td class="text-right"><strong>Sub-Total</strong></td>
													<td class="text-left">$1,101.00</td>
												</tr>
												<tr>
													<td class="text-right"><strong>Eco Tax (-2.00)</strong></td>
													<td class="text-left">$4.00</td>
												</tr>
												<tr>
													<td class="text-right"><strong>VAT (17.5%)</strong></td>
													<td class="text-left">$192.68</td>
												</tr>-->
                                                
												<!-- <tr>
													<td class="text-right"><strong>Total</strong></td>
													<td class="text-left">Rs.<XXXXXX?php echo $sub_total; ?></td>
												</tr>
											</tbody>
										</table>
										<p class="text-right btn-block1">
											<a href="<XXXXXX?php echo base_url();?>cashback/cart_listing_page" class="btn btn-blue">View Cart		</a>
											<!--<a href="#" class="btn btn-default">	Checkout</a>-->
									<!--	</p>
									</li>									
								
                        <XXXXXX?php
							}
							else { ?>
                            
                   			 <li>
                           		 No Items Found
                            </li>
                            
                    <XXXXXXX?php } ?>
                    </ul>
                                
                            </li> -->
                                        
                         <!-- negocio de carrinho - FINAL -->               
                                        
                                        
            <!-- INICIO LOGIN ANTIGO                            
                            <XXXXX?php
								$user_id = $this->session->userdata('user_id');
								if($user_id!="")
								{
									?>
                                     <li class="right_item bor-rht">
									  <XXXXX?php
										$attribute = array('class'=>'','data-effect'=>'mfp-move-from-top');
										echo anchor('cashback/myaccount','Minha Conta',$attribute);
									 ?>
									 </li>
                                     
                                     
									 <li class="right_item bor-rht">
									  <XXXXX?php
										$attribute = array('class'=>'','data-effect'=>'mfp-move-from-top');
										echo anchor('cashback/logout',' Logout',$attribute);
									 ?>
									 </li>
								   <XXXXX?php
								}
								else
								{
									?>
									  <li class="right_item bor-rht">
                                       <a data-toggle="modal" href="#myModal" class="popup-text">Sign in</a>
										</li>
									 <li class="right_item bor-rht">
									<XXXXX?php
										$attributes = array('class'=>'');
										echo anchor('cashback/register','Sign up',$attributes);
									 ?>
									</li>
									<XXXXX?php
								}
							?>
                    
                    
              </li>
                         FINAL LOGIN ANTIGO -->
                   </ul>
                         
                        <!-- ALTEREI LOGIN-FINAL" -->
            
               <!-- MENU dropdown INICIO --> 
<div class="header-right">
  <ul id="menu"> <!-- Inicia o menu principal -->
      <?php
								$user_id = $this->session->userdata('user_id');
								if($user_id!="")
								{
									//$username = word_limiter($firstname,20);
									?>
                                    <li style="width:200px !important; line-height:20px !important;">  <!-- Item dropdown do menu, leva a class 'drop' para a adição da seta -->
			<a  href = "<?php echo base_url();?>minha_conta" class="drop" data-effect="mfp-move-from-top"><div class="menu-case"> <i class="icon-user ico-med2 menu-pic"></i>&nbsp;<?php echo $username;?> <br><b><font color="green">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;R$<?php echo $balance; ?></font><font color="yellow">&nbsp;R$<?php echo $total_amt; ?></font></b> </a> </div>
         <div class="columns"> <!-- Inicia o box que conterá o conteúdo do dropdown -->
                <div class="menu-cabec"> Visão Geral </div> 
			<ul class="col"> <!-- Inicia o box de uma das 3 colunas com os links -->
				<!--<span>WordPress</span>  Título da coluna -->
				<li class="mb-lt-lb"><a href="<?php echo base_url();?>extrato"><i class="icon-receipt ico-mai ico-green"></i><a href="<?php echo base_url();?>extrato">Extrato</a></a></li>   
			</ul> <!-- fecha o box de uma das 3 colunas com os links -->
			<ul class="col">
				<!-- <span>JQuery</span> -->
				<li><a href="<?php echo base_url();?>resgate"><i class="icon-bag-dollar ico-mai ico-green"></i><a href="<?php echo base_url();?>resgate"> Resgate</a></a></li>    
			</ul>
			<ul class="col">
				<!-- <span>Miscel&acirc;nea</span> --> 
 				<li class="mb-rt-rb"><a href="#"><i class="icon-users-plus ico-mai ico-green"></i><a href="#">Indicações</a></a></i></li>
			</ul>
            <div class="menu-cabec"> Facilidades </div>
            <ul class="col col2">
				<!-- <span>JQuery</span> -->
				<li class="mb-lt-lb"><a href="#"><i class="icon-bullhorn  ico-mai ico-blue"></i><a href="#">Lembrador</a></a></li>
			</ul>
			<ul class="col col2">
				<!-- <span>Miscel&acirc;nea</span> -->
                <li class="mb-rt-rb"><a href="#"><i class="icon-android  ico-mai ico-blue"></i><a href="#">Aplicativo</a></a></li>
				<!-- <li class="mb-rt-rb"><a href="#">Aplicativo</a></li>  --> 
			</ul> 
             <div class="menu-cabec-c3"> 
                  <div class="menu-cabec">Ajuda </div>
            <ul class="col col3">
				<!-- <span>JQuery</span> -->
 				<li class="mb-lt-rt"><a href="#"><i class="icon-binoculars ico-peq2 pad-6r"></i>Como funciona?</a></li>
                <li class="mb-lb-rb"><a href="#"><i class="icon-bubbles ico-peq2 pad-6r"></i>Fale conosco</a></li>    
			</ul>
            </div>
            <div class="menu-cabec-c3 mcol3p-left"> 
			    <div class="menu-cabec"> Sua Conta </div>
            <ul class="col col3">
				<!-- <span>Miscel&acirc;nea</span> -->
				<li class="mb-lt-rt"><a href="<?php echo base_url();?>minha_conta"><i class="icon-pencil5 ico-peq2 pad-6r"></i>Meus dados</a></li>   
                <li><?php
										$attribute = array('class'=>'mb-lb-rb','data-effect'=>'mfp-move-from-top');
										echo anchor('cashback/logout','<i class="icon-power-switch ico-peq2 pad-6r"></i>Sair',$attribute);
									 ?></li>  
			</ul> 
            </div>
        </div> <!-- Fecha o box que conterá o conteúdo do dropdown -->
        
        
                                     
    </li> <!-- Fecha item dropdown do menu -->
  <?php
}
								else
								{
									?>
									  <li class="btn-login">
                                       <a data-toggle="modal" href="#entrar" class="btn-login-color">Login</a>
										</li>
                                        <?php
								}
							?>
 </ul> <!-- Fecha o menu principal -->    
</div>        
             <!-- MENU dropdown FINAL --> 
            
            
   <?php
								$user_id = $this->session->userdata('user_id');
								if($user_id!="")
								{
									$styledropdownposition='style="margin-right:100px !important"' 
								
									?>
                                    <?php } ?>
                                              
            
            <ul class="mgmenu" <?php echo $styledropdownposition ?>>
              
              <li class="mgmenu_button">&nbsp;</li>
              
              <!--<li class="active"><a  href="<XXXXX?php echo base_url();?>"><span>Home</span></a> </li> -->
              <!-- ALTEREI REMOVI INICIO-SHOPS -->
              <!-- ALTEREI REMOVI <li><span>Shops <i class="fa fa-caret-down"></i></span>
                
                <div class="dropdown_fullwidth mgmenu_tabs" id="tabs">
                  
                  <ul class="mgmenu_tabs_nav">
                  <li><a href="#section2" class="">Most Popular Stores</a></li><!--seetha remove class  current -->
                   <!-- ALTEREI REMOVI <li><a href="#section1" >Shop By Category</a></li>
                    
                  </ul>
                  <div class="mgmenu_tabs_panels">
                 
                    <div id="section1" class="mgmenu_tabs_hide">
                    
                    <XXXXX?php
					$listof_categorys = $this->front_model->getmaincategorys();
					/*echo "<pre>";
					print_r($listof_categorys);
					exit;*/
					$s =1;
					foreach($listof_categorys as $category_list)
					{
						$category_id = $category_list->category_id;
						$category_url = $category_list->category_url;
						$category_name = $category_list->category_name;
						if($s==2)
						{
							?>
                            <ul class="nav_gray list-unstyled clearfix col-md-2 col-sm-2 col-xs-12 pad-no">
                                <li><img src="<XXXXX?php echo base_url();?>front/images/meni-img.png" class="img-responsive"></li>
                                
                              </ul>
                            <XXXXX?php
						}
						?>
                        <ul class="list-unstyled clearfix col-md-2 col-sm-2 col-xs-12">
                            <li><XXXXX?php echo anchor('cashback/products/'.$category_url,'<strong>'.$category_name.'</strong>'); ?></li>      
                            <XXXXX?php
							$subcate =  $this->front_model->get_sub_categorys_list($category_id);
							if(count($subcate)!=0)
							{
								
								foreach($subcate as $subcatelist)
								{
									$sub_category_name = $subcatelist->sub_category_name;
									$sub_category_url = $subcatelist->sub_category_url;
									$sun_category_id = $subcatelist->sun_category_id;
									?>
                                    <li><XXXXX?php echo anchor('cashback/products/'.$category_url.'/'.$sub_category_url,$sub_category_name); ?></li>
                                    <XXXXX?php
								}
							}
							?>                    
                      	</ul>
                        <XXXXX?php
						$s++;
					}
					?>
                    
                      
                     
                    </div>
                    <!-- End Section 1 -->
                    
                    <!-- ALTEREI REMOVI <div id="section2" ><!-- Begin Section 2 -->
                   <!-- ALTEREI REMOVI <XXXXX?php 
					$get_stores_category = $this->front_model->get_available_store_cate();
					foreach($get_stores_category as $store_category)
					{
						$category_id = $store_category->category_id;
						$category_url = $store_category->category_url;
						$category_name = $store_category->category_name;
						?>
                        <ul class="list-unstyled clearfix col-md-2 col-sm-2 col-xs-12">
                        <li><XXXXX?php echo anchor('cashback/products/'.$category_url,'<strong>'.$category_name.'</strong>'); ?></li>   
                        <XXXXX?php
							$affiliates =  $this->front_model->get_available_affiliates($category_id);
							if(count($affiliates)!=0)
							{
								foreach($affiliates as $affiliateslist)
								{
									$affiliate_name = $affiliateslist->affiliate_name;
									$affiliate_url = $affiliateslist->affiliate_url;
									$affiliate_id = $affiliateslist->affiliate_id;
									?>
                                    <li><XXXXX?php echo anchor('cashback/stores/'.$affiliate_url,$affiliate_name." Coupons"); ?></li>
                                    <XXXXX?php
								}
							}
							?>
                      </ul>
                        <XXXXX?php
					}					
					?>
                    </div>
                    
                  </div>
                  
                </div>
                
              </li>
              
             <!-- <li><XXXXX?php echo anchor('cashback/category','<span>Category</span>'); ?>
                
                <div class="dropdown_container dropdown_8columns">
                  
                <div class="col_12">
                    <h3>Eye-Catching Heading</h3>
                    <p>Donec elementum vitae elit ac imperdiet. Nullam blandit sapien et ligula mollis, nec eleifend eros consequat. Donec vestibulum, elit in pharetra tempus, dui urna imperdiet nisl, vitae imperdiet quam erat vitae ipsum. Mauris consequat augue. Pellentesque eros risus, sollicitudin tincidunt eleifend ac, malesuada vel massa. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse blandit lectus et eros faucibus.</p>
                    <hr>
                  </div>
                  <div class="col_8">
                    <h4>Second Heading Title</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eu urna lobortis, posuere orci id, consectetur lacus. Donec pulvinar pharetra adipiscing. Sit amet nisi vestibulum malesuada. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                    <h4>Third Headline Example</h4>
                    <p>Praesent sed lectus vel tortor aliquet bibendum at non orci. Aliquam nunc sapien, pretium eu dapibus vel, adipiscing sed nisl. Nulla facilisi. Duis iaculis leo. Sed quis scelerisque massa. Donec pharetra feugiat ante, at fermentum sem. Cum sociis natoque penatibus et magnis dis.</p>
                  </div>
                  <div class="col_4">
                    <p class="text_box">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eu urna lobortis, posuere orci id, consectetur lacus. Donec pulvinar pharetra adipiscing. Praesent sed lectus vel tortor aliquet bibendum at non orci. Aliquam nunc sapien, pretium eu dapibus vel, adipiscing sed nisl.</p>
                  </div>
                </div>
               
                
              </li>-->
              <!-- End Item -->
              <!--  ALTEREI REMOVI FINAL-SHOPS -->
              
              <?php
			  $enable_shopping = $getadmindetails[0]->enable_shopping;
			  if($enable_shopping==1)
			  {
			  ?>  
             <li><a href="<?php echo base_url();?>shopping"><span>Ofertas</span></a></li>
              <?php
			  }
			  ?>
                        
    
              
            <li><a href="<?php echo base_url();?>top_cashback"><span>Destaques</span></a></li>
             <li><a href="<?php echo base_url();?>desconto_cupom"><span>Lojas</span></a></li> 
              
              <!-- <XXXXX?php
			  $enable_blog = $getadmindetails[0]->enable_blog;
			  if($enable_blog==1)
			  {
			  ?>  
              <li><XXXXX?php echo anchor($blog_url,'<span>Blog</span>'); ?></li>
              <XXXXX?php
			  }
			  ?>              ALTEREI: REMOVI  ir para o footer -->   
					<?php
	               $result = $this->front_model->header_menu();
				   if($result)
				   {
                    foreach($result as $view)
                    {
                	?>
                     <li><a href="<?php echo base_url();?>cashback/cms/<?php echo $view->cms_title;?>"><span><?php echo $view->cms_heading;?></span></a></li>	                   	
                	<?php 
					} 
				   }?>
            
              <!-- End Item -->
              
            </ul>
          
            <!-- End Mega Menu --> 
            
          </div>
          <!-- End Mega Menu Container --> 
          
          <!-- END MAIN NAVIGATION --> 
        </div>
      </div>
    </div>
  </div>  
      
           <!-- ALTEREI SEARCH AQUUUUII -->
       
</header>


<script type="text/javascript">
$('[class^="fa icon-"]').addClass("cls_new_font");
$('[class^="icon-"]').addClass("cls_new_font");
</script>
<div class="newclass">