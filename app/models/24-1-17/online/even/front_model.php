<?php
class Front_model extends CI_Model
{
	function home_slider()
	{
		$this->db->connection_check();
		$this->db->where("banner_status",'1');
		$this->db->where("banner_position",'0');
		$query = $this->db->get('tbl_banners');
		return $query->result();
	}
	function email_sub($email)
	{
		$this->db->connection_check();
		$this->db->where('subscriber_email',$email);
		$query = $this->db->get('subscribers');
		if($query->num_rows() == 0)
		{
			$date = date('Y-m-d h:m:s');
			$data = array(
			'subscriber_email' => $email,
			'subscriber_status' => '1',
			'date_subscribed' => $date
			);
			$this->db->insert('subscribers',$data);
			return 1;
		}
		else
			return 0;
	}
	
	//list out Header menu

	function header_menu()
	{
		$this->db->connection_check();
		$this->db->where('cms_status','1');
		$this->db->where('cms_position','header');
		$query = $this->db->get('tbl_cms');
		if($query->num_rows() > 0)
			return $query->result();
	}

	//New code for header page login user name and Amount details //

	function userdetails($user_id)
	{
	   $this->db->connection_check();
	   $this->db->where('user_id',$user_id);
	   $this->db->where('admin_status','');
		$userdetails = $this->db->get('tbl_users');
		if($userdetails->num_rows > 0){
			return $userdetails->row();
		}
		return false;
	}
	function admindetails()
	{
	   $this->db->connection_check();
	   $this->db->where('admin_id',1);
	    
		$admindetails = $this->db->get('admin');
		if($admindetails->num_rows > 0){
			return $admindetails->row();
		}
		return false;
	}

	
		
	function referalamt($user_id)
	{
	   $this->db->connection_check();
	   $this->db->where('user_id',$user_id);
	   $this->db->where('transation_reason','Pending Referal Payment');
	   $this->db->where('transation_status','pending');
	   $referalamt = $this->db->get('transation_details');
		if($referalamt->num_rows > 0){

			foreach($referalamt->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
	}

	function cashbackamt($user_id)
	{
	   $this->db->connection_check();
	   $this->db->where('user_id',$user_id);
		$this->db->where('status','pending');
		$cashbackamt = $this->db->get('cashback');
		if($cashbackamt->num_rows > 0){

			foreach($cashbackamt->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
	}

	//End//

	//Code for all email subscribers details//	 

	 
	//End// 
	
	//list out sub footer menu
	
	function sub_menu()
	{
		$this->db->connection_check();
		$this->db->where('cms_status','1');
		$this->db->where('cms_position','footer');
		$query = $this->db->get('tbl_cms');
		if($query->num_rows() > 0)
			return $query->result();
	}

	//header menu link
	function cms_content($names)
	{
		$this->db->connection_check();
		$this->db->where("cms_title",$names);
		$query = $this->db->get('tbl_cms');
		//$result = $this->db->query("select * from tbl_cms where cms_title='$names'")->row();
		if($query->num_rows() > 0)
			return $query->result();
	}

//Refer Friends

	function refer_friends()
	{
		$this->db->connection_check();
		$user_id = $this->session->userdata('user_id');
		$this->db->where('user_id',$user_id);
		$this->db->where('admin_status','');
		$query = $this->db->get('tbl_users');
		
		//$result = $this->db->get_where('tbl_users',array('user_id'=>'2'))->row('random_code');
		return $query->result();
	}

//mail function
	
	function invite_mail()
	{
		$this->db->connection_check();
		$this->load->library('email');

		$user_id 		= $this->session->userdata('user_id');
		$mail 			= $this->input->post('email');
		$random 		= $this->input->post('random');
		$mail_text  	= $this->input->post('mail_text');
		$mail_temp  	= $this->db->query("select * from tbl_mailtemplates where mail_id='5'")->row();
		$fe_cont  		= $mail_temp->email_template;
		//$subject 		= 'Seu amigo acaba de indicar você, vem ganhar dinheiro de volta!';
		$subject 		= $mail_temp->email_subject;
		$name 		    = $this->db->query("select * from admin")->row();
		$admin_emailid  = $name->admin_email;
		$contact_number = $name->contact_number;
		$site_logo 		= $name->site_logo;
		$servername 	= base_url();
		$nows 			= date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */
		$sep_email 		= explode(',',$mail);
		
		
		$sfirst_name	= $this->db->query("select * from tbl_users where user_id='$user_id'")->row('first_name');
		$slast_name		= $this->db->query("select * from tbl_users where user_id='$user_id'")->row('last_name');
		$semail_id		= $this->db->query("select * from tbl_users where user_id='$user_id'")->row('email');

		$susername      = $sfirst_name." ".$slast_name;
		$ename      	= explode('@', $semail_id);
		$susernames 	= $ename[0];


		if($sfirst_name == '' && $slast_name == '')
		{
			$susername = $susernames;
		}
		else
		{
			$susername  = $susername;	
		}

		foreach($sep_email as $emails)
		{
		

			$gd_api=array(
				'###EMAIL###'=>"! Here is " . $susername ." (".$semail_id.")",
				'###SITENAME###'=>$name->site_name,
				'###ADMINNO###'=>$contact_number,
				'###DATE###'=>$nows,
				'###CONTENT###'=>$mail_text."<br><br> <h2><a href='".base_url()."?ref=".$random."'>Click here to Activate</a> and get all the offers</h2>",
				'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
				'###URL###'=>'<a href='.$servername.$random.'></a>'
				);
					   
			$gd_message=strtr($fe_cont,$gd_api);
			//echo $gd_message;exit;
			$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				);
			$this->email->initialize($config);        
     		$this->email->set_newline("\r\n");
			$this->email->from($admin_emailid,$name->site_name.'!');
			$this->email->to($emails);
			$this->email->subject($subject);
			$this->email->message($gd_message);
			$this->email->send();
			
			$this->email->print_debugger();
			//return true;
		}
			//return false;
	}


//insert contact form
	function contact_form()
	{
		
		$this->load->library('email');
		$this->db->connection_check();
		$data = array(
		'name' => $this->input->post('name'),
		'email' => $this->input->post('email'),
		'message' => $this->input->post('message'),
		);
		$this->db->insert('tbl_contact',$data);
		
		//send email 		
		
		$this->db->where('admin_id',1);
		$admin_det = $this->db->get('admin');
		
		if($admin_det->num_rows > 0) 
		{    
			$admin = $admin_det->row();
			$admin_email = $admin->admin_email;
			$site_name = $admin->site_name;
			$admin_no = $admin->contact_number;
			$site_logo = $admin->site_logo;
		}
		
		$date 	   = date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */	
		$name 	   = $this->input->post('name');
		$useremail = $this->input->post('email');
		$message   = $this->input->post('message');
		$url 	   = "<img src=".base_url()."uploads/adminpro/".$site_logo.">";
		
		$this->db->where('mail_id',7);
		$mail_template = $this->db->get('tbl_mailtemplates');
		if($mail_template->num_rows > 0) 
		{        
		    $fetch 		= $mail_template->row();
		    $subject 	= $fetch->email_subject;  
		    $templete 	= $fetch->email_template;  
		   
			
			/*$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
			); */
			/*$config = Array(
			'protocol' => 'sendmail',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'vivek.developer@osiztechnologies.com',
			'smtp_pass' => 'iamnotlosero',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1',
			'wordwrap'  => TRUE
			);*/

			/*$this->email->initialize($config); 
			$this->email->set_newline("\r\n");       
			$this->email->from($useremail);
			$arrto   = array();
			$arrto[] = $useremail;
			$arrto[] = $admin_email;
			$this->email->to($arrto,$site_name.'!');
			$this->email->subject($subject);*/		   
		   
		    
			$data = array(
				'###CONTENT###'=>$message,
				'###COMPANYLOGO###' =>$url,
				'###SITENAME###'=>$site_name,
				'###ADMINNO###'=>$admin_no,
				'###DATE###'=>$date								
		    );
		  	 
		    $content_pop=strtr($templete,$data);		   
		   
		   	$to 	  = $admin_email;
	        $subject  = $subject;	
	        $message  = $content_pop;
	        $headers  = "From:$useremail \r\n";
	        $headers .= "MIME-Version: 1.0\r\n";
	        $headers .= "Content-type: text/html\r\n";

			mail($to,$subject,$message,$headers);
		   /*$this->email->message($content_pop);
		   $this->email->send();
		   $this->email->print_debugger();   */
              
			return true;                
		}
		
	}

////Referral Friends Network	

	function referral_network()
	{
		$this->db->connection_check();
		$user_id = $this->session->userdata('user_id');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get("referrals");
		if($query->num_rows() > 0)
			return $query->result();
	}

 	

//login (SL) 
	function login()
	{	
		$this->db->connection_check();
		$user_email = $this->input->post('email');
		$user_pwd 	= $this->input->post('password');

		$cashback_id 	  = $this->input->post('cashback_id');
		$cashback_details = $this->input->post('cashback_details');
		$expirydate  	  = $this->input->post('expirydate');
		$cashbackweb 	  = $this->input->post('cashbackweb');
		
		$this->db->where('email',$user_email);
		$this->db->where('password',$user_pwd);
		$this->db->where('admin_status','');
	
		$query = $this->db->get('tbl_users');
		$numrows = $query->num_rows();
		if($numrows==1)
		{
			$fetch = $query->row();
			$user_id = $fetch->user_id;
			$user_email=$fetch->email;
			$status = $fetch->status;
			if($status =='0')
			{
				return 2;
			}
			else
			{				
				//set session
				$this->session->set_userdata('user_id',$user_id);
				$this->session->set_userdata('user_email',$user_email);
				/*New code for popup count session details 20-11-16*/
				$this->session->set_userdata('ses_popup_count',0);
				/*End 20-11-16*/

				//set cookie
				setcookie("user_id", $user_id, time() + 120, "/"); // 86400 = 1 day
				setcookie("user_email", $user_email, time() + 120, "/");

				return 1;
			}
	    }
		return 0;
	}
	

	function login_google($user_details,$new_cash_ex_id,$expiry_dates,$cashback_web,$cashback_details)
	{

		$this->db->connection_check();
		$user_email = $user_details->email;
		$user_fid   = $user_details->photoURL;
		 
		$selqry="SELECT * FROM  tbl_users where email='$user_email'"; // and profile ='$user_fid'
		$query=$this->db->query("$selqry");
		$numrows = $query->num_rows();
		if($numrows==1)
		{
			$fetch = $query->row();
			$user_id = $fetch->user_id;
			$user_email=$fetch->email;

			/*New code for update a already registered user details 12-1-17*/
			$data = array('first_name' =>$user_details->firstName,'last_name' =>$user_details->lastName,'profile'=>$user_fid);
			$this->db->where('user_id',$user_id);
			$update_qry = $this->db->update('tbl_users',$data);	
			/*End 12-1-17*/
			
			$this->session->set_userdata('user_id',$user_id);
			$this->session->set_userdata('user_email',$user_email);
			/*new code for cashback exclusive session start 18-7-16.*/
			$this->session->set_userdata('cashback_id',$new_cash_ex_id);
			$this->session->set_userdata('cashback_details',$cashback_details);
			$this->session->set_userdata('expirydate',$expiry_dates);
			$this->session->set_userdata('cashbackweb',$cashback_web);
			/*End*/ 

			//set cookie
			setcookie("user_id", $user_id, time() + (86400 * 30), "/"); // 86400 = 1 day
			setcookie("user_email", $user_email, time() + (86400 * 30), "/");

			return 1;
	    }
	    else
	    {
	    	/*New code added 20-11-16*/
	    	$cat_type_url  =  $this->session->userdata('cat_type');
	    	 
	    	$arrcat_type   = array('3454'=>2,'8765'=>3,'2345'=>4,'1647'=>5,'6536'=>6,'7486'=>7,'8362'=>8,'9326'=>9,'2695'=>10);
		    if (array_key_exists($cat_type_url,$arrcat_type))
			{
		    	$categorytype = $arrcat_type[$cat_type_url];
		    }
		    else
		    {	
		    	$categorytype = 1;
		    }
	    	  
	    	/*End 20-11-16*/

		    /*SOCIAL REFERENCE USER_ID END */
		    $random = $this->session->userdata('ses_random_ref');
			if($random !='')
			{
				$new_uid=$random;
				$this->db->where('random_code',$new_uid);
				$this->db->where('admin_status','');
				$query = $this->db->get('tbl_users');
				if($query->num_rows() > 0)
				{	
					$fetch = $query->row();
					$user_id = $fetch->user_id;
				}
				else
				{
					$user_id = 0;
				}
			}
			else
			{
				$user_id = 0;
			}
			$refer=$user_id;
			/* SOCIAL REFERENCE USER_ID END */			

		    $new_random = mt_rand(1000000,99999999);
		    //New code for random code 23-1-17//
		    $unic_code 	= $this->random_string(10);
		    //End//
		   	$user_email = $user_details->email;
		   	$date = date('Y-m-d h:i:s');
		   	$data = array(		
			'first_name'=>$user_details->firstName,
			'last_name'=>$user_details->lastName,
			'email'=>$user_email,
			'street'=>$user_details->address,
			'city'=>$user_details->city,
			'state'=>$user_details->region,
			'zipcode'=>$user_details->zip,
			'country'=>$user_details->country,
			'contact_no'=>$user_details->phone,
			'random_code'=>$new_random,
			'profile'=>$user_fid,
			'refer'=>$refer,
			'status'=>1,		
			'cashback_mail'=>1,
			'withdraw_mail'=>1,
			'referral_mail'=>1,
			'acbalance_mail'=>1,
			'support_tickets'=>1,
			'newsletter_mail'=>1,
			'referral_category_type'=>$categorytype,
			'date_added'=>$date,
			'unic_bonus_code'=>$unic_code
			);
			$this->db->insert('tbl_users',$data);
			$insert_id = $this->db->insert_id();
			/*Unset session details for referral category details 22-11-16*/
			$this->session->unset_userdata('cat_type');
			/*end 22-11-16*/

			if($new_uid)
			{      
				$this->db->where('random_code',$new_uid);
				$this->db->where('admin_status','');
				$query = $this->db->get('tbl_users');
				if($query->num_rows() > 0)
				{	
					$fetch = $query->row();
					$user_id = $fetch->user_id;
					$email =$fetch->email;
					
					$datas = array(
					'user_id' => $user_id,
					'user_email' => $email,
					'referral_email' => $user_email,
					'status' => 'Ativa',
					'date_added' => $date
					);
					$this->db->insert('referrals',$datas);
				}
			}

			/* Social site refer table END */
			$this->session->set_userdata('user_id',$insert_id);
			$this->session->set_userdata('user_email',$user_email);

			//set cookie
			setcookie("user_id", $insert_id, time() + (86400 * 30), "/"); // 86400 = 1 day
			setcookie("user_email", $user_email, time() + (86400 * 30), "/");

			$this->db->where('admin_id',1);
			$admin_det = $this->db->get('admin');
			if($admin_det->num_rows >0) 
			{    
				 $admin = $admin_det->row();
				 $admin_email = $admin->admin_email;
				 $site_name = $admin->site_name;
				 $admin_no = $admin->contact_number;
				 $site_logo = $admin->site_logo;
			}
			//if($admin->activate_method == 0)
			//{
				$date =date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */
				//$first_name = $this->input->post('first_name');
				$last_name  = $user_details->lastName;
				$user_email = $user_email;
			
				$this->db->where('mail_id',18);
				$mail_template = $this->db->get('tbl_mailtemplates');
			
				if($mail_template->num_rows >0) 
				{        
				    $fetch    	 = $mail_template->row();
				    $subject  	 = $fetch->email_subject; 
				    $templete 	 = $fetch->email_template;  
				    $regurl	 	 = base_url().'verify-account/'.$new_random;
				    $unsuburl	 = base_url().'un-subscribe-signup/'.$new_random;
				    
				    $Content = 'Thank you for registering with <a href='.base_url().'>'.$site_name.'</a>';

				   	$this->load->library('email'); 

					$config = Array(
					'mailtype'  => 'html',
					'charset'   => 'utf-8',
					);
					// $this->email->initialize($config);        
		     		$this->email->set_newline("\r\n");
					$this->email->initialize($config);        
					$this->email->from($admin_email,$site_name.'!');
					$this->email->to($user_email);
					$this->email->subject($subject);
				   	$data = array(
						'###EMAIL###'=>$first_name,
						'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
						'###SITENAME###'=>$site_name,
						'###ADMINNO###'=>$admin_no,
						'###DATE###'=>$date,
						'###CONTENT###'=>$Content
				   	);
				   
				    $content_pop=strtr($templete,$data);	
				    $this->email->message($content_pop);
				    $this->email->send();
				    /* echo $content_pop;
				    exit; */
				    return 1;   
			    }             
			//}
			return 1;
	    }		
	}


	//registration form..	
	function register()
	{
		
		$this->db->connection_check();
		$new_random    = mt_rand(1000000,99999999);
		$user_email    = $this->input->post('user_email');
		$date 	  	   = date('Y-m-d h:i:s');
		$uni_id   	   = $this->input->post('uni_id');
		$ref_id   	   = $uni_id;
		$pagename 	   = $this->input->post('pagename');
		$cat_type_url  = $this->input->post('categorytype');
	    $unic_code 	   = $this->random_string(10);
	 	
		//echo "<pre>";print_r($_POST); exit;
	   	
	    $arrcat_type   = array('3454'=>2,'8765'=>3,'2345'=>4,'1647'=>5,'6536'=>6,'7486'=>7,'8362'=>8,'9326'=>9,'2695'=>10);
	    if (array_key_exists($cat_type_url,$arrcat_type))
		{
	    	$categorytype = $arrcat_type[$cat_type_url];
	    }
	    else
	    {	
	    	$categorytype = 1;
	    }
		/*if($category_type ==3454)
		{
			$categorytype = 2;
		}
		elseif($category_type ==8765)
		{
			$categorytype = 3;	
		}
		elseif($category_type ==2345)
		{
			$categorytype = 4;	
		}
		elseif($category_type ==1647)
		{
			$categorytype = 5;	
		}
		else
		{
			$categorytype = 1;			
		}*/

		if($pagename)
		{
			$new_uid = $uni_id;
		}
		else
		{
			$new_uid = $ref_id;
		}
		if($new_uid)
		{	
			//echo $new_uid; exit;
			$this->db->where('random_code',$new_uid);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows() > 0)
			{	
				$fetch = $query->row();
				$user_id = $fetch->user_id;
				$ref_cat_type = $fetch->referral_category_type;

			}
			else
			{
				$user_id = 0;
			}
		}
		else
		{
			$user_id = 0;
			if($ref_cat_type == '')
			{
				$ref_cat_type = 0;
			}
		}

		$data = array(		
		//'first_name'=>$this->input->post('first_name'),
		/*'last_name'=>$this->input->post('last_name'),*/
		'email'=>$user_email,
		/*'street'=>$this->input->post('street'),
		'city'=>$this->input->post('city'),
		'state'=>$this->input->post('state'),
		'zipcode'=>$this->input->post('zipcode'),
		'country'=>$this->input->post('country'),
		'contact_no'=>$this->input->post('contact_no'),*/
		'password'=>$this->input->post('user_pwd'),
		'random_code'=>$new_random,
		'refer'=>$user_id,
		'cashback_mail'=>1,
		'withdraw_mail'=>1,
		'referral_mail'=>1,
		'acbalance_mail'=>1,
		'support_tickets'=>1,
		'newsletter_mail'=>1,
		'referral_category_type'=>$categorytype,
		'date_added'=>$date,
		'unic_bonus_code'=>$unic_code,
		'ref_user_cat_type'=>$ref_cat_type
		);
		
		$this->db->insert('tbl_users',$data,$datas);
		$insert_id = $this->db->insert_id();
		
		
		//New code for (type 2 format) user referral cashback amount for referred user (Pending status) 8-9-16//
		if($user_id !=0)
		{
			 
			$user_deta = $this->front_model->view_user($user_id);
			if($user_deta)
			{
				//echo "hai"; exit;
				//$refer = $user_deta->refer;
				//New code for refferal category type//
				$categorytype = $user_deta[0]->referral_category_type;
				
				
				$name   = $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();
				$status = $name->ref_by_rate;
				if($status == 1)
				{	
					$caspe = $name->ref_cashback_rate;
				}
				//End//
				
				if($status == 1)
				{

					//$cal_percent = ($trans_amount*$caspe)/100;
					$this->db->select_max('trans_id');
					$result   = $this->db->get('transation_details')->row();  
					$trans_id = $result->trans_id;
					$trans_id = $trans_id+1;
					$n9  	  = '5236555';
					$n12 	  = $n9 + $trans_id; 
					$now 	  = date('Y-m-d');
					$newid 	  = rand(1000,9999);
					$newtransaction_id = md5($newid);


					$data = array(		
					'transation_amount' => $caspe,
					'user_id' => $user_id,
					'transation_date' => $now,
					'transaction_date' => $now,
					'transation_id'=>$n12,
					'transation_reason' => "Pending Referal Payment",
					'mode' => 'Credited',
					'details_id'=>'',
					'table'=>'',
					'report_update_id'=>$newtransaction_id,
					'ref_user_tracking_id' =>$insert_id,
					'transation_status ' => 'Pending');
					$this->db->insert('transation_details',$data);
				}	
			}
		}
		//End refferal details 8-9-16//

		//New code for (type 3 format) user referral cashback amount count of referred user(Pending status) 9-9-16//
		$referrals = $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();	
		$ref_by_percentage  = $referrals->ref_by_percentage;
		$ref_by_rate 		= $referrals->ref_by_rate;
		$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;
		$category_names     = ucfirst($referrals->category_type);
		
		//3** Bonus by Refferal Rate type//
		if($bonus_by_ref_rate == 1)
		{
			
			$newid 	= rand(1000,9999);
			$newtransaction_id = md5($newid);

			$n9      = '333445';
			$n12     = $n9 + $user_id;
			$now     = date('Y-m-d H:i:s');	
			$selqry  = "SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Pending Referal Payment' AND user_id=$user_id"; 
			$query 	 =  $this->db->query("$selqry");
			$numrows = $query->num_rows();

			if($numrows > 0)
			{
				$fetch 			= $query->row();
				$usercount 		= $fetch->userid;
				$referrals      = $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();	
				$bonus_amount   = $referrals->ref_cashback_rate_bonus;
				$friends_count  = $referrals->friends_count;

				if($usercount == $friends_count)
				{	
					if($bonus_amount!='')
					{	 
						$referral_amt = $user_deta[0]->referral_amt;
						if($referral_amt == 0)
						{
							//$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`=Referral Bonus for $category_names User' AND `user_id`=$user_id"; 
							
							//$query1=$this->db->query("$selqry");
							//$newnumrows = $query1->num_rows();
							//if($newnumrows > 0)
							//{
								//$fetch = $query1->row();
								//$users_count = $fetch->userid;
								//if($users_count == 0)	
								//{	
									$data = array(			
									'transation_amount' => $bonus_amount,	
									'user_id' => $user_id,	
									'transation_date' => $now,
									'transaction_date' => $now,
									'transation_id'=>$n12,	
									'transation_reason' => 'Referral Bonus for '. ucfirst($category_names) .' User',	
									'mode' => 'Credited',
									'details_id'=>'',	
									'table'=>'',	
									'new_txn_id'=>0,
									'transation_status ' => 'Pending',
									'report_update_id'=>$newtransaction_id,
									'ref_bonus_type'=>$categorytype
									);	
									$this->db->insert('transation_details',$data);
								
									//User table update referral status//
									$data = array(		
									'referral_amt' => 1);
									$this->db->where('user_id',$user_id);
									$update_qry = $this->db->update('tbl_users',$data);	
									//End//

								//}	
							//}
								
						}		
					}	
				}
			} 
		}
		//End referral details 9-9-16//

		//New code for activation method 18-4-16//
		$admindetailssss = $this->front_model->getadmindetails_main();
		$activate  = $admindetailssss->activate_method;
		 
		//End//

		/*New code for Add a subscriber users list 31-8-16*/
		$this->db->where('subscriber_email',$user_email);
		$query = $this->db->get('subscribers');
		if($query->num_rows() == 0)
		{
			$date = date('Y-m-d h:m:s');
			$data = array(
			'subscriber_email' => $user_email,
			'subscriber_status' => '1',
			'date_subscribed' => $date
			);
			$this->db->insert('subscribers',$data);
		}
		/*End 31-8-16*/
		
		//$uni_id = $this->input->post('uni_id');
		
		if($new_uid)
		{
			$this->db->where('random_code',$new_uid);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows() > 0)
			{	
				$fetch = $query->row();
				$user_id = $fetch->user_id;
				$email =$fetch->email;
				
				$datas = array(
				'user_id' => $user_id,
				'user_email' => $email,
				'referral_email' => $user_email,
				'status' => 'Ativa',
				'date_added' => $date
				);
				$this->db->insert('referrals',$datas);
			}
		}
		
		/*New code for referral for cashback_exclusive via registered user 30-7-16*/
		$user_id 		  = $this->session->userdata('user_id'); 
		$cashback_details = $this->session->userdata('link_name');	
		$cashbackdetails  = $this->front_model->cashback_exclusive_details($cashback_details);
		$Email_id         = $cashbackdetails->user_email;
		$userdetailss     = $this->db->query("select * from tbl_users where email='$Email_id'")->row();
		$newuserid        = $userdetailss->user_id;
		if($user_id=='')
		{
			if($cashback_details !='')
			{
				$datas = array(
				'user_id' => $newuserid,
				'user_email' => $Email_id,
				'referral_email' => $user_email,
				'status' => 'Ativa',
				'date_added' => $date
				);
				$this->db->insert('referrals',$datas);
			}
		}
		/*End 30-7-16*/
		
		//return true;
		//send email 
		
		//New code for mail notification//
		//echo "hai".$activate; echo "<br>";
		if($activate == 0)
		{
		
			$this->db->where('admin_id',1);
			$admin_det = $this->db->get('admin');
			if($admin_det->num_rows >0) 
			{    
				$admin 		 = $admin_det->row();
				$admin_email = $admin->admin_email;
				$site_name 	 = $admin->site_name;
				$admin_no 	 = $admin->contact_number;
				$site_logo 	 = $admin->site_logo;
			}
			
			$date =date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */
			$first_name = $this->input->post('first_name');
			$last_name  = $this->input->post('last_name');
			$user_email = $this->input->post('user_email');
		
			$this->db->where('mail_id',1);
			$mail_template = $this->db->get('tbl_mailtemplates');
			
			if($mail_template->num_rows >0) 
			{        
			    $fetch    	 = $mail_template->row();
			    $subject  	 = $fetch->email_subject;  
			    $templete 	 = $fetch->email_template;  
			    $regurl	 	 = base_url().'verify-account/'.$new_random;
			    $unsuburl	 = base_url().'un-subscribe-signup/'.$new_random;

			   	$this->load->library('email'); 

				$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				);
				// $this->email->initialize($config);        
	     		$this->email->set_newline("\r\n");
				$this->email->initialize($config);        
				$this->email->from($admin_email,$site_name.'!');
				$this->email->to($user_email);
				$this->email->subject($subject);
			   	$data = array(
					'###USERNAME###'=>$first_name,
					'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
					'###SITENAME###'=>$site_name,
					'###ADMINNO###'=>$admin_no,
					'###DATE###'=>$date,
					'###LINK###'=>'<a href='.$regurl.'>'.$regurl.'</a>',
					'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>'
			   	);
			   
			    $content_pop=strtr($templete,$data);	
			    $this->email->message($content_pop);
			    $this->email->send();
			    /* echo $content_pop;
			    exit; */
			    //return true;                
			}
			return 0;
		}
		if($activate == 1)
		{
			//echo "hai"; exit;
			$datas = array('status'=>1);
			$this->db->where('user_id',$insert_id);
			$this->db->where('admin_status','');
			$update_qry = $this->db->update('tbl_users',$datas);

			$this->session->set_userdata('user_id',$insert_id);
			$this->session->set_userdata('user_email',$user_email);

			$this->db->where('admin_id',1);
			$admin_det = $this->db->get('admin');
			if($admin_det->num_rows >0) 
			{    
				$admin 		 = $admin_det->row();
				$admin_email = $admin->admin_email;
				$site_name 	 = $admin->site_name;
				$admin_no 	 = $admin->contact_number;
				$site_logo 	 = $admin->site_logo;
			}

			$date =date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */
			$this->db->where('mail_id',18);
			$mail_template = $this->db->get('tbl_mailtemplates');
		
			if($mail_template->num_rows >0) 
			{        
			    $fetch    	 = $mail_template->row();
			    //$subject  	 = 'User Registration - Social Login'; 
			    $subject 	 = $fetch->email_subject;
			    $templete 	 = $fetch->email_template;  
			    $Content 	 = 'Thank you for registering with <a href='.base_url().'>'.$site_name.'</a>';
			   	$this->load->library('email'); 
				$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				);       
	     		$this->email->set_newline("\r\n");
				$this->email->initialize($config);        
				$this->email->from($admin_email,$site_name.'!');
				$this->email->to($user_email);
				$this->email->subject($subject);
			   	$data = array(
					'###EMAIL###'=>$first_name,
					'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
					'###SITENAME###'=>$site_name,
					'###ADMINNO###'=>$admin_no,
					'###DATE###'=>$date,
					'###CONTENT###'=>$Content
			   	);
			   
			    $content_pop=strtr($templete,$data);	
			    $this->email->message($content_pop);
			    $this->email->send();
			    /*echo $user_email; echo "<br>";
			    echo $content_pop;
			    exit;*/
			    return 1;   
		    }
			return 1;
		}	
	}


		function random_string($length) 
		{
		$key = '';
		$keys = array_merge(range(0, 9), range('A', 'Z'));

		for ($i = 0; $i < $length; $i++) {
		$key .= $keys[array_rand($keys)];
		}

		return $key;
		}



	
	//check Email 
	function check_email($email)
	{
		$this->db->connection_check();
		$this->db->where('email',$email);
		$this->db->where('admin_status','');
		
		$qry = $this->db->get('tbl_users');
		$numrows1 = $qry->num_rows();
		if($numrows1 == 0)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	//check Email 
	function check_cpf($ifsc_code)
	{
		$this->db->connection_check();
		$this->db->where('ifsc_code',$ifsc_code);
		$this->db->where('admin_status','');
		
		$qry = $this->db->get('tbl_users');
		$numrows1 = $qry->num_rows();
		if($numrows1 == 0)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	

	
	//get all countries
	function get_allcounties()
	{
		$this->db->connection_check();
		$fetcountry = $this->db->get('country');
		if($fetcountry->num_rows > 0)
		{		
			return $fetcountry->result();
		}
		return false;
	}
	//get the particular users details
	function edit_account($user_id)
	{
		$this->db->connection_check();
	   $this->db->where('user_id',$user_id);
	   $this->db->where('admin_status','');
		$user_details = $this->db->get('tbl_users');
		if($user_details->num_rows > 0){
			return $user_details->row();
		}
		return false;	
	}
	//New code for Bank details page//

	function bank_details()
	{
		$this->db->connection_check();
		$this->db->order_by('bankid','desc');
		$result = $this->db->get('tbl_banknames');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	//End//
	//update the user details by SATz 
	function update_account()
	{
		//echo "<pre>";print_r($_POST); exit;
		$this->db->connection_check();
		$edit_id = $this->input->post('user_id');
		$contact_no = str_replace("_","",$this->input->post('contact_no'));
		$cell_no    = str_replace("_","",$this->input->post('cell_no'));
		
		$data = array(		
		'first_name' => $this->input->post('first_name'),
		'last_name' => $this->input->post('last_name'),
		'sex' => $this->input->post('sex'),
		'street' => $this->input->post('street'),
		'streetnumber' => $this->input->post('streetnumber'),
		'city' => $this->input->post('city'),
		'celular_no'=> $cell_no,
		'state' => $this->input->post('state'),
		'zipcode' => $this->input->post('zipcode'),
		'country' => $this->input->post('country'),
		'contact_no'=> $contact_no,
		'complemento'=> $this->input->post('complemento'),
		'bairro'=> $this->input->post('bairro')
		
		);
		
		$this->db->where('user_id',$edit_id);
		$this->db->where('admin_status','');
		$update_qry = $this->db->update('tbl_users',$data);
		if($update_qry)
		{
			return true;
		}
		else 
		{ 
			return false;
		}	
	}	
	
	function add_rating()
	{
		$this->db->connection_check();
		$cop_id = $this->input->post('id');
		$rating = $this->input->post('rating');
		$data = array(		
		'rating' =>  $rating
		);
		
		$this->db->where('affiliate_id',$cop_id);
		$this->db->where('affiliate_status','1');
		$update_qry = $this->db->update('affiliates',$data);
		if($update_qry)
		{
			return true;
		}
		else 
		{ 
			return false;
		}	
	}	
	
	function getunicbonuscount()
	{
		$userids 	= $this->input->post('userid');
		$this->db->where('user_id',$userids);
		$user_details = $this->db->get('tbl_users');
		//echo $this->db->last_query();
		$userdetails = $user_details->result();
		return  $userdetails[0]->bonus_benefit;
	}
	
	

	//New code for one time benefit bonus aded for user//
	function onetime_benefit()
	{
		$this->db->connection_check();
		$userids 	= $this->input->post('userid');

		$this->db->where('user_id',$userids);
		$user_details = $this->db->get('tbl_users');
		if($user_details->num_rows >0) 
		{	 
			$fetch    	  = $user_details->row();
		    $balance 	  = $fetch->balance;  
			$amount       = $this->input->post('amt');
		 	$total_amt    = $balance + $amount;
		 	$bonus_status = $fetch->bonus_benefit;

		 	$dataa = array(		
						'transation_reason' => "Unic bonus amonut Added",		
						'transation_amount' => $amount,		
						'mode' => "debited",
						'transation_status' => "Credited",			
						'transaction_date' => date('Y-m-d'),
						'transation_date' => date('Y-m-d'),			
						'user_id' => $userids	
						);
						$this->db->insert('transation_details',$dataa);
			if($bonus_status == 0)
			{
				$data = array('bonus_benefit' => 1,
				'balance'=> $total_amt
				);
				$this->db->where('user_id',$userids);
				$this->db->where('admin_status','');
				$update_qry = $this->db->update('tbl_users',$data);
			}	
			if($update_qry)
			{	 
				return 1;
			}
			else 
			{ 
				return 0;
			}
		} 
	}
	//End//

	//New code email notification page update//
	function notifymail_update()
	{	
		//echo "hai<pre>"; print_r($_POST); exit;
		$this->db->connection_check();
		$this->load->library('email');
		$postuser 	= $this->input->post('user_id');
		$caseback 	= $this->input->post('cashback');
		$withdraw 	= $this->input->post('withdraw');
		$referral   = $this->input->post('referral');
		$acbalance  = $this->input->post('acbalance');
		$newsletter = $this->input->post('newsletter');
		$support_tickets = $this->input->post('support_tickets');
		//echo $caseback;

		if((isset($caseback)) && ($caseback =='on'))
		{
			$caseback = '1';
		}
		else
		{
			$caseback = '0';
		}
		if((isset($withdraw)) && ($withdraw =='on'))
		{
			$withdraw = '1';
		}
		else
		{
			$withdraw = '0';
		}
		if((isset($referral)) && ($referral =='on'))
		{
			$referral = '1';
		}
		else
		{
			$referral = '0';
		}
		if((isset($acbalance)) && ($acbalance =='on'))
		{
			$acbalance = '1';
		}
		else
		{
			$acbalance = '0';
		}
		if((isset($newsletter)) && ($newsletter =='on'))
		{
			$newsletter = '1';
		}
		else
		{
			$newsletter = '0';
		}

		if((isset($support_tickets)) && ($support_tickets =='on'))
		{
			$support_tickets = '1';
		}
		else
		{
			$support_tickets = '0';
		}
		
		$news_email = $this->db->get_where('tbl_users',array('user_id'=>$postuser))->row('newsletter_mail');

		$data = array(		
		'cashback_mail'   => $caseback,
		'withdraw_mail'   => $withdraw,
		'referral_mail'   => $referral,
		'acbalance_mail'  => $acbalance,
		'newsletter_mail' => $newsletter,
		'support_tickets' => $support_tickets
		);

		$this->db->where('user_id',$postuser);
		$this->db->where('admin_status','');
		$update_qry = $this->db->update('tbl_users',$data);
		if($update_qry)
		{
			
			/*New code for update a subscribers table 3-9-16*/
				$email 		= $this->db->get_where('tbl_users',array('user_id'=>$postuser))->row('email');
				
				$data = array(		
				'subscriber_status' =>$newsletter,
				);
				$this->db->where('subscriber_email',$email);	
				$this->db->update('subscribers',$data);
			/*end */	

			//echo $newsletter; exit; 
			/*New code for email notification for newsletter subscriber 31-8-2016*/
			if($news_email == 1 && $newsletter == 0)
			{ 

				/*$admin_email = $this->db->get_where('admin', array('admin_id'=>'1'))->row('admin_email');
				$message 	 = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">
								Você está recebendo esta beacuse você é um membro Pingou . Você pode apenas <a href='.base_url().'un-subscribe/subscribers>un-subscrever</a> se você quiser.
								</span>';
				$emailid 	= $this->db->get_where('tbl_users',array('user_id'=>$postuser))->row('email');
				
				if($emailid) 
				{	
					
					$subject = "Your Un Subscribers Notification"; 
					$config = Array(
					'mailtype'  => 'html',
					'charset'   => 'utf-8'
					);  	 

					$this->email->set_newline("\r\n");
					$this->email->initialize($config);
					$this->email->from($admin_email);
					$this->email->to($emailid);
					//$this->email->bcc($emails);
					$this->email->subject($subject);
					$this->email->message($message);
					$this->email->send();
	        		$this->email->print_debugger();			
        		}*/
			}
			/*end 31-8-16*/
			return true;
		}
		else 
		{ 
			return false;
		}
	}
	
	//End//
	
	//update password
	function update_password()
	{
		$this->db->connection_check();
		 $old_password = $this->input->post('old_password');
		 $new_password = $this->input->post('new_password');
		 $id = $this->input->post('user_id');
		
			$where = array('password'=>$old_password,'user_id'=>$id);
			$this->db->where($where);
			
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(
				'password'=>$new_password
				);
				//print_r($data);
				//exit;
				$this->db->where('user_id',$id);	
				$this->db->update('tbl_users',$data);
				return true;
			}    
			else 
			{     
				return false;
			}			
	}
	
	//bankpayment form
	function bankpayment()
	{
		$this->db->connection_check();
		 $new_id = $this->input->post('user_id');
		 $con_pwd = $this->input->post('con_pwd');
		 
		$where = array('password'=>$con_pwd,'user_id'=>$new_id);
		$this->db->where($where);
		$query = $this->db->get('tbl_users');
		if($query->num_rows >= 1) 
		{
			$data = array(		
			'account_holder' => $this->input->post('act_holder'),
			'bank_name' => $this->input->post('bank_name'),
			'branch_name' => $this->input->post('bank_brch_name'),
			'account_number' => $this->input->post('act_no'),
			'ifsc_code' => $this->input->post('ifsc_code')
			);
			//print_r($data);
			//exit;
			$this->db->where('user_id',$new_id);	
			$this->db->update('tbl_users',$data);
			return true;
		}    
		else 
		{     
			return false;
		}			
	}

	//bankpayment form
	function bankpayment_ifsc()
	{
		$this->db->connection_check();
		 $new_id = $this->input->post('user_id');
		 $con_pwd = $this->input->post('con_pwd');
		 
		$where = array('password'=>$con_pwd,'user_id'=>$new_id);
		$this->db->where($where);
		$query = $this->db->get('tbl_users');
		if($query->num_rows >= 1) 
		{
			$data = array(		
			'account_holder' => $this->input->post('act_holder'),
			'bank_name' => $this->input->post('bank_name'),
			'branch_name' => $this->input->post('bank_brch_name'),
			'account_number' => $this->input->post('act_no')
			);
			$this->db->where('user_id',$new_id);	
			$this->db->update('tbl_users',$data);
			return true;
		}    
		else 
		{     
			return false;
		}			
	}

	//get all state
	function get_state($user_id)
	{
		$this->db->connection_check();
		$result = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('country');
		//print_r($result);
		
		$this->db->where('country_id',$result);
		$res = $this->db->get('state');
		if($res->num_rows > 0){
			return $res->result();
		}
		return false;	
		
	}
	//cheque_payment
	function cheque_payment()
	{
		$this->db->connection_check();
		$us_id = $this->input->post('user_id');
		$con_pwd = $this->input->post('cheque_confirm_pwd');
		
		$where = array('password'=>$con_pwd,'user_id'=>$us_id);
		$this->db->where($where);
		$query = $this->db->get('tbl_users');
		if($query->num_rows >= 1) 
		{
			$data = array(		
			'cheque_full_name' => $this->input->post('cheque_full_name'),
			'cheque_full_address' => $this->input->post('cheque_full_adr'),
			'cheque_city' => $this->input->post('cheque_city'),
			'cheque_state' => $this->input->post('cheque_state'),
			'cheque_postel_code' => $this->input->post('cheque_postal_code'),
			'cheque_contact_no' => $this->input->post('cheque_contact_no')
			);
			//print_r($data);
			//exit;
			$this->db->where('user_id',$us_id);	
			$this->db->update('tbl_users',$data);
			return true;
		}    
		else 
		{     
			return false;
		}	
	}
	
	//forget password
	function forgetpassword()
	{
		$this->db->connection_check();

		$email = $this->input->post('email');
		//send email 
		$this->load->library('email');
		$this->db->where('admin_id',1);
		$admin_det = $this->db->get('admin');
		
		if($admin_det->num_rows >0) 
		{    
			$admin 	     = $admin_det->row();
			$admin_email = $admin->admin_email;
			$site_name   = $admin->site_name;
			$admin_no 	 = $admin->contact_number;
			$site_logo   = $admin->site_logo;
		}
		
		$date =date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */
		
		$this->db->where('email',$email);
		$this->db->where('admin_status','');
		$query = $this->db->get('tbl_users');
		if($query->num_rows >0) 
		{
			$getuser 	= $query->row();
			$user_id    = $getuser->user_id;
			$password   = $getuser->password;
			$first_name = $getuser->first_name;
			$last_name  = $getuser->last_name;
		    $user_email = $getuser->email;
			$random_id  = $getuser->random_code;
			$this->db->where('mail_id',2);
			$mail_template = $this->db->get('tbl_mailtemplates');
			if($mail_template->num_rows >0) 
			{        
			    $fetch 	  = $mail_template->row();
			    $subject  = $fetch->email_subject;  
			    $templete = $fetch->email_template;  
			  	$regurl   = base_url().'password_reset/'.insep_encode($user_id);
			    
			 	$config = Array(
				 'mailtype' => 'html',
				  'charset' => 'utf-8',
				);
     			// $this->email->initialize($config);        
     			$this->email->set_newline("\r\n");
			    
			    $this->email->initialize($config);        
			    $this->email->from($admin_email,$site_name.'!');
			    $this->email->to($user_email);
			    $this->email->subject($subject);

			    $data = array(
					'###USERNAME###'=>$first_name.' '.$last_name,
					'###PASSWORD###'=>$password,
					'###SITENAME###'=>$site_name,
					'###ADMINNO###'=>$admin_no,
					'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
					'###DATE###'=>$date,
					'###LINK###'=>'<a href='.$regurl.'>'.'Click here'.'</a>'
				);
			   
			   $content_pop=strtr($templete,$data); 
			   $this->email->message($content_pop);
			   $this->email->send();	          
			}
			return true;          
		}
		else
		{
			return false;                
		}   	
	}
	
	//reset_password
	function reset_password($user_id)
	{
		/*echo $user_id;
		exit;*/
		$this->db->connection_check();
		if(!isset($user_id))
		{
			$user_id = $this->input->post('user_id');
		}
		$new_password = $this->input->post('new_password');
		$confirm_password = $this->input->post('confirm_password');
					
		$where = array('user_id'=>$user_id);
		// print_r($where);
		// exit;
		$this->db->where($where);
		
		$query = $this->db->get('tbl_users');
		if($query->num_rows >0) 
		{
			$data = array(
			'password'=>$new_password
			);
			//print_r($data);
			//exit; 
			$this->db->where('user_id',$user_id);	
			$this->db->update('tbl_users',$data);
			return true;
		}    
		else 
		{     
			return false;
		}			
	}
	//refer_friends
	function get_random($user_id)
	{
		$this->db->connection_check();
		$res =$this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('random_code');
			
			return $res;
	}
//End (SL)
//End (SL)

/*********************Nathan Start*************************/
/******Nov 19 th*********/
	function get_category_details($categoryurl) //get_category_details
	{
		$this->db->connection_check();
			$this->db->where('category_url',$categoryurl);
			$query = $this->db->get('categories');
			if($query->num_rows >= 1)
			{
			   $row = $query->row();
			   return $row;
			}
			return false;
	}
	
	function get_category_details_byid($categoryid) //get_category_details
	{
		$this->db->connection_check();
			$this->db->where('category_id',$categoryid);
			$query = $this->db->get('categories');
			if($query->num_rows >= 1)
			{
			   $row = $query->row();
			   return $row;
			}
			return false;
	}
	
	

	function get_subcategories($category_id)
	{
		$this->db->connection_check();
			$this->db->where('cate_id',$category_id);
			$query = $this->db->get('sub_categories');
			if($query->num_rows >= 1)
			{
			   $row = $query->row();
			   return $query->result();
			}
			return false;
	}
	
	function get_coupons($categories)
	{
		$this->db->connection_check();
		$k=0;
		foreach($categories as $catenames)
		{
			if($k==0)
			{
				$this->db->like('title', $catenames);				
			}
			else
			{
				$this->db->or_like('title', $catenames); 
			}
			$k++;
		}
		
		
		
	}
	
	function get_stores_list($category_id)
	{
		$this->db->connection_check();
		$this->db->order_by("sort_order", "ASC");		
		$this->db->where("FIND_IN_SET('$category_id',store_categorys) !=", 0);
		$this->db->where('affiliate_status','1');
		$query = $this->db->get('affiliates');
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
	
	function referal_percentage()
	{
		$this->db->connection_check();
		$query = $this->db->get('admin');
		if($query->num_rows >= 1)
		{
		   return $query->row('referral_cashback');
		}
	}


//New code for satheesh//

function referal__category_percentage($category)
{
		$this->db->connection_check();
		$this->db->where('ref_id',$category);
		$query = $this->db->get('referral_settings');
		if($query->num_rows >= 1)
		{
		   return $query->row('ref_cashback');
		}
}

function referal__category_description($category)
{
	$this->db->connection_check();
		$this->db->where('ref_id',$category);
		$query = $this->db->get('referral_settings');
		if($query->num_rows >= 1)
		{
		   return $query->row('cat_description');
		}
}
	
function referal__category()
{
		$this->db->connection_check();
			$user_id = $this->session->userdata('user_id');
			if($user_id){
	        $this->db->where('user_id',$user_id);
			$query = $this->db->get('tbl_users');
		if($query->num_rows >= 1)
		{
		   return $query->row('referral_category_type');
		}
			}
}
	
//End//

	//New code for referral percentage//


	//End//
	
	// get all faqs..
	function get_allfaqs(){
		$this->db->connection_check();
		$this->db->where('status','1');
		$allfaqs = $this->db->get('tbl_faq');
		if($allfaqs->num_rows > 0)
        {
            $row = $allfaqs->row();
            return $allfaqs->result();
        }
		else
		{
			return false;
		}
	}
	
	function user_balance($user_id=null)
	{
		$this->db->connection_check();
		if($user_id!="")
		{
			$this->db->where('user_id',$user_id);
			$this->db->where('admin_status','');
			$allfaqs = $this->db->get('tbl_users');
			return $allfaqs->row("balance");
		}
		else
		{
			return 0;
		}
		
	}
	
	
	function missing_cashback($user_id=null)
	{
		$this->db->connection_check();
		if($user_id!="")
		{	
			$this->db->order_by('cashback_id','desc');
			$this->db->where('user_id',$user_id);
			//$this->db->where('missing_reason','Missing Cashback');
			$miss_cashbacks = $this->db->get('missing_cashback');
			//echo $this->db->last_query();die;
			return $miss_cashbacks->result();
		}
		else
		{
			return 0;
		}
	}
	
	function minimum_withdraw()
	{
		$this->db->connection_check();
			$minimum_cashback = $this->db->get('admin');
			return $minimum_cashback->row("minimum_cashback");
	}
	
	function my_payments($user_id)
	{
		$this->db->connection_check();		
		if($user_id!="")
		{	
			$this->db->where('user_id',$user_id);
			$this->db->order_by('withdraw_id','desc');
			$miss_cashbacks = $this->db->get('withdraw');
			
			return $miss_cashbacks->result();
		}
		else
		{
			return 0;
		}
	}
	
	function update_user_balance($userid,$requestpay,$ifsc_code)
	{
		//balance
		$this->db->connection_check();
		$requestpay    = str_replace(',','.', $requestpay);
		$userbalance   = $this->user_balance($userid);
		$userbalance   = str_replace(',','.', $userbalance);  
		$new_balnce    = $userbalance - $requestpay; 
		$accountholder = $this->db->get_where('tbl_users',array('user_id'=>$userid))->row('account_holder');
		$ubank_name    = $this->db->get_where('tbl_users',array('user_id'=>$userid))->row('bank_name');
		$branch_name   = $this->db->get_where('tbl_users',array('user_id'=>$userid))->row('branch_name');
		$accountnumber = $this->db->get_where('tbl_users',array('user_id'=>$userid))->row('account_number');
		/*New code Changes 2-8-16*/
		$bank_name     = $this->db->get_where('tbl_banknames',array('bank_id'=>$ubank_name))->row('bank_name');
		$ifsc_code     = str_replace(array('.', '-'), '' , $ifsc_code);
		/*End*/

		$User_details = $this->front_model->userdetails($userid);

		$data = array(		
		'balance' => $new_balnce);
		$this->db->where('user_id',$userid);
		$update_qry = $this->db->update('tbl_users',$data);
		if($update_qry)
		{
			$now = date('Y-m-d H:i:s');
			// withdraw
			$data = array(		
			'requested_amount' => $requestpay,
			'user_id'          => $userid,
			'date_added'       => $now,
			'account_holder'   => $accountholder,
			'bank_name'        => $bank_name,
			'branch_name'      => $branch_name,
			'account_number'   => $accountnumber,
			'ifsc_code' 	   => $ifsc_code,
			'bank_id'		   => $ubank_name,
			'status ' => 'Requested');
			$this->db->insert('withdraw',$data);

			$withdraw_mail = $User_details->withdraw_mail;
			/*new code for username details 5-1-17*/
			$firstname     = $User_details->first_name;
			$lastname 	   = $User_details->last_name;
			if($firstname == '' && $lastname == '')
			{
				$ex_name  = explode('@',$User_details->email);
				$username = $ex_name[0]; 
			}
			else
			{
				$username = $firstname.''.$lastname;
			}	
			/*End 5-1-17*/
			if($withdraw_mail == 1)
			{
				$mail_temp 		 = $this->db->query("select * from tbl_mailtemplates where mail_id='17'")->row();
				$fe_cont   		 = $mail_temp->email_template;
				$admindetailssss = $this->front_model->getadmindetails_main(); 
				//$subject 		 = $admindetailssss->site_name." has recieved your Withdraw Ticket";
				$subject 		 = $mail_temp->email_subject;
				$name 			 = $this->db->query("select * from admin")->row();
				$admin_emailid   = $name->admin_email;
				$site_logo 		 = $name->site_logo;
				$site_name  	 = $name->site_name;
				$contact_number  = $name->contact_number;
				$servername 	 = base_url();
				$unsuburl     	 = base_url().'un-subscribe/withdraw/'.$userid;

				$nows = date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */
				$this->load->library('email');
				
				//Pilaventhiran 05/05/2016 START
				$see_status_withdraw = "<a href='".base_url()."resgate'>status do resgate</a>";
				$gd_api=array(
					'###ADMINNO###'=>$contact_number,
					'###EMAIL###'=>$username,
					'###DATE###'=>$nows,
					'###CONTENT###'=>$mail_text,
					'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
					'###SITENAME###' =>$site_name,
					'###SEE_STATUS_WITHDRAW###'=>$see_status_withdraw,
					'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>'
                	
					);
				//Pilaventhiran 05/05/2016 END
					   
				$gd_message=strtr($fe_cont,$gd_api);
				//echo $gd_message;exit;
				/*
				$config['protocol'] = 'sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				*/
			
				$config = Array(
					 'mailtype'  => 'html',
					  'charset'   => 'utf-8',
					  );
				$mail = $User_details->email;
				//$list = array($mail, $admin_emailid);
				
				$this->email->initialize($config);
				$this->email->set_newline("\r\n");
				$this->email->from($admin_emailid,$site_name.'!');
				$this->email->to($mail);
				$this->email->subject($subject);
				$this->email->message($gd_message);
				$this->email->send();
				$this->email->print_debugger();
			}
			return true;
		}
		else 
		{ 
			return false;	
		}	
	}
	
	function paid_earnings($userid)
	{	
		$this->db->connection_check();
		$this->db->select('SUM(requested_amount) as completed_bal');
		$this->db->where('status','Completed');
		$this->db->where('user_id',$userid);
		$paid_earning= $this->db->get('withdraw');
		if($paid_earning->num_rows > 0)
        {
            return $paid_earning->row('completed_bal');
        }
		else
		{
			return false;
		}
	}
	
	function total_earnings($userid)
	{	
		$this->db->connection_check();
		$this->db->select('SUM(transation_amount) as completed_bal');
		$this->db->where('user_id',$userid);
		$this->db->where('mode','Credited');
		$paid_earning= $this->db->get('transation_details');
		$waitng = $paid_earning->row('completed_bal');
		//echo $userid;
		 $balcne =  $this->user_balance($userid);	
		 //exit;
		//$newbalset =  $waitng+$balcne;
		if($paid_earning->num_rows > 0)
        {
            return $balcne;
        }
		else
		{
			return false;
		}
	}
	
	function waiting_approval($userid)
	{
		$this->db->connection_check();
		$this->db->select('SUM(requested_amount) as completed_bal');
		$this->db->where('user_id',$userid);
				$this->db->where('status','Process');
		$paid_earning= $this->db->get('withdraw');		
		if($paid_earning->num_rows > 0)
        {
            return $paid_earning->row('completed_bal');
        }
		else
		{
			return false;
		}
	}
	
	function get_all_categories($count=null)
	{
		$this->db->connection_check();
		if($count!="")
		{
			$this->db->limit($count,0);
		}	
		//$this->db->order_by('sort_order');
		$this->db->where('category_status','1');
		$result = $this->db->get('categories');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	
	function get_all_stores($count=null)
	{
		$this->db->connection_check();
		if($count!="")
		{
			$this->db->limit($count,0);
		}
		$this->db->where('affiliate_status','1');
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	
	function getmaincategorys()
	{
		$this->db->connection_check();
		$results = $this->db->query("SELECT
			category_id,category_name,category_url,
			count(c.cate_id) AS num_categorys
			FROM
				categories AS p
				JOIN sub_categories AS c 
					ON p.category_id = c.cate_id
			where p.category_status=1 
			GROUP BY c.cate_id
			order by p.sort_order ASC");
			
			if($results->num_rows > 0){
				return $results->result();	
			}
			return false;
	}
	
	function get_sub_categorys_list($category_id)
	{
		$this->db->connection_check();
		$results = $this->db->query("SELECT * FROM `sub_categories` where cate_id=$category_id and category_status=1 order by sort_order ASC");
		if($results->num_rows > 0)
		{
				return $results->result();	
		}
		else
		{
			return false;
		}
	}
	
	function get_stores_category()
	{
		$this->db->connection_check();
		$results = $this->db->query("SELECT * FROM categories as a WHERE a.category_id NOT IN (SELECT cate_id FROM sub_categories)");
		if($results->num_rows > 0)
		{
				return $results->result();	
		}
		else
		{
			return false;
		}
	}
	
	
	function get_available_store_cate()
	{
		$this->db->connection_check();
		$results = $this->db->query("SELECT 
		category_id,category_url, category_name 
		FROM `categories` as c 
		join (select store_categorys from affiliates) as a 
		WHERE 
		c.category_status=1 and FIND_IN_SET(c.category_id,a.store_categorys) 
		GROUP BY category_id order by c.sort_order ASC");
		
		if($results->num_rows > 0)
		{
				return $results->result();	
		}
		else
		{
			return false;
		}
	}
	
	function get_available_affiliates($cateid)
	{
		$this->db->connection_check();
		//$this->db->order_by("sort_order", "ASC");
		$results = $this->db->query("SELECT affiliate_url,affiliate_name,affiliate_id from affiliates as a WHERE affiliate_status=1 and FIND_IN_SET($cateid,a.store_categorys) ORDER BY  a.sort_order ");
		if($results->num_rows > 0)
		{
				return $results->result();	
		}
		else
		{
			return false;
		}
	}
	
	function sub_category_details($subcateurl=null)
	{
		$this->db->connection_check();
		if($subcateurl=='')
		{
			return false;
		}
		$factal_subcat = $this->db->query("SELECT sun_category_id as category_id,sub_category_url as category_url,sub_category_name as category_name, meta_keyword, meta_description FROM `sub_categories` where sub_category_url='$subcateurl'");
		if($factal_subcat->num_rows > 0)
        {
            return $factal_subcat->row();
        }
		else
		{
			return false;
		}
	}
	
	function count_coupons($catename=null)
	{
		$date = date('Y-m-d'); 
		$this->db->connection_check();
		$count_coupons = $this->db->query("SELECT count(*) as counting FROM `coupons` where offer_name like '%$catename%' and expiry_date >='$date'");
		if($count_coupons->num_rows > 0)
        {
            return $count_coupons->row();
        }
		else
		{
			return false;
		}
	}
	
	function get_coupons_sets($store,$coupon_count=null)
	{
		$this->db->connection_check();
		if($coupon_count!="")
		{
			$this->db->limit($coupon_count,0);
		}	
		$this->db->like('offer_name', $store);	
		$result = $this->db->get('coupons');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	/******Nov 19 th*********/
	/******Nov 26 th*********/
	
	function get_store_details($affiliate_url=null)
	{
		$this->db->connection_check();
		//$this->db->where('affiliate_id',$affiliate_id);
		$this->db->like('affiliate_url', $affiliate_url);	
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->row();	
		}
			return false;
	}
	//New code for store details//

	function all_store_details()
	{	
 
		$this->db->connection_check();
		$storedetail = $this->db->get('tbl_stores');
		if($storedetail->num_rows > 0){
			foreach($storedetail->result() as $row)
			{
				$data[] = $row;
			}
		 	return $data;
		}
			
	}
	//end//
	
	function get_coupons_from_store($store,$coupon_count=null)
	{
		//echo $expiry; exit;
		$user_id = $this->session->userdata('user_id');
		$date = date('Y-m-d');
		$this->db->connection_check();
		if($coupon_count!="")
		{
			$this->db->limit($coupon_count,0);
		}

		$this->db->order_by('coupon_options','desc');
		$this->db->where('offer_name', $store);
		$this->db->where('expiry_date >=', $date);
		$result = $this->db->get('coupons');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	
	//New code for expiry coupons list in pefore Login pages //

	function expiry_coupons_from_store($store,$coupon_count=null)
	{
		$user_id = $this->session->userdata('user_id');
		$date = date('Y-m-d');
		$this->db->connection_check();
		if($coupon_count!="")
		{
			$this->db->limit($coupon_count,0);
		}

		$this->db->order_by('coupon_options','desc');
		$this->db->where('offer_name', $store);
		$this->db->where('expiry_date <', $date);
		$result = $this->db->get('coupons');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	} 

	//End//

	//New code for Expiry coupons Insert into Expiry coupons Table//

	//End//


/******Nov 26 th*********/

	function get_store_details_byid($affiliate_id=null)
	{
		$this->db->connection_check();
		$this->db->where('affiliate_id',$affiliate_id);
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->row();	
		}
			return false;
	}

	/*New code for premium coupons details 11-1-17*/
	function get_premiumcoupons_details_byid($shop_id=null)
	{
		$this->db->connection_check();
		$this->db->where('shoppingcoupon_id',$shop_id);
		$result = $this->db->get('shopping_coupons');
		if($result->num_rows > 0){
			return $result->row();	
		}
			return false;	
	}

	function get_premiumcategorys_details_byid($shop_id=null)
	{
		$this->db->connection_check();
		$this->db->where('category_id',$shop_id);
		$result = $this->db->get('premium_categories');
		if($result->num_rows > 0){
			return $result->row();	
		}
			return false;	
	}
	/*End 11-1-17*/

	function get_coupons_from_coupon_byid($coupon_id)
	{	
		$date = date('m/d/Y');
		$this->db->connection_check();
		$this->db->where('coupon_id',$coupon_id);
		//$this->db->where('expiry_date >' $date);
		$result = $this->db->get('coupons');
		if($result->num_rows > 0)
		{
			return $result->row();	
		}
			return false;
	}
	
	// get admin details..
	function getadmindetails()
	{
		$this->db->connection_check();
		$this->db->where('admin_id','1');
		$query_admin = $this->db->get('admin');
		if($query_admin->num_rows >= 1) 
		{
			$row = $query_admin->row();
			return $query_admin->result();
		}
		else
		{
			return false;		
		}	
	}	
	
	function available_for_provider($affid)
	{
		$this->db->connection_check();
		$this->db->where('affiliate_name',$affid);
		$query_admin = $this->db->get('providers');
		if($query_admin->num_rows >= 1) 
		{
			return $query_admin->row();
		}
		else
		{
			return false;		
		}	
	}
	
	function click_history($store_id,$coupon_id,$userid,$useragent)
	{
		$this->db->connection_check();
		$store_details = $this->get_store_details_byid($store_id);
		
		$coupon_details  = $this->get_coupons_from_coupon_byid($coupon_id);		
		$voucher_name 	 = $coupon_details->title;
		$store_name   	 = $store_details->affiliate_name;
		$store_url   	 = $store_details->affiliate_url;
		$store_id 		 = $store_details->affiliate_id;
		$ip_address  	 = $this->input->ip_address();
		$admindetailssss = $this->front_model->getadmindetails_main(); 
			if($store_details->cashback_percentage!="")
			{	
				$cppercentage 	= $store_details->cashback_percentage;
				$cashback_per   = $this->session->userdata('cashback_web');
				$affiliate_urls = $this->session->userdata('affiliate_urls');
				
				if($store_url == $affiliate_urls)
				{
					if($cashback_per !='')
					{
						$cppercentage = $cashback_per;
					}
				}
				
				
				$voucher_name .=  " + Get additional ".$cppercentage."% Cashback from ".$admindetailssss->site_name;
			}
			else
			{
				$voucher_name .= "";
			}			
				
			/*New code for remove symbols and other characters in storename 11-1-17*/
			//$store_url  = $this->front_model->seoUrl($store_name);
			/*End*/	


			$current_date_time = date('Y-m-d H:i:s');
			$data = array(
			'voucher_name' => $voucher_name,
			'store_name' => $store_name,
			'affiliate_id' => $store_id,
			'user_id' => $userid,
			'ip_address'=>$ip_address,
			'date_added' => $current_date_time,
			'store_url' => $store_url
			);
			$this->db->insert('click_history',$data);
			return true;
	}

	/*New code for save premium coupons details in click history table 11-1-17*/
	function newclick_history($store_id,$coupon_id,$userid,$useragent)
	{	
		/*echo $store_id; echo "<br>";
		echo $coupon_id; echo "<br>";
		echo $userid; echo "<br>";
		exit;*/

		$this->db->connection_check();
		$store_details = $this->get_store_details_byid($store_id);
		
		$coupon_details  = $this->get_coupons_from_coupon_byid($coupon_id);		
		$voucher_name 	 = $coupon_details->title;
		$store_name   	 = $store_details->affiliate_name;
		$store_url   	 = $store_details->affiliate_url;
		$store_id 		 = $store_details->affiliate_id;
		$ip_address  	 = $this->input->ip_address();
		$admindetailssss = $this->front_model->getadmindetails_main(); 
			if($store_details->cashback_percentage!="")
			{	
				$cppercentage 	= $store_details->cashback_percentage;
				$cashback_per   = $this->session->userdata('cashback_web');
				$affiliate_urls = $this->session->userdata('affiliate_urls');
				
				if($store_url == $affiliate_urls)
				{
					if($cashback_per !='')
					{
						$cppercentage = $cashback_per;
					}
				}
				
				
				$voucher_name .=  " + Get additional ".$cppercentage."% Cashback from ".$admindetailssss->site_name;
			}
			else
			{
				$voucher_name .= "";
			}			
				
			/*New code for remove symbols and other characters in storename 11-1-17*/
			//$store_url  = $this->front_model->seoUrl($store_name);
			/*End*/	

			$current_date_time = date('Y-m-d H:i:s');
			$data = array(
			'voucher_name' => $voucher_name,
			'store_name' => $store_name,
			'affiliate_id' => $store_id,
			'user_id' => $userid,
			'ip_address'=>$ip_address,
			'date_added' => $current_date_time,
			'store_url' => $store_url
			);
			$this->db->insert('click_history',$data);
			return true;
	}
	/*end 11-1-17*/
	
	function verify_account($verifyid)
	{
		$this->db->connection_check();
		$where = array('random_code'=>$verifyid);
		$this->db->where($where);
		$this->db->where('admin_status','');
		$query = $this->db->get('tbl_users');
		if($query->num_rows >= 1) 
		{
			$data = array(		
			'status' => 1,
			);
			$this->db->where('random_code',$verifyid);	
			$this->db->update('tbl_users',$data);
			
			$fetch = $query->row();
			$user_id = $fetch->user_id;
			$user_email=$fetch->email;
			$this->session->set_userdata('user_id',$user_id);
			$this->session->set_userdata('user_email',$user_email);
			return 1;
		}  
		else
		{
			return 0;
		}  
	}
	//New Code for Un-sbuscribe email//
	function un_subscribe_signup($verifyid)
	{
		$this->db->connection_check();
		$where = array('random_code'=>$verifyid);
		$this->db->where($where);
		$this->db->where('admin_status','');
		$query = $this->db->get('tbl_users');
		if($query->num_rows >= 1) 
		{
			$data = array(		
			'status' => 0,
			);
			$this->db->where('random_code',$verifyid);	
			$this->db->update('tbl_users',$data);
			
			//$fetch = $query->row();
			$user_id = $fetch->user_id;
			$user_email=$fetch->email;

			//$this->session->set_userdata('user_id',$user_id);
			//$this->session->set_userdata('user_email',$user_email);
			return 1;
		}  
		else
		{
			return 0;
		}
	}


	function un_subscribe($type,$userid)
	{
		
		//echo $type; echo $userid;exit; 
		$this->db->connection_check();
		if($type =='cashback')
		{
			//echo "hai".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'cashback_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		} 
		if($type =='myaccount')
		{
			//echo "hai1".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'acbalance_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		} 
		if($type =='referral')
		{
			//echo "hai2".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'referral_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		}
		if($type =='newsletter')
		{
			//echo "hai2".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'newsletter_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		}
		if($type =='subscribers')
		{
			//echo "hai2".$userid; exit;
			$udetails    = $this->db->query("select * from tbl_users where user_id=$userid")->row();
			$emailid     = $udetails->email;
			$news_status = $udetails->newsletter_mail;

			if($news_status == 1)
			{
				$data = array(		
				'newsletter_mail' => 0,		
				);
				$this->db->where('user_id',$userid);
				$update_qry = $this->db->update('tbl_users',$data);
			}

			$where = array('subscriber_email'=>$emailid,'subscriber_status'=>1);
			$this->db->where($where);
			$query = $this->db->get('subscribers');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'subscriber_status' => 0,
				);
				$this->db->where('subscriber_email',$emailid);	
				$this->db->update('subscribers',$data);
				$user_id = $fetch->subscriber_id;
				//$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		}
		/*New code for withdraw unsubscribe details 9-1-17*/
		if($type =='withdraw')
		{
			//echo "hai".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'withdraw_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		} 
		/*End*/
		/*New code for missing cashback unsubscribe details 23-1-17*/
		if($type =='missing_cashback' || $type =='missing_approval')
		{
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'support_tickets' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		}
		/*End*/
	}
	//End// 
	
	/************ Dec 8th *************/
	function get_details_ajax($ajdate,$user_id)
	{
		$this->db->connection_check();
		$this->db->where('DATE(`date_added`)',$ajdate);
		$this->db->where('user_id',$user_id);
		$this->db->group_by('store_name'); 
		$query_details_ses = $this->db->get('click_history');
		
		if($query_details_ses->num_rows >= 1) 
		{
			return $query_details_ses->result();
		}
		else
		{
			return false;		
		}	
	}
	
	function get_clicked_details_ajax($ajdate,$click_store,$user_id)
	{
		$this->db->connection_check();
		$this->db->where('DATE(`date_added`)',$ajdate);
		$this->db->where('store_name',$click_store);
		$this->db->where('user_id',$user_id);
		$query_details_ses = $this->db->get('click_history');		
		if($query_details_ses->num_rows >= 1) 
		{
			return $query_details_ses->result();
		}
		else
		{
			return false;		
		}			
	}
	/************ Dec 8th *************/
	
	/************ Dec 9th *************/
	function missing_Cashback_submit_mod($img)
	{	
		//echo "<pre>"; print_r($_POST);exit;
		$name = $this->db->query("select * from admin")->row();
		$site_name  = $name->site_name;
		$this->db->connection_check();
		$user_id = $this->session->userdata('user_id');
		$date = date('Y-m-d');
		$data = array(
			'user_id'			=>$user_id,
			'missing_reason'	=>'Missing Cashback',
			'attachment'		=>$img,
			'retailer_name'  	=>$this->input->post('store'),
			'transaction_ref_id'=>$this->input->post('transaction_reference'),
			'transation_amount' =>$this->input->post('ordervalue'),
			'click_id'			=>$this->input->post('hid_click_id'),
			'coupon_code'		=>$this->input->post('coupon_used'),
			'ordervalue'		=>$this->input->post('ordervalue'),
			'cashback_details'	=>$this->input->post('details'),
			'status'			=>3,
			'trans_date'		=>$date,
			'phone_no' 			=>$this->input->post('phone_no')
		);
		//echo "<pre>";print_r($data); exit;

		$this->db->insert('missing_cashback',$data);
		$user_details = $this->edit_account($user_id);
		$mail 		  = $user_details->email;
		if($user_details->support_tickets == 1)
		{
			
			/*new code for username details 5-1-17*/
			$firstname = $user_details->first_name;
			$lastname  = $user_details->last_name;
			if($firstname == '' && $lastname == '')
			{
				$ex_name  = explode('@',$user_details->email);
				$username = $ex_name[0]; 
			}
			else
			{
				$username = $firstname.''.$lastname;
			}	
			/*End 5-1-17*/


			//$username 		 = $user_details->first_name." ".$user_details->last_name;
			$admindetailssss = $this->front_model->getadmindetails_main();  
			$mail_text 		 = '<p><span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:26px;font-style:normal">
			Acabamos de enviar a sua reclamação para a loja. O processo de conferência é manual e pode levar até 40 dias úteis para que eles nos respondam. (A gente sabe que isso é muito demorado e é um saco esperar tanto, mas infelizmente não depende de nós . E esse é um “prazo máximo” pode ser que leve bem menos que isso).</span></p>';
				
			$mail_temp 		 = $this->db->query("select * from tbl_mailtemplates where mail_id='13'")->row();
			$fe_cont 		 = $mail_temp->email_template;
			$admindetailssss = $this->front_model->getadmindetails_main(); 
			//$subject 		 = $admindetailssss->site_name." has recieved your Missing Cashback Ticket";
			$subject 		 = $mail_temp->email_subject;
			$name 			 = $this->db->query("select * from admin")->row();
			$admin_emailid 	 = $name->admin_email;
			$site_logo 		 = $name->site_logo;
			$site_name  	 = $name->site_name;
			$contact_number  = $name->contact_number;
			$servername 	 = base_url();
			$nows 			 = date('d/m/Y');/*ALTEREI ERA 'Y-m-d'*/
			$unsuburl     	 = base_url().'un-subscribe/missing_cashback/'.$user_id;
	        $myaccount    	 = base_url().'minha_conta';

			$this->load->library('email');
			//Pilaventhiran 04/05/2016 START
			$see_status_missing = "<a href='".base_url()."loja_nao_avisou_compra'>status da solicitação</a>";
			$gd_api=array(
						'###ADMINNO###'    =>$contact_number,
						'###EMAIL###'      =>$username,
						'###DATE###'	   =>$nows,
						'###CONTENT###'	   =>$mail_text,
						'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
						'###SITENAME###'   =>$site_name,
						'###SEE_STATUS_MISSING###'=>$see_status_missing,
						'###ULINK###'      =>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
						'###MYACLINK###'   =>'<a href='.$myaccount.'>'.$myaccount.'</a>'
						);
		//Pilaventhiran 04/05/2016 END
					   
			$gd_message=strtr($fe_cont,$gd_api);
			//echo $gd_message;exit;
			/*
			$config['protocol'] = 'sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			*/
			
			$config = Array(
				 'mailtype'  => 'html',
				  'charset'   => 'utf-8',
				  );
			//$list = array($mail, $admin_emailid);
			
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			$this->email->from($admin_emailid,$site_name.'!');
			$this->email->to($mail);
			$this->email->subject($subject);
			$this->email->message($gd_message);
			$this->email->send();
			$this->email->print_debugger();
		}
		return true;
	}
	
	/************ Dec 9th *************/
	
	/************ Dec11th *************/
	function user_cashback_balance($user_id=null)
	{
		$this->db->connection_check();
		if($user_id!="")
		{
			//SELECT sum(transation_amount) FROM `transation_details` where user_id=1 and transation_reason='Cashback'
			/*$this->db->select('SUM(transation_amount)');
			$this->db->where('user_id',$user_id);
			$this->db->where('transation_reason','Cashback');
			$this->db->where('transation_status','Paid');
			$allfaqs = $this->db->get('transation_details');
			return $allfaqs->row('SUM(transation_amount)');*/
			$this->db->select('SUM(cashback_amount)');
			$this->db->where('user_id',$user_id);
			//$this->db->where('transation_reason','Cashback');
			$this->db->where('status','Completed');
			$allfaqs = $this->db->get('cashback');
			return $allfaqs->row('SUM(cashback_amount)');
		}
		else
		{
			return 0;
		}
	}
	
	function get_clicked_expirycheck_ajax($click_id)
	{
		$this->db->where('click_id',$click_id);
		$all = $this->db->get('missing_cashback');
		if($all->num_rows > 0){
			return $all->row();
		}
		return false;
	}
	
	/************ Dec11th *************/
	
	/************ Dec 13 th *************/
	function get_top_cashback_stores_list_cate($category_id)
	{
		$this->db->connection_check();
		$this->db->order_by('cashback_percentage','desc');
		$this->db->where("FIND_IN_SET('$category_id',store_categorys) !=", 0);
		$this->db->where('affiliate_status','1');
		$query = $this->db->get('affiliates');
		//echo $this->db->last_query();die;
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
	function get_top_cashback_stores_list()
	{
		$this->db->connection_check();
		$this->db->order_by('cashback_percentage','desc');
		//$this->db->where("FIND_IN_SET('$category_id',store_categorys) !=", 0);
		$this->db->where('affiliate_status','1');
		$this->db->where("(featured='1' OR store_of_week='1')", NULL, FALSE);
		$query = $this->db->get('affiliates');
		//echo $this->db->last_query();die;
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
	function get_top_cashback_stores_limit($limit)
	{
		$this->db->connection_check();
		$this->db->limit($limit,0);
		//$this->db->order_by('affiliate_id','desc');
		//$this->db->where("FIND_IN_SET('$category_id',store_categorys) !=", 0);
		$this->db->where('affiliate_logo !=','');
		$this->db->where('affiliate_status','1');
		$query = $this->db->get('affiliates');
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
		// view user email	
	function user_name($user_id)
	{
		$this->db->connection_check();	
		$result = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('first_name');
		return $result;	
	}
	
	function get_blog_details()
	{
		$this->db->connection_check();
		$this->db->order_by('cms_id','desc');
		//$this->db->where("FIND_IN_SET('$category_id',store_categorys) !=", 0);
		$this->db->where('cms_status','1');
		$query = $this->db->get('tbl_blog');
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
	function blog_comments($blog_id)
	{
		$this->db->connection_check();
		//echo $blog_id;
		$this->db->where('bid',$blog_id);
		$query = $this->db->get('tbl_bloguser_comments');
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
	function blog_details($blog_id)
	{
		$this->db->connection_check();
		//echo $blog_id;
		$this->db->where('cms_id',$blog_id);
		$query = $this->db->get('tbl_blog');
		if($query->num_rows >= 1)
		{
		   return $query->row();
		}
		return false;
	}
	
	function post_comments()
	{
		$this->db->connection_check();
		$now = date('Y-m-d H:i:s');
		$data = array(
		'bid'=>$this->input->post('blog_id'),
		'user_id'=>$this->input->post('user_id'),
		'comments'=>$this->input->post('comments'),
		'created_date'=>$now,
		'c_date'=>$now,
		'status'=>'deactive'	
		);
		$this->db->insert('tbl_bloguser_comments',$data);		
		return true;
	}
	
	// site visits..
	function unique_visits($ip_address)
	{
		$this->db->connection_check();
		$date = date('Y-m-d');	 /*ALTEREI ERA 'Y-m-d' */
		
		$this->db->where('date_added',$date);
		$this->db->where('ip_address',$ip_address);		
		$check = $this->db->get('site_visits');
		if($check->num_rows == 0){
			
			$data = array(
				'ip_address'=>$ip_address,
				'date_added'=>$date
			);
			
			$this->db->insert('site_visits',$data);
			return true;
		}
	}
	
	/************ Dec 13 th *************/
	
	

/*********************Nathan End*************************/
	
	
	 //  5/12/2014  renuka  
 
 function get_allpremiumcoupon_cat()
 { 
		 $this->db->connection_check();/**/
      //    premium_categories 	category_id 	category_name 	category_url 	meta_keyword 	meta_description 	sort_order 	category_status 	date_added
	   
 		$this->db->where('category_status','1');  
		$query = $this->db->get('premium_categories');
		if($query->num_rows >= 1) 
		{ 
			  $result=$query->result();  
			return $result;
		}  
		else
		{
		    return false; 
		}          
 
 } 
 
 function get_countofpremiumcat_addcoupon($db_category_id) 
 { 
 $this->db->connection_check();
  $location =  $this->session->userdata('cityname');
      //    premium_categories 	category_id 	category_name 	category_url 	meta_keyword 	meta_description 	sort_order 	category_status 	date_added   category   
	    $this->db->where('location',$location);
 		$this->db->where( "FIND_IN_SET ('$db_category_id',category) >", 0 ); 
		$this->db->where('remain_coupon_code <>','');
		$this->db->where('expiry_date >=',date('Y-m-d'));
		
		//$this->db->where( "t2.remain_coupon_code !=", '' ); 
		$query=$this->db->get('shopping_coupons');  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->num_rows;  
			 return $num_rows; 
		}  
		else 
		{
		     return $num_rows=0;  
		}             
 }

 	/*new code for shopping_couopn page 01-10-16*/
	function get_premium_cat_coupon($db_category_id) 
	{ 
		$this->db->connection_check();
	  	$location =  $this->session->userdata('cityname');
		//$this->db->where('location',$location);
	 	$this->db->where( "FIND_IN_SET ('$db_category_id',category) >", 0 ); 
		//$this->db->where('remain_coupon_code <>','');
		$this->db->where('expiry_date >=',date('Y-m-d'));
		$query=$this->db->get('shopping_coupons');  
		//echo $this->db->last_query();die;
		if($query->num_rows >= 1) 
		{ 
		    $result=$query->result(); 
		    return $result; 
		}  
		else 
		{
		    return false;  
		}             
	}
 	/*end*/

 	/*New code for get all premium coupon details 4-1-17*/
 	function get_allpremiumcoupons()
 	{
 		$this->db->connection_check();
	  	$location =  $this->session->userdata('cityname');
		//$this->db->where('location',$location);
	 	//$this->db->where( "FIND_IN_SET ('$db_category_id',category) >", 0 ); 
		//$this->db->where('remain_coupon_code <>','');
		$this->db->where('expiry_date >=',date('Y-m-d'));
		$query=$this->db->get('shopping_coupons');  
		//echo $this->db->last_query();die;
		if($query->num_rows >= 1) 
		{ 
		    $result=$query->result(); 
		    return $result; 
		}  
		else 
		{
		    return false;  
		}         
 	}
 	/*End*/

     
 function getcnt_allpremiumcoupon_incat() 
 { 
      //    premium_categories 	category_id 	category_name 	category_url 	meta_keyword 	meta_description 	sort_order 	category_status 	date_added   category   
	   $this->db->connection_check();
	   $location =  $this->session->userdata('cityname');
	   $selqry="SELECT `t1`.* FROM (`premium_categories` as t1) JOIN `shopping_coupons` as t2 ON  FIND_IN_SET (`t1`.`category_id`,`t2`.`category`) > 0 WHERE `t1`.`category_status` = '1' AND  `t2`.`status` = '1'   and t2.remain_coupon_code!='' and t2.expiry_date >='".date('Y-m-d')."' GROUP BY t2.shoppingcoupon_id ";  //and t2.location='".$location."'    
	     
		 $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->num_rows;  
			return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;  
		}          
  
 }   
 
  function getall_premiumcoupons($perpage,$urisegment,$ajax='') 
 {  
 
 		$this->db->connection_check();
		if($urisegment=="")
		{
              $urisegment=0; 
		} 
		else
		{
              $urisegment=$urisegment; 
		}   
 	      
		  $sess_cashback_premiumcatid=trim($this->session->userdata("sess_cashback_premiumcatid"));
		  $sess_cashback_starting_price=trim($this->session->userdata("sess_cashback_starting_price"));
		  $sess_cashback_cashback_end_price=trim($this->session->userdata("sess_cashback_end_price"));
		  $sess_cashback_featured=trim($this->session->userdata("sess_cashback_featured"));
		  $sess_cashback_popular=trim($this->session->userdata("sess_cashback_popular"));
		  $sess_cashback_new=trim($this->session->userdata("sess_cashback_new"));
		  $sess_cashback_es=trim($this->session->userdata("sess_cashback_es"));
		
		  //print_r($this->session->userdata);
		
		  $query='';
	 	
	     if($sess_cashback_premiumcatid!='' &&  $sess_cashback_premiumcatid!="all" && $sess_cashback_premiumcatid!='0')
		   {
			   $sess_cashback_premiumcatid;
			   
		   $query.= "AND FIND_IN_SET($sess_cashback_premiumcatid,`t2`.`category`) ";
		   }
	    if($sess_cashback_starting_price!='' && $sess_cashback_cashback_end_price!='')
		  {
			 $query.=" AND (amount BETWEEN $sess_cashback_starting_price AND $sess_cashback_cashback_end_price) ";
		  }
		  $sess_cashback_feature_query='';
		  $sess_cashback_feature_query1='';
		  $sess_cashback_feature_query2='';
		  
		  if($sess_cashback_featured!='0' || $sess_cashback_popular!='0' || $sess_cashback_new!='0' || $sess_cashback_es!='0') 
		  {
			$sess_cashback_feature_query1="AND (  ";
			$sess_cashback_feature_query2=" OR ";
		  }
		  if($sess_cashback_featured!='0')
		  {
			if($sess_cashback_feature_query=='')			  
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('featured',`t2`.`features`) ";
			 else
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('featured',`t2`.`features`) ";
			 
		  }
		  if($sess_cashback_popular!='0')
		  {
			 if($sess_cashback_feature_query=='')			
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('popular',`t2`.`features`) ";  		
			 else
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('popular',`t2`.`features`) ";
			
		  }
		  if($sess_cashback_new!='0')
		  {
			  //echo $sess_cashback_new;
			 if($sess_cashback_feature_query=='')	
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('new',`t2`.`features`) ";
			 else
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('new',`t2`.`features`) ";
		  }
		  if($sess_cashback_es!='0')
		  {
			  if($sess_cashback_feature_query=='')	
			  $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('endingsoon',`t2`.`features`) ";
			  else
			  $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('endingsoon',`t2`.`features`) ";
			
		  }
		  if($sess_cashback_feature_query!='')
		  {
			  $sess_cashback_feature_query.=" ) ";
		  }		
		  $query.=$sess_cashback_feature_query;
		  if($ajax=='')
		  {
			$query='';  
		  }
		 $location =  $this->session->userdata('cityname');
		   $selqry="SELECT `t2`.* FROM (`premium_categories` as t1) JOIN `shopping_coupons` as t2 ON  FIND_IN_SET (`t1`.`category_id`,`t2`.`category`) > 0 WHERE `t1`.`category_status` = '1' AND `t2`.`status` = '1'  and `t2`.`remain_coupon_code`!='' and t2.expiry_date >='".date('Y-m-d')."' $query  GROUP BY t2.shoppingcoupon_id  limit $urisegment,$perpage ";     
		 
 
		   //echo "SELECT `t2`.* FROM (`premium_categories` as t1) JOIN `shopping_coupons` as t2 ON  FIND_IN_SET (`t1`.`category_id`,`t2`.`category`) > 0 WHERE `t1`.`category_status` = '1' AND `t2`.`status` = '1'  and `t2`.`remain_coupon_code`!='' and t2.expiry_date >='".date('Y-m-d')."' $query  GROUP BY t2.shoppingcoupon_id  limit $urisegment,$perpage ";
		 $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			$result=$query->result();  
			return $result; 
		}  
		else  
		{
		    return "0";   			
		}          
 
 }
 
 function getall_fetured_products()
 {
	 $this->db->connection_check();
	 $selqry="SELECT * FROM shopping_coupons  WHERE FIND_IN_SET('featured',`features`)  and `remain_coupon_code`!='' AND `status` = '1' and expiry_date >='".date('Y-m-d')."' order by shoppingcoupon_id desc";     
 	
		 $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			$result=$query->result();  
			return $result; 
		}  
		else  
		{
		    return "0";   			
		}           
 }
 function getall_premiumcouponscount($ajax='') 
 { 
 	$this->db->connection_check();
  $sess_cashback_premiumcatid=$this->session->userdata("sess_cashback_premiumcatid");
		  $sess_cashback_starting_price=$this->session->userdata("sess_cashback_starting_price");
		  $sess_cashback_cashback_end_price=$this->session->userdata("sess_cashback_end_price");
		  $sess_cashback_featured=$this->session->userdata("sess_cashback_featured");
		  $sess_cashback_popular=$this->session->userdata("sess_cashback_popular");
		  $sess_cashback_new=$this->session->userdata("sess_cashback_new");
		  $sess_cashback_es=$this->session->userdata("sess_cashback_es");
		  
		  $query='';
	 	
	      if($sess_cashback_premiumcatid!='' &&  $sess_cashback_premiumcatid!="all" && $sess_cashback_premiumcatid!='0')
		   {
		   $query.= "AND FIND_IN_SET($sess_cashback_premiumcatid,`t2`.`category`) ";
		   }
	    if($sess_cashback_starting_price!='' && $sess_cashback_starting_price!=0 && $sess_cashback_cashback_end_price!='' && $sess_cashback_cashback_end_price!=0)
		  {
			 $query.=" AND (amount BETWEEN $sess_cashback_starting_price AND $sess_cashback_cashback_end_price) ";
		  }
		  $sess_cashback_feature_query='';
		  $sess_cashback_feature_query1='';
		  $sess_cashback_feature_query2='';
		  
		  if($sess_cashback_featured!='0' || $sess_cashback_popular!='0' || $sess_cashback_new!='0' || $sess_cashback_es!='0') 
		  {
			$sess_cashback_feature_query1="AND (  ";
			$sess_cashback_feature_query2=" OR ";
		  }
		  if($sess_cashback_featured!='0')
		  {
			if($sess_cashback_feature_query=='')			  
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('featured',`t2`.`features`) ";
			 else
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('featured',`t2`.`features`) ";
			 
		  }
		  if($sess_cashback_popular!='0')
		  {
			 if($sess_cashback_feature_query=='')			
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('popular',`t2`.`features`) ";  		
			 else
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('popular',`t2`.`features`) ";
			
		  }
		  if($sess_cashback_new!='0')
		  {
			 if($sess_cashback_feature_query=='')	
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('new',`t2`.`features`) ";
			 else
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('new',`t2`.`features`) ";
		  }
		  if($sess_cashback_es!='0')
		  {
			  if($sess_cashback_feature_query=='')	
			  $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('endingsoon',`t2`.`features`) ";
			  else
			  $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('endingsoon',`t2`.`features`) ";
			
		  }
		  if($sess_cashback_feature_query!='')
		  {
			  $sess_cashback_feature_query.=" ) ";
		  }		
		  $query.=$sess_cashback_feature_query;
		  if($ajax=='')
		  {
			 $query=''; 
		  }
		  $location =  $this->session->userdata('cityname');
		    
		   
		   
		  $selqry="SELECT `t2`.* FROM (`premium_categories` as t1) JOIN `shopping_coupons` as t2 ON  FIND_IN_SET (`t1`.`category_id`,`t2`.`category`) > 0 WHERE `t1`.`category_status` = '1' AND  `t2`.`status` = '1' and t2.location='".$location."' and `t2`.`remain_coupon_code`!='' and t2.expiry_date >='".date('Y-m-d')."' $query  GROUP BY t2.shoppingcoupon_id";  
		  
		  
	    
	    $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->num_rows;   
			   return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}          
 
 }
  
  
   
 function getrowsperpage()
{ 
	$this->db->connection_check();
    	/* $query=$this->db->get('admin');
        if($query->num_rows==1)
         {   
              $row=$query->row();   
			$rowsperpage=$row->intRows_front;    
         }  
		//  $rowsperpage="10"; */
		 
	return $rowsperpage=6;     
 }
	
	function find_remainingdays($datestr=null)
 {
	 $this->db->connection_check();
   
   //Convert to date
  //$datestr="2014-12-25 00:00:00";//Your date
$date=strtotime($datestr);//Converted to a PHP date (a second count)
 
//Calculate difference
$diff=$date-time();//time returns current time in seconds
$days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
$hours=round(($diff-$days*60*60*24)/(60*60));
   $data['days']=$days; 
   $data['hours']=$hours; 
   
   
   return $data; 
  //Report  
  // echo "$days days $hours hours remain<br />";  
 
 }
	//change code for id to name field 7-6-16//
	function details($name)
	{
		$this->db->connection_check();
		$selqry = "SELECT * FROM shopping_coupons  WHERE seo_url = '$name'"; //this also 7-5-16//        
	    $query  = $this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			$num_rows 		 	 = $query->row(); 
			$ip_addr 		 	 = $this->input->ip_address();
			$db_coupon_image 	 = $num_rows->coupon_image;
			$coupon_name     	 = $num_rows->seo_url;
			$exp_db_coupon_image = explode(",",$db_coupon_image);  							 
			$f_dbcouponfirst_img = $exp_db_coupon_image[0];
							 
			$selqry1 = "SELECT * FROM recently_viewed  WHERE ip = '".$ip_addr."' order by date ASC"; 
			$query1  = $this->db->query("$selqry1"); 	
			$selqry2 = "SELECT * FROM recently_viewed  WHERE product_id =$num_rows->shoppingcoupon_id order by date ASC"; 
			$query2  = $this->db->query("$selqry2"); 	
			
			if($query2->num_rows == 0) 
			{
				if($query1->num_rows >= 3) 
				{ 
					
					$num_rows1=$query1->row();  
					$selqry="update recently_viewed set product_id='".$num_rows->shoppingcoupon_id."',name='".$name."',image='".$f_dbcouponfirst_img."',price='".$num_rows->amount."',ip='".$ip_addr."' WHERE ip = '".$ip_addr."' and date='".$num_rows1->date."'  ";
				}
				else
				{							
			   		$selqry="insert into recently_viewed (product_id,name,image,price,ip) values ('".$num_rows->shoppingcoupon_id."','".$name."','".$f_dbcouponfirst_img."','".$num_rows->amount."','".$ip_addr."') ";
				}
			}
	    	$this->db->query("$selqry"); 
			return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}          
	}

	function related_products($id,$name)
	{
		//echo $name;
	$this->db->connection_check();	
		$explode=explode(",",$id);
		$query="";
		for($i=0;$i<count($explode);$i++)
		{
			if($i==0)
			$query.="AND (  FIND_IN_SET($explode[$i],`category`)";
			else
			$query.=" OR FIND_IN_SET($explode[$i],`category`)";
			
		}
		$query.=" ) ";
		$selqry="SELECT * FROM shopping_coupons  WHERE status = '1'  and `remain_coupon_code`!='' and seo_url !='$name'  and expiry_date >='".date('Y-m-d')."'";        
	  
	    $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->result();   
			   return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}  
	}
	/*New code for populer 10-5-16*/
	function popular_products()
	{
		$this->db->connection_check();

		$selqry="SELECT * FROM `shopping_coupons`  WHERE `status`='1' and `remain_coupon_code`!='' and `expiry_date` >='".date('Y-m-d')."' AND  FIND_IN_SET('popular',`features`) LIMIT 3";        
	 
	    $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->result();   
			   return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}  
	}
	
	function popular_products_index()
	{
		$this->db->connection_check();

		$selqry="SELECT * FROM shopping_coupons  WHERE status = '1' and `remain_coupon_code`!='' and expiry_date >='".date('Y-m-d')."' AND  FIND_IN_SET('popular',`features`) order by shoppingcoupon_id desc";        
	 
	    $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->result();   
			   return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}  
	}
	
	function latest_products_index()
	{
		$this->db->connection_check();

		$selqry="SELECT * FROM shopping_coupons  WHERE status = '1' and `remain_coupon_code`!='' and expiry_date >='".date('Y-m-d')."' AND  FIND_IN_SET('new',`features`) order by shoppingcoupon_id desc";        
	 
	    $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->result();   
			   return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}  
	}
	
	
	function avg_rating($id)
	{
		$this->db->connection_check();
		$selqry="SELECT AVG(ratings) as rate FROM reviews r join tbl_users  u on r.user_id=u.user_id WHERE coupon_id = '".$id."' and r.approve=1 "; 
		$query=$this->db->query("$selqry")  ;   
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->row();   
			   return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}  	
	}
	
	
	function recently_viewed()
	{
		$this->db->connection_check();
		$ip_addr = $this->input->ip_address();
        $selqry="SELECT * FROM recently_viewed  WHERE ip = '".$ip_addr."' group by `id` desc limit 0,3";        
	     
	    $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->result();   
			   return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}  	
	}
			function recent_reviews($id)
			{
				
				$this->db->connection_check();
				 $selqry="SELECT * FROM reviews r join tbl_users  u on r.user_id=u.user_id WHERE coupon_id = '".$id."' and r.approve=1  LIMIT 3";        
				 
				$query=$this->db->query("$selqry"); 
				  
				if($query->num_rows >= 1) 
				{ 
					  $num_rows=$query->result();   
					   return $num_rows; 
				}  
				else  
				{
					return $num_rows=0;   
				}  	
			}
			function reviews($id)
			{
				$this->db->connection_check();
				 $selqry="SELECT * FROM reviews r join tbl_users  u on r.user_id=u.user_id WHERE coupon_id = '".$id."' and r.approve=1 ";        
				 
				$query=$this->db->query("$selqry"); 
				  
				if($query->num_rows >= 1) 
				{ 
					  $num_rows=$query->result();   
					   return $num_rows; 
				}  
				else  
				{
					return $num_rows=0;   
				}  	
			}
			function insert_comments()
			{
				$this->db->connection_check();
				$data = array(
					'comments' =>$this->input->post('comments'),
					'ratings' => $this->input->post('rating'),
					'coupon_id' =>$this->input->post('coupon'),
					'user_id'=>$this->session->userdata('user_id')
					);
					$var=$this->db->insert('reviews',$data);
					if($var==1)
					{
						return true;
					}
					else
					{
						return false;
					}
			}
		 function insert_coupon()
		 {
			 $this->db->connection_check();
			 $selqry1="SELECT * FROM premium_cart  WHERE product_id= '".$this->input->post('coupon')."' and user_id = '".$this->session->userdata('user_id')."'";        
			 $query1=$this->db->query("$selqry1"); 
					if($query1->num_rows >= 1) 
					{  
					$var = 0;
					}
					else
					{
					 $data = array(
					'product_id' =>$this->input->post('coupon'),
					'quantity' => '1',			
					'user_id'=>$this->session->userdata('user_id')
					);
					$var=$this->db->insert('premium_cart',$data);
					}
					
					if($var==1)
					{			
						return $var;		
					}
					else
					{
						return $var;
					}
		 }
		 function getuser_cart()
		 {			
		 		$this->db->connection_check();
				if($this->session->userdata('user_id'))			
				{
				 	$selqry="SELECT * FROM premium_cart  WHERE user_id= '".$this->session->userdata('user_id')."'";   
				 	$query=$this->db->query("$selqry");  
					if($query->num_rows >= 1) 
					{ 
						 $num_rows=$query->result();   
						 return $num_rows;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
		 }
		 function check_amount()
		 {
			 $this->db->connection_check();
			  $selqry="SELECT balance FROM tbl_users  WHERE user_id= '".$this->session->userdata('user_id')."'";   
			  $query=$this->db->query("$selqry");  
				if($query->num_rows >= 1) 
					{ 
						 $num_rows=$query->row(); 
						 if($this->input->post('amount')<=$num_rows->balance)  
						 return true;
					}
					else
					{
						return false;
					}
		 }
		 function cal_amount()
		 {
			 $this->db->connection_check();
			 $selqry="SELECT balance FROM tbl_users  WHERE user_id= '".$this->session->userdata('user_id')."'";   
			  $query=$this->db->query("$selqry");  
				if($query->num_rows >= 1) 
					{ 
						 $num_rows=$query->row(); 
						 $remain_amount=$num_rows->balance-$this->input->post('otot');
						 
						 $data = array(		
						'balance' => $remain_amount,		
						);
				
						$this->db->where('user_id',$this->session->userdata('user_id'));
						$update_qry = $this->db->update('tbl_users',$data);
						
						$data = array(		
						'transation_reason' => "premium coupon",		
						'transation_amount' => $this->input->post('otot'),		
						'mode' => "debited",
						'transation_status' => "Paid",			
						'transation_date' => date('Y-m-d'),	
						'transaction_date' => date('Y-m-d'),		
						'user_id' => $this->session->userdata('user_id'),		
						);
				
						$this->db->insert('transation_details',$data);				
						 
						 for($i=1;$i<=$this->input->post('i_val');$i++)
						 {
							  	
								
							
						  $data = array(		
								'amount' => $this->input->post('price'.$i),
								'tot_amount' => $this->input->post('tot'.$i),
								'quantity' => $this->input->post('quan'.$i),
								'product_id' => $this->input->post('product_id'.$i),	
								'user_id' => $this->session->userdata('user_id'),
								'status' => "Paid",	
								);
								$this->db->insert('premium_order',$data);							
					
						 }
						 $query = $this->db->where('user_id', $this->session->userdata('user_id'));
						 $query = $this->db->delete('premium_cart');
						 
						 return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
						 
					}
					else
					{
						return false;
					}
			 
		 }
		 function get_user()
		 {
			 $this->db->connection_check();
			  $selqry="SELECT * FROM tbl_users  WHERE user_id= '".$this->session->userdata('user_id')."'";   
			  $query=$this->db->query("$selqry");  
				if($query->num_rows >= 1) 
					{ 
						 $num_rows=$query->row();   
						 return $num_rows;
					}
					else
					{
						return false;
					}
			  
		 }
		 function insert_umoney_order($hash)
		 {
			$this->db->connection_check();
			for($i=1;$i<=$this->input->post('i');$i++)
				 {
					  $data = array(		
						'amount' => $this->input->post('price_'.$i),
						'tot_amount' => $this->input->post('quantity_'.$i)*$this->input->post('price_'.$i),
						'quantity' => $this->input->post('quantity_'.$i),
						'product_id' => $this->input->post('product_id'.$i),	
						'user_id' => $this->session->userdata('user_id'),
						'hash_id' => $hash,	
						'status' => 'pending',	
						);
						$this->db->insert('premium_order',$data);
										
				 } 
		 }
	function sendmailcoupon($new_coupon_last_trim,$prod_id)
	{
		$this->db->connection_check();

		$new_coupon_last_trim = explode(",",$new_coupon_last_trim);
		$shoping_coupon 	  = $this->db->query("select * from shopping_coupons where shoppingcoupon_id=$prod_id")->row();
		$current_msg 		  = '<center><h2>Your Coupon Code for: <span style="color:yellow">'.$shoping_coupon->offer_name.'</span> </h2></center><table align="center">';
		
		for($i=0;$i<count($new_coupon_last_trim);$i++)
		{
			$current_msg.=' <tr>
			<td>Coupon Code 1 : </td><td style="color:white; background:black">'.$new_coupon_last_trim[$i].'</td>
			</tr>';
		}
		
		$current_msg.= ' </table>';
		
		$mail_temp 		= $this->db->query("select * from tbl_mailtemplates where mail_id='5'")->row();
		$fe_cont 	 	= $mail_temp->email_template;
		$name 			= $this->db->query("select * from admin")->row();
		$subject 		 = $mail_temp->email_subject;
		//$subject 	    = "Your Missing Ticket Reply";
		$admin_emailid  = $name->admin_email;
		$site_logo 		= $name->site_logo;
		$site_name  	= $name->site_name;
		$contact_number = $name->contact_number;
		$servername 	= base_url();
		$nows 			= date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */	

		$this->load->library('email');
		$gd_api = array(
			'###ADMINNO###'=>$contact_number,
			'###EMAIL###'=>$this->session->userdata('user_email'),
			'###DATE###'=>$nows,
			'###CONTENT###'=>$current_msg,
			'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
			'###SITENAME###' =>$site_name
		);
		
		$gd_message=strtr($fe_cont,$gd_api);
						
		$config = Array(
			'mailtype'  => 'html',
			'charset'   => 'utf-8',
		);
     	//$this->email->initialize($config);        
     	$this->email->set_newline("\r\n");
		$this->email->initialize($config);        
		$this->email->from($admin_emailid,$site_name.'!');
		$this->email->to($user_email);
		$this->email->subject("Coupon Code");
		$this->email->message($gd_message);
		$this->email->send();
		$this->email->print_debugger();
	}
	
		function update_pay_u_money($mihpayid,$TxnID,$useragent,$ipaddress,$old_id)/*seetha 19.8.2015*/
		{
			 $this->db->connection_check();
			$data = array('transaction_id' => $mihpayid, 
							'status'=>'Paid',
							'useragent'=>$useragent,
							'ipaddress'=>$ipaddress,
							'hash_id'=>$TxnID);
			$this->db->where('hash_id',$old_id);
			
			$update_qry = $this->db->update('premium_order',$data); 
			
			$selqry00="SELECT * FROM premium_order  WHERE hash_id='".$TxnID."'";        	     
							$query00=$this->db->query("$selqry00"); 
						//	echo $selqry00;
						//	print_r($query00->result());
						//	exit;
			
			foreach($query00->result() as $query_result)
			{
			$selqry1="SELECT * FROM shopping_coupons  WHERE shoppingcoupon_id= '".$query_result->product_id."'";     
			//echo $selqry1."<br/>";   	     
							$query1=$this->db->query("$selqry1"); 
							
							if($query1->num_rows >= 1) 
							{ 
								 $num_rows1=$query1->row();   
								 
								 $old_coupon=$num_rows1->coupon_code;
								 
								 $new_coupon=explode(",",$num_rows1->remain_coupon_code);
								 $new_coupon_last='';
								 
								 for($c=0;$c<count($this->input->post($query_result->quantity));$c++)
								 {
								 $new_coupon_last.=$new_coupon[$c].",";
								 }
								 $new_coupon_last_trim=rtrim($new_coupon_last,",");
								 $coupon_code=$num_rows1->coupon_code.",".$new_coupon_last_trim;
								 
								 $remain_coupon_code=str_replace("$new_coupon_last","",$num_rows1->remain_coupon_code);
								 
							   $this->sendmailcoupon($new_coupon_last_trim,$query_result->product_id);
								 
								 $data = array(		
								 'coupon_code' => $coupon_code,
								 'remain_coupon_code' => $remain_coupon_code,				
								 );
								 
						
							$this->db->where('shoppingcoupon_id',$query_result->product_id);
							$update_qry = $this->db->update('shopping_coupons',$data);
		
						  }  

			}
			 $query = $this->db->where('user_id', $this->session->userdata('user_id'));
			 $query = $this->db->delete('premium_cart');
									 // exit;
			return true;
		 }
		 function update_hash($old,$new)
		 {
			 $this->db->connection_check();
			$data = array('hash_id' => $new);
			$this->db->where('hash_id',$old);
			$update_qry = $this->db->update('premium_order',$data); 
			return true;
		 }
		 function get_orders()
		 {
			 $this->db->connection_check();
			  $selqry="SELECT * FROM premium_order join shopping_coupons on shopping_coupons.shoppingcoupon_id=premium_order.product_id   WHERE premium_order.status='Paid' and premium_order.user_id= '".$this->session->userdata('user_id')."' ORDER BY `id` desc ";   
			  $query=$this->db->query($selqry); 		  
				if($query->num_rows >= 1) 
					{ 
						 $num_rows=$query->result();   
						 return $num_rows;
					}
					else
					{
						return false;
					}
			  
		 }
		 
		 function gettransations($userid)
		 {
			 $this->db->connection_check();
			 if($userid!="")
			{	
				$this->db->where('user_id',$userid);
				$miss_cashbacks = $this->db->get('transation_details');
				return $miss_cashbacks->result();
			}
			else
			{
				return 0;
			}
		 }
		 
 function getcashbacks($userid)
		 {
			 $this->db->connection_check();
			 if($userid!="")
			{	
				$this->db->where('user_id',$userid);
				$miss_cashbacks = $this->db->get('cashback');
				return $miss_cashbacks->result();
			}
			else
			{
				return 0;
			}
		 }
		 function ref_earnings($userid)
		{
			$this->db->connection_check();	
			/*$this->db->select('SUM(transation_amount) as ref_amount');
			$this->db->where('user_id',$userid);
			$this->db->where('transation_reason','Referal Payment');
			$paid_earning= $this->db->get('transation_details');
			$waitng = $paid_earning->row();	
			if($paid_earning->num_rows > 0)
			{
				return $waitng->ref_amount;
			}
			else
			{
				return 0;
			}*/

			$categorytype    	= $this->db->query("select * from tbl_users where user_id='$userid'")->row('referral_category_type');
			$category_names 	= $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row('category_type');
			$category_names 	= ucfirst($category_names);

			$result = $this->db->query("SELECT SUM(transation_amount) as ref_amount FROM (`transation_details`)
			WHERE `user_id` = '$userid' AND (`transation_reason` = 'Referal Payment' OR `transation_reason` = 'Referral Cashback amount' OR `transation_reason`= 'Referral Bonus for $category_names User')
			AND `mode` = 'Credited' AND `transation_status` = 'Approved'");
			//echo $this->db->last_query();die;
			//print_r($result); exit;
			$var = 0;
			if($result)
			{
				$var = $result->row('ref_amount');
				return $var;
			}else
			{
				return 0;
			}
			
		}
		
		function premium_home_slider()
		{
			$this->db->connection_check();
			$this->db->where("banner_status",'1');
			$this->db->where("banner_position",'1');
			$query = $this->db->get('tbl_banners');
			return $query->result();
		}
		
		
		function recently_viewed_index()
		{	
			$this->db->connection_check();
			 $selqry="SELECT * FROM recently_viewed order by id desc limit 0,2";        
			 
			$query=$this->db->query("$selqry"); 
			  
			if($query->num_rows >= 1) 
			{ 
				  $num_rows=$query->result();   
				   return $num_rows; 
			}  
			else  
			{
				return $num_rows=0;   
			}  	
		}
		
		
		function clock_history_stores()
		{	
			$this->db->connection_check();
			 $selqry="SELECT * FROM `click_history` as a INNER JOIN affiliates as b on a.store_name=b.affiliate_name  group by store_name order by click_id ";        			 
			$query=$this->db->query("$selqry"); 
			  
			if($query->num_rows >= 1) 
			{ 
				  $num_rows=$query->result();   
				   return $num_rows; 
			}  
			else  
			{
				return $num_rows=0;   
			}  	
		}
		
		function seoUrl($string) 
		{
			$this->db->connection_check();
			//Lower case everything
			$string = strtolower($string);
			//Make alphanumeric (removes all other characters)
			//$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
			/*New code for spl char replace 