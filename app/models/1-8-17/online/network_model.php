<?php
class Network_model extends CI_Model
{
	
	function Affiliate_details($aff_name)
	{
		$this->db->connection_check();
		$this->db->where('affiliate_network',$aff_name);
		$affiliates = $this->db->get('affiliates_list');
		if($affiliates->num_rows > 0){
			return $affiliates->row();
		}
		return false;
	}
	function get_offer_provider_cashback($storename)
	{
		$this->db->connection_check();
		$this->db->like('affiliate_name', $storename);
		$this->db->limit(1,0);
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->row();
		}
		return false;
	}
	function check_ref_user($ref_user)
	{
		$this->db->connection_check();
		$this->db->where('user_id',$ref_user);        
		$result = $this->db->get('tbl_users');
		if($result->num_rows > 0){
			return $result->row('refer');
		}
		return false;
	}
	function check_active_user($user_id)
	{
		
		$this->db->where('user_id',$user_id);
		$this->db->where('status','1');
		$this->db->where('admin_status','');
		$ret = $this->db->get('tbl_users');
		if($ret->num_rows>0){
			return 1;
		}
		return 0;
	}
	
	// view balance 4-5-17..
	function view_balance($user_id){
		$this->db->connection_check();
		$balace = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('balance');
		return $balace;
	}

	// view user details
	function view_user($userid)
	{
		$this->db->connection_check();
		$this->db->where('user_id',$userid);        
        $query = $this->db->get('tbl_users');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
            return $query->result();
        }
        return false;
	}
	function get_affiliatename($category_ids)
	{

		$this->db->connection_check();
			$this->db->where('affiliate_id',$category_ids);
			$query = $this->db->get('affiliates');
			if($query->num_rows >= 1)
			{
			   $row = $query->row();
			   return $row;
			}
			return false;
	}
	/*new code for currency converter 24-6-16*/
	function currencyConverter($currency_from,$currency_to,$currency_input)
	{
	    $yql_base_url    = "http://query.yahooapis.com/v1/public/yql";
	    $yql_query 	     = 'select * from yahoo.finance.xchange where pair in ("'.$currency_from.$currency_to.'")';
	    $yql_query_url   = $yql_base_url . "?q=" . urlencode($yql_query);
	    $yql_query_url  .= "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
	    $yql_session 	 = curl_init($yql_query_url);
	    curl_setopt($yql_session, CURLOPT_RETURNTRANSFER,true);
	    $yqlexec 	     = curl_exec($yql_session);
	    $yql_json 		 =  json_decode($yqlexec,true);
	    $currency_output = (float) $currency_input*$yql_json['query']['results']['rate']['Rate'];

	    return $currency_output;
	}
	

	//Zanox tracking transcations
	function Zanox_trackingtransactions($content)
	{		
		$now = date('Y-m-d H:i:s');	
		$last_updated = $now;
		$tracking =$this->input->post('tracking_id');
		$name = $this->db->query("select * from admin")->row();	
		$ref_cashbcak_percent =  $name->referral_cashback;		
		if(count($content)!=0)
		{
			$array = array();
			$duplicate = 0;
			$duplicate_trans_id = '';				
			$sampless =0;
			
			
			$info=$_SERVER['REMOTE_ADDR'].'","'
			.$_SERVER['HTTP_USER_AGENT'].'","'
			.$_SERVER['HTTP_REFERER'].'","'
			.date("D dS M,Y h:i a").'"'."\n";
			
			$file= 'tracker_log.csv';
			$fp = fopen($file, "a");
			fputs($fp, $info);
			fclose($fp);
			
			foreach($content['saleItems']['saleItem'] as $cont)
			{			
				//Write a contents in logs file
				$myfile = fopen("trackinglogs.log", "a") or die("Unable to open file!");
				//$txt = "user id date";
				fwrite($myfile, $cont['@attributes']['id']. "\t" .$cont['reviewState']."\t" .$cont['trackingDate']. "\t" .$cont['modifiedDate']. "\t" .$cont['clickDate']. "\t" .$cont['adspace']. "\t" .$cont['admedium']. "\t" .$cont['program']. "\t" .$cont['clickId']. "\t" .$cont['clickInId']. "\t" .$cont['amount']. "\t" .$cont['commission']. "\t" .$cont['currency']. "\t" .$cont['gpps']['gpp']. "\n");
				fclose($myfile);
				
				$sampless++;				
				$store_name =$this->db->escape_str($cont['program']);
				$remove_cur = explode(' ',$store_name);
				$storenam = $remove_cur[0];
				
				$tracking1=$cont['gpps']['gpp'];
				
				$site_prefix= 'P0001';
				$pos = strpos($tracking1, $site_prefix);				
				if ($pos == false) {
					$tracking_id = '';
					continue;
				} else {
					$tracking_id = $tracking1;
				}
								
				$get_userid =  decode_userid($tracking_id);
				if($get_userid<=0 || $get_userid=='' )
				{
					continue;
				}
			
				$storeslisty = $this->get_offer_provider_cashback($storenam);
				$store_id = $storeslisty->affiliate_id;
				$cashback_percentage = $storeslisty->cashback_percentage;
				$sale_amount = $cont['amount'];
				$approved_payout = $cont['commission'];					
				if($storeslisty->affiliate_cashback_type=='Percentage')
				{						
					$cashback_amount = number_format(($sale_amount*$cashback_percentage)/100,2);
				}
				else
				{
					if($approved_payout>$cashback_percentage)
					{
						$cashback_amount = number_format(($cashback_percentage),2);
					}
				}
				
				$check_ref = $this->check_ref_user($get_userid);
				$ref_cashback_amount = 0;
				$ref_id = 0;
				$referred = 0;
				$txn_id_new = 0;
				if($check_ref>0)		
				{
					$ref_id  = $check_ref;
					$return = $this->check_active_user($ref_id);
					if($return==1)
					{
						$referred = 1;
						$ref_cashback_percent = $ref_cashbcak_percent;
						$ref_cashback_amount = number_format(($cashback_amount*$ref_cashbcak_percent)/100,2);
					}
				}
				
				$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
				
				$is_cashback = 1;	
				$affiliate_cashback_type=$storeslisty->affiliate_cashback_type;
				$transaction_id=$cont['clickId'];					
				$pay_out_amount =$cont['commission'];
				$sale_amount =$cont['amount'];
				$date = $cont['trackingDate'];
				$status = 'Approved';
				
				$this->db->where('transaction_id',$transaction_id);
				$result = $this->db->get('tbl_report');
				$now = date('Y-m-d H:i:s');	
				$last_updated = $now;
				if($result->num_rows == 0)
				{
					$results = $this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`, `is_cashback`, `cashback_percentage`, `affiliate_cashback_type`,`cashback_amount`, `ref_id`, `ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`) VALUES ('$storenam', '$date', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '$last_updated', '$is_cashback', '$cashback_percentage', '$affiliate_cashback_type','$cashback_amount', '$ref_id','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','$status');");	
					$insert_id = $this->db->insert_id();
					if($is_cashback!=0)	
					{	
						$now = date('Y-m-d H:i:s');	
						$mode = "Credited";	
						if($ref_cashback_amount!=0)	
						{
							$data = array(			
								'transation_amount' => $ref_cashback_amount,	
								'user_id' => $ref_id,	
								'transation_date' => $now,	
								'transation_reason' => 'Pending Referal Payment',	
								'mode' => $mode,	
								'details_id'=>'',	
								'table'=>'',	
								'transation_status' => 'Progress');
							$this->db->insert('transation_details',$data);
							$txn_id_new = $this->db->insert_id();
						}
	
						$data = array(
						'user_id' => $get_userid,	
						'coupon_id' => $storenam,	
						'affiliate_id' => $storenam,	
						'status' => 'Pending',	
						'cashback_amount'=>$cashback_amount,	
						'date_added' => $date,
						'referral' => $referred,
						'txn_id' => $txn_id_new
						);	
						$this->db->insert('cashback',$data);	
						
					/* mail for pending cashback */
					$user_detail = $this->view_user($get_userid);
					if($user_detail){
						foreach($user_detail as $user_detail_single){
							$referral_balance = $user_detail_single->balance;
							$user_email = $user_detail_single->email;
							$user_name = $user_detail_single->first_name.' '.$user_detail_single->last_name;
						}
					}
					$this->db->where('admin_id',1);
					$admin_det = $this->db->get('admin');
					if($admin_det->num_rows >0) 
					{    
						$admin = $admin_det->row();
						$admin_email = $admin->admin_email;
						$site_name = $admin->site_name;
						$admin_no = $admin->contact_number;
						$site_logo = $admin->site_logo;
					}
					
					$date =date('Y-m-d');
					
					$this->db->where('mail_id',12);
					$mail_template = $this->db->get('tbl_mailtemplates');
						if($mail_template->num_rows >0) 
						{
						   $fetch = $mail_template->row();
						   $subject = $fetch->email_subject;
						   $templete = $fetch->email_template;
						   // $url = base_url().'cashback/my_earnings/';
						   
							$this->load->library('email');
							
							$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
							);
							
							$sub_data = array(
								'###SITENAME###'=>$site_name
							);
							$subject_new = strtr($subject,$sub_data);
							
							// $this->email->initialize($config);
							 $this->email->set_newline("\r\n");
							   $this->email->initialize($config);
							   $this->email->from($admin_email);
							   $this->email->to($user_email);
							   $this->email->subject($subject_new);
						   
							$data = array(
								'###NAME###'=>$user_name,
								'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
								'###SITENAME###'=>$site_name,
								'###ADMINNO###'=>$admin_no,
								'###DATE###'=>$date,
								'###AMOUNT###'=>$cashback_amount
							);
						   
						   $content_pop=strtr($templete,$data);
							//$content_pop; echo $subject_new;
							
						   $this->email->message($content_pop);
						   $this->email->send();  
						}
					/* mail for pending cashback */
					}	
				}
				else{
					$duplicate+=1;
					$duplicate_trans_id .= $transaction_id.', ';
				}
			}
			$array['duplicate'] = $duplicate;
			$array['trans_id'] = rtrim($duplicate_trans_id,', ');		
		}
		//print_r($array);die;
		return $array;	
	}
	//lomadee tracking transcations
	function Lomadee_trackingtransactions($content)
	{
		
		$now = date('Y-m-d H:i:s');	
		$last_updated = $now;
		$tracking =$this->input->post('tracking_id');
		$name = $this->db->query("select * from admin")->row();	
		$ref_cashbcak_percent =  $name->referral_cashback;		
		if(count($content)!=0)
		{
			$array = array();
			$duplicate = 0;
			$duplicate_trans_id = '';				
			$sampless =0;
			
			
			$info=$_SERVER['REMOTE_ADDR'].'","'
			.$_SERVER['HTTP_USER_AGENT'].'","'
			.$_SERVER['HTTP_REFERER'].'","'
			.date("D dS M,Y h:i a").'"'."\n";
			
			$file= 'tracker_log.csv';
			$fp = fopen($file, "a");
			fputs($fp, $info);
			fclose($fp);
			
			foreach($content['item'] as $cont){			
				
				//Write a contents in logs file
				
             	$myfile = fopen("trackinglogs.log", "a") or die("Unable to open file!");
				//$txt = "user id date";
				fwrite($myfile, $cont['date']. "\t" .$cont['currency']."\t" .$cont['commission']. "\t" .$cont['associateId']. "\t" .$cont['site']['siteId']. "\t" .$cont['site']['siteName']. "\t" .$cont['advertiser']['advertiserId']. "\t" .$cont['advertiser']['advertiserName']. "\t" .$cont['events']['event']['eventId']. "\t" .$cont['events']['event']['eventName']. "\t" .$cont['events']['event']['gmv']. "\t" .$cont['application']['applicationId']. "\t" .$cont['application']['applicationName']. "\t" .$cont['transactionCode']. "\t" .$cont['transactionId']. "\t" .$cont['Product Name']. "\t" .$cont['statusId']. "\t" .$cont['statusName']. "\t" .$cont['clickDate']. "\t" .$cont['checkoutDate']. "\n");
				fclose($myfile);
				
				$sampless++;				
				$storenam =$this->db->escape_str($cont['advertiser']['advertiserName']);			
				$offerurl_id=$this->db->escape_str($cont['events']['event']['eventId']);
				$offerurl_name=$this->db->escape_str($cont['events']['event']['eventName']);
				$tracking1=$cont['associateId'];
				
				//$tracking_id= 'KDID230149';
				if($tracking1!=''){
					$tracking_id = $tracking1;
				}else{
					$tracking_id = '';
					continue;
				}
				
				$get_userid =  decode_userid($tracking_id);
				if($get_userid<=0 || $get_userid=='' )
				{
					continue;
				}
			
				$storeslisty = $this->get_offer_provider_cashback($storenam);
				$store_id = $storeslisty->affiliate_id;
				$cashback_percentage = $storeslisty->cashback_percentage;
				$sale_amount = $cont['gmv'];
				$approved_payout = $cont['commission'];					
				if($storeslisty->affiliate_cashback_type=='Percentage')
				{						
					$cashback_amount = number_format(($sale_amount*$cashback_percentage)/100,2);
				}
				else
				{
					if($approved_payout>$cashback_percentage)
					{
						$cashback_amount = number_format(($cashback_percentage),2);
					}
				}
				
				$check_ref = $this->check_ref_user($get_userid);
				$ref_cashback_amount = 0;
				$ref_id = 0;
				$referred = 0;
				$txn_id_new = 0;
				if($check_ref>0)		
				{
					$ref_id  = $check_ref;
					$return = $this->check_active_user($ref_id);
					if($return==1)
					{
						$referred = 1;
						$ref_cashback_percent = $ref_cashbcak_percent;
						$ref_cashback_amount = number_format(($cashback_amount*$ref_cashbcak_percent)/100,2);
					}
				}
				
				$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
				
				$is_cashback = 1;	
				$affiliate_cashback_type=$storeslisty->affiliate_cashback_type;
				$transaction_id=$cont['transactionId'];					
				$pay_out_amount =$cont['commission'];
				$sale_amount =$cont['gmv'];
				$date = $cont['date'];
				$offerid = $cont['events']['event']['eventId'];
				$status =  $cont['statusName'];
				
				$this->db->where('transaction_id',$transaction_id);
				$result = $this->db->get('tbl_report');
				$now = date('Y-m-d H:i:s');	
				$last_updated = $now;
				if($result->num_rows == 0)
				{
					$results = $this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`, `is_cashback`, `cashback_percentage`, `affiliate_cashback_type`,`cashback_amount`, `ref_id`, `ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`) VALUES ('$storenam', '$date', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '$last_updated', '$is_cashback', '$cashback_percentage', '$affiliate_cashback_type','$cashback_amount', '$ref_id','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','$status');");	
					$insert_id = $this->db->insert_id();
					if($is_cashback!=0)	
					{	
						$now = date('Y-m-d H:i:s');	
						$mode = "Credited";	
						if($ref_cashback_amount!=0)	
						{
							$data = array(			
								'transation_amount' => $ref_cashback_amount,	
								'user_id' => $ref_id,	
								'transation_date' => $now,	
								'transation_reason' => 'Pending Referal Payment',	
								'mode' => $mode,	
								'details_id'=>'',	
								'table'=>'',	
								'transation_status' => 'Progress');
							$this->db->insert('transation_details',$data);
							$txn_id_new = $this->db->insert_id();
						}
	
						$data = array(
						'user_id' => $get_userid,	
						'coupon_id' => $storenam,	
						'affiliate_id' => $storenam,	
						'status' => 'Pending',	
						'cashback_amount'=>$cashback_amount,	
						'date_added' => $date,
						'referral' => $referred,
						'txn_id' => $txn_id_new
						);	
						$this->db->insert('cashback',$data);	
						
					/* mail for pending cashback */
					$user_detail = $this->view_user($get_userid);
					if($user_detail){
						foreach($user_detail as $user_detail_single){
							$referral_balance = $user_detail_single->balance;
							$user_email = $user_detail_single->email;
							$user_name = $user_detail_single->first_name.' '.$user_detail_single->last_name;
						}
					}
					$this->db->where('admin_id',1);
					$admin_det = $this->db->get('admin');
					if($admin_det->num_rows >0) 
					{    
						$admin = $admin_det->row();
						$admin_email = $admin->admin_email;
						$site_name = $admin->site_name;
						$admin_no = $admin->contact_number;
						$site_logo = $admin->site_logo;
					}
					
					$date =date('Y-m-d');
					
					$this->db->where('mail_id',12);
					$mail_template = $this->db->get('tbl_mailtemplates');
						if($mail_template->num_rows >0) 
						{
						   $fetch = $mail_template->row();
						   $subject = $fetch->email_subject;
						   $templete = $fetch->email_template;
						   // $url = base_url().'cashback/my_earnings/';
						   
							$this->load->library('email');
							
							$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
							);
							
							$sub_data = array(
								'###SITENAME###'=>$site_name
							);
							$subject_new = strtr($subject,$sub_data);
							
							// $this->email->initialize($config);
							 $this->email->set_newline("\r\n");
							   $this->email->initialize($config);
							   $this->email->from($admin_email);
							   $this->email->to($user_email);
							   $this->email->subject($subject_new);
						   
							$data = array(
								'###NAME###'=>$user_name,
								'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
								'###SITENAME###'=>$site_name,
								'###ADMINNO###'=>$admin_no,
								'###DATE###'=>$date,
								'###AMOUNT###'=>$cashback_amount
							);
						   
						   $content_pop=strtr($templete,$data);
							//$content_pop; echo $subject_new;
							
						   $this->email->message($content_pop);
						   $this->email->send();  
						}
					/* mail for pending cashback */
					}	
				}
				else{
					$duplicate+=1;
					$duplicate_trans_id .= $transaction_id.', ';
				}
			}
			$array['duplicate'] = $duplicate;
			$array['trans_id'] = rtrim($duplicate_trans_id,', ');		
		}
		//print_r($array);die;
		return $array;	
	}
	//Cityads tracking transactions
	function importCityads_trackingtransactions($content)
	{
		$now = date('Y-m-d H:i:s');	
		$last_updated = $now;
		$tracking =$this->input->post('tracking_id');
		$name = $this->db->query("select * from admin")->row();	
		$ref_cashbcak_percent =  $name->referral_cashback;	
		if(count($content)!=0)
		{					
			$array = array();
			$duplicate = 0;
			$duplicate_trans_id = '';				
			$sampless =0;		
				
			
			$info=$_SERVER['REMOTE_ADDR'].'","'
			.$_SERVER['HTTP_USER_AGENT'].'","'
			.$_SERVER['HTTP_REFERER'].'","'
			.date("D dS M,Y h:i a").'"'."\n";
			
			$file= 'tracker_log.csv';
			$fp = fopen($file, "a");
			fputs($fp, $info);
			fclose($fp);
			
			foreach($content['data']['items'] as $cont)
			{	
				//Write a contents in logs file
		
				$myfile = fopen("trackinglogs.log", "a") or die("Unable to open file!");
				//$txt = "user id date";
				fwrite($myfile, $cont['offerID']. "\t" .$cont['offerName']."\t" .$cont['basketQTY']. "\t" .$cont['campaignTargetName']. "\t" .$cont['clickTime]']. "\t" .$cont['commissionOpen']. "\t" .$cont['commissionCancelled']. "\t" .$cont['commissionApproved']. "\t" .$cont['originalCurrency']. "\t" .$cont['customerType']. "\t" .$cont['conversion']. "\t" .$cont['leadDelta']. "\t" .$cont['leadTime']. "\t" .$cont['orderID']. "\t" .$cont['orderTotal']. "\t" .$cont['originalBasket']. "\t" .$cont['paymentMethod'].  "\t" .$cont['commission']. "\t" .$cont['saleDelta']. "\t" .$cont['saleTime']. "\t" .$cont['status']. "\t" .$cont['subaccount']. "\t" .$cont['subaccount2']. "\t" .$cont['subaccount3'].  "\t" .$cont['subaccount4']. "\t" .$cont['subaccount5']. "\t" .$cont['submissionID']. "\t" .$cont['wmCurrency']. "\t" .$cont['xid']. "\n");
				fclose($myfile);
				
				$sampless++;				
				$storenam =$this->db->escape_str($cont['offerName']);
				
				$tracking1=$cont['subaccount'];
				
				//$tracking_id= 'KDID230149';
				if($tracking1!=''){
					$tracking_id = $tracking1;
				}else{
					$tracking_id = '';
					continue;
				}
				
				$get_userid =  decode_userid($tracking_id);
				if($get_userid<=0 || $get_userid=='' )
				{
					continue;
				}
			
				$storeslisty = $this->get_offer_provider_cashback($storenam);
				$store_id = $storeslisty->affiliate_id;
				$cashback_percentage = $storeslisty->cashback_percentage;
				$sale_amount = $cont['commissionOpen'];
				$approved_payout = $cont['commission'];					
				if($storeslisty->affiliate_cashback_type=='Percentage')
				{						
					$cashback_amount = number_format(($sale_amount*$cashback_percentage)/100,2);
				}
				else
				{
					if($approved_payout>$cashback_percentage)
					{
						$cashback_amount = number_format(($cashback_percentage),2);
					}
				}
				
				$check_ref = $this->check_ref_user($get_userid);
				$ref_cashback_amount = 0;
				$ref_id = 0;
				$referred = 0;
				$txn_id_new = 0;
				if($check_ref>0)		
				{
					$ref_id  = $check_ref;
					$return = $this->check_active_user($ref_id);
					if($return==1)
					{
						$referred = 1;
						$ref_cashback_percent = $ref_cashbcak_percent;
						$ref_cashback_amount = number_format(($cashback_amount*$ref_cashbcak_percent)/100,2);
					}
				}
				
				$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
				
				$is_cashback = 1;	
				$affiliate_cashback_type=$storeslisty->affiliate_cashback_type;
				$transaction_id=$cont['submissionID'];					
				$pay_out_amount =$cont['commission'];
				$sale_amount =$cont['commissionOpen'];
				$date = $cont['leadTime'];
				//$offerid = $cont['Offer ID'];
				$status = $cont['status'];
				
				$this->db->where('transaction_id',$transaction_id);
				$result = $this->db->get('tbl_report');
				$now = date('Y-m-d H:i:s');	
				$last_updated = $now;
				if($result->num_rows == 0)
				{
					$results = $this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`, `is_cashback`, `cashback_percentage`, `affiliate_cashback_type`,`cashback_amount`, `ref_id`, `ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`) VALUES ('$storenam', '$date', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '$last_updated', '$is_cashback', '$cashback_percentage', '$affiliate_cashback_type','$cashback_amount', '$ref_id','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','$status');");	
					$insert_id = $this->db->insert_id();
					if($is_cashback!=0)	
					{	
						$now = date('Y-m-d H:i:s');	
						$mode = "Credited";	
						if($ref_cashback_amount!=0)	
						{
							$data = array(			
								'transation_amount' => $ref_cashback_amount,	
								'user_id' => $ref_id,	
								'transation_date' => $now,	
								'transation_reason' => 'Pending Referal Payment',	
								'mode' => $mode,	
								'details_id'=>'',	
								'table'=>'',	
								'transation_status' => 'Progress');
							$this->db->insert('transation_details',$data);
							$txn_id_new = $this->db->insert_id();
						}
	
						$data = array(
						'user_id' => $get_userid,	
						'coupon_id' => $storenam,	
						'affiliate_id' => $storenam,	
						'status' => 'Pending',	
						'cashback_amount'=>$cashback_amount,	
						'date_added' => $date,
						'referral' => $referred,
						'txn_id' => $txn_id_new
						);	
						$this->db->insert('cashback',$data);	
						
					/* mail for pending cashback */
					$user_detail = $this->view_user($get_userid);
					if($user_detail){
						foreach($user_detail as $user_detail_single){
							$referral_balance = $user_detail_single->balance;
							$user_email = $user_detail_single->email;
							$user_name = $user_detail_single->first_name.' '.$user_detail_single->last_name;
						}
					}
					$this->db->where('admin_id',1);
					$admin_det = $this->db->get('admin');
					if($admin_det->num_rows >0) 
					{    
						$admin = $admin_det->row();
						$admin_email = $admin->admin_email;
						$site_name = $admin->site_name;
						$admin_no = $admin->contact_number;
						$site_logo = $admin->site_logo;
					}
					
					$date =date('Y-m-d');
					
					$this->db->where('mail_id',12);
					$mail_template = $this->db->get('tbl_mailtemplates');
						if($mail_template->num_rows >0) 
						{
						   $fetch = $mail_template->row();
						   $subject = $fetch->email_subject;
						   $templete = $fetch->email_template;
						   // $url = base_url().'cashback/my_earnings/';
						   
							$this->load->library('email');
							
							$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
							);
							
							$sub_data = array(
								'###SITENAME###'=>$site_name
							);
							$subject_new = strtr($subject,$sub_data);
							
							// $this->email->initialize($config);
							 $this->email->set_newline("\r\n");
							   $this->email->initialize($config);
							   $this->email->from($admin_email);
							   $this->email->to($user_email);
							   $this->email->subject($subject_new);
						   
							$data = array(
								'###NAME###'=>$user_name,
								'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
								'###SITENAME###'=>$site_name,
								'###ADMINNO###'=>$admin_no,
								'###DATE###'=>$date,
								'###AMOUNT###'=>$cashback_amount
							);
						   
						   $content_pop=strtr($templete,$data);
							//$content_pop; echo $subject_new;
							
						   $this->email->message($content_pop);
						   $this->email->send();  
						}
					/* mail for pending cashback */
					}	
				}
				else{
					$duplicate+=1;
					$duplicate_trans_id .= $transaction_id.', ';
				}						
			}
			$array['duplicate'] = $duplicate;
			$array['trans_id'] = rtrim($duplicate_trans_id,', ');		
		}
		//print_r($array);die;
		return $array;	
	}
	//Afilio tracking transactions
	function importafilio_trackingtransactions($content)
	{
		
		$now = date('Y-m-d H:i:s');	
		$last_updated = $now;
		$tracking =$this->input->post('tracking_id');
		$name = $this->db->query("select * from admin")->row();	
		$ref_cashbcak_percent =  $name->referral_cashback;	
		if(count($content)!=0)
		{		
			
			$array = array();
			$duplicate = 0;
			$duplicate_trans_id = '';				
			$sampless =0;		
				
				
			$info=$_SERVER['REMOTE_ADDR'].'","'
			.$_SERVER['HTTP_USER_AGENT'].'","'
			.$_SERVER['HTTP_REFERER'].'","'
			.date("D dS M,Y h:i a").'"'."\n";
			
			$file= 'tracker_log.csv';
			$fp = fopen($file, "a");
			fputs($fp, $info);
			fclose($fp);
			
			foreach($content['list'] as $contss){			
				foreach($contss as $conts)
				{	
					foreach($conts as $cont)
					{	
						//Write a contents in logs file
				
						$myfile = fopen("trackinglogs.log", "a") or die("Unable to open file!");
						//$txt = "user id date";
						fwrite($myfile, $cont['saleid']. "\t" .$cont['status']."\t" .$cont['progid']. "\t" .$cont['order_id']. "\t" .$cont['order_price]']. "\t" .$cont['comission']. "\t" .$cont['date']. "\t" .$cont['payment']. "\t" .$cont['xtra']. "\n");
						fclose($myfile);
						
						$sampless++;				
						$storeid =$this->db->escape_str($cont['progid']);
						$storenam = $this->get_affiliatename($storeid);
				
				
						$tracking1=$cont['xtra'];
						
						//$tracking_id= 'KDID230149';
						if($tracking1!=''){
							$tracking_id = $tracking1;
						}else{
							$tracking_id = '';
							continue;
						}
						
						$get_userid =  decode_userid($tracking_id);
						if($get_userid<=0 || $get_userid=='' )
						{
							continue;
						}
					
						$storeslisty = $this->get_offer_provider_cashback($storenam->affiliate_name);
						$store_id = $storeslisty->affiliate_id;
						$cashback_percentage = $storeslisty->cashback_percentage;
						$sale_amount = $cont['order_price'];
						$approved_payout = $cont['comission'];					
						if($storeslisty->affiliate_cashback_type=='Percentage')
						{						
							$cashback_amount = number_format(($sale_amount*$cashback_percentage)/100,2);
						}
						else
						{
							if($approved_payout>$cashback_percentage)
							{
								$cashback_amount = number_format(($cashback_percentage),2);
							}
						}
						
						$check_ref = $this->check_ref_user($get_userid);
						$ref_cashback_amount = 0;
						$ref_id = 0;
						$referred = 0;
						$txn_id_new = 0;
						if($check_ref>0)		
						{
							$ref_id  = $check_ref;
							$return = $this->check_active_user($ref_id);
							if($return==1)
							{
								$referred = 1;
								$ref_cashback_percent = $ref_cashbcak_percent;
								$ref_cashback_amount = number_format(($cashback_amount*$ref_cashbcak_percent)/100,2);
							}
						}
						
						$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
						
						$is_cashback = 1;	
						$affiliate_cashback_type=$storeslisty->affiliate_cashback_type;
						$transaction_id=$cont['saleid'];					
						$pay_out_amount =$cont['order_price'];
						$sale_amount =$cont['Sales'];
						$date = $cont['date'];
						//$offerid = $cont['Offer ID'];
						$status = $cont['status'];
						
						$this->db->where('transaction_id',$transaction_id);
						$result = $this->db->get('tbl_report');
						$now = date('Y-m-d H:i:s');	
						$last_updated = $now;
						if($result->num_rows == 0)
						{
							$results = $this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`, `is_cashback`, `cashback_percentage`, `affiliate_cashback_type`,`cashback_amount`, `ref_id`, `ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`) VALUES ('$storenam', '$date', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '$last_updated', '$is_cashback', '$cashback_percentage', '$affiliate_cashback_type','$cashback_amount', '$ref_id','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','$status');");	
							$insert_id = $this->db->insert_id();
							if($is_cashback!=0)	
							{	
								$now = date('Y-m-d H:i:s');	
								$mode = "Credited";	
								if($ref_cashback_amount!=0)	
								{
									$data = array(			
										'transation_amount' => $ref_cashback_amount,	
										'user_id' => $ref_id,	
										'transation_date' => $now,	
										'transation_reason' => 'Pending Referal Payment',	
										'mode' => $mode,	
										'details_id'=>'',	
										'table'=>'',	
										'transation_status' => 'Progress');
									$this->db->insert('transation_details',$data);
									$txn_id_new = $this->db->insert_id();
								}
			
								$data = array(
								'user_id' => $get_userid,	
								'coupon_id' => $storenam,	
								'affiliate_id' => $storenam,	
								'status' => 'Pending',	
								'cashback_amount'=>$cashback_amount,	
								'date_added' => $date,
								'referral' => $referred,
								'txn_id' => $txn_id_new
								);	
								$this->db->insert('cashback',$data);	
								
							/* mail for pending cashback */
							$user_detail = $this->view_user($get_userid);
							if($user_detail){
								foreach($user_detail as $user_detail_single){
									$referral_balance = $user_detail_single->balance;
									$user_email = $user_detail_single->email;
									$user_name = $user_detail_single->first_name.' '.$user_detail_single->last_name;
								}
							}
							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name = $admin->site_name;
								$admin_no = $admin->contact_number;
								$site_logo = $admin->site_logo;
							}
							
							$date =date('Y-m-d');
							
							$this->db->where('mail_id',12);
							$mail_template = $this->db->get('tbl_mailtemplates');
								if($mail_template->num_rows >0) 
								{
								   $fetch = $mail_template->row();
								   $subject = $fetch->email_subject;
								   $templete = $fetch->email_template;
								   // $url = base_url().'cashback/my_earnings/';
								   
									$this->load->library('email');
									
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);
									
									$sub_data = array(
										'###SITENAME###'=>$site_name
									);
									$subject_new = strtr($subject,$sub_data);
									
									// $this->email->initialize($config);
									 $this->email->set_newline("\r\n");
									   $this->email->initialize($config);
									   $this->email->from($admin_email);
									   $this->email->to($user_email);
									   $this->email->subject($subject_new);
								   
									$data = array(
										'###NAME###'=>$user_name,
										'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###AMOUNT###'=>$cashback_amount
									);
								   
								   $content_pop=strtr($templete,$data);
									//$content_pop; echo $subject_new;
									
								   $this->email->message($content_pop);
								   $this->email->send();  
								}
							/* mail for pending cashback */
							}	
						}
						else{
							$duplicate+=1;
							$duplicate_trans_id .= $transaction_id.', ';
						}
					}
				}			
			}
			$array['duplicate'] = $duplicate;
			$array['trans_id'] = rtrim($duplicate_trans_id,', ');		
		}
		//print_r($array);die;
		return $array;	
	}
	//Rakuten (linkshare) tracking transactions
	function import_trackingtransactions($content)
	{
		$now = date('Y-m-d H:i:s');	
		$last_updated = $now;
		$tracking =$this->input->post('tracking_id');
		$name = $this->db->query("select * from admin")->row();	
		$ref_cashbcak_percent =  $name->referral_cashback;	
		
		if(count($content)!=0)
		{
			$array = array();
			$duplicate = 0;
			$duplicate_trans_id = '';				
			$sampless =0;
			
			
			$info=$_SERVER['REMOTE_ADDR'].'","'
			.$_SERVER['HTTP_USER_AGENT'].'","'
			.$_SERVER['HTTP_REFERER'].'","'
			.date("D dS M,Y h:i a").'"'."\n";
			
			$file= 'tracker_log.csv';
			$fp = fopen($file, "a");
			fputs($fp, $info);
			fclose($fp);
			
			foreach($content as $cont){			

				//Write a contents in logs file
				
             	$myfile = fopen("trackinglogs.log", "a") or die("Unable to open file!");
				//$txt = "user id date";
				fwrite($myfile, $cont['MID']. "\t" .$cont['Advertiser Name']."\t" .$cont['Click Through Rate (CTR)']. "\t" .$cont['Orders/Click']. "\t" .$cont['Earnings Per Click (EPC)']. "\t" .$cont['Sales']. "\t" .$cont['Baseline Commission']. "\t" .$cont['Adjusted Commission']. "\t" .$cont['Total Commission']. "\t" .$cont['Currency']. "\t" .$cont['Member ID (U1)']. "\t" .$cont['Network']. "\t" .$cont['Network ID']. "\t" .$cont['Offer ID']. "\t" .$cont['Offer Name']. "\t" .$cont['Product Name']. "\t" .$cont['Transaction Date']. "\t" .$cont['Transaction ID']. "\t" .$cont['id']. "\n");
				fclose($myfile);
				
				$sampless++;				
				$storenam =$this->db->escape_str($cont['Advertiser Name']);			
				$offerurl_id=$this->db->escape_str($cont['Offer ID']);
				$offerurl_name=$this->db->escape_str($cont['Offer Name']);
				$tracking1=$cont['Member ID (U1)'];
				
				//$tracking_id= 'KDID230149';
				if($tracking1!=''){
					$tracking_id = $tracking1;
				}else{
					$tracking_id = '';
					continue;
				}
				
				$get_userid =  decode_userid($tracking_id);
				if($get_userid<=0 || $get_userid=='' )
				{
					continue;
				}
			
				$storeslisty = $this->get_offer_provider_cashback($storenam);
				$store_id = $storeslisty->affiliate_id;
				$cashback_percentage = $storeslisty->cashback_percentage;
				$sale_amount = $cont['Sales'];
				$approved_payout = $cont['Total Commission'];					
				if($storeslisty->affiliate_cashback_type=='Percentage')
				{						
					$cashback_amount = number_format(($sale_amount*$cashback_percentage)/100,2);
				}
				else
				{
					if($approved_payout>$cashback_percentage)
					{
						$cashback_amount = number_format(($cashback_percentage),2);
					}
				}
				
				$check_ref = $this->check_ref_user($get_userid);
				$ref_cashback_amount = 0;
				$ref_id = 0;
				$referred = 0;
				$txn_id_new = 0;
				if($check_ref>0)		
				{
					$ref_id  = $check_ref;
					$return = $this->check_active_user($ref_id);
					if($return==1)
					{
						$referred = 1;
						$ref_cashback_percent = $ref_cashbcak_percent;
						$ref_cashback_amount = number_format(($cashback_amount*$ref_cashbcak_percent)/100,2);
					}
				}
				
				$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
				
				$is_cashback = 1;	
				$affiliate_cashback_type=$storeslisty->affiliate_cashback_type;
				$transaction_id=$cont['Transaction ID'];					
				$pay_out_amount =$cont['Total Commission'];
				$sale_amount =$cont['Sales'];
				$date = $cont['Transaction Date'];
				$offerid = $cont['Offer ID'];
				$status = 'Approved';
				
				$this->db->where('transaction_id',$transaction_id);
				$result = $this->db->get('tbl_report');
				$now = date('Y-m-d H:i:s');	
				$last_updated = $now;
				if($result->num_rows == 0)
				{
					$results = $this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`, `is_cashback`, `cashback_percentage`, `affiliate_cashback_type`,`cashback_amount`, `ref_id`, `ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`) VALUES ('$storenam', '$date', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '$last_updated', '$is_cashback', '$cashback_percentage', '$affiliate_cashback_type','$cashback_amount', '$ref_id','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','$status');");	
					$insert_id = $this->db->insert_id();
					if($is_cashback!=0)	
					{	
						$now = date('Y-m-d H:i:s');	
						$mode = "Credited";	
						if($ref_cashback_amount!=0)	
						{
							$data = array(			
								'transation_amount' => $ref_cashback_amount,	
								'user_id' => $ref_id,	
								'transation_date' => $now,	
								'transation_reason' => 'Pending Referal Payment',	
								'mode' => $mode,	
								'details_id'=>'',	
								'table'=>'',	
								'transation_status' => 'Progress');
							$this->db->insert('transation_details',$data);
							$txn_id_new = $this->db->insert_id();
						}
	
						$data = array(
						'user_id' => $get_userid,	
						'coupon_id' => $storenam,	
						'affiliate_id' => $storenam,	
						'status' => 'Pending',	
						'cashback_amount'=>$cashback_amount,	
						'date_added' => $date,
						'referral' => $referred,
						'txn_id' => $txn_id_new
						);	
						$this->db->insert('cashback',$data);	
						
					/* mail for pending cashback */
					$user_detail = $this->view_user($get_userid);
					if($user_detail){
						foreach($user_detail as $user_detail_single){
							$referral_balance = $user_detail_single->balance;
							$user_email = $user_detail_single->email;
							$user_name = $user_detail_single->first_name.' '.$user_detail_single->last_name;
						}
					}
					$this->db->where('admin_id',1);
					$admin_det = $this->db->get('admin');
					if($admin_det->num_rows >0) 
					{    
						$admin = $admin_det->row();
						$admin_email = $admin->admin_email;
						$site_name = $admin->site_name;
						$admin_no = $admin->contact_number;
						$site_logo = $admin->site_logo;
					}
					
					$date =date('Y-m-d');
					
					$this->db->where('mail_id',12);
					$mail_template = $this->db->get('tbl_mailtemplates');
						if($mail_template->num_rows >0) 
						{
						   $fetch = $mail_template->row();
						   $subject = $fetch->email_subject;
						   $templete = $fetch->email_template;
						   // $url = base_url().'cashback/my_earnings/';
						   
							$this->load->library('email');
							
							$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
							);
							
							$sub_data = array(
								'###SITENAME###'=>$site_name
							);
							$subject_new = strtr($subject,$sub_data);
							
							// $this->email->initialize($config);
							 $this->email->set_newline("\r\n");
							   $this->email->initialize($config);
							   $this->email->from($admin_email);
							   $this->email->to($user_email);
							   $this->email->subject($subject_new);
						   
							$data = array(
								'###NAME###'=>$user_name,
								'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
								'###SITENAME###'=>$site_name,
								'###ADMINNO###'=>$admin_no,
								'###DATE###'=>$date,
								'###AMOUNT###'=>$cashback_amount
							);
						   
						   $content_pop=strtr($templete,$data);
							//$content_pop; echo $subject_new;
							
						   $this->email->message($content_pop);
						   $this->email->send();  
						}
					/* mail for pending cashback */
					}	
				}
				else{
					$duplicate+=1;
					$duplicate_trans_id .= $transaction_id.', ';
				}
			}
			$array['duplicate'] = $duplicate;
			$array['trans_id'] = rtrim($duplicate_trans_id,', ');		
		}
		//print_r($array);die;
		return $array;	
	}

	/*New code for report upload Via API 15-6-16.*/
	function import_apireports($content,$affiliate_name)
	{
		//echo "<pre>";print_r($content); exit;
		$report_type = '';
		if(count($content)!=0)
		{
			$duplicate = 0;
			$duplicate_promo_id = '';
			
			if($affiliate_name == 'zanox')
			{
				//echo "<pre>";print_r($content); exit;
				if($content['saleItems']!='')
				{

					if(is_array($content['saleItems']['saleItem'][0]))
					{
						//echo "hai";
						//echo "hai1".$content['saleItems']['saleItem']; exit;
						foreach($content['saleItems']['saleItem'] as $cont)
						{
							  
							$all=0;
							//$transaction_id = $cont['clickId'];
							$transaction_id = $cont['@attributes']['id'];
							/*$this->db->where('transaction_id',$transaction_id);
							$all = $this->db->get('tbl_report')->num_rows();*/		

							$this->db->where('report_update_id',$transaction_id);
							$all = $this->db->get('cashback')->num_rows();		 	  	 
							 
							if($all == 0)
							{
								 
								$duplicate = 1;

								$user_id            = $cont['gpps']['gpp'][0];
								$val_user_id		= substr($user_id, 0, 5);
								 
								if(count($cont['gpps']['gpp']) == 2)
								{
									if($val_user_id == 'P0001')
									{
										$get_userid     = decode_userid($user_id);	
									}
									else
									{
										$get_userid     = $user_id;
									} 
									
								 
									$currency_type      = $cont['currency'];
									if($currency_type != 'BRL')
									{
										$amount   			= $cont['amount'];
										$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
									}	
									else
									{
										$transaction_amount = $cont['amount'];	
									}
										
									$coupon_name    = $cont['program'];
									//$offname         = preg_split("/[ ]/", $coupon_names);
					    			//$coupon_name     = trim($offname, " BR");


									$commission_amount  = $cont['commission'];
									$transaction_date   = $cont['trackingDate'];
									$status 			= $cont['reviewState'];
									
									$report_update_id   = $cont['@attributes']['id'];
									 
									//New code for referral payment and mail notification details 23-5-17
									$date 		     	 = date('Y-m-d');
									$userdetails     	 = $this->db->query("SELECT * from `tbl_users` where `user_id`=$get_userid")->row();
									$getuser 		 	 = $this->admin_model->view_user($get_userid);
									$check_ref 	     	 = $this->admin_model->check_ref_user($get_userid);
									$ref_cat_type    	 = $userdetails->ref_user_cat_type;
									
									$new_txn_id 		 = random_string('alnum', 16);//mt_rand(1000000000,9999999999);
									//$new_txn_id 		 = rand(1000,9999);
									$ref_cashback_amount = 0;
									$ref_id 			 = 0;
									$referred 			 = 0;
									$txn_id_new 		 = 0;
									$is_cashback 		 = 1;	
									$cashback_percentage = 0;
									$cashback_amt    	 = $cashback_amt;
									$new_sale_amount 	 = $new_sale_amount;
									$five_digit_num	     = 0;

									if($check_ref > 0)		
									{	
										$ref_id  = $check_ref;
										$return  = $this->admin_model->check_active_user($ref_id);
										$now  	 = date('Y-m-d');
										$n9   	 = '666554';
										$n12  	 = random_string('alnum', 16);//$n9 + $ref_id; 	
										$mode 	 = "Credited";	
										  
										if($return)
										{	
											$referred = 1;
											$i = 1;
											foreach($return as $newreturn)
											{
												
												$category_type        = $ref_cat_type;
												$referrals 	  		  = $this->db->query("select * from referral_settings where ref_id ='$category_type'")->row();	
												$ref_by_percentage    = $referrals->ref_by_percentage;
												//New code hide 23-5-17
												//$cashback_percentage  = $referrals->ref_cashback;
												//$dayscount		    = $referrals->valid_months;
												$ref_by_rate          = $referrals->ref_by_rate;
												$ref_cashback_amount  = $referrals->ref_cashback_rate;
												$bonus_by_ref_rate    = $referrals->bonus_by_ref_rate;

												//Code for (1** Type--Refferal by Percentage) 30-3-16//
												if($ref_by_percentage == 1)
												{	
													foreach($getuser as $newusers)
													{	
														/*new code for referral first type details getting user table 23-5-17*/
														$cashback_percentage = $newusers->typeONE_percentual;
														$dayscount 	  		 = $newusers->typeONE_days;
														/*End 23-5-17*/

														$reg_date     = date('Y-m-d', strtotime($newusers->date_added));
														$exp_date     = date('Y-m-d', strtotime("+$dayscount day", strtotime($reg_date)));
														$new_expdate  = strtotime($exp_date);
														$current_date = date("Y-m-d");
														$tday_date    = strtotime($current_date);
														$mailids 	  = $newusers->email;

														 
														if(($new_expdate) >= ($tday_date))
														{	
															$new_cashback_amount = (($cashback_amt)*($cashback_percentage)/100);  
															 
															$data = array(			
															'transation_amount' => $new_cashback_amount,	
															'user_id' => $ref_id,	
															'transation_date' => $now,
															'transation_id'=>$n12,	
															'transation_reason' => 'Referral Cashback amount',	
															'mode' => $mode,
															'transaction_date' => $now,
															'details_id'=>'',	
															'table'=>'',	
															'transation_status ' => 'Pending',
															'new_txn_id'=> $new_txn_id,
															'report_update_id'=>$newtransaction_id,
															'ref_user_tracking_id'=>$get_userid
															);

															$this->db->insert('transation_details',$data);
															$txn_id_new = $this->db->insert_id();

															/*New code 11-7-17*/
															/*$txn_ids    = rand(1000000,9999999);
															$txn_id_new = $txn_ids.$txn_id_new;*/
															/*End 11-7-17*/

															$referal_mail = $newreturn->referral_mail;
															$user_email   = $newreturn->email;
															$first_name   = $newreturn->first_name;
															$last_name 	  = $newreturn->last_name;								

															if($first_name == '' && $last_name == '')
															{
																$ex_name   = explode('@', $user_email);
																$user_name = $ex_name[0]; 
															}
															else
															{
																$user_name = $first_name.' '.$last_name;
															}

															//New mail code for Pending referral Cashback Mail 29-3-17//
															$this->db->where('admin_id',1);
															$admin_det = $this->db->get('admin');
															if($admin_det->num_rows() >0) 
															{    
																$admin 		 = $admin_det->row();
																$admin_email = $admin->admin_email;
																$site_name   = $admin->site_name;
																$admin_no 	 = $admin->contact_number;
																$site_logo 	 = $admin->site_logo;
															}
															$date =date('Y-m-d');
															 
															if($referal_mail == 1)
															{	
																 
																$this->db->where('mail_id',20);
																$mail_template = $this->db->get('tbl_mailtemplates');
																if($mail_template->num_rows() >0) 
																{
																	 
																	$fetch = $mail_template->row();
																	$subject = $fetch->email_subject;
																	$templete = $fetch->email_template;
																	$url = base_url().'my_earnings/';
																	$unsuburls	 = base_url().'un-subscribe/referral/'.$ref_id;
															   		$myaccount    = base_url().'minha-conta';
																	
																	$this->load->library('email');
																	$config = Array(
																	'mailtype'  => 'html',
																	'charset'   => 'utf-8',
																	);
																			
																	$sub_data = array(
																	'###SITENAME###'=>$site_name
																	);
																	
																	$subject_new = strtr($subject,$sub_data);
																	// $this->email->initialize($config);
																	$this->email->set_newline("\r\n");
																	$this->email->initialize($config);
																	$this->email->from($admin_email,$site_name.'!');
																	$this->email->to($user_email);
																	$this->email->subject($subject_new);
																	$datas = array(
																	'###NAME###'=>$user_name,
																	'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
																	'###SITENAME###'=>$site_name,
																	'###ADMINNO###'=>$admin_no,
																	'###DATE###'=>date('y-m-d'),
																	'###AMOUNT###'=>str_replace('.', ',', bcdiv($new_cashback_amount,1,2)),
																	'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
																	'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
																	);
																	$content_pop=strtr($templete,$datas);
																	$this->email->message($content_pop);
																	$this->email->send();  
																}
															}	
															//End 29-3-17//
														}
														else
														{
															$data = array(
																'status' => 'Inativa',
															);
															$this->db->where('referral_email',$mailids);
															$updates = $this->db->update('referrals',$data);	 
														}
													}
												}	
												$i++;	 
											} 
										}
										//End//
									}
									//End 23-5-17

									$this->db->where('transaction_id',$transaction_id);
									$tbl_all = $this->db->get('tbl_report')->num_rows();				 	  	 
						 
									if($tbl_all == 0)
									{	
										$ref_cashback_percent = 0;
										$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`cashback_amount`,`ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`,`five_digits`,`is_cashback`,`cashback_percentage`) 
										VALUES ('$coupon_name', '$transaction_date', '$commission_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s H:i:s')."','$commission_amount','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','".date('Y-m-d H:i:s')."','$report_update_id','$five_digit_num','$is_cashback','$cashback_percentage')");

											$insert_id = $this->db->insert_id();
									}	

									if($is_cashback!=0)
									{
										$this->db->select_max('cashback_id');
										$result 	 = $this->db->get('cashback')->row();  
										$cashback_id = $result->cashback_id;
										$cashback_id = $cashback_id+1;
										$n9  		 = '666554';
										$n12 		 = $n9 + $cashback_id;
										$now 		 = date('Y-m-d H:i:s');

										$this->db->where('report_update_id',$transaction_id);
										$cash_all = $this->db->get('cashback')->num_rows();

										if($cash_all == 0)
										{
											$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`cashback_amount`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`,`txn_id`)
											VALUES ('$get_userid','$coupon_name',$commission_amount,'$coupon_name','Pending','".date('Y-m-d H:i:s')."', '$transaction_amount', '".date('Y-m-d H:i:s')."','$new_txn_id','$commission_amount','$report_update_id','$txn_id_new')"); //$transaction_id
										
											//mail for pending cashback
											$this->db->where('admin_id',1);
											$admin_det = $this->db->get('admin');
											if($admin_det->num_rows() >0) 
											{    
												$admin 		 = $admin_det->row();
												$admin_email = $admin->admin_email;
												$site_name 	 = $admin->site_name;
												$admin_no 	 = $admin->contact_number;
												$site_logo 	 = $admin->site_logo;
											}

											if(count($userdetails) > 0)
											{
												$cashback_status = $userdetails->cashback_mail;
												$getuser_mail 	 = $userdetails->email;
												$first_name      = $userdetails->first_name;
												$last_name       = $userdetails->last_name;
												
												if($first_name == '' && $last_name == '')
												{
													$ex_name   = explode('@', $getuser_mail);
													$user_name = $ex_name[0]; 
												}
												else
												{
													$user_name = $first_name.' '.$last_name;
												}
												
												if($cashback_status == 1)
												{
													$this->db->where('mail_id',10);
													$mail_template = $this->db->get('tbl_mailtemplates');
													if($mail_template->num_rows() >0) 
													{
														
													   	$fetch 	   = $mail_template->row();
													    $subject   = $fetch->email_subject;
													    $templete  = $fetch->email_template;
													    $unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
													    $myaccount = base_url().'minha-conta';
													    //$url 	   = base_url().'cashback/my_earnings/';
													   
														$this->load->library('email');
														
														$config = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
														);
														
														$sub_data = array(
															'###SITENAME###'=>$site_name
														);
														$subject_new = strtr($subject,$sub_data);
														
														$this->email->set_newline("\r\n");
													    $this->email->initialize($config);
													    $this->email->from($admin_email,$site_name.'!');
													    $this->email->to($getuser_mail);
													    $this->email->subject($subject_new);
													   
														$data = array(
															'###NAME###'=>$user_name,
															'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
															'###SITENAME###'=>$site_name,
															'###ADMINNO###'=>$admin_no,
															'###DATE###'=>$date,
															'###AMOUNT###'=>str_replace('.', ',', bcdiv($cashback_amt,1,2)),
															'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
															'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
													    );
													   
													    $content_pop=strtr($templete,$data);
													    $this->email->message($content_pop);
													    $this->email->send();  
													}
												} 	
											}
										}
	 								}
	 							}	
							}
							else
							{
								$duplicate = 0;
							}		
						}	
					} 
					else
					{
						//echo "hai".$content['saleItems']['saleItem']['clickId']; exit;
						$all=0;
						//$transaction_id = $content['saleItems']['saleItem']['clickId'];
						$transaction_id   = $content['saleItems']['saleItem']['@attributes']['id'];
						$new_txn_id 	  = random_string('alnum', 16);
						$this->db->where('transaction_id',$transaction_id);
						$all = $this->db->get('tbl_report')->num_rows();				 	  	 
						if($all == 0)
						{

							$duplicate = 1;
							$user_id            = $content['saleItems']['saleItem']['gpps']['gpp'][0];
							$val_user_id		= substr($user_id, 0, 5);
								
							if($val_user_id == 'P0001')
							{
								$get_userid     = decode_userid($user_id);	
							}
							else
							{
								$get_userid     = $user_id;
							} 

							$currency_type      = $content['saleItems']['saleItem']['currency'];

							if($currency_type != 'BRL')
							{
								$amount   			= $content['saleItems']['saleItem']['amount'];
								$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
							}	
							else
							{
								$transaction_amount = $content['saleItems']['saleItem']['amount'];	
							}

							//$coupon_name      = $content['saleItems']['saleItem']['program'];
							$coupon_name     	= $content['saleItems']['saleItem']['program'];
							//$offname          = preg_split("/[ ]/", $coupon_names);
				    		//$coupon_name     	= trim($offname, " BR");


							$commission_amount  = $content['saleItems']['saleItem']['commission'];
							$transaction_date   = $content['saleItems']['saleItem']['trackingDate'];
							$transaction_amount = $content['saleItems']['saleItem']['amount'];
							$report_update_id   = $content['saleItems']['saleItem']['@attributes']['id'];
							
							/*$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
							VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Pending')");*/
							
							if($transaction_amount!='')
							{
								 
								$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`cashback_amount`,`ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`) 
								VALUES ('$coupon_name', '$transaction_date', '$commission_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s H:i:s')."','$cashback_amount','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','".date('Y-m-d H:i:s')."','$report_update_id')");
							}
							if($coupon_name !='')
							{
								$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`cashback_amount`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`)
								VALUES ('$get_userid','$coupon_name',$commission_amount,'$coupon_name','Pending','".date('Y-m-d H:i:s')."', '$transaction_amount', '".date('Y-m-d H:i:s')."','$new_txn_id','$commission_amount','$report_update_id')");	//transaction_id
							}
						}
					}
				}
				else
				{
					return 2;
				}		  
			}
			
			if($affiliate_name == 'lomadee')
			{
				//echo '<pre>'; print_r($content['item']); exit;
				if($content['item']!='')
				{
					$new_txn_id  = random_string('alnum', 16);
					if(is_array($content['item'][0]))
					{
						//echo "hai";
						foreach($content['item'] as $cont)
						{
							$all=0;

							$transaction_id   = $cont['transactionId'];
							$this->db->where('transaction_id',$transaction_id);
							$all = $this->db->get('tbl_report')->num_rows();
					 
							if($all == 0)
							{

								$duplicate = 1;
								$user_id            = $cont['associateId'];
							 	$get_userid     	= decode_userid($user_id);	
							 	 
							 	if($get_userid =='-230000')
							 	{
							 		$get_userid     = 0;
							 	}

							 	$currency_type      = $cont['currency'];
								if($currency_type != 'BRL')
								{
									$amount   			= $cont['gmv'];
									$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
								}	
								else
								{
									$transaction_amount = $cont['gmv'];
								}

								$coupon_id          = $cont['advertiser']['advertiserName'];
								$cashback_amount    = $cont['commission'];
								$old_trans_date     = $cont['checkoutDate'];
								$transaction_date   = preg_replace('/\//', '-',$old_trans_date);
								 
								
								 
								if($transaction_id !='')
								{
									$report_update_id   = $transaction_id;
								}else
								{
									$report_update_id   = rand(1000,9999);	
								}

								/*if($transaction_id != '')
								{
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d')."','".date('Y-m-d')."','Cashback','Credited','$report_update_id','Pending')");
								 
								}*/
								if($transaction_amount!='')
								{
									$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`cashback_amount`,`ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`) 
									VALUES ('$coupon_id', '$transaction_date', '$cashback_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s')."','$cashback_amount','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','".date('Y-m-d H:i:s')."','$report_update_id')");
								}
								
								
								if($coupon_id !='')
								{
									$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`)
									VALUES ('$get_userid','$coupon_id','$coupon_id','Pending','".date('Y-m-d')."', '$transaction_amount', '".date('Y-m-d')."','$new_txn_id','$cashback_amount','$report_update_id')"); //transaction_id
								}
							}	 
						}
					}	
					else
					{
						//echo "hello";
						$all = 0;
						$transaction_id  = $content['item']['transactionId'];
						$this->db->where('transaction_id',$transaction_id);
						$all = $this->db->get('tbl_report')->num_rows();
					 					 
						if($all == 0)
						{
							 
							$duplicate = 1;
							$user_id            = $content['item']['associateId'];
							$get_userid     	= decode_userid($user_id);	
							 
							if($get_userid =='-230000')
							{
								$get_userid     = 0;
							}
							$currency_type      = $content['item']['currency'];
							
							if($currency_type != 'BRL')
							{
								$amount   			= $content['item']['gmv'];
								$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
							}	
							else
							{
								$transaction_amount = $content['item']['gmv'];
							}

							$coupon_id          = $content['item']['advertiser']['advertiserName'];
							$cashback_amount    = $content['item']['commission'];
							$old_trans_date     = $content['item']['checkoutDate'];
							$transaction_date   = preg_replace('/\//', '-',$old_trans_date);
							//$transaction_amount = $content['item']['gmv'];
							//echo $transaction_amount; exit;
							$ref_cashback_percent= 0;
							$ref_cashback_amount = 0;
							$total_Cashback_paid = 0;
							if($transaction_id !='')
							{
								$report_update_id   = $transaction_id;
							}
							else
							{
								$report_update_id   = rand(1000,9999);	
							}

							/*if($transaction_id != '')
							{
								$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
								VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d')."','".date('Y-m-d')."','Cashback','Credited','$report_update_id','Pending')");
							}*/
							if($transaction_amount!='')
							{
								$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`cashback_amount`,`ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`) 
								VALUES ('$coupon_id', '$transaction_date', '$cashback_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s')."','$cashback_amount','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','".date('Y-m-d H:i:s')."','$report_update_id')");
							}
							if($coupon_id !='')
							{
								$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`)
								VALUES ('$get_userid','$coupon_id','$coupon_id','Pending','".date('Y-m-d')."', '$transaction_amount', '".date('Y-m-d')."','$transaction_id','$cashback_amount','$report_update_id')");
							}
						}
					}
				}
				else
				{
					return 2;
				}		  
			}
			
			//Pon prakesh start//
			if($affiliate_name == 'rakuten')
			{
				
				if($content!='')
				{
					//echo "hai"; exit;
					foreach($content as $cont)
					{
						
						$all=0;
						$new_txn_id 	   = random_string('alnum', 16);
						$transaction_id    = $cont['Transaction ID'];
						$this->db->where('transaction_id',$transaction_id);
						$all = $this->db->get('tbl_report')->num_rows();
			 
						if($all == 0)
						{
							$duplicate = 1;
							
							//transation_details table fields details//

							$transaction_id		 = $cont['Transaction ID'];
							//$transaction_amount	 = $cont['Sales'];
							$user_id 			 = $cont['Member ID (U1)'];
							$get_userid          = decode_userid($user_id);

							if($transaction_id !='')
							{
								$report_update_id   = $transaction_id;
							}else
							{
								$report_update_id   = rand(1000,9999);	
							}

							$currency_type      = $cont['Currency'];
							if($currency_type != 'BRL')
							{
								$amount   			= $cont['Sales'];
								$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
							}	
							else
							{
								$transaction_amount = $cont['Sales'];
							}

							//tbl_report table fields details//
							$coupon_name 		 = $cont['Product Name'];
							$old_trans_date 	 = $cont['Transaction Date'];
							$transaction_date    = preg_replace('/\//', '-',$old_trans_date);
							$pay_out_amount   	 = $cont['Total Commission'];
							$ref_cashback_percent= 0;
							$ref_cashback_amount = 0;
							$total_Cashback_paid = 0;

							//Cashback table fields details// 
							$cashback_amount     = $cont['Total Commission'];
							
							$now 				 = date('Y-m-d H:i:s');
							$current_date  		 = date('Y-m-d',strtotime($now));
							
							
							/*$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
							VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d')."','".date('Y-m-d')."','Cashback','Credited','$report_update_id','Pending')");*/

							if($transaction_amount != '')
							{
								$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`cashback_amount`,`ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`) 
								VALUES ('$coupon_name', '$transaction_date', '$pay_out_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s')."','$pay_out_amount','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','".date('Y-m-d H:i:s')."','$report_update_id')");
							}
							if($coupon_name !='')
							{	
								$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`)
								VALUES ('$get_userid','$coupon_name','$coupon_name','Pending','".date('Y-m-d')."', '$transaction_amount', '".date('Y-m-d')."','$new_txn_id','$cashback_amount','$report_update_id')"); //transaction_id
							}	
						}
						else
						{
							$duplicate = 0;
						}	
					}
				}
				else
				{
					return 2;
				}	
			}

			//new URL changes 20-6-16//
			if($affiliate_name == 'cityads')
			{
				//echo "<pre>";print_r($content); exit;
				if($content['data']['items']!='')
				{
					foreach($content['data']['items'] as $cont)
					{
							
						$all = 0;
						$new_txn_id 	   = random_string('alnum', 16);
						$transaction_id    = $cont['orderID'];
						$this->db->where('transaction_id',$transaction_id);
						$all = $this->db->get('tbl_report')->num_rows(); 
			 
						if($all == 0)
						{
							
							$duplicate 			 = 1;
							$offer_name			 = $cont['offerName'];
							$transaction_amount  = $cont['orderTotal'];
							$commission          = $cont['commissionOpen'];
							$user_id             = $cont['subaccount'];
							$get_userid          = decode_userid($user_id);
							$report_update_id 	 = $cont['submissionID'];

							/*$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
							VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Pending')");*/
							 
							if($transaction_amount!='')
							{	 
								$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`status`,`report_update_id`) 
								VALUES ('$offer_name', '".date('Y-m-d H:i:s')."', '$transaction_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','$report_update_id')");
							}
							
							if($offer_name !='')
							{
								$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`)
								VALUES ('$get_userid','$offer_name','$offer_name','Pending','".date('Y-m-d H:i:s')."', '$transaction_amount', '".date('Y-m-d H:i:s')."','$new_txn_id','$commission','$report_update_id')");	 //transaction_id
							}
						}
		 			}
	 			}
	 			else
	 			{
	 				return 2;
	 			}	 
			} 
			//Pon prakesh End//

			if($affiliate_name == 'afilio')
			{	
				//echo "<pre>";print_r($content['list']); exit;
				if($content['list']['sale']!='')
				{	
					foreach($content['list']['sale'] as $cont)
					{
						 
						$all=0;
						$new_txn_id 	   = random_string('alnum', 16);
						$transaction_id    = $cont['saleid']['value'];
						$this->db->where('transaction_id',$transaction_id);
						$all = $this->db->get('tbl_report')->num_rows();
						 
						if($all == 0)
						{

							$duplicate = 1;
							 
							$category_id 		 = $cont['progid']['value'];	
							$selqry        		 = $this->db->query("SELECT category_name from afilio_category_name_details where category_id='$category_id'")->row(); 
							$coupon_id   		 = utf8_encode($selqry->category_name);
							$user_id             = $cont['xtra']['value'];	
							$get_userid          = decode_userid($user_id);
							$commission_amount   = $cont['comission']['value'];
							$transaction_date    = $cont['date']['value'];
							//$transaction_date  = preg_replace('/\//', '-',$old_trans_date);
							$transaction_amount  = $cont['order_price']['value'];
							$order_id   		 = $cont['order_id']['value'];

							if($transaction_id !='')
							{
								$report_update_id   = $transaction_id;
							}else
							{
								$report_update_id   = rand(1000,9999);	
							}


							if($transaction_amount!='')
							{
								$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`cashback_amount`,`ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`) 
								VALUES ('$coupon_id', '$transaction_date', '$cashback_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s')."','$cashback_amount','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','".date('Y-m-d H:i:s')."','$report_update_id')");
							}
							if($coupon_id !='')
							{
								$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`)
								VALUES ('$get_userid','$coupon_id','$coupon_id','Pending','".date('Y-m-d H:i:s')."', '$transaction_amount', '".date('Y-m-d H:i:s')."','$new_txn_id','$cashback_amount','$report_update_id')"); //transaction_id
							}	
						}	 
					}
				}
				else
				{
					return 2;
				}
			}
		}	
		return $duplicate;
	}
	/*End*/

	/*New code for report upload Via API 15-6-16.*/
	function update_apireports($content,$affiliate_name)
	{
		//echo "<pre>"; print_r($this->input->post());exit;

		$site_status = $this->input->post('zanox_status');
		
		//echo "<pre>";print_r($content); exit;
		$report_type = '';
		if(count($content)!=0)
		{
			$duplicate = 0;
			$duplicate_promo_id = '';
			
			//Completed
			if($affiliate_name == 'zanox')
			{
				if($content['saleItems']!='')
				{	
					if(is_array($content['saleItems']['saleItem'][0]))
					{
						foreach($content['saleItems']['saleItem'] as $cont)
						{
							  
							$all=0;
							$transaction_id    = $cont['@attributes']['id'];
							$report_update_id  = $cont['@attributes']['id'];
							$newtransaction_id = $cont['@attributes']['id'];
							
							$cash_status 	   = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('status');

							if($newtransaction_id == '')
							{
								$newid             = rand(1000,9999);
								$newtransaction_id = md5($newid);
							}
							//echo $report_update_id; echo "<br>";
							//}exit;	
							$this->db->where('report_update_id',$report_update_id);
							$all = $this->db->get('cashback');				 	  	 
							//echo $this->db->last_query();
							$fetch = $all->row();
							 	
							 
							if(!empty($fetch))
							{
								//$fetch 	    = $all->row();
								$get_userid 	    = $fetch->user_id;	 
								$transaction_amount = $fetch->cashback_amount;
								$cash_id 			= $fetch->cashback_id;
								$duplicate = 1;
								
								/*$user_id            = $cont['gpps']['gpp'][0]; 
								$val_user_id		= substr($user_id, 0, 5); 
								if($val_user_id == 'P0001')
								{
									$get_userid     = decode_userid($user_id);	
								}
								else
								{
									$get_userid     = $user_id;
								}*/ 
								  
							  
								/*$currency_type      = $cont['currency'];
								if($currency_type != 'BRL')
								{
									//$amount   			= $cont['amount']; //$cont['commission'];
									$amount  		    = $fetch->cashback_amount;
									$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
								}	
								else
								{
									$transaction_amount = $cont['amount']; //$cont['commission'];	
								}*/

								$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$get_userid))->row('balance');
								if($prev_userbal == '')
								{
									$prev_userbal = 0;	
								}

								$status = $cont['reviewState'];

								if($status == 'open')
								{
									$tstatus = 'Pending';
									$cstatus = 'Pending';
								}

								if($status == 'approved')
								{
									if($site_status == 'all_confirm' || $site_status == 'approve_confirm')
									{
										$tstatus = 'Approved';
										$cstatus = 'Completed';
									}
									if($site_status == 'all_pending' || $site_status == 'approve_pending')
									{
										$tstatus = 'Pending';
										$cstatus = 'Pending';
									}
								}

								if($status == 'rejected')
								{
									$tstatus = 'Canceled';
									$cstatus = 'Canceled';
								}
								if($status == 'confirmed')
								{
									$tstatus = 'Credited';
									$cstatus = 'Completed';
								}

								

								if($status !='')
								{

									//New hide 12-7-17
									/*if($tstatus == 'Approved')
									{
										if($site_status == 1)
										{
											//$data = array('transation_status' => $tstatus); 
											//$this->db->where('report_update_id',$report_update_id);
											//$updation = $this->db->update('transation_details',$data);
											//New hide 11-7-17//
											$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
											VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");
											//End 11-7-17//
										}
									}*/
									//New hide 12-7-17

									if($tstatus == 'Canceled')
									{
										//$data = array('transation_status' => $tstatus); 
										//$this->db->where('report_update_id',$report_update_id);
										//$updation = $this->db->update('transation_details',$data);
										/*New hide 11-7-17*/
										//$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
										//VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Canceled')");
									}

									/*New code for mail notification details 1-3-17*/
									$userdetails 	 = $this->db->query("SELECT * from tbl_users where user_id=$get_userid")->row();
									$cashback_status = $userdetails->cashback_mail;
									$user_email      = $userdetails->email;
									$first_name      = $userdetails->first_name;
									$last_name       = $userdetails->last_name;
									$balance  		 = $userdetails->balance;
									if($balance == '')
									{	
										$balance == '0';
									}
									if($first_name == '' && $last_name == '')
									{
										$ex_name   = explode('@', $user_email);
										$user_name = $ex_name[0]; 
									}
									else
									{
										$user_name = $first_name.''.$last_name;
									}

									$this->db->where('admin_id',1);
									$admin_det = $this->db->get('admin');
									if($admin_det->num_rows >0) 
									{    
										$admin 		 = $admin_det->row();
										$admin_email = $admin->admin_email;
										$site_name 	 = $admin->site_name;
										$admin_no	 = $admin->contact_number;
										$site_logo 	 = $admin->site_logo;
									}
									$date = date('Y-m-d');
									/*end 1-3-17*/

									if($cstatus == 'Completed')
									{
										if($cash_status != 'Completed')
										{
											 	
										 	$data = array('status' => $cstatus); 
											$this->db->where('report_update_id',$report_update_id);
											$updation = $this->db->update('cashback',$data);	


											$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
											VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");
											 

											$this->db->where('user_id',$get_userid);
											$this->db->update('tbl_users',array('balance'=>$prev_userbal+$transaction_amount));
											
											//mail for Completed cashback 1-3-17
											if($cashback_status == 1)
											{
												$this->db->where('mail_id',8);
												$mail_template = $this->db->get('tbl_mailtemplates');
												if($mail_template->num_rows >0) 
												{
												   	$fetch     = $mail_template->row();
												   	$subject   = $fetch->email_subject;
												   	$templete  = $fetch->email_template;
												   	$url 	   = base_url().'my_earnings/';
												   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
												   	$myaccount = base_url().'minha_conta';
												   
												  	$this->load->library('email');
													
													$config = Array(
														'mailtype'  => 'html',
														'charset'   => 'utf-8',
													);
													
													$sub_data = array(
														'###SITENAME###'=>$site_name
													);

													$subject_new = strtr($subject,$sub_data);

													$this->email->set_newline("\r\n");
													$this->email->initialize($config);
													$this->email->from($admin_email, $site_name.'!');
													$this->email->to($user_email);
													$this->email->subject($subject_new);
												   
													$data = array(
														'###NAME###'=>$user_name,
														'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
														'###SITENAME###'=>$site_name,
														'###ADMINNO###'=>$admin_no,
														'###DATE###'=>$date,
														'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
														'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
														'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
												    );
												   
												   $content_pop=strtr($templete,$data);
												   $this->email->message($content_pop);
												   $this->email->send();  
												}
											} 
											//end 1-3-17

											//New code for referral mail and user cashback balance details 03-5-17
											$this->db->where('report_update_id',$report_update_id);
											$cashbacks 	   = $this->db->get('cashback');
											$cashback_data = $cashbacks->row();

											if($cashback_data->referral!=0)
											{
												$this->db->where('trans_id',$cashback_data->txn_id);
												$txn = $this->db->get('transation_details');
												$txn_detail = $txn->row();

												if($txn_detail)
												{
													$txn_id 	 	    = $txn_detail->trans_id;
													$ref_user_id 	    = $txn_detail->user_id;
													$transation_amounts = $txn_detail->transation_amount;
													$refer_user 	    = $this->view_user($ref_user_id);
													
													if($refer_user)
													{
														foreach($refer_user as $single)
														{
															$referral_balance = $single->balance;
															$user_email 	  = $single->email;
															$first_name 	  = $single->first_name;
															$last_name 		  = $single->last_name;

															if($first_name == '' && $last_name == '')
															{
																$ex_name   = explode('@', $user_email);
																$user_name = $ex_name[0]; 
															}
															else
															{
																$user_name = $first_name.' '.$last_name;
															}
														}

														//Update refer user balance details in User table and transaction table(approve referral cashback amount)//
														$this->db->where('user_id',$ref_user_id);
														$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amounts));
														 

														$data = array('transation_status'=>'Approved','transation_reason'=>'Referal Payment'); //'transaction_date'=>$date,
														//$this->db->where('transation_reason','Pending Referal Payment');
														$this->db->where('trans_id',$cashback_data->txn_id);
														$this->db->update('transation_details',$data);

														//mail for Approve cashback amt mail notifications
														if($single->referral_mail == 1)
														{	
															$this->db->where('mail_id',9);
															$mail_template = $this->db->get('tbl_mailtemplates');
															if($mail_template->num_rows >0) 
															{
															   $fetch 	  = $mail_template->row();
															   $subject   = $fetch->email_subject;
															   $templete  = $fetch->email_template;
															   $url 	  = base_url().'my_earnings/';
															   $unsuburls = base_url().'un-subscribe/referral/'.$ref_user_id;
												   			   $myaccount = base_url().'minha_conta';
															   
																$this->load->library('email');
																
																$config = Array(
																	'mailtype'  => 'html',
																	'charset'   => 'utf-8',
																);
																
																$sub_data = array(
																	'###SITENAME###'=>$site_name
																);
																$subject_new = strtr($subject,$sub_data);
																
																$this->email->set_newline("\r\n");
																$this->email->initialize($config);
																$this->email->from($admin_email,$site_name.'!');
																$this->email->to($user_email);
																$this->email->subject($subject_new);
															   
																$datas = array(
																	'###NAME###'=>$user_name,
																	'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
																	'###SITENAME###'=>$site_name,
																	'###ADMINNO###'=>$admin_no,
																	'###DATE###'=>$date,
																	'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amounts,1,2)),
																	'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
																	'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
																);
															   
															   $content_pop=strtr($templete,$datas);
															   $this->email->message($content_pop);
															   $this->email->send();  
															}
														}	
														//Mail for Approve referral cashback amount mail End
													}
												}
											}
											//end 3-5-17

											//first and second Withdraw mail notification for referral users 3-5-17
											$this->db->where('admin_id',1);
											$Admin_Details_Query    = $this->db->get('admin');
											$Admin_Details 		    = $Admin_Details_Query->row();
											$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
											$remain_minimum_amt     = $Admin_Details->remain_minimum_with_amt;
											$Site_Logo 				= $Admin_Details->site_logo;
											$admin_emailid 			= $Admin_Details->admin_email;

											if($ref_user_id!='')
											{

												$this->db->where('report_update_id',$report_update_id);
												$cashbacks 	   = $this->db->get('cashback');
												$cashback_data = $cashbacks->row();

												if($cashback_data->referral!=0)
												{
													$this->db->where('trans_id',$cashback_data->txn_id);
													$txn = $this->db->get('transation_details');
													$txn_detail = $txn->row();
													
													if($txn_detail)
													{
														//$new_txn_ids = $txn_detail->new_txn_id;
														$new_txn_ids   = $txn_detail->trans_id;
													
														if($single->referral_category_type != '')
														{ 	
															$referrals    	    = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
															$ref_by_percentage  = $referrals->ref_by_percentage;
															$ref_by_rate 		= $referrals->ref_by_rate;
															$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

															//3** Bonus by Refferal Rate type//
															if($bonus_by_ref_rate == 1)
															{
																$n9  	 = '333445';
																$n12 	 = $n9 + $ref_user_id;
																$now 	 = date('Y-m-d H:i:s');	
																$selqry  = "SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Referal Payment' AND user_id=$ref_user_id"; 
														 		$query   = $this->db->query("$selqry");
																$numrows = $query->num_rows();
																if($numrows > 0)
																{
																	$fetch 		   = $query->row();
																	$usercount 	   = $fetch->userid;
																	$referrals     = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
																	$bonus_amount  = $referrals->ref_cashback_rate_bonus;
																	$friends_count = $referrals->friends_count;
																	
																	if($usercount == $friends_count)
																	{	
																		if($bonus_amount!='')
																		{	
																			if($single->referral_category_type == 1)
																			{ 
																			 	$types = 'One';
																			}
																			if($single->referral_category_type == 2)
																			{ 
																			 	$types = 'Two';
																			}
																			if($single->referral_category_type == 3)
																			{ 
																			 	$types = 'Three';
																			}
																			if($single->referral_category_type == 4)
																			{ 
																			 	$types = 'Four';
																			}
																			if($single->referral_category_type == 5)
																			{ 
																			 	$types = 'Five';
																			}
																			if($single->referral_category_type == 6)
																			{ 
																			 	$types = 'Six';
																			}
																			if($single->referral_category_type == 7)
																			{ 
																			 	$types = 'Seven';
																			}
																			if($single->referral_category_type == 8)
																			{ 
																			 	$types = 'Eight';
																			}
																			if($single->referral_category_type == 9)
																			{ 
																			 	$types = 'Nine';
																			}
																			if($single->referral_category_type == 10)
																			{ 
																			 	$types = 'Ten';
																			}

																			$selqry	= "SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category $types User' AND `user_id`=$ref_user_id"; 
															 				$query1	= $this->db->query("$selqry");
															 				$newnumrows = $query1->num_rows();
																			if($newnumrows > 0)
																			{
																				$fetch 		 = $query1->row();
																				$users_count = $fetch->userid;
																				if($users_count == 0)	
																				{	
																					$data = array(			
																					'transation_amount' => $bonus_amount,	
																					'user_id' => $ref_user_id,	
																					'transation_date' => $now,
																					'transaction_date' => $now,
																					'transation_id'=>$n12,	
																					'transation_reason' => 'Referral Bonus for Category ' .$types. ' User',	
																					'mode' => 'Credited',
																					'details_id'=>'',	
																					'table'=>'',	
																					'new_txn_id'=>0,
																					'transation_status ' => 'Approved',
																					'report_update_id'=>$newtransaction_id
																					);	
																					$this->db->insert('transation_details',$data);
																				}	
																			}
																		}	
																	}
																} 
															}
															//3** Bonus by Refferal Rate type End//

															//1** Refferal by Percentage type Start//
															if($ref_by_percentage == 1)
															{
																$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); /*,'transaction_date'=>$date*/
																$this->db->where('trans_id',$new_txn_ids);
																$this->db->update('transation_details',$data);
															}
															//1** Refferal by Percentage type End//	
														}	
													}
												}

												//New code for withdraw notification 1-4-17
												$ref_prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$ref_user_id))->row('balance');
												if($ref_prev_userbal == '')
												{
													$ref_prev_userbal = 0;	
												}


												//$ref_Total_Amount = ($ref_balance+$ref_cashback_amount);
												$ref_Total_Amount 	= ($referral_balance+$transation_amounts);
												$ref_User_details 	= $this->view_user($ref_user_id);
												$ref_us_email 	  	= $ref_User_details[0]->email;
												$ref_with_status 	= $ref_User_details[0]->withdraw_mail;
												$ref_unsuburl	  	= base_url().'un-subscribe/withdraw/'.$ref_user_id;
												$ref_myaccount    	= base_url().'resgate';
												$ref_firstname 	  	= $ref_User_details[0]->first_name;
												$ref_lastname  	  	= $ref_User_details[0]->last_name;
												$with_mail_status 	= $ref_User_details[0]->first_withdraw_mail_status;
												$first_with_status  = $ref_User_details[0]->first_withdraw_status;
												$second_with_status = $ref_User_details[0]->second_withdraw_status;

												if($with_mail_status == 1)
												{
													$Admin_Minimum_Cashback = $remain_minimum_amt;
													$new_withdraw_status    = $second_with_status;
												}
												else
												{
													$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;	
													$new_withdraw_status    = $first_with_status;
												}

												if($ref_firstname == '' && $ref_lastname == '')
												{
													$ex_name  	  = explode('@', $ref_User_details[0]->email);
													$ref_username = $ex_name[0]; 
												}
												else
												{
													$ref_username = $ref_firstname.' '.$ref_lastname;
												}	
												//End 9-1-17
												 
												if($ref_with_status == 1)
												{
													if($new_withdraw_status == 0)
													{
														if($ref_Total_Amount>=$Admin_Minimum_Cashback)
														{
															$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
															if($obj_temp->num_rows>0)
															{
																$mail_temp  = $obj_temp->row(); 
																$fe_cont    = $mail_temp->email_template;	
																$subject  	= $mail_temp->email_subject;	
																$servername = base_url();
																$nows 		= date('Y-m-d');	
																$this->load->library('email');
																$gd_api=array(
																	
																	'###NAME###'=>$ref_username,
																	'###AMOUNT###'=>str_replace('.', ',', bcdiv($ref_Total_Amount,1,2)),
																	'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
																	'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
																	'###ULINK###'=>'<a href='.$ref_unsuburl.'>'.$ref_unsuburl.'</a>',
																	'###MYACLINK###'=>'<a href='.$ref_myaccount.'>'.$ref_myaccount.'</a>'
																	);
																				   
																$gd_message=strtr($fe_cont,$gd_api);
																$config = Array(
																	'mailtype'  => 'html',
																	'charset'   => 'utf-8',
																);

																$this->email->set_newline("\r\n");
																$this->email->initialize($config);
																$this->email->from($admin_emailid,$site_name.'!');
																$this->email->to($ref_us_email);
																$this->email->subject($subject);
																$this->email->message($gd_message);
																$this->email->send();
																$this->email->print_debugger();

																//new code for update a first or second withdraw amount status 20-5-17
																if($new_withdraw_status == 0)
																{
																	if($with_mail_status == 1)
																	{
																		$data = array(		
																		'second_withdraw_status'  => 1);
																		$this->db->where('user_id',$ref_user_id);
																		$update_qry= $this->db->update('tbl_users',$data);
																	}
																	else
																	{
																		$data = array(		
																		'first_withdraw_status'  => 1);
																		$this->db->where('user_id',$ref_user_id);
																		$update_qry= $this->db->update('tbl_users',$data);
																	}	
																}
																//End
															}
														}
													}	
												}
												//end 1-4-17		
											}
											//End mail notification for referral users 3-5-17

											//first and second withdraw mail notification for refered user 3-5-17
											$Total_Amount 	  	= ($balance+$transaction_amount);
											$User_details 	  	= $this->view_user($get_userid);
											$us_email 		  	= $User_details[0]->email;
											$with_status      	= $User_details[0]->withdraw_mail;
											$firstname 		  	= $User_details[0]->first_name;
											$lastname  		  	= $User_details[0]->last_name;
											$with_mail_status 	= $User_details[0]->first_withdraw_mail_status;
											$unsuburl	 	  	= base_url().'un-subscribe/withdraw/'.$get_userid;
											$myaccount    	  	= base_url().'resgate';
											$first_with_status  = $User_details[0]->first_withdraw_status;
											$second_with_status = $User_details[0]->second_withdraw_status;

											if($with_mail_status == 1)
											{
												//$Admin_Minimum_Cashback = $remain_minimum_amt;
												$Admin_Minimum_Cashback = $remain_minimum_amt;
												$new_withdraw_status    = $second_with_status;
											}
											else
											{
												$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;
												$new_withdraw_status    = $first_with_status;	
											}

											if($firstname == '' && $lastname == '')
											{
												$ex_name  = explode('@', $User_details[0]->email);
												$username = $ex_name[0]; 
											}
											else
											{
												$username = $firstname.' '.$lastname;
											}	
											//End 9-1-17
											
											 
											if($with_status == 1)
											{
												if($new_withdraw_status == 0)
												{
													if($Total_Amount>=$Admin_Minimum_Cashback)
													{
														$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
														if($obj_temp->num_rows>0)
														{
															$mail_temp  = $obj_temp->row(); 
															$fe_cont    = $mail_temp->email_template;	
															$subject  	= $mail_temp->email_subject;		
															$servername = base_url();
															$nows 		= date('Y-m-d');	
															$this->load->library('email');
															$gd_api=array(
																
																'###NAME###'=>$username,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($Total_Amount,1,2)),
																'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
																'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
																'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
																);
																			   
															$gd_message=strtr($fe_cont,$gd_api);
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);
															 
															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_emailid,$site_name.'!');
															$this->email->to($us_email);
															$this->email->subject($subject);
															$this->email->message($gd_message);
															$this->email->send();
															$this->email->print_debugger();

															//new code for update a first or second withdraw amount status 20-5-17
															if($new_withdraw_status == 0)
															{
																if($with_mail_status == 1)
																{
																	$data = array(		
																	'second_withdraw_status'  => 1);
																	$this->db->where('user_id',$get_userid);
																	$update_qry= $this->db->update('tbl_users',$data);
																}
																else
																{
																	$data = array(		
																	'first_withdraw_status'  => 1);
																	$this->db->where('user_id',$get_userid);
																	$update_qry= $this->db->update('tbl_users',$data);
																}	
															}
															//End
														}
													}
												}	
											}	
											 	
											//End 9-1-17
											//End mail notification for referred user 3-5-17
											//End 3-5-17
										}	
									}

									if($cstatus == 'Canceled')
									{
										if($cash_status != 'Canceled')
										{
											$data = array('status' => $cstatus); 
											$this->db->where('report_update_id',$report_update_id);
											$updation = $this->db->update('cashback',$data);
											
											//mail for Canceled cashback 1-3-17
											if($cashback_status == 1)
											{
												$this->db->where('mail_id',11);
												$mail_template = $this->db->get('tbl_mailtemplates');
												if($mail_template->num_rows >0) 
												{

												   	$fetch 	   = $mail_template->row();
												   	$subject   = $fetch->email_subject;
												   	$templete  = $fetch->email_template;
												   	$url 	   = base_url().'cashback/my_earnings/';
												   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
												   	$myaccount = base_url().'minha_conta';
												   
													$this->load->library('email');
													
													$config = Array(
														'mailtype'  => 'html',
														'charset'   => 'utf-8',
													);
													
													$sub_data = array(
														'###SITENAME###'=>$site_name
													);
													$subject_new = strtr($subject,$sub_data);
													
													//$this->email->initialize($config);
													$this->email->set_newline("\r\n");
													$this->email->initialize($config);
													$this->email->from($admin_email,$site_name.'!');
													$this->email->to($user_email);
													$this->email->subject($subject_new);
												   
													$data = array(
														'###NAME###'=>$user_name,
														'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
														'###SITENAME###'=>$site_name,
														'###ADMINNO###'=>$admin_no,
														'###DATE###'=>$date,
														'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
														'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
														'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
												    );
												   
												   $content_pop=strtr($templete,$data);
												   $this->email->message($content_pop);
												   $this->email->send();  
												}
											} 
											//end 1-3-17 

											//New code for update user table 4-5-17
											if($cash_status =='Completed' && $cstatus == 'Canceled')
											{
												$user_bale 		= $balance;
												$newbalnce 		= $user_bale - $transaction_amount;
													
												$data = array(		
												'balance' => $newbalnce);
												$this->db->where('user_id',$get_userid);
												$update_qry = $this->db->update('tbl_users',$data);

												/*new code 26-7-17*/
												$this->db->delete('transation_details',array('report_update_id' => $report_update_id,'transation_reason'=>'Cashback','user_id'=>$get_userid));
												/*End 26-7-17*/
											}
											//End 4-5-17

											//Mail for cancelled referral amount for reffered user mail 04-5-17
											$this->db->where('report_update_id',$report_update_id);
											$cashbacks 	   = $this->db->get('cashback');
											$cashback_data = $cashbacks->row();
											if($cashback_data->referral!=0)
											{
												$this->db->where('trans_id',$cashback_data->txn_id);
												$txn 		= $this->db->get('transation_details');
												$txn_detail = $txn->row();
												
												if(!empty($txn_detail))
												{
													$txn_id 	 	   = $txn_detail->trans_id;
													$ref_user_id 	   = $txn_detail->user_id;
													$transation_amount = $txn_detail->transation_amount;
													$newtrans_status   = $txn_detail->transation_status; 
													$refer_user 	   = $this->view_user($ref_user_id);

													if($refer_user)
													{
														foreach($refer_user as $single)
														{
															$referral_balance = $single->balance;
															$user_email 	  = $single->email;
															$first_name 	  = $single->first_name;
															$last_name 		  = $single->last_name;
															$reffer_status    = $single->referral_mail;

															if($referral_balance == '')
															{
																$referral_balance == '0';
															}

															if($first_name == '' && $last_name == '')
															{
																$ex_name   = explode('@', $user_email);
																$user_name = $ex_name[0]; 
															}
															else
															{
																$user_name = $first_name.' '.$last_name;
															}
														}

														if($cstatus == 'Canceled')
														{
															if($newtrans_status == 'Approved')
															{
																$new_balance = ($referral_balance - $transation_amount);
																$this->db->where('user_id',$ref_user_id);
																$this->db->update('tbl_users',array('balance'=>$new_balance));
															}
															/*new code 11-7-17*/
															$this->db->delete('transation_details',array('report_update_id' => $txn_detail->report_update_id,'transation_reason'=>'Cashback'));
															/*End 11-7-17*/
														}

														$data = array(	
														'transation_status'=>$tstatus
														);
														$this->db->where('trans_id',$cashback_data->txn_id);
														//$this->db->where('report_update_id',$report_update_id);
														$updation = $this->db->update('transation_details',$data);
															
														$unsuburl  = base_url().'un-subscribe/referral/'.$ref_user_id;
													    $myaccount = base_url().'minha-conta';

														$this->db->where('admin_id',1);
														$admin_det = $this->db->get('admin');
														if($admin_det->num_rows >0) 
														{    
															$admin 		 = $admin_det->row();
															$admin_email = $admin->admin_email;
															$site_name 	 = $admin->site_name;
															$admin_no 	 = $admin->contact_number;
															$site_logo 	 = $admin->site_logo;
														}

														if($reffer_status == 1)
														{
															$this->db->where('mail_id',19);
															$mail_template = $this->db->get('tbl_mailtemplates');
															if($mail_template->num_rows >0) 
															{
															   $fetch = $mail_template->row();
															   $subject = $fetch->email_subject;
															   $templete = $fetch->email_template;
																
																$config = Array(
																	'mailtype'  => 'html',
																	'charset'   => 'utf-8',
																);
																
																// $this->email->initialize($config);
																$this->email->set_newline("\r\n");
																$this->email->initialize($config);
																$this->email->from($admin_email,$site_name.'!');
																$this->email->to($user_email);
																$this->email->subject($subject);
															   
																$data = array(
																	'###ADMINNO###'=>$admin_no,
																	'###EMAIL###'=>$user_name,
																	'###DATE###'=>$date,
																	'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
																	'###SITENAME###' =>$site_name,
																	'###STATUS###'=>$status,
																	'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amount,1,2)),
																	'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
																	'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
															    );
															   
															   $content_pop=strtr($templete,$data);
															   $this->email->message($content_pop);
															   $this->email->send();  
															}
														}
													}
													/*end*/
												}	
											}
											//End 4-5-17
										}	
									}

									if($cstatus == 'Pending')
									{
										//New hide 25-7-17
										/*if($cash_status != 'Pending')
										{
											$data = array('status' => $cstatus); 
											$this->db->where('report_update_id',$report_update_id);
											$updation = $this->db->update('cashback',$data);
											
											//if($site_status == '')
											//{
												//New code for update user balance details 4-5-17
												if($cash_status =='Completed') 
												{	
													$user_bale 		= $balance;
													$newbalnce 		= $user_bale - $transaction_amount;
														
													$data = array(		
													'balance' => $newbalnce);
													$this->db->where('user_id',$get_userid);
													$update_qry = $this->db->update('tbl_users',$data);

													//New code 25-7-17
													$this->db->delete('transation_details',array('transation_id'=>$cash_id,'report_update_id'=>$report_update_id));
													//End 25-7-17
												}
												//end 4-5-17

												//mail for Pending cashback 1-3-17
												if($cashback_status == 1)
												{
													$this->db->where('mail_id',10);
													$mail_template = $this->db->get('tbl_mailtemplates');
													
													if($mail_template->num_rows >0) 
													{
													    $fetch     = $mail_template->row();
													    $subject   = $fetch->email_subject;
													    $templete  = $fetch->email_template;
													    $unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
													    $myaccount = base_url().'minha_conta';
													   
														$this->load->library('email');
														
														$config = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
														);
														
														$sub_data = array(
															'###SITENAME###'=>$site_name
														);
														$subject_new = strtr($subject,$sub_data);
														
														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_email,$site_name.'!');
														$this->email->to($user_email);
														$this->email->subject($subject_new);
													   
														$data = array(
															'###NAME###'=>$user_name,
															'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
															'###SITENAME###'=>$site_name,
															'###ADMINNO###'=>$admin_no,
															'###DATE###'=>$date,
															'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
															'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
															'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
													    );
													   
													    $content_pop=strtr($templete,$data);
													   	$this->email->message($content_pop);
													   	$this->email->send();  
													}
												} 
												//end 1-3-17//	

												//New code for pending referral mail for (REFER) User 4-5-17//
												$check_ref    = $this->check_ref_user($get_userid);

												if($check_ref > 0)		
												{
													$ref_id  	  = $check_ref;
													$ref_user_bal = $this->view_balance($check_ref);
													$return  	  = $this->check_active_user($ref_id);

													if($return)
													{
														if($cash_status  == 'Completed')
														{	
															$this->db->where('ref_user_tracking_id',$get_userid);
															$this->db->where('report_update_id',$report_update_id);
															$this->db->where('transation_status','Paid');
															$this->db->or_where('transation_status','Approved');
															$all = $this->db->get('transation_details');				 	  	 
															$ref_trans_details = $all->row();
															//$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Approved'")->row();	
														
														}
														if($cash_status  == 'Canceled')
														{
															$this->db->where('ref_user_tracking_id',$get_userid);
															$this->db->where('report_update_id',$report_update_id);
															$this->db->where('transation_status','Canceled');
															$all = $this->db->get('transation_details');				 	  	 
															$ref_trans_details = $all->row();
															//$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Canceled'")->row();
														}
														if($cash_status  == 'Pending')
														{
															$this->db->where('ref_user_tracking_id',$get_userid);
															$this->db->where('report_update_id',$report_update_id);
															$this->db->where('transation_status','Pending');
															$all = $this->db->get('transation_details');				 	  	 
															$ref_trans_details = $all->row();

															//$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Pending'")->row();
														}
														 
													 										 
													 	$transation_amount = $ref_trans_details->transation_amount;

														foreach($return as $newreturn)
														{
															$referral_balance   = $newreturn->balance; 
															$user_referral_mail = $newreturn->referral_mail;
															$ref_user_email     = $newreturn->email;
															$ref_first_name   	= $newreturn->first_name;
															$ref_last_name 	  	= $newreturn->last_name;								

															if($ref_first_name == '' && $ref_last_name == '')
															{
																$ex_name       = explode('@', $ref_user_email);
																$ref_user_name = $ex_name[0]; 
															}
															else
															{
																$ref_user_name = $ref_first_name.' '.$ref_last_name;
															}
														}

														if($cash_status  == 'Completed')
														{
															$bal_ref = $referral_balance - $transation_amount;
															$this->db->where('user_id',$ref_id);
															$this->db->update('tbl_users',array('balance'=>$bal_ref));
														}

														$data = array('transation_status'=>'Pending'); 
														$this->db->where('report_update_id',$report_update_id);
														$this->db->where('ref_user_tracking_id',$get_userid);
														$this->db->update('transation_details',$data);	
														 
														if($user_referral_mail == 1)
														{
															$this->db->where('mail_id',20);
															$mail_template = $this->db->get('tbl_mailtemplates');
															if($mail_template->num_rows >0) 
															{
																$fetch     = $mail_template->row();
																$subject   = $fetch->email_subject;
																$templete  = $fetch->email_template;
																$url 	   = base_url().'my_earnings/';
																$unsuburls = base_url().'un-subscribe/referral/'.$ref_id;
														   		$myaccount = base_url().'minha-conta';
																
																$this->load->library('email');

																$config    = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
																);
																		
																$sub_data = array(
																'###SITENAME###'=>$site_name
																);
																
																$subject_new = strtr($subject,$sub_data);
																// $this->email->initialize($config);
																$this->email->set_newline("\r\n");
																$this->email->initialize($config);
																$this->email->from($admin_email,$site_name.'!');
																$this->email->to($ref_user_email);
																$this->email->subject($subject_new);

																//echo $transation_amount; exit;											
																$datas = array(
																'###NAME###'=>$ref_user_name,
																'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
																'###SITENAME###'=>$site_name,
																'###ADMINNO###'=>$admin_no,
																'###DATE###'=>date('y-m-d'),
																'###AMOUNT###'=>str_replace('.',',',bcdiv($transation_amount,1,2)),
																'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
																);
																$content_pop=strtr($templete,$datas);
																$this->email->message($content_pop);
																$this->email->send();  
															}
														}
													}
												}
												//End 4-5-17
											//}
										}*/	
										//End 25-7-17
									}
								}
							}	
						}	
					} 
					else
					{
 
						$all=0;
						$transaction_id    = $content['saleItems']['saleItem']['@attributes']['id'];
						$report_update_id  = $content['saleItems']['saleItem']['@attributes']['id'];

 						$newtransaction_id = $content['saleItems']['saleItem']['@attributes']['id'];
						$cash_status 	   = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('status');

						if($newtransaction_id == '')
						{
							$newid             = rand(1000,9999);
							$newtransaction_id = md5($newid);
						}

						$this->db->where('report_update_id',$report_update_id);
						$all   = $this->db->get('cashback');
						$fetch = $all->row();			 	  	 
						
						if(!empty($fetch))
						{
							$get_userid 		= $fetch->user_id;
							$transaction_amount = $fetch->cashback_amount;
							$duplicate = 1;
							
							/*
							$user_id      = $content['saleItems']['saleItem']['gpps']['gpp'][0]; 
							$val_user_id  = substr($user_id, 0, 5); 
							if($val_user_id == 'P0001')
							{$get_userid = decode_userid($user_id); }else{ $get_userid = $user_id;}
							*/   
							/*
							$currency_type    = $content['saleItems']['saleItem']['currency'];
							if($currency_type != 'BRL')
							{
								$amount   			= $content['saleItems']['saleItem']['amount'];
								$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
							}	
							else
							{
								$transaction_amount = $content['saleItems']['saleItem']['amount'];	
							}
							*/

							$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$get_userid))->row('balance');
							if($prev_userbal == '')
							{
								$prev_userbal = 0;	
							}

							$status = $content['saleItems']['saleItem']['reviewState'];

							if($status == 'open')
							{
								$tstatus = 'Pending';
								$cstatus = 'Pending';
							}
							
							if($status == 'approved')
							{
								if($site_status == 'all_confirm' || $site_status == 'approve_confirm')
								{
									$tstatus = 'Approved';
									$cstatus = 'Completed';
								}
								if($site_status == 'all_pending' || $site_status == 'approve_pending')
								{
									$tstatus = 'Pending';
									$cstatus = 'Pending';
								}
							}

							if($status == 'rejected')
							{
								$tstatus = 'Canceled';
								$cstatus = 'Canceled';
							}
							if($status == 'confirmed')
							{
								$tstatus = 'Credited';
								$cstatus = 'Completed';
							}							
							
							if($status !='')
							{
								/*if($tstatus == 'Approved' || $tstatus = 'Credited')
								{
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");
								}

								if($tstatus == 'Canceled')
								{
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Canceled')");
								}*/

								/*New code for mail notification details 1-3-17*/
								$userdetails 	 = $this->db->query("SELECT * from tbl_users where user_id=$get_userid")->row();
								$cashback_status = $userdetails->cashback_mail;
								$user_email      = $userdetails->email;
								$first_name      = $userdetails->first_name;
								$last_name       = $userdetails->last_name;
								$balance  		 = $userdetails->balance;
								
								if($balance == '')
								{	
									$balance == '0';
								}

								if($first_name == '' && $last_name == '')
								{
									$ex_name   = explode('@', $user_email);
									$user_name = $ex_name[0]; 
								}
								else
								{
									$user_name = $first_name.''.$last_name;
								}

								$this->db->where('admin_id',1);
								$admin_det = $this->db->get('admin');
								if($admin_det->num_rows >0) 
								{    
									$admin 		 = $admin_det->row();
									$admin_email = $admin->admin_email;
									$site_name 	 = $admin->site_name;
									$admin_no	 = $admin->contact_number;
									$site_logo 	 = $admin->site_logo;
								}

								$date = date('Y-m-d');
								/*end 1-3-17*/

								 
								if($cstatus == 'Completed')
								{
									if($cash_status != 'Completed')
									{
										$data = array('status' => $cstatus); 
										$this->db->where('report_update_id',$report_update_id);
										$updation = $this->db->update('cashback',$data);


										$this->db->where('user_id',$get_userid);
										$this->db->update('tbl_users',array('balance'=>$prev_userbal+$transaction_amount));
										
										/*mail for Completed cashback 1-3-17*/
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',8);
											$mail_template = $this->db->get('tbl_mailtemplates');
											if($mail_template->num_rows >0) 
											{
											   	$fetch     = $mail_template->row();
											   	$subject   = $fetch->email_subject;
											   	$templete  = $fetch->email_template;
											   	$url 	  = base_url().'my_earnings/';
											   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
											   	$myaccount = base_url().'minha_conta';
											   
											  	$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);

												$subject_new = strtr($subject,$sub_data);
												
												//$this->email->initialize($config);
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email, $site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

											    );
											   
											   $content_pop=strtr($templete,$data);
											   $this->email->message($content_pop);
											   $this->email->send();  
											}
										} 
										/*end 1-3-17*/

										/*New code for referral mail and user cashback balance details 04-5-17*/
										$this->db->where('report_update_id',$report_update_id);
										$cashbacks 	   = $this->db->get('cashback');
										$cashback_data = $cashbacks->row();

										if($cashback_data->referral!=0)
										{
											$this->db->where('trans_id',$cashback_data->txn_id);
											$txn = $this->db->get('transation_details');
											$txn_detail = $txn->row();

											if($txn_detail)
											{
												$txn_id 	 	    = $txn_detail->trans_id;
												$ref_user_id 	    = $txn_detail->user_id;
												$transation_amounts = $txn_detail->transation_amount;
												$refer_user 	    = $this->view_user($ref_user_id);
												
												if($refer_user)
												{
													foreach($refer_user as $single)
													{
														$referral_balance = $single->balance;
														$user_email 	  = $single->email;
														$first_name 	  = $single->first_name;
														$last_name 		  = $single->last_name;

														if($first_name == '' && $last_name == '')
														{
															$ex_name   = explode('@', $user_email);
															$user_name = $ex_name[0]; 
														}
														else
														{
															$user_name = $first_name.' '.$last_name;
														}
													}

													//Update refer user balance details in User table and transaction table(approve referral cashback amount)//
													$this->db->where('user_id',$ref_user_id);
													$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amounts));
													 

													$data = array('transation_status'=>'Approved','transation_reason'=>'Referal Payment'); //'transaction_date'=>$date,
													//$this->db->where('transation_reason','Pending Referal Payment');
													$this->db->where('trans_id',$cashback_data->txn_id);
													$this->db->update('transation_details',$data);

													/* mail for Approve cashback amt mail notifications */
													if($single->referral_mail == 1)
													{	
														$this->db->where('mail_id',9);
														$mail_template = $this->db->get('tbl_mailtemplates');
														if($mail_template->num_rows >0) 
														{
														   $fetch 	  = $mail_template->row();
														   $subject   = $fetch->email_subject;
														   $templete  = $fetch->email_template;
														   $url 	  = base_url().'my_earnings/';
														   $unsuburls = base_url().'un-subscribe/referral/'.$ref_user_id;
											   			   $myaccount = base_url().'minha_conta';
														   
															$this->load->library('email');
															
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);
															
															$sub_data = array(
																'###SITENAME###'=>$site_name
															);
															$subject_new = strtr($subject,$sub_data);
															
															// $this->email->initialize($config);
															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_email,$site_name.'!');
															$this->email->to($user_email);
															$this->email->subject($subject_new);
														   
															$datas = array(
																'###NAME###'=>$user_name,
																'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
																'###SITENAME###'=>$site_name,
																'###ADMINNO###'=>$admin_no,
																'###DATE###'=>$date,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amounts,1,2)),
																'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
															);
														   
														   $content_pop=strtr($templete,$datas);
														   $this->email->message($content_pop);
														   $this->email->send();  
														}
													}	
													/* Mail for Approve referral cashback amount mail End*/
												}
											}
										}
										/*end 4-5-17*/

										/*first and second Withdraw mail notification for referral users 4-5-17*/
										$this->db->where('admin_id',1);
										$Admin_Details_Query    = $this->db->get('admin');
										$Admin_Details 		    = $Admin_Details_Query->row();
										$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
										$remain_minimum_amt     = $Admin_Details->remain_minimum_with_amt;
										$Site_Logo 				= $Admin_Details->site_logo;
										$admin_emailid 			= $Admin_Details->admin_email;

										if($ref_user_id!='')
										{

											$this->db->where('report_update_id',$report_update_id);
											$cashbacks 	   = $this->db->get('cashback');
											$cashback_data = $cashbacks->row();

											if($cashback_data->referral!=0)
											{
												$this->db->where('new_txn_id',$cashback_data->new_txn_id);
												$txn = $this->db->get('transation_details');
												$txn_detail = $txn->row();
												
												if($txn_detail)
												{
													$new_txn_ids = $txn_detail->new_txn_id;
											
													if($single->referral_category_type != '')
													{ 	
														$referrals    	    = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
														$ref_by_percentage  = $referrals->ref_by_percentage;
														$ref_by_rate 		= $referrals->ref_by_rate;
														$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

														//3** Bonus by Refferal Rate type//
														if($bonus_by_ref_rate == 1)
														{
															$n9  	 = '333445';
															$n12 	 = $n9 + $ref_user_id;
															$now 	 = date('Y-m-d H:i:s');	
															$selqry  = "SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Referal Payment' AND user_id=$ref_user_id"; 
													 		$query   = $this->db->query("$selqry");
															$numrows = $query->num_rows();
															if($numrows > 0)
															{
																$fetch 		   = $query->row();
																$usercount 	   = $fetch->userid;
																$referrals     = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
																$bonus_amount  = $referrals->ref_cashback_rate_bonus;
																$friends_count = $referrals->friends_count;
																
																if($usercount == $friends_count)
																{	
																	if($bonus_amount!='')
																	{	
																		if($single->referral_category_type == 1)
																		{ 
																		 	$types = 'One';
																		}
																		if($single->referral_category_type == 2)
																		{ 
																		 	$types = 'Two';
																		}
																		if($single->referral_category_type == 3)
																		{ 
																		 	$types = 'Three';
																		}
																		if($single->referral_category_type == 4)
																		{ 
																		 	$types = 'Four';
																		}
																		if($single->referral_category_type == 5)
																		{ 
																		 	$types = 'Five';
																		}
																		if($single->referral_category_type == 6)
																		{ 
																		 	$types = 'Six';
																		}
																		if($single->referral_category_type == 7)
																		{ 
																		 	$types = 'Seven';
																		}
																		if($single->referral_category_type == 8)
																		{ 
																		 	$types = 'Eight';
																		}
																		if($single->referral_category_type == 9)
																		{ 
																		 	$types = 'Nine';
																		}
																		if($single->referral_category_type == 10)
																		{ 
																		 	$types = 'Ten';
																		}

																		$selqry	= "SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category $types User' AND `user_id`=$ref_user_id"; 
														 				$query1	= $this->db->query("$selqry");
														 				$newnumrows = $query1->num_rows();
																		if($newnumrows > 0)
																		{
																			$fetch 		 = $query1->row();
																			$users_count = $fetch->userid;
																			if($users_count == 0)	
																			{	
																				$data = array(			
																				'transation_amount' => $bonus_amount,	
																				'user_id' => $ref_user_id,	
																				'transation_date' => $now,
																				'transaction_date' => $now,
																				'transation_id'=>$n12,	
																				'transation_reason' => 'Referral Bonus for Category ' .$types. ' User',	
																				'mode' => 'Credited',
																				'details_id'=>'',	
																				'table'=>'',	
																				'new_txn_id'=>0,
																				'transation_status ' => 'Approved',
																				'report_update_id'=>$newtransaction_id
																				);	
																				$this->db->insert('transation_details',$data);
																			}	
																		}
																	}	
																}
															} 
														}
														//3** Bonus by Refferal Rate type End//

														//1** Refferal by Percentage type Start//
														if($ref_by_percentage == 1)
														{
															$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); /*,'transaction_date'=>$date*/
															$this->db->where('new_txn_id',$new_txn_ids);
															$this->db->update('transation_details',$data);
														}
														//1** Refferal by Percentage type End//	
													}	
												}
											}

											/*New code for withdraw notification 1-4-17*/
											$ref_prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$ref_user_id))->row('balance');
											if($ref_prev_userbal == '')
											{
												$ref_prev_userbal = 0;	
											}


											//$ref_Total_Amount = ($ref_balance+$ref_cashback_amount);
											$ref_Total_Amount 	= ($referral_balance+$transation_amounts);
											$ref_User_details 	= $this->view_user($ref_user_id);
											$ref_us_email 	  	= $ref_User_details[0]->email;
											$ref_with_status  	= $ref_User_details[0]->withdraw_mail;
											$ref_unsuburl	 	= base_url().'un-subscribe/withdraw/'.$ref_user_id;
											$ref_myaccount    	= base_url().'resgate';
											$ref_firstname 	  	= $ref_User_details[0]->first_name;
											$ref_lastname  	  	= $ref_User_details[0]->last_name;
											$with_mail_status 	= $ref_User_details[0]->first_withdraw_mail_status;
											$first_with_status  = $ref_User_details[0]->first_withdraw_status;
											$second_with_status = $ref_User_details[0]->second_withdraw_status;

											if($with_mail_status == 1)
											{
												$Admin_Minimum_Cashback = $remain_minimum_amt;
												$new_withdraw_status    = $second_with_status;
											}
											else
											{
												$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;
												$new_withdraw_status    = $first_with_status;	
											}

											if($ref_firstname == '' && $ref_lastname == '')
											{
												$ex_name  	  = explode('@', $ref_User_details[0]->email);
												$ref_username = $ex_name[0]; 
											}
											else
											{
												$ref_username = $ref_firstname.' '.$ref_lastname;
											}	
											/*End 9-1-17*/
											 
											if($ref_with_status == 1)
											{
												if($new_withdraw_status == 0)
												{
													if($ref_Total_Amount>=$Admin_Minimum_Cashback)
													{
														$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
														if($obj_temp->num_rows>0)
														{
															$mail_temp  = $obj_temp->row(); 
															$fe_cont    = $mail_temp->email_template;	
															$subject  	= $mail_temp->email_subject;	
															$servername = base_url();
															$nows 		= date('Y-m-d');	
															$this->load->library('email');
															$gd_api=array(
																
																'###NAME###'=>$ref_username,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($ref_Total_Amount,1,2)),
																'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
																'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
																'###ULINK###'=>'<a href='.$ref_unsuburl.'>'.$ref_unsuburl.'</a>',
																'###MYACLINK###'=>'<a href='.$ref_myaccount.'>'.$ref_myaccount.'</a>'
																);
																			   
															$gd_message=strtr($fe_cont,$gd_api);
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);

															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_emailid,$site_name.'!');
															$this->email->to($ref_us_email);
															$this->email->subject($subject);
															$this->email->message($gd_message);
															$this->email->send();
															$this->email->print_debugger();

															/*new code for update a first or second withdraw amount status 22-6-17*/
															if($new_withdraw_status == 0)
															{
																if($with_mail_status == 1)
																{
																	$data = array(		
																	'second_withdraw_status'  => 1);
																	$this->db->where('user_id',$ref_user_id);
																	$update_qry= $this->db->update('tbl_users',$data);
																}
																else
																{
																	$data = array(		
																	'first_withdraw_status'  => 1);
																	$this->db->where('user_id',$ref_user_id);
																	$update_qry= $this->db->update('tbl_users',$data);
																}	
															}
															/*End*/
														}
													}
												}	
											}
											 
											/*end 1-4-17*/		
										}
										/*End mail notification for referral users 4-5-17*/

										/*first and second withdraw mail notification for refered user 4-5-17*/
										$Total_Amount 	  	= ($balance+$transaction_amount);
										$User_details 	 	= $this->view_user($get_userid);
										$us_email 		  	= $User_details[0]->email;
										$with_status      	= $User_details[0]->withdraw_mail;
										$firstname 		  	= $User_details[0]->first_name;
										$lastname  		 	= $User_details[0]->last_name;
										$with_mail_status 	= $User_details[0]->first_withdraw_mail_status;

										$unsuburl	 	  	= base_url().'un-subscribe/withdraw/'.$get_userid;
										$myaccount    	  	= base_url().'resgate';
										$first_with_status  = $User_details[0]->first_withdraw_status;
										$second_with_status = $User_details[0]->second_withdraw_status;

										if($with_mail_status == 1)
										{
											$Admin_Minimum_Cashback = $remain_minimum_amt;
											$new_withdraw_status    = $second_with_status;
										}
										else
										{
											$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;
											$new_withdraw_status    = $first_with_status;	
										}

										if($firstname == '' && $lastname == '')
										{
											$ex_name  = explode('@', $User_details[0]->email);
											$username = $ex_name[0]; 
										}
										else
										{
											$username = $firstname.' '.$lastname;
										}	
										/*End 9-1-17*/

										if($with_status == 1)
										{
											if($new_withdraw_status == 0)
											{
												if($Total_Amount>=$Admin_Minimum_Cashback)
												{
													$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
													if($obj_temp->num_rows>0)
													{
														$mail_temp  = $obj_temp->row(); 
														$fe_cont    = $mail_temp->email_template;	
														$subject  	= $mail_temp->email_subject;		
														$servername = base_url();
														$nows 		= date('Y-m-d');	
														$this->load->library('email');
														$gd_api=array(
															
															'###NAME###'=>$username,
															'###AMOUNT###'=>str_replace('.', ',', bcdiv($Total_Amount,1,2)),
															'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
															'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
															'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
															'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
															);
																		   
														$gd_message=strtr($fe_cont,$gd_api);
														$config = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
														);
														 
														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_emailid,$site_name.'!');
														$this->email->to($us_email);
														$this->email->subject($subject);
														$this->email->message($gd_message);
														$this->email->send();
														$this->email->print_debugger();

														/*new code for update a first or second withdraw amount status 23-6-17*/
														if($new_withdraw_status == 0)
														{
															if($with_mail_status == 1)
															{
																$data = array(		
																'second_withdraw_status'  => 1);
																$this->db->where('user_id',$get_userid);
																$update_qry= $this->db->update('tbl_users',$data);
															}
															else
															{
																$data = array(		
																'first_withdraw_status'  => 1);
																$this->db->where('user_id',$get_userid);
																$update_qry= $this->db->update('tbl_users',$data);
															}	
														}
														/*End*/
													}
												}
											}	
										}	
										 	
										/*End 9-1-17*/
										/*End mail notification for referred user 4-5-17*/
									}		
								}

								if($cstatus == 'Canceled')
								{
									if($cash_status != 'Canceled')
									{
										$data = array('status' => $cstatus); 
										$this->db->where('report_update_id',$report_update_id);
										$updation = $this->db->update('cashback',$data);

										/*mail for Canceled cashback 1-3-17*/
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',11);
											$mail_template = $this->db->get('tbl_mailtemplates');
											if($mail_template->num_rows >0) 
											{

											   	$fetch 	   = $mail_template->row();
											   	$subject   = $fetch->email_subject;
											   	$templete  = $fetch->email_template;
											   	$url 	   = base_url().'cashback/my_earnings/';
											   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
											   	$myaccount = base_url().'minha_conta';
											   
												$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);
												$subject_new = strtr($subject,$sub_data);
												
												//$this->email->initialize($config);
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email,$site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', $transaction_amount),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
											    );
											   
											   $content_pop=strtr($templete,$data);
											   //echo print_r($content_pop); exit;
											   // echo $content_pop; echo $subject_new;
											   $this->email->message($content_pop);
											   $this->email->send();  
											}
										} 
										/*end 1-3-17*/ 

										/*New code for update user table 4-5-17*/
										if($cash_status =='Completed' && $cstatus == 'Canceled')
										{
											$user_bale 		= $balance;
											$newbalnce 		= $user_bale - $transaction_amount;
												
											$data = array(		
											'balance' => $newbalnce);
											$this->db->where('user_id',$get_userid);
											$update_qry = $this->db->update('tbl_users',$data);

											/*new code 26-7-17*/
											$this->db->delete('transation_details',array('report_update_id' => $report_update_id,'transation_reason'=>'Cashback','user_id'=>$get_userid));
											/*End 26-7-17*/
										}
										/*End 4-5-17*/

										/*Mail for cancelled referral amount for reffered user mail 04-5-17*/
										$this->db->where('report_update_id',$report_update_id);
										$cashbacks 	   = $this->db->get('cashback');
										$cashback_data = $cashbacks->row();
										if($cashback_data->referral!=0)
										{
											$this->db->where('trans_id',$cashback_data->txn_id);
											$txn 		= $this->db->get('transation_details');
											$txn_detail = $txn->row();
											
											if(!empty($txn_detail))
											{
												$txn_id 	 	   = $txn_detail->trans_id;
												$ref_user_id 	   = $txn_detail->user_id;
												$transation_amount = $txn_detail->transation_amount;
												$newtrans_status   = $txn_detail->transation_status; 
												$refer_user 	   = $this->view_user($ref_user_id);

												if($refer_user)
												{
													foreach($refer_user as $single)
													{
														$referral_balance = $single->balance;
														$user_email 	  = $single->email;
														$first_name 	  = $single->first_name;
														$last_name 		  = $single->last_name;
														$reffer_status    = $single->referral_mail;

														if($first_name == '' && $last_name == '')
														{
															$ex_name   = explode('@', $user_email);
															$user_name = $ex_name[0]; 
														}
														else
														{
															$user_name = $first_name.' '.$last_name;
														}
													}

													if($cstatus == 'Canceled')
													{
														if($newtrans_status == 'Approved')
														{
															$new_balance = ($referral_balance - $transation_amount);
															$this->db->where('user_id',$ref_user_id);
															$this->db->update('tbl_users',array('balance'=>$new_balance));
														}
														/*new code 11-7-17*/
														$this->db->delete('transation_details',array('report_update_id' => $txn_detail->report_update_id,'transation_reason'=>'Cashback'));
														/*End 11-7-17*/
													}

													$data = array(	
													'transation_status'=>$cstatus
													);
													$this->db->where('trans_id',$cashback_data->txn_id);
													//$this->db->where('report_update_id',$report_update_id);
													$updation = $this->db->update('transation_details',$data);
														
													$unsuburl  = base_url().'un-subscribe/referral/'.$ref_user_id;
												    $myaccount = base_url().'minha-conta';

													$this->db->where('admin_id',1);
													$admin_det = $this->db->get('admin');
													if($admin_det->num_rows >0) 
													{    
														$admin 		 = $admin_det->row();
														$admin_email = $admin->admin_email;
														$site_name 	 = $admin->site_name;
														$admin_no 	 = $admin->contact_number;
														$site_logo 	 = $admin->site_logo;
													}

													if($reffer_status == 1)
													{
														$this->db->where('mail_id',19);
														$mail_template = $this->db->get('tbl_mailtemplates');
														if($mail_template->num_rows >0) 
														{
														   $fetch = $mail_template->row();
														   $subject = $fetch->email_subject;
														   $templete = $fetch->email_template;
															
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);
															
															// $this->email->initialize($config);
															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_email,$site_name.'!');
															$this->email->to($user_email);
															$this->email->subject($subject);
														   
															$data = array(
																'###ADMINNO###'=>$admin_no,
																'###EMAIL###'=>$user_name,
																'###DATE###'=>$date,
																'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
																'###SITENAME###' =>$site_name,
																'###STATUS###'=>$status,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amount,1,2)),
																'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
														    );
														   
														   $content_pop=strtr($templete,$data);
														   $this->email->message($content_pop);
														   $this->email->send();  
														}
													}
												}
												/*end*/
											}	
										}
										/*End 4-5-17*/
									}	
								}
								if($cstatus == 'Pending')
								{
									/*if($cash_status != 'Pending')
									{
										$data = array('status' => $cstatus); 
										$this->db->where('report_update_id',$report_update_id);
										$updation = $this->db->update('cashback',$data);

										//New code for update user balance details 4-5-17
										if($cash_status =='Completed') 
										{	
											$user_bale 		= $balance;
											$newbalnce 		= $user_bale - $transaction_amount;
												
											$data = array(		
											'balance' => $newbalnce);
											$this->db->where('user_id',$get_userid);
											$update_qry = $this->db->update('tbl_users',$data);
										}
										//end 4-5-17

										//mail for Pending cashback 1-3-17
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',10);
											$mail_template = $this->db->get('tbl_mailtemplates');
											
											if($mail_template->num_rows >0) 
											{
											    $fetch     = $mail_template->row();
											    $subject   = $fetch->email_subject;
											    $templete  = $fetch->email_template;
											    //$url     = base_url().'cashback/my_earnings/';
											    $unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
											    $myaccount = base_url().'minha_conta';
											   
												$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);
												$subject_new = strtr($subject,$sub_data);
												
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email,$site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
											    );
											   
											    $content_pop=strtr($templete,$data);
											   	$this->email->message($content_pop);
											   	$this->email->send();  
											}
										} 
										//end 1-3-17//

										//New code for pending referral mail for (REFER) User 4-5-17//
										$check_ref    = $this->check_ref_user($get_userid);

										if($check_ref > 0)		
										{
											$ref_id  	  = $check_ref;
											$ref_user_bal = $this->view_balance($check_ref);
											$return  	  = $this->check_active_user($ref_id);

											if($return)
											{
												if($cash_status  == 'Completed')
												{	
													$this->db->where('ref_user_tracking_id',$get_userid);
													$this->db->where('report_update_id',$report_update_id);
													$this->db->where('transation_status','Paid');
													$this->db->or_where('transation_status','Approved');
													$all = $this->db->get('transation_details');				 	  	 
													$ref_trans_details = $all->row();
													//$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Approved'")->row();	
												
												}
												if($cash_status  == 'Canceled')
												{
													$this->db->where('ref_user_tracking_id',$get_userid);
													$this->db->where('report_update_id',$report_update_id);
													$this->db->where('transation_status','Canceled');
													$all = $this->db->get('transation_details');				 	  	 
													$ref_trans_details = $all->row();
													//$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Canceled'")->row();
												}
												if($cash_status  == 'Pending')
												{
													$this->db->where('ref_user_tracking_id',$get_userid);
													$this->db->where('report_update_id',$report_update_id);
													$this->db->where('transation_status','Pending');
													$all = $this->db->get('transation_details');				 	  	 
													$ref_trans_details = $all->row();

													//$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Pending'")->row();
												}
												 
											 										 
											 	$transation_amount = $ref_trans_details->transation_amount;

												foreach($return as $newreturn)
												{
													$referral_balance   = $newreturn->balance; 
													$user_referral_mail = $newreturn->referral_mail;
													$ref_user_email     = $newreturn->email;
													$ref_first_name   	= $newreturn->first_name;
													$ref_last_name 	  	= $newreturn->last_name;								

													if($ref_first_name == '' && $ref_last_name == '')
													{
														$ex_name       = explode('@', $ref_user_email);
														$ref_user_name = $ex_name[0]; 
													}
													else
													{
														$ref_user_name = $ref_first_name.' '.$ref_last_name;
													}
												}

												if($cash_status  == 'Completed')
												{
													$bal_ref = $referral_balance - $transation_amount;
													$this->db->where('user_id',$ref_id);
													$this->db->update('tbl_users',array('balance'=>$bal_ref));
												}

												$data = array('transation_status'=>'Pending'); 
												$this->db->where('report_update_id',$report_update_id);
												$this->db->where('ref_user_tracking_id',$get_userid);
												$this->db->update('transation_details',$data);	
												 
												if($user_referral_mail == 1)
												{
													$this->db->where('mail_id',20);
													$mail_template = $this->db->get('tbl_mailtemplates');
													if($mail_template->num_rows >0) 
													{
														$fetch     = $mail_template->row();
														$subject   = $fetch->email_subject;
														$templete  = $fetch->email_template;
														$url 	   = base_url().'my_earnings/';
														$unsuburls = base_url().'un-subscribe/referral/'.$ref_id;
												   		$myaccount = base_url().'minha-conta';
														
														$this->load->library('email');

														$config    = Array(
														'mailtype'  => 'html',
														'charset'   => 'utf-8',
														);
																
														$sub_data = array(
														'###SITENAME###'=>$site_name
														);
														
														$subject_new = strtr($subject,$sub_data);
														// $this->email->initialize($config);
														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_email,$site_name.'!');
														$this->email->to($ref_user_email);
														$this->email->subject($subject_new);

														//echo $transation_amount; exit;											
														$datas = array(
														'###NAME###'=>$ref_user_name,
														'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
														'###SITENAME###'=>$site_name,
														'###ADMINNO###'=>$admin_no,
														'###DATE###'=>date('y-m-d'),
														'###AMOUNT###'=>str_replace('.',',',bcdiv($transation_amount,1,2)),
														'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
														'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
														);
														$content_pop=strtr($templete,$datas);
														$this->email->message($content_pop);
														$this->email->send();  
													}
												}
											}
										}
										//End 4-5-17//
									}*/	
								}	
							}
						}
					}
				}		  
			}
			

			//Completed
			if($affiliate_name == 'lomadee')
			{
				//echo "<pre>";print_r($content); exit;
				if($content['item']!='')
				{
					if(is_array($content['item'][0]))
					{	
						foreach($content['item'] as $cont)
						{
							$all=0;
							$transaction_id   = $cont['transactionCode']; //transactionId
							$this->db->where('report_update_id',$transaction_id);
							$all = $this->db->get('cashback');				 	  	 
							$fetch = $all->row(); 

							if(!empty($fetch))
							{

								$duplicate  = 1;
								$get_userid 		= $fetch->user_id;
								$transaction_amount = $fetch->cashback_amount;

								$coupon_id          = $cont['advertiser']['advertiserName'];
								$cashback_amount    = $cont['commission'];
								$old_trans_date     = $cont['checkoutDate'];
								$transaction_date   = preg_replace('/\//', '-',$old_trans_date);
								$status 			= $cont['statusName']; 
								$status_id 			= $cont['statusId'];
								 
								if($transaction_id !='')
								{
									$report_update_id   = $transaction_id;
								}else
								{
									$report_update_id   = rand(1000,9999);	
								}

								/*new code 4-5-17*/
							 	$newtransaction_id = $cont['transactionCode'];
								$cash_status 	   = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('status');
								if($newtransaction_id == '')
								{
									$newid             = rand(1000,9999);
									$newtransaction_id = md5($newid);
								}
							 	/*End 4-5-17*/

								$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$get_userid))->row('balance');
								if($prev_userbal == '')
								{
									$prev_userbal = 0;	
								}

								/*New code for mail notification details 2-3-17*/
								$userdetails 	 = $this->db->query("SELECT * from tbl_users where user_id=$get_userid")->row();
								$cashback_status = $userdetails->cashback_mail;
								$user_email      = $userdetails->email;
								$first_name      = $userdetails->first_name;
								$last_name       = $userdetails->last_name;
								$balance  		 = $userdetails->balance;
								if($balance == '')
								{	
									$balance == '0';
								}
								if($first_name == '' && $last_name == '')
								{
									$ex_name   = explode('@', $user_email);
									$user_name = $ex_name[0]; 
								}
								else
								{
									$user_name = $first_name.''.$last_name;
								}

								$this->db->where('admin_id',1);
								$admin_det = $this->db->get('admin');
								if($admin_det->num_rows >0) 
								{    
									$admin 		 = $admin_det->row();
									$admin_email = $admin->admin_email;
									$site_name 	 = $admin->site_name;
									$admin_no	 = $admin->contact_number;
									$site_logo 	 = $admin->site_logo;
								}
								$date = date('Y-m-d');
							 
								if($status !='')
								{
									/*if($status == 'CONFIRMED' && $status_id == 1)
									{
										$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
										VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");
									}
									else if($status == 'CANCELED' && $status_id == 2)
									{
										$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
										VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Canceled')");
									}
									else 
									{
										$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
										VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Pending')");
									}*/

									if($status == 'CONFIRMED')
									{
										$cstatus = 'Completed';
									}
									if($status == 'CANCELED')
									{	
										$cstatus = 'Canceled';
									}
									if($status == 'PENDING')
									{	
										$cstatus = 'Pending';
									}
									
										

									if($cstatus == 'Completed')
									{
										if($cash_status != 'Completed')
										{
											$data = array('status' => $cstatus); 
											$this->db->where('report_update_id',$report_update_id);
											$updation = $this->db->update('cashback',$data);

											$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
											VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");

											$this->db->where('user_id',$get_userid);
											$this->db->update('tbl_users',array('balance'=>$prev_userbal+$transaction_amount));
											

											/*mail for Completed cashback 2-3-17*/
											if($cashback_status == 1)
											{
												$this->db->where('mail_id',8);
												$mail_template = $this->db->get('tbl_mailtemplates');
												if($mail_template->num_rows >0) 
												{
												   	$fetch     = $mail_template->row();
												   	$subject   = $fetch->email_subject;
												   	$templete  = $fetch->email_template;
												   	$url 	  = base_url().'my_earnings/';
												   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
												   	$myaccount = base_url().'minha_conta';
												   
												  	$this->load->library('email');
													
													$config = Array(
														'mailtype'  => 'html',
														'charset'   => 'utf-8',
													);
													
													$sub_data = array(
														'###SITENAME###'=>$site_name
													);

													$subject_new = strtr($subject,$sub_data);
													
													//$this->email->initialize($config);
													$this->email->set_newline("\r\n");
													$this->email->initialize($config);
													$this->email->from($admin_email, $site_name.'!');
													$this->email->to($user_email);
													$this->email->subject($subject_new);
												   
													$data = array(
														'###NAME###'=>$user_name,
														'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
														'###SITENAME###'=>$site_name,
														'###ADMINNO###'=>$admin_no,
														'###DATE###'=>$date,
														'###AMOUNT###'=>str_replace('.', ',', $transaction_amount),
														'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
														'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

												    );
												   
												   $content_pop=strtr($templete,$data);
												   $this->email->message($content_pop);
												   $this->email->send();  
												}
											} 
											/*end 2-3-17*/

											/*New code for referral mail and user cashback balance details 04-5-17*/
											$this->db->where('report_update_id',$report_update_id);
											$cashbacks 	   = $this->db->get('cashback');
											$cashback_data = $cashbacks->row();

											if($cashback_data->referral!=0)
											{
												$this->db->where('trans_id',$cashback_data->txn_id);
												$txn = $this->db->get('transation_details');
												$txn_detail = $txn->row();

												if($txn_detail)
												{
													$txn_id 	 	    = $txn_detail->trans_id;
													$ref_user_id 	    = $txn_detail->user_id;
													$transation_amounts = $txn_detail->transation_amount;
													$refer_user 	    = $this->view_user($ref_user_id);
													
													if($refer_user)
													{
														foreach($refer_user as $single)
														{
															$referral_balance = $single->balance;
															$user_email 	  = $single->email;
															$first_name 	  = $single->first_name;
															$last_name 		  = $single->last_name;

															if($first_name == '' && $last_name == '')
															{
																$ex_name   = explode('@', $user_email);
																$user_name = $ex_name[0]; 
															}
															else
															{
																$user_name = $first_name.' '.$last_name;
															}
														}

														//Update refer user balance details in User table and transaction table(approve referral cashback amount)//
														$this->db->where('user_id',$ref_user_id);
														$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amounts));
														 

														$data = array('transation_status'=>'Approved','transation_reason'=>'Referal Payment'); //'transaction_date'=>$date,
														//$this->db->where('transation_reason','Pending Referal Payment');
														$this->db->where('trans_id',$cashback_data->txn_id);
														$this->db->update('transation_details',$data);

														/* mail for Approve cashback amt mail notifications */
														if($single->referral_mail == 1)
														{	
															$this->db->where('mail_id',9);
															$mail_template = $this->db->get('tbl_mailtemplates');
															if($mail_template->num_rows >0) 
															{
															   $fetch 	  = $mail_template->row();
															   $subject   = $fetch->email_subject;
															   $templete  = $fetch->email_template;
															   $url 	  = base_url().'my_earnings/';
															   $unsuburls = base_url().'un-subscribe/referral/'.$ref_user_id;
												   			   $myaccount = base_url().'minha_conta';
															   
																$this->load->library('email');
																
																$config = Array(
																	'mailtype'  => 'html',
																	'charset'   => 'utf-8',
																);
																
																$sub_data = array(
																	'###SITENAME###'=>$site_name
																);
																$subject_new = strtr($subject,$sub_data);
																
																// $this->email->initialize($config);
																$this->email->set_newline("\r\n");
																$this->email->initialize($config);
																$this->email->from($admin_email,$site_name.'!');
																$this->email->to($user_email);
																$this->email->subject($subject_new);
															   
																$datas = array(
																	'###NAME###'=>$user_name,
																	'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
																	'###SITENAME###'=>$site_name,
																	'###ADMINNO###'=>$admin_no,
																	'###DATE###'=>$date,
																	'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amounts,1,2)),
																	'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
																	'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
																);
															   
															   $content_pop=strtr($templete,$datas);
															   $this->email->message($content_pop);
															   $this->email->send();  
															}
														}	
														/* Mail for Approve referral cashback amount mail End*/
													}
												}
											}
											/*end 4-5-17*/

											/*first and second Withdraw mail notification for referral users 4-5-17*/
											$this->db->where('admin_id',1);
											$Admin_Details_Query    = $this->db->get('admin');
											$Admin_Details 		    = $Admin_Details_Query->row();
											$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
											$remain_minimum_amt     = $Admin_Details->remain_minimum_with_amt;
											$Site_Logo 				= $Admin_Details->site_logo;
											$admin_emailid 			= $Admin_Details->admin_email;

											if($ref_user_id!='')
											{

												$this->db->where('report_update_id',$report_update_id);
												$cashbacks 	   = $this->db->get('cashback');
												$cashback_data = $cashbacks->row();

												if($cashback_data->referral!=0)
												{
													$this->db->where('trans_id',$cashback_data->txn_id);
													$txn = $this->db->get('transation_details');
													$txn_detail = $txn->row();
													
													if($txn_detail)
													{
														//$new_txn_ids = $txn_detail->new_txn_id;
														$new_txn_ids   = $txn_detail->trans_id;
												
														if($single->referral_category_type != '')
														{ 	
															$referrals    	    = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
															$ref_by_percentage  = $referrals->ref_by_percentage;
															$ref_by_rate 		= $referrals->ref_by_rate;
															$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

															//3** Bonus by Refferal Rate type//
															if($bonus_by_ref_rate == 1)
															{
																$n9  	 = '333445';
																$n12 	 = $n9 + $ref_user_id;
																$now 	 = date('Y-m-d H:i:s');	
																$selqry  = "SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Referal Payment' AND user_id=$ref_user_id"; 
														 		$query   = $this->db->query("$selqry");
																$numrows = $query->num_rows();
																if($numrows > 0)
																{
																	$fetch 		   = $query->row();
																	$usercount 	   = $fetch->userid;
																	$referrals     = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
																	$bonus_amount  = $referrals->ref_cashback_rate_bonus;
																	$friends_count = $referrals->friends_count;
																	
																	if($usercount == $friends_count)
																	{	
																		if($bonus_amount!='')
																		{	
																			if($single->referral_category_type == 1)
																			{ 
																			 	$types = 'One';
																			}
																			if($single->referral_category_type == 2)
																			{ 
																			 	$types = 'Two';
																			}
																			if($single->referral_category_type == 3)
																			{ 
																			 	$types = 'Three';
																			}
																			if($single->referral_category_type == 4)
																			{ 
																			 	$types = 'Four';
																			}
																			if($single->referral_category_type == 5)
																			{ 
																			 	$types = 'Five';
																			}
																			if($single->referral_category_type == 6)
																			{ 
																			 	$types = 'Six';
																			}
																			if($single->referral_category_type == 7)
																			{ 
																			 	$types = 'Seven';
																			}
																			if($single->referral_category_type == 8)
																			{ 
																			 	$types = 'Eight';
																			}
																			if($single->referral_category_type == 9)
																			{ 
																			 	$types = 'Nine';
																			}
																			if($single->referral_category_type == 10)
																			{ 
																			 	$types = 'Ten';
																			}

																			$selqry	= "SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category $types User' AND `user_id`=$ref_user_id"; 
															 				$query1	= $this->db->query("$selqry");
															 				$newnumrows = $query1->num_rows();
																			if($newnumrows > 0)
																			{
																				$fetch 		 = $query1->row();
																				$users_count = $fetch->userid;
																				if($users_count == 0)	
																				{	
																					$data = array(			
																					'transation_amount' => $bonus_amount,	
																					'user_id' => $ref_user_id,	
																					'transation_date' => $now,
																					'transaction_date' => $now,
																					'transation_id'=>$n12,	
																					'transation_reason' => 'Referral Bonus for Category ' .$types. ' User',	
																					'mode' => 'Credited',
																					'details_id'=>'',	
																					'table'=>'',	
																					'new_txn_id'=>0,
																					'transation_status ' => 'Approved',
																					'report_update_id'=>$newtransaction_id
																					);	
																					$this->db->insert('transation_details',$data);
																				}	
																			}
																		}	
																	}
																} 
															}
															//3** Bonus by Refferal Rate type End//

															//1** Refferal by Percentage type Start//
															if($ref_by_percentage == 1)
															{
																$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); /*,'transaction_date'=>$date*/
																$this->db->where('trans_id',$new_txn_ids);
																$this->db->update('transation_details',$data);
															}
															//1** Refferal by Percentage type End//	
														}	
													}
												}

												/*New code for withdraw notification 1-4-17*/
												$ref_prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$ref_user_id))->row('balance');
												if($ref_prev_userbal == '')
												{
													$ref_prev_userbal = 0;	
												}


												//$ref_Total_Amount = ($ref_balance+$ref_cashback_amount);
												$ref_Total_Amount 	= ($referral_balance+$transation_amounts);
												$ref_User_details 	= $this->view_user($ref_user_id);
												$ref_us_email 	  	= $ref_User_details[0]->email;
												$ref_with_status  	= $ref_User_details[0]->withdraw_mail;
												$ref_unsuburl	  	= base_url().'un-subscribe/withdraw/'.$ref_user_id;
												$ref_myaccount    	= base_url().'resgate';
												$ref_firstname 	  	= $ref_User_details[0]->first_name;
												$ref_lastname  	  	= $ref_User_details[0]->last_name;
												$with_mail_status 	= $ref_User_details[0]->first_withdraw_mail_status;
												$first_with_status  = $ref_User_details[0]->first_withdraw_status;
												$second_with_status = $ref_User_details[0]->second_withdraw_status;

												if($with_mail_status == 1)
												{
													$Admin_Minimum_Cashback = $remain_minimum_amt;
													$new_withdraw_status    = $second_with_status;
												}
												else
												{
													$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;	
													$new_withdraw_status    = $first_with_status;
												}

												if($ref_firstname == '' && $ref_lastname == '')
												{
													$ex_name  	  = explode('@', $ref_User_details[0]->email);
													$ref_username = $ex_name[0]; 
												}
												else
												{
													$ref_username = $ref_firstname.' '.$ref_lastname;
												}	
												/*End 9-1-17*/
												 
												if($ref_with_status == 1)
												{
													if($new_withdraw_status == 0)
													{
														if($ref_Total_Amount>=$Admin_Minimum_Cashback)
														{
															$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
															if($obj_temp->num_rows>0)
															{
																$mail_temp  = $obj_temp->row(); 
																$fe_cont    = $mail_temp->email_template;	
																$subject  	= $mail_temp->email_subject;	
																$servername = base_url();
																$nows 		= date('Y-m-d');	
																$this->load->library('email');
																$gd_api=array(
																	
																	'###NAME###'=>$ref_username,
																	'###AMOUNT###'=>str_replace('.', ',', bcdiv($ref_Total_Amount,1,2)),
																	'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
																	'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
																	'###ULINK###'=>'<a href='.$ref_unsuburl.'>'.$ref_unsuburl.'</a>',
																	'###MYACLINK###'=>'<a href='.$ref_myaccount.'>'.$ref_myaccount.'</a>'
																	);
																				   
																$gd_message=strtr($fe_cont,$gd_api);
																$config = Array(
																	'mailtype'  => 'html',
																	'charset'   => 'utf-8',
																);

																$this->email->set_newline("\r\n");
																$this->email->initialize($config);
																$this->email->from($admin_emailid,$site_name.'!');
																$this->email->to($ref_us_email);
																$this->email->subject($subject);
																$this->email->message($gd_message);
																$this->email->send();
																$this->email->print_debugger();

																/*new code for update a first or second withdraw amount status 23-6-17*/
																if($new_withdraw_status == 0)
																{
																	if($with_mail_status == 1)
																	{
																		$data = array(		
																		'second_withdraw_status'  => 1);
																		$this->db->where('user_id',$ref_user_id);
																		$update_qry= $this->db->update('tbl_users',$data);
																	}
																	else
																	{
																		$data = array(		
																		'first_withdraw_status'  => 1);
																		$this->db->where('user_id',$ref_user_id);
																		$update_qry= $this->db->update('tbl_users',$data);
																	}	
																}
																/*End*/
															}
														}
													}	
												}
												 
												/*end 1-4-17*/		
											}
											/*End mail notification for referral users 4-5-17*/

											/*first and second withdraw mail notification for refered user 4-5-17*/
											$Total_Amount 	  	= ($balance+$transaction_amount);
											$User_details 	  	= $this->view_user($get_userid);
											$us_email 		  	= $User_details[0]->email;
											$with_status      	= $User_details[0]->withdraw_mail;
											$firstname 		  	= $User_details[0]->first_name;
											$lastname  		  	= $User_details[0]->last_name;
											$with_mail_status 	= $User_details[0]->first_withdraw_mail_status;
											$unsuburl	 	  	= base_url().'un-subscribe/withdraw/'.$get_userid;
											$myaccount    	  	= base_url().'resgate';
											$first_with_status  = $User_details[0]->first_withdraw_status;
											$second_with_status = $User_details[0]->second_withdraw_status;

											if($with_mail_status == 1)
											{
												$Admin_Minimum_Cashback = $remain_minimum_amt;
												$new_withdraw_status    = $second_with_status;
											}
											else
											{
												$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;
												$new_withdraw_status    = $first_with_status;	
											}

											if($firstname == '' && $lastname == '')
											{
												$ex_name  = explode('@', $User_details[0]->email);
												$username = $ex_name[0]; 
											}
											else
											{
												$username = $firstname.' '.$lastname;
											}	
											/*End 9-1-17*/
											
											if($with_status == 1)
											{
												if($new_withdraw_status == 0)
												{
													if($Total_Amount >= $Admin_Minimum_Cashback)
													{
														$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
														if($obj_temp->num_rows>0)
														{
															$mail_temp  = $obj_temp->row(); 
															$fe_cont    = $mail_temp->email_template;	
															$subject  	= $mail_temp->email_subject;		
															$servername = base_url();
															$nows 		= date('Y-m-d');	
															$this->load->library('email');
															$gd_api=array(
																
																'###NAME###'=>$username,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($Total_Amount,1,2)),
																'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
																'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
																'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
																);
																			   
															$gd_message=strtr($fe_cont,$gd_api);
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);
															 
															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_emailid,$site_name.'!');
															$this->email->to($us_email);
															$this->email->subject($subject);
															$this->email->message($gd_message);
															$this->email->send();
															$this->email->print_debugger();

															/*new code for update a first or second withdraw amount status 23-6-17*/
															if($new_withdraw_status == 0)
															{
																if($with_mail_status == 1)
																{
																	$data = array(		
																	'second_withdraw_status'  => 1);
																	$this->db->where('user_id',$user_id);
																	$update_qry= $this->db->update('tbl_users',$data);
																}
																else
																{
																	$data = array(		
																	'first_withdraw_status'  => 1);
																	$this->db->where('user_id',$user_id);
																	$update_qry= $this->db->update('tbl_users',$data);
																}	
															}
															/*End*/
														}
													}
												}	
											}													
											/*End 9-1-17*/
											/*End mail notification for referred user 4-5-17*/
										}	
									}

									if($cstatus == 'Canceled')
									{
										if($cash_status != 'Canceled')
										{
											$data = array('status' => $cstatus); 
											$this->db->where('report_update_id',$report_update_id);
											$updation = $this->db->update('cashback',$data);

											/*mail for Canceled cashback 2-3-17*/
											if($cashback_status == 1)
											{
												$this->db->where('mail_id',11);
												$mail_template = $this->db->get('tbl_mailtemplates');
												if($mail_template->num_rows >0) 
												{

												   	$fetch 	   = $mail_template->row();
												   	$subject   = $fetch->email_subject;
												   	$templete  = $fetch->email_template;
												   	$url 	   = base_url().'cashback/my_earnings/';
												   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
												   	$myaccount = base_url().'minha_conta';
												   
													$this->load->library('email');
													
													$config = Array(
														'mailtype'  => 'html',
														'charset'   => 'utf-8',
													);
													
													$sub_data = array(
														'###SITENAME###'=>$site_name
													);
													$subject_new = strtr($subject,$sub_data);
													
													//$this->email->initialize($config);
													$this->email->set_newline("\r\n");
													$this->email->initialize($config);
													$this->email->from($admin_email,$site_name.'!');
													$this->email->to($user_email);
													$this->email->subject($subject_new);
												   
													$data = array(
														'###NAME###'=>$user_name,
														'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
														'###SITENAME###'=>$site_name,
														'###ADMINNO###'=>$admin_no,
														'###DATE###'=>$date,
														'###AMOUNT###'=>str_replace('.', ',', $transaction_amount),
														'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
														'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
												    );
												   
												   $content_pop=strtr($templete,$data);
												   //echo print_r($content_pop); exit;
												   // echo $content_pop; echo $subject_new;
												   $this->email->message($content_pop);
												   $this->email->send();  
												}
											} 
											/*end 2-3-17*/

											/*New code for update user table 4-5-17*/
											if($cash_status =='Completed' && $cstatus == 'Canceled')
											{
												$user_bale 		= $balance;
												$newbalnce 		= $user_bale - $transaction_amount;
													
												$data = array(		
												'balance' => $newbalnce);
												$this->db->where('user_id',$get_userid);
												$update_qry = $this->db->update('tbl_users',$data);

												/*new code 26-7-17*/
												$this->db->delete('transation_details',array('report_update_id' => $report_update_id,'transation_reason'=>'Cashback','user_id'=>$get_userid));
												/*End 26-7-17*/
											}
											/*End 4-5-17*/

											/*Mail for cancelled referral amount for reffered user mail 04-5-17*/
											$this->db->where('report_update_id',$report_update_id);
											$cashbacks 	   = $this->db->get('cashback');
											$cashback_data = $cashbacks->row();
											if($cashback_data->referral!=0)
											{
												$this->db->where('trans_id',$cashback_data->txn_id);
												$txn 		= $this->db->get('transation_details');
												$txn_detail = $txn->row();
												
												if(!empty($txn_detail))
												{
													$txn_id 	 	   = $txn_detail->trans_id;
													$ref_user_id 	   = $txn_detail->user_id;
													$transation_amount = $txn_detail->transation_amount;
													$newtrans_status   = $txn_detail->transation_status; 
													$refer_user 	   = $this->view_user($ref_user_id);

													if($refer_user)
													{
														foreach($refer_user as $single)
														{
															$referral_balance = $single->balance;
															$user_email 	  = $single->email;
															$first_name 	  = $single->first_name;
															$last_name 		  = $single->last_name;
															$reffer_status    = $single->referral_mail;

															if($first_name == '' && $last_name == '')
															{
																$ex_name   = explode('@', $user_email);
																$user_name = $ex_name[0]; 
															}
															else
															{
																$user_name = $first_name.' '.$last_name;
															}
														}

														if($cstatus == 'Canceled')
														{	
															if($newtrans_status == 'Approved')
															{
																$new_balance = ($referral_balance - $transation_amount);
																$this->db->where('user_id',$ref_user_id);
																$this->db->update('tbl_users',array('balance'=>$new_balance));
															}

															/*new code 11-7-17*/
															$this->db->delete('transation_details',array('report_update_id' => $txn_detail->report_update_id,'transation_reason'=>'Cashback'));
															/*End 11-7-17*/	
														}

														$data = array(	
														'transation_status'=>$cstatus
														);
														$this->db->where('report_update_id',$report_update_id);
														$updation = $this->db->update('transation_details',$data);
															
														$unsuburl  = base_url().'un-subscribe/referral/'.$ref_user_id;
													    $myaccount = base_url().'minha-conta';

														$this->db->where('admin_id',1);
														$admin_det = $this->db->get('admin');
														if($admin_det->num_rows >0) 
														{    
															$admin 		 = $admin_det->row();
															$admin_email = $admin->admin_email;
															$site_name 	 = $admin->site_name;
															$admin_no 	 = $admin->contact_number;
															$site_logo 	 = $admin->site_logo;
														}

														if($reffer_status == 1)
														{
															$this->db->where('mail_id',19);
															$mail_template = $this->db->get('tbl_mailtemplates');
															if($mail_template->num_rows >0) 
															{
															   $fetch = $mail_template->row();
															   $subject = $fetch->email_subject;
															   $templete = $fetch->email_template;
																
																$config = Array(
																	'mailtype'  => 'html',
																	'charset'   => 'utf-8',
																);
																
																// $this->email->initialize($config);
																$this->email->set_newline("\r\n");
																$this->email->initialize($config);
																$this->email->from($admin_email,$site_name.'!');
																$this->email->to($user_email);
																$this->email->subject($subject);
															   
																$data = array(
																	'###ADMINNO###'=>$admin_no,
																	'###EMAIL###'=>$user_name,
																	'###DATE###'=>$date,
																	'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
																	'###SITENAME###' =>$site_name,
																	'###STATUS###'=>$status,
																	'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amount,1,2)),
																	'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
																	'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
															    );
															   
															   $content_pop=strtr($templete,$data);
															   $this->email->message($content_pop);
															   $this->email->send();  
															}
														}
													}
													/*end*/
												}	
											}
											/*End 4-5-17*/
										}	
									}
									if($cstatus == 'Pending')
									{
										//New hide 25-7-17
										/*if($cash_status != 'Pending')
										{	
											$data = array('status' => $cstatus); 
											$this->db->where('report_update_id',$report_update_id);
											$updation = $this->db->update('cashback',$data);

											//New code for update user balance details 4-5-17
											if($cash_status =='Completed') 
											{	
												$user_bale 		= $balance;
												$newbalnce 		= $user_bale - $transaction_amount;
													
												$data = array(		
												'balance' => $newbalnce);
												$this->db->where('user_id',$get_userid);
												$update_qry = $this->db->update('tbl_users',$data);
											}
											//end 4-5-17

											//mail for Pending cashback 2-3-17
											if($cashback_status == 1)
											{
												$this->db->where('mail_id',10);
												$mail_template = $this->db->get('tbl_mailtemplates');
												
												if($mail_template->num_rows >0) 
												{
												    $fetch     = $mail_template->row();
												    $subject   = $fetch->email_subject;
												    $templete  = $fetch->email_template;
												    //$url     = base_url().'cashback/my_earnings/';
												    $unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
												    $myaccount = base_url().'minha_conta';
												   
													$this->load->library('email');
													
													$config = Array(
														'mailtype'  => 'html',
														'charset'   => 'utf-8',
													);
													
													$sub_data = array(
														'###SITENAME###'=>$site_name
													);
													$subject_new = strtr($subject,$sub_data);
													
													$this->email->set_newline("\r\n");
													$this->email->initialize($config);
													$this->email->from($admin_email,$site_name.'!');
													$this->email->to($user_email);
													$this->email->subject($subject_new);
												   
													$data = array(
														'###NAME###'=>$user_name,
														'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
														'###SITENAME###'=>$site_name,
														'###ADMINNO###'=>$admin_no,
														'###DATE###'=>$date,
														'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
														'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
														'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
												    );
												   
												    $content_pop=strtr($templete,$data);
												   	$this->email->message($content_pop);
												   	$this->email->send();  
												}
											} 
											//end 2-3-17

											//New code for pending referral mail for (REFER) User 4-5-17
											$check_ref    = $this->check_ref_user($get_userid);

											if($check_ref > 0)		
											{
												$ref_id  	  = $check_ref;
												$ref_user_bal = $this->view_balance($check_ref);
												$return  	  = $this->check_active_user($ref_id);

												if($return)
												{
													if($cash_status  == 'Completed')
													{	
														$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Approved'")->row();	
													}
													if($cash_status  == 'Canceled')
													{
														$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Canceled'")->row();
													}
													if($cash_status  == 'Pending')
													{
														$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Pending'")->row();
													}
													 
												 										 
												 	$transation_amount = $ref_trans_details->transation_amount;

													foreach($return as $newreturn)
													{
														$referral_balance   = $newreturn->balance; 
														$user_referral_mail = $newreturn->referral_mail;
														$ref_user_email     = $newreturn->email;
														$ref_first_name   	= $newreturn->first_name;
														$ref_last_name 	  	= $newreturn->last_name;								

														if($ref_first_name == '' && $ref_last_name == '')
														{
															$ex_name       = explode('@', $ref_user_email);
															$ref_user_name = $ex_name[0]; 
														}
														else
														{
															$ref_user_name = $ref_first_name.' '.$ref_last_name;
														}
													}

													if($cash_status  == 'Completed')
													{
														$bal_ref = $referral_balance - $transation_amount;
														$this->db->where('user_id',$ref_id);
														$this->db->update('tbl_users',array('balance'=>$bal_ref));
													}

													$data = array('transation_status'=>'Pending'); 
													$this->db->where('report_update_id',$report_update_id);
													$this->db->where('ref_user_tracking_id',$get_userid);
													$this->db->update('transation_details',$data);	
													 
													if($user_referral_mail == 1)
													{
														$this->db->where('mail_id',20);
														$mail_template = $this->db->get('tbl_mailtemplates');
														if($mail_template->num_rows >0) 
														{
															$fetch     = $mail_template->row();
															$subject   = $fetch->email_subject;
															$templete  = $fetch->email_template;
															$url 	   = base_url().'my_earnings/';
															$unsuburls = base_url().'un-subscribe/referral/'.$ref_id;
													   		$myaccount = base_url().'minha-conta';
															
															$this->load->library('email');

															$config    = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
															);
																	
															$sub_data = array(
															'###SITENAME###'=>$site_name
															);
															
															$subject_new = strtr($subject,$sub_data);
															// $this->email->initialize($config);
															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_email,$site_name.'!');
															$this->email->to($ref_user_email);
															$this->email->subject($subject_new);

															//echo $transation_amount; exit;											
															$datas = array(
															'###NAME###'=>$ref_user_name,
															'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
															'###SITENAME###'=>$site_name,
															'###ADMINNO###'=>$admin_no,
															'###DATE###'=>date('y-m-d'),
															'###AMOUNT###'=>str_replace('.',',',bcdiv($transation_amount,1,2)),
															'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
															'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
															);
															$content_pop=strtr($templete,$datas);
															$this->email->message($content_pop);
															$this->email->send();  
														}
													}
												}
											}
											//End 4-5-17
										}*/	
										//New hide 25-7-17
									}
								}								
							}	 
						}
					}	
					else
					{
						//echo "hello";
						$all = 0;
						$transaction_id   = $content['item']['transactionCode'];
						$this->db->where('report_update_id',$transaction_id);
						$all = $this->db->get('cashback'); /*->num_rows();*/
					 	$fetch = $all->row();

						if(!empty($fetch))
						{
							 
							$duplicate = 1;
							$get_userid 		= $fetch->user_id;
							$transaction_amount = $fetch->cashback_amount;
							/*$user_id            = $content['item']['associateId'];
							$get_userid     	= decode_userid($user_id);	
							 
							if($get_userid =='-230000')
							{
								$get_userid     = 0;
							}
							$currency_type      = $content['item']['currency'];
							
							if($currency_type != 'BRL')
							{
								$amount   			= $content['item']['gmv'];
								$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
							}	
							else
							{
								$transaction_amount = $content['item']['gmv'];
							}*/
							
							$coupon_id          = $content['item']['advertiser']['advertiserName'];
							$cashback_amount    = $content['item']['commission'];
							$old_trans_date     = $content['item']['checkoutDate'];
							$transaction_date   = preg_replace('/\//', '-',$old_trans_date);
							//$transaction_amount = $content['item']['gmv'];
							//echo $transaction_amount; exit;
							$ref_cashback_percent= 0;
							$ref_cashback_amount = 0;
							$total_Cashback_paid = 0;
							$status 			 = $content['item']['statusName']; 
							$status_id 			 = $content['item']['statusId'];


							if($transaction_id !='')
							{
								$report_update_id   = $transaction_id;
							}
							else
							{
								$report_update_id   = rand(1000,9999);	
							}

							/*new code 4-5-17*/
						 	$newtransaction_id = $content['item']['transactionCode'];
							$cash_status 	   = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('status');
							if($newtransaction_id == '')
							{
								$newid             = rand(1000,9999);
								$newtransaction_id = md5($newid);
							}
						 	/*End 4-5-17*/

							$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$get_userid))->row('balance');
							if($prev_userbal == '')
							{
								$prev_userbal = 0;	
							}

							/*New code for mail notification details 2-3-17*/
							$userdetails 	 = $this->db->query("SELECT * from tbl_users where user_id=$get_userid")->row();
							$cashback_status = $userdetails->cashback_mail;
							$user_email      = $userdetails->email;
							$first_name      = $userdetails->first_name;
							$last_name       = $userdetails->last_name;
							
							if($first_name == '' && $last_name == '')
							{
								$ex_name   = explode('@', $user_email);
								$user_name = $ex_name[0]; 
							}
							else
							{
								$user_name = $first_name.''.$last_name;
							}

							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin 		 = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name 	 = $admin->site_name;
								$admin_no	 = $admin->contact_number;
								$site_logo 	 = $admin->site_logo;
							}
							$date = date('Y-m-d');
							/*end 2-3-17*/
							//echo $status;
							//} exit; 
							if($status !='')
							{
								
								/*if($status == 'CONFIRMED' && $status_id == 1)
								{
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");
								}
								if($status == 'CANCELED' && $status_id == 2)
								{
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Canceled')");
								}
								if($status == 'PENDING')
								{
									//echo "hai"; exit;
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Pending')");
								}*/

								if($status == 'CONFIRMED')
								{
									$cstatus = 'Completed';
								}
								if($status == 'CANCELED')
								{	
									$cstatus = 'Canceled';
								}
								if($status == 'PENDING')
								{	
									$cstatus = 'Pending';
								}
								

								if($cstatus == 'Completed')
								{
									if($cash_status != 'Completed')
									{
										
										$data = array('status' => $cstatus); 
										$this->db->where('report_update_id',$report_update_id);
										$updation = $this->db->update('cashback',$data);



										$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
										VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");

										$this->db->where('user_id',$get_userid);
										$this->db->update('tbl_users',array('balance'=>$prev_userbal+$transaction_amount));
										
										/*mail for Completed cashback 2-3-17*/
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',8);
											$mail_template = $this->db->get('tbl_mailtemplates');
											if($mail_template->num_rows >0) 
											{
											   	$fetch     = $mail_template->row();
											   	$subject   = $fetch->email_subject;
											   	$templete  = $fetch->email_template;
											   	$url 	  = base_url().'my_earnings/';
											   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
											   	$myaccount = base_url().'minha_conta';
											   
											  	$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);

												$subject_new = strtr($subject,$sub_data);
												
												//$this->email->initialize($config);
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email, $site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

											    );
											   
											   $content_pop=strtr($templete,$data);
											   $this->email->message($content_pop);
											   $this->email->send();  
											}
										} 
										/*end 2-3-17*/

										/*New code for referral mail and user cashback balance details 04-5-17*/
										$this->db->where('report_update_id',$report_update_id);
										$cashbacks 	   = $this->db->get('cashback');
										$cashback_data = $cashbacks->row();

										if($cashback_data->referral!=0)
										{
											$this->db->where('trans_id',$cashback_data->txn_id);
											$txn = $this->db->get('transation_details');
											$txn_detail = $txn->row();

											if($txn_detail)
											{
												$txn_id 	 	    = $txn_detail->trans_id;
												$ref_user_id 	    = $txn_detail->user_id;
												$transation_amounts = $txn_detail->transation_amount;
												$refer_user 	    = $this->view_user($ref_user_id);
												
												if($refer_user)
												{
													foreach($refer_user as $single)
													{
														$referral_balance = $single->balance;
														$user_email 	  = $single->email;
														$first_name 	  = $single->first_name;
														$last_name 		  = $single->last_name;

														if($first_name == '' && $last_name == '')
														{
															$ex_name   = explode('@', $user_email);
															$user_name = $ex_name[0]; 
														}
														else
														{
															$user_name = $first_name.' '.$last_name;
														}
													}

													//Update refer user balance details in User table and transaction table(approve referral cashback amount)//
													$this->db->where('user_id',$ref_user_id);
													$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amounts));
													 

													$data = array('transation_status'=>'Approved','transation_reason'=>'Referal Payment'); //'transaction_date'=>$date,
													//$this->db->where('transation_reason','Pending Referal Payment');
													$this->db->where('trans_id',$cashback_data->txn_id);
													$this->db->update('transation_details',$data);

													/* mail for Approve cashback amt mail notifications */
													if($single->referral_mail == 1)
													{	
														$this->db->where('mail_id',9);
														$mail_template = $this->db->get('tbl_mailtemplates');
														if($mail_template->num_rows >0) 
														{
														   $fetch 	  = $mail_template->row();
														   $subject   = $fetch->email_subject;
														   $templete  = $fetch->email_template;
														   $url 	  = base_url().'my_earnings/';
														   $unsuburls = base_url().'un-subscribe/referral/'.$ref_user_id;
											   			   $myaccount = base_url().'minha_conta';
														   
															$this->load->library('email');
															
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);
															
															$sub_data = array(
																'###SITENAME###'=>$site_name
															);
															$subject_new = strtr($subject,$sub_data);
															
															// $this->email->initialize($config);
															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_email,$site_name.'!');
															$this->email->to($user_email);
															$this->email->subject($subject_new);
														   
															$datas = array(
																'###NAME###'=>$user_name,
																'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
																'###SITENAME###'=>$site_name,
																'###ADMINNO###'=>$admin_no,
																'###DATE###'=>$date,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amounts,1,2)),
																'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
															);
														   
														   $content_pop=strtr($templete,$datas);
														   $this->email->message($content_pop);
														   $this->email->send();  
														}
													}	
													/* Mail for Approve referral cashback amount mail End*/
												}
											}
										}
										/*end 4-5-17*/

										/*first and second Withdraw mail notification for referral users 4-5-17*/
										$this->db->where('admin_id',1);
										$Admin_Details_Query    = $this->db->get('admin');
										$Admin_Details 		    = $Admin_Details_Query->row();
										$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
										$remain_minimum_amt     = $Admin_Details->remain_minimum_with_amt;
										$Site_Logo 				= $Admin_Details->site_logo;
										$admin_emailid 			= $Admin_Details->admin_email;

										if($ref_user_id!='')
										{

											$this->db->where('report_update_id',$report_update_id);
											$cashbacks 	   = $this->db->get('cashback');
											$cashback_data = $cashbacks->row();

											if($cashback_data->referral!=0)
											{
												$this->db->where('trans_id',$cashback_data->txn_id);
												$txn = $this->db->get('transation_details');
												$txn_detail = $txn->row();
												
												if($txn_detail)
												{
													//$new_txn_ids = $txn_detail->new_txn_id;
													$new_txn_ids   = $txn_detail->trans_id;
											
													if($single->referral_category_type != '')
													{ 	
														$referrals    	    = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
														$ref_by_percentage  = $referrals->ref_by_percentage;
														$ref_by_rate 		= $referrals->ref_by_rate;
														$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

														//3** Bonus by Refferal Rate type//
														if($bonus_by_ref_rate == 1)
														{
															$n9  	 = '333445';
															$n12 	 = $n9 + $ref_user_id;
															$now 	 = date('Y-m-d H:i:s');	
															$selqry  = "SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Referal Payment' AND user_id=$ref_user_id"; 
													 		$query   = $this->db->query("$selqry");
															$numrows = $query->num_rows();
															if($numrows > 0)
															{
																$fetch 		   = $query->row();
																$usercount 	   = $fetch->userid;
																$referrals     = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
																$bonus_amount  = $referrals->ref_cashback_rate_bonus;
																$friends_count = $referrals->friends_count;
																
																if($usercount == $friends_count)
																{	
																	if($bonus_amount!='')
																	{	
																		if($single->referral_category_type == 1)
																		{ 
																		 	$types = 'One';
																		}
																		if($single->referral_category_type == 2)
																		{ 
																		 	$types = 'Two';
																		}
																		if($single->referral_category_type == 3)
																		{ 
																		 	$types = 'Three';
																		}
																		if($single->referral_category_type == 4)
																		{ 
																		 	$types = 'Four';
																		}
																		if($single->referral_category_type == 5)
																		{ 
																		 	$types = 'Five';
																		}
																		if($single->referral_category_type == 6)
																		{ 
																		 	$types = 'Six';
																		}
																		if($single->referral_category_type == 7)
																		{ 
																		 	$types = 'Seven';
																		}
																		if($single->referral_category_type == 8)
																		{ 
																		 	$types = 'Eight';
																		}
																		if($single->referral_category_type == 9)
																		{ 
																		 	$types = 'Nine';
																		}
																		if($single->referral_category_type == 10)
																		{ 
																		 	$types = 'Ten';
																		}

																		$selqry	= "SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category $types User' AND `user_id`=$ref_user_id"; 
														 				$query1	= $this->db->query("$selqry");
														 				$newnumrows = $query1->num_rows();
																		if($newnumrows > 0)
																		{
																			$fetch 		 = $query1->row();
																			$users_count = $fetch->userid;
																			if($users_count == 0)	
																			{	
																				$data = array(			
																				'transation_amount' => $bonus_amount,	
																				'user_id' => $ref_user_id,	
																				'transation_date' => $now,
																				'transaction_date' => $now,
																				'transation_id'=>$n12,	
																				'transation_reason' => 'Referral Bonus for Category ' .$types. ' User',	
																				'mode' => 'Credited',
																				'details_id'=>'',	
																				'table'=>'',	
																				'new_txn_id'=>0,
																				'transation_status ' => 'Approved',
																				'report_update_id'=>$newtransaction_id
																				);	
																				$this->db->insert('transation_details',$data);
																			}	
																		}
																	}	
																}
															} 
														}
														//3** Bonus by Refferal Rate type End//

														//1** Refferal by Percentage type Start//
														if($ref_by_percentage == 1)
														{
															$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); /*,'transaction_date'=>$date*/
															$this->db->where('new_txn_id',$new_txn_ids);
															$this->db->update('transation_details',$data);
														}
														//1** Refferal by Percentage type End//	
													}	
												}
											}

											/*New code for withdraw notification 1-4-17*/
											$ref_prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$ref_user_id))->row('balance');
											if($ref_prev_userbal == '')
											{
												$ref_prev_userbal = 0;	
											}


											//$ref_Total_Amount = ($ref_balance+$ref_cashback_amount);
											$ref_Total_Amount 	= ($referral_balance+$transation_amounts);
											$ref_User_details 	= $this->view_user($ref_user_id);
											$ref_us_email 	  	= $ref_User_details[0]->email;
											$ref_with_status  	= $ref_User_details[0]->withdraw_mail;
											$ref_unsuburl	  	= base_url().'un-subscribe/withdraw/'.$ref_user_id;
											$ref_myaccount    	= base_url().'resgate';
											$ref_firstname 	  	= $ref_User_details[0]->first_name;
											$ref_lastname  	  	= $ref_User_details[0]->last_name;
											$with_mail_status 	= $ref_User_details[0]->first_withdraw_mail_status;
											$first_with_status  = $ref_User_details[0]->first_withdraw_status;
											$second_with_status = $ref_User_details[0]->second_withdraw_status;

											if($with_mail_status == 1)
											{
												$Admin_Minimum_Cashback = $remain_minimum_amt;
												$new_withdraw_status    = $second_with_status;
											}
											else
											{
												$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;
												$new_withdraw_status    = $first_with_status;	
											}

											if($ref_firstname == '' && $ref_lastname == '')
											{
												$ex_name  	  = explode('@', $ref_User_details[0]->email);
												$ref_username = $ex_name[0]; 
											}
											else
											{
												$ref_username = $ref_firstname.' '.$ref_lastname;
											}	
											/*End 9-1-17*/
											 
											if($ref_with_status == 1)
											{
												if($new_withdraw_status == 0)
												{
													if($ref_Total_Amount>=$Admin_Minimum_Cashback)
													{
														$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
														if($obj_temp->num_rows>0)
														{
															$mail_temp  = $obj_temp->row(); 
															$fe_cont    = $mail_temp->email_template;	
															$subject  	= $mail_temp->email_subject;	
															$servername = base_url();
															$nows 		= date('Y-m-d');	
															$this->load->library('email');
															$gd_api=array(
																
																'###NAME###'=>$ref_username,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($ref_Total_Amount,1,2)),
																'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
																'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
																'###ULINK###'=>'<a href='.$ref_unsuburl.'>'.$ref_unsuburl.'</a>',
																'###MYACLINK###'=>'<a href='.$ref_myaccount.'>'.$ref_myaccount.'</a>'
																);
																			   
															$gd_message=strtr($fe_cont,$gd_api);
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);

															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_emailid,$site_name.'!');
															$this->email->to($ref_us_email);
															$this->email->subject($subject);
															$this->email->message($gd_message);
															$this->email->send();
															$this->email->print_debugger();

															/*new code for update a first or second withdraw amount status 20-5-17*/
															if($new_withdraw_status == 0)
															{
																if($with_mail_status == 1)
																{
																	$data = array(		
																	'second_withdraw_status'  => 1);
																	$this->db->where('user_id',$ref_user_id);
																	$update_qry= $this->db->update('tbl_users',$data);
																}
																else
																{
																	$data = array(		
																	'first_withdraw_status'  => 1);
																	$this->db->where('user_id',$ref_user_id);
																	$update_qry= $this->db->update('tbl_users',$data);
																}	
															}
															/*End*/
														}
													}
												}	
											}
											 
											/*end 1-4-17*/		
										}
										/*End mail notification for referral users 4-5-17*/

										/*first and second withdraw mail notification for refered user 4-5-17*/
										$Total_Amount 	  	= ($balance+$transaction_amount);
										$User_details 	  	= $this->view_user($get_userid);
										$us_email 		  	= $User_details[0]->email;
										$with_status      	= $User_details[0]->withdraw_mail;
										$firstname 		  	= $User_details[0]->first_name;
										$lastname  		  	= $User_details[0]->last_name;
										$with_mail_status 	= $User_details[0]->first_withdraw_mail_status;

										$unsuburl	 	  	= base_url().'un-subscribe/withdraw/'.$get_userid;
										$myaccount    	  	= base_url().'resgate';
										$first_with_status  = $User_details[0]->first_withdraw_status;
										$second_with_status = $User_details[0]->second_withdraw_status;

										if($with_mail_status == 1)
										{
											$Admin_Minimum_Cashback = $remain_minimum_amt;
											$new_withdraw_status    = $second_with_status;
										}
										else
										{
											$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;	
											$new_withdraw_status    = $first_with_status;
										}

										if($firstname == '' && $lastname == '')
										{
											$ex_name  = explode('@', $User_details[0]->email);
											$username = $ex_name[0]; 
										}
										else
										{
											$username = $firstname.' '.$lastname;
										}	
										/*End 9-1-17*/

										if($with_status == 1)
										{
											if($new_withdraw_status == 0)
											{
												if($Total_Amount>=$Admin_Minimum_Cashback)
												{
													$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
													if($obj_temp->num_rows>0)
													{
														$mail_temp  = $obj_temp->row(); 
														$fe_cont    = $mail_temp->email_template;	
														$subject  	= $mail_temp->email_subject;		
														$servername = base_url();
														$nows 		= date('Y-m-d');	
														$this->load->library('email');
														$gd_api=array(
															
															'###NAME###'=>$username,
															'###AMOUNT###'=>str_replace('.', ',', bcdiv($Total_Amount,1,2)),
															'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
															'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
															'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
															'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
															);
																		   
														$gd_message=strtr($fe_cont,$gd_api);
														$config = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
														);
														 
														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_emailid,$site_name.'!');
														$this->email->to($us_email);
														$this->email->subject($subject);
														$this->email->message($gd_message);
														$this->email->send();
														$this->email->print_debugger();

														/*new code for update a first or second withdraw amount status 23-6-17*/
														if($new_withdraw_status == 0)
														{
															if($with_mail_status == 1)
															{
																$data = array(		
																'second_withdraw_status'  => 1);
																$this->db->where('user_id',$get_userid);
																$update_qry= $this->db->update('tbl_users',$data);
															}
															else
															{
																$data = array(		
																'first_withdraw_status'  => 1);
																$this->db->where('user_id',$get_userid);
																$update_qry= $this->db->update('tbl_users',$data);
															}	
														}
														/*End*/
													}
												}
											}	
										}	

										/*End 9-1-17*/
										/*End mail notification for referred user 4-5-17*/
									}	
								}
								
								if($cstatus == 'Canceled')
								{
									if($cash_status != 'Canceled')
									{
										$data = array('status' => $cstatus); 
										$this->db->where('report_update_id',$report_update_id);
										$updation = $this->db->update('cashback',$data);

										/*mail for Canceled cashback 2-3-17*/
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',11);
											$mail_template = $this->db->get('tbl_mailtemplates');
											if($mail_template->num_rows >0) 
											{

											   	$fetch 	   = $mail_template->row();
											   	$subject   = $fetch->email_subject;
											   	$templete  = $fetch->email_template;
											   	$url 	   = base_url().'cashback/my_earnings/';
											   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
											   	$myaccount = base_url().'minha_conta';
											   
												$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);
												$subject_new = strtr($subject,$sub_data);
												
												//$this->email->initialize($config);
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email,$site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', $transaction_amount),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
											    );
											   
											   $content_pop=strtr($templete,$data);
											   //echo print_r($content_pop); exit;
											   // echo $content_pop; echo $subject_new;
											   $this->email->message($content_pop);
											   $this->email->send();  
											}
										} 
										/*end 2-3-17*/

										/*New code for update user table 4-5-17*/
										if($cash_status =='Completed' && $cstatus == 'Canceled')
										{
											$user_bale 		= $balance;
											$newbalnce 		= $user_bale - $transaction_amount;
												
											$data = array(		
											'balance' => $newbalnce);
											$this->db->where('user_id',$get_userid);
											$update_qry = $this->db->update('tbl_users',$data);

											/*new code 26-7-17*/
											$this->db->delete('transation_details',array('report_update_id' => $report_update_id,'transation_reason'=>'Cashback','user_id'=>$get_userid));
											/*End 26-7-17*/
										}
										/*End 4-5-17*/

										/*Mail for cancelled referral amount for reffered user mail 04-5-17*/
										$this->db->where('report_update_id',$report_update_id);
										$cashbacks 	   = $this->db->get('cashback');
										$cashback_data = $cashbacks->row();
										if($cashback_data->referral!=0)
										{
											$this->db->where('trans_id',$cashback_data->txn_id);
											$txn 		= $this->db->get('transation_details');
											$txn_detail = $txn->row();
											
											if(!empty($txn_detail))
											{
												$txn_id 	 	   = $txn_detail->trans_id;
												$ref_user_id 	   = $txn_detail->user_id;
												$transation_amount = $txn_detail->transation_amount;
												$newtrans_status   = $txn_detail->transation_status; 
												$refer_user 	   = $this->view_user($ref_user_id);

												if($refer_user)
												{
													foreach($refer_user as $single)
													{
														$referral_balance = $single->balance;
														$user_email 	  = $single->email;
														$first_name 	  = $single->first_name;
														$last_name 		  = $single->last_name;
														$reffer_status    = $single->referral_mail;

														if($first_name == '' && $last_name == '')
														{
															$ex_name   = explode('@', $user_email);
															$user_name = $ex_name[0]; 
														}
														else
														{
															$user_name = $first_name.' '.$last_name;
														}
													}

													if($cstatus == 'Canceled')
													{
														if($newtrans_status == 'Approved')
														{	
															$new_balance = ($referral_balance - $transation_amount);
															$this->db->where('user_id',$ref_user_id);
															$this->db->update('tbl_users',array('balance'=>$new_balance));
														}
														/*new code 11-7-17*/
														$this->db->delete('transation_details',array('report_update_id' => $txn_detail->report_update_id,'transation_reason'=>'Cashback'));
														/*End 11-7-17*/
													}

													$data = array(	
													'transation_status'=>$cstatus
													);
													$this->db->where('trans_id',$cashback_data->txn_id);
													//$this->db->where('report_update_id',$report_update_id);
													$updation = $this->db->update('transation_details',$data);
														
													$unsuburl  = base_url().'un-subscribe/referral/'.$ref_user_id;
												    $myaccount = base_url().'minha-conta';

													$this->db->where('admin_id',1);
													$admin_det = $this->db->get('admin');
													if($admin_det->num_rows >0) 
													{    
														$admin 		 = $admin_det->row();
														$admin_email = $admin->admin_email;
														$site_name 	 = $admin->site_name;
														$admin_no 	 = $admin->contact_number;
														$site_logo 	 = $admin->site_logo;
													}

													if($reffer_status == 1)
													{
														$this->db->where('mail_id',19);
														$mail_template = $this->db->get('tbl_mailtemplates');
														if($mail_template->num_rows >0) 
														{
														   $fetch = $mail_template->row();
														   $subject = $fetch->email_subject;
														   $templete = $fetch->email_template;
															
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);
															
															// $this->email->initialize($config);
															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_email,$site_name.'!');
															$this->email->to($user_email);
															$this->email->subject($subject);
														   
															$data = array(
																'###ADMINNO###'=>$admin_no,
																'###EMAIL###'=>$user_name,
																'###DATE###'=>$date,
																'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
																'###SITENAME###' =>$site_name,
																'###STATUS###'=>$status,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amount,1,2)),
																'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
														    );
														   
														   $content_pop=strtr($templete,$data);
														   $this->email->message($content_pop);
														   $this->email->send();  
														}
													}
												}
												/*end*/
											}	
										}
										/*End 4-5-17*/
									} 
								}

								if($cstatus == 'Pending')
								{
									/*if($cash_status != 'Pending')
									{
										//New code for update user balance details 4-5-17
										if($cash_status =='Completed') 
										{	
											$user_bale 		= $balance;
											$newbalnce 		= $user_bale - $transaction_amount;
												
											$data = array(		
											'balance' => $newbalnce);
											$this->db->where('user_id',$get_userid);
											$update_qry = $this->db->update('tbl_users',$data);
										}
										//end 4-5-17

										//mail for Pending cashback 2-3-17
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',10);
											$mail_template = $this->db->get('tbl_mailtemplates');
											
											if($mail_template->num_rows >0) 
											{
											    $fetch     = $mail_template->row();
											    $subject   = $fetch->email_subject;
											    $templete  = $fetch->email_template;
											    //$url     = base_url().'cashback/my_earnings/';
											    $unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
											    $myaccount = base_url().'minha_conta';
											   
												$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);
												$subject_new = strtr($subject,$sub_data);
												
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email,$site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
											    );
											   
											    $content_pop=strtr($templete,$data);
											   	$this->email->message($content_pop);
											   	$this->email->send();  
											}
										} 
										//end 2-3-17

										//New code for pending referral mail for (REFER) User 4-5-17
										$check_ref    = $this->check_ref_user($get_userid);

										if($check_ref > 0)		
										{
											$ref_id  	  = $check_ref;
											$ref_user_bal = $this->view_balance($check_ref);
											$return  	  = $this->check_active_user($ref_id);

											if($return)
											{
												if($cash_status  == 'Completed')
												{	
													$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Approved'")->row();	
												}
												if($cash_status  == 'Canceled')
												{
													$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Canceled'")->row();
												}
												if($cash_status  == 'Pending')
												{
													$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$user_id' AND `transation_status`='Pending'")->row();
												}
												 
											 										 
											 	$transation_amount = $ref_trans_details->transation_amount;

												foreach($return as $newreturn)
												{
													$referral_balance   = $newreturn->balance; 
													$user_referral_mail = $newreturn->referral_mail;
													$ref_user_email     = $newreturn->email;
													$ref_first_name   	= $newreturn->first_name;
													$ref_last_name 	  	= $newreturn->last_name;								

													if($ref_first_name == '' && $ref_last_name == '')
													{
														$ex_name       = explode('@', $ref_user_email);
														$ref_user_name = $ex_name[0]; 
													}
													else
													{
														$ref_user_name = $ref_first_name.' '.$ref_last_name;
													}
												}

												if($cash_status  == 'Completed')
												{
													$bal_ref = $referral_balance - $transation_amount;
													$this->db->where('user_id',$ref_id);
													$this->db->update('tbl_users',array('balance'=>$bal_ref));
												}

												$data = array('transation_status'=>'Pending'); 
												$this->db->where('report_update_id',$report_update_id);
												$this->db->where('ref_user_tracking_id',$get_userid);
												$this->db->update('transation_details',$data);	
												 
												if($user_referral_mail == 1)
												{
													$this->db->where('mail_id',20);
													$mail_template = $this->db->get('tbl_mailtemplates');
													if($mail_template->num_rows >0) 
													{
														$fetch     = $mail_template->row();
														$subject   = $fetch->email_subject;
														$templete  = $fetch->email_template;
														$url 	   = base_url().'my_earnings/';
														$unsuburls = base_url().'un-subscribe/referral/'.$ref_id;
												   		$myaccount = base_url().'minha-conta';
														
														$this->load->library('email');

														$config    = Array(
														'mailtype'  => 'html',
														'charset'   => 'utf-8',
														);
																
														$sub_data = array(
														'###SITENAME###'=>$site_name
														);
														
														$subject_new = strtr($subject,$sub_data);
														// $this->email->initialize($config);
														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_email,$site_name.'!');
														$this->email->to($ref_user_email);
														$this->email->subject($subject_new);

														//echo $transation_amount; exit;											
														$datas = array(
														'###NAME###'=>$ref_user_name,
														'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
														'###SITENAME###'=>$site_name,
														'###ADMINNO###'=>$admin_no,
														'###DATE###'=>date('y-m-d'),
														'###AMOUNT###'=>str_replace('.',',',bcdiv($transation_amount,1,2)),
														'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
														'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
														);
														$content_pop=strtr($templete,$datas);
														$this->email->message($content_pop);
														$this->email->send();  
													}
												}
											}
										}
										//End 4-5-17//
									}*/	
								}
							} 
						}
					}
				}		  
			}

			//Not yet Completed
			if($affiliate_name == 'rakuten')
			{
				//echo "<pre>";print_r($content); exit;
				if($content['saleItems']!='')
				{

					if(is_array($content['saleItems']['saleItem'][0]))
					{
						//echo "hai1".$content['saleItems']['saleItem']; exit;
						foreach($content['saleItems']['saleItem'] as $cont)
						{
							  
							$all=0;
							//$transaction_id   = $cont['clickId'];
							$transaction_id   = $cont['@attributes']['id'];
							$report_update_id = $cont['@attributes']['id'];

							//$this->db->where('transation_id',$transaction_id);
							$this->db->where('report_update_id',$report_update_id);
							$all   = $this->db->get('cashback'); //->num_rows();				 	  	 
							$fetch = $all->row();
							//echo $all;   
							//}exit;
							if(!empty($fetch))
							{
								 
								$duplicate = 1;
								
								$get_userid     	= $fetch->user_id;
								$transaction_amount = $fetch->cashback_amount;

								/*$user_id            = $cont['gpps']['gpp'][0]; 
								$val_user_id		= substr($user_id, 0, 5); 
								if($val_user_id == 'P0001')
								{
									$get_userid     = decode_userid($user_id);	
								}
								else
								{
									$get_userid     = $user_id;
								} 
								$currency_type      = $cont['currency'];
								if($currency_type != 'BRL')
								{
									$amount   			= $cont['amount'];
									$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
								}	
								else
								{
									$transaction_amount = $cont['amount'];	
								}*/

								$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$get_userid))->row('balance');
								if($prev_userbal == '')
								{
									$prev_userbal = 0;	
								}

								$status 			= $cont['reviewState'];
								$report_update_id   = $cont['@attributes']['id'];

								if($status == 'open')
								{
									$tstatus = 'Pending';
									$cstatus = 'Pending';
								}
								if($status == 'approved')
								{
									$tstatus = 'Approved';
									$cstatus = 'Completed';
								}
								if($status == 'rejected')
								{
									$tstatus = 'Canceled';
									$cstatus = 'Canceled';
								}
								if($status == 'confirmed')
								{
									$tstatus = 'Credited';
									$cstatus = 'Completed';
								}

								/*$data = array('transation_status' => $tstatus); 
								$this->db->where('report_update_id',$report_update_id);
								$updation = $this->db->update('transation_details',$data);*/
								 
								
								if($status !='')
								{
									
									if($tstatus == 'Approved' || $tstatus = 'Credited')
									{
										if($site_status == 1)
										{
											$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
											VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");
										}
									}
									if($tstatus == 'Canceled')
									{
										$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
										VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Canceled')");
									}

									/*New code for mail notification details 1-3-17*/
									$userdetails 	 = $this->db->query("SELECT * from tbl_users where user_id=$get_userid")->row();
									$cashback_status = $userdetails->cashback_mail;
									$user_email      = $userdetails->email;
									$first_name      = $userdetails->first_name;
									$last_name       = $userdetails->last_name;
									$balance  		 = $userdetails->balance;
									
									if($first_name == '' && $last_name == '')
									{
										$ex_name   = explode('@', $user_email);
										$user_name = $ex_name[0]; 
									}
									else
									{
										$user_name = $first_name.''.$last_name;
									}

									$this->db->where('admin_id',1);
									$admin_det = $this->db->get('admin');
									if($admin_det->num_rows >0) 
									{    
										$admin 		 = $admin_det->row();
										$admin_email = $admin->admin_email;
										$site_name 	 = $admin->site_name;
										$admin_no	 = $admin->contact_number;
										$site_logo 	 = $admin->site_logo;
									}
									$date = date('Y-m-d');
									/*end 1-3-17*/
									
									if($cstatus == 'Completed')
									{
										if($site_status == 0)
										{
											$cstatus = 'Pending';
										}
									}	

									$data = array('status' => $cstatus); 
									$this->db->where('report_update_id',$report_update_id);
									$updation = $this->db->update('cashback',$data);



									if($cstatus == 'Completed')
									{
										if($site_status == 1)
										{
											$this->db->where('user_id',$get_userid);
											$this->db->update('tbl_users',array('balance'=>$prev_userbal+$transaction_amount));
											
											/*mail for Completed cashback 1-3-17*/
											if($cashback_status == 1)
											{
												$this->db->where('mail_id',8);
												$mail_template = $this->db->get('tbl_mailtemplates');
												if($mail_template->num_rows >0) 
												{
												   	$fetch     = $mail_template->row();
												   	$subject   = $fetch->email_subject;
												   	$templete  = $fetch->email_template;
												   	$url 	   = base_url().'my_earnings/';
												   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
												   	$myaccount = base_url().'minha_conta';
												   
												  	$this->load->library('email');
													
													$config = Array(
														'mailtype'  => 'html',
														'charset'   => 'utf-8',
													);
													
													$sub_data = array(
														'###SITENAME###'=>$site_name
													);

													$subject_new = strtr($subject,$sub_data);
													
													//$this->email->initialize($config);
													$this->email->set_newline("\r\n");
													$this->email->initialize($config);
													$this->email->from($admin_email, $site_name.'!');
													$this->email->to($user_email);
													$this->email->subject($subject_new);
												   
													$data = array(
														'###NAME###'=>$user_name,
														'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
														'###SITENAME###'=>$site_name,
														'###ADMINNO###'=>$admin_no,
														'###DATE###'=>$date,
														'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
														'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
														'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

												    );
												   
												   $content_pop=strtr($templete,$data);
												   $this->email->message($content_pop);
												   $this->email->send();  
												}
											} 
											/*end 1-3-17*/

											/*New code for referral mail and user cashback balance details 03-5-17*/
											$this->db->where('report_update_id',$report_update_id);
											$cashbacks 	   = $this->db->get('cashback');
											$cashback_data = $cashbacks->row();

											if($cashback_data->referral!=0)
											{
												$this->db->where('trans_id',$cashback_data->txn_id);
												$txn = $this->db->get('transation_details');
												$txn_detail = $txn->row();

												if($txn_detail)
												{
													$txn_id 	 	    = $txn_detail->trans_id;
													$ref_user_id 	    = $txn_detail->user_id;
													$transation_amounts = $txn_detail->transation_amount;
													$refer_user 	    = $this->view_user($ref_user_id);
													
													if($refer_user)
													{
														foreach($refer_user as $single)
														{
															$referral_balance = $single->balance;
															$user_email 	  = $single->email;
															$first_name 	  = $single->first_name;
															$last_name 		  = $single->last_name;

															if($first_name == '' && $last_name == '')
															{
																$ex_name   = explode('@', $user_email);
																$user_name = $ex_name[0]; 
															}
															else
															{
																$user_name = $first_name.' '.$last_name;
															}
														}

														//Update refer user balance details in User table and transaction table(approve referral cashback amount)//
														$this->db->where('user_id',$ref_user_id);
														$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amounts));
														 

														$data = array('transation_status'=>'Approved','transation_reason'=>'Referal Payment'); //'transaction_date'=>$date,
														//$this->db->where('transation_reason','Pending Referal Payment');
														$this->db->where('trans_id',$cashback_data->txn_id);
														$this->db->update('transation_details',$data);

														/* mail for Approve cashback amt mail notifications */
														if($single->referral_mail == 1)
														{	
															$this->db->where('mail_id',9);
															$mail_template = $this->db->get('tbl_mailtemplates');
															if($mail_template->num_rows >0) 
															{
															   $fetch 	  = $mail_template->row();
															   $subject   = $fetch->email_subject;
															   $templete  = $fetch->email_template;
															   $url 	  = base_url().'my_earnings/';
															   $unsuburls = base_url().'un-subscribe/referral/'.$ref_user_id;
												   			   $myaccount = base_url().'minha_conta';
															   
																$this->load->library('email');
																
																$config = Array(
																	'mailtype'  => 'html',
																	'charset'   => 'utf-8',
																);
																
																$sub_data = array(
																	'###SITENAME###'=>$site_name
																);
																$subject_new = strtr($subject,$sub_data);
																
																// $this->email->initialize($config);
																$this->email->set_newline("\r\n");
																$this->email->initialize($config);
																$this->email->from($admin_email,$site_name.'!');
																$this->email->to($user_email);
																$this->email->subject($subject_new);
															   
																$datas = array(
																	'###NAME###'=>$user_name,
																	'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
																	'###SITENAME###'=>$site_name,
																	'###ADMINNO###'=>$admin_no,
																	'###DATE###'=>$date,
																	'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amounts,1,2)),
																	'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
																	'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
																);
															   
															   $content_pop=strtr($templete,$datas);
															   $this->email->message($content_pop);
															   $this->email->send();  
															}
														}	
														/* Mail for Approve referral cashback amount mail End*/
													}
												}
											}
											/*end 3-5-17*/

											/*first and second Withdraw mail notification for referral users 3-5-17*/
											$this->db->where('admin_id',1);
											$Admin_Details_Query    = $this->db->get('admin');
											$Admin_Details 		    = $Admin_Details_Query->row();
											$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
											$remain_minimum_amt     = $Admin_Details->remain_minimum_with_amt;
											$Site_Logo 				= $Admin_Details->site_logo;
											$admin_emailid 			= $Admin_Details->admin_email;

											if($ref_user_id!='')
											{

												$this->db->where('report_update_id',$report_update_id);
												$cashbacks 	   = $this->db->get('cashback');
												$cashback_data = $cashbacks->row();

												if($cashback_data->referral!=0)
												{
													$this->db->where('trans_id',$cashback_data->txn_id);
													$txn = $this->db->get('transation_details');
													$txn_detail = $txn->row();
													
													if($txn_detail)
													{
														//$new_txn_ids = $txn_detail->new_txn_id;
														$new_txn_ids   = $txn_detail->trans_id;
													
														if($single->referral_category_type != '')
														{ 	
															$referrals    	    = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
															$ref_by_percentage  = $referrals->ref_by_percentage;
															$ref_by_rate 		= $referrals->ref_by_rate;
															$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

															//3** Bonus by Refferal Rate type//
															if($bonus_by_ref_rate == 1)
															{
																$n9  	 = '333445';
																$n12 	 = $n9 + $ref_user_id;
																$now 	 = date('Y-m-d H:i:s');	
																$selqry  = "SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Referal Payment' AND user_id=$ref_user_id"; 
														 		$query   = $this->db->query("$selqry");
																$numrows = $query->num_rows();
																if($numrows > 0)
																{
																	$fetch 		   = $query->row();
																	$usercount 	   = $fetch->userid;
																	$referrals     = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
																	$bonus_amount  = $referrals->ref_cashback_rate_bonus;
																	$friends_count = $referrals->friends_count;
																	
																	if($usercount == $friends_count)
																	{	
																		if($bonus_amount!='')
																		{	
																			if($single->referral_category_type == 1)
																			{ 
																			 	$types = 'One';
																			}
																			if($single->referral_category_type == 2)
																			{ 
																			 	$types = 'Two';
																			}
																			if($single->referral_category_type == 3)
																			{ 
																			 	$types = 'Three';
																			}
																			if($single->referral_category_type == 4)
																			{ 
																			 	$types = 'Four';
																			}
																			if($single->referral_category_type == 5)
																			{ 
																			 	$types = 'Five';
																			}
																			if($single->referral_category_type == 6)
																			{ 
																			 	$types = 'Six';
																			}
																			if($single->referral_category_type == 7)
																			{ 
																			 	$types = 'Seven';
																			}
																			if($single->referral_category_type == 8)
																			{ 
																			 	$types = 'Eight';
																			}
																			if($single->referral_category_type == 9)
																			{ 
																			 	$types = 'Nine';
																			}
																			if($single->referral_category_type == 10)
																			{ 
																			 	$types = 'Ten';
																			}

																			$selqry	= "SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category $types User' AND `user_id`=$ref_user_id"; 
															 				$query1	= $this->db->query("$selqry");
															 				$newnumrows = $query1->num_rows();
																			if($newnumrows > 0)
																			{
																				$fetch 		 = $query1->row();
																				$users_count = $fetch->userid;
																				if($users_count == 0)	
																				{	
																					$data = array(			
																					'transation_amount' => $bonus_amount,	
																					'user_id' => $ref_user_id,	
																					'transation_date' => $now,
																					'transaction_date' => $now,
																					'transation_id'=>$n12,	
																					'transation_reason' => 'Referral Bonus for Category ' .$types. ' User',	
																					'mode' => 'Credited',
																					'details_id'=>'',	
																					'table'=>'',	
																					'new_txn_id'=>0,
																					'transation_status ' => 'Approved',
																					'report_update_id'=>$newtransaction_id
																					);	
																					$this->db->insert('transation_details',$data);
																				}	
																			}
																		}	
																	}
																} 
															}
															//3** Bonus by Refferal Rate type End//

															//1** Refferal by Percentage type Start//
															if($ref_by_percentage == 1)
															{
																$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); /*,'transaction_date'=>$date*/
																$this->db->where('trans_id',$new_txn_ids);
																$this->db->update('transation_details',$data);
															}
															//1** Refferal by Percentage type End//	
														}	
													}
												}

												/*New code for withdraw notification 1-4-17*/
												$ref_prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$ref_user_id))->row('balance');
												if($ref_prev_userbal == '')
												{
													$ref_prev_userbal = 0;	
												}


												//$ref_Total_Amount = ($ref_balance+$ref_cashback_amount);
												$ref_Total_Amount 	= ($referral_balance+$transation_amounts);
												$ref_User_details 	= $this->view_user($ref_user_id);
												$ref_us_email 	  	= $ref_User_details[0]->email;
												$ref_with_status 	= $ref_User_details[0]->withdraw_mail;
												$ref_unsuburl	  	= base_url().'un-subscribe/withdraw/'.$ref_user_id;
												$ref_myaccount    	= base_url().'resgate';
												$ref_firstname 	  	= $ref_User_details[0]->first_name;
												$ref_lastname  	  	= $ref_User_details[0]->last_name;
												$with_mail_status 	= $ref_User_details[0]->first_withdraw_mail_status;
												$first_with_status  = $ref_User_details[0]->first_withdraw_status;
												$second_with_status = $ref_User_details[0]->second_withdraw_status;

												if($with_mail_status == 1)
												{
													$Admin_Minimum_Cashback = $remain_minimum_amt;
													$new_withdraw_status    = $second_with_status;
												}
												else
												{
													$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;	
													$new_withdraw_status    = $first_with_status;
												}

												if($ref_firstname == '' && $ref_lastname == '')
												{
													$ex_name  	  = explode('@', $ref_User_details[0]->email);
													$ref_username = $ex_name[0]; 
												}
												else
												{
													$ref_username = $ref_firstname.' '.$ref_lastname;
												}	
												/*End 9-1-17*/
												 
												if($ref_with_status == 1)
												{
													if($new_withdraw_status == 0)
													{
														if($ref_Total_Amount>=$Admin_Minimum_Cashback)
														{
															$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
															if($obj_temp->num_rows>0)
															{
																$mail_temp  = $obj_temp->row(); 
																$fe_cont    = $mail_temp->email_template;	
																$subject  	= $mail_temp->email_subject;	
																$servername = base_url();
																$nows 		= date('Y-m-d');	
																$this->load->library('email');
																$gd_api=array(
																	
																	'###NAME###'=>$ref_username,
																	'###AMOUNT###'=>str_replace('.', ',', bcdiv($ref_Total_Amount,1,2)),
																	'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
																	'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
																	'###ULINK###'=>'<a href='.$ref_unsuburl.'>'.$ref_unsuburl.'</a>',
																	'###MYACLINK###'=>'<a href='.$ref_myaccount.'>'.$ref_myaccount.'</a>'
																	);
																				   
																$gd_message=strtr($fe_cont,$gd_api);
																$config = Array(
																	'mailtype'  => 'html',
																	'charset'   => 'utf-8',
																);

																$this->email->set_newline("\r\n");
																$this->email->initialize($config);
																$this->email->from($admin_emailid,$site_name.'!');
																$this->email->to($ref_us_email);
																$this->email->subject($subject);
																$this->email->message($gd_message);
																$this->email->send();
																$this->email->print_debugger();

																/*new code for update a first or second withdraw amount status 20-5-17*/
																if($new_withdraw_status == 0)
																{
																	if($with_mail_status == 1)
																	{
																		$data = array(		
																		'second_withdraw_status'  => 1);
																		$this->db->where('user_id',$ref_user_id);
																		$update_qry= $this->db->update('tbl_users',$data);
																	}
																	else
																	{
																		$data = array(		
																		'first_withdraw_status'  => 1);
																		$this->db->where('user_id',$ref_user_id);
																		$update_qry= $this->db->update('tbl_users',$data);
																	}	
																}
																/*End*/
															}
														}
													}	
												}
												 
												/*end 1-4-17*/		
											}
											/*End mail notification for referral users 3-5-17*/

											/*first and second withdraw mail notification for refered user 3-5-17*/
											$Total_Amount 	  	= ($balance+$transaction_amount);
											$User_details 	  	= $this->view_user($get_userid);
											$us_email 		  	= $User_details[0]->email;
											$with_status      	= $User_details[0]->withdraw_mail;
											$firstname 		  	= $User_details[0]->first_name;
											$lastname  		  	= $User_details[0]->last_name;
											$with_mail_status 	= $User_details[0]->first_withdraw_mail_status;
											$unsuburl	 	  	= base_url().'un-subscribe/withdraw/'.$get_userid;
											$myaccount    	  	= base_url().'resgate';
											$first_with_status  = $User_details[0]->first_withdraw_status;
											$second_with_status = $User_details[0]->second_withdraw_status;

											if($with_mail_status == 1)
											{
												//$Admin_Minimum_Cashback = $remain_minimum_amt;
												$Admin_Minimum_Cashback = $remain_minimum_amt;
												$new_withdraw_status    = $second_with_status;
											}
											else
											{
												$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;
												$new_withdraw_status    = $first_with_status;	
											}

											if($firstname == '' && $lastname == '')
											{
												$ex_name  = explode('@', $User_details[0]->email);
												$username = $ex_name[0]; 
											}
											else
											{
												$username = $firstname.' '.$lastname;
											}	
											/*End 9-1-17*/
											
											 
											if($with_status == 1)
											{
												if($new_withdraw_status == 0)
												{
													if($Total_Amount>=$Admin_Minimum_Cashback)
													{
														$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
														if($obj_temp->num_rows>0)
														{
															$mail_temp  = $obj_temp->row(); 
															$fe_cont    = $mail_temp->email_template;	
															$subject  	= $mail_temp->email_subject;		
															$servername = base_url();
															$nows 		= date('Y-m-d');	
															$this->load->library('email');
															$gd_api=array(
																
																'###NAME###'=>$username,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($Total_Amount,1,2)),
																'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
																'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
																'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
																);
																			   
															$gd_message=strtr($fe_cont,$gd_api);
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);
															 
															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_emailid,$site_name.'!');
															$this->email->to($us_email);
															$this->email->subject($subject);
															$this->email->message($gd_message);
															$this->email->send();
															$this->email->print_debugger();

															/*new code for update a first or second withdraw amount status 20-5-17*/
															if($new_withdraw_status == 0)
															{
																if($with_mail_status == 1)
																{
																	$data = array(		
																	'second_withdraw_status'  => 1);
																	$this->db->where('user_id',$get_userid);
																	$update_qry= $this->db->update('tbl_users',$data);
																}
																else
																{
																	$data = array(		
																	'first_withdraw_status'  => 1);
																	$this->db->where('user_id',$get_userid);
																	$update_qry= $this->db->update('tbl_users',$data);
																}	
															}
															/*End*/
														}
													}
												}	
											}	
											 	
											/*End 9-1-17*/
											/*End mail notification for referred user 3-5-17*/
											/*End 3-5-17*/
										}
									}

									if($cstatus == 'Canceled')
									{
										/*mail for Canceled cashback 1-3-17*/
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',11);
											$mail_template = $this->db->get('tbl_mailtemplates');
											if($mail_template->num_rows >0) 
											{

											   	$fetch 	   = $mail_template->row();
											   	$subject   = $fetch->email_subject;
											   	$templete  = $fetch->email_template;
											   	$url 	   = base_url().'cashback/my_earnings/';
											   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
											   	$myaccount = base_url().'minha_conta';
											   
												$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);
												$subject_new = strtr($subject,$sub_data);
												
												//$this->email->initialize($config);
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email,$site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
											    );
											   
											   $content_pop=strtr($templete,$data);
											   $this->email->message($content_pop);
											   $this->email->send();  
											}
										} 
										/*end 1-3-17*/ 

										/*New code for update user table 4-5-17*/
										if($cash_status =='Completed' && $cstatus == 'Canceled')
										{
											$user_bale 		= $balance;
											$newbalnce 		= $user_bale - $transaction_amount;
												
											$data = array(		
											'balance' => $newbalnce);
											$this->db->where('user_id',$get_userid);
											$update_qry = $this->db->update('tbl_users',$data);

											/*new code 26-7-17*/
											$this->db->delete('transation_details',array('report_update_id' => $report_update_id,'transation_reason'=>'Cashback','user_id'=>$get_userid));
											/*End 26-7-17*/
										}
										/*End 4-5-17*/

										/*Mail for cancelled referral amount for reffered user mail 04-5-17*/
										$this->db->where('report_update_id',$report_update_id);
										$cashbacks 	   = $this->db->get('cashback');
										$cashback_data = $cashbacks->row();
										if($cashback_data->referral!=0)
										{
											$this->db->where('trans_id',$cashback_data->txn_id);
											$txn 		= $this->db->get('transation_details');
											$txn_detail = $txn->row();
											
											if($txn_detail)
											{
												$txn_id 	 	   = $txn_detail->trans_id;
												$ref_user_id 	   = $txn_detail->user_id;
												$transation_amount = $txn_detail->transation_amount;
												$newtrans_status   = $txn_detail->transation_status; 
												$refer_user 	   = $this->view_user($ref_user_id);

												if($refer_user)
												{
													foreach($refer_user as $single)
													{
														$referral_balance = $single->balance;
														$user_email 	  = $single->email;
														$first_name 	  = $single->first_name;
														$last_name 		  = $single->last_name;
														$reffer_status    = $single->referral_mail;

														if($first_name == '' && $last_name == '')
														{
															$ex_name   = explode('@', $user_email);
															$user_name = $ex_name[0]; 
														}
														else
														{
															$user_name = $first_name.' '.$last_name;
														}
													}

													if(($status == 'Canceled') && ($newtrans_status == 'Approved'))
													{
														$new_balance = ($referral_balance - $transation_amount);
														$this->db->where('user_id',$ref_user_id);
														$this->db->update('tbl_users',array('balance'=>$new_balance));
													}

													$data = array(	
													'transation_status'=>$status
													);
													$this->db->where('report_update_id',$report_update_id);
													$updation = $this->db->update('transation_details',$data);
														
													$unsuburl  = base_url().'un-subscribe/referral/'.$ref_user_id;
												    $myaccount = base_url().'minha-conta';

													$this->db->where('admin_id',1);
													$admin_det = $this->db->get('admin');
													if($admin_det->num_rows >0) 
													{    
														$admin 		 = $admin_det->row();
														$admin_email = $admin->admin_email;
														$site_name 	 = $admin->site_name;
														$admin_no 	 = $admin->contact_number;
														$site_logo 	 = $admin->site_logo;
													}

													if($reffer_status == 1)
													{
														$this->db->where('mail_id',19);
														$mail_template = $this->db->get('tbl_mailtemplates');
														if($mail_template->num_rows >0) 
														{
														   $fetch = $mail_template->row();
														   $subject = $fetch->email_subject;
														   $templete = $fetch->email_template;
															
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);
															
															// $this->email->initialize($config);
															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_email,$site_name.'!');
															$this->email->to($user_email);
															$this->email->subject($subject);
														   
															$data = array(
																'###ADMINNO###'=>$admin_no,
																'###EMAIL###'=>$user_name,
																'###DATE###'=>$date,
																'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
																'###SITENAME###' =>$site_name,
																'###STATUS###'=>$status,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amount,1,2)),
																'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
														    );
														   
														   $content_pop=strtr($templete,$data);
														   $this->email->message($content_pop);
														   $this->email->send();  
														}
													}
												}
												/*end*/
											}	
										}
										/*End 4-5-17*/
									}

									if($cstatus == 'Pending')
									{
										/*if($site_status == '')
										{
											//New code for update user balance details 4-5-17
											if($cash_status =='Completed') 
											{	
												$user_bale 		= $balance;
												$newbalnce 		= $user_bale - $transaction_amount;
													
												$data = array(		
												'balance' => $newbalnce);
												$this->db->where('user_id',$get_userid);
												$update_qry = $this->db->update('tbl_users',$data);
											}
											//end 4-5-17

											//mail for Pending cashback 1-3-17
											if($cashback_status == 1)
											{
												$this->db->where('mail_id',10);
												$mail_template = $this->db->get('tbl_mailtemplates');
												
												if($mail_template->num_rows >0) 
												{
												    $fetch     = $mail_template->row();
												    $subject   = $fetch->email_subject;
												    $templete  = $fetch->email_template;
												    $unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
												    $myaccount = base_url().'minha_conta';
												   
													$this->load->library('email');
													
													$config = Array(
														'mailtype'  => 'html',
														'charset'   => 'utf-8',
													);
													
													$sub_data = array(
														'###SITENAME###'=>$site_name
													);
													$subject_new = strtr($subject,$sub_data);
													
													$this->email->set_newline("\r\n");
													$this->email->initialize($config);
													$this->email->from($admin_email,$site_name.'!');
													$this->email->to($user_email);
													$this->email->subject($subject_new);
												   
													$data = array(
														'###NAME###'=>$user_name,
														'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
														'###SITENAME###'=>$site_name,
														'###ADMINNO###'=>$admin_no,
														'###DATE###'=>$date,
														'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
														'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
														'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
												    );
												   
												    $content_pop=strtr($templete,$data);
												   	$this->email->message($content_pop);
												   	$this->email->send();  
												}
											} 
											//end 1-3-17	

											//New code for pending referral mail for (REFER) User 4-5-17
											$check_ref    = $this->check_ref_user($get_userid);

											if($check_ref > 0)		
											{
												$ref_id  	  = $check_ref;
												$ref_user_bal = $this->view_balance($check_ref);
												$return  	  = $this->check_active_user($ref_id);

												if($return)
												{
													if($cash_status  == 'Completed')
													{	
														$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Approved'")->row();	
													}
													if($cash_status  == 'Canceled')
													{
														$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Canceled'")->row();
													}
													if($cash_status  == 'Pending')
													{
														$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Pending'")->row();
													}
													 
												 										 
												 	$transation_amount = $ref_trans_details->transation_amount;

													foreach($return as $newreturn)
													{
														$referral_balance   = $newreturn->balance; 
														$user_referral_mail = $newreturn->referral_mail;
														$ref_user_email     = $newreturn->email;
														$ref_first_name   	= $newreturn->first_name;
														$ref_last_name 	  	= $newreturn->last_name;								

														if($ref_first_name == '' && $ref_last_name == '')
														{
															$ex_name       = explode('@', $ref_user_email);
															$ref_user_name = $ex_name[0]; 
														}
														else
														{
															$ref_user_name = $ref_first_name.' '.$ref_last_name;
														}
													}

													if($cash_status  == 'Completed')
													{
														$bal_ref = $referral_balance - $transation_amount;
														$this->db->where('user_id',$ref_id);
														$this->db->update('tbl_users',array('balance'=>$bal_ref));
													}

													$data = array('transation_status'=>'Pending'); 
													$this->db->where('report_update_id',$report_update_id);
													$this->db->where('ref_user_tracking_id',$get_userid);
													$this->db->update('transation_details',$data);	
													 
													if($user_referral_mail == 1)
													{
														$this->db->where('mail_id',20);
														$mail_template = $this->db->get('tbl_mailtemplates');
														if($mail_template->num_rows >0) 
														{
															$fetch     = $mail_template->row();
															$subject   = $fetch->email_subject;
															$templete  = $fetch->email_template;
															$url 	   = base_url().'my_earnings/';
															$unsuburls = base_url().'un-subscribe/referral/'.$ref_id;
													   		$myaccount = base_url().'minha-conta';
															
															$this->load->library('email');

															$config    = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
															);
																	
															$sub_data = array(
															'###SITENAME###'=>$site_name
															);
															
															$subject_new = strtr($subject,$sub_data);
															// $this->email->initialize($config);
															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_email,$site_name.'!');
															$this->email->to($ref_user_email);
															$this->email->subject($subject_new);

															//echo $transation_amount; exit;											
															$datas = array(
															'###NAME###'=>$ref_user_name,
															'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
															'###SITENAME###'=>$site_name,
															'###ADMINNO###'=>$admin_no,
															'###DATE###'=>date('y-m-d'),
															'###AMOUNT###'=>str_replace('.',',',bcdiv($transation_amount,1,2)),
															'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
															'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
															);
															$content_pop=strtr($templete,$datas);
															$this->email->message($content_pop);
															$this->email->send();  
														}
													}
												}
											}
											//End 4-5-17
										}*/	
									}									 
								}
							}
							else
							{
								$duplicate = 0;
							}		
						}	
					} 
					else
					{
						//echo "hai".$content['saleItems']['saleItem']['clickId']; exit;
						$all=0;
						$transaction_id   = $content['saleItems']['saleItem']['@attributes']['id'];
						$report_update_id = $content['saleItems']['saleItem']['@attributes']['id'];
						

						$this->db->where('report_update_id',$report_update_id);
						$all   = $this->db->get('cashback'); //->num_rows();				 	  	 
						$fetch = $all->row();				 	  	 
						
						if(!empty($fetch))
						{

							$get_userid 		= $fetch->user_id;
							$transaction_amount = $fetch->cashback_amount;
							$duplicate = 1;
							
							/*
							$user_id      = $content['saleItems']['saleItem']['gpps']['gpp'][0]; 
							$val_user_id  = substr($user_id, 0, 5); 
							if($val_user_id == 'P0001')
							{$get_userid = decode_userid($user_id); }else{ $get_userid = $user_id;}
							*/   
							/*
							$currency_type    = $content['saleItems']['saleItem']['currency'];
							if($currency_type != 'BRL')
							{
								$amount   			= $content['saleItems']['saleItem']['amount'];
								$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
							}	
							else
							{
								$transaction_amount = $content['saleItems']['saleItem']['amount'];	
							}
							*/

							$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$get_userid))->row('balance');
							if($prev_userbal == '')
							{
								$prev_userbal = 0;	
							}

							$status = $content['saleItems']['saleItem']['reviewState'];

							if($status == 'open')
							{
								$tstatus = 'Pending';
								$cstatus = 'Pending';
							}
							if($status == 'approved')
							{
								$tstatus = 'Approved';
								$cstatus = 'Completed';
							}
							if($status == 'rejected')
							{
								$tstatus = 'Canceled';
								$cstatus = 'Canceled';
							}
							if($status == 'confirmed')
							{
								$tstatus = 'Credited';
								$cstatus = 'Completed';
							}							
							
							if($status !='')
							{
								if($tstatus == 'Approved' || $tstatus = 'Credited')
								{
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");
								}

								if($tstatus == 'Canceled')
								{
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Canceled')");
								}

								/*New code for mail notification details 1-3-17*/
								$userdetails 	 = $this->db->query("SELECT * from tbl_users where user_id=$get_userid")->row();
								$cashback_status = $userdetails->cashback_mail;
								$user_email      = $userdetails->email;
								$first_name      = $userdetails->first_name;
								$last_name       = $userdetails->last_name;
								$balance  		 = $userdetails->balance;
								
								if($first_name == '' && $last_name == '')
								{
									$ex_name   = explode('@', $user_email);
									$user_name = $ex_name[0]; 
								}
								else
								{
									$user_name = $first_name.''.$last_name;
								}

								$this->db->where('admin_id',1);
								$admin_det = $this->db->get('admin');
								if($admin_det->num_rows >0) 
								{    
									$admin 		 = $admin_det->row();
									$admin_email = $admin->admin_email;
									$site_name 	 = $admin->site_name;
									$admin_no	 = $admin->contact_number;
									$site_logo 	 = $admin->site_logo;
								}

								$date = date('Y-m-d');
								/*end 1-3-17*/

								if($cstatus == 'Completed')
								{
									if($site_status == 0)
									{
										$cstatus = 'Pending';
									}
								}	

								$data = array('status' => $cstatus); 
								$this->db->where('report_update_id',$report_update_id);
								$updation = $this->db->update('cashback',$data);

								if($cstatus == 'Completed')
								{
									$this->db->where('user_id',$get_userid);
									$this->db->update('tbl_users',array('balance'=>$prev_userbal+$transaction_amount));
									
									/*mail for Completed cashback 1-3-17*/
									if($cashback_status == 1)
									{
										$this->db->where('mail_id',8);
										$mail_template = $this->db->get('tbl_mailtemplates');
										if($mail_template->num_rows >0) 
										{
										   	$fetch     = $mail_template->row();
										   	$subject   = $fetch->email_subject;
										   	$templete  = $fetch->email_template;
										   	$url 	  = base_url().'my_earnings/';
										   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
										   	$myaccount = base_url().'minha_conta';
										   
										  	$this->load->library('email');
											
											$config = Array(
												'mailtype'  => 'html',
												'charset'   => 'utf-8',
											);
											
											$sub_data = array(
												'###SITENAME###'=>$site_name
											);

											$subject_new = strtr($subject,$sub_data);
											
											//$this->email->initialize($config);
											$this->email->set_newline("\r\n");
											$this->email->initialize($config);
											$this->email->from($admin_email, $site_name.'!');
											$this->email->to($user_email);
											$this->email->subject($subject_new);
										   
											$data = array(
												'###NAME###'=>$user_name,
												'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
												'###SITENAME###'=>$site_name,
												'###ADMINNO###'=>$admin_no,
												'###DATE###'=>$date,
												'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
												'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
												'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

										    );
										   
										   $content_pop=strtr($templete,$data);
										   $this->email->message($content_pop);
										   $this->email->send();  
										}
									} 
									/*end 1-3-17*/

									/*New code for referral mail and user cashback balance details 04-5-17*/
									$this->db->where('report_update_id',$report_update_id);
									$cashbacks 	   = $this->db->get('cashback');
									$cashback_data = $cashbacks->row();

									if($cashback_data->referral!=0)
									{
										$this->db->where('trans_id',$cashback_data->txn_id);
										$txn = $this->db->get('transation_details');
										$txn_detail = $txn->row();

										if($txn_detail)
										{
											$txn_id 	 	    = $txn_detail->trans_id;
											$ref_user_id 	    = $txn_detail->user_id;
											$transation_amounts = $txn_detail->transation_amount;
											$refer_user 	    = $this->view_user($ref_user_id);
											
											if($refer_user)
											{
												foreach($refer_user as $single)
												{
													$referral_balance = $single->balance;
													$user_email 	  = $single->email;
													$first_name 	  = $single->first_name;
													$last_name 		  = $single->last_name;

													if($first_name == '' && $last_name == '')
													{
														$ex_name   = explode('@', $user_email);
														$user_name = $ex_name[0]; 
													}
													else
													{
														$user_name = $first_name.' '.$last_name;
													}
												}

												//Update refer user balance details in User table and transaction table(approve referral cashback amount)//
												$this->db->where('user_id',$ref_user_id);
												$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amounts));
												 

												$data = array('transation_status'=>'Approved','transation_reason'=>'Referal Payment'); //'transaction_date'=>$date,
												//$this->db->where('transation_reason','Pending Referal Payment');
												$this->db->where('trans_id',$cashback_data->txn_id);
												$this->db->update('transation_details',$data);

												/* mail for Approve cashback amt mail notifications */
												if($single->referral_mail == 1)
												{	
													$this->db->where('mail_id',9);
													$mail_template = $this->db->get('tbl_mailtemplates');
													if($mail_template->num_rows >0) 
													{
													   $fetch 	  = $mail_template->row();
													   $subject   = $fetch->email_subject;
													   $templete  = $fetch->email_template;
													   $url 	  = base_url().'my_earnings/';
													   $unsuburls = base_url().'un-subscribe/referral/'.$ref_user_id;
										   			   $myaccount = base_url().'minha_conta';
													   
														$this->load->library('email');
														
														$config = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
														);
														
														$sub_data = array(
															'###SITENAME###'=>$site_name
														);
														$subject_new = strtr($subject,$sub_data);
														
														// $this->email->initialize($config);
														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_email,$site_name.'!');
														$this->email->to($user_email);
														$this->email->subject($subject_new);
													   
														$datas = array(
															'###NAME###'=>$user_name,
															'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
															'###SITENAME###'=>$site_name,
															'###ADMINNO###'=>$admin_no,
															'###DATE###'=>$date,
															'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amounts,1,2)),
															'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
															'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
														);
													   
													   $content_pop=strtr($templete,$datas);
													   $this->email->message($content_pop);
													   $this->email->send();  
													}
												}	
												/* Mail for Approve referral cashback amount mail End*/
											}
										}
									}
									/*end 4-5-17*/

									/*first and second Withdraw mail notification for referral users 4-5-17*/
									$this->db->where('admin_id',1);
									$Admin_Details_Query    = $this->db->get('admin');
									$Admin_Details 		    = $Admin_Details_Query->row();
									$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
									$remain_minimum_amt     = $Admin_Details->remain_minimum_with_amt;
									$Site_Logo 				= $Admin_Details->site_logo;
									$admin_emailid 			= $Admin_Details->admin_email;

									if($ref_user_id!='')
									{

										$this->db->where('report_update_id',$report_update_id);
										$cashbacks 	   = $this->db->get('cashback');
										$cashback_data = $cashbacks->row();

										if($cashback_data->referral!=0)
										{
											$this->db->where('new_txn_id',$cashback_data->new_txn_id);
											$txn = $this->db->get('transation_details');
											$txn_detail = $txn->row();
											
											if($txn_detail)
											{
												$new_txn_ids = $txn_detail->new_txn_id;
										
												if($single->referral_category_type != '')
												{ 	
													$referrals    	    = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
													$ref_by_percentage  = $referrals->ref_by_percentage;
													$ref_by_rate 		= $referrals->ref_by_rate;
													$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

													//3** Bonus by Refferal Rate type//
													if($bonus_by_ref_rate == 1)
													{
														$n9  	 = '333445';
														$n12 	 = $n9 + $ref_user_id;
														$now 	 = date('Y-m-d H:i:s');	
														$selqry  = "SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Referal Payment' AND user_id=$ref_user_id"; 
												 		$query   = $this->db->query("$selqry");
														$numrows = $query->num_rows();
														if($numrows > 0)
														{
															$fetch 		   = $query->row();
															$usercount 	   = $fetch->userid;
															$referrals     = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
															$bonus_amount  = $referrals->ref_cashback_rate_bonus;
															$friends_count = $referrals->friends_count;
															
															if($usercount == $friends_count)
															{	
																if($bonus_amount!='')
																{	
																	if($single->referral_category_type == 1)
																	{ 
																	 	$types = 'One';
																	}
																	if($single->referral_category_type == 2)
																	{ 
																	 	$types = 'Two';
																	}
																	if($single->referral_category_type == 3)
																	{ 
																	 	$types = 'Three';
																	}
																	if($single->referral_category_type == 4)
																	{ 
																	 	$types = 'Four';
																	}
																	if($single->referral_category_type == 5)
																	{ 
																	 	$types = 'Five';
																	}
																	if($single->referral_category_type == 6)
																	{ 
																	 	$types = 'Six';
																	}
																	if($single->referral_category_type == 7)
																	{ 
																	 	$types = 'Seven';
																	}
																	if($single->referral_category_type == 8)
																	{ 
																	 	$types = 'Eight';
																	}
																	if($single->referral_category_type == 9)
																	{ 
																	 	$types = 'Nine';
																	}
																	if($single->referral_category_type == 10)
																	{ 
																	 	$types = 'Ten';
																	}

																	$selqry	= "SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category $types User' AND `user_id`=$ref_user_id"; 
													 				$query1	= $this->db->query("$selqry");
													 				$newnumrows = $query1->num_rows();
																	if($newnumrows > 0)
																	{
																		$fetch 		 = $query1->row();
																		$users_count = $fetch->userid;
																		if($users_count == 0)	
																		{	
																			$data = array(			
																			'transation_amount' => $bonus_amount,	
																			'user_id' => $ref_user_id,	
																			'transation_date' => $now,
																			'transaction_date' => $now,
																			'transation_id'=>$n12,	
																			'transation_reason' => 'Referral Bonus for Category ' .$types. ' User',	
																			'mode' => 'Credited',
																			'details_id'=>'',	
																			'table'=>'',	
																			'new_txn_id'=>0,
																			'transation_status ' => 'Approved',
																			'report_update_id'=>$newtransaction_id
																			);	
																			$this->db->insert('transation_details',$data);
																		}	
																	}
																}	
															}
														} 
													}
													//3** Bonus by Refferal Rate type End//

													//1** Refferal by Percentage type Start//
													if($ref_by_percentage == 1)
													{
														$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); /*,'transaction_date'=>$date*/
														$this->db->where('new_txn_id',$new_txn_ids);
														$this->db->update('transation_details',$data);
													}
													//1** Refferal by Percentage type End//	
												}	
											}
										}

										/*New code for withdraw notification 1-4-17*/
										$ref_prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$ref_user_id))->row('balance');
										if($ref_prev_userbal == '')
										{
											$ref_prev_userbal = 0;	
										}


										//$ref_Total_Amount = ($ref_balance+$ref_cashback_amount);
										$ref_Total_Amount 	= ($referral_balance+$transation_amounts);
										$ref_User_details 	= $this->view_user($ref_user_id);
										$ref_us_email 	  	= $ref_User_details[0]->email;
										$ref_with_status  	= $ref_User_details[0]->withdraw_mail;
										$ref_unsuburl	 	= base_url().'un-subscribe/withdraw/'.$ref_user_id;
										$ref_myaccount    	= base_url().'resgate';
										$ref_firstname 	  	= $ref_User_details[0]->first_name;
										$ref_lastname  	  	= $ref_User_details[0]->last_name;
										$with_mail_status 	= $ref_User_details[0]->first_withdraw_mail_status;
										$first_with_status  = $ref_User_details[0]->first_withdraw_status;
										$second_with_status = $ref_User_details[0]->second_withdraw_status;

										if($with_mail_status == 1)
										{
											$Admin_Minimum_Cashback = $remain_minimum_amt;
											$new_withdraw_status    = $second_with_status;
										}
										else
										{
											$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;
											$new_withdraw_status    = $first_with_status;	
										}

										if($ref_firstname == '' && $ref_lastname == '')
										{
											$ex_name  	  = explode('@', $ref_User_details[0]->email);
											$ref_username = $ex_name[0]; 
										}
										else
										{
											$ref_username = $ref_firstname.' '.$ref_lastname;
										}	
										/*End 9-1-17*/
										 
										if($ref_with_status == 1)
										{
											if($new_withdraw_status == 0)
											{
												if($ref_Total_Amount>=$Admin_Minimum_Cashback)
												{
													$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
													if($obj_temp->num_rows>0)
													{
														$mail_temp  = $obj_temp->row(); 
														$fe_cont    = $mail_temp->email_template;	
														$subject  	= $mail_temp->email_subject;	
														$servername = base_url();
														$nows 		= date('Y-m-d');	
														$this->load->library('email');
														$gd_api=array(
															
															'###NAME###'=>$ref_username,
															'###AMOUNT###'=>str_replace('.', ',', bcdiv($ref_Total_Amount,1,2)),
															'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
															'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
															'###ULINK###'=>'<a href='.$ref_unsuburl.'>'.$ref_unsuburl.'</a>',
															'###MYACLINK###'=>'<a href='.$ref_myaccount.'>'.$ref_myaccount.'</a>'
															);
																		   
														$gd_message=strtr($fe_cont,$gd_api);
														$config = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
														);

														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_emailid,$site_name.'!');
														$this->email->to($ref_us_email);
														$this->email->subject($subject);
														$this->email->message($gd_message);
														$this->email->send();
														$this->email->print_debugger();

														/*new code for update a first or second withdraw amount status 22-6-17*/
														if($new_withdraw_status == 0)
														{
															if($with_mail_status == 1)
															{
																$data = array(		
																'second_withdraw_status'  => 1);
																$this->db->where('user_id',$ref_user_id);
																$update_qry= $this->db->update('tbl_users',$data);
															}
															else
															{
																$data = array(		
																'first_withdraw_status'  => 1);
																$this->db->where('user_id',$ref_user_id);
																$update_qry= $this->db->update('tbl_users',$data);
															}	
														}
														/*End*/
													}
												}
											}	
										}
										 
										/*end 1-4-17*/		
									}
									/*End mail notification for referral users 4-5-17*/

									/*first and second withdraw mail notification for refered user 4-5-17*/
									$Total_Amount 	  	= ($balance+$transaction_amount);
									$User_details 	 	= $this->view_user($get_userid);
									$us_email 		  	= $User_details[0]->email;
									$with_status      	= $User_details[0]->withdraw_mail;
									$firstname 		  	= $User_details[0]->first_name;
									$lastname  		 	= $User_details[0]->last_name;
									$with_mail_status 	= $User_details[0]->first_withdraw_mail_status;

									$unsuburl	 	  	= base_url().'un-subscribe/withdraw/'.$get_userid;
									$myaccount    	  	= base_url().'resgate';
									$first_with_status  = $User_details[0]->first_withdraw_status;
									$second_with_status = $User_details[0]->second_withdraw_status;

									if($with_mail_status == 1)
									{
										$Admin_Minimum_Cashback = $remain_minimum_amt;
										$new_withdraw_status    = $second_with_status;
									}
									else
									{
										$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;
										$new_withdraw_status    = $first_with_status;	
									}

									if($firstname == '' && $lastname == '')
									{
										$ex_name  = explode('@', $User_details[0]->email);
										$username = $ex_name[0]; 
									}
									else
									{
										$username = $firstname.' '.$lastname;
									}	
									/*End 9-1-17*/

									if($with_status == 1)
									{
										if($new_withdraw_status == 0)
										{
											if($Total_Amount>=$Admin_Minimum_Cashback)
											{
												$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
												if($obj_temp->num_rows>0)
												{
													$mail_temp  = $obj_temp->row(); 
													$fe_cont    = $mail_temp->email_template;	
													$subject  	= $mail_temp->email_subject;		
													$servername = base_url();
													$nows 		= date('Y-m-d');	
													$this->load->library('email');
													$gd_api=array(
														
														'###NAME###'=>$username,
														'###AMOUNT###'=>str_replace('.', ',', bcdiv($Total_Amount,1,2)),
														'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
														'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
														'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
														'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
														);
																	   
													$gd_message=strtr($fe_cont,$gd_api);
													$config = Array(
														'mailtype'  => 'html',
														'charset'   => 'utf-8',
													);
													 
													$this->email->set_newline("\r\n");
													$this->email->initialize($config);
													$this->email->from($admin_emailid,$site_name.'!');
													$this->email->to($us_email);
													$this->email->subject($subject);
													$this->email->message($gd_message);
													$this->email->send();
													$this->email->print_debugger();

													/*new code for update a first or second withdraw amount status 23-6-17*/
													if($new_withdraw_status == 0)
													{
														if($with_mail_status == 1)
														{
															$data = array(		
															'second_withdraw_status'  => 1);
															$this->db->where('user_id',$get_userid);
															$update_qry= $this->db->update('tbl_users',$data);
														}
														else
														{
															$data = array(		
															'first_withdraw_status'  => 1);
															$this->db->where('user_id',$get_userid);
															$update_qry= $this->db->update('tbl_users',$data);
														}	
													}
													/*End*/
												}
											}
										}	
									}	
									 	
									/*End 9-1-17*/
									/*End mail notification for referred user 4-5-17*/
								}
								if($cstatus == 'Canceled')
								{
									/*mail for Canceled cashback 1-3-17*/
									if($cashback_status == 1)
									{
										$this->db->where('mail_id',11);
										$mail_template = $this->db->get('tbl_mailtemplates');
										if($mail_template->num_rows >0) 
										{

										   	$fetch 	   = $mail_template->row();
										   	$subject   = $fetch->email_subject;
										   	$templete  = $fetch->email_template;
										   	$url 	   = base_url().'cashback/my_earnings/';
										   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
										   	$myaccount = base_url().'minha_conta';
										   
											$this->load->library('email');
											
											$config = Array(
												'mailtype'  => 'html',
												'charset'   => 'utf-8',
											);
											
											$sub_data = array(
												'###SITENAME###'=>$site_name
											);
											$subject_new = strtr($subject,$sub_data);
											
											//$this->email->initialize($config);
											$this->email->set_newline("\r\n");
											$this->email->initialize($config);
											$this->email->from($admin_email,$site_name.'!');
											$this->email->to($user_email);
											$this->email->subject($subject_new);
										   
											$data = array(
												'###NAME###'=>$user_name,
												'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
												'###SITENAME###'=>$site_name,
												'###ADMINNO###'=>$admin_no,
												'###DATE###'=>$date,
												'###AMOUNT###'=>str_replace('.', ',', $transaction_amount),
												'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
												'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
										    );
										   
										   $content_pop=strtr($templete,$data);
										   //echo print_r($content_pop); exit;
										   // echo $content_pop; echo $subject_new;
										   $this->email->message($content_pop);
										   $this->email->send();  
										}
									} 
									/*end 1-3-17*/ 

									/*New code for update user table 4-5-17*/
									if($cash_status =='Completed' && $cstatus == 'Canceled')
									{
										$user_bale 		= $balance;
										$newbalnce 		= $user_bale - $transaction_amount;
											
										$data = array(		
										'balance' => $newbalnce);
										$this->db->where('user_id',$get_userid);
										$update_qry = $this->db->update('tbl_users',$data);

										/*new code 26-7-17*/
										$this->db->delete('transation_details',array('report_update_id' => $report_update_id,'transation_reason'=>'Cashback','user_id'=>$get_userid));
										/*End 26-7-17*/
									}
									/*End 4-5-17*/

									/*Mail for cancelled referral amount for reffered user mail 04-5-17*/
									$this->db->where('report_update_id',$report_update_id);
									$cashbacks 	   = $this->db->get('cashback');
									$cashback_data = $cashbacks->row();
									if($cashback_data->referral!=0)
									{
										$this->db->where('trans_id',$cashback_data->txn_id);
										$txn 		= $this->db->get('transation_details');
										$txn_detail = $txn->row();
										
										if($txn_detail)
										{
											$txn_id 	 	   = $txn_detail->trans_id;
											$ref_user_id 	   = $txn_detail->user_id;
											$transation_amount = $txn_detail->transation_amount;
											$newtrans_status   = $txn_detail->transation_status; 
											$refer_user 	   = $this->view_user($ref_user_id);

											if($refer_user)
											{
												foreach($refer_user as $single)
												{
													$referral_balance = $single->balance;
													$user_email 	  = $single->email;
													$first_name 	  = $single->first_name;
													$last_name 		  = $single->last_name;
													$reffer_status    = $single->referral_mail;

													if($first_name == '' && $last_name == '')
													{
														$ex_name   = explode('@', $user_email);
														$user_name = $ex_name[0]; 
													}
													else
													{
														$user_name = $first_name.' '.$last_name;
													}
												}

												if(($status == 'Canceled') && ($newtrans_status == 'Approved'))
												{
													$new_balance = ($referral_balance - $transation_amount);
													$this->db->where('user_id',$ref_user_id);
													$this->db->update('tbl_users',array('balance'=>$new_balance));
												}

												$data = array(	
												'transation_status'=>$status
												);
												$this->db->where('report_update_id',$report_update_id);
												$updation = $this->db->update('transation_details',$data);
													
												$unsuburl  = base_url().'un-subscribe/referral/'.$ref_user_id;
											    $myaccount = base_url().'minha-conta';

												$this->db->where('admin_id',1);
												$admin_det = $this->db->get('admin');
												if($admin_det->num_rows >0) 
												{    
													$admin 		 = $admin_det->row();
													$admin_email = $admin->admin_email;
													$site_name 	 = $admin->site_name;
													$admin_no 	 = $admin->contact_number;
													$site_logo 	 = $admin->site_logo;
												}

												if($reffer_status == 1)
												{
													$this->db->where('mail_id',19);
													$mail_template = $this->db->get('tbl_mailtemplates');
													if($mail_template->num_rows >0) 
													{
													   $fetch = $mail_template->row();
													   $subject = $fetch->email_subject;
													   $templete = $fetch->email_template;
														
														$config = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
														);
														
														// $this->email->initialize($config);
														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_email,$site_name.'!');
														$this->email->to($user_email);
														$this->email->subject($subject);
													   
														$data = array(
															'###ADMINNO###'=>$admin_no,
															'###EMAIL###'=>$user_name,
															'###DATE###'=>$date,
															'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
															'###SITENAME###' =>$site_name,
															'###STATUS###'=>$status,
															'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amount,1,2)),
															'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
															'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
													    );
													   
													   $content_pop=strtr($templete,$data);
													   $this->email->message($content_pop);
													   $this->email->send();  
													}
												}
											}
											/*end*/
										}	
									}
									/*End 4-5-17*/
								}
								if($cstatus == 'Pending')
								{
									//New code for update user balance details 4-5-17
									/*if($cash_status =='Completed') 
									{	
										$user_bale 		= $balance;
										$newbalnce 		= $user_bale - $transaction_amount;
											
										$data = array(		
										'balance' => $newbalnce);
										$this->db->where('user_id',$get_userid);
										$update_qry = $this->db->update('tbl_users',$data);
									}
									//end 4-5-17

									//mail for Pending cashback 1-3-17
									if($cashback_status == 1)
									{
										$this->db->where('mail_id',10);
										$mail_template = $this->db->get('tbl_mailtemplates');
										
										if($mail_template->num_rows >0) 
										{
										    $fetch     = $mail_template->row();
										    $subject   = $fetch->email_subject;
										    $templete  = $fetch->email_template;
										    //$url     = base_url().'cashback/my_earnings/';
										    $unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
										    $myaccount = base_url().'minha_conta';
										   
											$this->load->library('email');
											
											$config = Array(
												'mailtype'  => 'html',
												'charset'   => 'utf-8',
											);
											
											$sub_data = array(
												'###SITENAME###'=>$site_name
											);
											$subject_new = strtr($subject,$sub_data);
											
											$this->email->set_newline("\r\n");
											$this->email->initialize($config);
											$this->email->from($admin_email,$site_name.'!');
											$this->email->to($user_email);
											$this->email->subject($subject_new);
										   
											$data = array(
												'###NAME###'=>$user_name,
												'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
												'###SITENAME###'=>$site_name,
												'###ADMINNO###'=>$admin_no,
												'###DATE###'=>$date,
												'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
												'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
												'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
										    );
										   
										    $content_pop=strtr($templete,$data);
										   	$this->email->message($content_pop);
										   	$this->email->send();  
										}
									} 
									//end 1-3-17

									//New code for pending referral mail for (REFER) User 4-5-17
									$check_ref    = $this->check_ref_user($get_userid);

									if($check_ref > 0)		
									{
										$ref_id  	  = $check_ref;
										$ref_user_bal = $this->view_balance($check_ref);
										$return  	  = $this->check_active_user($ref_id);

										if($return)
										{
											if($cash_status  == 'Completed')
											{	
												$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Approved'")->row();	
											}
											if($cash_status  == 'Canceled')
											{
												$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Canceled'")->row();
											}
											if($cash_status  == 'Pending')
											{
												$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Pending'")->row();
											}
											 
										 										 
										 	$transation_amount = $ref_trans_details->transation_amount;

											foreach($return as $newreturn)
											{
												$referral_balance   = $newreturn->balance; 
												$user_referral_mail = $newreturn->referral_mail;
												$ref_user_email     = $newreturn->email;
												$ref_first_name   	= $newreturn->first_name;
												$ref_last_name 	  	= $newreturn->last_name;								

												if($ref_first_name == '' && $ref_last_name == '')
												{
													$ex_name       = explode('@', $ref_user_email);
													$ref_user_name = $ex_name[0]; 
												}
												else
												{
													$ref_user_name = $ref_first_name.' '.$ref_last_name;
												}
											}

											if($cash_status  == 'Completed')
											{
												$bal_ref = $referral_balance - $transation_amount;
												$this->db->where('user_id',$ref_id);
												$this->db->update('tbl_users',array('balance'=>$bal_ref));
											}

											$data = array('transation_status'=>'Pending'); 
											$this->db->where('report_update_id',$report_update_id);
											$this->db->where('ref_user_tracking_id',$get_userid);
											$this->db->update('transation_details',$data);	
											 
											if($user_referral_mail == 1)
											{
												$this->db->where('mail_id',20);
												$mail_template = $this->db->get('tbl_mailtemplates');
												if($mail_template->num_rows >0) 
												{
													$fetch     = $mail_template->row();
													$subject   = $fetch->email_subject;
													$templete  = $fetch->email_template;
													$url 	   = base_url().'my_earnings/';
													$unsuburls = base_url().'un-subscribe/referral/'.$ref_id;
											   		$myaccount = base_url().'minha-conta';
													
													$this->load->library('email');

													$config    = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
													);
															
													$sub_data = array(
													'###SITENAME###'=>$site_name
													);
													
													$subject_new = strtr($subject,$sub_data);
													// $this->email->initialize($config);
													$this->email->set_newline("\r\n");
													$this->email->initialize($config);
													$this->email->from($admin_email,$site_name.'!');
													$this->email->to($ref_user_email);
													$this->email->subject($subject_new);

													//echo $transation_amount; exit;											
													$datas = array(
													'###NAME###'=>$ref_user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>date('y-m-d'),
													'###AMOUNT###'=>str_replace('.',',',bcdiv($transation_amount,1,2)),
													'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
													);
													$content_pop=strtr($templete,$datas);
													$this->email->message($content_pop);
													$this->email->send();  
												}
											}
										}
									}*/
									//End 4-5-17
								}	
							}
						}
					}

				}		  
			}

			//Completed 
			if($affiliate_name == 'cityads')
			{
				//echo "<pre>";print_r($content); exit;
				if($content['data']['items']!='')
				{
					foreach($content['data']['items'] as $cont)
					{
							
						$all   = 0;
						$transaction_id   	  = $cont['orderID'];

						if($transaction_id !='')
						{
							$report_update_id   = $transaction_id;
						}else
						{
							$report_update_id   = rand(1000,9999);	
						}

						/*new code 4-5-17*/
					 	$newtransaction_id    = $cont['orderID'];
						$cash_status 	      = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('status');
						if($newtransaction_id == '')
						{
							$newid             = rand(1000,9999);
							$newtransaction_id = md5($newid);
						}
					 	/*End 4-5-17*/ 

						$this->db->where('report_update_id',$report_update_id);
			 			$all   = $this->db->get('cashback');				 	  	 
						$fetch = $all->row();

						if(!empty($fetch))
						{
							
							$duplicate 			  = 1;
							$get_userid           = $fetch->user_id;
							$transaction_amount   = $fetch->cashback_amount;


							$offer_name			  = $cont['offerName'];
							$commission           = $cont['commissionOpen'];
							$status 			  = $cont['status']; 	
							
							$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$get_userid))->row('balance');
							if($prev_userbal == '')
							{
								$prev_userbal = 0;	
							}

							/*New code for mail notification details 3-3-17*/
							$userdetails 	 = $this->db->query("SELECT * from tbl_users where user_id=$get_userid")->row();
							$cashback_status = $userdetails->cashback_mail;
							$user_email      = $userdetails->email;
							$first_name      = $userdetails->first_name;
							$last_name       = $userdetails->last_name;
							$balance  		 = $userdetails->balance;
							
							if($first_name == '' && $last_name == '')
							{
								$ex_name   = explode('@', $user_email);
								$user_name = $ex_name[0]; 
							}
							else
							{
								$user_name = $first_name.''.$last_name;
							}

							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin 		 = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name 	 = $admin->site_name;
								$admin_no	 = $admin->contact_number;
								$site_logo 	 = $admin->site_logo;
							}
							$date = date('Y-m-d');
							/*end 3-3-17*/

							if($status !='')
							{
								/*if($status == 'Approved')
								{
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");
								}
								else if($status == 'Rejected')
								{
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Canceled')");
								}
								else 
								{
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Pending')");
								}*/

								if($status == 'Approved')
								{
									$cstatus = 'Completed';
								}
								if($status == 'Rejected')
								{	
									$cstatus = 'Canceled';
								}
								if($status == 'Open')
								{	
									$cstatus = 'Pending';
								}
								
									

								if($cstatus == 'Completed')
								{
									if($cash_status != 'Completed')
									{
										
										$data = array('status' => $cstatus); 
										$this->db->where('report_update_id',$report_update_id);
										$updation = $this->db->update('cashback',$data);

										$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
										VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");


										$this->db->where('user_id',$get_userid);
										$this->db->update('tbl_users',array('balance'=>$prev_userbal+$transaction_amount));
										
										/*mail for Completed cashback 2-3-17*/
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',8);
											$mail_template = $this->db->get('tbl_mailtemplates');
											if($mail_template->num_rows >0) 
											{
											   	$fetch     = $mail_template->row();
											   	$subject   = $fetch->email_subject;
											   	$templete  = $fetch->email_template;
											   	$url 	  = base_url().'my_earnings/';
											   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
											   	$myaccount = base_url().'minha_conta';
											   
											  	$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);

												$subject_new = strtr($subject,$sub_data);
												
												//$this->email->initialize($config);
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email, $site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', $transaction_amount),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

											    );
											   
											   $content_pop=strtr($templete,$data);
											   $this->email->message($content_pop);
											   $this->email->send();  
											}
										} 
										/*end 2-3-17*/

										/*New code for referral mail and user cashback balance details 04-5-17*/
										$this->db->where('report_update_id',$report_update_id);
										$cashbacks 	   = $this->db->get('cashback');
										$cashback_data = $cashbacks->row();

										if($cashback_data->referral!=0)
										{
											$this->db->where('trans_id',$cashback_data->txn_id);
											$txn = $this->db->get('transation_details');
											$txn_detail = $txn->row();

											if($txn_detail)
											{
												$txn_id 	 	    = $txn_detail->trans_id;
												$ref_user_id 	    = $txn_detail->user_id;
												$transation_amounts = $txn_detail->transation_amount;
												$refer_user 	    = $this->view_user($ref_user_id);
												
												if($refer_user)
												{
													foreach($refer_user as $single)
													{
														$referral_balance = $single->balance;
														$user_email 	  = $single->email;
														$first_name 	  = $single->first_name;
														$last_name 		  = $single->last_name;

														if($first_name == '' && $last_name == '')
														{
															$ex_name   = explode('@', $user_email);
															$user_name = $ex_name[0]; 
														}
														else
														{
															$user_name = $first_name.' '.$last_name;
														}
													}

													//Update refer user balance details in User table and transaction table(approve referral cashback amount)//
													$this->db->where('user_id',$ref_user_id);
													$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amounts));
													 

													$data = array('transation_status'=>'Approved','transation_reason'=>'Referal Payment'); //'transaction_date'=>$date,
													//$this->db->where('transation_reason','Pending Referal Payment');
													$this->db->where('trans_id',$cashback_data->txn_id);
													$this->db->update('transation_details',$data);

													/* mail for Approve cashback amt mail notifications */
													if($single->referral_mail == 1)
													{	
														$this->db->where('mail_id',9);
														$mail_template = $this->db->get('tbl_mailtemplates');
														if($mail_template->num_rows >0) 
														{
														   $fetch 	  = $mail_template->row();
														   $subject   = $fetch->email_subject;
														   $templete  = $fetch->email_template;
														   $url 	  = base_url().'my_earnings/';
														   $unsuburls = base_url().'un-subscribe/referral/'.$ref_user_id;
											   			   $myaccount = base_url().'minha_conta';
														   
															$this->load->library('email');
															
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);
															
															$sub_data = array(
																'###SITENAME###'=>$site_name
															);
															$subject_new = strtr($subject,$sub_data);
															
															// $this->email->initialize($config);
															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_email,$site_name.'!');
															$this->email->to($user_email);
															$this->email->subject($subject_new);
														   
															$datas = array(
																'###NAME###'=>$user_name,
																'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
																'###SITENAME###'=>$site_name,
																'###ADMINNO###'=>$admin_no,
																'###DATE###'=>$date,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amounts,1,2)),
																'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
															);
														   
														   $content_pop=strtr($templete,$datas);
														   $this->email->message($content_pop);
														   $this->email->send();  
														}
													}	
													/* Mail for Approve referral cashback amount mail End*/
												}
											}
										}
										/*end 4-5-17*/

										/*first and second Withdraw mail notification for referral users 4-5-17*/
										$this->db->where('admin_id',1);
										$Admin_Details_Query    = $this->db->get('admin');
										$Admin_Details 		    = $Admin_Details_Query->row();
										$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
										$remain_minimum_amt     = $Admin_Details->remain_minimum_with_amt;
										$Site_Logo 				= $Admin_Details->site_logo;
										$admin_emailid 			= $Admin_Details->admin_email;

										if($ref_user_id!='')
										{

											$this->db->where('report_update_id',$report_update_id);
											$cashbacks 	   = $this->db->get('cashback');
											$cashback_data = $cashbacks->row();

											if($cashback_data->referral!=0)
											{
												$this->db->where('trans_id',$cashback_data->txn_id);
												$txn = $this->db->get('transation_details');
												$txn_detail = $txn->row();
												
												if($txn_detail)
												{
													//$new_txn_ids = $txn_detail->new_txn_id;
													$new_txn_ids   = $txn_detail->trans_id;

													if($single->referral_category_type != '')
													{ 	
														$referrals    	    = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
														$ref_by_percentage  = $referrals->ref_by_percentage;
														$ref_by_rate 		= $referrals->ref_by_rate;
														$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

														//3** Bonus by Refferal Rate type//
														if($bonus_by_ref_rate == 1)
														{
															$n9  	 = '333445';
															$n12 	 = $n9 + $ref_user_id;
															$now 	 = date('Y-m-d H:i:s');	
															$selqry  = "SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Referal Payment' AND user_id=$ref_user_id"; 
													 		$query   = $this->db->query("$selqry");
															$numrows = $query->num_rows();
															if($numrows > 0)
															{
																$fetch 		   = $query->row();
																$usercount 	   = $fetch->userid;
																$referrals     = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
																$bonus_amount  = $referrals->ref_cashback_rate_bonus;
																$friends_count = $referrals->friends_count;
																
																if($usercount == $friends_count)
																{	
																	if($bonus_amount!='')
																	{	
																		if($single->referral_category_type == 1)
																		{ 
																		 	$types = 'One';
																		}
																		if($single->referral_category_type == 2)
																		{ 
																		 	$types = 'Two';
																		}
																		if($single->referral_category_type == 3)
																		{ 
																		 	$types = 'Three';
																		}
																		if($single->referral_category_type == 4)
																		{ 
																		 	$types = 'Four';
																		}
																		if($single->referral_category_type == 5)
																		{ 
																		 	$types = 'Five';
																		}
																		if($single->referral_category_type == 6)
																		{ 
																		 	$types = 'Six';
																		}
																		if($single->referral_category_type == 7)
																		{ 
																		 	$types = 'Seven';
																		}
																		if($single->referral_category_type == 8)
																		{ 
																		 	$types = 'Eight';
																		}
																		if($single->referral_category_type == 9)
																		{ 
																		 	$types = 'Nine';
																		}
																		if($single->referral_category_type == 10)
																		{ 
																		 	$types = 'Ten';
																		}

																		$selqry	= "SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category $types User' AND `user_id`=$ref_user_id"; 
														 				$query1	= $this->db->query("$selqry");
														 				$newnumrows = $query1->num_rows();
																		if($newnumrows > 0)
																		{
																			$fetch 		 = $query1->row();
																			$users_count = $fetch->userid;
																			if($users_count == 0)	
																			{	
																				$data = array(			
																				'transation_amount' => $bonus_amount,	
																				'user_id' => $ref_user_id,	
																				'transation_date' => $now,
																				'transaction_date' => $now,
																				'transation_id'=>$n12,	
																				'transation_reason' => 'Referral Bonus for Category ' .$types. ' User',	
																				'mode' => 'Credited',
																				'details_id'=>'',	
																				'table'=>'',	
																				'new_txn_id'=>0,
																				'transation_status ' => 'Approved',
																				'report_update_id'=>$newtransaction_id
																				);	
																				$this->db->insert('transation_details',$data);
																			}	
																		}
																	}	
																}
															} 
														}
														//3** Bonus by Refferal Rate type End//

														//1** Refferal by Percentage type Start//
														if($ref_by_percentage == 1)
														{
															$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); /*,'transaction_date'=>$date*/
															$this->db->where('trans_id',$new_txn_ids);
															$this->db->update('transation_details',$data);
														}
														//1** Refferal by Percentage type End//	
													}	
												}
											}

											/*New code for withdraw notification 1-4-17*/
											$ref_prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$ref_user_id))->row('balance');
											if($ref_prev_userbal == '')
											{
												$ref_prev_userbal = 0;	
											}


											//$ref_Total_Amount = ($ref_balance+$ref_cashback_amount);
											$ref_Total_Amount 	= ($referral_balance+$transation_amounts);
											$ref_User_details 	= $this->view_user($ref_user_id);
											$ref_us_email 	  	= $ref_User_details[0]->email;
											$ref_with_status  	= $ref_User_details[0]->withdraw_mail;
											$ref_unsuburl	  	= base_url().'un-subscribe/withdraw/'.$ref_user_id;
											$ref_myaccount    	= base_url().'resgate';
											$ref_firstname 	  	= $ref_User_details[0]->first_name;
											$ref_lastname  	  	= $ref_User_details[0]->last_name;
											$with_mail_status 	= $ref_User_details[0]->first_withdraw_mail_status;
											$first_with_status  = $ref_User_details[0]->first_withdraw_status;
											$second_with_status = $ref_User_details[0]->second_withdraw_status;


											if($with_mail_status == 1)
											{
												$Admin_Minimum_Cashback = $remain_minimum_amt;
												$new_withdraw_status    = $second_with_status;
											}
											else
											{
												$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;
												$new_withdraw_status    = $first_with_status;	
											}

											if($ref_firstname == '' && $ref_lastname == '')
											{
												$ex_name  	  = explode('@', $ref_User_details[0]->email);
												$ref_username = $ex_name[0]; 
											}
											else
											{
												$ref_username = $ref_firstname.' '.$ref_lastname;
											}	
											/*End 9-1-17*/
											 
											if($ref_with_status == 1)
											{
												if($new_withdraw_status == 0)
												{
													if($ref_Total_Amount>=$Admin_Minimum_Cashback)
													{
														$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
														if($obj_temp->num_rows>0)
														{
															$mail_temp  = $obj_temp->row(); 
															$fe_cont    = $mail_temp->email_template;	
															$subject  	= $mail_temp->email_subject;	
															$servername = base_url();
															$nows 		= date('Y-m-d');	
															$this->load->library('email');
															$gd_api=array(
																
																'###NAME###'=>$ref_username,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($ref_Total_Amount,1,2)),
																'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
																'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
																'###ULINK###'=>'<a href='.$ref_unsuburl.'>'.$ref_unsuburl.'</a>',
																'###MYACLINK###'=>'<a href='.$ref_myaccount.'>'.$ref_myaccount.'</a>'
																);
																			   
															$gd_message=strtr($fe_cont,$gd_api);
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);

															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_emailid,$site_name.'!');
															$this->email->to($ref_us_email);
															$this->email->subject($subject);
															$this->email->message($gd_message);
															$this->email->send();
															$this->email->print_debugger();

															/*new code for update a first or second withdraw amount status 23-6-17*/
															if($new_withdraw_status == 0)
															{
																if($with_mail_status == 1)
																{
																	$data = array(		
																	'second_withdraw_status'  => 1);
																	$this->db->where('user_id',$ref_user_id);
																	$update_qry= $this->db->update('tbl_users',$data);
																}
																else
																{
																	$data = array(		
																	'first_withdraw_status'  => 1);
																	$this->db->where('user_id',$ref_user_id);
																	$update_qry= $this->db->update('tbl_users',$data);
																}	
															}
															/*End*/
														}
													}
												}	
											}
											 
											/*end 1-4-17*/		
										}
										/*End mail notification for referral users 4-5-17*/

										/*first and second withdraw mail notification for refered user 4-5-17*/
										$Total_Amount 	  	= ($balance+$transaction_amount);
										$User_details 	  	= $this->view_user($get_userid);
										$us_email 		  	= $User_details[0]->email;
										$with_status      	= $User_details[0]->withdraw_mail;
										$firstname 		  	= $User_details[0]->first_name;
										$lastname  		  	= $User_details[0]->last_name;
										$with_mail_status 	= $User_details[0]->first_withdraw_mail_status;

										$unsuburl	 	  	= base_url().'un-subscribe/withdraw/'.$get_userid;
										$myaccount    	  	= base_url().'resgate';
										$first_with_status  = $User_details[0]->first_withdraw_status;
										$second_with_status = $User_details[0]->second_withdraw_status;

										if($with_mail_status == 1)
										{
											$Admin_Minimum_Cashback = $remain_minimum_amt;
											$new_withdraw_status    = $second_with_status;
										}
										else
										{
											$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;	
											$new_withdraw_status    = $first_with_status;
										}

										if($firstname == '' && $lastname == '')
										{
											$ex_name  = explode('@', $User_details[0]->email);
											$username = $ex_name[0]; 
										}
										else
										{
											$username = $firstname.' '.$lastname;
										}	
										/*End 9-1-17*/
										
										 
										if($with_status == 1)
										{
											if($new_withdraw_status == 0)
											{
												if($Total_Amount>=$Admin_Minimum_Cashback)
												{
													$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
													if($obj_temp->num_rows>0)
													{
														$mail_temp  = $obj_temp->row(); 
														$fe_cont    = $mail_temp->email_template;	
														$subject  	= $mail_temp->email_subject;		
														$servername = base_url();
														$nows 		= date('Y-m-d');	
														$this->load->library('email');
														$gd_api=array(
															
															'###NAME###'=>$username,
															'###AMOUNT###'=>str_replace('.', ',', bcdiv($Total_Amount,1,2)),
															'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
															'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
															'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
															'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
															);
																		   
														$gd_message=strtr($fe_cont,$gd_api);
														$config = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
														);
														 
														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_emailid,$site_name.'!');
														$this->email->to($us_email);
														$this->email->subject($subject);
														$this->email->message($gd_message);
														$this->email->send();
														$this->email->print_debugger();

														/*new code for update a first or second withdraw amount status 20-5-17*/
														if($new_withdraw_status == 0)
														{
															if($with_mail_status == 1)
															{
																$data = array(		
																'second_withdraw_status'  => 1);
																$this->db->where('user_id',$get_userid);
																$update_qry= $this->db->update('tbl_users',$data);
															}
															else
															{
																$data = array(		
																'first_withdraw_status'  => 1);
																$this->db->where('user_id',$get_userid);
																$update_qry= $this->db->update('tbl_users',$data);
															}	
														}
														/*End*/
													}
												}
											}	
										}	
										 	
										/*End 9-1-17*/
										/*End mail notification for referred user 4-5-17*/
									}	
								}
								if($cstatus == 'Canceled')
								{
									if($cash_status != 'Canceled')
									{
										$data = array('status' => $cstatus); 
										$this->db->where('report_update_id',$report_update_id);
										$updation = $this->db->update('cashback',$data);

										/*mail for Canceled cashback 2-3-17*/
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',11);
											$mail_template = $this->db->get('tbl_mailtemplates');
											if($mail_template->num_rows >0) 
											{

											   	$fetch 	   = $mail_template->row();
											   	$subject   = $fetch->email_subject;
											   	$templete  = $fetch->email_template;
											   	$url 	   = base_url().'cashback/my_earnings/';
											   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
											   	$myaccount = base_url().'minha_conta';
											   
												$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);
												$subject_new = strtr($subject,$sub_data);
												
												//$this->email->initialize($config);
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email,$site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', $transaction_amount),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
											    );
											   
											   $content_pop=strtr($templete,$data);
											   //echo print_r($content_pop); exit;
											   // echo $content_pop; echo $subject_new;
											   $this->email->message($content_pop);
											   $this->email->send();  
											}
										} 
										/*end 2-3-17*/ 

										/*New code for update user table 4-5-17*/
										if($cash_status =='Completed' && $cstatus == 'Canceled')
										{
											$user_bale 		= $balance;
											$newbalnce 		= $user_bale - $transaction_amount;
												
											$data = array(		
											'balance' => $newbalnce);
											$this->db->where('user_id',$get_userid);
											$update_qry = $this->db->update('tbl_users',$data);

											/*new code 26-7-17*/
											$this->db->delete('transation_details',array('report_update_id' => $report_update_id,'transation_reason'=>'Cashback','user_id'=>$get_userid));
											/*End 26-7-17*/
										}
										/*End 4-5-17*/

										/*Mail for cancelled referral amount for reffered user mail 04-5-17*/
										$this->db->where('report_update_id',$report_update_id);
										$cashbacks 	   = $this->db->get('cashback');
										$cashback_data = $cashbacks->row();
										if($cashback_data->referral!=0)
										{
											$this->db->where('trans_id',$cashback_data->txn_id);
											$txn 		= $this->db->get('transation_details');
											$txn_detail = $txn->row();
											
											if(!empty($txn_detail))
											{
												$txn_id 	 	   = $txn_detail->trans_id;
												$ref_user_id 	   = $txn_detail->user_id;
												$transation_amount = $txn_detail->transation_amount;
												$newtrans_status   = $txn_detail->transation_status; 
												$refer_user 	   = $this->view_user($ref_user_id);

												if($refer_user)
												{
													foreach($refer_user as $single)
													{
														$referral_balance = $single->balance;
														$user_email 	  = $single->email;
														$first_name 	  = $single->first_name;
														$last_name 		  = $single->last_name;
														$reffer_status    = $single->referral_mail;

														if($first_name == '' && $last_name == '')
														{
															$ex_name   = explode('@', $user_email);
															$user_name = $ex_name[0]; 
														}
														else
														{
															$user_name = $first_name.' '.$last_name;
														}
													}

													if($status == 'Canceled')
													{
														if($newtrans_status == 'Approved')
														{
															$new_balance = ($referral_balance - $transation_amount);
															$this->db->where('user_id',$ref_user_id);
															$this->db->update('tbl_users',array('balance'=>$new_balance));
														}
													}

													$data = array(	
													'transation_status'=>$cstatus
													);
													$this->db->where('trans_id',$cashback_data->txn_id);
													$updation = $this->db->update('transation_details',$data);
														
													$unsuburl  = base_url().'un-subscribe/referral/'.$ref_user_id;
												    $myaccount = base_url().'minha-conta';

													$this->db->where('admin_id',1);
													$admin_det = $this->db->get('admin');
													if($admin_det->num_rows >0) 
													{    
														$admin 		 = $admin_det->row();
														$admin_email = $admin->admin_email;
														$site_name 	 = $admin->site_name;
														$admin_no 	 = $admin->contact_number;
														$site_logo 	 = $admin->site_logo;
													}

													if($reffer_status == 1)
													{
														$this->db->where('mail_id',19);
														$mail_template = $this->db->get('tbl_mailtemplates');
														if($mail_template->num_rows >0) 
														{
														   $fetch = $mail_template->row();
														   $subject = $fetch->email_subject;
														   $templete = $fetch->email_template;
															
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);
															
															// $this->email->initialize($config);
															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_email,$site_name.'!');
															$this->email->to($user_email);
															$this->email->subject($subject);
														   
															$data = array(
																'###ADMINNO###'=>$admin_no,
																'###EMAIL###'=>$user_name,
																'###DATE###'=>$date,
																'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
																'###SITENAME###' =>$site_name,
																'###STATUS###'=>$status,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amount,1,2)),
																'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
														    );
														   
														   $content_pop=strtr($templete,$data);
														   $this->email->message($content_pop);
														   $this->email->send();  
														}
													}
												}
												/*end*/
											}	
										}
										/*End 4-5-17*/
									}	
								}
								if($cstatus == 'Pending')
								{
									/*if($cash_status != 'Pending')
									{
										//New code for update user balance details 4-5-17
										if($cash_status =='Completed') 
										{	
											$user_bale 		= $balance;
											$newbalnce 		= $user_bale - $transaction_amount;
												
											$data = array(		
											'balance' => $newbalnce);
											$this->db->where('user_id',$get_userid);
											$update_qry = $this->db->update('tbl_users',$data);
										}
										//end 4-5-17

										//mail for Pending cashback 2-3-17
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',10);
											$mail_template = $this->db->get('tbl_mailtemplates');
											
											if($mail_template->num_rows >0) 
											{
											    $fetch     = $mail_template->row();
											    $subject   = $fetch->email_subject;
											    $templete  = $fetch->email_template;
											    //$url     = base_url().'cashback/my_earnings/';
											    $unsuburl  = base_url().'cashback/un-subscribe/cashback/'.$get_userid;
											    $myaccount = base_url().'cashback/minha_conta';
											   
												$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);
												$subject_new = strtr($subject,$sub_data);
												
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email,$site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
											    );
											   
											    $content_pop=strtr($templete,$data);
											   	$this->email->message($content_pop);
											   	$this->email->send();  
											}
										} 
										//end 2-3-17

										//New code for pending referral mail for (REFER) User 4-5-17
										$check_ref    = $this->check_ref_user($get_userid);

										if($check_ref > 0)		
										{
											$ref_id  	  = $check_ref;
											$ref_user_bal = $this->view_balance($check_ref);
											$return  	  = $this->check_active_user($ref_id);

											if($return)
											{
												if($cash_status  == 'Completed')
												{	
													$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Approved'")->row();	
												}
												if($cash_status  == 'Canceled')
												{
													$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Canceled'")->row();
												}
												if($cash_status  == 'Pending')
												{
													$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$user_id' AND `transation_status`='Pending'")->row();
												}
												 
											 										 
											 	$transation_amount = $ref_trans_details->transation_amount;

												foreach($return as $newreturn)
												{
													$referral_balance   = $newreturn->balance; 
													$user_referral_mail = $newreturn->referral_mail;
													$ref_user_email     = $newreturn->email;
													$ref_first_name   	= $newreturn->first_name;
													$ref_last_name 	  	= $newreturn->last_name;								

													if($ref_first_name == '' && $ref_last_name == '')
													{
														$ex_name       = explode('@', $ref_user_email);
														$ref_user_name = $ex_name[0]; 
													}
													else
													{
														$ref_user_name = $ref_first_name.' '.$ref_last_name;
													}
												}

												if($cash_status  == 'Completed')
												{
													$bal_ref = $referral_balance - $transation_amount;
													$this->db->where('user_id',$ref_id);
													$this->db->update('tbl_users',array('balance'=>$bal_ref));
												}

												$data = array('transation_status'=>'Pending'); 
												$this->db->where('report_update_id',$report_update_id);
												$this->db->where('ref_user_tracking_id',$get_userid);
												$this->db->update('transation_details',$data);	
												 
												if($user_referral_mail == 1)
												{
													$this->db->where('mail_id',20);
													$mail_template = $this->db->get('tbl_mailtemplates');
													if($mail_template->num_rows >0) 
													{
														$fetch     = $mail_template->row();
														$subject   = $fetch->email_subject;
														$templete  = $fetch->email_template;
														$url 	   = base_url().'my_earnings/';
														$unsuburls = base_url().'un-subscribe/referral/'.$ref_id;
												   		$myaccount = base_url().'minha-conta';
														
														$this->load->library('email');

														$config    = Array(
														'mailtype'  => 'html',
														'charset'   => 'utf-8',
														);
																
														$sub_data = array(
														'###SITENAME###'=>$site_name
														);
														
														$subject_new = strtr($subject,$sub_data);
														// $this->email->initialize($config);
														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_email,$site_name.'!');
														$this->email->to($ref_user_email);
														$this->email->subject($subject_new);

														//echo $transation_amount; exit;											
														$datas = array(
														'###NAME###'=>$ref_user_name,
														'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
														'###SITENAME###'=>$site_name,
														'###ADMINNO###'=>$admin_no,
														'###DATE###'=>date('y-m-d'),
														'###AMOUNT###'=>str_replace('.',',',bcdiv($transation_amount,1,2)),
														'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
														'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
														);
														$content_pop=strtr($templete,$datas);
														$this->email->message($content_pop);
														$this->email->send();  
													}
												}
											}
										}
										//End 4-5-17
									}*/	
								}
							}
						}
		 			}
	 			}		  
			}
			//Completed
			if($affiliate_name == 'afilio')
			{
				//echo "<pre>";print_r($content); exit;
				if($content['list']['sale']!='')
				{
					foreach($content['list']['sale'] as $cont)
					{
						 
						$all=0;
						$transaction_id    = $cont['saleid']['value'];
						$this->db->where('report_update_id',$transaction_id);
						//$all = $this->db->get('cashback')->num_rows();
						$all   = $this->db->get('cashback');				 	  	 
						$fetch = $all->row();

						if(!empty($fetch))
						{

							$duplicate = 1;
							$get_userid 	     = $fetch->user_id;	 
							$transaction_amount  = $fetch->cashback_amount;
							$category_id 		 = $cont['progid']['value'];	
							$selqry        		 = $this->db->query("SELECT category_name from afilio_category_name_details where category_id='$category_id'")->row(); 
							$coupon_id   		 = utf8_encode($selqry->category_name);
							//$user_id           = $cont['xtra']['value'];	
							//$get_userid        = decode_userid($user_id);
							$commission_amount   = $cont['comission']['value'];
							$transaction_date    = $cont['date']['value'];
							//$transaction_date  = preg_replace('/\//', '-',$old_trans_date);
							//$transaction_amount  = $cont['order_price']['value'];
							$order_id   		 = $cont['order_id']['value'];
							$status 			 = $cont['status']['value'];

							//if($get_userid =='-230000')
						 	//{
						 	//	$get_userid = 0;
						 	//}
						 	//$get_userid = 850;

						 	if($transaction_id !='')
							{
								$report_update_id   = $transaction_id;
							}else
							{
								$report_update_id   = rand(1000,9999);	
							}

							/*new code 4-5-17*/
						 	$newtransaction_id = $cont['saleid']['value'];
							$cash_status 	   = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('status');
							if($newtransaction_id == '')
							{
								$newid             = rand(1000,9999);
								$newtransaction_id = md5($newid);
							}
						 	/*End 4-5-17*/

							$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$get_userid))->row('balance');
							if($prev_userbal == '')
							{
								$prev_userbal = 0;	
							}

							/*New code for mail notification details 3-3-17*/
							$userdetails 	 = $this->db->query("SELECT * from tbl_users where user_id=$get_userid")->row();
							$cashback_status = $userdetails->cashback_mail;
							$user_email      = $userdetails->email;
							$first_name      = $userdetails->first_name;
							$last_name       = $userdetails->last_name;
							$balance  		 = $userdetails->balance;
							
							if($first_name == '' && $last_name == '')
							{
								$ex_name   = explode('@', $user_email);
								$user_name = $ex_name[0]; 
							}
							else
							{
								$user_name = $first_name.''.$last_name;
							}

							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin 		 = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name 	 = $admin->site_name;
								$admin_no	 = $admin->contact_number;
								$site_logo 	 = $admin->site_logo;
							}
							$date = date('Y-m-d');
							/*end 3-3-17*/

							if($status !='')
							{
								/*if($status == 'pending')
								{
									//$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									//VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Pending')");
								}
								else if($status == 'refused')
								{
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Canceled')");
								}
								else 
								{
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");
								}*/

								if($status == 'refused')
								{	
									$cstatus = 'Canceled';
								}
								else if($status == 'pending')
								{	
									$cstatus = 'Pending';
								}
								else
								{
									//$cstatus = 'Completed';
									if($cont['payment']['value'] == 'Unpaid')
									{
										$cstatus = 'Pending';
									}
									else
									{
										$cstatus = 'Completed';
									}
								}
									

								if($cstatus == 'Completed')
								{
									if($cash_status != 'Completed')
									{
										$data = array('status' => $cstatus); 
										$this->db->where('report_update_id',$report_update_id);
										$updation = $this->db->update('cashback',$data);

										$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
										VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");

										$this->db->where('user_id',$get_userid);
										$this->db->update('tbl_users',array('balance'=>$prev_userbal+$transaction_amount));
										
										/*mail for Completed cashback 3-3-17*/
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',8);
											$mail_template = $this->db->get('tbl_mailtemplates');
											if($mail_template->num_rows >0) 
											{
											   	$fetch     = $mail_template->row();
											   	$subject   = $fetch->email_subject;
											   	$templete  = $fetch->email_template;
											   	$url 	  = base_url().'my_earnings/';
											   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
											   	$myaccount = base_url().'minha_conta';
											   
											  	$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);

												$subject_new = strtr($subject,$sub_data);
												
												//$this->email->initialize($config);
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email, $site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', $transaction_amount),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

											    );
											   
											   $content_pop=strtr($templete,$data);
											   $this->email->message($content_pop);
											   $this->email->send();  
											}
										} 
										/*end 3-3-17*/

										/*New code for referral mail and user cashback balance details 04-5-17*/
										$this->db->where('report_update_id',$report_update_id);
										$cashbacks 	   = $this->db->get('cashback');
										$cashback_data = $cashbacks->row();

										if($cashback_data->referral!=0)
										{
											$this->db->where('trans_id',$cashback_data->txn_id);
											$txn = $this->db->get('transation_details');
											$txn_detail = $txn->row();

											if($txn_detail)
											{
												$txn_id 	 	    = $txn_detail->trans_id;
												$ref_user_id 	    = $txn_detail->user_id;
												$transation_amounts = $txn_detail->transation_amount;
												$refer_user 	    = $this->view_user($ref_user_id);
												
												if($refer_user)
												{
													foreach($refer_user as $single)
													{
														$referral_balance = $single->balance;
														$user_email 	  = $single->email;
														$first_name 	  = $single->first_name;
														$last_name 		  = $single->last_name;

														if($first_name == '' && $last_name == '')
														{
															$ex_name   = explode('@', $user_email);
															$user_name = $ex_name[0]; 
														}
														else
														{
															$user_name = $first_name.' '.$last_name;
														}
													}

													//Update refer user balance details in User table and transaction table(approve referral cashback amount)//
													$this->db->where('user_id',$ref_user_id);
													$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amounts));
													 

													$data = array('transation_status'=>'Approved','transation_reason'=>'Referal Payment'); //'transaction_date'=>$date,
													//$this->db->where('transation_reason','Pending Referal Payment');
													$this->db->where('trans_id',$cashback_data->txn_id);
													$this->db->update('transation_details',$data);

													/* mail for Approve cashback amt mail notifications */
													if($single->referral_mail == 1)
													{	
														$this->db->where('mail_id',9);
														$mail_template = $this->db->get('tbl_mailtemplates');
														if($mail_template->num_rows >0) 
														{
														   $fetch 	  = $mail_template->row();
														   $subject   = $fetch->email_subject;
														   $templete  = $fetch->email_template;
														   $url 	  = base_url().'my_earnings/';
														   $unsuburls = base_url().'un-subscribe/referral/'.$ref_user_id;
											   			   $myaccount = base_url().'minha_conta';
														   
															$this->load->library('email');
															
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);
															
															$sub_data = array(
																'###SITENAME###'=>$site_name
															);
															$subject_new = strtr($subject,$sub_data);
															
															// $this->email->initialize($config);
															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_email,$site_name.'!');
															$this->email->to($user_email);
															$this->email->subject($subject_new);
														   
															$datas = array(
																'###NAME###'=>$user_name,
																'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
																'###SITENAME###'=>$site_name,
																'###ADMINNO###'=>$admin_no,
																'###DATE###'=>$date,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amounts,1,2)),
																'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
															);
														   
														   $content_pop=strtr($templete,$datas);
														   $this->email->message($content_pop);
														   $this->email->send();  
														}
													}	
													/* Mail for Approve referral cashback amount mail End*/
												}
											}
										}
										/*end 4-5-17*/

										/*first and second Withdraw mail notification for referral users 4-5-17*/
										$this->db->where('admin_id',1);
										$Admin_Details_Query    = $this->db->get('admin');
										$Admin_Details 		    = $Admin_Details_Query->row();
										$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
										$remain_minimum_amt     = $Admin_Details->remain_minimum_with_amt;
										$Site_Logo 				= $Admin_Details->site_logo;
										$admin_emailid 			= $Admin_Details->admin_email;

										if($ref_user_id!='')
										{

											$this->db->where('report_update_id',$report_update_id);
											$cashbacks 	   = $this->db->get('cashback');
											$cashback_data = $cashbacks->row();

											if($cashback_data->referral!=0)
											{
												$this->db->where('trans_id',$cashback_data->txn_id);
												$txn = $this->db->get('transation_details');
												$txn_detail = $txn->row();
												
												if($txn_detail)
												{
													//$new_txn_ids = $txn_detail->new_txn_id;
													$new_txn_ids   = $txn_detail->trans_id;
											
													if($single->referral_category_type != '')
													{ 	
														$referrals    	    = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
														$ref_by_percentage  = $referrals->ref_by_percentage;
														$ref_by_rate 		= $referrals->ref_by_rate;
														$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

														//3** Bonus by Refferal Rate type//
														if($bonus_by_ref_rate == 1)
														{
															$n9  	 = '333445';
															$n12 	 = $n9 + $ref_user_id;
															$now 	 = date('Y-m-d H:i:s');	
															$selqry  = "SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Referal Payment' AND user_id=$ref_user_id"; 
													 		$query   = $this->db->query("$selqry");
															$numrows = $query->num_rows();
															if($numrows > 0)
															{
																$fetch 		   = $query->row();
																$usercount 	   = $fetch->userid;
																$referrals     = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
																$bonus_amount  = $referrals->ref_cashback_rate_bonus;
																$friends_count = $referrals->friends_count;
																
																if($usercount == $friends_count)
																{	
																	if($bonus_amount!='')
																	{	
																		if($single->referral_category_type == 1)
																		{ 
																		 	$types = 'One';
																		}
																		if($single->referral_category_type == 2)
																		{ 
																		 	$types = 'Two';
																		}
																		if($single->referral_category_type == 3)
																		{ 
																		 	$types = 'Three';
																		}
																		if($single->referral_category_type == 4)
																		{ 
																		 	$types = 'Four';
																		}
																		if($single->referral_category_type == 5)
																		{ 
																		 	$types = 'Five';
																		}
																		if($single->referral_category_type == 6)
																		{ 
																		 	$types = 'Six';
																		}
																		if($single->referral_category_type == 7)
																		{ 
																		 	$types = 'Seven';
																		}
																		if($single->referral_category_type == 8)
																		{ 
																		 	$types = 'Eight';
																		}
																		if($single->referral_category_type == 9)
																		{ 
																		 	$types = 'Nine';
																		}
																		if($single->referral_category_type == 10)
																		{ 
																		 	$types = 'Ten';
																		}

																		$selqry	= "SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category $types User' AND `user_id`=$ref_user_id"; 
														 				$query1	= $this->db->query("$selqry");
														 				$newnumrows = $query1->num_rows();
																		if($newnumrows > 0)
																		{
																			$fetch 		 = $query1->row();
																			$users_count = $fetch->userid;
																			if($users_count == 0)	
																			{	
																				$data = array(			
																				'transation_amount' => $bonus_amount,	
																				'user_id' => $ref_user_id,	
																				'transation_date' => $now,
																				'transaction_date' => $now,
																				'transation_id'=>$n12,	
																				'transation_reason' => 'Referral Bonus for Category ' .$types. ' User',	
																				'mode' => 'Credited',
																				'details_id'=>'',	
																				'table'=>'',	
																				'new_txn_id'=>0,
																				'transation_status ' => 'Approved',
																				'report_update_id'=>$newtransaction_id
																				);	
																				$this->db->insert('transation_details',$data);
																			}	
																		}
																	}	
																}
															} 
														}
														//3** Bonus by Refferal Rate type End//

														//1** Refferal by Percentage type Start//
														if($ref_by_percentage == 1)
														{
															$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); /*,'transaction_date'=>$date*/
															$this->db->where('trans_id',$new_txn_ids);
															$this->db->update('transation_details',$data);
														}
														//1** Refferal by Percentage type End//	
													}	
												}
											}

											/*New code for withdraw notification 1-4-17*/
											$ref_prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$ref_user_id))->row('balance');
											if($ref_prev_userbal == '')
											{
												$ref_prev_userbal = 0;	
											}


											//$ref_Total_Amount = ($ref_balance+$ref_cashback_amount);
											$ref_Total_Amount 	= ($referral_balance+$transation_amounts);
											$ref_User_details 	= $this->view_user($ref_user_id);
											$ref_us_email 	  	= $ref_User_details[0]->email;
											$ref_with_status  	= $ref_User_details[0]->withdraw_mail;
											$ref_unsuburl	  	= base_url().'un-subscribe/withdraw/'.$ref_user_id;
											$ref_myaccount    	= base_url().'resgate';
											$ref_firstname 	  	= $ref_User_details[0]->first_name;
											$ref_lastname  	  	= $ref_User_details[0]->last_name;
											$with_mail_status 	= $ref_User_details[0]->first_withdraw_mail_status;
											$first_with_status  = $ref_User_details[0]->first_withdraw_status;
											$second_with_status = $ref_User_details[0]->second_withdraw_status;

											if($with_mail_status == 1)
											{
												$Admin_Minimum_Cashback = $remain_minimum_amt;
												$new_withdraw_status    = $second_with_status;
											}
											else
											{
												$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;	
												$new_withdraw_status    = $first_with_status;
											}

											if($ref_firstname == '' && $ref_lastname == '')
											{
												$ex_name  	  = explode('@', $ref_User_details[0]->email);
												$ref_username = $ex_name[0]; 
											}
											else
											{
												$ref_username = $ref_firstname.' '.$ref_lastname;
											}	
											/*End 9-1-17*/
											if($ref_with_status == 1)
											{
												if($new_withdraw_status == 0)
												{
													if($ref_Total_Amount>=$Admin_Minimum_Cashback)
													{
														$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
														if($obj_temp->num_rows>0)
														{
															$mail_temp  = $obj_temp->row(); 
															$fe_cont    = $mail_temp->email_template;	
															$subject  	= $mail_temp->email_subject;	
															$servername = base_url();
															$nows 		= date('Y-m-d');	
															$this->load->library('email');
															$gd_api=array(
																
																'###NAME###'=>$ref_username,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($ref_Total_Amount,1,2)),
																'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
																'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
																'###ULINK###'=>'<a href='.$ref_unsuburl.'>'.$ref_unsuburl.'</a>',
																'###MYACLINK###'=>'<a href='.$ref_myaccount.'>'.$ref_myaccount.'</a>'
																);
																			   
															$gd_message=strtr($fe_cont,$gd_api);
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);

															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_emailid,$site_name.'!');
															$this->email->to($ref_us_email);
															$this->email->subject($subject);
															$this->email->message($gd_message);
															$this->email->send();
															$this->email->print_debugger();

															/*new code for update a first or second withdraw amount status 20-5-17*/
															if($new_withdraw_status == 0)
															{
																if($with_mail_status == 1)
																{
																	$data = array(		
																	'second_withdraw_status'  => 1);
																	$this->db->where('user_id',$ref_user_id);
																	$update_qry= $this->db->update('tbl_users',$data);
																}
																else
																{
																	$data = array(		
																	'first_withdraw_status'  => 1);
																	$this->db->where('user_id',$ref_user_id);
																	$update_qry= $this->db->update('tbl_users',$data);
																}	
															}
															/*End*/
														}
													}
												}	
											}
											/*end 1-4-17*/		
										}
										/*End mail notification for referral users 4-5-17*/

										/*first and second withdraw mail notification for refered user 4-5-17*/
										$Total_Amount 	  	= ($balance+$transaction_amount);
										$User_details 	  	= $this->view_user($get_userid);
										$us_email 		  	= $User_details[0]->email;
										$with_status      	= $User_details[0]->withdraw_mail;
										$firstname 		  	= $User_details[0]->first_name;
										$lastname  		  	= $User_details[0]->last_name;
										$with_mail_status 	= $User_details[0]->first_withdraw_mail_status;
										$first_with_status  = $User_details[0]->first_withdraw_status;
										$second_with_status = $User_details[0]->second_withdraw_status;

										$unsuburl	 	  = base_url().'un-subscribe/withdraw/'.$get_userid;
										$myaccount    	  = base_url().'resgate';

										if($with_mail_status == 1)
										{
											$Admin_Minimum_Cashback = $remain_minimum_amt;
											$new_withdraw_status    = $second_with_status;
										}
										else
										{
											$Admin_Minimum_Cashback = $Admin_Minimum_Cashback;
											$new_withdraw_status    = $first_with_status;	
										}

										if($firstname == '' && $lastname == '')
										{
											$ex_name  = explode('@', $User_details[0]->email);
											$username = $ex_name[0]; 
										}
										else
										{
											$username = $firstname.' '.$lastname;
										}	
										/*End 9-1-17*/
										
										if($with_status == 1)
										{
											if($new_withdraw_status == 0)
											{
												if($Total_Amount>=$Admin_Minimum_Cashback)
												{
													$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
													if($obj_temp->num_rows>0)
													{
														$mail_temp  = $obj_temp->row(); 
														$fe_cont    = $mail_temp->email_template;	
														$subject  	= $mail_temp->email_subject;		
														$servername = base_url();
														$nows 		= date('Y-m-d');	
														$this->load->library('email');
														$gd_api=array(
															
															'###NAME###'=>$username,
															'###AMOUNT###'=>str_replace('.', ',', bcdiv($Total_Amount,1,2)),
															'###REQUEST_WITHDRAW###'=>str_replace('.', ',', bcdiv($Admin_Minimum_Cashback,1,2)),
															'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
															'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
															'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
															);
																		   
														$gd_message=strtr($fe_cont,$gd_api);
														$config = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
														);
														 
														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_emailid,$site_name.'!');
														$this->email->to($us_email);
														$this->email->subject($subject);
														$this->email->message($gd_message);
														$this->email->send();
														$this->email->print_debugger();

														/*new code for update a first or second withdraw amount status 20-5-17*/
														if($new_withdraw_status == 0)
														{
															if($with_mail_status == 1)
															{
																$data = array(		
																'second_withdraw_status'  => 1);
																$this->db->where('user_id',$get_userid);
																$update_qry= $this->db->update('tbl_users',$data);
															}
															else
															{
																$data = array(		
																'first_withdraw_status'  => 1);
																$this->db->where('user_id',$get_userid);
																$update_qry= $this->db->update('tbl_users',$data);
															}	
														}
														/*End*/
													}
												}
											}	
										}	
										 	
										/*End 9-1-17*/
										/*End mail notification for referred user 4-5-17*/
									}	
								}
								if($cstatus == 'Canceled')
								{
									if($cash_status != 'Canceled')
									{
										
										$data = array('status' => $cstatus); 
										$this->db->where('report_update_id',$report_update_id);
										$updation = $this->db->update('cashback',$data);
								
										/*mail for Canceled cashback 3-3-17*/
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',11);
											$mail_template = $this->db->get('tbl_mailtemplates');
											if($mail_template->num_rows >0) 
											{

											   	$fetch 	   = $mail_template->row();
											   	$subject   = $fetch->email_subject;
											   	$templete  = $fetch->email_template;
											   	$url 	   = base_url().'cashback/my_earnings/';
											   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
											   	$myaccount = base_url().'minha_conta';
											   
												$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);
												$subject_new = strtr($subject,$sub_data);
												
												//$this->email->initialize($config);
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email,$site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', $transaction_amount),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
											    );
											   
											   $content_pop=strtr($templete,$data);
											   //echo print_r($content_pop); exit;
											   // echo $content_pop; echo $subject_new;
											   $this->email->message($content_pop);
											   $this->email->send();  
											}
										} 
										/*end 3-3-17*/ 

										/*New code for update user table 4-5-17*/
										if($cash_status =='Completed' && $cstatus == 'Canceled')
										{
											$user_bale 		= $balance;
											$newbalnce 		= $user_bale - $transaction_amount;
												
											$data = array(		
											'balance' => $newbalnce);
											$this->db->where('user_id',$get_userid);
											$update_qry = $this->db->update('tbl_users',$data);

											/*new code 26-7-17*/
											$this->db->delete('transation_details',array('report_update_id' => $report_update_id,'transation_reason'=>'Cashback','user_id'=>$get_userid));
											/*End 26-7-17*/
										}
										/*End 4-5-17*/

										/*Mail for cancelled referral amount for reffered user mail 04-5-17*/
										$this->db->where('report_update_id',$report_update_id);
										$cashbacks 	   = $this->db->get('cashback');
										$cashback_data = $cashbacks->row();
										if($cashback_data->referral!=0)
										{
											$this->db->where('trans_id',$cashback_data->txn_id);
											$txn 		= $this->db->get('transation_details');
											$txn_detail = $txn->row();
											
											if($txn_detail)
											{
												$txn_id 	 	   = $txn_detail->trans_id;
												$ref_user_id 	   = $txn_detail->user_id;
												$transation_amount = $txn_detail->transation_amount;
												$newtrans_status   = $txn_detail->transation_status; 
												$refer_user 	   = $this->view_user($ref_user_id);

												if($refer_user)
												{
													foreach($refer_user as $single)
													{
														$referral_balance = $single->balance;
														$user_email 	  = $single->email;
														$first_name 	  = $single->first_name;
														$last_name 		  = $single->last_name;
														$reffer_status    = $single->referral_mail;

														if($first_name == '' && $last_name == '')
														{
															$ex_name   = explode('@', $user_email);
															$user_name = $ex_name[0]; 
														}
														else
														{
															$user_name = $first_name.' '.$last_name;
														}
													}

													if($cstatus == 'Canceled')
													{
														if($newtrans_status == 'Approved')
														{
															$new_balance = ($referral_balance - $transation_amount);
															$this->db->where('user_id',$ref_user_id);
															$this->db->update('tbl_users',array('balance'=>$new_balance));
														}
														/*new code 13-7-17*/
														$this->db->delete('transation_details',array('report_update_id' => $txn_detail->report_update_id,'transation_reason'=>'Cashback'));
														/*End 13-7-17*/
													}

													$data = array(	
													'transation_status'=>$cstatus
													);
													$this->db->where('trans_id',$cashback_data->txn_id);
													//$this->db->where('report_update_id',$report_update_id);
													$updation = $this->db->update('transation_details',$data);
														
													$unsuburl  = base_url().'un-subscribe/referral/'.$ref_user_id;
												    $myaccount = base_url().'minha-conta';

													$this->db->where('admin_id',1);
													$admin_det = $this->db->get('admin');
													if($admin_det->num_rows >0) 
													{    
														$admin 		 = $admin_det->row();
														$admin_email = $admin->admin_email;
														$site_name 	 = $admin->site_name;
														$admin_no 	 = $admin->contact_number;
														$site_logo 	 = $admin->site_logo;
													}

													if($reffer_status == 1)
													{
														$this->db->where('mail_id',19);
														$mail_template = $this->db->get('tbl_mailtemplates');
														if($mail_template->num_rows >0) 
														{
														   $fetch = $mail_template->row();
														   $subject = $fetch->email_subject;
														   $templete = $fetch->email_template;
															
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);
															
															// $this->email->initialize($config);
															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_email,$site_name.'!');
															$this->email->to($user_email);
															$this->email->subject($subject);
														   
															$data = array(
																'###ADMINNO###'=>$admin_no,
																'###EMAIL###'=>$user_name,
																'###DATE###'=>$date,
																'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
																'###SITENAME###' =>$site_name,
																'###STATUS###'=>$status,
																'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amount,1,2)),
																'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
														    );
														   
														   $content_pop=strtr($templete,$data);
														   $this->email->message($content_pop);
														   $this->email->send();  
														}
													}
												}
												/*end*/
											}	
										}
										/*End 4-5-17*/
									}	
								}
								if($cstatus == 'Pending')
								{
									/*if($cash_status != 'Pending')
									{
										//New code for update user balance details 4-5-17
										if($cash_status =='Completed') 
										{	
											$user_bale 		= $balance;
											$newbalnce 		= $user_bale - $transaction_amount;
												
											$data = array(		
											'balance' => $newbalnce);
											$this->db->where('user_id',$get_userid);
											$update_qry = $this->db->update('tbl_users',$data);
										}
										//end 4-5-17

										//mail for Pending cashback 3-3-17
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',10);
											$mail_template = $this->db->get('tbl_mailtemplates');
											
											if($mail_template->num_rows >0) 
											{
											    $fetch     = $mail_template->row();
											    $subject   = $fetch->email_subject;
											    $templete  = $fetch->email_template;
											    //$url     = base_url().'cashback/my_earnings/';
											    $unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
											    $myaccount = base_url().'minha_conta';
											   
												$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);
												$subject_new = strtr($subject,$sub_data);
												
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email,$site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', bcdiv($transaction_amount,1,2)),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
											    );
											   
											    $content_pop=strtr($templete,$data);
											   	$this->email->message($content_pop);
											   	$this->email->send();  
											}
										} 
										//end 3-3-17

										//New code for pending referral mail for (REFER) User 4-5-17
										$check_ref    = $this->check_ref_user($get_userid);

										if($check_ref > 0)		
										{
											$ref_id  	  = $check_ref;
											$ref_user_bal = $this->view_balance($check_ref);
											$return  	  = $this->check_active_user($ref_id);

											if($return)
											{
												if($cash_status  == 'Completed')
												{	
													$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Approved'")->row();	
												}
												if($cash_status  == 'Canceled')
												{
													$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$get_userid' AND `transation_status`='Canceled'")->row();
												}
												if($cash_status  == 'Pending')
												{
													$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'  AND `ref_user_tracking_id`='$user_id' AND `transation_status`='Pending'")->row();
												}
												 
											 										 
											 	$transation_amount = $ref_trans_details->transation_amount;

												foreach($return as $newreturn)
												{
													$referral_balance   = $newreturn->balance; 
													$user_referral_mail = $newreturn->referral_mail;
													$ref_user_email     = $newreturn->email;
													$ref_first_name   	= $newreturn->first_name;
													$ref_last_name 	  	= $newreturn->last_name;								

													if($ref_first_name == '' && $ref_last_name == '')
													{
														$ex_name       = explode('@', $ref_user_email);
														$ref_user_name = $ex_name[0]; 
													}
													else
													{
														$ref_user_name = $ref_first_name.' '.$ref_last_name;
													}
												}

												if($cash_status  == 'Completed')
												{
													$bal_ref = $referral_balance - $transation_amount;
													$this->db->where('user_id',$ref_id);
													$this->db->update('tbl_users',array('balance'=>$bal_ref));
												}

												$data = array('transation_status'=>'Pending'); 
												$this->db->where('report_update_id',$report_update_id);
												$this->db->where('ref_user_tracking_id',$get_userid);
												$this->db->update('transation_details',$data);	
												 
												if($user_referral_mail == 1)
												{
													$this->db->where('mail_id',20);
													$mail_template = $this->db->get('tbl_mailtemplates');
													if($mail_template->num_rows >0) 
													{
														$fetch     = $mail_template->row();
														$subject   = $fetch->email_subject;
														$templete  = $fetch->email_template;
														$url 	   = base_url().'my_earnings/';
														$unsuburls = base_url().'un-subscribe/referral/'.$ref_id;
												   		$myaccount = base_url().'minha-conta';
														
														$this->load->library('email');

														$config    = Array(
														'mailtype'  => 'html',
														'charset'   => 'utf-8',
														);
																
														$sub_data = array(
														'###SITENAME###'=>$site_name
														);
														
														$subject_new = strtr($subject,$sub_data);
														// $this->email->initialize($config);
														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_email,$site_name.'!');
														$this->email->to($ref_user_email);
														$this->email->subject($subject_new);

														//echo $transation_amount; exit;											
														$datas = array(
														'###NAME###'=>$ref_user_name,
														'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
														'###SITENAME###'=>$site_name,
														'###ADMINNO###'=>$admin_no,
														'###DATE###'=>date('y-m-d'),
														'###AMOUNT###'=>str_replace('.',',',bcdiv($transation_amount,1,2)),
														'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
														'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
														);
														$content_pop=strtr($templete,$datas);
														$this->email->message($content_pop);
														$this->email->send();  
													}
												}
											}
										}
										//End 4-5-17
									}*/	
								}
							}							
						}	 
					}
				}		  
			}
		}	
		return 1;
	}
	/*End*/
	
}
?>
