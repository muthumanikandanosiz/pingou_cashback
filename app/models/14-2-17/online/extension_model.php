<?php
class Extension_model extends CI_Model
{
	
function userdetails($user_id)
	{
	   $this->db->connection_check();
	   $this->db->where('user_id',$user_id);
	   $this->db->where('admin_status','');
		$userdetails = $this->db->get('tbl_users');
		if($userdetails->num_rows > 0){
			return $userdetails->row();
		}
		return false;
	}
	
function getalldetails()
{
	 $this->db->connection_check();
	   $this->db->where('admin_id','1');
		$userdetails = $this->db->get('admin');
		if($userdetails->num_rows > 0){
			return $userdetails->row();
		}
		return false;
}


function getcurrentcoupons($siteurl)
{
	   $this->db->connection_check();
	   $this->db->like('site_url',$siteurl);
		$userdetails = $this->db->get('affiliates');		
		if($userdetails->num_rows > 0){
			return $userdetails->row();
		}
		return false;
}

function get_store_coupons($store_name)
{
    $this->db->connection_check();
    $this->db->where('expiry_date >=',date('Y-m-d')); // active coupons
    $this->db->where('offer_name',$store_name);
	$coupons = $this->db->get('coupons');
	//echo $this->db->last_query();die;		
	return $coupons->num_rows;
}


function getpreviousclickactivate($affid,$user_id)
{
		$this->db->connection_check();
		$this->db->where('user_id',$user_id);
		$this->db->where('affiliate_id',$affid);
		$userdetails = $this->db->get('click_history');		
		if($userdetails->num_rows > 0){
		return $userdetails->result();
		}
		return false;
}

function currency_format($osiz_amount)
{
	$osiz_pos_amount 	 = strpos($osiz_amount,',');
	$osiz_pos_amount1 	 = strpos($osiz_amount,'.');
	if($osiz_pos_amount  === false && $osiz_pos_amount1 === false)
	{
	  	$osiz_amount  	 = $osiz_amount.".00";
	  	$osiz_new_amount = preg_replace('/\./', ',', $osiz_amount);
	  	
	  	if($osiz_new_amount == ',00')
		{
				$osiz_new_amount = '0,00';
		} 
		return $osiz_new_amount;
	}
	else
	{	
		//$osiz_final_amount   = round($osiz_amount,2);
		$osiz_new_amount      = $osiz_amount;
		$osiz_final_amount    = preg_replace('/\./', ',', $osiz_new_amount); 
	
	  	/*if($osiz_final_amount == ',00')
		{
				$osiz_final_amount = '0,00';
		} */
	  	return $osiz_final_amount;
	}
	//echo $osiz_new_amount; 
} 

function unicbonus_content()
{
	$this->db->connection_check();
	$content = $this->db->get('ex_bonus_content');		
	if($content->num_rows > 0){
		return $content->row();
	}
		return false;
}


}
?>
