<?php
class Extension_model extends CI_Model
{
	
function userdetails($user_id)
	{
	   $this->db->connection_check();
	   $this->db->where('user_id',$user_id);
	   $this->db->where('admin_status','');
		$userdetails = $this->db->get('tbl_users');
		if($userdetails->num_rows > 0){
			return $userdetails->row();
		}
		return false;
	}
	
function getalldetails()
{
	 $this->db->connection_check();
	   $this->db->where('admin_id','1');
		$userdetails = $this->db->get('admin');
		if($userdetails->num_rows > 0){
			return $userdetails->row();
		}
		return false;
}


function getcurrentcoupons($siteurl)
{
	   $this->db->connection_check();
	   $this->db->like('site_url',$siteurl);
		$userdetails = $this->db->get('affiliates');		
		if($userdetails->num_rows > 0){
			return $userdetails->row();
		}
		return false;
}


function getpreviousclickactivate($affid,$user_id)
{
		$this->db->connection_check();
		$this->db->where('user_id',$user_id);
		$this->db->where('affiliate_id',$affid);
		$userdetails = $this->db->get('click_history');		
		if($userdetails->num_rows > 0){
		return $userdetails->result();
		}
		return false;
}	


}
?>
