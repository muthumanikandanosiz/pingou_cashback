<?php
class Front_model extends CI_Model
{
	function home_slider()
	{
		$this->db->connection_check();
		$this->db->where("banner_status",'1');
		$this->db->where("banner_position",'0');
		$query = $this->db->get('tbl_banners');
		return $query->result();
	}
	function email_sub($email)
	{
		$this->db->connection_check();
		$this->db->where('subscriber_email',$email);
		$query = $this->db->get('subscribers');
		if($query->num_rows() == 0)
		{
			$date = date('Y-m-d h:m:s');
			$data = array(
			'subscriber_email' => $email,
			'subscriber_status' => '1',
			'date_subscribed' => $date
			);
			$this->db->insert('subscribers',$data);
			return 1;
		}
		else
			return 0;
	}
	
	//list out Header menu

	function header_menu()
	{
		$this->db->connection_check();
		$this->db->where('cms_status','1');
		$this->db->where('cms_position','header');
		$query = $this->db->get('tbl_cms');
		if($query->num_rows() > 0)
			return $query->result();
	}

	//New code for header page login user name and Amount details //

	function userdetails($user_id)
	{
	   $this->db->connection_check();
	   $this->db->where('user_id',$user_id);
	   $this->db->where('admin_status','');
		$userdetails = $this->db->get('tbl_users');
		if($userdetails->num_rows > 0){
			return $userdetails->row();
		}
		return false;
	}
	function admindetails()
	{
	   $this->db->connection_check();
	   $this->db->where('admin_id',1);
	    
		$admindetails = $this->db->get('admin');
		if($admindetails->num_rows > 0){
			return $admindetails->row();
		}
		return false;
	}

	
		
	function referalamt($user_id)
	{
	   $this->db->connection_check();
	   $this->db->where('user_id',$user_id);
	   $this->db->where('transation_reason','Pending Referal Payment');
	   $this->db->where('transation_status','pending');
	   $referalamt = $this->db->get('transation_details');
		if($referalamt->num_rows > 0){

			foreach($referalamt->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
	}

	function cashbackamt($user_id)
	{
	   $this->db->connection_check();
	   $this->db->where('user_id',$user_id);
		$this->db->where('status','pending');
		$cashbackamt = $this->db->get('cashback');
		if($cashbackamt->num_rows > 0){

			foreach($cashbackamt->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
	}

	//End//

	//Code for all email subscribers details//	 

	 
	//End// 
	
	//list out sub footer menu
	
	function sub_menu()
	{
		$this->db->connection_check();
		$this->db->where('cms_status','1');
		$this->db->where('cms_position','footer');
		$query = $this->db->get('tbl_cms');
		if($query->num_rows() > 0)
			return $query->result();
	}

	//header menu link
	function cms_content($names)
	{
		$this->db->connection_check();
		$this->db->where("cms_title",$names);
		$query = $this->db->get('tbl_cms');
		//$result = $this->db->query("select * from tbl_cms where cms_title='$names'")->row();
		if($query->num_rows() > 0)
			return $query->result();
	}

//Refer Friends

	function refer_friends()
	{
		$this->db->connection_check();
		$user_id = $this->session->userdata('user_id');
		$this->db->where('user_id',$user_id);
		$this->db->where('admin_status','');
		$query = $this->db->get('tbl_users');
		
		//$result = $this->db->get_where('tbl_users',array('user_id'=>'2'))->row('random_code');
		return $query->result();
	}

//mail function
	
	function invite_mail()
	{
		$this->db->connection_check();
		$this->load->library('email');

		$user_id 		= $this->session->userdata('user_id');
		$mail 			= $this->input->post('email');
		$random 		= $this->input->post('random');
		$mail_text  	= $this->input->post('mail_text');
		$type 			= $this->input->post('type');

		$mail_temp  	= $this->db->query("select * from tbl_mailtemplates where mail_id='5'")->row();
		$fe_cont  		= $mail_temp->email_template;
		//$subject 		= 'Seu amigo acaba de indicar você, vem ganhar dinheiro de volta!';
		//$subject 		= $mail_temp->email_subject;
		
		$name 		    = $this->db->query("select * from admin")->row();
		$admin_emailid  = $name->admin_email;
		$contact_number = $name->contact_number;
		$site_logo 		= $name->site_logo;
		$servername 	= base_url();
		$nows 			= date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */
		$sep_email 		= explode(',',$mail);
		
		$userdetails    = $this->front_model->userdetails($user_id);
		$first_name		= $userdetails->first_name;
		$last_name		= $userdetails->last_name;
		$email_id		= $userdetails->email;		 		

		$user_name   	= $first_name." ".$last_name;
		$ename      	= explode('@', $email_id);
		$usernames 		= $ename[0];


		if($first_name == '' && $last_name == '')
		{
			$username = $usernames;
		}
		else
		{
			$username  = $user_name;	
		}

		$subject = "Send Newsletter ".$username;

		foreach($sep_email as $emails)
		{
		
			$gd_api=array(
				'###EMAIL###'=>"! Here is " . $username ." (".$email_id.")",
				'###SITENAME###'=>$name->site_name,
				'###ADMINNO###'=>$contact_number,
				'###DATE###'=>$nows,
				'###CONTENT###'=>$mail_text."<br><br> <h2><a href='".base_url()."?ref=".$random."'>Click here to Activate</a> and get all the offers</h2>",
				'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
				'###URL###'=>'<a href='.$servername.$random.'></a>',
				'###MAILTYPE###'=>$type
				);
			
			if($email_id == 'fabriciocmello@gmail.com')
			{
				$reply_to_mail_id   = $email_id;
			}
			else
			{
				$reply_to_mail_id   = $admin_emailid;
			}


			$gd_message=strtr($fe_cont,$gd_api);
			//echo $gd_message;exit;
			$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				);
			$this->email->initialize($config);        
     		$this->email->set_newline("\r\n");
			$this->email->from($email_id, $first_name .' via '. $name->site_name.'!'); //$admin_emailid
			$this->email->reply_to($reply_to_mail_id);
			$this->email->to($emails);
			$this->email->subject($subject);
			$this->email->message($gd_message);
			$this->email->send();
			
			$this->email->print_debugger();
			//return true;
			$this->session->set_flashdata('success_msg',"Emails successfully Send to the selected contacts");
		}
			//return false;
	}


//insert contact form
	function contact_form()
	{
		//echo "<pre>"; print_r($_POST); exit;
		$this->load->library('email');
		$this->db->connection_check();
		$data = array(
		'name' => $this->input->post('name'),
		'email' => $this->input->post('email'),
		'message' => $this->input->post('message'),
		);
		$this->db->insert('tbl_contact',$data);
		
		//send email 		
		
		$this->db->where('admin_id',1);
		$admin_det = $this->db->get('admin');
		
		if($admin_det->num_rows > 0) 
		{    
			$admin = $admin_det->row();
			$admin_email = $admin->admin_email;
			$site_name = $admin->site_name;
			$admin_no = $admin->contact_number;
			$site_logo = $admin->site_logo;
		}
		
		$date 	   = date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */	
		$name 	   = $this->input->post('name');
		$useremail = $this->input->post('email');
		$message   = $this->input->post('message');
		$url 	   = "<img src=".base_url()."uploads/adminpro/".$site_logo.">";
		
		$this->db->where('mail_id',7);
		$mail_template = $this->db->get('tbl_mailtemplates');
		if($mail_template->num_rows > 0) 
		{        
		    $fetch 		= $mail_template->row();
		    $subject 	= $fetch->email_subject;  
		    $templete 	= $fetch->email_template;  
		   
			
			/*$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
			); */
			/*$config = Array(
			'protocol' => 'sendmail',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'vivek.developer@osiztechnologies.com',
			'smtp_pass' => 'iamnotlosero',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1',
			'wordwrap'  => TRUE
			);*/

			/*$this->email->initialize($config); 
			$this->email->set_newline("\r\n");       
			$this->email->from($useremail);
			$arrto   = array();
			$arrto[] = $useremail;
			$arrto[] = $admin_email;
			$this->email->to($arrto,$site_name.'!');
			$this->email->subject($subject);*/		   
		   
		    
			$data = array(
				'###CONTENT###'=>$message,
				'###COMPANYLOGO###' =>$url,
				'###SITENAME###'=>$site_name,
				'###ADMINNO###'=>$admin_no,
				'###DATE###'=>$date								
		    );
		  	 
		    $content_pop=strtr($templete,$data);		   
		   
		   	$to 	  = $admin_email;
	        $subject  = $subject;	
	        $message  = $content_pop;
	        $headers  = "From:$useremail \r\n";
	        $headers .= "MIME-Version: 1.0\r\n";
	        $headers .= "Content-type: text/html\r\n";

			$mails = mail($to,$subject,$message,$headers);
			//echo $mails; exit;
		   /*$this->email->message($content_pop);
		   $this->email->send();
		   $this->email->print_debugger();   */
              
			return true;                
		}
	}

////Referral Friends Network	

	function referral_network()
	{
		$this->db->connection_check();
		$user_id = $this->session->userdata('user_id');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get("referrals");
		if($query->num_rows() > 0)
			return $query->result();
	}

 	

//login (SL) 
	function login()
	{	
		$this->db->connection_check();
		$user_email = $this->input->post('email');
		$user_pwd 	= $this->input->post('password');

		$cashback_id 	  = $this->input->post('cashback_id');
		$cashback_details = $this->input->post('cashback_details');
		$expirydate  	  = $this->input->post('expirydate');
		$cashbackweb 	  = $this->input->post('cashbackweb');
		
		$this->db->where('email',$user_email);
		$this->db->where('password',$user_pwd);
		$this->db->where('admin_status','');
		$this->db->where('status',1);
		
		$query = $this->db->get('tbl_users');
		$numrows = $query->num_rows();
		if($numrows==1)
		{
			$fetch = $query->row();
			$user_id = $fetch->user_id;
			$user_email=$fetch->email;
			$status = $fetch->status;
			if($status =='0')
			{
				return 2;
			}
			else
			{	      

				//set session
				$this->session->set_userdata('user_id',$user_id);
				$this->session->set_userdata('user_email',$user_email);
				/*New code for popup count session details 20-11-16*/
				$this->session->set_userdata('ses_popup_count',0);
				/*End 20-11-16*/

				/*new code for unset the cashback exclusive condition 6-5-17*/
				$user_id = $this->session->userdata('user_id');
				if($user_id!="")
				{
					$newcashback_ex_id = $this->session->userdata('cash_ex_id');
			    	$cash_query        = $this->db->query("SELECT * from cashback_exclusive where id='$newcashback_ex_id'")->row();
					$expiry_dates      = strtotime($cash_query->expirydate); 
					$tday_dates        = strtotime(date('Y-m-d'));
					
					if($tday_dates > $expiry_dates)
					{
						/*Unset a cashack exclusive store details*/
				        $destroy_items = array('cash_ex_id' => '','storenames' => '','affiliate_urls' => '','link_name' => '','affiliate_names' => '', 'expiry_dates' => '','cashback_web' => '', 'analytics_info' => '');
				        $this->session->unset_userdata($destroy_items);
				        //$this->session->set_userdata('valid_purchase','expiry');
  						/*End*/
					}

					if($cash_query->valid_purchase_status == 1)
					{
				    	$getcashdetails = $this->getcashbacks($user_id);
				   		$cashback_count = count($getcashdetails);
					    if($cashback_count!=0)
					    {
					    	/*Unset a cashack exclusive store details*/
					        $destroy_items = array('cash_ex_id' => '','storenames' => '','affiliate_urls' => '','link_name' => '','affiliate_names' => '', 'expiry_dates' => '','cashback_web' => '', 'analytics_info' => '');
					        $this->session->unset_userdata($destroy_items);
					        $this->session->set_userdata('valid_purchase','expiry');
      						/*End*/
					    }  
					}
				}	    
				/*End 6-5-17*/

				//set cookie
				setcookie("user_id", $user_id, time() + (86400 * 30), "/"); // 86400 = 1 day
				setcookie("user_email", $user_email, time() + (86400 * 30), "/");

				return 1;
			}
	    }
		return 0;
	}
	

	function login_google($user_details,$new_cash_ex_id,$expiry_dates,$cashback_web,$cashback_details)
	{

		$this->db->connection_check();
		$user_email = $user_details->email;
		$user_fid   = $user_details->photoURL;
		 
		$selqry="SELECT * FROM  tbl_users where email='$user_email' and status=1 and admin_status=''"; // and profile ='$user_fid'
		$query=$this->db->query("$selqry");
		$numrows = $query->num_rows();
		if($numrows==1)
		{
			$fetch = $query->row();
			$user_id = $fetch->user_id;
			$user_email=$fetch->email;

			/*New code for update a already registered user details 12-1-17*/
			$data = array('first_name' =>$user_details->firstName,'last_name' =>$user_details->lastName,'profile'=>$user_fid);
			$this->db->where('user_id',$user_id);
			$update_qry = $this->db->update('tbl_users',$data);	
			/*End 12-1-17*/
			
			$this->session->set_userdata('user_id',$user_id);
			$this->session->set_userdata('user_email',$user_email);
			/*new code for cashback exclusive session start 18-7-16.*/
			$this->session->set_userdata('cashback_id',$new_cash_ex_id);
			$this->session->set_userdata('cashback_details',$cashback_details);
			$this->session->set_userdata('expirydate',$expiry_dates);
			$this->session->set_userdata('cashbackweb',$cashback_web);
			/*End*/ 

			//set cookie
			setcookie("user_id", $user_id, time() + (86400 * 30), "/"); // 86400 = 1 day
			setcookie("user_email", $user_email, time() + (86400 * 30), "/");

			return 1;
	    }
	    else
	    {
	    	/*New code added 20-11-16*/
	    	$cat_type_url  =  $this->session->userdata('cat_type');
	    	 
	    	$arrcat_type   = array('3454'=>2,'8765'=>3,'2345'=>4,'1647'=>5,'6536'=>6,'7486'=>7,'8362'=>8,'9326'=>9,'2695'=>10);
		    if (array_key_exists($cat_type_url,$arrcat_type))
			{
		    	$categorytype = $arrcat_type[$cat_type_url];
		    }
		    else
		    {	
		    	$categorytype = 1;
		    }
	    	  
	    	/*End 20-11-16*/

		    /*SOCIAL REFERENCE USER_ID END */
		    $random = $this->session->userdata('ses_random_ref');
			if($random !='')
			{
				$new_uid=$random;
				$this->db->where('random_code',$new_uid);
				$this->db->where('admin_status','');
				$query = $this->db->get('tbl_users');
				if($query->num_rows() > 0)
				{	
					$fetch = $query->row();
					$user_id = $fetch->user_id;
					$ref_cat_type = $fetch->referral_category_type;
					/*new code for category type details 15-4-17*/
					$selcatname    = $this->db->query("SELECT * from `referral_settings` where `ref_id`=$ref_cat_type")->row();
					$new_cat_types = $selcatname->new_ref_cat_types;
					/*End 15-4-17*/
				}
				else
				{
					$user_id 	   = 0;
					$new_cat_types = 1;
					$ref_cat_type  = 0;
				}
			}
			else
			{
				$user_id = 0;
				$new_cat_types = 1;

				if($ref_cat_type == '')
				{
					$ref_cat_type = 0;
				}
			}
			$refer = $user_id;
			/* SOCIAL REFERENCE USER_ID END */			

			/*New code for referral settings 23-5-17*/
			$this->db->where('ref_id',$ref_cat_type);
			$newref_details = $this->db->get('referral_settings');
			if($newref_details->num_rows() > 0)
			{
				$typeone_details = $newref_details->row();
				$ref_by_percentage = $typeone_details->ref_by_percentage;
				
			}
			if($ref_by_percentage == 1)
			{
				$typeONE_percent   = $typeone_details->ref_cashback;
				$typeONE_days 	   = $typeone_details->valid_months;
			}
			else
			{
				$typeONE_percent   = '';
				$typeONE_days 	   = 0;
				
			}

			/*End 23-5-17*/

		    $new_random = mt_rand(1000000,99999999);
		    //New code for random code 23-1-17//
		    $unic_code 	= $this->random_string(10);
		    //End//
		   	$user_email = $user_details->email;
		   	$date = date('Y-m-d h:i:s');
		   	$data = array(		
			'first_name'=>$user_details->firstName,
			'last_name'=>$user_details->lastName,
			'email'=>$user_email,
			'street'=>$user_details->address,
			'city'=>$user_details->city,
			'state'=>$user_details->region,
			'zipcode'=>$user_details->zip,
			'country'=>$user_details->country,
			'contact_no'=>$user_details->phone,
			'random_code'=>$new_random,
			'profile'=>$user_fid,
			'refer'=>$refer,
			'status'=>1,		
			'cashback_mail'=>1,
			'withdraw_mail'=>1,
			'referral_mail'=>1,
			'acbalance_mail'=>1,
			'support_tickets'=>1,
			'newsletter_mail'=>1,
			'referral_category_type'=>$new_cat_types,
			'date_added'=>$date,
			'unic_bonus_code'=>$unic_code,
			'ref_user_cat_type'=>$ref_cat_type,
			/*New code for first access popup status 20-3-17*/
			'first_ac_popup_status' => 1,	
			/*End 20-3-17*/
			/*new code for withdraw mail status 20-5-17*/
			'first_withdraw_status' => 0,
			'second_withdraw_status' => 0,
			/*End 20-5-17*/
			/*New code for referral settings for typeone 23-5-17*/
			'typeONE_percentual'=>$typeONE_percent,
			'typeONE_days'=>$typeONE_days
			/*End 23-5-17*/
			);
			$this->db->insert('tbl_users',$data);
			$insert_id = $this->db->insert_id();
			/*Unset session details for referral category details 22-11-16*/
			$this->session->unset_userdata('cat_type');
			/*end 22-11-16*/

			/*New code for referral settings 1-6-17*/
			//New code for (type 2 format) user referral cashback amount for referred user (Pending status) 8-9-16//
			if($user_id !=0)
			{
				$user_deta = $this->front_model->view_user($user_id);
				if($user_deta)
				{
					//echo "hai"; exit;
					//$refer = $user_deta->refer;
					//New code for refferal category type//
					$categorytype = $user_deta[0]->referral_category_type;
					
					$referral_mail  = $user_deta[0]->referral_mail;
					$reg_first_name = $user_deta[0]->first_name;
					$reg_last_name  = $user_deta[0]->last_name;
					$reg_email      = $user_deta[0]->email;

					if($reg_first_name == '' && $reg_last_name == '')
					{
						$ex_name  	   = explode('@',$reg_email);
						$reg_user_name = $ex_name[0]; 
					}
					else
					{
						$reg_user_name = $reg_first_name.''.$reg_last_name;
					}	

					
					$name   = $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();
					$status = $name->ref_by_rate;
					if($status == 1)
					{	
						$caspe = $name->ref_cashback_rate;
					}
					//End//
					
					if($status == 1)
					{
						//$cal_percent = ($trans_amount*$caspe)/100;
						$this->db->select_max('trans_id');
						$result   = $this->db->get('transation_details')->row();  
						$trans_id = $result->trans_id;
						$trans_id = $trans_id+1;
						$n9  	  = '5236555';
						$n12 	  = $n9 + $trans_id; 
						$now 	  = date('Y-m-d');
						$newid 	  = rand(1000,9999);
						//$newtransaction_id = md5($newid);
						$newtransaction_id = "user".$user_id."-referred".$insert_id;

						$data = array(		
						'transation_amount' => $caspe,
						'user_id' => $user_id,
						'transation_date' => $now,
						'transaction_date' => $now,
						'transation_id'=>$n12,
						'transation_reason' => "Pending Referal Payment",
						'mode' => 'Credited',
						'details_id'=>'',
						'table'=>'',
						'report_update_id'=>$newtransaction_id,
						'ref_user_tracking_id' =>$insert_id,
						'transation_status ' => 'Pending');
						$this->db->insert('transation_details',$data);
					
						//$caspe = number_format($caspe,2);
						$caspe = $this->front_model->currency_format($caspe);


						//mail for pending referral 24-3-17
						$this->db->where('admin_id',1);
						$admin_det = $this->db->get('admin');
						if($admin_det->num_rows >0) 
						{    
							$admin 		 = $admin_det->row();
							$admin_email = $admin->admin_email;
							$site_name   = $admin->site_name;
							$admin_no 	 = $admin->contact_number;
							$site_logo 	 = $admin->site_logo;
						}
						$date =date('Y-m-d');
						if($referral_mail == 1)
						{	
							$this->db->where('mail_id',4);
							$mail_template = $this->db->get('tbl_mailtemplates');
							if($mail_template->num_rows >0) 
							{
								$fetch = $mail_template->row();
								$subject = $fetch->email_subject;
								$templete = $fetch->email_template;
								$url = base_url().'my_earnings/';
								$unsuburls	 = base_url().'un-subscribe/referral/'.$user_id;
						   		$myaccount    = base_url().'minha-conta';
								
								$this->load->library('email');
								$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
								);
										
								$sub_data = array(
								'###SITENAME###'=>$site_name
								);
								
								$subject_new = strtr($subject,$sub_data);
								// $this->email->initialize($config);
								$this->email->set_newline("\r\n");
								$this->email->initialize($config);
								$this->email->from($admin_email,$site_name.'!');
								$this->email->to($reg_email);
								$this->email->subject($subject_new);
								$datas = array(
								'###NAME###'=>$reg_user_name,
								'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
								'###SITENAME###'=>$site_name,
								'###ADMINNO###'=>$admin_no,
								'###DATE###'=>date('y-m-d'),
								'###AMOUNT###'=>$caspe,
								'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
								'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>',
								'###STATUS###' => 'Pending'
								);
								$content_pop=strtr($templete,$datas);
								$this->email->message($content_pop);
								$this->email->send();  
							}
						}	
						//mail for pending referral	24-3-17
					}	
				}
			}
			//End refferal details 8-9-16//

			//New code for (type 3 format) user referral cashback amount count of referred user(Pending status) 9-9-16//
			$referrals = $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();	
			$ref_by_percentage  = $referrals->ref_by_percentage;
			$ref_by_rate 		= $referrals->ref_by_rate;
			$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

			$category_names     = str_replace('category','',$referrals->category_type);
			//$category_names     = ucfirst($referrals->category_type);
			
			//3** Bonus by Refferal Rate type//
			if($bonus_by_ref_rate == 1)
			{
				
				$newid 	= rand(1000,9999);
				$newtransaction_id = md5($newid);

				$n9      = '333445';
				$n12     = $n9 + $user_id;
				$now     = date('Y-m-d H:i:s');	
				$selqry  = "SELECT COUNT(user_id) as userid FROM transation_details where user_id=$user_id AND transation_reason ='Pending Referal Payment'"; 
				$query 	 =  $this->db->query("$selqry");
				$numrows = $query->num_rows();

				if($numrows > 0)
				{
					$fetch 			= $query->row();
					$usercount 		= $fetch->userid;
					$referrals      = $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();	
					$bonus_amount   = $referrals->ref_cashback_rate_bonus;
					$friends_count  = $referrals->friends_count;

					if($usercount == $friends_count)
					{	
						if($bonus_amount!='')
						{	 
							$referral_amt = $user_deta[0]->referral_amt;
							if($referral_amt == 0)
							{
								$data = array(			
								'transation_amount' => $bonus_amount,	
								'user_id' => $user_id,	
								'transation_date' => $now,
								'transaction_date' => $now,
								'transation_id'=>$n12,	
								'transation_reason' => 'Referral Bonus for Category '. ucfirst($category_names) .' User',	
								'mode' => 'Credited',
								'details_id'=>'',	
								'table'=>'',	
								'new_txn_id'=>0,
								'transation_status ' => 'Pending',
								'report_update_id'=>$newtransaction_id,
								'ref_bonus_type'=>$categorytype
								);	
								$this->db->insert('transation_details',$data);
							
								//User table update referral status//
								$data = array(		
								'referral_amt' => 1);
								$this->db->where('user_id',$user_id);
								$update_qry = $this->db->update('tbl_users',$data);	
								//End//
							

								/*New code for referral bonus amount mail notification 19-5-17*/
								$this->db->where('admin_id',1);
								$admin_det = $this->db->get('admin');
								if($admin_det->num_rows >0) 
								{    
									$admin 		 = $admin_det->row();
									$admin_email = $admin->admin_email;
									$site_name   = $admin->site_name;
									$admin_no 	 = $admin->contact_number;
									$site_logo 	 = $admin->site_logo;
								}
								$date =date('Y-m-d');
								if($referral_mail == 1)
								{	
									$this->db->where('mail_id',23);
									$mail_template = $this->db->get('tbl_mailtemplates');
									if($mail_template->num_rows >0) 
									{
										$fetch = $mail_template->row();
										$subject = $fetch->email_subject;
										$templete = $fetch->email_template;
										$url = base_url().'my_earnings/';
										$unsuburls	 = base_url().'un-subscribe/referral/'.$user_id;
								   		$myaccount    = base_url().'minha-conta';
										
										$this->load->library('email');
										$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
										);
												
										$sub_data = array(
										'###SITENAME###'=>$site_name
										);
										
										$subject_new = strtr($subject,$sub_data);
										// $this->email->initialize($config);
										$this->email->set_newline("\r\n");
										$this->email->initialize($config);
										$this->email->from($admin_email,$site_name.'!');
										$this->email->to($reg_email);
										$this->email->subject($subject_new);
										$datas = array(
										'###NAME###'=>$reg_user_name,
										'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>date('y-m-d'),
										'###AMOUNT###'=>str_replace('.', ',', bcdiv($bonus_amount,1,2)),
										'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
										'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>',
										'###STATUS###' => 'Pending' 
										);
										$content_pop=strtr($templete,$datas);
										$this->email->message($content_pop);
										$this->email->send();  
									}
								}	
								/*End 19-5-17*/
							}		
						}	
					}
				} 
			}
			//End referral details 9-9-16//
			/*End 1-6-17*/


			if($new_uid)
			{      
				$this->db->where('random_code',$new_uid);
				$this->db->where('admin_status','');
				$query = $this->db->get('tbl_users');
				if($query->num_rows() > 0)
				{	
					$fetch 		= $query->row();
					$user_id 	= $fetch->user_id;
					$email 		= $fetch->email;
					$refcattype = $fetch->referral_category_type;
					
					$datas = array(
					'user_id' => $user_id,
					'user_email' => $email,
					'referral_email' => $user_email,
					'status' => 'Ativa',
					'date_added' => $date
					);
					$this->db->insert('referrals',$datas);


					//New add code 1-6-17
					/*New code for referral upgrade mail notification content 19-4-17*/
					$first_name    = $fetch->first_name;
					$last_name     = $fetch->last_name;

					if($first_name == '' && $last_name == '')
					{
						$ex_name  = explode('@', $fetch->email);
						$username = $ex_name[0]; 
					}
					else
					{
						$username = $first_name.' '.$last_name;
					}	

					//Referral count details
					$ref_counts    = $this->db->query("SELECT count(`referral_id`) as `ref_counts` from `referrals` where `user_id`=$user_id")->row();
					$totref_counts = $ref_counts->ref_counts;
					//Referral table details
					$referrals 			  = $this->db->query("SELECT * from `referral_settings` where `ref_id`='$refcattype'")->row();	
					$ref_cat_upgrade_type = $referrals->ref_cat_upgrade_type;
					//echo  $ref_cat_upgrade_type;
					/*Admin details*/ 
					$this->db->where('admin_id',1);
					$admin_det = $this->db->get('admin');
					if($admin_det->num_rows() >0) 
					{    
						$admin 		 = $admin_det->row();
						$admin_email = $admin->admin_email;
						$site_name 	 = $admin->site_name;
						$admin_no 	 = $admin->contact_number;
						$site_logo 	 = $admin->site_logo;
					}
					/*Admin details*/
					
					if($ref_cat_upgrade_type != 0)
					{	 
						if($ref_cat_upgrade_type == 1)
						{	
							
							$ref_cat_upgrade_first_count    = $referrals->ref_cat_upgrade_first_count;
							$cong_mail_content 			    = $referrals->cong_mail_content;

							$datas = array(
								'###user-name###'=>$username,
								'###ref-number###'=>$ref_cat_upgrade_first_count,
								'###user-email###'=>$fetch->email
								);

							$contents = strtr($cong_mail_content,$datas);

							if($ref_cat_upgrade_first_count == $totref_counts)
							{
								$this->db->where('mail_id',21);
								$mail_template = $this->db->get('tbl_mailtemplates');
				
								if($mail_template->num_rows() >0) 
								{        
								    $fetch    	 = $mail_template->row();
								    $subject  	 = $fetch->email_subject; //"Referral category congrats mail";//$fetch->email_subject;  
								    $templete 	 = $fetch->email_template;  

								    $subject_data = array(
								    	'###USER-NAME###' => $username
								    );
								    $new_subject  = strtr($subject,$subject_data);

								   	$this->load->library('email'); 

									$config = Array(
									'mailtype'  => 'html',
									'charset'   => 'utf-8',
									);    

						     		$this->email->set_newline("\r\n");
									$this->email->initialize($config);        
									$this->email->from($admin_email,$site_name.'!');
									$this->email->to($email);
									$this->email->subject($new_subject);
								   	$data = array(
										'###EMAIL###'=>$username,
										'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###CONTENT###'=>$contents
								   	);
								   
								    $content_pop=strtr($templete,$data);	
								    $this->email->message($content_pop);
								    $this->email->send();               
								}
							}
						}

						if($ref_cat_upgrade_type == 2)
						{
							$ref_cat_upgrade_second_count   = $referrals->ref_cat_upgrade_second_count;
							$cong_upgrade_mail_content 	    = $referrals->cong_upgrade_mail_content;
							$ref_cat_upgrade_second_cattype = $referrals->ref_cat_upgrade_second_cattype;

							$datas = array(
								'###user-name###'=>$username,
								'###ref-number-upgrade###'=>$ref_cat_upgrade_second_count,
								'###user-email###'=>$fetch->email
								);

							$contents = strtr($cong_upgrade_mail_content,$datas);

							if($ref_cat_upgrade_second_count == $totref_counts)
							{
								
								/*Upgrade category details in tbl_users table*/
								$data = array(		
									'referral_category_type'  => $ref_cat_upgrade_second_cattype);
									$this->db->where('user_id',$user_id);
									$update_qry= $this->db->update('tbl_users',$data);
								/*End*/

								$this->db->where('mail_id',21);
								$mail_template = $this->db->get('tbl_mailtemplates');
				
								if($mail_template->num_rows() >0) 
								{        
								    $fetch    	 = $mail_template->row();
								    $subject  	 = $fetch->email_subject; //"UPGRADE Referral category congrats mail";//$fetch->email_subject;  
								    $templete 	 = $fetch->email_template;  

								    $subject_data = array(
								    	'###USER-NAME###' => $username
								    );
								    $new_subject  = strtr($subject,$subject_data);

								   	$this->load->library('email'); 

									$config = Array(
									'mailtype'  => 'html',
									'charset'   => 'utf-8',
									);    

						     		$this->email->set_newline("\r\n");
									$this->email->initialize($config);        
									$this->email->from($admin_email,$site_name.'!');
									$this->email->to($email);
									$this->email->subject($new_subject);
								   	$data = array(
										'###EMAIL###'=>$username,
										'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###CONTENT###'=>$contents
								   	);
								   
								    $content_pop=strtr($templete,$data);	
								    $this->email->message($content_pop);
								    $this->email->send();               
								}
							}
						}

						if($ref_cat_upgrade_type == 3)
						{	
							$ref_cat_upgrade_third_count   = $referrals->ref_cat_upgrade_third_count;
							$ref_cat_upgrade_fourth_count  = $referrals->ref_cat_upgrade_fourth_count;

							if($ref_cat_upgrade_third_count == $totref_counts)
							{
								
								$cong_mail_content 			    = $referrals->cong_mail_content;
								$ref_cat_upgrade_fourth_cattype = $referrals->ref_cat_upgrade_fourth_cattype;

								$datas = array(
									'###user-name###' =>$username,
									'###ref-number###'=>$ref_cat_upgrade_third_count,
									'###user-email###'=>$fetch->email
									);

								$contents = strtr($cong_mail_content,$datas);

								/*Upgrade category details in tbl_users table*/
								/*$data = array(		
									'referral_category_type'  => $ref_cat_upgrade_fourth_cattype);
									$this->db->where('user_id',$user_id);
									$update_qry= $this->db->update('tbl_users',$data);*/
								/*End*/

								$this->db->where('mail_id',21);
								$mail_template = $this->db->get('tbl_mailtemplates');
				
								if($mail_template->num_rows() >0) 
								{        
								    $fetch    	  = $mail_template->row();
								    $subject  	  = $fetch->email_subject;  
								    $templete 	  = $fetch->email_template;  
								    
								    $subject_data = array(
								    	'###USER-NAME###' => $username
								    );

								    $new_subject  = strtr($subject,$subject_data);

								   	$this->load->library('email'); 

									$config = Array(
									'mailtype'  => 'html',
									'charset'   => 'utf-8',
									);    

						     		$this->email->set_newline("\r\n");
									$this->email->initialize($config);        
									$this->email->from($admin_email,$site_name.'!');
									$this->email->to($email);
									$this->email->subject($new_subject);
								   	$data = array(
										'###EMAIL###'=>$username,
										'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###CONTENT###'=>$contents
								   	);
								   
								    $content_pop=strtr($templete,$data);	
								    $this->email->message($content_pop);
								    $this->email->send();               
								}
							}
							if($ref_cat_upgrade_fourth_count == $totref_counts)
							{
								
								$cong_upgrade_mail_content 	    = $referrals->cong_upgrade_mail_content;
								$ref_cat_upgrade_fourth_cattype = $referrals->ref_cat_upgrade_fourth_cattype;

								$datas = array(
									'###user-name###'=>$username,
									'###ref-number-upgrade###'=>$ref_cat_upgrade_fourth_count,
									'###user-email###'=>$fetch->email
									);

								$contents = strtr($cong_upgrade_mail_content,$datas);

								/*Upgrade category details in tbl_users table*/
								$data = array(		
									'referral_category_type'  => $ref_cat_upgrade_fourth_cattype);
									$this->db->where('user_id',$user_id);
									$update_qry= $this->db->update('tbl_users',$data);
								/*End*/

								$this->db->where('mail_id',21);
								$mail_template = $this->db->get('tbl_mailtemplates');
				
								if($mail_template->num_rows() >0) 
								{        
								    $fetch    	 = $mail_template->row();
								    $subject  	 = $fetch->email_subject; //"UPGRADE Referral category congrats mail";
								    $templete 	 = $fetch->email_template;  

								    $subject_data = array(
								    	'###USER-NAME###' => $username
								    );
								    
								    $new_subject  = strtr($subject,$subject_data);

								   	$this->load->library('email'); 

									$config = Array(
									'mailtype'  => 'html',
									'charset'   => 'utf-8',
									);    

						     		$this->email->set_newline("\r\n");
									$this->email->initialize($config);        
									$this->email->from($admin_email,$site_name.'!');
									$this->email->to($email);
									$this->email->subject($new_subject);
								   	$data = array(
										'###EMAIL###'=>$username,
										'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###CONTENT###'=>$contents
								   	);
								   
								    $content_pop=strtr($templete,$data);	
								    $this->email->message($content_pop);
								    $this->email->send();               
								}
							}
						}
					}	
					/*End 19-4-17*/
					//End add code 1-6-17

				}
			}

			/* Social site refer table END */
			$this->session->set_userdata('user_id',$insert_id);
			$this->session->set_userdata('user_email',$user_email);

			//set cookie
			setcookie("user_id", $insert_id, time() + (86400 * 30), "/"); // 86400 = 1 day
			setcookie("user_email", $user_email, time() + (86400 * 30), "/");

			$this->db->where('admin_id',1);
			$admin_det = $this->db->get('admin');
			if($admin_det->num_rows >0) 
			{    
				 $admin = $admin_det->row();
				 $admin_email = $admin->admin_email;
				 $site_name = $admin->site_name;
				 $admin_no = $admin->contact_number;
				  $site_logo = $admin->site_logo;
			}
			//if($admin->activate_method == 0)
			//{
				$date =date('d/m/Y');
				//$first_name = $this->input->post('first_name');
				$last_name  = $user_details->lastName;
				$user_email = $user_email;
				$ex_name    = explode('@', $user_email);
				$first_name = $ex_name[0]; 

				$this->db->where('mail_id',18);
				$mail_template = $this->db->get('tbl_mailtemplates');
			
				if($mail_template->num_rows >0) 
				{        
				    $fetch    	 = $mail_template->row();
				    $subject  	 = $fetch->email_subject; 
				    $templete 	 = $fetch->email_template;  
				    $regurl	 	 = base_url().'verify-account/'.$new_random;
				    $unsuburl	 = base_url().'un-subscribe-signup/'.$new_random;
				    
				    $Content = 'Thank you for registering with <a href='.base_url().'>'.$site_name.'</a>';

				   	$this->load->library('email'); 

					$config = Array(
					'mailtype'  => 'html',
					'charset'   => 'utf-8',
					);
					// $this->email->initialize($config);        
		     		$this->email->set_newline("\r\n");
					$this->email->initialize($config);        
					$this->email->from($admin_email,$site_name.'!');
					$this->email->to($user_email);
					$this->email->subject($subject);
				   	$data = array(
						'###EMAIL###'=>$first_name,
						'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
						'###SITENAME###'=>$site_name,
						'###ADMINNO###'=>$admin_no,
						'###DATE###'=>$date,
						'###CONTENT###'=>$Content
				   	);
				   
				    $content_pop=strtr($templete,$data);	
				    $this->email->message($content_pop);
				    $this->email->send();
				    /* echo $content_pop;
				    exit; */
				    return 1;   
			    }             
			//}
			return 1;
	    }		
	}


	//registration form..	
	function register()
	{
		
		$this->db->connection_check();
		$new_random    = mt_rand(1000000,99999999);
		$user_email    = $this->input->post('user_email');
		$date 	  	   = date('Y-m-d h:i:s');
		$uni_id   	   = $this->input->post('uni_id');
		$ref_id   	   = $uni_id;
		$pagename 	   = $this->input->post('pagename');
		$cat_type_url  = $this->input->post('categorytype');
	    $unic_code 	   = $this->random_string(10);
	 	
	 	$cash_ex_link  = $this->input->post('cash_ex_link');
	 	
		
		//echo "<pre>";print_r($_POST); exit;
	   	
	   	//New code hide 15-4-17
	    /*$arrcat_type   = array('3454'=>2,'8765'=>3,'2345'=>4,'1647'=>5,'6536'=>6,'7486'=>7,'8362'=>8,'9326'=>9,'2695'=>10);
	    if (array_key_exists($cat_type_url,$arrcat_type))
		{
	    	$categorytype = $arrcat_type[$cat_type_url];
	    }
	    else
	    {	
	    	$categorytype = 1;
	    }*/
	    //End 15-4-17


		if($pagename)
		{
			$new_uid = $uni_id;
		}
		else
		{
			$new_uid = $ref_id;
		}
		 
		if($new_uid)
		{	
			//echo $new_uid; exit;
			$this->db->where('random_code',$new_uid);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows() > 0)
			{	
				$fetch 	      = $query->row();
				$user_id 	  = $fetch->user_id;
				$ref_cat_type = $fetch->referral_category_type;

				/*new code for category type details 15-4-17*/
				$selcatname    = $this->db->query("SELECT * from `referral_settings` where `ref_id`=$ref_cat_type")->row();
				$new_cat_types = $selcatname->new_ref_cat_types;
				/*End 15-4-17*/
			}
			else
			{
				$user_id 	   = 0;
				$new_cat_types = 1;
				$ref_cat_type  = 0;
			}
			 
		}
		else
		{
			$user_id 	   = 0;
			$new_cat_types = 1;

			if($ref_cat_type == '')
			{
				$ref_cat_type = 0;
			}
		}
		//echo $ref_cat_type; exit;
		/*New code for referral settings 23-5-17*/
		$this->db->where('ref_id',$ref_cat_type);
		$newref_details = $this->db->get('referral_settings');
		if($newref_details->num_rows() > 0)
		{
			$typeone_details = $newref_details->row();
			$ref_by_percentage = $typeone_details->ref_by_percentage;	
		}
		if($ref_by_percentage == 1)
		{
			$typeONE_percent   = $typeone_details->ref_cashback;
			$typeONE_days 	   = $typeone_details->valid_months;
		}
		else
		{
			$typeONE_percent   = 0;
			$typeONE_days 	   = 0;	
		}
		/*End 23-5-17*/


		$data = array(		
		'email'=>$user_email,
		'password'=>$this->input->post('user_pwd'),
		'random_code'=>$new_random,
		'refer'=>$user_id,
		'cashback_mail'=>1,
		'withdraw_mail'=>1,
		'referral_mail'=>1,
		'acbalance_mail'=>1,
		'support_tickets'=>1,
		'newsletter_mail'=>1,
		'referral_category_type'=>$new_cat_types, //$categorytype
		'date_added'=>$date,
		'unic_bonus_code'=>$unic_code,
		'ref_user_cat_type'=>$ref_cat_type,
		/*New code for first access popup status 20-3-17*/
		'first_ac_popup_status' => 1,	
		/*End 20-3-17*/
		/*new code for withdraw mail status 20-5-17*/
		'first_withdraw_status' => 0,
		'second_withdraw_status' => 0,
		/*End 20-5-17*/
		/*New code for referral settings for typeone 23-5-17*/
		'typeONE_percentual'=>$typeONE_percent,
		'typeONE_days'=>$typeONE_days
		/*End 23-5-17*/
		);
		
		$this->db->insert('tbl_users',$data,$datas);
		$insert_id = $this->db->insert_id();
		
		/*New code for register via cashback exclusive link 22-4-17*/
		if(!empty($cash_ex_link))
		{	
			$cashdetails 	   	   = $this->front_model->cashback_ex_details($cash_ex_link);
			$cash_ex_signcount     = $cashdetails->cash_sign_up_counts;
			$newcash_ex_signcount  = $cash_ex_signcount + 1;
	    	
	    	$data = array(		
			'cash_sign_up_counts' => $newcash_ex_signcount);
			$this->db->where('id',$cash_ex_link);
			$update_qry = $this->db->update('cashback_exclusive',$data);
		}
		/*end 22-4-17*/


		//New code for (type 2 format) user referral cashback amount for referred user (Pending status) 8-9-16//
		if($user_id !=0)
		{
			$user_deta = $this->front_model->view_user($user_id);
			if($user_deta)
			{
				//echo "hai"; exit;
				//$refer = $user_deta->refer;
				//New code for refferal category type//
				$categorytype = $user_deta[0]->referral_category_type;
				
				$referral_mail  = $user_deta[0]->referral_mail;
				$reg_first_name = $user_deta[0]->first_name;
				$reg_last_name  = $user_deta[0]->last_name;
				$reg_email      = $user_deta[0]->email;

				if($reg_first_name == '' && $reg_last_name == '')
				{
					$ex_name  	   = explode('@',$reg_email);
					$reg_user_name = $ex_name[0]; 
				}
				else
				{
					$reg_user_name = $reg_first_name.''.$reg_last_name;
				}	

				
				$name   = $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();
				$status = $name->ref_by_rate;
				if($status == 1)
				{	
					$caspe = $name->ref_cashback_rate;
				}
				//End//
				
				if($status == 1)
				{
					//$cal_percent = ($trans_amount*$caspe)/100;
					$this->db->select_max('trans_id');
					$result   = $this->db->get('transation_details')->row();  
					$trans_id = $result->trans_id;
					$trans_id = $trans_id+1;
					$n9  	  = '5236555';
					$n12 	  = $n9 + $trans_id; 
					$now 	  = date('Y-m-d');
					$newid 	  = rand(1000,9999);
					//$newtransaction_id = md5($newid);
					$newtransaction_id = "user".$user_id."-referred".$insert_id;

					$data = array(		
					'transation_amount' => $caspe,
					'user_id' => $user_id,
					'transation_date' => $now,
					'transaction_date' => $now,
					'transation_id'=>$n12,
					'transation_reason' => "Pending Referal Payment",
					'mode' => 'Credited',
					'details_id'=>'',
					'table'=>'',
					'report_update_id'=>$newtransaction_id,
					'ref_user_tracking_id' =>$insert_id,
					'transation_status ' => 'Pending');
					$this->db->insert('transation_details',$data);
				
					//$caspe = number_format($caspe,2);
					$caspe = $this->front_model->currency_format($caspe);


					//mail for pending referral 24-3-17
					$this->db->where('admin_id',1);
					$admin_det = $this->db->get('admin');
					if($admin_det->num_rows >0) 
					{    
						$admin 		 = $admin_det->row();
						$admin_email = $admin->admin_email;
						$site_name   = $admin->site_name;
						$admin_no 	 = $admin->contact_number;
						$site_logo 	 = $admin->site_logo;
					}
					$date =date('Y-m-d');
					if($referral_mail == 1)
					{	
						$this->db->where('mail_id',4);
						$mail_template = $this->db->get('tbl_mailtemplates');
						if($mail_template->num_rows >0) 
						{
							$fetch = $mail_template->row();
							$subject = $fetch->email_subject;
							$templete = $fetch->email_template;
							$url = base_url().'my_earnings/';
							$unsuburls	 = base_url().'un-subscribe/referral/'.$user_id;
					   		$myaccount    = base_url().'minha-conta';
							
							$this->load->library('email');
							$config = Array(
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
							);
									
							$sub_data = array(
							'###SITENAME###'=>$site_name
							);
							
							$subject_new = strtr($subject,$sub_data);
							// $this->email->initialize($config);
							$this->email->set_newline("\r\n");
							$this->email->initialize($config);
							$this->email->from($admin_email,$site_name.'!');
							$this->email->to($reg_email);
							$this->email->subject($subject_new);
							$datas = array(
							'###NAME###'=>$reg_user_name,
							'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
							'###SITENAME###'=>$site_name,
							'###ADMINNO###'=>$admin_no,
							'###DATE###'=>date('y-m-d'),
							'###AMOUNT###'=>$caspe,
							'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>',
							'###STATUS###' => 'Pending'
							);
							$content_pop=strtr($templete,$datas);
							$this->email->message($content_pop);
							$this->email->send();  
						}
					}	
					//mail for pending referral	24-3-17
				}	
			}
		}
		//End refferal details 8-9-16//

		//New code for (type 3 format) user referral cashback amount count of referred user(Pending status) 9-9-16//
		$referrals = $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();	
		$ref_by_percentage  = $referrals->ref_by_percentage;
		$ref_by_rate 		= $referrals->ref_by_rate;
		$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

		$category_names     = str_replace('category','',$referrals->category_type);
		//$category_names     = ucfirst($referrals->category_type);
		
		//3** Bonus by Refferal Rate type//
		if($bonus_by_ref_rate == 1)
		{
			
			$newid 	= rand(1000,9999);
			$newtransaction_id = md5($newid);

			$n9      = '333445';
			$n12     = $n9 + $user_id;
			$now     = date('Y-m-d H:i:s');	
			$selqry  = "SELECT COUNT(user_id) as userid FROM transation_details where user_id=$user_id AND transation_reason ='Pending Referal Payment'"; 
			$query 	 =  $this->db->query("$selqry");
			$numrows = $query->num_rows();

			if($numrows > 0)
			{
				$fetch 			= $query->row();
				$usercount 		= $fetch->userid;
				$referrals      = $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();	
				$bonus_amount   = $referrals->ref_cashback_rate_bonus;
				$friends_count  = $referrals->friends_count;

				if($usercount == $friends_count)
				{	
					if($bonus_amount!='')
					{	 
						$referral_amt = $user_deta[0]->referral_amt;
						if($referral_amt == 0)
						{
							$data = array(			
							'transation_amount' => $bonus_amount,	
							'user_id' => $user_id,	
							'transation_date' => $now,
							'transaction_date' => $now,
							'transation_id'=>$n12,	
							'transation_reason' => 'Referral Bonus for Category '. ucfirst($category_names) .' User',	
							'mode' => 'Credited',
							'details_id'=>'',	
							'table'=>'',	
							'new_txn_id'=>0,
							'transation_status ' => 'Pending',
							'report_update_id'=>$newtransaction_id,
							'ref_bonus_type'=>$categorytype
							);	
							$this->db->insert('transation_details',$data);
						
							//User table update referral status//
							$data = array(		
							'referral_amt' => 1);
							$this->db->where('user_id',$user_id);
							$update_qry = $this->db->update('tbl_users',$data);	
							//End//
						

							/*New code for referral bonus amount mail notification 19-5-17*/
							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin 		 = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name   = $admin->site_name;
								$admin_no 	 = $admin->contact_number;
								$site_logo 	 = $admin->site_logo;
							}
							$date =date('Y-m-d');
							if($referral_mail == 1)
							{	
								$this->db->where('mail_id',23);
								$mail_template = $this->db->get('tbl_mailtemplates');
								if($mail_template->num_rows >0) 
								{
									$fetch = $mail_template->row();
									$subject = $fetch->email_subject;
									$templete = $fetch->email_template;
									$url = base_url().'my_earnings/';
									$unsuburls	 = base_url().'un-subscribe/referral/'.$user_id;
							   		$myaccount    = base_url().'minha-conta';
									
									$this->load->library('email');
									$config = Array(
									'mailtype'  => 'html',
									'charset'   => 'utf-8',
									);
											
									$sub_data = array(
									'###SITENAME###'=>$site_name
									);
									
									$subject_new = strtr($subject,$sub_data);
									// $this->email->initialize($config);
									$this->email->set_newline("\r\n");
									$this->email->initialize($config);
									$this->email->from($admin_email,$site_name.'!');
									$this->email->to($reg_email);
									$this->email->subject($subject_new);
									$datas = array(
									'###NAME###'=>$reg_user_name,
									'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
									'###SITENAME###'=>$site_name,
									'###ADMINNO###'=>$admin_no,
									'###DATE###'=>date('y-m-d'),
									'###AMOUNT###'=>str_replace('.', ',', bcdiv($bonus_amount,1,2)),
									'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
									'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>',
									'###STATUS###' => 'Pending' 
									);
									$content_pop=strtr($templete,$datas);
									$this->email->message($content_pop);
									$this->email->send();  
								}
							}	
							/*End 19-5-17*/


						}		
					}	
				}
			} 
		}
		//End referral details 9-9-16//

		//New code for activation method 18-4-16//
		$admindetailssss = $this->front_model->getadmindetails_main();
		$activate  = $admindetailssss->activate_method;
		 
		//End//

		/*New code for Add a subscriber users list 31-8-16*/
		$this->db->where('subscriber_email',$user_email);
		$query = $this->db->get('subscribers');
		if($query->num_rows() == 0)
		{
			$date = date('Y-m-d h:m:s');
			$data = array(
			'subscriber_email' => $user_email,
			'subscriber_status' => '1',
			'date_subscribed' => $date
			);
			$this->db->insert('subscribers',$data);
		}
		/*End 31-8-16*/
		
		//$uni_id = $this->input->post('uni_id');
		
		if($new_uid)
		{
			$this->db->where('random_code',$new_uid);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows() > 0)
			{	
				$fetch      = $query->row();
				$user_id    = $fetch->user_id;
				$email      = $fetch->email;
				$refcattype = $fetch->referral_category_type;
				
				$datas = array(
				'user_id' 	 	 => $user_id,
				'user_email' 	 => $email,
				'referral_email' => $user_email,
				'status' 		 => 'Ativa',
				'date_added' 	 => $date
				);
				$this->db->insert('referrals',$datas);


				/*New code for referral upgrade mail notification content 19-4-17*/
				$first_name    = $fetch->first_name;
				$last_name     = $fetch->last_name;

				if($first_name == '' && $last_name == '')
				{
					$ex_name  = explode('@', $fetch->email);
					$username = $ex_name[0]; 
				}
				else
				{
					$username = $first_name.' '.$last_name;
				}	

				//Referral count details
				$ref_counts    = $this->db->query("SELECT count(`referral_id`) as `ref_counts` from `referrals` where `user_id`=$user_id")->row();
				$totref_counts = $ref_counts->ref_counts;
				//Referral table details
				$referrals 			  = $this->db->query("SELECT * from `referral_settings` where `ref_id`='$refcattype'")->row();	
				$ref_cat_upgrade_type = $referrals->ref_cat_upgrade_type;
				//echo  $ref_cat_upgrade_type;
				/*Admin details*/ 
				$this->db->where('admin_id',1);
				$admin_det = $this->db->get('admin');
				if($admin_det->num_rows() >0) 
				{    
					$admin 		 = $admin_det->row();
					$admin_email = $admin->admin_email;
					$site_name 	 = $admin->site_name;
					$admin_no 	 = $admin->contact_number;
					$site_logo 	 = $admin->site_logo;
				}
				/*Admin details*/
				
				if($ref_cat_upgrade_type != 0)
				{	 
					if($ref_cat_upgrade_type == 1)
					{	
						
						$ref_cat_upgrade_first_count    = $referrals->ref_cat_upgrade_first_count;
						$cong_mail_content 			    = $referrals->cong_mail_content;

						$datas = array(
							'###user-name###'=>$username,
							'###ref-number###'=>$ref_cat_upgrade_first_count,
							'###user-email###'=>$fetch->email
							);

						$contents = strtr($cong_mail_content,$datas);

						if($ref_cat_upgrade_first_count == $totref_counts)
						{
							$this->db->where('mail_id',21);
							$mail_template = $this->db->get('tbl_mailtemplates');
			
							if($mail_template->num_rows() >0) 
							{        
							    $fetch    	 = $mail_template->row();
							    $subject  	 = $fetch->email_subject; //"Referral category congrats mail";//$fetch->email_subject;  
							    $templete 	 = $fetch->email_template;  

							    $subject_data = array(
							    	'###USER-NAME###' => $username
							    );
							    $new_subject  = strtr($subject,$subject_data);

							   	$this->load->library('email'); 

								$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
								);    

					     		$this->email->set_newline("\r\n");
								$this->email->initialize($config);        
								$this->email->from($admin_email,$site_name.'!');
								$this->email->to($email);
								$this->email->subject($new_subject);
							   	$data = array(
									'###EMAIL###'=>$username,
									'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
									'###SITENAME###'=>$site_name,
									'###ADMINNO###'=>$admin_no,
									'###DATE###'=>$date,
									'###CONTENT###'=>$contents
							   	);
							   
							    $content_pop=strtr($templete,$data);	
							    $this->email->message($content_pop);
							    $this->email->send();               
							}
						}
					}

					if($ref_cat_upgrade_type == 2)
					{
						$ref_cat_upgrade_second_count   = $referrals->ref_cat_upgrade_second_count;
						$cong_upgrade_mail_content 	    = $referrals->cong_upgrade_mail_content;
						$ref_cat_upgrade_second_cattype = $referrals->ref_cat_upgrade_second_cattype;

						$datas = array(
							'###user-name###'=>$username,
							'###ref-number-upgrade###'=>$ref_cat_upgrade_second_count,
							'###user-email###'=>$fetch->email
							);

						$contents = strtr($cong_upgrade_mail_content,$datas);

						if($ref_cat_upgrade_second_count == $totref_counts)
						{
							
							/*Upgrade category details in tbl_users table*/
							$data = array(		
								'referral_category_type'  => $ref_cat_upgrade_second_cattype);
								$this->db->where('user_id',$user_id);
								$update_qry= $this->db->update('tbl_users',$data);
							/*End*/

							$this->db->where('mail_id',21);
							$mail_template = $this->db->get('tbl_mailtemplates');
			
							if($mail_template->num_rows() >0) 
							{        
							    $fetch    	 = $mail_template->row();
							    $subject  	 = $fetch->email_subject; //"UPGRADE Referral category congrats mail";//$fetch->email_subject;  
							    $templete 	 = $fetch->email_template;  

							    $subject_data = array(
							    	'###USER-NAME###' => $username
							    );
							    $new_subject  = strtr($subject,$subject_data);

							   	$this->load->library('email'); 

								$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
								);    

					     		$this->email->set_newline("\r\n");
								$this->email->initialize($config);        
								$this->email->from($admin_email,$site_name.'!');
								$this->email->to($email);
								$this->email->subject($new_subject);
							   	$data = array(
									'###EMAIL###'=>$username,
									'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
									'###SITENAME###'=>$site_name,
									'###ADMINNO###'=>$admin_no,
									'###DATE###'=>$date,
									'###CONTENT###'=>$contents
							   	);
							   
							    $content_pop=strtr($templete,$data);	
							    $this->email->message($content_pop);
							    $this->email->send();               
							}
						}
					}

					if($ref_cat_upgrade_type == 3)
					{	
						$ref_cat_upgrade_third_count   = $referrals->ref_cat_upgrade_third_count;
						$ref_cat_upgrade_fourth_count  = $referrals->ref_cat_upgrade_fourth_count;

						if($ref_cat_upgrade_third_count == $totref_counts)
						{
							
							$cong_mail_content 			    = $referrals->cong_mail_content;
							$ref_cat_upgrade_fourth_cattype = $referrals->ref_cat_upgrade_fourth_cattype;

							$datas = array(
								'###user-name###' =>$username,
								'###ref-number###'=>$ref_cat_upgrade_third_count,
								'###user-email###'=>$fetch->email
								);

							$contents = strtr($cong_mail_content,$datas);

							/*Upgrade category details in tbl_users table*/
							/*$data = array(		
								'referral_category_type'  => $ref_cat_upgrade_fourth_cattype);
								$this->db->where('user_id',$user_id);
								$update_qry= $this->db->update('tbl_users',$data);*/
							/*End*/

							$this->db->where('mail_id',21);
							$mail_template = $this->db->get('tbl_mailtemplates');
			
							if($mail_template->num_rows() >0) 
							{        
							    $fetch    	  = $mail_template->row();
							    $subject  	  = $fetch->email_subject;  
							    $templete 	  = $fetch->email_template;  
							    
							    $subject_data = array(
							    	'###USER-NAME###' => $username
							    );

							    $new_subject  = strtr($subject,$subject_data);

							   	$this->load->library('email'); 

								$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
								);    

					     		$this->email->set_newline("\r\n");
								$this->email->initialize($config);        
								$this->email->from($admin_email,$site_name.'!');
								$this->email->to($email);
								$this->email->subject($new_subject);
							   	$data = array(
									'###EMAIL###'=>$username,
									'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
									'###SITENAME###'=>$site_name,
									'###ADMINNO###'=>$admin_no,
									'###DATE###'=>$date,
									'###CONTENT###'=>$contents
							   	);
							   
							    $content_pop=strtr($templete,$data);	
							    $this->email->message($content_pop);
							    $this->email->send();               
							}
						}
						if($ref_cat_upgrade_fourth_count == $totref_counts)
						{
							
							$cong_upgrade_mail_content 	    = $referrals->cong_upgrade_mail_content;
							$ref_cat_upgrade_fourth_cattype = $referrals->ref_cat_upgrade_fourth_cattype;

							$datas = array(
								'###user-name###'=>$username,
								'###ref-number-upgrade###'=>$ref_cat_upgrade_fourth_count,
								'###user-email###'=>$fetch->email
								);

							$contents = strtr($cong_upgrade_mail_content,$datas);

							/*Upgrade category details in tbl_users table*/
							$data = array(		
								'referral_category_type'  => $ref_cat_upgrade_fourth_cattype);
								$this->db->where('user_id',$user_id);
								$update_qry= $this->db->update('tbl_users',$data);
							/*End*/

							$this->db->where('mail_id',21);
							$mail_template = $this->db->get('tbl_mailtemplates');
			
							if($mail_template->num_rows() >0) 
							{        
							    $fetch    	 = $mail_template->row();
							    $subject  	 = $fetch->email_subject; //"UPGRADE Referral category congrats mail";
							    $templete 	 = $fetch->email_template;  

							    $subject_data = array(
							    	'###USER-NAME###' => $username
							    );
							    
							    $new_subject  = strtr($subject,$subject_data);

							   	$this->load->library('email'); 

								$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
								);    

					     		$this->email->set_newline("\r\n");
								$this->email->initialize($config);        
								$this->email->from($admin_email,$site_name.'!');
								$this->email->to($email);
								$this->email->subject($new_subject);
							   	$data = array(
									'###EMAIL###'=>$username,
									'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
									'###SITENAME###'=>$site_name,
									'###ADMINNO###'=>$admin_no,
									'###DATE###'=>$date,
									'###CONTENT###'=>$contents
							   	);
							   
							    $content_pop=strtr($templete,$data);	
							    $this->email->message($content_pop);
							    $this->email->send();               
							}
						}
					}
				}	
				/*End 19-4-17*/
			}
		}
		
		/*New code for referral for cashback_exclusive via registered user 30-7-16*/
		$user_id 		  = $this->session->userdata('user_id'); 
		$cashback_details = $this->session->userdata('link_name');	
		$cashbackdetails  = $this->front_model->cashback_exclusive_details($cashback_details);
		$Email_id         = $cashbackdetails->user_email;
		$userdetailss     = $this->db->query("select * from tbl_users where email='$Email_id'")->row();
		$newuserid        = $userdetailss->user_id;
		if($user_id=='')
		{
			if($cashback_details !='')
			{
				$datas = array(
				'user_id' => $newuserid,
				'user_email' => $Email_id,
				'referral_email' => $user_email,
				'status' => 'Ativa',
				'date_added' => $date
				);
				$this->db->insert('referrals',$datas);
			}
		}
		/*End 30-7-16*/
		
		//return true;
		//send email 
		
		//New code for mail notification//
		//echo "hai".$activate; echo "<br>";
		if($activate == 0)
		{
		
			$this->db->where('admin_id',1);
			$admin_det = $this->db->get('admin');
			if($admin_det->num_rows >0) 
			{    
				$admin 		 = $admin_det->row();
				$admin_email = $admin->admin_email;
				$site_name 	 = $admin->site_name;
				$admin_no 	 = $admin->contact_number;
				$site_logo 	 = $admin->site_logo;
			}
			
			$date =date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */
			$first_name = $this->input->post('first_name');
			$last_name  = $this->input->post('last_name');
			$user_email = $this->input->post('user_email');
		
			$this->db->where('mail_id',1);
			$mail_template = $this->db->get('tbl_mailtemplates');
			
			if($mail_template->num_rows >0) 
			{        
			    $fetch    	 = $mail_template->row();
			    $subject  	 = $fetch->email_subject;  
			    $templete 	 = $fetch->email_template;  
			    $regurl	 	 = base_url().'verify-account/'.$new_random;
			    $unsuburl	 = base_url().'un-subscribe-signup/'.$new_random;

			   	$this->load->library('email'); 

				$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				);
				// $this->email->initialize($config);        
	     		$this->email->set_newline("\r\n");
				$this->email->initialize($config);        
				$this->email->from($admin_email,$site_name.'!');
				$this->email->to($user_email);
				$this->email->subject($subject);
			   	$data = array(
					'###USERNAME###'=>$first_name,
					'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
					'###SITENAME###'=>$site_name,
					'###ADMINNO###'=>$admin_no,
					'###DATE###'=>$date,
					'###LINK###'=>'<a href='.$regurl.'>'.$regurl.'</a>',
					'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>'
			   	);
			   
			    $content_pop=strtr($templete,$data);	
			    $this->email->message($content_pop);
			    $this->email->send();
			    /* echo $content_pop;
			    exit; */
			    //return true;                
			}
			return 0;
		}
		if($activate == 1)
		{
			//echo "hai"; exit;
			$datas = array('status'=>1);
			$this->db->where('user_id',$insert_id);
			$this->db->where('admin_status','');
			$update_qry = $this->db->update('tbl_users',$datas);

			$this->session->set_userdata('user_id',$insert_id);
			$this->session->set_userdata('user_email',$user_email);


			/*new code for unset the cashback exclusive condition 6-5-17*/
			$user_id = $this->session->userdata('user_id');
			if($user_id!="")
			{
				$newcashback_ex_id = $this->session->userdata('cash_ex_id');
		    	$cash_query        = $this->db->query("SELECT * from cashback_exclusive where id='$newcashback_ex_id'")->row();
				$expiry_dates      = strtotime($cash_query->expirydate); 
				$tday_dates        = strtotime(date('Y-m-d'));

				if($cash_query->valid_purchase_status == 1)
				{
			    	$getcashdetails = $this->getcashbacks($user_id);
			   		$cashback_count = count($getcashdetails);
				    if($cashback_count!=0)
				    {
				    	/*Unset a cashack exclusive store details*/
				        $destroy_items = array('cash_ex_id' => '','storenames' => '','affiliate_urls' => '','link_name' => '','affiliate_names' => '', 'expiry_dates' => '','cashback_web' => '', 'analytics_info' => '');
				        $this->session->unset_userdata($destroy_items);
				        $this->session->set_userdata('valid_purchase','expiry');
  						/*End*/
				    }  
				}
			}	    
			/*End 6-5-17*/

			$this->db->where('admin_id',1);
			$admin_det = $this->db->get('admin');
			if($admin_det->num_rows >0) 
			{    
				$admin 		 = $admin_det->row();
				$admin_email = $admin->admin_email;
				$site_name 	 = $admin->site_name;
				$admin_no 	 = $admin->contact_number;
				$site_logo 	 = $admin->site_logo;
			}

			$date =date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */
			$this->db->where('mail_id',18);
			$mail_template = $this->db->get('tbl_mailtemplates');
		
			if($mail_template->num_rows >0) 
			{        
			    $fetch    	 = $mail_template->row();
			    //$subject  	 = 'User Registration - Social Login'; 
			    $subject 	 = $fetch->email_subject;
			    $templete 	 = $fetch->email_template;  
			    $Content 	 = 'Thank you for registering with <a href='.base_url().'>'.$site_name.'</a>';
			   	$this->load->library('email'); 
				$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				);       
	     		$this->email->set_newline("\r\n");
				$this->email->initialize($config);        
				$this->email->from($admin_email,$site_name.'!');
				$this->email->to($user_email);
				$this->email->subject($subject);
			   	$data = array(
					'###EMAIL###'=>$first_name,
					'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
					'###SITENAME###'=>$site_name,
					'###ADMINNO###'=>$admin_no,
					'###DATE###'=>$date,
					'###CONTENT###'=>$Content
			   	);
			   
			    $content_pop=strtr($templete,$data);	
			    $this->email->message($content_pop);
			    $this->email->send();
			    /*echo $user_email; echo "<br>";
			    echo $content_pop;
			    exit;*/
			    return 1;   
		    }
			return 1;
		}	
	}


		function random_string($length) 
		{
		$key = '';
		$keys = array_merge(range(0, 9), range('A', 'Z'));

		for ($i = 0; $i < $length; $i++) {
		$key .= $keys[array_rand($keys)];
		}

		return $key;
		}



	
	//check Email 
	function check_email($email)
	{
		$this->db->connection_check();
		$this->db->where('email',$email);
		$this->db->where('admin_status','');
		
		$qry = $this->db->get('tbl_users');
		$numrows1 = $qry->num_rows();
		if($numrows1 == 0)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	//check IFSC code in DB 12-4-17 
	function check_ifsccode($check_cpf)
	{
		$this->db->connection_check();
		$this->db->where('ifsc_code',$ifsc_code);
		$this->db->where('admin_status','');
		
		$qry = $this->db->get('tbl_users');
		$numrows1 = $qry->num_rows();
		if($numrows1 == 0)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}

	/*new ifsc code validate function 23-3-17*/
	function check_cpf($check_cpf) 
	{
    	
    	$cpf = $check_cpf;
	    //Check if the number was informed
	    if(empty($cpf)) 
	    {
	        return false;
	    }
	 
	    // removes possible masks in the number eg: turns  911.139.032-87   into   91113903287
	    $cpf = ereg_replace('[^0-9]', '', $cpf);
	    $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
	     
	    // check if the number informed has 11 digits
	    if (strlen($cpf) != 11) {
	        return false;
	    }
	    // check if one of sequences bellow is typed 
	    // Case positive, returns FALSE
	    else if ($cpf == '00000000000' || 
	        $cpf == '11111111111' || 
	        $cpf == '22222222222' || 
	        $cpf == '33333333333' || 
	        $cpf == '44444444444' || 
	        $cpf == '55555555555' || 
	        $cpf == '66666666666' || 
	        $cpf == '77777777777' || 
	        $cpf == '88888888888' || 
	        $cpf == '99999999999') {
	        return false;
	     // Do the math to verify if the IFSC_number is alid or not
	     } else {   
	         
	        for ($t = 9; $t < 11; $t++) {
	             
	            for ($d = 0, $c = 0; $c < $t; $c++) {
	                $d += $cpf{$c} * (($t + 1) - $c);
	            }
	            $d = ((10 * $d) % 11) % 10;
	            if ($cpf{$c} != $d) {
	                return false;
	            }
	        }
	        return true;
	    }
	} 
	

	
	//get all countries
	function get_allcounties()
	{
		$this->db->connection_check();
		$fetcountry = $this->db->get('country');
		if($fetcountry->num_rows > 0)
		{		
			return $fetcountry->result();
		}
		return false;
	}
	//get the particular users details
	function edit_account($user_id)
	{
		$this->db->connection_check();
	   $this->db->where('user_id',$user_id);
	   $this->db->where('admin_status','');
		$user_details = $this->db->get('tbl_users');
		if($user_details->num_rows > 0){
			return $user_details->row();
		}
		return false;	
	}
	//New code for Bank details page//

	function bank_details()
	{
		$this->db->connection_check();
		$this->db->order_by('bankid','desc');
		$result = $this->db->get('tbl_banknames');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	//End//
	//update the user details by SATz 
	function update_account()
	{
		//echo "<pre>";print_r($_POST); exit;
		$this->db->connection_check();
		$edit_id = $this->input->post('user_id');
		$contact_no = str_replace("_","",$this->input->post('contact_no'));
		$cell_no    = str_replace("_","",$this->input->post('cell_no'));
		
		$data = array(		
		'first_name' => $this->input->post('first_name'),
		'last_name' => $this->input->post('last_name'),
		'sex' => $this->input->post('sex'),
		'street' => $this->input->post('street'),
		'streetnumber' => $this->input->post('streetnumber'),
		'city' => $this->input->post('city'),
		'celular_no'=> $cell_no,
		'state' => $this->input->post('state'),
		'zipcode' => $this->input->post('zipcode'),
		'country' => $this->input->post('country'),
		'contact_no'=> $contact_no,
		'complemento'=> $this->input->post('complemento'),
		'bairro'=> $this->input->post('bairro')
		
		);
		
		$this->db->where('user_id',$edit_id);
		$this->db->where('admin_status','');
		$update_qry = $this->db->update('tbl_users',$data);
		if($update_qry)
		{
			return true;
		}
		else 
		{ 
			return false;
		}	
	}	
	
	function add_rating()
	{
		$this->db->connection_check();
		$cop_id = $this->input->post('id');
		$rating = $this->input->post('rating');
		$data = array(		
		'rating' =>  $rating
		);
		
		$this->db->where('affiliate_id',$cop_id);
		$this->db->where('affiliate_status','1');
		$update_qry = $this->db->update('affiliates',$data);
		if($update_qry)
		{
			return true;
		}
		else 
		{ 
			return false;
		}	
	}	
	
	function getunicbonuscount()
	{
		$userids 	= $this->input->post('userid');
		$this->db->where('user_id',$userids);
		$user_details = $this->db->get('tbl_users');
		//echo $this->db->last_query();
		$userdetails = $user_details->result();
		return  $userdetails[0]->bonus_benefit;
	}
	
	

	//New code for one time benefit bonus aded for user//
	function onetime_benefit()
	{
		$this->db->connection_check();
		$userids 	= $this->input->post('userid');

		$this->db->where('user_id',$userids);
		$user_details = $this->db->get('tbl_users');
		if($user_details->num_rows >0) 
		{	 
			$fetch    	  	  = $user_details->row();
		    $balance 	  	  = $fetch->balance;  
			$amount       	  = $this->input->post('amt');
		 	$total_amt    	  = $balance + $amount;
		 	$bonus_status 	  = $fetch->bonus_benefit;
		 	$report_update_id = "uniquebonus".$userids;

		 	$dataa = array(		
						'transation_reason' => "Unic bonus amonut Added",		
						'transation_amount' => $amount,		
						'mode' 				=> "debited",
						'transation_status' => "Credited",			
						'transaction_date'  => date('Y-m-d'),
						'transation_date'   => date('Y-m-d'),			
						'user_id' 			=> $userids,
						'report_update_id'  => $report_update_id	
						);
						$this->db->insert('transation_details',$dataa);
			if($bonus_status == 0)
			{
				$data = array('bonus_benefit' => 1,
				'balance'=> $total_amt
				);
				$this->db->where('user_id',$userids);
				$this->db->where('admin_status','');
				$update_qry = $this->db->update('tbl_users',$data);
			}	
			$this->session->set_flashdata('bonus_alert',"Unic bônus added Successfully!");
			if($update_qry)
			{	 
				return 1;

			}
			else 
			{ 
				return 0;
			}
		} 
	}
	//End//

	//New code email notification page update//
	function notifymail_update()
	{	
		//echo "hai<pre>"; print_r($_POST); exit;
		$this->db->connection_check();
		$this->load->library('email');
		$postuser 	= $this->input->post('user_id');
		$caseback 	= $this->input->post('cashback');
		$withdraw 	= $this->input->post('withdraw');
		$referral   = $this->input->post('referral');
		$acbalance  = $this->input->post('acbalance');
		$newsletter = $this->input->post('newsletter');
		$support_tickets = $this->input->post('support_tickets');
		//echo $caseback;

		if((isset($caseback)) && ($caseback =='on'))
		{
			$caseback = '1';
		}
		else
		{
			$caseback = '0';
		}
		if((isset($withdraw)) && ($withdraw =='on'))
		{
			$withdraw = '1';
		}
		else
		{
			$withdraw = '0';
		}
		if((isset($referral)) && ($referral =='on'))
		{
			$referral = '1';
		}
		else
		{
			$referral = '0';
		}
		if((isset($acbalance)) && ($acbalance =='on'))
		{
			$acbalance = '1';
		}
		else
		{
			$acbalance = '0';
		}
		if((isset($newsletter)) && ($newsletter =='on'))
		{
			$newsletter = '1';
		}
		else
		{
			$newsletter = '0';
		}

		if((isset($support_tickets)) && ($support_tickets =='on'))
		{
			$support_tickets = '1';
		}
		else
		{
			$support_tickets = '0';
		}
		
		$news_email = $this->db->get_where('tbl_users',array('user_id'=>$postuser))->row('newsletter_mail');

		$data = array(		
		'cashback_mail'   => $caseback,
		'withdraw_mail'   => $withdraw,
		'referral_mail'   => $referral,
		'acbalance_mail'  => $acbalance,
		'newsletter_mail' => $newsletter,
		'support_tickets' => $support_tickets
		);

		$this->db->where('user_id',$postuser);
		$this->db->where('admin_status','');
		$update_qry = $this->db->update('tbl_users',$data);
		if($update_qry)
		{
			
			/*New code for update a subscribers table 3-9-16*/
				$email 		= $this->db->get_where('tbl_users',array('user_id'=>$postuser))->row('email');
				
				$data = array(		
				'subscriber_status' =>$newsletter,
				);
				$this->db->where('subscriber_email',$email);	
				$this->db->update('subscribers',$data);
			/*end */	

			//echo $newsletter; exit; 
			/*New code for email notification for newsletter subscriber 31-8-2016*/
			if($news_email == 1 && $newsletter == 0)
			{ 

				/*$admin_email = $this->db->get_where('admin', array('admin_id'=>'1'))->row('admin_email');
				$message 	 = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">
								Você está recebendo esta beacuse você é um membro Pingou . Você pode apenas <a href='.base_url().'un-subscribe/subscribers>un-subscrever</a> se você quiser.
								</span>';
				$emailid 	= $this->db->get_where('tbl_users',array('user_id'=>$postuser))->row('email');
				
				if($emailid) 
				{	
					
					$subject = "Your Un Subscribers Notification"; 
					$config = Array(
					'mailtype'  => 'html',
					'charset'   => 'utf-8'
					);  	 

					$this->email->set_newline("\r\n");
					$this->email->initialize($config);
					$this->email->from($admin_email);
					$this->email->to($emailid);
					//$this->email->bcc($emails);
					$this->email->subject($subject);
					$this->email->message($message);
					$this->email->send();
	        		$this->email->print_debugger();			
        		}*/
			}
			/*end 31-8-16*/
			return true;
		}
		else 
		{ 
			return false;
		}
	}
	
	//End//
	
	//update password
	function update_password()
	{
		$this->db->connection_check();
		 $old_password = $this->input->post('old_password');
		 $new_password = $this->input->post('new_password');
		 $id = $this->input->post('user_id');
		
			$where = array('password'=>$old_password,'user_id'=>$id);
			$this->db->where($where);
			
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(
				'password'=>$new_password
				);
				//print_r($data);
				//exit;
				$this->db->where('user_id',$id);	
				$this->db->update('tbl_users',$data);
				return true;
			}    
			else 
			{     
				return false;
			}			
	}
	
	//bankpayment form
	function bankpayment()
	{
		$this->db->connection_check();
		 $new_id = $this->input->post('user_id');
		 $con_pwd = $this->input->post('con_pwd');
		 
		$where = array('password'=>$con_pwd,'user_id'=>$new_id);
		$this->db->where($where);
		$query = $this->db->get('tbl_users');
		if($query->num_rows >= 1) 
		{
			$data = array(		
			'account_holder' => $this->input->post('act_holder'),
			'bank_name' => $this->input->post('bank_name'),
			'branch_name' => $this->input->post('bank_brch_name'),
			'account_number' => $this->input->post('act_no'),
			'ifsc_code' => $this->input->post('ifsc_code')
			);
			//print_r($data);
			//exit;
			$this->db->where('user_id',$new_id);	
			$this->db->update('tbl_users',$data);
			return true;
		}    
		else 
		{     
			return false;
		}			
	}

	//bankpayment form
	function bankpayment_ifsc()
	{
		$this->db->connection_check();
		 $new_id = $this->input->post('user_id');
		 $con_pwd = $this->input->post('con_pwd');
		 
		$where = array('password'=>$con_pwd,'user_id'=>$new_id);
		$this->db->where($where);
		$query = $this->db->get('tbl_users');
		if($query->num_rows >= 1) 
		{
			$data = array(		
			'account_holder' => $this->input->post('act_holder'),
			'bank_name' => $this->input->post('bank_name'),
			'branch_name' => $this->input->post('bank_brch_name'),
			'account_number' => $this->input->post('act_no')
			);
			$this->db->where('user_id',$new_id);	
			$this->db->update('tbl_users',$data);
			return true;
		}    
		else 
		{     
			return false;
		}			
	}

	//get all state
	function get_state($user_id)
	{
		$this->db->connection_check();
		$result = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('country');
		//print_r($result);
		
		$this->db->where('country_id',$result);
		$res = $this->db->get('state');
		if($res->num_rows > 0){
			return $res->result();
		}
		return false;	
		
	}
	//cheque_payment
	function cheque_payment()
	{
		$this->db->connection_check();
		$us_id = $this->input->post('user_id');
		$con_pwd = $this->input->post('cheque_confirm_pwd');
		
		$where = array('password'=>$con_pwd,'user_id'=>$us_id);
		$this->db->where($where);
		$query = $this->db->get('tbl_users');
		if($query->num_rows >= 1) 
		{
			$data = array(		
			'cheque_full_name' => $this->input->post('cheque_full_name'),
			'cheque_full_address' => $this->input->post('cheque_full_adr'),
			'cheque_city' => $this->input->post('cheque_city'),
			'cheque_state' => $this->input->post('cheque_state'),
			'cheque_postel_code' => $this->input->post('cheque_postal_code'),
			'cheque_contact_no' => $this->input->post('cheque_contact_no')
			);
			//print_r($data);
			//exit;
			$this->db->where('user_id',$us_id);	
			$this->db->update('tbl_users',$data);
			return true;
		}    
		else 
		{     
			return false;
		}	
	}
	
	//forget password
	function forgetpassword()
	{
		$this->db->connection_check();

		$email = $this->input->post('email');
		//send email 
		$this->load->library('email');
		$this->db->where('admin_id',1);
		$admin_det = $this->db->get('admin');
		
		if($admin_det->num_rows >0) 
		{    
			$admin 	     = $admin_det->row();
			$admin_email = $admin->admin_email;
			$site_name   = $admin->site_name;
			$admin_no 	 = $admin->contact_number;
			$site_logo   = $admin->site_logo;
		}
		
		$date =date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */
		
		$this->db->where('email',$email);
		$this->db->where('admin_status','');
		$query = $this->db->get('tbl_users');
		if($query->num_rows >0) 
		{
			$getuser 	= $query->row();
			$user_id    = $getuser->user_id;
			$password   = $getuser->password;
			$first_name = $getuser->first_name;
			$last_name  = $getuser->last_name;
		    $user_email = $getuser->email;
			$random_id  = $getuser->random_code;
			$this->db->where('mail_id',2);
			$mail_template = $this->db->get('tbl_mailtemplates');
			if($mail_template->num_rows >0) 
			{        
			    $fetch 	  = $mail_template->row();
			    $subject  = $fetch->email_subject;  
			    $templete = $fetch->email_template;  
			  	$regurl   = base_url().'password-reset/'.insep_encode($user_id);
			    
			 	$config = Array(
				 'mailtype' => 'html',
				  'charset' => 'utf-8',
				);
     			// $this->email->initialize($config);        
     			$this->email->set_newline("\r\n");
			    
			    $this->email->initialize($config);        
			    $this->email->from($admin_email,$site_name.'!');
			    $this->email->to($user_email);
			    $this->email->subject($subject);

			    $data = array(
					'###USERNAME###'=>$first_name.' '.$last_name,
					'###PASSWORD###'=>$password,
					'###SITENAME###'=>$site_name,
					'###ADMINNO###'=>$admin_no,
					'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
					'###DATE###'=>$date,
					'###LINK###'=>'<a href='.$regurl.'>'.'Click here'.'</a>'
				);
			   
			   $content_pop=strtr($templete,$data); 
			   $this->email->message($content_pop);
			   $this->email->send();	          
			}
			return true;          
		}
		else
		{
			return false;                
		}   	
	}
	
	//reset_password
	function reset_password($user_id)
	{
		/*echo $user_id;
		exit;*/
		$this->db->connection_check();
		if(!isset($user_id))
		{
			$user_id = $this->input->post('user_id');
		}
		$new_password = $this->input->post('new_password');
		$confirm_password = $this->input->post('confirm_password');
					
		$where = array('user_id'=>$user_id);
		// print_r($where);
		// exit;
		$this->db->where($where);
		
		$query = $this->db->get('tbl_users');
		if($query->num_rows >0) 
		{
			$data = array(
			'password'=>$new_password
			);
			//print_r($data);
			//exit; 
			$this->db->where('user_id',$user_id);	
			$this->db->update('tbl_users',$data);
			return true;
		}    
		else 
		{     
			return false;
		}			
	}
	//refer_friends
	function get_random($user_id)
	{
		$this->db->connection_check();
		$res =$this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('random_code');
			
			return $res;
	}
//End (SL)
//End (SL)

/*********************Nathan Start*************************/
/******Nov 19 th*********/
	function get_category_details($categoryurl) //get_category_details
	{
		$this->db->connection_check();
			$this->db->where('category_url',$categoryurl);
			$query = $this->db->get('categories');
			if($query->num_rows >= 1)
			{
			   $row = $query->row();
			   return $row;
			}
			return false;
	}
	
	function get_category_details_byid($categoryid) //get_category_details
	{
		$this->db->connection_check();
			$this->db->where('category_id',$categoryid);
			$query = $this->db->get('categories');
			if($query->num_rows >= 1)
			{
			   $row = $query->row();
			   return $row;
			}
			return false;
	}
	
	

	function get_subcategories($category_id)
	{
		$this->db->connection_check();
			$this->db->where('cate_id',$category_id);
			$query = $this->db->get('sub_categories');
			if($query->num_rows >= 1)
			{
			   $row = $query->row();
			   return $query->result();
			}
			return false;
	}
	
	function get_coupons($categories)
	{
		$this->db->connection_check();
		$k=0;
		foreach($categories as $catenames)
		{
			if($k==0)
			{
				$this->db->like('title', $catenames);				
			}
			else
			{
				$this->db->or_like('title', $catenames); 
			}
			$k++;
		}
		
		
		
	}
	
	function get_stores_list($category_id)
	{
		$this->db->connection_check();
		$this->db->order_by("sort_order", "ASC");		
		$this->db->where("FIND_IN_SET('$category_id',store_categorys) !=", 0);
		$this->db->where('affiliate_status','1');
		$query = $this->db->get('affiliates');
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
	
	function referal_percentage()
	{
		$this->db->connection_check();
		$query = $this->db->get('admin');
		if($query->num_rows >= 1)
		{
		   return $query->row('referral_cashback');
		}
	}


//New code for satheesh//

function referal__category_percentage($category)
{
	$this->db->connection_check();
	$this->db->where('ref_id',$category);
	$query = $this->db->get('referral_settings');
	if($query->num_rows >= 1)
	{
	   return $query->row('ref_cashback');
	}
}

function referal__category_description($category)
{
	$this->db->connection_check();
	$this->db->where('ref_id',$category);
	$query = $this->db->get('referral_settings');
	if($query->num_rows >= 1)
	{
	   return $query->row('cat_description');
	}
}
	
function referal__category()
{
	$this->db->connection_check();
	$user_id = $this->session->userdata('user_id');
	if($user_id)
	{
        $this->db->where('user_id',$user_id);
		$query = $this->db->get('tbl_users');
		if($query->num_rows >= 1)
		{
		   return $query->row('referral_category_type');
		}
	}
}

/*New code for referral category details 29-4-17*/
function get_referral_settings($user_cat_id)
{
	$this->db->connection_check();
	$this->db->where('ref_id',$user_cat_id);
	$query = $this->db->get('referral_settings');
	if($query->num_rows > 0)
	{
	   return $query->row();
	}
	return false;
}
/*End 29-4-17*/
	
//End//

	//New code for referral percentage//


	//End//
	
	// get all faqs..
	function get_allfaqs(){
		$this->db->connection_check();
		$this->db->where('status','1');
		$allfaqs = $this->db->get('tbl_faq');
		if($allfaqs->num_rows > 0)
        {
            $row = $allfaqs->row();
            return $allfaqs->result();
        }
		else
		{
			return false;
		}
	}
	
	function user_balance($user_id=null)
	{
		$this->db->connection_check();
		if($user_id!="")
		{
			$this->db->where('user_id',$user_id);
			$this->db->where('admin_status','');
			$allfaqs = $this->db->get('tbl_users');
			return $allfaqs->row("balance");
		}
		else
		{
			return 0;
		}
		
	}
	
	
	function missing_cashback($user_id=null)
	{
		$this->db->connection_check();
		if($user_id!="")
		{	
			$this->db->order_by('cashback_id','desc');
			$this->db->where('user_id',$user_id);
			//$this->db->where('missing_reason','Missing Cashback');
			$miss_cashbacks = $this->db->get('missing_cashback');
			//echo $this->db->last_query();die;
			return $miss_cashbacks->result();
		}
		else
		{
			return 0;
		}
	}

	/*New code 5-6-17*/
	function missing_cashback_using_id($cash_id)
	{
		$this->db->connection_check();
		$user_id = $this->session->userdata('user_id');
		if($user_id!="")
		{	
			$this->db->order_by('cashback_id','desc');
			$this->db->where('cashback_id',$cash_id);
			$miss_cashbacks = $this->db->get('missing_cashback');
			//echo $this->db->last_query();exit;
			//$row = $miss_cashbacks->row();
			return $miss_cashbacks->row();
		}
		else
		{
			return 0;
		}
	}
	/*End 5-6-17*/

	function minimum_withdraw()
	{
		$this->db->connection_check();
			$minimum_cashback = $this->db->get('admin');
			return $minimum_cashback->row("minimum_cashback");
	}

	/*new code for request minimum withdraw amount 3-4-17*/
	function request_minimum_withdraw()
	{
		$this->db->connection_check();
			$req_minimum_cashback = $this->db->get('admin');
			return $req_minimum_cashback->row("remain_minimum_with_amt");
	}
	/*end 3-4-17*/
	
	function my_payments($user_id)
	{
		$this->db->connection_check();		
		if($user_id!="")
		{	
			$this->db->where('user_id',$user_id);
			$this->db->order_by('withdraw_id','desc');
			$miss_cashbacks = $this->db->get('withdraw');
			
			return $miss_cashbacks->result();
		}
		else
		{
			return 0;
		}
	}
	
	function update_user_balance($userid,$requestpay,$ifsc_code)
	{
		//balance
		$this->db->connection_check();
		$requestpay    = str_replace(',','.', $requestpay);
		$userbalance   = $this->user_balance($userid);
		$userbalance   = str_replace(',','.', $userbalance);  
		$new_balnce    = $userbalance - $requestpay; 
		$accountholder = $this->db->get_where('tbl_users',array('user_id'=>$userid))->row('account_holder');
		$ubank_name    = $this->db->get_where('tbl_users',array('user_id'=>$userid))->row('bank_name');
		$branch_name   = $this->db->get_where('tbl_users',array('user_id'=>$userid))->row('branch_name');
		$accountnumber = $this->db->get_where('tbl_users',array('user_id'=>$userid))->row('account_number');
		/*New code Changes 2-8-16*/
		$bank_name     = $this->db->get_where('tbl_banknames',array('bank_id'=>$ubank_name))->row('bank_name');
		$ifsc_code     = str_replace(array('.', '-'), '' , $ifsc_code);
		/*End*/

		$User_details = $this->front_model->userdetails($userid);

		$data = array(		
		'balance' => $new_balnce);
		$this->db->where('user_id',$userid);
		$update_qry = $this->db->update('tbl_users',$data);
		if($update_qry)
		{
			$getwith_details = $this->db->query('SELECT count(`user_id`) as `user_count` from `withdraw` where user_id="'.$userid.'"')->row();
			$withdraw_count  = $getwith_details->user_count;

			if($withdraw_count == 0)
			{
				$data = array(		
				'first_withdraw_mail_status'  => 1);
				$this->db->where('user_id',$userid);
				$update_qry= $this->db->update('tbl_users',$data);
			}

			$now = date('Y-m-d H:i:s');
			// withdraw
			$data = array(		
			'requested_amount' => $requestpay,
			'user_id'          => $userid,
			'date_added'       => $now,
			'account_holder'   => $accountholder,
			'bank_name'        => $bank_name,
			'branch_name'      => $branch_name,
			'account_number'   => $accountnumber,
			'ifsc_code' 	   => $ifsc_code,
			'bank_id'		   => $ubank_name,
			'status ' => 'Requested');
			$this->db->insert('withdraw',$data);

			$withdraw_mail = $User_details->withdraw_mail;
			/*new code for username details 5-1-17*/
			$firstname     = $User_details->first_name;
			$lastname 	   = $User_details->last_name;
			if($firstname == '' && $lastname == '')
			{
				$ex_name  = explode('@',$User_details->email);
				$username = $ex_name[0]; 
			}
			else
			{
				$username = $firstname.' '.$lastname;
			}	
			/*End 5-1-17*/
			if($withdraw_mail == 1)
			{
				$mail_temp 		 = $this->db->query("select * from tbl_mailtemplates where mail_id='17'")->row();
				$fe_cont   		 = $mail_temp->email_template;
				$admindetailssss = $this->front_model->getadmindetails_main(); 
				//$subject 		 = $admindetailssss->site_name." has recieved your Withdraw Ticket";
				$subject 		 = $mail_temp->email_subject;
				$name 			 = $this->db->query("select * from admin")->row();
				$admin_emailid   = $name->admin_email;
				$site_logo 		 = $name->site_logo;
				$site_name  	 = $name->site_name;
				$contact_number  = $name->contact_number;
				$servername 	 = base_url();
				$unsuburl     	 = base_url().'un-subscribe/withdraw/'.$userid;

				$nows = date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */
				$this->load->library('email');
				
				//Pilaventhiran 05/05/2016 START
				$mail_text 			 = 'Requested amount R$ ' .$this->front_model->currency_format($requestpay);
				$see_status_withdraw = "<a href='".base_url()."resgate'>status do resgate</a>";
				$gd_api=array(
					'###ADMINNO###'=>$contact_number,
					'###EMAIL###'=>$username,
					'###DATE###'=>$nows,
					'###CONTENT###'=>$mail_text,
					'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
					'###SITENAME###' =>$site_name,
					'###SEE_STATUS_WITHDRAW###'=>$see_status_withdraw,
					'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>'
                	
					);
				//Pilaventhiran 05/05/2016 END
					   
				$gd_message=strtr($fe_cont,$gd_api);
				//echo $gd_message;exit;
				/*
				$config['protocol'] = 'sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				*/
			
				$config = Array(
					 'mailtype'  => 'html',
					  'charset'   => 'utf-8',
					  );
				$mail = $User_details->email;
				//$list = array($mail, $admin_emailid);
				
				$this->email->initialize($config);
				$this->email->set_newline("\r\n");
				$this->email->from($admin_emailid,$site_name.'!');
				$this->email->to($mail);
				$this->email->subject($subject);
				$this->email->message($gd_message);
				$this->email->send();
				$this->email->print_debugger();
			}
			return true;
		}
		else 
		{ 
			return false;	
		}	
	}
	
	function paid_earnings($userid)
	{	
		$this->db->connection_check();
		$this->db->select('SUM(requested_amount) as completed_bal');
		$this->db->where('status','Completed');
		$this->db->where('user_id',$userid);
		$paid_earning= $this->db->get('withdraw');
		if($paid_earning->num_rows > 0)
        {
            return $paid_earning->row('completed_bal');
        }
		else
		{
			return false;
		}
	}
	
	function total_earnings($userid)
	{	
		$this->db->connection_check();
		$this->db->select('SUM(transation_amount) as completed_bal');
		$this->db->where('user_id',$userid);
		$this->db->where('mode','Credited');
		$paid_earning= $this->db->get('transation_details');
		$waitng = $paid_earning->row('completed_bal');
		//echo $userid;
		 $balcne =  $this->user_balance($userid);	
		 //exit;
		//$newbalset =  $waitng+$balcne;
		if($paid_earning->num_rows > 0)
        {
            return $balcne;
        }
		else
		{
			return false;
		}
	}
	
	function waiting_approval($userid)
	{
		$this->db->connection_check();
		$paid_earning = $this->db->query("SELECT SUM(requested_amount) as completed_bal FROM (`withdraw`) WHERE `user_id` = '$userid' AND (`status` = 'Processing' OR `status` = 'Requested')");
		if($paid_earning->num_rows > 0)
        {
            return $paid_earning->row('completed_bal');
        }
		else
		{
			return false;
		}
	}
	
	function get_all_categories($count=null)
	{
		$this->db->connection_check();
		if($count!="")
		{
			$this->db->limit($count,0);
		}	
		//$this->db->order_by('sort_order');
		$this->db->where('category_status','1');
		$result = $this->db->get('categories');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	
	function get_all_stores($count=null)
	{
		$this->db->connection_check();
		if($count!="")
		{
			$this->db->limit($count,0);
		}
		$this->db->where('affiliate_status','1');
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	
	function getmaincategorys()
	{
		$this->db->connection_check();
		$results = $this->db->query("SELECT
			category_id,category_name,category_url,
			count(c.cate_id) AS num_categorys
			FROM
				categories AS p
				JOIN sub_categories AS c 
					ON p.category_id = c.cate_id
			where p.category_status=1 
			GROUP BY c.cate_id
			order by p.sort_order ASC");
			
			if($results->num_rows > 0){
				return $results->result();	
			}
			return false;
	}
	
	function get_sub_categorys_list($category_id)
	{
		$this->db->connection_check();
		$results = $this->db->query("SELECT * FROM `sub_categories` where cate_id=$category_id and category_status=1 order by sort_order ASC");
		if($results->num_rows > 0)
		{
				return $results->result();	
		}
		else
		{
			return false;
		}
	}
	
	function get_stores_category()
	{
		$this->db->connection_check();
		$results = $this->db->query("SELECT * FROM categories as a WHERE a.category_id NOT IN (SELECT cate_id FROM sub_categories)");
		if($results->num_rows > 0)
		{
				return $results->result();	
		}
		else
		{
			return false;
		}
	}
	
	
	function get_available_store_cate()
	{
		$this->db->connection_check();
		$results = $this->db->query("SELECT 
		category_id,category_url, category_name 
		FROM `categories` as c 
		join (select store_categorys from affiliates) as a 
		WHERE 
		c.category_status=1 and FIND_IN_SET(c.category_id,a.store_categorys) 
		GROUP BY category_id order by c.sort_order ASC");
		
		if($results->num_rows > 0)
		{
				return $results->result();	
		}
		else
		{
			return false;
		}
	}
	
	function get_available_affiliates($cateid)
	{
		$this->db->connection_check();
		//$this->db->order_by("sort_order", "ASC");
		$results = $this->db->query("SELECT affiliate_url,affiliate_name,affiliate_id from affiliates as a WHERE affiliate_status=1 and FIND_IN_SET($cateid,a.store_categorys) ORDER BY  a.sort_order ");
		if($results->num_rows > 0)
		{
				return $results->result();	
		}
		else
		{
			return false;
		}
	}
	
	function sub_category_details($subcateurl=null)
	{
		$this->db->connection_check();
		if($subcateurl=='')
		{
			return false;
		}
		$factal_subcat = $this->db->query("SELECT sun_category_id as category_id,sub_category_url as category_url,sub_category_name as category_name, meta_keyword, meta_description FROM `sub_categories` where sub_category_url='$subcateurl'");
		if($factal_subcat->num_rows > 0)
        {
            return $factal_subcat->row();
        }
		else
		{
			return false;
		}
	}
	
	function count_coupons($catename=null)
	{
		$date = date('Y-m-d'); 
		$this->db->connection_check();
		$count_coupons = $this->db->query("SELECT count(*) as counting FROM `coupons` where offer_name like '%$catename%' and expiry_date >='$date'");
		if($count_coupons->num_rows > 0)
        {
            return $count_coupons->row();
        }
		else
		{
			return false;
		}
	}
	
	function get_coupons_sets($store,$coupon_count=null)
	{
		$this->db->connection_check();
		if($coupon_count!="")
		{
			$this->db->limit($coupon_count,0);
		}	
		$this->db->like('offer_name', $store);	
		$result = $this->db->get('coupons');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	/******Nov 19 th*********/
	/******Nov 26 th*********/
	
	function get_store_details($affiliate_url=null)
	{
		$this->db->connection_check();
		//$this->db->where('affiliate_id',$affiliate_id);
		$this->db->where('affiliate_url', $affiliate_url);	
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->row();	
		}
			return false;
	}
	//New code for store details//

	function all_store_details()
	{	
 
		$this->db->connection_check();
		$storedetail = $this->db->get('tbl_stores');
		if($storedetail->num_rows > 0){
			foreach($storedetail->result() as $row)
			{
				$data[] = $row;
			}
		 	return $data;
		}
			
	}
	//end//
	
	function get_coupons_from_store($store,$coupon_count=null)
	{
		//echo $expiry; exit;
		$user_id = $this->session->userdata('user_id');
		$date = date('Y-m-d');
		$this->db->connection_check();
		if($coupon_count!="")
		{
			$this->db->limit($coupon_count,0);
		}
		//$this->db->order_by('coupon_id','desc');
		$this->db->order_by('coupon_options','desc');
		/*New code for coupon status to filter the data's 8-2-17*/
		$this->db->where('coupon_status','completed');
		/*End 8-2-17*/
		$this->db->where('offer_name', $store);
		$this->db->where('expiry_date >=', $date);
		$result = $this->db->get('coupons');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	
	//New code for expiry coupons list in pefore Login pages //

	function expiry_coupons_from_store($store,$coupon_count=null)
	{
		$user_id = $this->session->userdata('user_id');
		$date = date('Y-m-d');
		$this->db->connection_check();
		if($coupon_count!="")
		{
			$this->db->limit($coupon_count,0);
		}

		$this->db->order_by('coupon_id','desc');
		$this->db->where('offer_name', $store);
		$this->db->where('expiry_date <', $date);
		$result = $this->db->get('coupons');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	} 

	//End//

	//New code for Expiry coupons Insert into Expiry coupons Table//

	//End//


/******Nov 26 th*********/

	function get_store_details_byid($affiliate_id=null)
	{
		$this->db->connection_check();
		$this->db->where('affiliate_id',$affiliate_id);
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->row();	
		}
			return false;
	}

	/*New code for premium coupons details 11-1-17*/
	function get_premiumcoupons_details_byid($shop_id=null)
	{
		$this->db->connection_check();
		$this->db->where('shoppingcoupon_id',$shop_id);
		$result = $this->db->get('shopping_coupons');
		if($result->num_rows > 0){
			return $result->row();	
		}
			return false;	
	}

	function get_premiumcategorys_details_byid($shop_id=null)
	{
		$this->db->connection_check();
		$this->db->where('category_id',$shop_id);
		$result = $this->db->get('premium_categories');
		if($result->num_rows > 0){
			return $result->row();	
		}
			return false;	
	}
	/*End 11-1-17*/

	function get_coupons_from_coupon_byid($coupon_id)
	{	
		$date = date('m/d/Y');
		$this->db->connection_check();
		$this->db->where('coupon_id',$coupon_id);
		//$this->db->where('expiry_date >' $date);
		$result = $this->db->get('coupons');
		if($result->num_rows > 0)
		{
			return $result->row();	
		}
			return false;
	}
	
	// get admin details..
	function getadmindetails()
	{
		$this->db->connection_check();
		$this->db->where('admin_id','1');
		$query_admin = $this->db->get('admin');
		if($query_admin->num_rows >= 1) 
		{
			$row = $query_admin->row();
			return $query_admin->result();
		}
		else
		{
			return false;		
		}	
	}	
	
	function available_for_provider($affid)
	{
		$this->db->connection_check();
		$this->db->where('affiliate_name',$affid);
		$query_admin = $this->db->get('providers');
		if($query_admin->num_rows >= 1) 
		{
			return $query_admin->row();
		}
		else
		{
			return false;		
		}	
	}
	
	function click_history($store_id,$coupon_id,$userid,$useragent)
	{
		$this->db->connection_check();
		$store_details = $this->get_store_details_byid($store_id);
		
		$coupon_details  = $this->get_coupons_from_coupon_byid($coupon_id);		
		$voucher_name 	 = $coupon_details->title;
		$store_name   	 = $store_details->affiliate_name;
		$store_url   	 = $store_details->affiliate_url;
		$store_id 		 = $store_details->affiliate_id;
		$ip_address  	 = $this->input->ip_address();
		$admindetailssss = $this->front_model->getadmindetails_main(); 
			if($store_details->cashback_percentage!="")
			{	
				$cppercentage 	= $store_details->cashback_percentage;
				$cashback_per   = $this->session->userdata('cashback_web');
				$affiliate_urls = $this->session->userdata('affiliate_urls');
				
				if($store_url == $affiliate_urls)
				{
					if($cashback_per !='')
					{
						$cppercentage = $cashback_per;
					}
				}
				
				
				$voucher_name .=  " + Get additional ".$cppercentage."% Cashback from ".$admindetailssss->site_name;
			}
			else
			{
				$voucher_name .= "";
			}			
				
			/*New code for remove symbols and other characters in storename 11-1-17*/
			//$store_url  = $this->front_model->seoUrl($store_name);
			/*End*/	


			$current_date_time = date('Y-m-d H:i:s');
			$data = array(
			'voucher_name' => $voucher_name,
			'store_name' => $store_name,
			'affiliate_id' => $store_id,
			'user_id' => $userid,
			'ip_address'=>$ip_address,
			'date_added' => $current_date_time,
			'store_url' => $store_url
			);
			$this->db->insert('click_history',$data);
			return true;
	}

	/*New code for save premium coupons details in click history table 11-1-17*/
	function newclick_history($store_id,$coupon_id,$userid,$useragent)
	{	
		/*echo $store_id; echo "<br>";
		echo $coupon_id; echo "<br>";
		echo $userid; echo "<br>";
		exit;*/

		$this->db->connection_check();
		$store_details = $this->get_store_details_byid($store_id);
		
		$coupon_details  = $this->get_coupons_from_coupon_byid($coupon_id);		
		$voucher_name 	 = $coupon_details->title;
		$store_name   	 = $store_details->affiliate_name;
		$store_url   	 = $store_details->affiliate_url;
		$store_id 		 = $store_details->affiliate_id;
		$ip_address  	 = $this->input->ip_address();
		$admindetailssss = $this->front_model->getadmindetails_main(); 
			if($store_details->cashback_percentage!="")
			{	
				$cppercentage 	= $store_details->cashback_percentage;
				$cashback_per   = $this->session->userdata('cashback_web');
				$affiliate_urls = $this->session->userdata('affiliate_urls');
				
				if($store_url == $affiliate_urls)
				{
					if($cashback_per !='')
					{
						$cppercentage = $cashback_per;
					}
				}
				
				
				$voucher_name .=  " + Get additional ".$cppercentage."% Cashback from ".$admindetailssss->site_name;
			}
			else
			{
				$voucher_name .= "";
			}			
				
			/*New code for remove symbols and other characters in storename 11-1-17*/
			//$store_url  = $this->front_model->seoUrl($store_name);
			/*End*/	

			$current_date_time = date('Y-m-d H:i:s');
			$data = array(
			'voucher_name' => $voucher_name,
			'store_name' => $store_name,
			'affiliate_id' => $store_id,
			'user_id' => $userid,
			'ip_address'=>$ip_address,
			'date_added' => $current_date_time,
			'store_url' => $store_url
			);
			$this->db->insert('click_history',$data);
			return true;
	}
	/*end 11-1-17*/
	
	function verify_account($verifyid)
	{
		$this->db->connection_check();
		$where = array('random_code'=>$verifyid);
		$this->db->where($where);
		$this->db->where('admin_status','');
		$query = $this->db->get('tbl_users');
		if($query->num_rows >= 1) 
		{
			$data = array(		
			'status' => 1,
			);
			$this->db->where('random_code',$verifyid);	
			$this->db->update('tbl_users',$data);
			
			$fetch = $query->row();
			$user_id = $fetch->user_id;
			$user_email=$fetch->email;
			$this->session->set_userdata('user_id',$user_id);
			$this->session->set_userdata('user_email',$user_email);
			return 1;
		}  
		else
		{
			return 0;
		}  
	}
	//New Code for Un-sbuscribe email//
	function un_subscribe_signup($verifyid)
	{
		$this->db->connection_check();
		$where = array('random_code'=>$verifyid);
		$this->db->where($where);
		$this->db->where('admin_status','');
		$query = $this->db->get('tbl_users');
		if($query->num_rows >= 1) 
		{
			$data = array(		
			'status' => 0,
			);
			$this->db->where('random_code',$verifyid);	
			$this->db->update('tbl_users',$data);
			
			//$fetch = $query->row();
			$user_id = $fetch->user_id;
			$user_email=$fetch->email;

			//$this->session->set_userdata('user_id',$user_id);
			//$this->session->set_userdata('user_email',$user_email);
			return 1;
		}  
		else
		{
			return 0;
		}
	}


	function un_subscribe($type,$userid)
	{
		
		//echo $type; echo $userid;exit; 
		$this->db->connection_check();
		if($type =='cashback')
		{
			//echo "hai".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'cashback_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		} 
		if($type =='myaccount')
		{
			//echo "hai1".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'acbalance_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		} 
		if($type =='referral')
		{
			//echo "hai2".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'referral_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		}
		if($type =='newsletter')
		{
			//echo "hai2".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'newsletter_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		}
		if($type =='subscribers')
		{
			//echo "hai2".$userid; exit;
			$udetails    = $this->db->query("select * from tbl_users where user_id=$userid")->row();
			$emailid     = $udetails->email;
			$news_status = $udetails->newsletter_mail;

			if($news_status == 1)
			{
				$data = array(		
				'newsletter_mail' => 0,		
				);
				$this->db->where('user_id',$userid);
				$update_qry = $this->db->update('tbl_users',$data);
			}

			$where = array('subscriber_email'=>$emailid,'subscriber_status'=>1);
			$this->db->where($where);
			$query = $this->db->get('subscribers');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'subscriber_status' => 0,
				);
				$this->db->where('subscriber_email',$emailid);	
				$this->db->update('subscribers',$data);
				$user_id = $fetch->subscriber_id;
				//$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		}
		/*New code for withdraw unsubscribe details 9-1-17*/
		if($type =='withdraw')
		{
			//echo "hai".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'withdraw_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		} 
		/*End*/
		/*New code for missing cashback unsubscribe details 23-1-17*/
		if($type =='missing_cashback' || $type =='missing_approval')
		{
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'support_tickets' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		}
		/*End*/
	}
	//End// 
	
	/************ Dec 8th *************/
	function get_details_ajax($ajdate,$user_id)
	{
		$this->db->connection_check();
		$this->db->where('DATE(`date_added`)',$ajdate);
		$this->db->where('user_id',$user_id);
		$this->db->group_by('store_name'); 
		$query_details_ses = $this->db->get('click_history');
		
		if($query_details_ses->num_rows >= 1) 
		{
			return $query_details_ses->result();
		}
		else
		{
			return false;		
		}	
	}
	
	function get_clicked_details_ajax($ajdate,$click_store,$user_id)
	{
		$this->db->connection_check();
		$this->db->where('DATE(`date_added`)',$ajdate);
		$this->db->where('store_name',$click_store);
		$this->db->where('user_id',$user_id);
		$query_details_ses = $this->db->get('click_history');		
		if($query_details_ses->num_rows >= 1) 
		{
			return $query_details_ses->result();
		}
		else
		{
			return false;		
		}			
	}
	/************ Dec 8th *************/
	
	/************ Dec 9th *************/
	function missing_Cashback_submit_mod($img)
	{	
		//echo "<pre>"; print_r($_POST);exit;
		$name = $this->db->query("select * from admin")->row();
		$site_name  = $name->site_name;
		$this->db->connection_check();
		$user_id = $this->session->userdata('user_id');
		$date = date('Y-m-d');
		$aff_url 	    = $this->input->post('store');
		$get_aff_name   = $this->get_store_details($aff_url);
		$affiliate_name = $get_aff_name->affiliate_name; 
		$trans_date 	= date('Y-m-d',strtotime($this->input->post('trans_date')));
		$data = array(
			'user_id'			=>$user_id,
			'missing_reason'	=>'Missing Cashback',
			'attachment'		=>$img,
			'retailer_name'  	=>$affiliate_name,
			'transaction_ref_id'=>$this->input->post('transaction_reference'),
			'transation_amount' =>$this->input->post('ordervalue'),
			'click_id'			=>$this->input->post('hid_click_id'),
			'coupon_code'		=>$this->input->post('coupon_used'),
			'ordervalue'		=>$this->input->post('ordervalue'),
			'cashback_details'	=>$this->input->post('details'),
			'status'			=>3,
			'trans_date'		=>$trans_date,
			'phone_no' 			=>$this->input->post('phone_no'),
			'ticket_created_in' =>$date
		);
		//echo "<pre>";print_r($data); exit;

		$this->db->insert('missing_cashback',$data);
		$user_details = $this->edit_account($user_id);
		$mail 		  = $user_details->email;
		if($user_details->support_tickets == 1)
		{
			/*new code for username details 5-1-17*/
			$firstname = $user_details->first_name;
			$lastname  = $user_details->last_name;
			if($firstname == '' && $lastname == '')
			{
				$ex_name  = explode('@',$user_details->email);
				$username = $ex_name[0]; 
			}
			else
			{
				$username = $firstname.''.$lastname;
			}	
			/*End 5-1-17*/

			//$username 		 = $user_details->first_name." ".$user_details->last_name;
			$admindetailssss = $this->front_model->getadmindetails_main();  
			$mail_text 		 = '<p><span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:26px;font-style:normal">
			Acabamos de enviar a sua reclamação para a loja. O processo de conferência é manual e pode levar até 40 dias úteis para que eles nos respondam. (A gente sabe que isso é muito demorado e é um saco esperar tanto, mas infelizmente não depende de nós . E esse é um “prazo máximo” pode ser que leve bem menos que isso).</span></p>';
				
			$mail_temp 		 = $this->db->query("select * from tbl_mailtemplates where mail_id='13'")->row();
			$fe_cont 		 = $mail_temp->email_template;
			$admindetailssss = $this->front_model->getadmindetails_main(); 
			//$subject 		 = $admindetailssss->site_name." has recieved your Missing Cashback Ticket";
			$subject 		 = $mail_temp->email_subject;
			$name 			 = $this->db->query("select * from admin")->row();
			$admin_emailid 	 = $name->admin_email;
			$site_logo 		 = $name->site_logo;
			$site_name  	 = $name->site_name;
			$contact_number  = $name->contact_number;
			$servername 	 = base_url();
			$nows 			 = date('d/m/Y');/*ALTEREI ERA 'Y-m-d'*/
			$unsuburl     	 = base_url().'un-subscribe/missing_cashback/'.$user_id;
	        $myaccount    	 = base_url().'minha-conta';

			$this->load->library('email');
			//Pilaventhiran 04/05/2016 START
			$see_status_missing = "<a href='".base_url()."loja_nao_avisou_compra'>status da solicitação</a>";
			$gd_api=array(
						'###ADMINNO###'    =>$contact_number,
						'###EMAIL###'      =>$username,
						'###DATE###'	   =>$nows,
						'###CONTENT###'	   =>$mail_text,
						'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
						'###SITENAME###'   =>$site_name,
						'###SEE_STATUS_MISSING###'=>$see_status_missing,
						'###ULINK###'      =>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
						'###MYACLINK###'   =>'<a href='.$myaccount.'>'.$myaccount.'</a>'
						);
			//Pilaventhiran 04/05/2016 END
	   
			$gd_message=strtr($fe_cont,$gd_api);
			//echo $gd_message;exit;
			/*
			$config['protocol'] = 'sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			*/
			
			$config = Array(
				 'mailtype'  => 'html',
				  'charset'   => 'utf-8',
				  );
			//$list = array($mail, $admin_emailid);
			
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			$this->email->from($admin_emailid,$site_name.'!');
			$this->email->to($mail);
			$this->email->subject($subject);
			$this->email->message($gd_message);
			$this->email->send();
			$this->email->print_debugger();
		}
		return true;
	}
	
	/************ Dec 9th *************/
	
	/************ Dec11th *************/
	function user_cashback_balance($user_id=null)
	{
		$this->db->connection_check();
		if($user_id!="")
		{
			//SELECT sum(transation_amount) FROM `transation_details` where user_id=1 and transation_reason='Cashback'
			$this->db->select('SUM(transation_amount)');
			$this->db->where('user_id',$user_id);
			$this->db->where_in('transation_reason',array('Cashback','Missing Cashback request','Missing Approval','Missing Cashback'));
			//$this->db->where('transation_reason','Cashback'); Missing Cashback request
			$this->db->where('transation_status','Paid');
			$allfaqs = $this->db->get('transation_details');
			return $allfaqs->row('SUM(transation_amount)');
			

			/*New code hide 7-6-17*/
			/*$this->db->select('SUM(cashback_amount)');
			$this->db->where('user_id',$user_id);
			//$this->db->where('transation_reason','Cashback');
			$this->db->where('status','Completed');
			$allfaqs = $this->db->get('cashback');
			return $allfaqs->row('SUM(cashback_amount)');*/
			/*End 7-6-17*/
		}
		else
		{
			return 0;
		}
	}
	
	function get_clicked_expirycheck_ajax($click_id)
	{
		$this->db->where('click_id',$click_id);
		$all = $this->db->get('missing_cashback');
		if($all->num_rows > 0){
			return $all->row();
		}
		return false;
	}
	
	/************ Dec11th *************/
	
	/************ Dec 13 th *************/
	function get_top_cashback_stores_list_cate($category_id)
	{
		$this->db->connection_check();
		$this->db->order_by('cashback_percentage','desc');
		$this->db->where("FIND_IN_SET('$category_id',store_categorys) !=", 0);
		$this->db->where('affiliate_status','1');
		$query = $this->db->get('affiliates');
		//echo $this->db->last_query();die;
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
	function get_top_cashback_stores_list()
	{
		$this->db->connection_check();
		$this->db->order_by('cashback_percentage','desc');
		//$this->db->where("FIND_IN_SET('$category_id',store_categorys) !=", 0);
		$this->db->where('affiliate_status','1');
		$this->db->where("(featured='1' OR store_of_week='1')", NULL, FALSE);
		$query = $this->db->get('affiliates');
		//echo $this->db->last_query();die;
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
	function get_top_cashback_stores_limit($limit)
	{
		$this->db->connection_check();
		$this->db->limit($limit,0);
		//$this->db->order_by('affiliate_id','desc');
		//$this->db->where("FIND_IN_SET('$category_id',store_categorys) !=", 0);
		$this->db->where('affiliate_logo !=','');
		$this->db->where('affiliate_status','1');
		$query = $this->db->get('affiliates');
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
		// view user email	
	function user_name($user_id)
	{
		$this->db->connection_check();	
		$result = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('first_name');
		return $result;	
	}
	
	function get_blog_details()
	{
		$this->db->connection_check();
		$this->db->order_by('cms_id','desc');
		//$this->db->where("FIND_IN_SET('$category_id',store_categorys) !=", 0);
		$this->db->where('cms_status','1');
		$query = $this->db->get('tbl_blog');
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
	function blog_comments($blog_id)
	{
		$this->db->connection_check();
		//echo $blog_id;
		$this->db->where('bid',$blog_id);
		$query = $this->db->get('tbl_bloguser_comments');
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
	function blog_details($blog_id)
	{
		$this->db->connection_check();
		//echo $blog_id;
		$this->db->where('cms_id',$blog_id);
		$query = $this->db->get('tbl_blog');
		if($query->num_rows >= 1)
		{
		   return $query->row();
		}
		return false;
	}
	
	function post_comments()
	{
		$this->db->connection_check();
		$now = date('Y-m-d H:i:s');
		$data = array(
		'bid'=>$this->input->post('blog_id'),
		'user_id'=>$this->input->post('user_id'),
		'comments'=>$this->input->post('comments'),
		'created_date'=>$now,
		'c_date'=>$now,
		'status'=>'deactive'	
		);
		$this->db->insert('tbl_bloguser_comments',$data);		
		return true;
	}
	
	// site visits..
	function unique_visits($ip_address)
	{
		$this->db->connection_check();
		$date = date('Y-m-d');	 /*ALTEREI ERA 'Y-m-d' */
		
		$this->db->where('date_added',$date);
		$this->db->where('ip_address',$ip_address);		
		$check = $this->db->get('site_visits');
		if($check->num_rows == 0){
			
			$data = array(
				'ip_address'=>$ip_address,
				'date_added'=>$date
			);
			
			$this->db->insert('site_visits',$data);
			return true;
		}
	}
	
	/************ Dec 13 th *************/
	
	

/*********************Nathan End*************************/
	
	
	 //  5/12/2014  renuka  
 
 function get_allpremiumcoupon_cat()
 { 
		 $this->db->connection_check();/**/
      //    premium_categories 	category_id 	category_name 	category_url 	meta_keyword 	meta_description 	sort_order 	category_status 	date_added
	   
 		$this->db->where('category_status','1');  
		$query = $this->db->get('premium_categories');
		if($query->num_rows >= 1) 
		{ 
			  $result=$query->result();  
			return $result;
		}  
		else
		{
		    return false; 
		}          
 
 } 
 
 function get_countofpremiumcat_addcoupon($db_category_id) 
 { 
 $this->db->connection_check();
  $location =  $this->session->userdata('cityname');
      //    premium_categories 	category_id 	category_name 	category_url 	meta_keyword 	meta_description 	sort_order 	category_status 	date_added   category   
	    $this->db->where('location',$location);
 		$this->db->where( "FIND_IN_SET ('$db_category_id',category) >", 0 ); 
		$this->db->where('remain_coupon_code <>','');
		$this->db->where('expiry_date >=',date('Y-m-d'));
		
		//$this->db->where( "t2.remain_coupon_code !=", '' ); 
		$query=$this->db->get('shopping_coupons');  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->num_rows;  
			 return $num_rows; 
		}  
		else 
		{
		     return $num_rows=0;  
		}             
 }

 	/*new code for shopping_couopn page 01-10-16*/
	function get_premium_cat_coupon($db_category_id) 
	{ 
		$this->db->connection_check();
	  	$location =  $this->session->userdata('cityname');
		//$this->db->where('location',$location);
	 	$this->db->where( "FIND_IN_SET ('$db_category_id',category) >", 0 ); 
		//$this->db->where('remain_coupon_code <>','');
		$this->db->where('expiry_date >=',date('Y-m-d'));
		$query=$this->db->get('shopping_coupons');  
		//echo $this->db->last_query();die;
		if($query->num_rows >= 1) 
		{ 
		    $result=$query->result(); 
		    return $result; 
		}  
		else 
		{
		    return false;  
		}             
	}
 	/*end*/

 	/*New code for get all premium coupon details 4-1-17*/
 	function get_allpremiumcoupons()
 	{
 		$this->db->connection_check();
	  	$location =  $this->session->userdata('cityname');
		//$this->db->where('location',$location);
	 	//$this->db->where( "FIND_IN_SET ('$db_category_id',category) >", 0 ); 
		//$this->db->where('remain_coupon_code <>','');
		$this->db->where('expiry_date >=',date('Y-m-d'));
		$query=$this->db->get('shopping_coupons');  
		//echo $this->db->last_query();die;
		if($query->num_rows >= 1) 
		{ 
		    $result=$query->result(); 
		    return $result; 
		}  
		else 
		{
		    return false;  
		}         
 	}
 	/*End*/

     
 function getcnt_allpremiumcoupon_incat() 
 { 
      //    premium_categories 	category_id 	category_name 	category_url 	meta_keyword 	meta_description 	sort_order 	category_status 	date_added   category   
	   $this->db->connection_check();
	   $location =  $this->session->userdata('cityname');
	   $selqry="SELECT `t1`.* FROM (`premium_categories` as t1) JOIN `shopping_coupons` as t2 ON  FIND_IN_SET (`t1`.`category_id`,`t2`.`category`) > 0 WHERE `t1`.`category_status` = '1' AND  `t2`.`status` = '1'   and t2.remain_coupon_code!='' and t2.expiry_date >='".date('Y-m-d')."' GROUP BY t2.shoppingcoupon_id ";  //and t2.location='".$location."'    
	     
		 $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->num_rows;  
			return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;  
		}          
  
 }   
 
  function getall_premiumcoupons($perpage,$urisegment,$ajax='') 
 {  
 
 		$this->db->connection_check();
		if($urisegment=="")
		{
              $urisegment=0; 
		} 
		else
		{
              $urisegment=$urisegment; 
		}   
 	      
		  $sess_cashback_premiumcatid=trim($this->session->userdata("sess_cashback_premiumcatid"));
		  $sess_cashback_starting_price=trim($this->session->userdata("sess_cashback_starting_price"));
		  $sess_cashback_cashback_end_price=trim($this->session->userdata("sess_cashback_end_price"));
		  $sess_cashback_featured=trim($this->session->userdata("sess_cashback_featured"));
		  $sess_cashback_popular=trim($this->session->userdata("sess_cashback_popular"));
		  $sess_cashback_new=trim($this->session->userdata("sess_cashback_new"));
		  $sess_cashback_es=trim($this->session->userdata("sess_cashback_es"));
		
		  //print_r($this->session->userdata);
		
		  $query='';
	 	
	     if($sess_cashback_premiumcatid!='' &&  $sess_cashback_premiumcatid!="all" && $sess_cashback_premiumcatid!='0')
		   {
			   $sess_cashback_premiumcatid;
			   
		   $query.= "AND FIND_IN_SET($sess_cashback_premiumcatid,`t2`.`category`) ";
		   }
	    if($sess_cashback_starting_price!='' && $sess_cashback_cashback_end_price!='')
		  {
			 $query.=" AND (amount BETWEEN $sess_cashback_starting_price AND $sess_cashback_cashback_end_price) ";
		  }
		  $sess_cashback_feature_query='';
		  $sess_cashback_feature_query1='';
		  $sess_cashback_feature_query2='';
		  
		  if($sess_cashback_featured!='0' || $sess_cashback_popular!='0' || $sess_cashback_new!='0' || $sess_cashback_es!='0') 
		  {
			$sess_cashback_feature_query1="AND (  ";
			$sess_cashback_feature_query2=" OR ";
		  }
		  if($sess_cashback_featured!='0')
		  {
			if($sess_cashback_feature_query=='')			  
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('featured',`t2`.`features`) ";
			 else
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('featured',`t2`.`features`) ";
			 
		  }
		  if($sess_cashback_popular!='0')
		  {
			 if($sess_cashback_feature_query=='')			
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('popular',`t2`.`features`) ";  		
			 else
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('popular',`t2`.`features`) ";
			
		  }
		  if($sess_cashback_new!='0')
		  {
			  //echo $sess_cashback_new;
			 if($sess_cashback_feature_query=='')	
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('new',`t2`.`features`) ";
			 else
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('new',`t2`.`features`) ";
		  }
		  if($sess_cashback_es!='0')
		  {
			  if($sess_cashback_feature_query=='')	
			  $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('endingsoon',`t2`.`features`) ";
			  else
			  $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('endingsoon',`t2`.`features`) ";
			
		  }
		  if($sess_cashback_feature_query!='')
		  {
			  $sess_cashback_feature_query.=" ) ";
		  }		
		  $query.=$sess_cashback_feature_query;
		  if($ajax=='')
		  {
			$query='';  
		  }
		 $location =  $this->session->userdata('cityname');
		   $selqry="SELECT `t2`.* FROM (`premium_categories` as t1) JOIN `shopping_coupons` as t2 ON  FIND_IN_SET (`t1`.`category_id`,`t2`.`category`) > 0 WHERE `t1`.`category_status` = '1' AND `t2`.`status` = '1'  and `t2`.`remain_coupon_code`!='' and t2.expiry_date >='".date('Y-m-d')."' $query  GROUP BY t2.shoppingcoupon_id  limit $urisegment,$perpage ";     
		 
 
		   //echo "SELECT `t2`.* FROM (`premium_categories` as t1) JOIN `shopping_coupons` as t2 ON  FIND_IN_SET (`t1`.`category_id`,`t2`.`category`) > 0 WHERE `t1`.`category_status` = '1' AND `t2`.`status` = '1'  and `t2`.`remain_coupon_code`!='' and t2.expiry_date >='".date('Y-m-d')."' $query  GROUP BY t2.shoppingcoupon_id  limit $urisegment,$perpage ";
		 $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			$result=$query->result();  
			return $result; 
		}  
		else  
		{
		    return "0";   			
		}          
 
 }
 
 function getall_fetured_products()
 {
	 $this->db->connection_check();
	 $selqry="SELECT * FROM shopping_coupons  WHERE FIND_IN_SET('featured',`features`)  and `remain_coupon_code`!='' AND `status` = '1' and expiry_date >='".date('Y-m-d')."' order by shoppingcoupon_id desc";     
 	
		 $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			$result=$query->result();  
			return $result; 
		}  
		else  
		{
		    return "0";   			
		}           
 }
 function getall_premiumcouponscount($ajax='') 
 { 
 	$this->db->connection_check();
  $sess_cashback_premiumcatid=$this->session->userdata("sess_cashback_premiumcatid");
		  $sess_cashback_starting_price=$this->session->userdata("sess_cashback_starting_price");
		  $sess_cashback_cashback_end_price=$this->session->userdata("sess_cashback_end_price");
		  $sess_cashback_featured=$this->session->userdata("sess_cashback_featured");
		  $sess_cashback_popular=$this->session->userdata("sess_cashback_popular");
		  $sess_cashback_new=$this->session->userdata("sess_cashback_new");
		  $sess_cashback_es=$this->session->userdata("sess_cashback_es");
		  
		  $query='';
	 	
	      if($sess_cashback_premiumcatid!='' &&  $sess_cashback_premiumcatid!="all" && $sess_cashback_premiumcatid!='0')
		   {
		   $query.= "AND FIND_IN_SET($sess_cashback_premiumcatid,`t2`.`category`) ";
		   }
	    if($sess_cashback_starting_price!='' && $sess_cashback_starting_price!=0 && $sess_cashback_cashback_end_price!='' && $sess_cashback_cashback_end_price!=0)
		  {
			 $query.=" AND (amount BETWEEN $sess_cashback_starting_price AND $sess_cashback_cashback_end_price) ";
		  }
		  $sess_cashback_feature_query='';
		  $sess_cashback_feature_query1='';
		  $sess_cashback_feature_query2='';
		  
		  if($sess_cashback_featured!='0' || $sess_cashback_popular!='0' || $sess_cashback_new!='0' || $sess_cashback_es!='0') 
		  {
			$sess_cashback_feature_query1="AND (  ";
			$sess_cashback_feature_query2=" OR ";
		  }
		  if($sess_cashback_featured!='0')
		  {
			if($sess_cashback_feature_query=='')			  
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('featured',`t2`.`features`) ";
			 else
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('featured',`t2`.`features`) ";
			 
		  }
		  if($sess_cashback_popular!='0')
		  {
			 if($sess_cashback_feature_query=='')			
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('popular',`t2`.`features`) ";  		
			 else
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('popular',`t2`.`features`) ";
			
		  }
		  if($sess_cashback_new!='0')
		  {
			 if($sess_cashback_feature_query=='')	
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('new',`t2`.`features`) ";
			 else
			 $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('new',`t2`.`features`) ";
		  }
		  if($sess_cashback_es!='0')
		  {
			  if($sess_cashback_feature_query=='')	
			  $sess_cashback_feature_query.=" $sess_cashback_feature_query1 FIND_IN_SET('endingsoon',`t2`.`features`) ";
			  else
			  $sess_cashback_feature_query.=" $sess_cashback_feature_query2 FIND_IN_SET('endingsoon',`t2`.`features`) ";
			
		  }
		  if($sess_cashback_feature_query!='')
		  {
			  $sess_cashback_feature_query.=" ) ";
		  }		
		  $query.=$sess_cashback_feature_query;
		  if($ajax=='')
		  {
			 $query=''; 
		  }
		  $location =  $this->session->userdata('cityname');
		    
		   
		   
		  $selqry="SELECT `t2`.* FROM (`premium_categories` as t1) JOIN `shopping_coupons` as t2 ON  FIND_IN_SET (`t1`.`category_id`,`t2`.`category`) > 0 WHERE `t1`.`category_status` = '1' AND  `t2`.`status` = '1' and t2.location='".$location."' and `t2`.`remain_coupon_code`!='' and t2.expiry_date >='".date('Y-m-d')."' $query  GROUP BY t2.shoppingcoupon_id";  
		  
		  
	    
	    $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->num_rows;   
			   return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}          
 
 }
  
  
   
 function getrowsperpage()
{ 
	$this->db->connection_check();
    	/* $query=$this->db->get('admin');
        if($query->num_rows==1)
         {   
              $row=$query->row();   
			$rowsperpage=$row->intRows_front;    
         }  
		//  $rowsperpage="10"; */
		 
	return $rowsperpage=6;     
 }
	
	function find_remainingdays($datestr=null)
 {
	 $this->db->connection_check();
   
   //Convert to date
  //$datestr="2014-12-25 00:00:00";//Your date
$date=strtotime($datestr);//Converted to a PHP date (a second count)
 
//Calculate difference
$diff=$date-time();//time returns current time in seconds
$days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
$hours=round(($diff-$days*60*60*24)/(60*60));
   $data['days']=$days; 
   $data['hours']=$hours; 
   
   
   return $data; 
  //Report  
  // echo "$days days $hours hours remain<br />";  
 
 }
	//change code for id to name field 7-6-16//
	function details($name)
	{
		$this->db->connection_check();
		$selqry = "SELECT * FROM shopping_coupons  WHERE seo_url = '$name'"; //this also 7-5-16//        
	    $query  = $this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			$num_rows 		 	 = $query->row(); 
			$ip_addr 		 	 = $this->input->ip_address();
			$db_coupon_image 	 = $num_rows->coupon_image;
			$coupon_name     	 = $num_rows->seo_url;
			$exp_db_coupon_image = explode(",",$db_coupon_image);  							 
			$f_dbcouponfirst_img = $exp_db_coupon_image[0];
							 
			$selqry1 = "SELECT * FROM recently_viewed  WHERE ip = '".$ip_addr."' order by date ASC"; 
			$query1  = $this->db->query("$selqry1"); 	
			$selqry2 = "SELECT * FROM recently_viewed  WHERE product_id =$num_rows->shoppingcoupon_id order by date ASC"; 
			$query2  = $this->db->query("$selqry2"); 	
			
			if($query2->num_rows == 0) 
			{
				if($query1->num_rows >= 3) 
				{ 
					
					$num_rows1=$query1->row();  
					$selqry="update recently_viewed set product_id='".$num_rows->shoppingcoupon_id."',name='".$name."',image='".$f_dbcouponfirst_img."',price='".$num_rows->amount."',ip='".$ip_addr."' WHERE ip = '".$ip_addr."' and date='".$num_rows1->date."'  ";
				}
				else
				{							
			   		$selqry="insert into recently_viewed (product_id,name,image,price,ip) values ('".$num_rows->shoppingcoupon_id."','".$name."','".$f_dbcouponfirst_img."','".$num_rows->amount."','".$ip_addr."') ";
				}
			}
	    	$this->db->query("$selqry"); 
			return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}          
	}

	function related_products($id,$name)
	{
		//echo $name;
	$this->db->connection_check();	
		$explode=explode(",",$id);
		$query="";
		for($i=0;$i<count($explode);$i++)
		{
			if($i==0)
			$query.="AND (  FIND_IN_SET($explode[$i],`category`)";
			else
			$query.=" OR FIND_IN_SET($explode[$i],`category`)";
			
		}
		$query.=" ) ";
		$selqry="SELECT * FROM shopping_coupons  WHERE status = '1'  and `remain_coupon_code`!='' and seo_url !='$name'  and expiry_date >='".date('Y-m-d')."'";        
	  
	    $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->result();   
			   return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}  
	}
	/*New code for populer 10-5-16*/
	function popular_products()
	{
		$this->db->connection_check();

		$selqry="SELECT * FROM `shopping_coupons`  WHERE `status`='1' and `remain_coupon_code`!='' and `expiry_date` >='".date('Y-m-d')."' AND  FIND_IN_SET('popular',`features`) LIMIT 3";        
	 
	    $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->result();   
			   return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}  
	}
	
	function popular_products_index()
	{
		$this->db->connection_check();

		$selqry="SELECT * FROM shopping_coupons  WHERE status = '1' and `remain_coupon_code`!='' and expiry_date >='".date('Y-m-d')."' AND  FIND_IN_SET('popular',`features`) order by shoppingcoupon_id desc";        
	 
	    $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->result();   
			   return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}  
	}
	
	function latest_products_index()
	{
		$this->db->connection_check();

		$selqry="SELECT * FROM shopping_coupons  WHERE status = '1' and `remain_coupon_code`!='' and expiry_date >='".date('Y-m-d')."' AND  FIND_IN_SET('new',`features`) order by shoppingcoupon_id desc";        
	 
	    $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->result();   
			   return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}  
	}
	
	
	function avg_rating($id)
	{
		$this->db->connection_check();
		$selqry="SELECT AVG(ratings) as rate FROM reviews r join tbl_users  u on r.user_id=u.user_id WHERE coupon_id = '".$id."' and r.approve=1 "; 
		$query=$this->db->query("$selqry")  ;   
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->row();   
			   return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}  	
	}
	
	
	function recently_viewed()
	{
		$this->db->connection_check();
		$ip_addr = $this->input->ip_address();
        $selqry="SELECT * FROM recently_viewed  WHERE ip = '".$ip_addr."' group by `id` desc limit 0,3";        
	     
	    $query=$this->db->query("$selqry"); 
		  
		if($query->num_rows >= 1) 
		{ 
			  $num_rows=$query->result();   
			   return $num_rows; 
		}  
		else  
		{
		    return $num_rows=0;   
		}  	
	}
			function recent_reviews($id)
			{
				
				$this->db->connection_check();
				 $selqry="SELECT * FROM reviews r join tbl_users  u on r.user_id=u.user_id WHERE coupon_id = '".$id."' and r.approve=1  LIMIT 3";        
				 
				$query=$this->db->query("$selqry"); 
				  
				if($query->num_rows >= 1) 
				{ 
					  $num_rows=$query->result();   
					   return $num_rows; 
				}  
				else  
				{
					return $num_rows=0;   
				}  	
			}
			function reviews($id)
			{
				$this->db->connection_check();
				 $selqry="SELECT * FROM reviews r join tbl_users  u on r.user_id=u.user_id WHERE coupon_id = '".$id."' and r.approve=1 ";        
				 
				$query=$this->db->query("$selqry"); 
				  
				if($query->num_rows >= 1) 
				{ 
					  $num_rows=$query->result();   
					   return $num_rows; 
				}  
				else  
				{
					return $num_rows=0;   
				}  	
			}
			function insert_comments()
			{
				$this->db->connection_check();
				$data = array(
					'comments' =>$this->input->post('comments'),
					'ratings' => $this->input->post('rating'),
					'coupon_id' =>$this->input->post('coupon'),
					'user_id'=>$this->session->userdata('user_id')
					);
					$var=$this->db->insert('reviews',$data);
					if($var==1)
					{
						return true;
					}
					else
					{
						return false;
					}
			}
		 function insert_coupon()
		 {
			 $this->db->connection_check();
			 $selqry1="SELECT * FROM premium_cart  WHERE product_id= '".$this->input->post('coupon')."' and user_id = '".$this->session->userdata('user_id')."'";        
			 $query1=$this->db->query("$selqry1"); 
					if($query1->num_rows >= 1) 
					{  
					$var = 0;
					}
					else
					{
					 $data = array(
					'product_id' =>$this->input->post('coupon'),
					'quantity' => '1',			
					'user_id'=>$this->session->userdata('user_id')
					);
					$var=$this->db->insert('premium_cart',$data);
					}
					
					if($var==1)
					{			
						return $var;		
					}
					else
					{
						return $var;
					}
		 }
		 function getuser_cart()
		 {			
		 		$this->db->connection_check();
				if($this->session->userdata('user_id'))			
				{
				 	$selqry="SELECT * FROM premium_cart  WHERE user_id= '".$this->session->userdata('user_id')."'";   
				 	$query=$this->db->query("$selqry");  
					if($query->num_rows >= 1) 
					{ 
						 $num_rows=$query->result();   
						 return $num_rows;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
		 }
		 function check_amount()
		 {
			 $this->db->connection_check();
			  $selqry="SELECT balance FROM tbl_users  WHERE user_id= '".$this->session->userdata('user_id')."'";   
			  $query=$this->db->query("$selqry");  
				if($query->num_rows >= 1) 
					{ 
						 $num_rows=$query->row(); 
						 if($this->input->post('amount')<=$num_rows->balance)  
						 return true;
					}
					else
					{
						return false;
					}
		 }
		 function cal_amount()
		 {
			 $this->db->connection_check();
			 $selqry="SELECT balance FROM tbl_users  WHERE user_id= '".$this->session->userdata('user_id')."'";   
			  $query=$this->db->query("$selqry");  
				if($query->num_rows >= 1) 
					{ 
						 $num_rows=$query->row(); 
						 $remain_amount=$num_rows->balance-$this->input->post('otot');
						 
						 $data = array(		
						'balance' => $remain_amount,		
						);
				
						$this->db->where('user_id',$this->session->userdata('user_id'));
						$update_qry = $this->db->update('tbl_users',$data);
						
						$data = array(		
						'transation_reason' => "premium coupon",		
						'transation_amount' => $this->input->post('otot'),		
						'mode' => "debited",
						'transation_status' => "Paid",			
						'transation_date' => date('Y-m-d'),	
						'transaction_date' => date('Y-m-d'),		
						'user_id' => $this->session->userdata('user_id'),		
						);
				
						$this->db->insert('transation_details',$data);				
						 
						 for($i=1;$i<=$this->input->post('i_val');$i++)
						 {
							  	
								
							
						  $data = array(		
								'amount' => $this->input->post('price'.$i),
								'tot_amount' => $this->input->post('tot'.$i),
								'quantity' => $this->input->post('quan'.$i),
								'product_id' => $this->input->post('product_id'.$i),	
								'user_id' => $this->session->userdata('user_id'),
								'status' => "Paid",	
								);
								$this->db->insert('premium_order',$data);							
					
						 }
						 $query = $this->db->where('user_id', $this->session->userdata('user_id'));
						 $query = $this->db->delete('premium_cart');
						 
						 return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
						 
					}
					else
					{
						return false;
					}
			 
		 }
		 function get_user()
		 {
			 $this->db->connection_check();
			  $selqry="SELECT * FROM tbl_users  WHERE user_id= '".$this->session->userdata('user_id')."'";   
			  $query=$this->db->query("$selqry");  
				if($query->num_rows >= 1) 
					{ 
						 $num_rows=$query->row();   
						 return $num_rows;
					}
					else
					{
						return false;
					}
			  
		 }
		 function insert_umoney_order($hash)
		 {
			$this->db->connection_check();
			for($i=1;$i<=$this->input->post('i');$i++)
				 {
					  $data = array(		
						'amount' => $this->input->post('price_'.$i),
						'tot_amount' => $this->input->post('quantity_'.$i)*$this->input->post('price_'.$i),
						'quantity' => $this->input->post('quantity_'.$i),
						'product_id' => $this->input->post('product_id'.$i),	
						'user_id' => $this->session->userdata('user_id'),
						'hash_id' => $hash,	
						'status' => 'pending',	
						);
						$this->db->insert('premium_order',$data);
										
				 } 
		 }
	function sendmailcoupon($new_coupon_last_trim,$prod_id)
	{
		$this->db->connection_check();

		$new_coupon_last_trim = explode(",",$new_coupon_last_trim);
		$shoping_coupon 	  = $this->db->query("select * from shopping_coupons where shoppingcoupon_id=$prod_id")->row();
		$current_msg 		  = '<center><h2>Your Coupon Code for: <span style="color:yellow">'.$shoping_coupon->offer_name.'</span> </h2></center><table align="center">';
		
		for($i=0;$i<count($new_coupon_last_trim);$i++)
		{
			$current_msg.=' <tr>
			<td>Coupon Code 1 : </td><td style="color:white; background:black">'.$new_coupon_last_trim[$i].'</td>
			</tr>';
		}
		
		$current_msg.= ' </table>';
		
		$mail_temp 		= $this->db->query("select * from tbl_mailtemplates where mail_id='5'")->row();
		$fe_cont 	 	= $mail_temp->email_template;
		$name 			= $this->db->query("select * from admin")->row();
		$subject 		 = $mail_temp->email_subject;
		//$subject 	    = "Your Missing Ticket Reply";
		$admin_emailid  = $name->admin_email;
		$site_logo 		= $name->site_logo;
		$site_name  	= $name->site_name;
		$contact_number = $name->contact_number;
		$servername 	= base_url();
		$nows 			= date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */	

		$this->load->library('email');
		$gd_api = array(
			'###ADMINNO###'=>$contact_number,
			'###EMAIL###'=>$this->session->userdata('user_email'),
			'###DATE###'=>$nows,
			'###CONTENT###'=>$current_msg,
			'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
			'###SITENAME###' =>$site_name
		);
		
		$gd_message=strtr($fe_cont,$gd_api);
						
		$config = Array(
			'mailtype'  => 'html',
			'charset'   => 'utf-8',
		);
     	//$this->email->initialize($config);        
     	$this->email->set_newline("\r\n");
		$this->email->initialize($config);        
		$this->email->from($admin_emailid,$site_name.'!');
		$this->email->to($user_email);
		$this->email->subject("Coupon Code");
		$this->email->message($gd_message);
		$this->email->send();
		$this->email->print_debugger();
	}
	
		function update_pay_u_money($mihpayid,$TxnID,$useragent,$ipaddress,$old_id)/*seetha 19.8.2015*/
		{
			 $this->db->connection_check();
			$data = array('transaction_id' => $mihpayid, 
							'status'=>'Paid',
							'useragent'=>$useragent,
							'ipaddress'=>$ipaddress,
							'hash_id'=>$TxnID);
			$this->db->where('hash_id',$old_id);
			
			$update_qry = $this->db->update('premium_order',$data); 
			
			$selqry00="SELECT * FROM premium_order  WHERE hash_id='".$TxnID."'";        	     
							$query00=$this->db->query("$selqry00"); 
						//	echo $selqry00;
						//	print_r($query00->result());
						//	exit;
			
			foreach($query00->result() as $query_result)
			{
			$selqry1="SELECT * FROM shopping_coupons  WHERE shoppingcoupon_id= '".$query_result->product_id."'";     
			//echo $selqry1."<br/>";   	     
							$query1=$this->db->query("$selqry1"); 
							
							if($query1->num_rows >= 1) 
							{ 
								 $num_rows1=$query1->row();   
								 
								 $old_coupon=$num_rows1->coupon_code;
								 
								 $new_coupon=explode(",",$num_rows1->remain_coupon_code);
								 $new_coupon_last='';
								 
								 for($c=0;$c<count($this->input->post($query_result->quantity));$c++)
								 {
								 $new_coupon_last.=$new_coupon[$c].",";
								 }
								 $new_coupon_last_trim=rtrim($new_coupon_last,",");
								 $coupon_code=$num_rows1->coupon_code.",".$new_coupon_last_trim;
								 
								 $remain_coupon_code=str_replace("$new_coupon_last","",$num_rows1->remain_coupon_code);
								 
							   $this->sendmailcoupon($new_coupon_last_trim,$query_result->product_id);
								 
								 $data = array(		
								 'coupon_code' => $coupon_code,
								 'remain_coupon_code' => $remain_coupon_code,				
								 );
								 
						
							$this->db->where('shoppingcoupon_id',$query_result->product_id);
							$update_qry = $this->db->update('shopping_coupons',$data);
		
						  }  

			}
			 $query = $this->db->where('user_id', $this->session->userdata('user_id'));
			 $query = $this->db->delete('premium_cart');
									 // exit;
			return true;
		 }
		 function update_hash($old,$new)
		 {
			 $this->db->connection_check();
			$data = array('hash_id' => $new);
			$this->db->where('hash_id',$old);
			$update_qry = $this->db->update('premium_order',$data); 
			return true;
		 }
		 function get_orders()
		 {
			 $this->db->connection_check();
			  $selqry="SELECT * FROM premium_order join shopping_coupons on shopping_coupons.shoppingcoupon_id=premium_order.product_id   WHERE premium_order.status='Paid' and premium_order.user_id= '".$this->session->userdata('user_id')."' ORDER BY `id` desc ";   
			  $query=$this->db->query($selqry); 		  
				if($query->num_rows >= 1) 
					{ 
						 $num_rows=$query->result();   
						 return $num_rows;
					}
					else
					{
						return false;
					}
			  
		 }
		 
		 function gettransations($userid)
		 {
			 $this->db->connection_check();
			 if($userid!="")
			{	
				$this->db->where('user_id',$userid);
				$this->db->order_by('trans_id','desc');
				$miss_cashbacks = $this->db->get('transation_details');
				return $miss_cashbacks->result();
			}
			else
			{
				return 0;
			}
		 }
		 
 		function getcashbacks($userid)
		{
			$this->db->connection_check();
			if($userid!="")
			{	
				$this->db->where('user_id',$userid);
				$this->db->order_by('cashback_id','desc');
				$miss_cashbacks = $this->db->get('cashback');
				return $miss_cashbacks->result();
			}
			else
			{
				return 0;
			}
		}
		function ref_earnings($userid)
		{
			$this->db->connection_check();	
			/*$this->db->select('SUM(transation_amount) as ref_amount');
			$this->db->where('user_id',$userid);
			$this->db->where('transation_reason','Referal Payment');
			$paid_earning= $this->db->get('transation_details');
			$waitng = $paid_earning->row();	
			if($paid_earning->num_rows > 0)
			{
				return $waitng->ref_amount;
			}
			else
			{
				return 0;
			}*/

			$categorytype    	= $this->db->query("select * from tbl_users where user_id='$userid'")->row('referral_category_type');
			$category_names 	= $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row('category_type');
			 
			$spit_cat_names 	= str_split($category_names,8);
			$newcatgory_name    = (ucfirst($spit_cat_names[0])." ".ucfirst($spit_cat_names[1]));

			$result = $this->db->query("SELECT SUM(transation_amount) as ref_amount FROM (`transation_details`)
			WHERE `user_id` = '$userid' AND (`transation_reason` = 'Referal Payment' OR `transation_reason` = 'Referral Cashback amount' OR `transation_reason`= 'Referral Bonus for $newcatgory_name User')
			AND `mode` = 'Credited' AND `transation_status` = 'Approved'");
			//echo $this->db->last_query();die;
			//print_r($result); exit;
			$var = 0;
			if($result)
			{
				$var = $result->row('ref_amount');
				return $var;
			}else
			{
				return 0;
			}
			
		}
		
		function premium_home_slider()
		{
			$this->db->connection_check();
			$this->db->where("banner_status",'1');
			$this->db->where("banner_position",'1');
			$query = $this->db->get('tbl_banners');
			return $query->result();
		}
		
		
		function recently_viewed_index()
		{	
			$this->db->connection_check();
			 $selqry="SELECT * FROM recently_viewed order by id desc limit 0,2";        
			 
			$query=$this->db->query("$selqry"); 
			  
			if($query->num_rows >= 1) 
			{ 
				  $num_rows=$query->result();   
				   return $num_rows; 
			}  
			else  
			{
				return $num_rows=0;   
			}  	
		}
		
		
		function clock_history_stores()
		{	
			$this->db->connection_check();
			 $selqry="SELECT * FROM `click_history` as a INNER JOIN affiliates as b on a.store_name=b.affiliate_name  group by store_name order by click_id ";        			 
			$query=$this->db->query("$selqry"); 
			  
			if($query->num_rows >= 1) 
			{ 
				  $num_rows=$query->result();   
				   return $num_rows; 
			}  
			else  
			{
				return $num_rows=0;   
			}  	
		}
		
		function seoUrl($string) 
		{
			$this->db->connection_check();
			//Lower case everything
			$string = strtolower($string);
			//Make alphanumeric (removes all other characters)
			//$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
			/*New code for spl char replace 22-11-16*/
			$arrstr = array('&'=>'e','ō'=>'o','ā'=>'a','í'=>'i');
			foreach($arrstr as $key=> $newstring)
			{
				$string = str_replace($key, $newstring, $string); 
			}
			/*end 22-11-16*/ 
			//Clean up multiple dashes or whitespaces
			$string = preg_replace("/[\s-]+/", " ", $string);
			//Convert whitespaces and underscore to dash
			$string = preg_replace("/[\s_]/", "-", $string);
			/*$string = base64_encode($string);*/
			$string = str_replace('%','-', $string);
			return $string;
		}
	
	
	
	
		 function transations_bydate($userid)
		 {	
		 	$this->db->connection_check();		
			 if($userid!="")
			{	
				//date_default_timezone_get()
				$startdate = $this->input->post('start_date');
				$enddate   = $this->input->post('end_date');
				$start = date('Y-m-d',strtotime($startdate));
				$end   = date('Y-m-d',strtotime($enddate));
				$this->db->where("transaction_date BETWEEN '$start' AND '$end'");
				$this->db->where('user_id',$userid);
				$miss_cashbacks = $this->db->get('transation_details');

				return $miss_cashbacks->result();
			}
			else
			{
				return 0;
			}
		 }

		/*function category(){
			$this->db->where('category_status','1');
			//$this->db->where('top_category','1');
			$this->db->order_by('date_added','desc');
			$res = $this->db->get('categories');
			if($res){
				return $res->result();
			}
			return false;
		}
		 function get_shopping_coupons()
		{
			$this->db->connection_check();
			$query = $this->db->get('coupons');
			if($query->num_rows>0){
				return $query->result();
			} else {
				return false;
			}
		}*/

		 //New code for cashback table details using date between//


		 function getcashbacks_bydate($userid)
		 {	
		 	$this->db->connection_check();		
			 if($userid!="")
			{	
				//date_default_timezone_get()
				$startdate = $this->input->post('start_date');
				$enddate   = $this->input->post('end_date');
				$start = date('Y-m-d',strtotime($startdate));
				$end   = date('Y-m-d',strtotime($enddate));
				$this->db->where("transaction_date BETWEEN '$start' AND '$end'");
				$this->db->where('user_id',$userid);
				$miss_cashbacks = $this->db->get('cashback');

				return $miss_cashbacks->result();
			}
			else
			{
				return 0;
			}
		 }

		 //End//

		
	//New code for Click History Menu//

	function clickhistory($user_id)
	{
    
	    //SATz 29 04 2016
	    //$this->db->connection_check();
		//this->db->order_by('click_id','desc');
		//this->db->where('user_id',$user_id);
		//cashbackamt = $this->db->get('click_history');
		//SATz
		$result = $this->db->query("select *,c.date_added,a.report_date from click_history as c inner join affiliates as a on a.affiliate_id = c.affiliate_id where c.user_id =$user_id order by click_id desc");
		//SATz
		/*if($cashbackamt->num_rows > 0){

			foreach($cashbackamt->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}*/
		//SATz 05 05 2016
		if($result->num_rows > 0)
		{
			return $result->result();	
		}
		return false;
	}

	//Not needed this function 4-2-17
	/*function clickhistory_bydate($userid)
	{
		$this->db->connection_check();		
			 if($userid!="")
			{	
				$startdate = $this->input->post('start_date');
				$enddate   = $this->input->post('end_date');
				$start = date('Y-m-d',strtotime($startdate)); 
				$end   = date('Y-m-d',strtotime($enddate));
				
				$this->db->where("date_added BETWEEN '$start' AND '$end'");
				$this->db->where('user_id',$userid);
				$miss_cashbacks = $this->db->get('click_history');
				return $miss_cashbacks->result();
			}
			else
			{
				return 0;
			}
	}*/
	//End//

		function store_ajax($page_num,$store_name)
		{
			$this->db->connection_check();
			$last = 20;
			$old_page = $page_num-1;
			$first = $old_page*20;			
			$this->db->limit($last,$first);
			$this->db->order_by('coupon_id','desc');
			$this->db->like('offer_name', $store_name);	
			$result = $this->db->get('coupons');
			//echo $this->db->last_query();
		
			if($result->num_rows > 0){
				return $result->result();	
			}
				return false;
		}

		/*new code for expiry and active coupns details 21-6-17*/
		function active_store_ajax($page_num,$store_name)
		{
			$this->db->connection_check();
			$date = date('Y-m-d');
			$last = 20;
			$old_page = $page_num-1;
			$first = $old_page*20;			
			$this->db->limit($last,$first);
			$this->db->order_by('coupon_id','desc');
			$this->db->like('offer_name', $store_name);	
			$this->db->where('expiry_date >=', $date);
			$this->db->where('coupon_status','completed');
			$result = $this->db->get('coupons');
			//echo $this->db->last_query();
		
			if($result->num_rows > 0){
				return $result->result();	
			}
				return false;
		}

		function expiry_store_ajax($page_num,$store_name)
		{
			$this->db->connection_check();
			$date = date('Y-m-d');
			$last = 20;
			$old_page = $page_num-1;
			$first = $old_page*20;			
			$this->db->limit($last,$first);
			$this->db->order_by('coupon_id','desc');
			$this->db->like('offer_name', $store_name);
			$this->db->where('expiry_date <', $date);	
			$result = $this->db->get('coupons');
			//echo $this->db->last_query();
		
			if($result->num_rows > 0){
				return $result->result();	
			}
				return false;
		}
		/*End 21-6-17*/
		
		function get_top_stores_list_cate()
		{	
		$this->db->connection_check();			
		$this->db->order_by('affiliate_name','ASC');
		//$this->db->where("FIND_IN_SET('$category_id',store_categorys) !=", 0);
		$this->db->where('affiliate_status','1');
		$query = $this->db->get('affiliates');
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	
		}	
		
	function get_stores_cashback_stores_list_cate($category_id)
	{
		$this->db->connection_check();
		$this->db->order_by('affiliate_url','ASC');
		$this->db->where("FIND_IN_SET('$category_id',store_categorys) !=", 0);
		$this->db->where('affiliate_status','1');
		$query = $this->db->get('affiliates');
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
	function getads($position)
	{
		$this->db->connection_check();
		$this->db->where('ads_position',$position);
		$query = $this->db->get('ads');
		if($query->num_rows >= 1)
		{
		   return $query->row();
		}
		return false;
	}
	
	function get_typehead_list($query)
	{
		$this->db->connection_check();
		//$this->db->select('affiliate_name');
		$this->db->like('affiliate_name', $query);	
		$this->db->where('affiliate_status','1');
		$query = $this->db->get('affiliates');
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
	function get_typehead_citys_list($query)
	{
		$this->db->connection_check();
		$this->db->like('city_name', $query);	
		$query = $this->db->get('citys');
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	
	function getadmindetails_main()
	{
		$this->db->connection_check();
		$this->db->connection_check();
		$this->db->where('admin_id','1');
		$query_admin = $this->db->get('admin');
		if($query_admin->num_rows >= 1) 
		{
			return $row = $query_admin->row();
		}
		else
		{
			return false;		
		}	
	}	
	function get_alreadyexist_useremail($email) 
    {  
            $this->db->where('email',$email);	       
 			$query=$this->db->get("tbl_users");  
			if($query->num_rows >= 1)  
			{ 
			    return $row=$query->row();  
			} 
			else 
			{ 
		      	return false;
			}	
    }
	/* 24/10/15 */
	function pending_cashback($user_id){
		$this->db->connection_check();
		$this->db->select('SUM(cashback_amount) as cb_amount');
		$this->db->where('user_id',$user_id);
		$this->db->where('status','Pending');
		$result = $this->db->get('cashback');
		$var = 0;
		if($result){
			$var = $result->row('cb_amount');
		}
		return $var;
	}
	
	function pending_referral($user_id){
		$this->db->connection_check();
		/*$this->db->select('SUM(transation_amount) as ref_amount');
		$this->db->where('user_id',$user_id);
		$this->db->where('transation_reason','Pending Referal Payment');
		$this->db->where('transation_reason','Referral Cashback amount');
		$this->db->where('mode','Credited');
		$this->db->where('transation_status','Pending');
		$result = $this->db->get('transation_details');*/
		$result = $this->db->query("SELECT SUM(transation_amount) as ref_amount FROM (`transation_details`)
		WHERE `user_id` = '$user_id' AND (`transation_reason` = 'Pending Referal Payment' OR `transation_reason` = 'Referral Cashback amount' OR `transation_reason` = 'Referral Bonus for Category One User' OR `transation_reason` = 'Referral Bonus for Category Two User' OR `transation_reason` = 'Referral Bonus for Category Three User' OR `transation_reason` = 'Referral Bonus for Category Four User' 
			OR `transation_reason` = 'Referral Bonus for Category Five User' OR `transation_reason` = 'Referral Bonus for Category Six User' OR `transation_reason` = 'Referral Bonus for Category Seven User' OR `transation_reason` = 'Referral Bonus for Category Eight User' OR `transation_reason` = 'Referral Bonus for Category Nine User' OR `transation_reason` = 'Referral Bonus for Category Ten User'
			OR `transation_reason` = 'Credit Account' OR `transation_reason` = 'Referal Payment')
		AND `mode` = 'Credited' AND `transation_status` = 'Pending'"); //OR `transation_reason` = 'Cashback'
		//echo $this->db->last_query();die;
		//print_r($result); exit;
		$var = 0;
		if($result)
		{
			$var = $result->row('ref_amount');
		}
		return $var;
	}
	
	/* 24/10/15 */

	//new code for search details//

	function search_name()
	{
		//echo "hai"; exit;
		//$my_data=$_GET['q'];
		$graphnew=array();
		 
		    $query = $this->db->query("SELECT `affiliate_url`,`affiliate_name`,`affiliate_logo` FROM (`affiliates`) WHERE `affiliate_status` = '1' order by affiliate_name");

		   if(count($query)!='0')
		   {
		       $cnt=0;
		       foreach($query->result() as $val)    
		       {
		           $graphnew[$cnt] = array(
		           				'logo' => $val->affiliate_logo,
		                       'label' => $val->affiliate_name, 
		                        'type'      =>'Website',                          
		                       'the_link'=>base_url().'busca/'.$val->affiliate_url

		                   ); 
		           $cnt++;
		            
		       }
		       $array2 =($graphnew);
		   }
		   else
		   {
		       return 'no';
		   }   
		   return json_encode ($array2);
	}

	function get_related_stores($storeid)
	{
			$this->db->connection_check();
			$this->db->where("FIND_IN_SET('$storeid',ref_affiliate_categorys) !=", 0);
			$this->db->where('ref_affiliate_status','active');
			$query = $this->db->get('aff_related_stores');
			if($query->num_rows >= 1)
			{
			   $row = $query->row();
			   return $row;
			}
			return false;
	}
	function get_related_categories($category_ids)
	{

		$this->db->connection_check();
			$this->db->where('affiliate_id',$category_ids);
			$query = $this->db->get('affiliates');
			if($query->num_rows >= 1)
			{
			   $row = $query->row();
			   return $row;
			}
			return false;
	}
 	//End//
 	/*New code for storedetails 4-5-16 */
        function storedetail($storename)
        {
                $this->db->connection_check();
                $this->db->like('report_date');
                $this->db->where('affiliate_url', $storename); 
                $this->db->where('affiliate_status','1');
                $query = $this->db->get('affiliates');
                if($query->num_rows >= 1)
                {
                   return $query->result();
                }
                return false;
        }
        /*End*/

        //Pilaventhiran 06/05/2016 START
	function missing_approval($user_id=null)
	{
		$this->db->connection_check();
		if($user_id!="")
		{	
			$this->db->where('user_id',$user_id);
			$this->db->where('status','Canceled');
			$miss_cashbacks = $this->db->get('cashback');
			return $miss_cashbacks->result();
		}
		else
		{
			return 0;
		}
	}

	function add_missing_approval($Cashback_Id=null)
	{
		$this->db->connection_check();
		if($Cashback_Id!="")
		{	
			$this->db->where('cashback_id',$Cashback_Id);
			$miss_cashbacks = $this->db->get('cashback');
			return $miss_cashbacks->result();
		}
		else
		{
			return 0;
		}
	}

	function missing_approval_fetch($Cashback_Id=null)
	{
		$this->db->connection_check();
		if($Cashback_Id!="")
		{	
			$this->db->where('cashback_reference',$Cashback_Id);
			$missing_approval_fetch = $this->db->get('missing_cashback');
			return $missing_approval_fetch->result();
		}
		else
		{
			return 0;
		}
	}

	function missing_approval_count($Reference_Id=null)
	{
		$this->db->connection_check();
		if($Reference_Id!="")
		{	
			$this->db->where('cashback_reference',$Reference_Id);
			$miss_approval_count = $this->db->get('missing_cashback');
			return $miss_approval_count->num_rows();
		}
		else
		{
			return 0;
		}
	}

	function missing_approval_submit_mod($img)
	{
		$this->db->connection_check();
		$name 		= $this->db->query("select * from admin")->row();
		$site_name  = $name->site_name;
		$user_id 	= $this->session->userdata('user_id');
		$date 		= date('Y/m/d');
		$trans_date = $this->input->post('trans_date');
		$ordervalue = $this->input->post('cashback_amt');
		$data 		= array(
			'user_id'=>$user_id,
			'missing_reason'=>'Missing Approval',
			'attachment'=>$img,
			'retailer_name'=>$this->input->post('store'),
			'transaction_ref_id'=>$this->input->post('transaction_reference'),
			'transation_amount'=>$this->input->post('transaction_amount'),
			'click_id'=>$this->input->post('hid_click_id'),
			'coupon_code'=>$this->input->post('coupon_used'),
			'ordervalue'=>$ordervalue,
			'cashback_details'=>$this->input->post('details'),
			'status'=>3,
			'trans_date'=>$trans_date,
			'cashback_reference'=>$this->input->post('cashback_reference'),
			'phone_no'=>$this->input->post('phone_no'),
			'ticket_created_in' =>$date
		);	




		$this->db->insert('missing_cashback',$data);

		$userdetails   = $this->front_model->userdetails($user_id);
		$contact_no    = $userdetails->contact_no;
		
		/*update contact number details 5-6-17*/
		if($contact_no == '')
		{
			$datas = array(		
			'contact_no' => $this->input->post('phone_no'));
			$this->db->where('user_id',$user_id);
			$update_qry = $this->db->update('tbl_users',$datas);
		}
		/*End 5-6-17*/

		$user_details = $this->edit_account($user_id);
		$mail 		  = $user_details->email;
		
		/*new code for username details 5-1-17*/
		$firstname = $user_details->first_name;
		$lastname  = $user_details->last_name;
		if($firstname == '' && $lastname == '')
		{
			$ex_name  = explode('@',$user_details->email);
			$username = $ex_name[0]; 
		}
		else
		{
			$username = $firstname.''.$lastname;
		}	
		/*End 5-1-17*/	
		

		if($user_details->support_tickets == 1)
		{
			//$username 		 = $user_details->first_name." ".$user_details->last_name;
		 	$admindetailssss = $this->front_model->getadmindetails_main();  
			$mail_text 		 = '<p><span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:26px;font-style:normal">Hope you are well.We have received your Missing Approval ticket for Ebay. We will get on it right away &amp; here is what happens next:<br><br> a) Within 7 days the status of this Missing Cashback will be updated to either "Resolved" (and Cashback will be added to your '.$admindetailssss->site_name.' account in "Pending" status immediately), or Sent to retailer (in this case Pending Cashback will be added to your account within 45 days) <br><br> b) We will then share your order details with the retailer to check why your Cashback did not track. If this was just a tracking error, retailer will honour this transaction and status of your Cashback will change to "Confirmed". However if the retailer believes that you did not follow Terms &amp; Conditions then they will not pay us and your Cashback will be "Cancelled"<br><br> Please note that as retailers look at Missing Approval queries out of the normal process, it takes approx 30-45 days to get back. Rest assured that we will keep trying to get an answer for you ASAP but unfortunately will just have to wait for the retailer decision.<br><br> You can check the status of your Missing Approval Ticket under Missing Approval section in your '.$admindetailssss->site_name.' account though we will email you as soon as there is an update. <br><br> Many thanks for your support and co-operation,<br> Team '.$site_name.' </span></p>';			
			$mail_temp 		 = $this->db->query("select * from tbl_mailtemplates where mail_id='13'")->row();
			$fe_cont 		 = $mail_temp->email_template;
			$admindetailssss = $this->front_model->getadmindetails_main(); 
			$subject 		 = $mail_temp->email_subject;
			//$subject 		 = $admindetailssss->site_name." has recieved your Missing Approval Ticket";
			$name 			 = $this->db->query("select * from admin")->row();
			$admin_emailid   = $name->admin_email;
			$site_logo 		 = $name->site_logo;
			$site_name  	 = $name->site_name;
			$contact_number  = $name->contact_number;
			$servername 	 = base_url();
			$nows 			 = date('Y-m-d');	
			$unsuburl     	 = base_url().'un-subscribe/missing_approval/'.$user_id;
	        $myaccount    	 = base_url().'minha-conta';


			$this->load->library('email');

			$gd_api=array(
				'###ADMINNO###'    =>$contact_number,
				'###EMAIL###'      =>$username,
				'###DATE###'	   =>$nows,
				'###CONTENT###'	   =>$mail_text,
				'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
				'###SITENAME###'   =>$site_name,
				'###SEE_STATUS_MISSING###'=>$see_status_missing,
				'###ULINK###'      =>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
				'###MYACLINK###'   =>'<a href='.$myaccount.'>'.$myaccount.'</a>'
			);			   
			$gd_message = strtr($fe_cont,$gd_api);
			
			$config = Array(
			 'mailtype'  => 'html',
			  'charset'  => 'utf-8',
			  );


			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			$this->email->from($admin_emailid,$site_name.'!');
			$this->email->to($mail);
			$this->email->subject($subject);
			$this->email->message($gd_message);
			$this->email->send();
			$this->email->print_debugger();
		}
		return true;
	}
	
	//Pilaventhiran 06/05/2016 END

	function hover()
	{
        $this->db->connection_check();
        $this->db->where('uid','1');
        $query = $this->db->get('user_information');
        if($query->num_rows >= 1)
        {
           return $query->result();
        }
        return false;
	}

    /*New code for shopping page redirect url 13-5-16*/    
    function get_coupons_from_shoppingcoupon_byid($coupon_id)
	{	
		$date = date('m/d/Y');
		$this->db->connection_check();
		$this->db->where('shoppingcoupon_id',$coupon_id);
		//$this->db->where('expiry_date >' $date);
		$result = $this->db->get('shopping_coupons');
		if($result->num_rows > 0)
		{
			return $result->row();	
		}
			return false;
	}
    /*End*/	

//SATz Social login get user details

        	function refer_type($user_id){
        	    $this->db->connection_check();
                $this->db->where('user_id',$user_id);
//                $this->db->where('reg_type',0);
                $this->db->where('password','');
                $query = $this->db->get('tbl_users');
                if($query->num_rows >= 1)
                {
                   //return $query->row();
                	return true;
                }
                return false;	
        	}
	function check_random()
	{
        	$this->db->connection_check();
	        $random = $this->session->userdata('ses_random_ref');
        	$refer_user_id=$this->db->where('random_code',$random)->row('user_id');
        	
        	$data = array(
        		'refer' => $refer_user_id
        		);
        	$this->db->where('user_id',$user_id);
			$this->db->where('admin_status','');
			$update_qry = $this->db->update('tbl_users',$data);
            }

    function cat_details($categoryid)
	{
		
		$this->db->connection_check();
		$this->db->limit(1,0);
		$this->db->where("FIND_IN_SET('$categoryid',ref_affiliate_name) !=", 0);
		$query = $this->db->get('aff_related_stores');
		//echo $this->db->last_query();die;
		if($query->num_rows >= 1)
		{	
			return $query->result();
		}
		return false;
	}        

	/*New code for new design changes 28-6-16*/
	function clock_history_stores_limit()
	{
			$this->db->connection_check();
			 $selqry="SELECT * FROM `click_history` as a INNER JOIN affiliates as b on a.store_name=b.affiliate_name  group by store_name order by click_id limit 0,8";        			 
			$query=$this->db->query("$selqry"); 
			  
			if($query->num_rows >= 1) 
			{ 
				  $num_rows=$query->result();   
				   return $num_rows; 
			}  
			else  
			{
				return $num_rows=0;   
			}  	
	}
	/*End*/
	/*new code for cashback_exclusive 30-7-16*/
	function cashback_exclusive_details($link_name)
	{
	   $this->db->connection_check();
	   $this->db->where('link_name',$link_name);
	   $this->db->where('status','0');
		$cashbackdetails = $this->db->get('cashback_exclusive');
		if($cashbackdetails->num_rows > 0){
			return $cashbackdetails->row();
		}
		return false;
	}
	/*New code 21-4-17*/
	function getcashback_ex_details($extra_param)
	{
	   $this->db->connection_check();
	   $this->db->where('analytics_info',$extra_param);
	   $this->db->where('status','0');
		$cashbackdetails = $this->db->get('cashback_exclusive');
		if($cashbackdetails->num_rows > 0){
			return $cashbackdetails->row();
		}
		return false;
	}
	/*New code 21-4-17*/
	/*New code 22-4-17*/
	function cashback_ex_details($id)
	{
	   $this->db->connection_check();
	   $this->db->where('id',$id);
	   $this->db->where('status','0');
		$cashbackdetails = $this->db->get('cashback_exclusive');
		if($cashbackdetails->num_rows > 0){
			return $cashbackdetails->row();
		}
		return false;
	}
	/*End 22-4-17*/
	
	/*New code for cashback exclusive details using email id 24-4-17*/
	function cashback_ex_details_email($email)
	{
	   $this->db->connection_check();
	   $this->db->where('user_email',$email);
	   $this->db->where('status','0');
		$cashbackdetails = $this->db->get('cashback_exclusive');
		if($cashbackdetails->num_rows >= 1)
		{	
			return $cashbackdetails->result();
		}
		return false;
	}
	/*End 24-4-17*/ 


	function rating()
	{
		
		$this->db->connection_check();

		if(isset($_POST['ratingPoints']))
		{
		    $postID 	  = $this->input->post('postID');
		    $ratingNum 	  = 1;
		    $ratingPoints = $this->input->post('ratingPoints');
		    $date 		  = date("Y-m-d H:i:s");

		    //Check the rating row with same post ID
			$prevRatingQuery = $this->db->query("SELECT * FROM post_rating WHERE post_id = ".$postID)->row();
		    
		    if($prevRatingQuery)
		    {
		    	
		        $ratingNum 	  = $prevRatingQuery->rating_number + $ratingNum; 
		        $ratingPoints = $prevRatingQuery->total_points + $ratingPoints;
		        
		        //Update rating data into the database
		        $datas = array(
		        	'rating_number'=>$ratingNum,
		        	'total_points' =>$ratingPoints,
		        	'modified'	   =>$date
		        	);
		        $this->db->where('post_id',$postID);
				$update_qry = $this->db->update('post_rating',$datas);
		    }	   
		    else
		    {
		    	//Insert rating data into the database
		    	$data = array(
				'post_id' => $postID,
				'rating_number' => $ratingNum,
				'total_points' => $ratingPoints,
				'created' => $date,
				'modified' => $date
				);
				$this->db->insert('post_rating',$data);
		    }	
		    //Fetch rating deatails from database
		    $query2 = $this->db->query("SELECT rating_number, FORMAT((total_points / rating_number),1) as average_rating FROM post_rating WHERE post_id = ".$postID." AND status = 1")->row();
		    
		    if($query2)
		    {
		    	return $query2;
		    }
		    return false;
		    //else
		    //{
		      //  $ratingRow['status'] = 'err';
		    //}
		    
		    //Return json formatted rating data
		}
	}
	/*New function for amount currency format 26-8-16*/
	function currency_format($osiz_amount)
	{
		$osiz_pos_amount 	 = strpos($osiz_amount,',');
		$osiz_pos_amount1 	 = strpos($osiz_amount,'.');
		if($osiz_pos_amount  === false && $osiz_pos_amount1 === false)
		{
		  	$osiz_amount  	 = $osiz_amount.".00";
		  	$osiz_new_amount = preg_replace('/\./', ',', $osiz_amount);
		  	
		  	if($osiz_new_amount == ',00')
			{
  				$osiz_new_amount = '0';
			}
			if($osiz_new_amount == '0')
			{
  				$osiz_new_amount = '0,00';
			}
			return $osiz_new_amount;
		}
		else
		{	
			$osiz_new_amount      = $osiz_amount;
			$osiz_final_amount    = preg_replace('/\./', ',', $osiz_new_amount); 
		
		  	if($osiz_final_amount == ',00')
			{
  				$osiz_final_amount = '0';
			}
			if($osiz_final_amount == '0')
			{
  				$osiz_final_amount = '0,00';
			}
		  	return $osiz_final_amount;
		}
		//echo $osiz_new_amount; 
	}
	/*End*/

	/*function formatinr($input)
    {
        //CUSTOM FUNCTION TO GENERATE ##,##,###.##
        $dec = "";
        $pos = strpos($input, ".");
        if ($pos === false){
            //no decimals   
        } else {
            //decimals
            $dec = substr(round(substr($input,$pos),2),1);
            $input = substr($input,0,$pos);
        }
        $num = substr($input,-3); //get the last 3 digits
        $input = substr($input,0, -3); //omit the last 3 digits already stored in $num
        while(strlen($input) > 0) //loop the process - further get digits 2 by 2
        {
            $num = substr($input,-2).",".$num;
            $input = substr($input,0,-2);
        }
        $osiz_amount = ($num . $dec);
        if($osiz_amount == '')
        {
        	$osiz_amount = '0,00';
        }
        return preg_replace('/\./', ',', $osiz_amount);
    }
	function currency_format($floatcurr, $curr = "INR")
	{
        
        $currencies['BRL'] = array(2,',','.');          //  Brazilian Real

        if ($curr == "INR")
        {    
            return $this->formatinr($floatcurr);
        } else {
            return number_format($floatcurr,$currencies[$curr][0],$currencies[$curr][1],$currencies[$curr][2]);
        }
    }*/



	//view user details 8-9-16
	function view_user($userid)
	{
		$this->db->connection_check();
		$this->db->where('user_id',$userid);        
        $query = $this->db->get('tbl_users');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
            return $query->result();
        }
        return false;
	}

	function update_referral_systems($user_id)
	{
		$this->db->connection_check();
		$fetch = $this->db->query("select * from tbl_users where user_id='$user_id'")->row();

		//New code to check condition to approve user referral amount 9-9-16
		$refer_user       = $fetch->refer;
		$user_balance     = $fetch->balance;

		if($user_balance == '')
		{
			$user_balance = 0;
		}
	
		$admindetails     = $this->front_model->getadmindetails_main();
		$min_withdraw_amt = $admindetails->minimum_cashback;
	
		if($refer_user !=0)
		{
			if($user_balance >= $min_withdraw_amt)
			{
				//echo "hai";
				$trans_details = $this->db->get_where('transation_details',array('user_id'=>$refer_user,'transation_reason'=>'Pending Referal Payment','transation_status'=>'Pending', 'ref_user_tracking_id'=>$user_id));
				$new_numrows   = $trans_details->num_rows();
				if($new_numrows == 1)
				{
					
					$new_fetch    = $trans_details->row();
					$trans_status = $new_fetch->transation_status;
					$report_id    = $new_fetch->report_update_id;
					$tran_amount  = $new_fetch->transation_amount;
					$ref_balance  = $this->db->query("select * from tbl_users where user_id='$refer_user'")->row('balance');
					$referal_mail = $this->db->query("select * from tbl_users where user_id='$refer_user'")->row('referral_mail');
					$user_email   = $this->db->query("select * from tbl_users where user_id='$refer_user'")->row('email');
					$first_name   = $this->db->query("select * from tbl_users where user_id='$refer_user'")->row('first_name');
					$last_name    = $this->db->query("select * from tbl_users where user_id='$refer_user'")->row('last_name');
					$category_type= $this->db->query("select * from tbl_users where user_id='$refer_user'")->row('referral_category_type');
					
					if($ref_balance == '')
					{
						$ref_balance = 0;	
					}


					if($first_name == '' && $last_name == '')
					{
						$username  = explode('@',$user_email);
						$user_name = $username[0];
					}
					else
					{
						$user_name  = $first_name." ".$last_name;
					}
						
					$referrals = $this->db->query("select * from referral_settings where ref_id='$category_type'")->row();	
					
					$ref_by_percentage  = $referrals->ref_by_percentage;
					//$tran_amount  		= $referrals->ref_cashback_rate;
					$new_tot_amt  		= $ref_balance + $tran_amount;  

					if($trans_status == 'Pending')
					{
						$data = array(		
						'transation_status' =>'Approved',
						'transation_reason' =>'Referal Payment',
						'transation_amount' =>$tran_amount,
						);
						$this->db->where('transation_reason','Pending Referal Payment');
						$this->db->where('ref_user_tracking_id',$user_id);
						$this->db->where('user_id',$refer_user);	
						$updates = $this->db->update('transation_details',$data);
						if($updates)
						{
							$data = array(		
							'balance' => $new_tot_amt);
							$this->db->where('user_id',$refer_user);
							$this->db->update('tbl_users',$data);
						
							//mail for pending referral
							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin 		 = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name   = $admin->site_name;
								$admin_no 	 = $admin->contact_number;
								$site_logo 	 = $admin->site_logo;
							}
							$date =date('Y-m-d');
							if($referal_mail == 1)
							{	
								$this->db->where('mail_id',4);
								$mail_template = $this->db->get('tbl_mailtemplates');
								if($mail_template->num_rows >0) 
								{
									$fetch = $mail_template->row();
									$subject = $fetch->email_subject;
									$templete = $fetch->email_template;
									$url = base_url().'my_earnings/';
									$unsuburls	 = base_url().'un-subscribe/referral/'.$refer_user;
							   		$myaccount    = base_url().'minha-conta';
									
									$this->load->library('email');
									$config = Array(
									'mailtype'  => 'html',
									'charset'   => 'utf-8',
									);
											
									$sub_data = array(
									'###SITENAME###'=>$site_name
									);
									
									$subject_new = strtr($subject,$sub_data);
									// $this->email->initialize($config);
									$this->email->set_newline("\r\n");
									$this->email->initialize($config);
									$this->email->from($admin_email,$site_name.'!');
									$this->email->to($user_email);
									$this->email->subject($subject_new);
									$datas = array(
									'###NAME###'=>$user_name,
									'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
									'###SITENAME###'=>$site_name,
									'###ADMINNO###'=>$admin_no,
									'###DATE###'=>date('y-m-d'),
									'###AMOUNT###'=>str_replace('.', ',', $tran_amount),
									'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
									'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>',
									'###STATUS###' => 'Approved'
									);
									$content_pop=strtr($templete,$datas);
									$this->email->message($content_pop);
									$this->email->send();  
								}
							}	
							//mail for pending referral	
						}
					}
				}
			}
		}

		//New code for (type 3 format) user referral cashback amount count of referred user(Pending status) 9-9-16//
		//$categorytype    	= $this->db->query("select * from tbl_users where user_id='$refer_user'")->row('referral_category_type'); //referral_category_type
		
		$ref_balances  		= $this->db->query("select * from tbl_users where user_id='$refer_user'")->row('balance');
		$referral_mail 	    = $this->db->query("select * from tbl_users where user_id='$refer_user'")->row('referral_mail');
		if($ref_balances == '')
		{
			$ref_balances = 0;	
		}

		//New code for referral sytem concepts 12-9-16
		//echo "select * from transation_details where user_id='$refer_user' and transation_status='Pending' and ref_bonus_type!=0"; echo "<br>";
		$trans_details   = $this->db->query("select * from transation_details where user_id='$refer_user' and transation_status='Pending' and ref_bonus_type!=0"); //and transation_reason='Referral Bonus for $category_names User'
		$trans_numrows   = $trans_details->num_rows();
		if($trans_numrows)
		{
			//echo "hai"; exit;
			$new_fetch     = $trans_details->row();
			$report_id     = $new_fetch->report_update_id;
			$categorytype  = $new_fetch->ref_bonus_type;

			/*new changes 25-10-16*/
			$referrals 			= $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();	
			$ref_by_percentage  = $referrals->ref_by_percentage;
			$ref_by_rate 		= $referrals->ref_by_rate;
			$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;
			$category_names     = ucfirst($referrals->category_type);
			$bonus_amount       = $referrals->ref_cashback_rate_bonus;
			$friends_count  	= $referrals->friends_count;
			/*End 25-10-16*/

			
			//$balance_con = $this->db->query("SELECT count(*) as tot_cont FROM (tbl_users AS t1 LEFT JOIN referrals AS t2 ON t1.refer = t2.user_id and t1.email = t2.referral_email) WHERE t1.balance >= $min_withdraw_amt and t1.refer=$refer_user")->row();
			$balance_con   = $this->db->query("SELECT count(*) as tot_cont FROM tbl_users  WHERE balance >= $min_withdraw_amt and refer=$refer_user")->row();
			//echo "<pre>";print_r($balance_con); exit; 

			$total_count = $balance_con->tot_cont;

			if($total_count >= $friends_count)
			{
				if($bonus_by_ref_rate == 1)
				{
					$trans_details = $this->db->get_where('transation_details',array('user_id'=>$refer_user,'transation_status'=>'Pending','ref_bonus_type != '=>0)); //'transation_reason'=>'Referral Bonus for '.$category_names .' User',
					$new_numrows   = $trans_details->num_rows();
					if($new_numrows == 1)
					{
						$new_fetch    = $trans_details->row();
						$report_id    = $new_fetch->report_update_id;


						$data = array(		
						'transation_status' =>'Approved',
						'transation_amount' =>$bonus_amount,
						);
						//$this->db->where('transation_reason','Referral Bonus for '.$category_names.' User');
						$this->db->where('report_update_id',$report_id);
						$this->db->where('user_id',$refer_user);	
						//echo $this->db->last_query();die;
						$updates = $this->db->update('transation_details',$data);
						
						/*New code for referral bouns amount mail notification 20-5-17*/
						$this->db->where('admin_id',1);
						$admin_det = $this->db->get('admin');
						if($admin_det->num_rows >0) 
						{    
							$admin 		 = $admin_det->row();
							$admin_email = $admin->admin_email;
							$site_name   = $admin->site_name;
							$admin_no 	 = $admin->contact_number;
							$site_logo 	 = $admin->site_logo;
						}
						$date =date('Y-m-d');
						if($referral_mail == 1)
						{	
							$this->db->where('mail_id',23);
							$mail_template = $this->db->get('tbl_mailtemplates');
							if($mail_template->num_rows >0) 
							{
								$fetch = $mail_template->row();
								$subject = $fetch->email_subject;
								$templete = $fetch->email_template;
								$url = base_url().'my_earnings/';
								$unsuburls	 = base_url().'un-subscribe/referral/'.$refer_user;
						   		$myaccount    = base_url().'minha-conta';
								
								$this->load->library('email');
								$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
								);
										
								$sub_data = array(
								'###SITENAME###'=>$site_name
								);
								
								$subject_new = strtr($subject,$sub_data);
								// $this->email->initialize($config);
								$this->email->set_newline("\r\n");
								$this->email->initialize($config);
								$this->email->from($admin_email,$site_name.'!');
								$this->email->to($user_email);
								$this->email->subject($subject_new);
								$datas = array(
								'###NAME###'=>$user_name,
								'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
								'###SITENAME###'=>$site_name,
								'###ADMINNO###'=>$admin_no,
								'###DATE###'=>date('y-m-d'),
								'###AMOUNT###'=>str_replace('.', ',', bcdiv($bonus_amount,1,2)),
								'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
								'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>',
								'###STATUS###' => 'Approved' 
								);
								$content_pop=strtr($templete,$datas);
								$this->email->message($content_pop);
								$this->email->send();  
							}
						}	
						/*End 20-5-17*/

						$new_tot_amts  = $ref_balances + $bonus_amount;  
						if($updates)
						{
							$data = array(		
							'balance' => $new_tot_amts);
							$this->db->where('user_id',$refer_user);
							$this->db->update('tbl_users',$data);
						}
					}
				}
			}
		}	
	}

	/*new code for imag url Amazon s3 cocnept 23-9-16*/
	function get_img_url()	
	{
		$this->db->connection_check();
		$img_url_details = $this->db->query("SELECT * FROM amazon_s3_settings WHERE s3_id = 1")->row();
		$img_url_status  = $img_url_details->file_url_status;
		if($img_url_status == 0)
		{
			$img_url = base_url();
		}
		else
		{
			$img_url = $img_url_details->config_file_url."/";
		}
		return $img_url;
	}
	
	function get_css_js_url()	
	{
		//echo "hai"; exit;
		$this->db->connection_check();
		$files_url_details   = $this->db->query("SELECT * FROM amazon_s3_settings WHERE s3_id = 1")->row();
		$css_js_url_status  = $files_url_details->file_url_status;
		if($css_js_url_status == 0)
		{
			$css_js_url = base_url();
		}
		else
		{
			$css_js_url = $files_url_details->config_file_url."/";
		}
		return $css_js_url;
	}
	function store_param_details($store_name)
	{
		$this->db->connection_check();
		$param_details = $this->db->query("SELECT * from affiliates where affiliate_name='$store_name' and affiliate_status=1")->row();
		$param_name    = $param_details->coupon_track_param;
		$ex_param_name = $param_details->coupon_ex_track_param;

		return $param_name."&&".$ex_param_name;
	}
	function store_url_details($store_name)
	{
		$this->db->connection_check();
		$param_details = $this->db->query("SELECT * from affiliates where affiliate_name='$store_name'")->row();
		$store_url     = $param_details->logo_url;
		return $store_url;
	}

	/*New code for bannerdetails 7-11-16*/
	function bannerdetails($cat_type,$bonus,$logtype)
	{
		$this->db->connection_check();
		
		if(($bonus == 0) && ($logtype == 0))
		{
			$condition_details = 'log_notusebonus_notuseapp';
		}

		if(($bonus == 1) && ($logtype == 0))
		{
			$condition_details = 'log_usebonus_notuseapp';
		}
		if(($bonus == 0) && ($logtype == 1))
		{
			$condition_details = 'log_notusebonus_logapp';
		}
		if(($bonus == 1) && ($logtype == 1))
		{
			$condition_details = 'log_usebonus_logapp';
		}

		$this->db->where("banner_status",'1');
		$this->db->where("(banner_cat_type='$cat_type' OR banner_cat_type='all')");
		$this->db->where("banner_condition_details",$condition_details);
		$query = $this->db->get('sales_funnel_banners');
		//echo $this->db->last_query();die;
		return $query->result();

	}
	/*End 7-11-16*/

	function banner_clickcount($banner_id)
	{
		$this->db->connection_check();

		//$click_count = $click_count + 1;
		
		$this->db->where("banner_id",$banner_id);
	    $result = $this->db->get('sales_funnel_banners')->row();

	    $click_count = $result->click_count + 1;

		$datas = array('click_count'=>$click_count);
		$this->db->where('banner_id',$banner_id);
		$update_qry = $this->db->update('sales_funnel_banners',$datas);
		if($update_qry)
		{
			return true;
		}else
		{
			return false;
		}
	}

	/*New code for baseurl 11-11-16*/
	function curPageURL() 
	{
		$pageURL = 'http';
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}

	/*End 11-11-16*/

	/*New code for poup details 17-11-16*/
	function popupdetails()
	{
		$this->db->connection_check();
		$popup_details = $this->db->query("SELECT * from popup_details")->result();
		return $popup_details;
	}
	/*End 17-11-16*/

	/*New code for User IP Address details 17-11-16*/
	function getUserIP()
	{
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];

	    if(filter_var($client, FILTER_VALIDATE_IP))
	    {
	        $ip = $client;
	    }
	    elseif(filter_var($forward, FILTER_VALIDATE_IP))
	    {
	        $ip = $forward;
	    }
	    else
	    {
	        $ip = $remote;
	    }

	    return $ip;
	}
	
	function set_ip_address()
	{	
		$this->db->connection_check();
		$ip_address = $this->getUserIP();
		$ip_details = $this->db->query("SELECT * from notlogusers_ipaddress where ip_address='$ip_address'")->row();
		if(count($ip_details) == 0)
		{
			$data = array(
			'session_count' => 0,
			'ip_address' => $ip_address
			);
			$inserts = $this->db->insert('notlogusers_ipaddress',$data);
		}
	}
	function update_count_details()
	{

		$this->db->connection_check();
		$ip_address = $this->getUserIP();
		$ip_details = $this->db->query("SELECT * from notlogusers_ipaddress where ip_address='$ip_address'")->row();
		if(count($ip_details) > 0)
		{
			$session_count = $ip_details->session_count;
			$count = $session_count + 1;
			$data = array(
			'session_count' => $count,
			);
			$this->db->where('ip_address',$ip_address);
			$updates = $this->db->update('notlogusers_ipaddress',$data);
		}	
	}
	/*End 17-11-16*/
	/*New code for Update count details for logged users 19-11-16*/
	function log_update_count_details()
	{

		$this->db->connection_check();
		$user_id      = $this->session->userdata('user_id');
		$user_details = $this->db->query("SELECT * from `tbl_users` where user_id='$user_id'")->row();
		if(count($user_details) > 0)
		{
			$session_count = $user_details->popup_ses_count;
			$count = $session_count + 1;
			$data = array(
			'popup_ses_count' => $count,
			);
			$this->db->where('user_id',$user_id);
			$updates = $this->db->update('tbl_users',$data);
		}	
	}
	/*End 19-11-16*/

	//New code for data table details in click history page 6-3-17//
	function newclickhistory($draw,$start,$length,$searchtext,$user_id)
	{
		$this->db->connection_check();
		$str 	= ''; 
		$limit  = '';
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);

		if($length != -1)
		{
			$limit = " LIMIT $start,$length";	
		}
		if($searchtext!='')
		{
			$str .=  "AND (`voucher_name` like '%".$searchtext."%' OR `store_name` like '%".$searchtext."%')"; 
			
			//New code for date format//
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 2) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$str .= "OR `date_added` like '%".$strnewdate."%'";
			}
			//End date format//
			$result = $this->db->query("SELECT * FROM `click_history`  WHERE `user_id` = $user_id $str order by click_id desc $limit");
		}
		else
		{
			$result = $this->db->query("SELECT * FROM `click_history`  WHERE `user_id` = $user_id order by click_id desc $limit");
		}

		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}

	function newloja_nao_avisou_compra($draw,$start,$length,$searchtext,$user_id)
	{
		$this->db->connection_check();
		$str 	= ''; 
		$limit  = '';
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);

		if($length != -1)
		{
			$limit = " LIMIT $start,$length";	
		}
		
		if($searchtext!='')
		{
			$cb_amt = str_replace(',', '.', $searchtext);
			$str .= "AND (`retailer_name` like '%".$searchtext."%' OR `missing_reason` like '%".$searchtext."%'
					OR `transation_amount` like '%".$cb_amt."%'
				"; 
			
			//if(strpos($searchtext,'Aprovado') !== false) 
			//{
			//if(strcmp($searchtext,'Aprovado')==0)
			//{
			//	echo "hai";
			if($searchtext == 'Aprovado')
			{
				$status = 0;
				$str   .= "OR `status` like '%".$status."%'";
			}
			if($searchtext == 'Cancelado')
			{
				$status = 1;
				$str   .= "OR `status` like '%".$status."%'";
			}
			if($searchtext == 'Enviar para o varejista')
			{
				$status = 2;
				$str   .= "OR `status` like '%".$status."%'";
			}
			if($searchtext == 'Pendente')
			{
				$status = 3;
				$str   .= "OR `status` like '%".$status."%'";
			}


			//New code for date format//
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 2) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$str .= "OR `trans_date` like '%".$strnewdate."%'";
			}

			$str .= ")";

			//End date format//
			//echo "SELECT * FROM `missing_cashback`  WHERE `user_id` = $user_id $str order by cashback_id desc $limit";
			$result = $this->db->query("SELECT * FROM `missing_cashback`  WHERE `user_id` = $user_id $str order by cashback_id desc $limit");
		}
		else
		{
			$result = $this->db->query("SELECT * FROM `missing_cashback`  WHERE `user_id` = $user_id order by cashback_id desc $limit");
		}

		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	//End//

	function newindique_e_ganhe($draw,$start,$length,$searchtext,$user_id)
	{
		$this->db->connection_check();
		$str 	= ''; 
		$limit  = '';
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);

		if($length != -1)
		{
			$limit = " LIMIT $start,$length";	
		}
		if($searchtext!='')
		{
			$str .=  "AND (`referral_email` like '%".$searchtext."%' OR `status` like '%".$searchtext."%')"; 
			
			//New code for date format//
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 2) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$str .= "OR `date_added` like '%".$strnewdate."%'";
			}
			//End date format//
			$result = $this->db->query("SELECT * FROM `referrals`  WHERE `user_id` = $user_id $str order by referral_id desc $limit");
		}
		else
		{
			$result = $this->db->query("SELECT * FROM `referrals`  WHERE `user_id` = $user_id order by referral_id desc $limit");
		}

		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}

	function newresgate($draw,$start,$length,$searchtext,$user_id)
	{
		$this->db->connection_check();
		$str 	= ''; 
		$limit  = '';
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);

		if($length != -1)
		{
			$limit = " LIMIT $start,$length";	
		}
		if($searchtext!='')
		{
			$cb_amt = str_replace(',', '.', $searchtext);
			$str .=  "AND (`requested_amount` like '%".$cb_amt."%'"; 
			

			if($searchtext == 'Solicitado')
			{
				$status = 'Requested';
				$str   .= "OR `status` like '%".$status."%'";
			}
			if($searchtext == 'Enviado ao Banco')
			{
				$status = 'Processing';
				$str   .= "OR `status` like '%".$status."%'";
			}
			if($searchtext == 'Pagamento Realizado')
			{
				$status = 'Completed';
				$str   .= "OR `status` like '%".$status."%'";
			}
			if($searchtext == 'Não Realizado')
			{
				$status = 'Cancelled';
				$str   .= "OR `status` like '%".$status."%'";
			}


			//New code for date format//
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 2) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$str .= "OR `date_added` like '%".$strnewdate."%'";
			}
			$str .=  ")";
			//End date format//
			//echo "SELECT * FROM `withdraw`  WHERE `user_id` = $user_id $str order by withdraw_id desc $limit";exit; 
			$result = $this->db->query("SELECT * FROM `withdraw`  WHERE `user_id` = $user_id $str order by withdraw_id desc $limit");
		}
		else
		{

			$result = $this->db->query("SELECT * FROM `withdraw`  WHERE `user_id` = $user_id order by withdraw_id desc $limit");
		}

		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}

	function newextrato($draw,$start,$length,$searchtext,$user_id)
	{
		$this->db->connection_check();
		$str 	= ''; 
		$limit  = '';
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);

		if($length != -1)
		{
			$limit = "limit $start,$length";	
		}
		if($searchtext!='')
		{	
			$cb_amt = str_replace(',', '.', $searchtext);
			$str .=  "AND (`cashback_amount` like '%".$cb_amt."%' OR `coupon_id` like '%".$searchtext."%' 
					  OR `affiliate_id` like '%".$searchtext."%'
				"; 
			

			if($searchtext == 'Pendente')
			{
				$status = 'Pending';
				$str   .= "OR `status` like '%".$status."%'";
			}
			if($searchtext == 'Confirmado')
			{
				$status = 'Completed';
				$str   .= "OR `status` like '%".$status."%'";
			}
			if($searchtext == 'Cancelado')
			{
				$status = 'Canceled';
				$str   .= "OR `status` like '%".$status."%'";
			}


			//New code for date format//
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 2) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$str .= "OR `transaction_date` like '%".$strnewdate."%'";
			}
			$str .=  ")";
			//End date format//
			//echo "SELECT * FROM `cashback`  WHERE `user_id` = $user_id $str order by cashback_id desc $limit"; echo "<br>";
			$result1 = $this->db->query("SELECT * FROM `cashback`  WHERE `user_id` = $user_id $str order by transaction_date desc"); //$limit
		}
		else
		{	
			//echo "SELECT * FROM `cashback`  WHERE `user_id` = $user_id order by transaction_date desc"; echo "<br>";
			$result1 = $this->db->query("SELECT * FROM `cashback`  WHERE `user_id` = $user_id order by transaction_date desc"); //$limit    //,pingou_store_id as cash_id,cashback_amount as cash_amt,transaction_amount as trans_amt,status as statuss// //,cashback_id as cash_id,transation_amount as cash_amt,ref_user_tracking_id as trans_amt,transation_status as statuss//
			//$result   = $this->db->query("SELECT transaction_date as trans_date FROM `cashback`  WHERE `user_id` = '$user_id'
			//UNION SELECT transaction_date as trans_date  FROM `transation_details` WHERE user_id = '$user_id'"); 
			//echo $this->db->last_query();echo "<br>";
		}

		if($result1->num_rows > 0){
			return $result1->result();
		}
		return false;
	}

	function newextrato1($draw,$start,$length,$searchtext,$user_id)
	{
		$this->db->connection_check();
		$str 	= ''; 
		$limit  = '';
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);

		if($length != -1)
		{
			$limit = " LIMIT $start,$length";	
		}
		if($searchtext!='')
		{
			$cb_amt = str_replace(',', '.', $searchtext);
			$str .=  "AND (`transation_amount` like '%".$cb_amt."%' OR `transation_reason` like '%".$searchtext."%'
					  OR `cashback_id` like '%".$searchtext."%' 
				"; 
			

			if($searchtext == 'Pendente')
			{
				$status = 'Pending';
				$str   .= "OR `transation_status` like '%".$status."%'";
			}
			 
			if($searchtext == 'Confirmado')
			{
				$status = 'Completed';
				$str   .= "OR `transation_status` like '%".$status."%' AND `transation_status` like '%Approved%' AND `transation_status` like '%Paid%'";
			}
			if($searchtext == 'Cancelado')
			{
				$status = 'Canceled';
				$str   .= "OR `transation_status` like '%".$status."%'";
			}


			//New code for date format//
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 2) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$str .= "OR `transaction_date` like '%".$strnewdate."%'";
			}
			$str .=  ")";
			//End date format//
			//echo "SELECT * FROM `transation_details`  WHERE `user_id` = $user_id AND transation_reason !='Cashback' $str order by transaction_date desc $limit"; echo "<br>";
			$result2 = $this->db->query("SELECT * FROM `transation_details`  WHERE `user_id` = $user_id AND transation_reason !='Cashback' $str order by transaction_date desc"); //$limit
		}
		else
		{	
			//echo "SELECT * FROM `transation_details`  WHERE `user_id` = $user_id AND transation_reason !='Cashback' order by transaction_date desc "; exit;
			$result2 = $this->db->query("SELECT * FROM `transation_details`  WHERE `user_id` = $user_id AND transation_reason !='Cashback' order by transaction_date desc");     //$limit
			 
			//,pingou_store_id as cash_id,cashback_amount as cash_amt,transaction_amount as trans_amt,status as statuss/ //,cashback_id as cash_id,transation_amount as cash_amt,ref_user_tracking_id as trans_amt,transation_status as statuss//
			//$result   = $this->db->query("SELECT transaction_date as trans_date FROM `cashback`  WHERE `user_id` = '$user_id'
								    //UNION SELECT transaction_date as trans_date  FROM `transation_details` WHERE user_id = '$user_id'"); 
			//echo $this->db->last_query();
		}

		if($result2->num_rows > 0){
			return $result2->result();
		}
		return false;
	}

	/*New code for approved missing cashback details 14-3-17*/
	function approve_missing_cashback($user_id)
	{
		$this->db->connection_check();
		if($user_id!="")
		{	
			$this->db->connection_check();
			$this->db->select('SUM(transation_amount) as trans_amt');
			$this->db->where('status',0);
			$this->db->where('user_id',$user_id);
			$this->db->where('missing_reason','Missing Cashback');
			$paid_earning= $this->db->get('missing_cashback');
			if($paid_earning->num_rows > 0)
	        {
	            return $paid_earning->row('trans_amt');
	        }
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	/*end 14-3-17*/

	/*function newapprove_missing_cashback($user_id)
	{
		$this->db->connection_check();
		if($user_id!="")
		{	
			$this->db->connection_check();
			$this->db->select('SUM(cancel_msg) as trans_amt');
			$this->db->where('status',0);
			$this->db->where('user_id',$user_id);
			$this->db->where('missing_reason','Missing Cashback');
			$paid_earning= $this->db->get('missing_cashback');
			if($paid_earning->num_rows > 0)
	        {
	            return $paid_earning->row('trans_amt');
	        }
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}*/

	/*New code for approved missing cashback details 14-3-17*/
	function missing_approve_ticket($user_id)
	{
		$this->db->connection_check();
		if($user_id!="")
		{	
			$this->db->connection_check();
			$this->db->select('SUM(transation_amount) as trans_amt');
			$this->db->where('status',0);
			$this->db->where('user_id',$user_id);
			$this->db->where('missing_reason','Missing Approval');
			$paid_earning= $this->db->get('missing_cashback');
			if($paid_earning->num_rows > 0)
	        {
	            return $paid_earning->row('trans_amt');
	        }
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	/*New code for approved missing cashback details 14-3-17*/
	function newmissing_approve_ticket($user_id)
	{
		$this->db->connection_check();
		if($user_id!="")
		{	
			$this->db->connection_check();
			$this->db->select('SUM(cancel_msg) as trans_amt');
			$this->db->where('status',0);
			$this->db->where('user_id',$user_id);
			$this->db->where('missing_reason','Missing Approval');
			$paid_earning= $this->db->get('missing_cashback');
			if($paid_earning->num_rows > 0)
	        {
	            return $paid_earning->row('trans_amt');
	        }
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function unic_bonus_amount($user_id)
	{
		$this->db->connection_check();
		if($user_id!="")
		{	
			$this->db->connection_check();
			$this->db->select('SUM(transation_amount) as trans_amt');
			$this->db->where('transation_status','Credited');
			$this->db->where('user_id',$user_id);
			$this->db->where('transation_reason','Unic bonus amonut Added');
			$paid_earning= $this->db->get('transation_details');
			if($paid_earning->num_rows > 0)
	        {
	            return $paid_earning->row('trans_amt');
	        }
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	function credit_account_details($user_id)
	{
		$this->db->connection_check();
		if($user_id!="")
		{	
			$this->db->connection_check();
			$this->db->select('SUM(transation_amount) as trans_amt');
			$this->db->where('transation_status','Approved');
			$this->db->where('user_id',$user_id);
			$this->db->where('transation_reason','Credit Account');
			$paid_earning= $this->db->get('transation_details');
			if($paid_earning->num_rows > 0)
	        {
	            return $paid_earning->row('trans_amt');
	        }
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}	
	/*end 14-3-17*/

	function delete_account_details($user_id)
	{
		$this->db->connection_check();
		if($user_id!='')
		{
			
			$userdetails = $this->db->query("SELECT * from `tbl_users` where user_id=$user_id")->row();
			$email_id    = $userdetails->email;
			$data = array(
				'status' 		  => 0,
				'admin_status' 	  => 'deleted',
				'street' 		  => '',
				'streetnumber' 	  => '',
				'city' 			  => '',
				'state' 		  => '',
				'country' 		  => '',
				'contact_no' 	  => '',
				'celular_no' 	  => '',
				'zipcode' 		  => '',
				'complemento' 	  => '',
				'bairro' 		  => '',
				'account_holder'  => '',
				'bank_name' 	  => '',
				'branch_name'     => '',
				'account_number'  => '',
				'ifsc_code' 	  => '',
				'balance' 		  => 0,
				'cashback_mail'   => 0,
				'withdraw_mail'   => 0,
				'referral_mail'   => 0,
				'newsletter_mail' => 0,
				'acbalance_mail'  => 0,
				'support_tickets' => 0,
				'bonus_benefit'   => 0,
				'unic_bonus_code' => 0,
				'referral_category_type'=> 0,
				'referral_amt'    => 0,
				'profile'   	  => '',
				'ref_user_cat_type'=> 0,
				'popup_ses_count' => 0,
				'acc_type'  	  => '',
				'acc_id' 		  => ''
			);

			$this->db->where('user_id',$user_id);
			$update_qry = $this->db->update('tbl_users',$data);

			$query = $this->db->where('user_id',$user_id);
			$query = $this->db->delete('cashback');

			$query = $this->db->where('user_id',$user_id);
			$query = $this->db->delete('click_history');

			$query = $this->db->where('user_id',$user_id);
			$query = $this->db->delete('missing_cashback');

			$query = $this->db->where('user_tracking_id',$user_id);
			$query = $this->db->delete('tbl_report');

			$query = $this->db->where('user_id',$user_id);
			$query = $this->db->delete('transation_details');

			$query = $this->db->where('user_id',$user_id);
			$query = $this->db->delete('withdraw');

			$data = array(
				'subscriber_status' => 0,
			);
				
			$this->db->where('subscriber_email',$email_id);
			$update_qry = $this->db->update('subscribers',$data);

			return true;	
		}
		else
		{
			return false;
		}	
	}

	function update_firstpoup_satus($user_id,$poup_status)
	{
		$this->db->connection_check();
		if($user_id!='')
		{
			$data = array(
				'first_ac_popup_status' => 0,
			);

			$this->db->where('user_id',$user_id);
			$update_qry = $this->db->update('tbl_users',$data);
			return true;	
		}
		else
		{
			return false;
		}
	}

	/*New code for update referal link clickcount details 18-4-17*/
	function update_refclick_count($user_id)
	{
		$this->db->connection_check(); 
		$selects = $this->db->query("SELECT * from `tbl_users` where user_id='$user_id' AND admin_status=''")->row('reflink_click_counts');
		
			$ref_clickcount = $selects + 1;
			$data = array(
			'reflink_click_counts' => $ref_clickcount,
			);
			$this->db->where('user_id',$user_id);
			$updates = $this->db->update('tbl_users',$data);
	}
	/*End 18-4-17*/

	/*New code for Champaign Users mail Notifications 24-4-17*/
	function champaign_mail_notify($cham_details)
	{
		$this->db->connection_check();
		$this->load->library('email');

		$pingou_mail 	 = $cham_details->pingou_mail;
		$user_email  	 = $cham_details->user_email;
		$ename       	 = explode('@', $user_email);
		$first_name    	 = $ename[0]; 
		$campaign_name   = $cham_details->campaign_name;
		$expiry_date  	 = $cham_details->expirydate;
		$click_counts  	 = $cham_details->cash_click_counts;
		$signup_counts 	 = $cham_details->cash_sign_up_counts;
		$conversion_rate = round((($signup_counts/$click_counts)*100),1)."%";

		//echo "<pre>"; print_r($cham_details); exit;
		$this->db->where('admin_id',1);
	    $admin_det = $this->db->get('admin');
	    if($admin_det->num_rows >0) 
	    {    
	        $admin       = $admin_det->row();
	        $admin_email = $admin->admin_email;
	        $site_name   = $admin->site_name;
	        $admin_no    = $admin->contact_number;
	        $site_logo   = $admin->site_logo;
	    }

        $this->db->where('mail_id',22);
        $mail_template = $this->db->get('tbl_mailtemplates');
      
        if($mail_template->num_rows >0) 
        {        
            $fetch     = $mail_template->row();
            $subject   = $fetch->email_subject; 
            $templete  = $fetch->email_template;  
            $redir_url = base_url().'indique_e_ganhe';

            $this->load->library('email'); 

            $config = Array(
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            );

            $this->email->set_newline("\r\n");
            $this->email->initialize($config);        
            $this->email->from($admin_email,$site_name.'!');
            //$this->email->reply_to($pingou_mail);
            //$this->email->to($user_email,$pingou_mail);
            $arrto[] = $user_email;
			$arrto[] = $pingou_mail;
			$this->email->to($arrto,$site_name.'!');
            $this->email->subject($subject);

            $data = array(
            '###ADMINNO###'=>$admin_no,
            '###DATE###'=>$date,
            '###USER-FIRST-NAME###'=>$first_name,
            '###CAMPAIGN-NAME###' =>$campaign_name,
            '###EXPIRATION-DATE###'=>$expiry_date,
            '###CLICK-COUNT###'=>$click_counts, 
            '###SIGNUP-COUNT###'=>$signup_counts,
            '###CONVERSION-RATE###' =>$conversion_rate,
            '###PINGOU-MAIL###'=>$pingou_mail,
            '###LINK###'=>$redir_url,
			'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
			'###SITENAME###'=>$site_name,
			'###DATE###'=>date('y-m-d')
            );
           
            $content_pop=strtr($templete,$data);  
            $this->email->message($content_pop);
            $this->email->send(); 
        }
	}
	/*End 24-4-17*/

	/*New code for pretty link url details 27-4-17*/
	function get_pretty_details($url)
	{
	   $this->db->connection_check();
	   $this->db->where('incoming_link',$url);
	   $this->db->where('status','1');
		$userdetails = $this->db->get('pretty_link_details');
		if($userdetails->num_rows > 0){
			return $userdetails->row();
		}
		return false;
	}	
	/*end 27-4-17*/

	function aaa($name)
	{	
		$html = file_get_contents(base_url().$name);
		//$html = file_get_contents("https://www.pingou.gmeec.com.br/netshoesOZIS");
		//echo "<pre>"; print_r($html);exit;
		//sleep(10);
		$doc = new DomDocument();
		@$doc->loadHTML($html);
		$xpath = new DOMXPath($doc);
		$query = '//*/meta';
		$metas = $xpath->query($query);
		$rmetas = array();
		foreach ($metas as $meta) {
		    $property = $meta->getAttribute('property');
		    $content = $meta->getAttribute('content');
		    if(!empty($property) && preg_match('#^og:#', $property)) {
		        $rmetas[$property] = $content;
		    }
		}
		 return $rmetas;
	}


	function shortcut_details($content)
	{
		$this->db->connection_check();

		$user_id 		= $this->session->userdata('user_id');
		if($user_id!='')
		{
			$userdetails    = $this->userdetails($user_id);
			$firstname      = $userdetails->first_name;
			$lastname       = $userdetails->last_name;
			$emailid        = $userdetails->email;
			$random_code 	= $userdetails->random_code;
			$category_type  = $userdetails->referral_category_type;
			$cat_details    = $this->db->query("select * from referral_settings where ref_id='$category_type'")->row();
	 	  	$log_content    = $cat_details->notify_log_users;
	    }
	    
	    
		if($random_code == '')
		{
			$random_code = '';	
		}

	    $getadmindetails    = $this->front_model->getadmindetails();  
	    //$log_content  	= $getadmindetails[0]->log_content;
	    $unlog_content  	= $getadmindetails[0]->unlog_content;
	    $log_status     	= $getadmindetails[0]->log_status;
	    $unlog_status   	= $getadmindetails[0]->unlog_status;
	    
	    $refer_code         = $userdetails->random_code;
	    if($refer_code == '')
	    {
	      $refer_code = '';
	    }

	    $date     = date('Y-m-d');
	    $newdate  = explode('-',$date);
	    $year     = $newdate[0];
	    $month    = $newdate[1];
	    $day      = $newdate[2];
	    $newday   = $day;
	    $off_year = date('y');
	    $local_month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); 
	    $braz_months_names = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
	    $dt = date('F');
	    $current_month     = str_ireplace($local_month_names, $braz_months_names, $dt);

	    $get_cat_type        = $this->referal__category();
	    $get_cat_details     = $this->get_referral_settings($get_cat_type);

	    $type_one_percentage = $get_cat_details->ref_cashback;
	    $type_one_days       = $get_cat_details->valid_months;
	    $type_two_amount     = $get_cat_details->ref_cashback_rate;  
	    $type_three_amount   = $get_cat_details->ref_cashback_rate_bonus;
	    $type_three_users    = $get_cat_details->friends_count;
	    $unique_bonus        = str_replace('.', ',',$get_cat_details->category_bonus_amount);

	    if($user_id == '')
	    {
		    if($_REQUEST['ref'])
			{	
				$ref_id  = $_REQUEST['ref'];
				
				$this->db->where('random_code',$ref_id);
				$user_details = $this->db->get('tbl_users');
				if($user_details->num_rows() > 0)
				{
					$userdetails  	   = $user_details->row();
					$ref_cat_type 	   = $userdetails->referral_category_type;

					$get_cat_detailss    = $this->get_referral_settings($ref_cat_type);
					$get_future_category = $get_cat_detailss->new_ref_cat_types;
					$new_fut_catdetails  = $this->get_referral_settings($get_future_category);
		    		$unique_bonus        = str_replace('.', ',',$new_fut_catdetails->category_bonus_amount);
				}
			}
			else
			{
				$get_cat_detailss  = $this->get_referral_settings(1);
		   	 	$unique_bonus     = str_replace('.', ',',$get_cat_detailss->category_bonus_amount);
			}

	    }
	    else
	    {
	    	if($firstname =='' && $lastname =='')
			{ 
			  $emails    = explode('@', $emailid);
			  $usernames = $emails[0];  
			}
			else
			{
			  $usernames = ucfirst($firstname)." ".ucfirst($lastname);
			}
	    }
	     
	    $data = array(
	    '###USER-NAME###' => $usernames,
	    '###REFERRAL-PARAMETER###'=> $refer_code,
	    '###DD###'    =>$newday,
	    '###MM###'    =>$month,
	    '###MONTH###' =>$current_month,
	    '###YYYY###'  =>$year,
	    '###YY###'    =>$off_year,
	    '###Type-ONE###'=>$type_one_percentage,
	    '###Type-ONE-days###'=>$type_one_days,
	    '###Type-TWO###'=>$type_two_amount,
	    '###Type-THREE###'=>$type_three_amount,
	    '###Type-THREE-number-of-users###'=>$type_three_users,
	    '###UNIQUE-BONUS###' => $unique_bonus,
	    
	    );
	    return $contents = strtr($content,$data);
	}

	function storeshort_details($storename)
	{

		$this->db->connection_check();
		$user_id = $this->session->userdata('user_id');

		$storedetail = $this->db->query("SELECT * from affiliates where affiliate_url='$storename'")->row();                  

	    /*new code for store page shrotcut details 6-6-17*/
	    $date     = date('Y-m-d');
	    $newdate  = explode('-',$date);
	    $year     = $newdate[0];
	    $month    = $newdate[1];
	    $day      = $newdate[2];
	    $days     = explode(' ',$day);
	    $newday   = $days[0];
	    $off_year = date('y');
	    $local_month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); 
	    $braz_months_names = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
	    $dt = date('F');
	    $current_month     = str_ireplace($local_month_names, $braz_months_names, $dt);
	
	    $affiliate_cashback_type = $storedetail->affiliate_cashback_type;
	    if($affiliate_cashback_type == 'Percentage')
	    {
	        $cashbacks = $storedetail->cashback_percentage."%";
	    }
	    else if($affiliate_cashback_type == 'Flat')
	    {
	        $cashbacks = "R$ ".$storedetail->cashback_percentage;
	    }
	    else
	    {
	        $cashbacks = ""; 
	    }

      	$get_cat_type        = $this->front_model->referal__category();
      	$get_cat_details     = $this->front_model->get_referral_settings($get_cat_type);
      	$type_one_percentage = $get_cat_details->ref_cashback;
      	$type_one_days       = $get_cat_details->valid_months;
      	$type_two_amount     = $get_cat_details->ref_cashback_rate;  
      	$type_three_amount   = $get_cat_details->ref_cashback_rate_bonus;
      	$type_three_users    = $get_cat_details->friends_count;
      	$unique_bonus        = str_replace('.', ',',$get_cat_details->category_bonus_amount);

      	if($user_id == '')
      	{
            if($_REQUEST['ref'])
            { 
              $ref_id  = $_REQUEST['ref'];
              
              $this->db->where('random_code',$ref_id);
              $user_details = $this->db->get('tbl_users');
              if($user_details->num_rows() > 0)
              {
                $userdetails       = $user_details->row();
                $ref_cat_type      = $userdetails->referral_category_type;

                $get_cat_detailss    = $this->front_model->get_referral_settings($ref_cat_type);
                $get_future_category = $get_cat_detailss->new_ref_cat_types;
                $new_fut_catdetails  = $this->front_model->get_referral_settings($get_future_category);
                $unique_bonus        = str_replace('.', ',',$new_fut_catdetails->category_bonus_amount);
              }
            }
            else
            {
              $get_cat_detailss  = $this->front_model->get_referral_settings(1);
              $unique_bonus      = str_replace('.', ',',$get_cat_detailss->category_bonus_amount);
            }
      	}

      	$datass = array(
        '###STORE-NAME###'=>$storedetail->affiliate_name,
        '###CASHBACK###'=>$cashbacks,
        '###TRACKING-SPEED###'=>$storedetail->report_date,
        '###ESTIMATED-PAYMENT###'=>$storedetail->retailer_ban_url,
        '###COUPON-NUMBER###' =>$count_act_coupons,
        '###DD###'    =>$newday,
        '###MM###'    =>$month,
        '###MONTH###' =>$current_month,
        '###YYYY###'  =>$year,
        '###YY###'    =>$off_year,
        '###STORE-IMG-ONE###'  =>$this->front_model->get_img_url()."uploads/affiliates/".$storedetail->store_one_img,
        '###STORE-IMG-TWO###'  =>$this->front_model->get_img_url()."uploads/affiliates/".$storedetail->store_two_img,
        '###STORE-IMG-THREE###'=>$this->front_model->get_img_url()."uploads/affiliates/".$storedetail->store_three_img,
        '###STORE-IMG-FOUR###' =>$this->front_model->get_img_url()."uploads/affiliates/".$storedetail->store_four_img,
        '###STORE-IMG-FIVE###' =>$this->front_model->get_img_url()."uploads/affiliates/".$storedetail->store_five_img,
        '###STORE-IMG-SIX###'  =>$this->front_model->get_img_url()."uploads/affiliates/".$storedetail->store_six_img,
        '###REFERRAL-PARAMETER###'=> $refer_code,
        
        '###Type-ONE###'=>$type_one_percentage,
        '###Type-ONE-days###'=>$type_one_days,
        '###Type-TWO###'=>$type_two_amount,
        '###Type-THREE###'=>$type_three_amount,
        '###Type-THREE-number-of-users###'=>$type_three_users,
        '###UNIQUE-BONUS###' => $unique_bonus,
        '###USER-NAME###' => $usernames,
        );
		
		$store_detail   = $this->db->query("SELECT * from affiliates where affiliate_url='$storename'")->row();
		$terms          = $store_detail->terms_and_conditions;

		$report_date    	= $store_detail->report_date;
		$affiliate_name 	= $store_detail->affiliate_name;
		$miss_cash_amt_type = $store_detail->miss_cash_amt_type;
		$terms_and_con  	= strtr($terms,$datass);
		

		return $contents = array('report_date' =>$report_date,'affiliate_name'=>$affiliate_name,'miss_cash_amt_type'=>$miss_cash_amt_type,'terms_and_con'=>$terms_and_con);
      	/*End 6-6-17*/
	}
}	
?>
