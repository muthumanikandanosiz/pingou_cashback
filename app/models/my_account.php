<?php 
 
?>
<section class="main_sec">
  <div class="container">
    <h3 class="page_tit text-center"> Profile Information</h3>
    <?php 
    if($userdetails->profile_picture!='')
    {
      $id='personal_info_form';
    }
    else
    {
      $id='personal_info_form_photo';
    }

    $success = $this->session->flashdata("success");  
    $error   = $this->session->flashdata("error");  
    if($success!="") 
    {
      ?>  
      <div  class="alert alert-success alert-dismissible" role="alert" style="display:block; height:auto;">
        <button style="" type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">&times;</span></button>
          <strong>Success!</strong> <span><?php  echo $success;  ?></span>
      </div>
      <?php
    } 
    if($error!="") 
    {
      ?> 
      <div  class="alert alert-danger alert-dismissible" role="alert" style="display:block; height:auto;">
        <button style="" type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">&times;</span></button>
          <strong>Oops!</strong> <span><?php echo $error; ?></span>  
      </div>
      <?php 
    } 
    ?>
    <div class="profi_tab">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="<?php if($type == 'profile'){echo 'active';}?>"><a href="<?php echo base_url('my_account/profile'); ?>">Profile Details</a></li> <!-- aria-controls="home" role="tab" data-toggle="tab" -->
        <li role="presentation" class="<?php if($type == 'changepassword'){echo 'active';}?>"><a href="<?php echo base_url('my_account/changepassword'); ?>">Change Password </a></li> <!-- aria-controls="profile" role="tab" data-toggle="tab" -->
        <li role="presentation" class="<?php if($type == 'bank'){echo 'active';}?>"><a href="<?php echo base_url('my_account/bank'); ?>" > Bank Details </a></li> <!-- aria-controls="messages" role="tab" data-toggle="tab" -->
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">

        <?php if($type == 'profile')
        {
          ?>
          <div role="tabpanel" class="tab-pane active" id="profile">
            <div class="pro_content">
              <form class="form-horizontal" enctype="multipart/form-data" id="personal_info_form" method="post" action="">

                <div class="form-group">
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label for="" class="col-sm-4 control-label">User Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="uname" name="uname" placeholder="User Name" value="<?php echo $userdetails->kc_username;?>">
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label for="" class="col-sm-4 control-label">Email ID</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="email" name="email" placeholder="johndeo@gmail.com" value="<?php echo $userdetails->kc_email;?>">
                    </div>
                  </div>  
                </div>

                <div class="form-group">
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label for="" class="col-sm-4 control-label">First Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" value="<?php echo $userdetails->first_name;?>">
                    </div>
                    </div>
                    <?php if($userdetails->last_name ==0){ $last_name = ''; }else{ $last_name = $userdetails->last_name; } ?>
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label for="" class="col-sm-4 control-label">Last Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="lname" name="lname" placeholder="Last name" value="<?php echo $last_name;?>">
                    </div>
                    </div>  
                </div>

                <div class="form-group">
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label for="" class="col-sm-4 col-xs-12 control-label">Gender</label>
                    <?php if($userdetails->gender != '') 
                    { ?>
                      <div class="col-sm-4 col-xs-6">
                        <label class="control control--radio">Male    
                          <input type="radio"  name="gender" value="male" <?php if($userdetails->gender == "male"){ echo "checked"; }  ?>  >
                          <div class="control__indicator"></div>
                        </label>
                      </div>
                      <?php
                    }
                    else
                    {
                      ?>
                      <div class="col-sm-4 col-xs-6">
                        <label class="control control--radio">Male    
                          <input type="radio"  name="gender" value="male" checked="checked">
                          <div class="control__indicator"></div>
                        </label>
                      </div>
                      <?php 
                    }
                    ?>
                    <div class="col-sm-4 col-xs-6">
                      <label class="control control--radio">Female    
                        <input type="radio"  name="gender" value="female" <?php if($userdetails->gender == "female"){ echo "checked"; } ?> >
                        <div class="control__indicator"></div>
                      </label>
                    </div>
                  </div>  
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label for="" class="col-sm-4 control-label">Phone Number</label>
                    <div class="col-sm-8">
                      <input type="text"  class="form-control" id="phone" name="phone" placeholder="Phone Number" value="<?php echo $userdetails->phone;?>">
                    </div>
                  </div>  
                </div>

                <div class="form-group">
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label for="" class="col-sm-4 control-label">Address</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="<?php echo $userdetails->address;?>"> 
                      <!-- <textarea id="address" name="address" class="form-control" cols="57" rows="5" style="height:130px"><?php echo $userdetails->address;?></textarea>-->
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label for="" class="col-sm-4 control-label">Postal Code</label>
                    <div class="col-sm-8">
                      <input type="text"  class="form-control" id="postal_code" name="postal_code" placeholder="Postal code" value="<?php echo $userdetails->postal_code;?>">
                    </div>
                  </div>  
                </div>

                <div class="form-group">
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label class="col-sm-4 control-label" for="">City</label>
                    <div class="col-sm-8">
                      <input type="text" placeholder="City" id="city" name="city" class="form-control" value="<?php echo $userdetails->city;?>">
                    </div>
                  </div>
                    
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label class="col-sm-4 control-label" for="">Country</label>
                    <div class="col-sm-8">
                      <input type="text" placeholder="Country" id="country" name="country" class="form-control" value="<?php echo $userdetails->country;?>">
                    </div>
                  </div>  
                </div>

                <div class="form-group mar-top-40">
                  <input type="hidden" name="save_profile" value="save_profile" />
                  <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                    <button type="submit" name="save_profile" value="Update" class="btn btnFrmSubmit ">Save</button>
                	</div>
                  <div class="col-md-6 col-sm-6 col-xs-6"> 
                    <button type="reset" class="btn btnFrmSubmit">Cancel</button>
              	  </div>
              	</div>

              </form>
            </div>
          </div>
          <?php 
        } 
        
        if($type == 'changepassword')
        {
          ?>
          <div role="tabpanel" class="tab-pane active" id="password">
            <div class="pro_content">
              <form class="form-horizontal bv-form" action="<?php echo base_url('my_account/changepassword'); ?>" method="post" id="change_form">
                <div class="form-group">
                  <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-xs-12">
                    <label for="" class="col-sm-5 col-md-4 control-label">Current Password</label>
                    <div class="col-md-6 col-sm-7">
                      <input type="password" class="form-control" id="old" name="old" placeholder="">
                    </div>
                  </div>
                </div>  
                <div class="form-group">    
                  <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-xs-12">
                    <label for="" class="col-sm-5 col-md-4 control-label">New Password</label>
                    <div class="col-md-6 col-sm-7">
                      <input type="password" class="form-control" id="new" name="new" placeholder="">
                    </div>
                  </div>  
                </div>
        
                <div class="form-group">
                  <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-xs-12">
                    <label for="" class="col-sm-5 col-md-4 control-label">Confirm new Password</label>
                    <div class="col-md-6 col-sm-7">
                      <input type="password" class="form-control" id="new_confirm" name="new_confirm" placeholder="">
                    </div>
                  </div>
                </div>
        
                <div class="form-group mar-top-40">
                  <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-xs-12 text-center">
                    <div class="col-sm-12 col-md-4"></div>
                    <input type="hidden" name="changepassword" value="changepassword" />
                    <div class="col-md-6 col-sm-8">
                      <input type="submit" name="changepassword" value="Submit" class="btn btnFrmSubmit">
                    </div>
                  </div>
                </div>  
              </form>
            </div>
          </div>
          <?php 
        }

        if($type == 'bank')
        {
          if($walletaccount && count($walletaccount) && !empty($walletaccount) ){
          $walletaccount = unserialize($walletaccount['account_details']);
          $accountdetails['account_number'] = $walletaccount['account_number'];
         /* $accountdetails['bank_swift_code'] = $walletaccount['bank_swift_code'];
          $accountdetails['account_name'] = $walletaccount['account_name'];*/
          $accountdetails['bank_name'] = $walletaccount['bank_name'];
          /*if(isset($walletaccount['description']))
          {
          $accountdetails['description'] = $walletaccount['description'];
          }
          else{
            $accountdetails['description'] = '';
          }*/
          }else{
            $accountdetails['account_number'] = '';
            /*$accountdetails['bank_swift_code'] = '';
            $accountdetails['account_name'] = '';*/
            $accountdetails['bank_name'] = '';
            //$accountdetails['description'] = '';
          }

          if($userdetails->bank_details!='')
          {
            $id='personal_bank_info_form';
          }
          else
          {
            $id='personal_bank_info_form_photo';
          }
          ?>
          <div role="tabpanel" class="tab-pane active" id="bank">
            <div class="pro_content">
              <form class="form-horizontal" enctype="multipart/form-data" id="<?php echo $id;?>" name="personal_bank_info_form" method="post" action="">  
                <div class="form-group">
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label for="" class="col-sm-4 control-label"> Name</label>
                    <div class="col-sm-8">
                      <!-- <input type="text" class="form-control" id="" placeholder="Johndeo"> -->
                      <input type="text" class="form-control" id="username" name="username" placeholder="User Name" value="<?php echo $userdetails->kc_username;?>">
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label for="" class="col-sm-4 control-label">Bank Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Bank Name" value="<?php echo $accountdetails['bank_name'];?>">
                    </div>
                  </div>  
                </div>
                <div class="form-group">
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label for="" class="col-sm-4 control-label">IBAN Number</label>
                    <div class="col-sm-8">
                      <input type="text"  class="form-control" id="account_number" name="account_number" placeholder="Account Number" value="<?php echo $accountdetails['account_number'];?>">
                    </div>
                  </div>  
                  <!-- <div class="col-sm-6 col-md-6 col-xs-6">
                    <label for="" class="col-sm-4 control-label">IFSC Code</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="" placeholder="SBI00025">
                    </div>
                  </div> -->  
                </div>
                <!-- <div class="form-group">
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label for="" class="col-sm-4 control-label">Bank Address</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="" placeholder="testing">
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <label for="" class="col-sm-4 control-label">Passbook Photo</label>
                    <div class="col-sm-8">
                     <div class="clstiming_btn" id="upload_btn">
                      <span> Choose File</span>
                       <input multiple="" name="file" type="file">
                      </div>
                    </div>
                  </div>  
                </div> -->
                <div class="form-group mar-top-40">
                  <div class="col-md-6 col-sm-6 text-right">
                    <button type="submit" name="save_profile" value="Update" class="btn btnFrmSubmit ">Save</button>
                	</div>
                  <div class="col-md-6 col-sm-6 ">
                    <button type="reset" class="btn btnFrmSubmit">Cancel</button>
              	  </div>
              	</div>
              </form>
            </div>
          </div>
          <?php 
        }

        ?>
      </div>
    </div> 
  </div>
</section>
<?php $this->load->view('front/newfooter'); ?>
<!-- Profile page scripts start -->
<script>
  $("#personal_info_form").validate({
    rules:
    {
      uname:{required:true,lettersonly:true,minlength:3},
      fname:{required:true,lettersonly:true,minlength:3},
      lname:{required:true,lettersonly:true,minlength:3},
      kc_username:{required:true,},
      email:{required:true,email:true},
      phone:{required:true,number:true,minlength:10,maxlength:10,
      remote: {
                url: base_url+'home/sameMobileNumber',
                type: 'GET'
              },
    },
    postal_code:{required:true,number:true},
    //city:{required:true,},
    //country:{required:true,},


    },
    messages:
    {
      fname:{required:'First Name is required',minlength:'Atleast 3 characters required',lettersonly:'Alphabets is required',},
      lname:{required:'last name is required',minlength:'Atleast 3 characters required',lettersonly:'Alphabets is required',},
      kc_username:{required:'User name is required',},
      email:{required:'Email address is required',email:'Enter valid email'},
      phone:{required:'Mobile number is required',number:'Enter valid number format',remote:'This phone number already exists',maxlength:'Ten digit only allowed',minlength:'Ten digit only allowed'},
      postal_code:{required:'Postal code is required',number:'Enter valid number format'},
    }

  });
</script>
<!-- Profile page scripts End -->

<!-- Change password scripts start -->
<script>
  $("#change_form").validate({
    rules:
    {
      old:{required:true, remote: {
                          
                          url: base_url+'home/check_pw',
                          type: 'GET'
                      },},
      new:{required:true,minlength:'6',maxlength:'12',noSpace:true,},
      new_confirm:{required:true,maxlength:'12',equalTo:'#new',minlength:'6',},
    },
    messages:
    {
      old:{required:'Current password is required',remote:'Current password is wrong',},
      new:{required:'New password is required',minlength:'Minimum 6 character is required'},
      new_confirm:{required:'New confirm password is required',equalTo:'Password does not match',minlength:'Minimum 6 character is required'},
    }
  });

  jQuery.validator.addMethod("noSpace", function(value, element) { 
  return value.indexOf(" ") < 0 && value != ""; 
  }, "Space not allowed");
</script>
<!-- Change password scripts End -->  

<!-- Bank details page script start -->
<script>
  $("#personal_bank_info_form_photo").validate(
  {
    rules:
    {
      bank_name:{required:true,},
      account_name:{required:true,},
      account_number:{required:true,number:true,minlength:5},
      account_number_confirm:{required:true,number:true,equalTo:'#account_number'},
      bank_swift_code:{required:true,},
      description:{required:true,},
      passport:{required:true,},
    },
    messages:
    {
      bank_name:{required:'Bank name is required',},
      account_name:{required:'Account holder name is required',},
      account_number:{required:'Account number is required',number:'Enter valid number format',minlength:'Minimum 5 digit required'},
      account_number_confirm:{required:'Confirm account number is required',number:'Enter valid number format',equalTo:'Not match account number'},
      bank_swift_code:{required:'IFSC code is required',},
      description:{required:'Bank address is required',},
      passport:{required:'Passbook photo is required',},
    },
  });

  $("#personal_bank_info_form").validate(
  {
    rules:
    {
      bank_name:{required:true,},
      account_name:{required:true,},
      account_number:{required:true,number:true,minlength:5},
      account_number_confirm:{required:true,number:true,equalTo:'#account_number'},
      bank_swift_code:{required:true,},
      description:{required:true,},
    
    },
    messages:
    {
      bank_name:{required:'Bank name is required',},
      account_name:{required:'Account holder name is required',},
      account_number:{required:'Account number is required',number:'Enter valid number format',minlength:'Minimum 5 digit required'},
      account_number_confirm:{required:'Confirm account number is required',number:'Enter valid number format',equalTo:'Not match account number'},
      bank_swift_code:{required:'IFSC code is required',},
      description:{required:'Bank address is required',},
    },
  });
</script>
<!-- Bank details page script End -->