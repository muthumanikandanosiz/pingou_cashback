<?php
class Admin_model extends CI_Model
{
	// this is to login check 
	function logincheck()
	{	
		$this->db->connection_check();
		$admin_username = $this->input->post('username');
		$admin_password = $this->input->post('password');
		
		$this->db->where('admin_username',$admin_username);
		$this->db->where('admin_password',$admin_password);
		
		$query = $this->db->get('admin');
		if($query->num_rows==1)
		{
			$fetch 			= $query->row();
	        $admin_id 		= $fetch->admin_id;
	        $admin_username = $fetch->admin_username;
		 	/*$main_access 	= $fetch->main_access;
			$sub_access 	= $fetch->sub_access;*/
			$role_access 	= $fetch->permission;
			$datetimes  	= date('d:h:i');
			
			/*new code for session login time updated in admin table 12-7-16*/
			$data = array(		
				'login_time' => $datetimes);
			$this->db->where('admin_id',$admin_id);
			$update_qry = $this->db->update('admin',$data);
			/*end*/		

			$this->session->set_userdata('admin_id',$admin_id);
			$this->session->set_userdata('admin_username',$admin_username);
			$this->session->set_userdata('admin_email',$admin_email);
			/*$this->session->set_userdata('main_access',unserialize($main_access));
			$this->session->set_userdata('sub_access',unserialize($sub_access));*/
			$this->session->set_userdata('sess_time',$datetimes);
			$this->session->set_userdata('user_access',unserialize($role_access));
			return true;
		}
		return false;
	}

	
	//New code for user settings details 16-04-16//
	function getusersettings()
	{
		$this->db->connection_check();
		$this->db->where('setting_id','1');
		$query_admin = $this->db->get('tbl_usersettings');
		if($query_admin->num_rows >= 1) 
		{
			$row = $query_admin->row();
			return $query_admin->result();
		}
		else
		{
			return false;		
		}	
	}	
	//End//
	
	// get admin details..
	function getadmindetails()
	{
		$this->db->connection_check();
		$this->db->where('admin_id','1');

		$query_admin = $this->db->get('admin');
		if($query_admin->num_rows >= 1)
		{
			$row = $query_admin->row();
			return $query_admin->result();
		}
		else
		{
			return false;		
		}	
	}
	function get_admindetails()
	{
		$this->db->connection_check();
		$this->db->where('admin_id','1');
		$query_admin = $this->db->get('admin');
		if($query_admin->num_rows == 1)
		{
			return $query_admin->row();
		}else{
			return false;		
		}	
	}
	function check_cate($cate)
	{
		$this->db->connection_check();
		$this->db->where('category_name',$cate);
		$qry 	  = $this->db->get('categories');
		$numrows1 = $qry->num_rows();
		if($numrows1 == 0)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	function check_sub_cate($sub,$cate)
	{
		$this->db->connection_check();
		$this->db->where('sub_category_name',$sub);
		$this->db->where('cate_id',$cate);
		$qry 	  = $this->db->get('sub_categories');
		$numrows1 = $qry->num_rows();
		if($numrows1 == 0)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	// update admin settings..
	function updatesettings($logo,$site_logo,$site_favicon)
	{
		$this->db->connection_check();
		$admin_logo = $logo;
		$posted 	= $this->input->post('username');
		$this->session->set_userdata('admin_username',$posted);
		
		$cat_bonus_amt = $this->input->post('cat_bonus_amt');
		if(count($cat_bonus_amt) > 0)
		{
			foreach ($cat_bonus_amt as $ref_id => $bonus_amt) 
			{
				$up_data = array('category_bonus_amount' => $bonus_amt
				);
				$this->db->where('ref_id',$ref_id);
				$updations = $this->db->update('referral_settings',$up_data); 
			}
		}
		//print_r($_POST); exit;
		/*New code for session timing details 12-7-16*/
		$dates    = $this->input->post('days');
		$hours    = $this->input->post('hours');
		$minutes  = $this->input->post('minutes');
		
		if($dates < '10')
		{
			$dates 	 = "0".$dates;
		}
		if($hours < '10')
		{
			$hours 	 = "0".$hours;
		}
		if($minutes < '10') 
		{
			$minutes = "0".$minutes;
		}
		
		$new_ses_time_format = $dates.":".$hours.":".$minutes;
		//echo $new_ses_time_format; exit;
		/*End*/
		$data = array(
		'admin_username'=>$this->input->post('username'),
		'admin_email'=>$this->input->post('email'),
		/*'email_notify'=>$this->input->post('email_notify'),*/
		'admin_paypal'=>$this->input->post('paypal_email'),
		'paypal_mode'=>$this->input->post('paypal_mode'),
		'admin_logo'=>$admin_logo,
		'site_logo'=>$site_logo,
		'site_favicon'=>$site_favicon,
		'homepage_title'=>$this->input->post('homepage_title'),	
		//'referral_cashback'=>$this->input->post('referral_cashback'),
		'minimum_cashback'=>$this->input->post('minimum_cashback'),
		'site_name'=>$this->input->post('site_name'),
		'site_url'=>$this->input->post('site_url'),
		'admin_fb'=>$this->input->post('fb_url'),
		'admin_twitter'=>$this->input->post('twitter_url'),
		'admin_gplus'=>$this->input->post('gplus_url'),
		'admin_instagram'=>$this->input->post('admin_instagram'),
		'admin_pintrust'=>$this->input->post('admin_pintrust'),
		'contact_number'=>$this->input->post('contact_number'),
		'contact_info'=>$this->input->post('contact_info'),
		'address'=>$this->input->post('address'),
		'meta_title'=>$this->input->post('meta_title'),
		'meta_keyword'=>$this->input->post('meta_keyword'),
		'meta_description'=>$this->input->post('meta_description'),
		'site_mode'=>$this->input->post('site_mode'),
		'google_analytics'=>$this->input->post('google_analytics'),
		'google_key'=>$this->input->post('google_key'),
		'google_secret'=>$this->input->post('google_secret'),
		/*facebook App Id & secret key seetha -----*/
		'facebook_key'=>$this->input->post('facebook_key'),
		'facebook_secret'=>$this->input->post('facebook_secret'),
		'yahoo_key'=>$this->input->post('yahoo_key'),
		'yahoo_secret'=>$this->input->post('yahoo_secret'),
		/*-----*/
		/*Hotmail configuration settings 6-9-16*/
		'hotmail_key'=>$this->input->post('hotmail_key'),
		'hotmail_secret'=>$this->input->post('hotmail_secret'),
		/*End*/
		'enable_blog'=>$this->input->post('enable_blog'),
		'enable_shopping'=>$this->input->post('enable_shopping'),
		'enable_slider'=>$this->input->post('enable_slider'),
		'blog_url'=>$this->input->post('blog_url'),
		'unic_bonus'=>$this->input->post('benefit_bonus'),
		'log_content'=>$this->input->post('log_content'),

		/*New code for Category type enable and disable settings 3-5-16*/
		'cat_two_status' =>$this->input->post('cat_two_status'),
		'cat_three_status'=>$this->input->post('cat_three_status'),
		'cat_four_status'=>$this->input->post('cat_four_status'),
		'cat_five_status'=>$this->input->post('cat_five_status'),
		/*End*/
		/* Session time setting  */
		/*'ses_datetime'=>$this->input->post('ses_datetime')*/
		'ses_datetime'=> $new_ses_time_format,
		'coupon_expiry_date'=> date('Y-m-d h:m:s',strtotime($this->input->post('coupon_expiry_date'))),

		/*New code for zanox tracking and extra tracking parameter details 8-1-17*/
		'zanox_tracking' =>$this->input->post('zanox_tracking'),
		'zanox_extra_tracking'=>$this->input->post('zanox_extra_tracking'),
		'cityads_tracking' =>$this->input->post('cityads_tracking'),
		'cityads_extra_tracking'=>$this->input->post('cityads_extra_tracking'),
		'rakuten_tracking' =>$this->input->post('rakuten_tracking'),
		'rakuten_extra_tracking'=>$this->input->post('rakuten_extra_tracking'),
		'afilio_tracking' =>$this->input->post('afilio_tracking'),
		'afilio_extra_tracking'=>$this->input->post('afilio_extra_tracking'),
		'lomadee_tracking' =>$this->input->post('lomadee_tracking'),
		'lomadee_extra_tracking'=>$this->input->post('lomadee_extra_tracking'),
		/*End 8-1-17*/
		'remain_minimum_with_amt'=>$this->input->post('remain_minimum_with_amt')
		);
	

		$id = $this->input->post('admin_id');
		$this->db->where('admin_id',$id);
		$updation = $this->db->update('admin',$data);
		
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}
	
	}
	
	//  change password for main admin
	function update_password()
	{
		
		$this->db->connection_check();
		$old_password = $this->input->post('old_password');
		$new_password = $this->input->post('new_password');
		$id = $this->input->post('admin_id');
		$email_notify = $this->input->post('email_notify');
		
		$where = array('admin_password'=>$old_password,'admin_id'=>$id);
		$this->db->where($where);
		$query_admin = $this->db->get('admin');
		if($query_admin->num_rows >= 1) 
		{
			$data = array(
			'admin_password'=>$new_password,
			'email_notify'=>$email_notify
			);
			$this->db->where('admin_id',$id);	
			$this->db->update('admin',$data);
			return true;
		}    
		else 
		{     
			return false;
		}			
	}
	
	// get referral cashback %
	function get_referral_percent(){
		$this->db->connection_check();
		$result = $this->db->get_where('admin',array('admin_id'=>'1'))->row('referral_cashback');
		return $result;
	
	}
	//adding cms..
	function addcms($meta_image){
		$this->db->connection_check();
		$seo_url  = $this->admin_model->seoUrl($this->input->post('page_title'));
		$data = array(
		'cms_heading'   => $this->input->post('page_title'),
		'cms_metatitle' => $this->input->post('meta_title'),
		'cms_metakey'   => $this->input->post('meta_keyword'),
		'cms_metadesc'  => $this->input->post('meta_description'),
		'cms_content'   => $this->input->post('cms_content'),
		'cms_position'  => 'footer',	
		'cms_title' 	=> $seo_url,	
		'cms_status' 	=> $this->input->post('cms_status'),
		/*New filed 22-12-16*/
		'cms_urlslag'   => $this->input->post('url_slag'),
		'cms_meta_type' => $this->input->post('meta_type'),	
		'meta_sitename' => $this->input->post('meta_sitename'),	
		'cms_meta_url'  => $this->input->post('meta_url'),
		'cms_meta_image'=> $meta_image
		/*End 22-12-16*/
		);
		
		$this->db->insert('tbl_cms',$data);
		return true;
	}
	// get all cms
	function get_allcms()
	{
		$this->db->connection_check();
		$this->db->order_by('cms_id','desc');
		$cms_query = $this->db->get('tbl_cms');
		if($cms_query->num_rows > 0)
        {
            $row = $cms_query->row();
            return $cms_query->result();
        }
		else
		{
			return false;		
		}
	}
	
	// get particular cms
	function get_cmscontent($id){
		$this->db->connection_check();
		$this->db->where('cms_id',$id);        
        $query = $this->db->get('tbl_cms');
        if($query->num_rows >= 1)
		{
           $row = $query->row();			
            return $query->result();			
        }      
        return false;		
	}
	
	
	//update cms ..
	function updatecms($meta_image)
	{
		$this->db->connection_check();
		$seo_url  = $this->admin_model->seoUrl($this->input->post('page_title'));
		$data = array(
			'cms_heading' 	=> $this->input->post('page_title'),
			'cms_metatitle' => $this->input->post('meta_title'),
			'cms_metakey' 	=> $this->input->post('meta_keyword'),
			'cms_metadesc'  => $this->input->post('meta_description'),
			'cms_content'   => $this->input->post('cms_content'),
			'cms_position'  => 'footer',	
			'cms_title' 	=> $seo_url,		
			'cms_status' 	=>  $this->input->post('cms_status'),
			/*New code 22-12-16*/
			'cms_urlslag'   => $this->input->post('url_slag'),
			'cms_meta_type' => $this->input->post('meta_type'),	
			'meta_sitename' => $this->input->post('meta_sitename'),	
			'cms_meta_url'  => $this->input->post('meta_url'),
			'cms_meta_image'=> $meta_image
			/*New code 22-12-16*/
		);
		$id =  $this->input->post('cms_id');
		$this->db->where('cms_id',$id);
		$upd = $this->db->update('tbl_cms',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}	
	}
	
	
	// delete cms..
	function deletecms($id)
	{
		$this->db->connection_check();
		$this->db->delete('tbl_cms',array('cms_id' => $id));
		return true;
	
	}
	
	
	
	// get all faqs..
	function get_allfaqs(){
		$this->db->connection_check();
		$this->db->order_by('faq_id','desc');
		$allfaqs = $this->db->get('tbl_faq');
		if($allfaqs->num_rows > 0)
        {
            $row = $allfaqs->row();
            return $allfaqs->result();
        }
		else
		{
			return false;
		}
	}
	
	// add new faq..
	function addfaqs(){
		$this->db->connection_check();
		$data = array(
		'faq_qn' => $this->input->post('faq_qn'),
		'faq_ans' => $this->input->post('faq_ans'),
		'status' => '1'
		);
		
		$this->db->insert('tbl_faq',$data);
		return true;
	}
	// get particular faq
	function get_faqcontent($id){
		$this->db->connection_check();
		$this->db->where('faq_id',$id);
        $query = $this->db->get('tbl_faq');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
           return $query->result();
        }
        return false;
	}
	
	// update faq details..
	function updatefaq(){
		$this->db->connection_check();
		$data = array(
			'faq_qn' => $this->input->post('faq_qn'),
			'faq_ans' => $this->input->post('faq_ans'),
			'status' =>  $this->input->post('status')
		);
		$id =  $this->input->post('faq_id');
		$this->db->where('faq_id',$id);
		$upd = $this->db->update('tbl_faq',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}
	}
	
	// delete faq..
	function deletefaq($id)
	{
		$this->db->connection_check();
		$this->db->delete('tbl_faq',array('faq_id' => $id));
		return true;
	}
	
// view all users..
	function get_allusers($searchtext)
	{
		$this->db->connection_check();
		
		if($searchtext!='' && $searchtext !='De-Activated')
		{
			$arr_like = array('user_id' => $searchtext,'first_name' => $searchtext,'last_name' => $searchtext,'email' => $searchtext,'balance' => $searchtext,'contact_no' => $searchtext,'ifsc_code' => $searchtext,'random_code' => $searchtext,'refer' => $searchtext,'referral_category_type' => $searchtext,'referral_amt' => $searchtext,'app_login' => $searchtext,'bonus_benefit' => $searchtext,'newsletter_mail' => $searchtext);
		
			/*New code for search date format details 26-1-17*/
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 4) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$arr_like['date_added'] = $strnewdate;
			}

			$this->db->or_like($arr_like);
			/*End 26-1-17*/
		}
		if($searchtext == 'De-Activated')
		{
			$this->db->where('status','0');
		} 

		$this->db->where('admin_status','');
		$this->db->order_by('user_id','desc');
		$user_query = $this->db->get('tbl_users');
		if($user_query->num_rows > 0)
        {
            $row = $user_query->row();
            return $user_query->result();
        }
		else
		{
			return false;
		}
	}
	// view user details
	function view_user($userid)
	{
		$this->db->connection_check();
		$this->db->where('user_id',$userid);        
        $query = $this->db->get('tbl_users');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
            return $query->result();
        }
        return false;
	}	
	// update user status..
	function userupdate()
	{
		$this->db->connection_check();
	$data = array(
		'status' 	      => $this->input->post('status'),
		'user_reviews'    => $this->input->post('user_review'),
		'referral_category_type' => $this->input->post('ref_category'),
		'cashback_mail'   => $this->input->post('cashback_mail'),
		'withdraw_mail'   => $this->input->post('withdraw_mail'),
		'referral_mail'   => $this->input->post('referral_mail'),
		'newsletter_mail' => $this->input->post('newsletter_mail'),
		'support_tickets' => $this->input->post('support_tickets'),
		'acbalance_mail'  => $this->input->post('acbalance_mail')
	);
	
		$user_id = $this->input->post('user_id');
		$this->db->where('user_id',$user_id);
		$upd = $this->db->update('tbl_users',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}
	}
	
	// delete user details..
	function deleteuser($id)
	{
		$this->db->connection_check();
		$this->db->where('user_id',$id);        
        $query = $this->db->get('tbl_users');
		$email = $query->row('email');
		$data = array(
			'admin_status' => 'deleted',
			'status' =>  '0'
		);
		$this->db->where('user_id',$id);
		$upd = $this->db->update('tbl_users',$data);
		
		$this->db->delete('referrals',array('referral_email' => $email));            
		//$this->db->delete('tbl_users',array('user_id' => $id)); 
		return true;
	}
	
	// get country name
	function get_country($country)
	{
		$this->db->connection_check();	
		$this->db->where('id',$country);
		$res = $this->db->get('countries');
		if($res->num_rows > 0){
			return $res->row('name');
		}
		return false;	
	}	
	// view all categories..
	function premium_categories()
	{	
		$this->db->connection_check();
		$this->db->order_by('sort_order');
		
		 
		 $this->db->where('category_status',1);
		 	
		$result = $this->db->get('premium_categories');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	
	// view all categories..
	function categories(){
		$this->db->connection_check();
		$this->db->order_by('sort_order','desc');
		$result = $this->db->get('categories');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	// add premium  category
	function addpremiumcategory()
	{	
		$this->db->connection_check();
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('premium_categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$max_val = $get->sort_order;
		}
		$maxval = $max_val + 1;
		$seo_url  = $this->admin_model->seoUrl($this->input->post('category_name'));
		$data = array(
		'category_name'=>$this->input->post('category_name'),
		'meta_keyword'=>$this->input->post('meta_keyword'),
		'meta_description'=>$this->input->post('meta_description'),
		'sort_order'=>$maxval,
		'category_status'=>$this->input->post('category_status'),
		'category_url'=>$seo_url
		);
		
		$this->db->insert('premium_categories',$data);
		return true;
	}	
	
	// add new category
	function addcategory()
	{
		$this->db->connection_check();
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$max_val = $get->sort_order;
		}
		$maxval = $max_val + 1;
		$seo_url  = $this->admin_model->seoUrl($this->input->post('category_name'));
		//echo print_r($_POST); exit;
		$cat_image = $_FILES['categoryimg']['name'];

		/*New code for amazon s3 copy settings 26-12-16*/
		$amazon_s3_details = $this->admin_model->amazon_s3_settings();
		$copy_status       = $amazon_s3_details->copy_files_status;
		/*End*/

		if($cat_image!="") 
		{
			$new_random 	= mt_rand(0,99999);
			/*New code for image name field changes 3-10-16*/
			$info 	 	 	= new SplFileInfo($cat_image);
			$file_ex 	 	= $info->getExtension();
			$newfilename 	= str_replace('.'.$file_ex,'', $cat_image);
			$cat_image 		= $newfilename.$new_random.'.'.$file_ex;
			/*End 3-10-16*/

			//$cat_image = remove_space($new_random.$cat_image);
			$config['upload_path'] ='uploads/img';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['file_name']=$cat_image;
			

			//New code for AWS 24-9-16//
			if($copy_status == 1)
			{
				$filepath = 'uploads/img/'.$cat_image;
				$tmp 	  = $_FILES['categoryimg']['tmp_name'];
				$this->load->library('S3');
				$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
				$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
			}
			//End//

			$this->load->library('upload', $config);
			$this->upload->initialize($config);	
			if($cat_image!="" && (!$this->upload->do_upload('categoryimg')))
			{
				$cat_imageerror = $this->upload->display_errors();
			}
				if(isset($cat_imageerror))        
				{
					$this->session->set_flashdata('error',$cat_imageerror);
					redirect('adminsettings/addcategory','refresh');
				}
		}	 
		 
		$data = array(
		'category_name'=>$this->input->post('category_name'),
		'meta_keyword'=>$this->input->post('meta_keyword'),
		'meta_description'=>$this->input->post('meta_description'),
		'sort_order'=>$maxval,
		'category_status'=>$this->input->post('category_status'),
		'category_url'=>$seo_url,
		'category_img'=>$cat_image,
		'category_desc'=>$this->input->post('cat_description'),
		'ofertas_description'=>$this->input->post('ofertas_description')
		);
		//echo print_r($data); exit;
		$this->db->insert('categories',$data);
		return true;
	}	
	
	// edit category
	function get_category($category_id){
	$this->db->connection_check();
		$this->db->where('category_id',$category_id);
        $query = $this->db->get('categories');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
           return $query->result();
        }
        return false;	
	}
	
	function get_premium_category($category_id){
		$this->db->connection_check();
		$this->db->where('category_id',$category_id);
        $query = $this->db->get('premium_categories');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
           return $query->result();
        }
        return false;	
	}
	
	//update category
		function update_category($id)
		{
			
			//print_r($_POST);
			$this->db->connection_check();
			$seo_url  = $this->admin_model->seoUrl($this->input->post('category_name'));
			 
			/*New code for amazon s3 copy settings 26-12-16*/
			$amazon_s3_details = $this->admin_model->amazon_s3_settings();
			$copy_status       = $amazon_s3_details->copy_files_status;
			/*End*/

			$cat_image = $_FILES['categoryimgs']['name'];
			if($cat_image!="") 
			{
				$new_random 	= mt_rand(0,99999);
				/*New code for image name field changes 3-10-16*/
				$info 	 	 	= new SplFileInfo($cat_image);
				$file_ex 	 	= $info->getExtension();
				$newfilename 	= str_replace('.'.$file_ex,'', $cat_image);
				$cat_image 		= $newfilename.$new_random.'.'.$file_ex;
				/*End 3-10-16*/

				//$cat_image = remove_space($new_random.$cat_image);
				$config['upload_path'] ='uploads/img';
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['file_name']=$cat_image;
				
				//New code for AWS 24-9-16//
				if($copy_status == 1)
				{
					$filepath = 'uploads/img/'.$cat_image;
					$tmp 	  = $_FILES['categoryimgs']['tmp_name'];
					$this->load->library('S3');
					$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
				}
				//End//



				$this->load->library('upload', $config);
				$this->upload->initialize($config);	
				if($cat_image!="" && (!$this->upload->do_upload('categoryimgs')))
				{
					$cat_imageerror = $this->upload->display_errors();
				}
				if(isset($cat_imageerror))        
				{
					$this->session->set_flashdata('error',$cat_imageerror);
					redirect('adminsettings/editcategory/'.$id,'refresh');
				}
			}else
			{
				$cat_image = $this->input->post('hidden_img');
			}
			//echo $cat_image; exit;
			$data = array(
			'category_name'=>$this->input->post('category_name'),
			'meta_keyword'=>$this->input->post('meta_keyword'),
			'meta_description'=>$this->input->post('meta_description'),
			'category_desc'=>$this->input->post('cat_descriptions'),
			'category_img'=>$cat_image,
			'category_status'=>$this->input->post('category_status'),
			'category_url'=>$seo_url,
			'ofertas_description'=>$this->input->post('ofertas_description')
			);
			//echo "<pre>";print_r($data); exit;

			$id = $this->input->post('category_id');
			$this->db->where('category_id',$id);
			$upd = $this->db->update('categories',$data);
			if($upd)
			{
				return true;
			}
			else
			{
				return false;
			}
	}
	
	//update premium category
		function update_premium_category()
		{
		$this->db->connection_check();
		$seo_url  = $this->admin_model->seoUrl($this->input->post('category_name'));
			$data = array(
			'category_name'=>$this->input->post('category_name'),
			'meta_keyword'=>$this->input->post('meta_keyword'),
			'meta_description'=>$this->input->post('meta_description'),
			'category_status'=>$this->input->post('category_status'),
			'category_url'=>$seo_url
		);
		$id = $this->input->post('category_id');
		$this->db->where('category_id',$id);
		$upd = $this->db->update('premium_categories',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}
	}
	
	// delete category
	function deletecategory($id)
	{
		$this->db->connection_check();	
		// get order of category which is to be deleted.
		$start_order = $this->db->get_where('categories',array('category_id'=>$id))->row('sort_order');
		
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$end_order = $get->sort_order;
		}
		$this->db->delete('categories',array('category_id' => $id));
		$newval = $start_order;
		for($inc=$start_order; $inc<=$end_order;$inc++){
			$newval = $newval + 1;
			
			$data = array('sort_order'=>$inc);
			$this->db->where('sort_order',$newval);
			$this->db->update('categories',$data);
		}
		return true;
	}
	
	// delete category
	function deletepremiumcategory($id)
	{
		$this->db->connection_check();
	
		// get order of category which is to be deleted.
		$start_order = $this->db->get_where('premium_categories',array('category_id'=>$id))->row('sort_order');
		
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('premium_categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$end_order = $get->sort_order;
		}
		$this->db->delete('premium_categories',array('category_id' => $id));
		$newval = $start_order;
		for($inc=$start_order; $inc<=$end_order;$inc++){
			$newval = $newval + 1;
			
			$data = array('sort_order'=>$inc);
			$this->db->where('sort_order',$newval);
			$this->db->update('premium_categories',$data);
		}
		return true;
	}
	
	// view all affiliates
	function affiliates()
	{
		$this->db->connection_check();
		$this->db->order_by('affiliate_id','desc');
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0)
		{
			return $result->result();
		}
		return false;
	}
	
	// add new affiliates
	// add new affiliates
	function addaffiliate($logo, $ban,$sidebar_image=null,$cover_photo=null,$report_date)
	{
		$this->db->connection_check();
		$stcat = $this->input->post('categorys_list');
		
		//echo "<pre>"; print_r($_POST); exit;
		
		if($this->input->post('categorys_list'))
		{
			$store_categorys =implode(",",$this->input->post('categorys_list'));
		}
		else
		{
			$store_categorys='';
		}
		
		//$seo_url  = $this->admin_model->seoUrl($this->input->post('affiliate_name'));
		
		$data = array(
		'affiliate_name'=>$this->input->post('affiliate_name'),
		'affiliate_logo'=>$logo,
		
		/*'site_url' => $this->input->post('site_url'),*/
		'logo_url'=>$this->input->post('logo_url'),
		'affiliate_desc'=>$this->input->post('affiliate_desc'),
		'cashback_percentage'=>$this->input->post('cashback_percentage'),
		'meta_keyword'=>$this->input->post('meta_keyword'),
		'meta_description'=>$this->input->post('meta_description'),
		'featured'=>$this->input->post('featured'),
		'store_of_week'=>$this->input->post('store_of_week'),
		'affiliate_status'=>$this->input->post('affiliate_status'),
		'affiliate_cashback_type'=>$this->input->post('affiliate_cashback_type'),
		'retailer_ban_url'=>$this->input->post('retailer_ban_url'),	
		
		'how_to_get_this_offer'=>$this->input->post('how_to_get_this_offer'),				
		
		'terms_and_conditions'=>$this->input->post('terms_and_conditions'),		
		
		'sidebar_image_url'=>$this->input->post('sidebar_image_url'),
						
		'coupon_image'=>$ban,
		
		'sidebar_image'=>$sidebar_image,
		/*'affiliate_url'=>$seo_url,*/
		'affiliate_url'=>$this->input->post('url_slug'),

		'store_categorys'=>$store_categorys,
		//Pilaventhiran 03/05/2016 START
		'report_date'=>$report_date,
		'notify_desk'=>$this->input->post('notify_desk'),
		//Pilaventhiran 13/05/2016 START
		'notify_mobile'=>$this->input->post('notify_mobile'),
		//Pilaventhiran 13/05/2016 END
		'old_cashback'=>$this->input->post('old_cashback'),
		'redir_notify'=>$this->input->post('redir_notify'),
		'name_extra_param'=>$this->input->post('name_extra_param'),
		'content_extra_param'=>$this->input->post('content_extra_param'),
		//Pilaventhiran 13/05/2016 START
		'content_extra_param_android'=> $this->input->post('content_extra_param_android'),
		//Pilaventhiran 13/05/2016 END
		'cashback_percent_android'=>$this->input->post('cashback_percent_android'),
		'cashback_content_android'=>$this->input->post('cashback_content_android'),
		'cover_photo'=>$cover_photo,
		/*New related field added 1-6-16.*/
		'related_details' =>$this->input->post('related_details'),
		/*End*/
		/*New code for Turn and go to shop button extra tracking parameter 30-7-16*/
		'tracking_param' =>$this->input->post('tracking_param'),
		/*End*/
		//Pilaventhiran 03/05/2016 END
		/*New code for retailers page extra details 14-10-16*/
		'extra_tracking_param' =>$this->input->post('ex_tracking_param'),
		'coupon_track_param' =>$this->input->post('coupon_tracking_param'),
		'coupon_ex_track_param' =>$this->input->post('coupon_ex_tracking_param'),
		/*End 14-10-16*/
		/*New code for Add a API Coupons program Id 9-2-17*/
		'zanox_pgm_id'   =>$this->input->post('zanox_pgm_id'),
		'cityads_pgm_id' =>$this->input->post('cityads_pgm_id'),
		'lomadee_pgm_id' =>$this->input->post('lamadee_pgm_id'),
		'rakuten_pgm_id' =>$this->input->post('rakuten_pgm_id'),
		'afilio_pgm_id'  =>$this->input->post('afilio_pgm_id'),
		/*End 9-2-17*/
		'zanox_offer_provider'  =>$this->input->post('zanox_offer_provider'),
		'miss_cash_amt_type' =>$this->input->post('miss_cash_amt_type')
		);
		$this->db->insert('affiliates',$data);
		$store_id = $this->db->insert_id();
		$store_category_count	=	count($stcat);
		if($stcat!="")
		{
			foreach($stcat as $maincat)
			{
				$var="size_".$maincat;
				$subcat = $this->input->post($var);
				if($subcat)
				{
					foreach($subcat as $subcategory)
					{
						$data = array(
						'category_id'=>$maincat,
						'sub_category_id'=>$subcategory,
						'store_id'=>$store_id
						);
						$this->db->insert('tbl_store_sub_cate',$data);
					}
				}
			}
		}
		return true;
	}
	
	// view affiliate
	function get_affiliate($id)
	{
		$this->db->connection_check();	
		$this->db->where('affiliate_id',$id);
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->result();		
		}	
	}
	
	
	// update affiliate
	function updateaffiliate($logo,$banimg,$sidebar_image,$cover_photo)
	{
		$this->db->connection_check();
		$affiliate_id = $this->input->post('affiliate_id');
		
		//echo "<pre>";print_r($_POST); exit;
		
		if($this->input->post('categorys_list'))
		{	
		$stcat = $this->input->post('categorys_list');	
			$store_categorys =implode(",",$this->input->post('categorys_list'));
		}
		else
		{
			$stcat = '';	
			$store_categorys='';
		}
		
		//$seo_url  = $this->admin_model->seoUrl($this->input->post('affiliate_name'));
		
		$this->db->delete('tbl_store_sub_cate',array('store_id' => $affiliate_id));
		
		$data = array(
			'affiliate_name'=>$this->input->post('affiliate_name'),
			'affiliate_logo'=>$logo,
			'logo_url'=>$this->input->post('logo_url'),
			
			//'site_url' => $this->input->post('site_url'),
			'meta_keyword'=>$this->input->post('meta_keyword'),
			'meta_description'=>$this->input->post('meta_description'),
			'affiliate_desc'=>$this->input->post('affiliate_desc'),
			'cashback_percentage'=>$this->input->post('cashback_percentage'),
			'featured'=>$this->input->post('featured'),
			'store_of_week'=>$this->input->post('store_of_week'),
			'affiliate_status'=>$this->input->post('affiliate_status'),
			'affiliate_cashback_type'=>$this->input->post('affiliate_cashback_type'),
			
			'retailer_ban_url'=>$this->input->post('retailer_ban_url'),
			'how_to_get_this_offer'=>$this->input->post('how_to_get_this_offer'),				
			
			'terms_and_conditions'=>$this->input->post('terms_and_conditions'),		
			
			'sidebar_image_url'=>$this->input->post('sidebar_image_url'),		
			
			/*'affiliate_url'=>$seo_url,*/

			'affiliate_url'=>$this->input->post('url_slug'),

			'coupon_image'=>$banimg,
			
			'sidebar_image'=>$sidebar_image,
			'store_categorys'=>$store_categorys,
			//Pilaventhiran 03/05/2016 START
			'notify_desk'=>$this->input->post('notify_desk'),
			//Pilaventhiran 13/05/2016 START
			'notify_mobile'=>$this->input->post('notify_mobile'),
			//Pilaventhiran 13/05/2016 END
			'old_cashback'=>$this->input->post('old_cashback'),
			'redir_notify'=>$this->input->post('redir_notify'),
			'name_extra_param'=>$this->input->post('name_extra_param'),
			'content_extra_param'=>$this->input->post('content_extra_param'),
			'content_extra_param_android'=>$this->input->post('content_extra_param_android'),
			'cashback_percent_android'=>$this->input->post('cashback_percent_android'),
			'cashback_content_android'=>$this->input->post('cashback_content_android'),
			'cover_photo'=>$cover_photo,
			'report_date'=>$this->input->post('report_date'),
			/*new related details 1-6-16.*/
			'related_details' =>$this->input->post('related_details'),
			/*End*/
			//Pilaventhiran 03/05/2016 END	
			/*New code for Turn and go to shop button extra tracking parameter 30-7-16*/
			'tracking_param' =>$this->input->post('tracking_param'),
			/*End*/
			/*New code for retailers page extra details 14-10-16*/
			'extra_tracking_param'  =>$this->input->post('ex_tracking_param'),
			'coupon_track_param'    =>$this->input->post('coupon_tracking_param'),
			'coupon_ex_track_param' =>$this->input->post('coupon_ex_tracking_param'),
			/*End 14-10-16*/
			/*New code for Add a API Coupons program Id 9-2-17*/
			'zanox_pgm_id'   =>$this->input->post('zanox_pgm_id'),
			'cityads_pgm_id' =>$this->input->post('cityads_pgm_id'),
			'lomadee_pgm_id' =>$this->input->post('lamadee_pgm_id'),
			'rakuten_pgm_id' =>$this->input->post('rakuten_pgm_id'),
			'afilio_pgm_id'  =>$this->input->post('afilio_pgm_id'),
			/*End 9-2-17*/
			'zanox_offer_provider'  =>$this->input->post('zanox_offer_provider'),
			'miss_cash_amt_type' =>$this->input->post('miss_cash_amt_type')
		);
		
		$this->db->where('affiliate_id',$affiliate_id);
		$updation = $this->db->update('affiliates',$data);
		$store_id= $affiliate_id;
		if($updation!="" )
		{
			if($stcat!='')
			{
				foreach($stcat as $maincat)
				{
					$var="size_".$maincat;
					$subcat = $this->input->post($var);
					if($subcat)
					{
						foreach($subcat as $subcategory)
						{
							$data = array(
							'category_id'=>$maincat,
							'sub_category_id'=>$subcategory,
							'store_id'=>$store_id
							);
							$this->db->insert('tbl_store_sub_cate',$data);
						}
					}
				}
			}
		
			return true;
		}
		else 
		{ 
			return false;   
		}
	//	return true;
	}
	
	// delete affiliate
	function deleteaffiliate($id)
	{
		$this->db->connection_check();
		$this->db->delete('affiliates',array('affiliate_id' => $id));
		$this->db->delete('tbl_store_sub_cate',array('store_id' => $id));
		$this->db->delete('click_history',array('affiliate_id' => $id));
		return true;	
	}
	
	//view all banners
	function banners(){
	$this->db->connection_check();
		$banners = $this->db->get('tbl_banners');
		if($banners->num_rows > 0){
			return $banners->result();		
		}
		return false;
	}
	
	// add banner
	function addbanner($img){
	$this->db->connection_check();
		$data = array(
			'banner_heading'=>$this->input->post('banner_name'),
			'banner_image'=>$img,
			'banner_url'=>$this->input->post('banner_url'),
			'banner_position'=>$this->input->post('banner_position'),
			'banner_status'=>$this->input->post('banner_status')
		);
	
		$this->db->insert('tbl_banners',$data);
		return true;
	}
	
	//edit banner
	function get_banner($id){
	$this->db->connection_check();
		$this->db->where('banner_id',$id);
		$banner = $this->db->get('tbl_banners');
		if($banner->num_rows > 0){
			return $banner->result();
		}
		return false;
	}
	
	// update banner
	function updatebanner($img){
	$this->db->connection_check();
	$banner_id = $this->input->post('banner_id');
	$data = array(
		'banner_heading'=>$this->input->post('banner_name'),
		'banner_image'=>$img,
		'banner_url'=>$this->input->post('banner_url'),
		'banner_position'=>$this->input->post('banner_position'),
		'banner_status'=>$this->input->post('banner_status')		
	);
	
	$this->db->where('banner_id',$banner_id);
	$update = $this->db->update('tbl_banners',$data);
		if($update!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}
	}
	
	// delete banner
	function deletebanner($delete){ 
		$this->db->connection_check();
		$this->db->delete('tbl_banners',array('banner_id' => $delete));
		return true;
	}
	
	// view all subscribers
	function subscribers(){	
		$this->db->connection_check();
		$this->db->order_by("subscriber_id", "desc");
		$all = $this->db->get('subscribers');
		if($all->num_rows > 0) {
			return $all->result();
		}
		return false;
	}
	// delete subscriber
	function deletesubscriber($id){
		$this->db->connection_check();
		$this->db->delete('subscribers',array('subscriber_id' => $id));
		return true;
	}
	
	
	function send_mail(){
	$this->db->connection_check();
	// get admin email
		$admin_email = $this->db->get_where('admin', array(
			'admin_id'=>'1'
		))->row('admin_email');
		//echo $admin_email; echo "<br>";
		$to_users = $this->input->post('to');
		
		if($to_users=="users")
		{
			$this->db->where('newsletter_mail',1);
			$users = $this->db->get('tbl_users');
			$results = $users->result();
			$emails='';
			foreach($results as $get){
				$emails .= $get->email.',';
			}
			$emails  = rtrim($emails,',');
			$semail  = explode(",", $emails);
 			foreach($semail as $newemail)
			{
				$user_id = $this->db->get_where('tbl_users',array('email'=>$newemail))->row('user_id');		
				 
				$message = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">
					<br>
					<br>'.$this->input->post('message').'
					<br>
					<br>
					Você está recebendo esta beacuse você é um membro Pingou . Você pode apenas <a href='.base_url().'un-subscribe/subscribers>un-subscrever</a> se você quiser.	
					</span>';
				
				/*New code for email unsubscribe settings 26-5-16.*/

				$this->load->library('email');
				$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8'
				);  	 

				$this->email->set_newline("\r\n");
				$this->email->initialize($config);
				$this->email->from($admin_email);
				$this->email->to($newemail);
				//$this->email->bcc($emails);
				$this->email->subject($this->input->post('subject'));
				$this->email->message($message);
				$this->email->send();
        		$this->email->print_debugger();
			} 				
			 
		}
		else if($to_users=="subscribers")
		{
			$this->db->where('subscriber_status',1);
			$subscribers = $this->db->get('subscribers');
			$results = $subscribers->result();
			$emails='';
			foreach($results as $get){
				$emails .= $get->subscriber_email.',';
			}
			$emails = rtrim($emails,',');
			$semail  = explode(",", $emails);
			foreach($semail as $newemail)
			{
				
				$user_id = $this->db->get_where('subscribers',array('subscriber_email'=>$newemail))->row('subscriber_id');		
				$message = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">
					<br>
					<br>'.$this->input->post('message').'
					<br>
					<br>
					Você está recebendo esta beacuse você é um membro Pingou . Você pode apenas <a href='.base_url().'un-subscribe/subscribers>un-subscrever</a> se você quiser.
					</span>';
			
				/*New code for email unsubscribe settings 26-5-16.*/

				$this->load->library('email');
				$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8'
				);  	 

				$this->email->set_newline("\r\n");
				$this->email->initialize($config);
				$this->email->from($admin_email);
				$this->email->to($newemail);
			//	$this->email->bcc($emails);
				$this->email->subject($this->input->post('subject'));
				$this->email->message($message);

				$this->email->send();
        		$this->email->print_debugger();
			

			} 
			
		}
		
			//echo $emails; exit;
		/*
		$config['protocal']  = 'smtp';
		$config['smtp_host'] = 'ssl://smtp.googlemail.com';
		$config['smtp_port'] = '465';
		$config['smtp_user'] = 'padmanathan@osiztechnologies.com';
		$config['smtp_pass'] = '';
		$config['mailtype']  = 'html';		
		$config['charset']   = 'iso-8859-1';
		$config['wordwrap']  = TRUE;
		$config['crlf'] = '\r\n';      //should be "\r\n"
		$config['newline'] = '\r\n';   //should be "\r\n"
		*/

		/*$this->load->library('email');

			  $config = Array(

				'mailtype'  => 'html',
				'charset'   => 'utf-8'
			);  	 

		$this->email->set_newline("\r\n");
		$this->email->initialize($config);
		$this->email->from($admin_email);
		$this->email->to("");
		$this->email->bcc($emails);
		$this->email->subject($this->input->post('subject'));
		$this->email->message($message);*/
		/*
		if($this->email->send())
        { 
           return true;
        }
		else{
            show_error($this->email->print_debugger());
			 return false;
        }
        */
        return true;
	}
	
	//get email template content..
	function get_email_template($id){
		$this->db->connection_check();
		$this->db->where('mail_id',$id);
		$mail_template = $this->db->get('tbl_mailtemplates');
		if($mail_template->num_rows > 0){
			return $mail_template->result();
		}
		return false;	
	}
	
	// update template
	function update_email_template(){
		$this->db->connection_check();
		$mail_id = $this->input->post('mail_id');
			
		$data = array(
			'email_subject'=>$this->input->post('email_subject'),
			'email_template'=>$this->input->post('email_template')			
		);
		
		$this->db->where('mail_id',$mail_id);
		$update = $this->db->update('tbl_mailtemplates',$data);
		if($update!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}	
	}
	
	// all referrals
	function referrals(){
		$this->db->connection_check();
		$this->db->group_by('user_id');
		$this->db->order_by("user_id", "desc");
		$referrals = $this->db->get('referrals');
		if($referrals->num_rows > 0)
        {
           return $referrals->result();
        }
		else
		{
			return false;
		}
	}
	
	// total count for referral
	function referral_count($email){
		$this->db->connection_check();
		$this->db->where('user_email',$email);
		$count = $this->db->get('referrals');
		return $count->num_rows();
	}	
	
	// delete referral by user id
	function deletereferral($user_id){
		$this->db->connection_check();
		$this->db->delete('referrals',array('user_id'=>$user_id));
		return true;
	}
	
	// view all coupons..
	function coupons($searchtext=null,$coupon_name)
	{
		$this->db->connection_check();
		$this->db->order_by("coupon_id", "desc");
		if($searchtext!='')
		{
			$arr_like = array('coupon_id' => $searchtext,'offer_name' => $searchtext,'title' => $searchtext,'code' => $searchtext,'type' => $searchtext,'Tracking' => $searchtext,'extra_tracking_param' => $searchtext);
			
			
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 4) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$arr_like['expiry_date'] = $strnewdate;
			}
			$this->db->or_like($arr_like);
		}

		if($coupon_name)
		{
			$this->db->where('offer_name',$coupon_name);
		}
		$this->db->where('coupon_status','completed');
		$result  = $this->db->get('coupons'); 
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	//bulk coupon action..
	function bulkcoupon($bulkcoupon)
	{
	  	$this->db->connection_check();
	    // delete all the previously added coupons of the coupon type (vcommission/icubewire)
		// $coupon_type = $this->input->post('coupon_type');
		$coupon_type = '';
		//$this->db->delete('coupons',array('coupon_type'=>$coupon_type));
		//$this->db->truncate('coupons');
		/*$this->db->delete('coupons');*/
		$this->load->library('CSVReader');
		$main_url = 'uploads/coupon/'.$bulkcoupon;
	 	$result =   $this->csvreader->parse_file($main_url);
		if(count($result)!=0)
		{
			foreach($result as $res)
			{
				
				$strdate = date('m/d/Y',strtotime($res['start_date(m/d/Year)']));
				
				$exp = date('m/d/Y',strtotime($res['Expiry']));
				$offer_url = $res['Offer Page  ( Ctrl C to copy )'];
				$Offer_Name = $this->db->escape_str($res['Offer Name']);
				$Title = $this->db->escape_str($res['Title']);
				$Description = $this->db->escape_str($res['Description']);
				$Type = $res['Type'];
				$Code = $res['Code'];
				$start_date = $strdate;
				$Expiry = $exp;
				
				$Coupon_featured = $res['Coupon_featured'];
				/*$Featured = $res['Featured ( 0 or 1)'];
				$Exclusive = $res['Exclusive (0 or 1)'];*/
				$traking_param = $res['Tracking Extra parameter'];	
				$results = $this->db->query("INSERT INTO `coupons` (`offer_name`, `title`, `description`, `type`, `code`, `offer_page`, `start_date`, `expiry_date`, `coupon_options`, `Tracking`) VALUES ('$Offer_Name', '$Title', '$Description', '$Type', '$Code', '$offer_url', '$start_date', '$Expiry', '$Coupon_featured', '$traking_param');");
			}
		}
		return true;
	}
	
	// add new coupon..
	function addcoupon()
	{
		$this->db->connection_check();
		$start_date  = $this->input->post('start_date');
		$start_date  = date('Y-m-d',strtotime($start_date));
		$expiry_date = $this->input->post('expiry_date');
	
		/*New code for expiry date 10-10-16*/
		if($expiry_date == '')
		{
			$expiry_date = $this->db->query("SELECT * from `admin` where admin_id=1")->row('coupon_expiry_date');
		}
		/*end 10-10-16*/		
			
		$expiry_date = date('Y-m-d',strtotime($expiry_date));
		$url_type 	 = $this->input->post('url_type');

		if($url_type == 2)
		{
			$url_link_type = 'Standard';
		}
		else if($url_type == 3)
		{
			$url_link_type = 'Deeplink';
		}
		else
		{
			$url_link_type = '';
		}

		$type = $this->input->post('type');
		if($type == 1)
		{
			$type = 'Promotion';
		}
		else
		{
 			$type = 'Coupon';
		}

		$data = array(
			'offer_name' 	=>$this->input->post('offer_name'),
			'title'		 	=>$this->input->post('title'),
			'description'	=>$this->input->post('description'),
			'type'			=>$type,
			'code'		 	=>$this->input->post('code'),
			'offer_page' 	=>$this->input->post('offer_page'),
			'start_date' 	=>$start_date,
			'expiry_date'	=>$expiry_date,
			'Tracking'   	=>$this->input->post('Tracking'),
			'coupon_options'=>$this->input->post('coupon_options'),
			'cashback_description'=>$this->input->post('cashback_description'),
			/*New code for link details 15-10-16*/
			'url_link_type' =>$url_link_type,
			'deeplink_url'  =>$this->input->post('deeplink_url'),
			/*end 15-10-16*/
			'extra_tracking_param'=>$this->input->post('extra_tracking'),
			'coupon_status' 	  => 'completed'
		);
		$this->db->insert('coupons',$data);
		return true;	
	}
	
	// view coupon..	
	function editcoupon($coupon_id)
	{
		$this->db->connection_check();
		$this->db->where('coupon_id',$coupon_id);
		$coupons = $this->db->get('coupons');
		if($coupons->num_rows > 0){
			return $coupons->result();
		}
		return false;
	}
	
	// update coupon details..
	function updatecoupon() 
	{
		$this->db->connection_check();

		$start_date  = $this->input->post('start_date');
		$start_date  = date('Y-m-d',strtotime($start_date));
		$expiry_date = $this->input->post('expiry_date');
		$coupon_id 	 = $this->input->post('coupon_id');
		
		/*New code for expiry date 10-10-16*/
		if($expiry_date == '')
		{
			$expiry_date = $this->db->query("SELECT * from `admin` where admin_id=1")->row('coupon_expiry_date');
		}
		/*end 10-10-16*/		
			
		$expiry_date = date('Y-m-d',strtotime($expiry_date));
		$url_type 	 = $this->input->post('url_type');

		if($url_type == 2)
		{
			$url_link_type = 'Standard';
		}
		else if($url_type == 3)
		{
			$url_link_type = 'Deeplink';
		}
		else
		{
			$url_link_type = 'Paste';
		}

		$type = $this->input->post('type');
		if($type == 1)
		{
			$type = 'Promotion';
		}
		else
		{
 			$type = 'Coupon';
		}	


		$data = array(
			'offer_name' =>$this->input->post('offer_name'),
			'expiry_date'=>$expiry_date,
			'start_date' =>$start_date,
			'title'      =>$this->input->post('title'),
			'description'=>$this->input->post('description'),
			'type' 		 =>$type,
			'code' 		 =>$this->input->post('code'),
			'cashback_description'=>$this->input->post('cashback_description'),
			/*New code for link details 15-10-16*/
			'url_link_type' =>$url_link_type,
			'deeplink_url'  =>$this->input->post('deeplink_url'),
			/*end 15-10-16*/
			'offer_page'    =>$this->input->post('offer_page'),
			'coupon_options'=>$this->input->post('coupon_options'),
			'Tracking'      =>$this->input->post('Tracking'),
			'extra_tracking_param'=>$this->input->post('extra_tracking')
		);
		$this->db->where('coupon_id',$coupon_id);
		$updation = $this->db->update('coupons',$data);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;
		}
	}		
	
	// delete coupon..
	function deletecoupon($delete_id){
		$this->db->connection_check();
		$this->db->delete('coupons',array('coupon_id'=>$delete_id));
		return true;	
	}	
	
	// view all shopping coupons..
	function shoppingcoupons($store_name)
	{
		$this->db->connection_check(); 
		if($store_name)	
	 	{
	 		$select 	   = $this->db->query("SELECT * from affiliates where affiliate_url='".$store_name."'")->row();
	 		$affiliate_name  = $select->affiliate_name;
	 		//$selqry="SELECT * FROM shopping_coupons  WHERE expiry_date >='".date('Y-m-d')."' AND store_name='".$affiliate_id."' order by shoppingcoupon_id desc";
	 		$selqry="SELECT * FROM coupons WHERE offer_name='".$affiliate_name."'";
	 	}
	 	else
	 	{
			$selqry="SELECT * FROM shopping_coupons  WHERE expiry_date >='".date('Y-m-d')."' order by shoppingcoupon_id desc";
	 	}
	 	
	 	
	 	$result=$this->db->query("$selqry"); 
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	function exp_shoppingcoupons($store_name)
	{
		$this->db->connection_check();
 		
 		if($store_name)	
	 	{
	 		$select 	   = $this->db->query("SELECT * from affiliates where affiliate_url='".$store_name."'")->row();
	 		$affiliate_name  = $select->affiliate_name;
	 		//$selqry="SELECT * FROM shopping_coupons  WHERE expiry_date >='".date('Y-m-d')."' AND store_name='".$affiliate_id."' order by shoppingcoupon_id desc";
	 		$selqry="SELECT * FROM coupons WHERE offer_name='".$affiliate_name."'";
	 	}
	 	else
	 	{
	 		$selqry="SELECT * FROM shopping_coupons  WHERE expiry_date <='".date('Y-m-d')."' order by shoppingcoupon_id desc";   	 
		}
		//$result = $this->db->get('shopping_coupons');
	 	$result=$this->db->query("$selqry"); 
		//$result = $this->db->get('shopping_coupons');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	// add new shopping coupon..
	
	// edit shopping coupon
	function edit_shoppingcoupon($coupon_id){
	$this->db->connection_check();
	$this->db->where('shoppingcoupon_id',$coupon_id);
		$coupons = $this->db->get('shopping_coupons');
		if($coupons->num_rows > 0){
			return $coupons->result();
		}
		return false;	
	}
	
	// update shopping coupon details..	
	
	// delete shopping coupon..
	function delete_shoppingcoupon($delete_id){
	$this->db->connection_check();
		$this->db->delete('shopping_coupons',array('shoppingcoupon_id'=>$delete_id));
		return true;
	}	
	// view user email	
	function user_email($user_id){
		$this->db->connection_check();
		$result = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('email');
		return $result;	
	}
	
	// view store name
	function view_store($affiliate_id){
		$this->db->connection_check();
		$result = $this->db->get_where('affiliates',array('affiliate_id'=>$affiliate_id))->row('affiliate_name');
		return $result;
	}
	
	// get promo id
	function view_promo($coupon_id){
		$this->db->connection_check();
		$result = $this->db->get_where('coupons',array('coupon_id'=>$coupon_id))->row('promo_id');
		return $result;
	}
	
	// get cashback % from store..
	function get_fromstore($affiliate_id){
		$this->db->connection_check();
		$result = $this->db->get_where('affiliates',array('affiliate_id'=>$affiliate_id))->row('cashback_percentage');
		return $result;	
	}
	function get_fromstore1($affiliate_id){
		$this->db->connection_check();
		$result = $this->db->get_where('affiliates',array('affiliate_url'=>$affiliate_id))->row('cashback_percentage');
		return $result;	
	}
	// view all cashback details
	function cashback($searchtext)
	{
		$this->db->connection_check();
		
		if($searchtext !='')
		{
			$this->db->like('affiliate_id',$retailer_name);
		}



		$this->db->order_by('cashback_id','desc');
		$result = $this->db->get('cashback');
		if($result->num_rows > 0){
	
			return $result->result();
		}
		return false;
	}
	
	// get cashback details
	function cashback_details($id){
		$this->db->connection_check();
		$this->db->where('cashback_id',$id);
		$result = $this->db->get('cashback');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	// update cashback status..
	function updatecashback($id){
		$this->db->connection_check();

		$userid = $this->input->post('user_id');
		 
		$cashback_status  = $this->db->query("SELECT * from `cashback` where `cashback_id`='$id' AND user_id='$userid'")->row('status');
		$report_update_id = $this->db->query("SELECT * from `cashback` where `cashback_id`='$id' AND user_id='$userid'")->row('report_update_id');
		$txn_id           = $this->db->query("SELECT * from `cashback` where `cashback_id`='$id' AND user_id='$userid'")->row('txn_id');
		


			if($this->input->post('current_status') == 'Approved')
			{
				$current_status = 'Completed';
				$user_bale 		= $this->view_balance($this->input->post('user_id'));

				$newbalnce 		= $user_bale + $this->input->post('transation_amount');
				
				$data = array(
				'balance' => $newbalnce);
				$this->db->where('user_id',$this->input->post('user_id'));

				$update_qry = $this->db->update('tbl_users',$data);
			}
	 
			if($this->input->post('current_status') == 'Canceled')
			{
				
				$cashback_amount  = $this->input->post('transation_amount');
				$user 			  = $this->view_user($userid);

				foreach($user as $single)
				{
					$balance    = $single->balance;
					$user_email = $single->email;
					$first_name = $single->first_name;
					$last_name  = $single->last_name;

					if($first_name == '' && $last_name == '')
					{
						$ex_name   = explode('@', $user_email);
						$user_name = $ex_name[0]; 
					}
					else
					{
						$user_name = $first_name.' '.$last_name;
					}
				}

				$current_status = 'Canceled';
				$user_bale 		= $this->view_balance($userid);
				$newbalnce 		= $user_bale - $cashback_amount;
					
				$data = array(		
				'balance' => $newbalnce);
				$this->db->where('user_id',$userid);
				$update_qry = $this->db->update('tbl_users',$data);

				if($txn_id)
				{
					$data2 = array('transation_status'=>'Canceled');
					$this->db->where('trans_id',$txn_id);
					$this->db->update('transation_details',$data2);
				}

				/* Cancel cashback mail notifications */
				$this->db->where('admin_id',1);
				$admin_det = $this->db->get('admin');
				if($admin_det->num_rows >0) 
				{    
					$admin 		 = $admin_det->row();
					$admin_email = $admin->admin_email;
					$site_name   = $admin->site_name;
					$admin_no    = $admin->contact_number;
					$site_logo   = $admin->site_logo;
				}

				$date =date('Y-m-d');

				if($single->cashback_mail == 1)
				{
					$this->db->where('mail_id',11);
					$mail_template = $this->db->get('tbl_mailtemplates');
					if($mail_template->num_rows >0) 
					{
					    
					   $fetch = $mail_template->row();
					   $subject = $fetch->email_subject;
					   $templete = $fetch->email_template;
					   $url = base_url().'my_earnings/';
					   $unsuburl	 = base_url().'un-subscribe/cashback/'.$userid;
					   $myaccount    = base_url().'minha-conta';
					   
						$this->load->library('email');
						
						$config = Array(
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
						);
						
						$sub_data = array(
							'###SITENAME###'=>$site_name
						);
						$subject_new = strtr($subject,$sub_data);
						
						// $this->email->initialize($config);
						 $this->email->set_newline("\r\n");
						   $this->email->initialize($config);
						   $this->email->from($admin_email,$site_name.'!');
						   $this->email->to($user_email);
						   $this->email->subject($subject_new);
					   
						$data = array(
							'###NAME###'=>$user_name,
							'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
							'###SITENAME###'=>$site_name,
							'###ADMINNO###'=>$admin_no,
							'###DATE###'=>$date,
							'###AMOUNT###'=>str_replace('.', ',', bcdiv($cashback_amount,1,2)),
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
					    );
					   
					   $content_pop=strtr($templete,$data);
					   $this->email->message($content_pop);
					   $this->email->send();  
					}
				} 
				/*End Cancel cashback mail notifications */


				$this->db->where('cashback_id',$id);
				$cashbacks 	   = $this->db->get('cashback');
				$cashback_data = $cashbacks->row();
				
				if($cashback_data->referral!=0)
				{
					$this->db->where('trans_id',$cashback_data->txn_id);
					$txn 	     = $this->db->get('transation_details');
					$txn_detail  = $txn->row();
					$new_txn_ids = $cashback_data->new_txn_id;

					if($txn_detail)
					{
						$txn_id 	 	   = $txn_detail->trans_id;
						$ref_user_id 	   = $txn_detail->user_id;
						$transation_amount = $txn_detail->transation_amount;
						$refer_user 	   = $this->view_user($ref_user_id);
						
						if($refer_user)
						{
							foreach($refer_user as $single)
							{
								$referral_balance = $single->balance;
								$user_email 	  = $single->email;
								$first_name 	  = $single->first_name;
								$last_name 		  = $single->last_name;

								if($first_name == '' && $last_name == '')
								{
									$ex_name   = explode('@', $user_email);
									$user_name = $ex_name[0]; 
								}
								else
								{
									$user_name = $first_name.' '.$last_name;
								}
							}
							 
							$data = array('transation_status'=>'Canceled','transation_reason'=>'Referal Payment');
							$this->db->where('trans_id',$txn_id);
							$this->db->update('transation_details',$data);
							
							/* mail for Canceled referral cashback amt mail notifications */
							if($single->referral_mail == 1)
							{
								$this->db->where('mail_id',12);
								$mail_template = $this->db->get('tbl_mailtemplates');
								if($mail_template->num_rows >0) 
								{
								   	$fetch 	   = $mail_template->row();
								   	$subject   = $fetch->email_subject;
								   	$templete  = $fetch->email_template;
								   	$url 	   = base_url().'my_earnings/';
								   	$unsuburl  = base_url().'un-subscribe/referral/'.$ref_user_id;
					   			   	$myaccount = base_url().'minha-conta';
								   
									$this->load->library('email');
									
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);

									$sub_data = array(
										'###SITENAME###'=>$site_name
									);

									$subject_new = strtr($subject,$sub_data);
									$this->email->set_newline("\r\n");
									$this->email->initialize($config);
									$this->email->from($admin_email,$site_name.'!');
									$this->email->to($user_email);
									$this->email->subject($subject_new);
								   
									$data = array(
										'###NAME###'=>$user_name,
										'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amount,1,2)),
										'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
										'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
									);
								   
								   $content_pop=strtr($templete,$data);
								   $this->email->message($content_pop);
								   $this->email->send();  
								}
							}	
							/* mail for Canceled referral cashback amt mail notifications */	 

							$data = array('transation_status'=>'Canceled','transation_reason'=>'Referral Cashback amount'); /*,'transaction_date'=>$date*/
							$this->db->where('new_txn_id',$new_txn_ids);
							$this->db->where('transation_status','Approved');
							$this->db->update('transation_details',$data);
						}
					}
				}
			}

			if($this->input->post('current_status') == 'Pending')
			{
				


				$transation_amt = $this->input->post('transation_amount');	
				$userid         = $this->input->post('user_id');
				$current_status = 'Pending';
				$user_bale 		= $this->view_balance($userid);
				
				/*if($cashback_status  != 'Canceled')
				{
					$newbalnce 		= $user_bale - $transation_amt;
				}*/
				
				$data = array(		
				'balance' => $newbalnce);
				$this->db->where('user_id',$userid);
				$update_qry = $this->db->update('tbl_users',$data);
			
				/*new code for Pending cashback Mail for transaction user (REFERRED) 6-4-17*/
				$user_detail = $this->view_user($userid);
				if($user_detail)
				{
					foreach($user_detail as $user_detail_single)
					{
						$referral_balance = $user_detail_single->balance;
						$user_email 	  = $user_detail_single->email;
						$accbalance 	  = $user_detail_single->cashback_mail;
						$first_name   	  = $user_detail_single->first_name;
						$last_name 	  	  = $user_detail_single->last_name;								

						if($first_name == '' && $last_name == '')
						{
							$ex_name   = explode('@', $user_email);
							$user_name = $ex_name[0]; 
						}
						else
						{
							$user_name = $first_name.' '.$last_name;
						}
					}
				}

				$this->db->where('admin_id',1);
				$admin_det = $this->db->get('admin');
				if($admin_det->num_rows >0) 
				{    
					$admin 		 = $admin_det->row();
					$admin_email = $admin->admin_email;
					$site_name 	 = $admin->site_name;
					$admin_no 	 = $admin->contact_number;
					$site_logo   = $admin->site_logo;
				}

				$date = date('Y-m-d');

				if($accbalance == 1)
				{
					$this->db->where('mail_id',10);
					$mail_template = $this->db->get('tbl_mailtemplates');
					
					if($mail_template->num_rows >0) 
					{
					    $fetch     = $mail_template->row();
					    $subject   = $fetch->email_subject;
					    $templete  = $fetch->email_template;
					    $unsuburl  = base_url().'un-subscribe/cashback/'.$userid;
					    $myaccount = base_url().'minha-conta';
					   
						$this->load->library('email');
						
						$config = Array(
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
						);
						
						$sub_data = array(
							'###SITENAME###'=>$site_name
						);
						$subject_new = strtr($subject,$sub_data);
						
						$this->email->set_newline("\r\n");
						$this->email->initialize($config);
						$this->email->from($admin_email,$site_name.'!');
						$this->email->to($user_email);
						$this->email->subject($subject_new);
					   
						$data = array(
							'###NAME###'=>$user_name,
							'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
							'###SITENAME###'=>$site_name,
							'###ADMINNO###'=>$admin_no,
							'###DATE###'=>$date,
							'###AMOUNT###'=>str_replace('.', ',', $transation_amt),
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
					    );
					   
					    $content_pop=strtr($templete,$data);
					   	$this->email->message($content_pop);
					   	$this->email->send();  
					}
				}
				/*End 6-4-17*/

				/*New code for pending referral mail for (REFER) User 6-4-17*/
				$check_ref    = $this->check_ref_user($userid);

				if($check_ref > 0)		
				{
					$ref_id  	  = $check_ref;
					$ref_user_bal = $this->view_balance($check_ref);
					$return  	  = $this->check_active_user($ref_id);

					if($cashback_status  == 'Approved')
					{
						$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id' AND `transation_reason`='Referral Cashback amount' AND `ref_user_tracking_id`='$userid' AND `transation_status`='Approved'")->row();	
					}
					if($cashback_status  == 'Canceled')
					{
						$ref_trans_details = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id' AND `transation_reason`='Referral Cashback amount' AND `ref_user_tracking_id`='$userid' AND `transation_status`='Canceled'")->row();
					}

					$transation_amount = $ref_trans_details->transation_amount;

					if($return)
					{
						foreach($return as $newreturn)
						{
							$referral_balance   = $newreturn->balance; 
							$user_referral_mail = $newreturn->referral_mail;
							$ref_user_email     = $newreturn->email;
							$ref_first_name   	= $newreturn->first_name;
							$ref_last_name 	  	= $newreturn->last_name;								

							if($ref_first_name == '' && $ref_last_name == '')
							{
								$ex_name       = explode('@', $ref_user_email);
								$ref_user_name = $ex_name[0]; 
							}
							else
							{
								$ref_user_name = $ref_first_name.' '.$ref_last_name;
							}
						}

						$this->db->where('user_id',$ref_id);
						$this->db->update('tbl_users',array('balance'=>$referral_balance-$transation_amount));
						
						$data = array('transation_status'=>'Pending'); //'transaction_date'=>$date,
						$this->db->where('transation_reason','Referral Cashback amount');
						$this->db->where('report_update_id',$report_update_id);
						$this->db->where('ref_user_tracking_id',$userid);
						$this->db->where('transation_status','Approved');
						$this->db->update('transation_details',$data);	

						if($user_referral_mail == 1)
						{
							$this->db->where('mail_id',20);
							$mail_template = $this->db->get('tbl_mailtemplates');
							if($mail_template->num_rows >0) 
							{
								$fetch     = $mail_template->row();
								$subject   = $fetch->email_subject;
								$templete  = $fetch->email_template;
								$url 	   = base_url().'my_earnings/';
								$unsuburls = base_url().'un-subscribe/referral/'.$ref_id;
						   		$myaccount = base_url().'minha-conta';
								
								$this->load->library('email');

								$config    = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
								);
										
								$sub_data = array(
								'###SITENAME###'=>$site_name
								);
								
								$subject_new = strtr($subject,$sub_data);
								// $this->email->initialize($config);
								$this->email->set_newline("\r\n");
								$this->email->initialize($config);
								$this->email->from($admin_email,$site_name.'!');
								$this->email->to($ref_user_email);
								$this->email->subject($subject_new);
								$datas = array(
								'###NAME###'=>$ref_user_name,
								'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
								'###SITENAME###'=>$site_name,
								'###ADMINNO###'=>$admin_no,
								'###DATE###'=>date('y-m-d'),
								'###AMOUNT###'=>str_replace('.', ',', $transation_amount),
								'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
								'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
								);
								$content_pop=strtr($templete,$datas);
								$this->email->message($content_pop);
								$this->email->send();  
							}
						}
					}
				}
				/*End 6-4-17*/
			}
		 
			
			$data = array(
				'status'=>$current_status
			);
			$this->db->where('cashback_id',$id);
			$updation = $this->db->update('cashback',$data);

		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;
		}
	}
	
	// delete cashback details..
	function deletecashback($id){

		$this->db->connection_check();
		$cb_r  = $this->db->get_where('cashback',array('cashback_id'=>$id))->row();   
		 
		if($cb_r)
		{
            $txn_id  = $cb_r->txn_id;
        	$amount  = $cb_r->cashback_amount;
            $user_id = $cb_r->user_id;
        }
		if($cb_r->status == "Completed")
		{
		 	/*New code for update a user balance details 27-5-16.*/
			$total_amt 	   = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('balance');
			$remain_amount = ($total_amt - $amount);  
			
			$data = array(		
				'balance' => $remain_amount);
				
			$this->db->where('user_id',$user_id);
			$update_qry = $this->db->update('tbl_users',$data);
			/*End 27-5-16.*/
		 }	
		

		$this->db->delete('cashback',array('cashback_id'=>$id));
		$this->db->delete('transation_details',array('trans_id'=>$txn_id));
		return true;
	}

	//New code for delete multiple cashback records 6/4/16//

	function delete_multi_cashbacks()
	{
		 
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		foreach($sort_order as $key=>$val)
		{
			$id = $key;		
			$this->db->delete('cashback',array('cashback_id' => $id));			
		}
		return true;
	}
	//End//

	 
	// view balance..
	function view_balance($user_id){
		$this->db->connection_check();
		$balace = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('balance');
		return $balace;
	}
	// withdraw ..
	function withdraw(){
		$this->db->connection_check();
	$this->db->order_by('withdraw_id','desc');
		$result = $this->db->get('withdraw');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;	
	}

	//New code for Process status for multiple Withdraw records 6/4/16//

	function process_multi_withdraw()
	{
		 
		$this->db->connection_check();
		$this->load->library('email');
		$sort_order = $this->input->post('chkbox');
		//print_r($sort_order);
		//exit;
		foreach($sort_order as $key=>$val)
		{
			$id = $key;	
			
			$data = array(		
			'status' => 'Processing');
			$this->db->where('withdraw_id',$id);
			$update_qry = $this->db->update('withdraw',$data);				
		
			/*New code for withdraw processed mail response for users start 26-8-16*/
			
			$name 			 = $this->db->query("select * from admin")->row();
			//$subject 		 = "Your Withdraw Ticket Reply";
			$admin_emailid 	 = $name->admin_email;
			$site_logo 		 = $name->site_logo;
			$site_name  	 = $name->site_name;
			$contact_number  = $name->contact_number;
			$DADOS_BANCARIOS = "<a href='".base_url().">DADOS_BANCARIOS</a>";
			$current_msg 	 = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Só para avisar que já estamos processando o seu resgate. Já já vai pingar dinheiro na sua conta, mas a gente avisa por email assim que o pagamento for realizado.</span>';

			if($update_qry)
			{
				$mail_temp 	  = $this->db->query("select * from tbl_mailtemplates where mail_id='15'")->row();
				$fe_cont 	  = $mail_temp->email_template;	
				$subject 	  = $mail_temp->email_subject;
				$servername   = base_url();
				$nows 		  = date('Y-m-d');	
				
				$statuss      = "Processing";
				
				$User_details = $this->admin_model->view_user($val);
				$us_email 	  = $User_details[0]->email;
				$list 	  	  = array($us_email);

				$config = Array(
					'mailtype'  => 'html',
					'charset'   => 'utf-8',
					);
				
				$this->email->initialize($config);
				$this->email->set_newline("\r\n");
				$this->email->from($admin_emailid,$site_name.'!');
				$this->email->to($list);
				$this->email->subject($subject);
				
				$gd_api=array(
				
					'###ADMINNO###'=>$contact_number,
					'###EMAIL###'=>$us_email,
					'###DATE###'=>$nows,
					'###MESSAGE###'=>$current_msg,
					'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
					'###SITENAME###' =>$site_name,
					'###WITHDRAW_STATUS###'=>$statuss
					);

				$gd_message=strtr($fe_cont,$gd_api);
				 
				$this->email->message($gd_message);
				$this->email->send();
				$this->email->print_debugger();	
			}
			/*End 26-8-16*/
		}
		return true;
	}
	//End//

	//New code for Completed status for withdraw records 6/4/16//

	function complete_multi_withdraw()
	{
		 
		$this->db->connection_check();
		$this->load->library('email');
		$sort_order = $this->input->post('chkbox');
		foreach($sort_order as $key=>$val)
		{
			$id = $key;	
            $date = date('Y-m-d'); //SATz
			$data = array(		
			'status' => 'Completed',
			'closing_date'=>$date
			);                       //SATz
			$this->db->where('withdraw_id',$id);
			$update_qry = $this->db->update('withdraw',$data);

			/*New code for withdraw process completed mail response for users start 26-8-16*/
			
			$name 			 = $this->db->query("select * from admin")->row();
			//$subject 		 = "Your Withdraw Ticket Reply";
			$admin_emailid 	 = $name->admin_email;
			$site_logo 		 = $name->site_logo;
			$site_name  	 = $name->site_name;
			$contact_number  = $name->contact_number;
			$DADOS_BANCARIOS = "<a href='".base_url().">DADOS_BANCARIOS</a>";
			
			$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> Seu resgate foi realizado com sucesso! Acabou de pingar R$ '.str_replace('.', ',', $requested_amount).' na sua conta bancária. ”
			where '.$requested_amount.' is the amount of the withdraw realized</span>';

			if($update_qry)
			{
				$mail_temp 	  = $this->db->query("select * from tbl_mailtemplates where mail_id='15'")->row();
				$fe_cont 	  = $mail_temp->email_template;	
				$subject 	  = $mail_temp->email_subject;
				$servername   = base_url();
				$nows 		  = date('Y-m-d');	
				
				$statuss      = "Completed";
				
				$User_details = $this->admin_model->view_user($val);
				$us_email 	  = $User_details[0]->email;
				$list 	  	  = array($us_email);

				$config = Array(
					'mailtype'  => 'html',
					'charset'   => 'utf-8',
					);
				
				$this->email->initialize($config);
				$this->email->set_newline("\r\n");
				$this->email->from($admin_emailid,$site_name.'!');
				$this->email->to($list);
				$this->email->subject($subject);
				
				$gd_api=array(
				
					'###ADMINNO###'=>$contact_number,
					'###EMAIL###'=>$us_email,
					'###DATE###'=>$nows,
					'###MESSAGE###'=>$current_msg,
					'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
					'###SITENAME###' =>$site_name,
					'###WITHDRAW_STATUS###'=>$statuss
					);

				$gd_message=strtr($fe_cont,$gd_api);
				 
				$this->email->message($gd_message);
				$this->email->send();
				$this->email->print_debugger();	
			}
			/*End 26-8-16*/		
		}
		return true;
	}
	//End//

	//New code for Cancel status for multiple Withdraw records 6/4/16//


function cancel_multi_withdraw()
	{
		 
		$this->db->connection_check();
		$this->load->library('email');
		$sort_order = $this->input->post('chkbox');
		
		//SATz 29 04 2016 --> 07 05 2016
 		foreach($sort_order as $key=>$val)
		{
			$id = $key;   
			$user_id=$val;

            $withdraw_id_amount    = $this->get_requested_amount($id);     //Get withdraw or added amount
            $user_id_amount 	   = $this->get_user_amount($user_id);         //Get Original amount
            $cancell_update_amount = $user_id_amount + $withdraw_id_amount ;

            $data=array('balance' => $cancell_update_amount);
            $this->db->where('user_id',$user_id);
		    $updation = $this->db->update('tbl_users',$data);        //Tbl_user update


            $date = date('Y-m-d');
			$data = array(		
			'closing_date'=>$date,
			'status' => 'Cancelled'

			);
			$this->db->where('withdraw_id',$id);
			$update_qry = $this->db->update('withdraw',$data);			//withdraw status update

			/*New code for withdraw process completed mail response for users start 26-8-16*/
			
			$name 			 = $this->db->query("select * from admin")->row();
			//$subject 		 = "Your Withdraw Ticket Reply";
			$admin_emailid 	 = $name->admin_email;
			$site_logo 		 = $name->site_logo;
			$site_name  	 = $name->site_name;
			$contact_number  = $name->contact_number;
			$DADOS_BANCARIOS = "<a href='".base_url()."minha-conta'>DADOS_BANCARIOS</a>";
			
			$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Ôpa, tentamos fazer o seu pagamento mas o banco não aceitou a operação porquê os dados estavam errados. O valor do seu resgate foi creditado novamente na sua conta. Pedimos que atualize os '.$DADOS_BANCARIOS.'  e qualquer problema entre em contato com a gente no '.$contact_number.'.</span>';

			if($update_qry)
			{
				$mail_temp 	  = $this->db->query("select * from tbl_mailtemplates where mail_id='15'")->row();
				$fe_cont 	  = $mail_temp->email_template;	
				$subject   	  = $mail_temp->email_subject;
				$servername   = base_url();
				$nows 		  = date('Y-m-d');	
				
				$statuss      = "Cancelled";
				
				$User_details = $this->admin_model->view_user($val);
				$us_email 	  = $User_details[0]->email;
				$list 	  	  = array($us_email);

				$config = Array(
					'mailtype'  => 'html',
					'charset'   => 'utf-8',
					);
				
				$this->email->initialize($config);
				$this->email->set_newline("\r\n");
				$this->email->from($admin_emailid,$site_name.'!');
				$this->email->to($list);
				$this->email->subject($subject);
				
				$gd_api=array(
				
					'###ADMINNO###'=>$contact_number,
					'###EMAIL###'=>$us_email,
					'###DATE###'=>$nows,
					'###MESSAGE###'=>$current_msg,
					'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
					'###SITENAME###' =>$site_name,
					'###WITHDRAW_STATUS###'=>$statuss
					);

				$gd_message=strtr($fe_cont,$gd_api);
				 
				$this->email->message($gd_message);
				$this->email->send();
				$this->email->print_debugger();	
			}
			/*End 26-8-16*/	
		}
		return true;
	}
	//End//


	
	// view withdraw details..
	function editwithdraw($id){
		$this->db->connection_check();
		$this->db->where('withdraw_id',$id);
		$result = $this->db->get('withdraw');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}	

//SATz 28 04 2016 
	function get_requested_amount($id){
		$this->db->connection_check();
		$this->db->where('withdraw_id',$id);
		$result = $this->db->get('withdraw');
	    return $result->row("requested_amount");
	}

function get_user_amount($user_id){
	    $id=$user_id;
		$this->db->connection_check();
		$this->db->where('user_id',$id);
		$result = $this->db->get('tbl_users');
	    return $result->row("balance");
	}
	
	// update withdraw status..
	function updatewithdraw($id)
	{
		$this->db->connection_check();
	    $current_status = $this->input->post('current_status');
	    $user_id  		  = $this->input->post('user_id');
	    $requested_amount = $this->get_requested_amount($id);
       	if($current_status == 'Cancelled')
       	{
        	$user_id  		  = $this->input->post('user_id');
            $user_bal 		  = $this->get_user_amount($user_id);
		    $requested_amount = $this->get_requested_amount($id); //Fetch from withdraw requested_amount
            $update_bal 	  = $user_bal+$requested_amount;
            
            $data=array(
            'balance' => $update_bal
            );
            //print_r($data);exit;
			$this->db->where('user_id',$user_id);
			$updation = $this->db->update('tbl_users',$data);
	    }

		//SATz 28 04 2016 
		
		//Pilaventhiran 04/05/2016 START
		// Processing , Cancelled, Completed
           
		$name 			  = $this->db->query("select * from admin")->row();
		//$subject 		  = "Your Withdraw Ticket Reply";
		$admin_emailid 	  = $name->admin_email;
		$site_logo 		  = $name->site_logo;
		$site_name  	  = $name->site_name;
		$contact_number   = $name->contact_number;
		$DADOS_BANCARIOS  = "<a href='".base_url()."minha-conta'>DADOS_BANCARIOS</a>";
		$User_details     = $this->admin_model->view_user($user_id);
		$firstname 		  = $User_details[0]->first_name;
		$lastname 		  = $User_details[0]->last_name;
		$osiz_with_status = $User_details[0]->withdraw_mail;
		/*new code for username details 5-1-17*/
		if($firstname == '' && $lastname == '')
		{
			$ex_name  = explode('@', $User_details[0]->email);
			$username = $ex_name[0]; 
		}
		else
		{
			$username = $firstname;
		}	
		/*End 5-1-17*/
		 
		switch($current_status)
		{
			case 'Processing':
				
				$with_status = '<a href='.base_url().'resgate>Processando</a>';
				$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">
				Só para avisar que já estamos processando o seu resgate. Já já vai pingar dinheiro na sua conta, mas a gente avisa por email assim que o pagamento for realizado.</span>';
			
			break;
			
			case 'Cancelled':

				$with_status = '<a href='.base_url().'resgate>Cancelado</a>';
				$contato     = "<a href=".base_url()."contato>Contato</a>";
				$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">
				Ôpa, tentamos fazer o seu pagamento mas o banco não aceitou a operação porquê os dados estavam errados. O valor do seu resgate foi creditado novamente na sua conta. Pedimos que atualize os '.$DADOS_BANCARIOS.'  e qualquer problema entre em '.$contato.' com a gente.</span>';
			
			break;
			
			case 'Completed':
				
				$with_status = '<a href='.base_url().'resgate>Pagamento realizado</a>';
				$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">
				Seu resgate foi realizado com sucesso! Acabou de pingar R$ '.str_replace('.', ',', $requested_amount).' na sua conta bancária. ”
				</span>'; //where '.$requested_amount.' is the amount of the withdraw realized
			
			break;	
		}

		$date = date('Y-m-d');
		if($current_status=='Completed' || $current_status=='Cancelled')
		{
			$data = array(
				'closing_date'=>$date,
				'status'=>$current_status
			);			
		}                                          
		else 
		{
			$data = array(
			'status'=>$current_status
			);
		}

		$this->db->where('withdraw_id',$id);
		$updation = $this->db->update('withdraw',$data);

		if($updation)
		{
			if($current_status!='Requested')
			{
				if($osiz_with_status == 1)
				{			
					$ulink 	    = base_url().'un-subscribe/withdraw/'.$user_id;
					$mail_temp  = $this->db->query("select * from tbl_mailtemplates where mail_id='15'")->row();
					$fe_cont    = $mail_temp->email_template;
					$subject    = $mail_temp->email_subject;
					$servername = base_url();
					$nows 		= date('Y-m-d');	
					$this->load->library('email');
			
					$gd_api=array(
						'###ADMINNO###'=>$contact_number,
						'###EMAIL###'=>$username,
						'###DATE###'=>$nows,
						'###MESSAGE###'=>$current_msg,
						'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
						'###SITENAME###' =>$site_name,
						'###WITHDRAW_STATUS###'=>$with_status,
						'###ULINK###'=>$ulink
						);
							   
					$gd_message = strtr($fe_cont,$gd_api);
					$config     = Array(
					 'mailtype' => 'html',
					  'charset' => 'utf-8',
					  );
					
					$us_email 	  = $User_details[0]->email;
					//$list = array($us_email);
					$this->email->initialize($config);
					$this->email->set_newline("\r\n");
					$this->email->from($admin_emailid,$site_name.'!');
					$this->email->to($us_email);
					$this->email->subject($subject);
					$this->email->message($gd_message);
					//print_r($gd_message); exit;
					$this->email->send();
					$this->email->print_debugger();
				}	
			}
			return true;
		}
		else
		{
			return false;
		}

		//Pilaventhiran 04/05/2016 END
		
	}
	
	// deleting withdraw..
	function deletewithdraw($id){
		$this->db->connection_check();
		$this->db->delete('withdraw',array('withdraw_id'=>$id));
		return true;
	}
	
	// to check the user is referral..
	function check_refer($email){
		$this->db->connection_check();
		$this->db->where('referral_email',$email);
		$refer = $this->db->get('referrals');
		// print_r($refer->num_rows());
		return $refer->num_rows();
	}
	
	// to fetch the code and amount using shoppingcoupon_id..
	function get_allcoupons($id){
		$this->db->connection_check();
		$this->db->where('shoppingcoupon_id',$id);
		$results = $this->db->get('shoppingcodes');
		if($results->num_rows > 0){
			return $results->result();
		}
		return false;
	}
	
	//  removes code on editing premium coupon..
	function delete_shopcoupon($ids){
	$this->db->connection_check();
		$this->db->delete('shoppingcodes',array('shoppingcode_id'=>$ids));
		return true;
	}
	
	// change  category order..
	function change_cate_order($old_order,$new_order){
		$this->db->connection_check();
		// fetching category id using sort_order id..
		$old_category = $this->db->get_where('categories',array('sort_order'=>$old_order))->row('category_id');
		$new_category = $this->db->get_where('categories',array('sort_order'=>$new_order))->row('category_id');
		
		$data1 = array('sort_order'=>$new_order);
		$this->db->where('category_id',$old_category);
		$this->db->update('categories',$data1);
		
		$data2 = array('sort_order'=>$old_order);
		$this->db->where('category_id',$new_category);
		$updation = $this->db->update('categories',$data2);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;
		}		
	}
	
	// change  premium category order..
	function change_premium_cate_order($old_order,$new_order)
	{	
		$this->db->connection_check();
		// fetching category id using sort_order id..
		$old_category = $this->db->get_where('premium_categories',array('sort_order'=>$old_order))->row('category_id');
		$new_category = $this->db->get_where('premium_categories',array('sort_order'=>$new_order))->row('category_id');
		
		$data1 = array('sort_order'=>$new_order);
		$this->db->where('category_id',$old_category);
		$this->db->update('premium_categories',$data1);
		
		$data2 = array('sort_order'=>$old_order);
		$this->db->where('category_id',$new_category);
		$updation = $this->db->update('premium_categories',$data2);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;
		}		
	}
	
	// get maximum category order..
	function get_maxcategory(){
		$this->db->connection_check();
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('categories');
		return $get_max->result();
	}
	
	// get overall count for shopping coupon code..
	function get_countshopcoupon($shopping_id){
		$this->db->connection_check();
		//$this->db->where('shoppingcoupon_id',$shopping_id);
		
		$result = $this->db->query("SELECT LENGTH(coupon_code) - LENGTH(REPLACE(coupon_code, ',', '')) as counting FROM `shopping_coupons` where shoppingcoupon_id=".$shopping_id);
		//$result = $this->db->get('shoppingcodes');
		if($result->num_rows > 0){
			return $result->row()->counting;
		}
		return false;
	}
	
	// get all referral emails..
	function all_referrals($email){
		$this->db->connection_check();
	$this->db->order_by('referral_id','desc');
		$this->db->where('user_email',$email);
		$referrals = $this->db->get('referrals');
		if($referrals->num_rows > 0){
			return $referrals->result();
		}
		return false;
	}
	
	// get all click history..
	function click_history($userid=null)
	{
		$this->db->connection_check();
		if($userid!='')
		{
			if(is_numeric($userid))
			{
				$this->db->where('user_id',$userid);
			}
			else
			{	
				$this->db->where('store_name',$userid);
			}
		}
		$this->db->order_by('click_id','desc');
		$all = $this->db->get('click_history');
		if($all->num_rows > 0){
			return $all->result();
		}
		return false;
	}
	
	// delete click history..
	function deletehistory($id){
		$this->db->connection_check();
		$this->db->delete('click_history',array('click_id'=>$id));
		return true;
	}
	
	//delete cashback
	function deletecashbackdetails($id){
		$this->db->connection_check();
		$this->db->delete('category_cashback',array('cbid'=>$id));
		return true;
	}
	
	//nathan
	
	// view all categories..
	function sub_categories($cateid){
		$this->db->connection_check();
		$this->db->order_by('sort_order');
		$this->db->where('cate_id',$cateid);
		$result = $this->db->get('sub_categories');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	// view all premium categories..
	function premium_sub_categories($cateid){
		$this->db->connection_check();
		$this->db->order_by('sort_order');
		$this->db->where('cate_id',$cateid);
		$result = $this->db->get('premium_sub_categories');
		if($result->num_rows > 0){
			return $result->result();	
		}
			return false;
	}
	// view cat name from id..
	function get_category_name($cateid){
		$this->db->connection_check();
		$this->db->where('category_id',$cateid);
		$result = $this->db->get('categories');
		if($result->num_rows > 0){
			return $result->row('category_name');	
		}
			return false;
	}
	// view cat name from id..
	function get_premium_category_name($cateid){
		$this->db->connection_check();
		$this->db->where('category_id',$cateid);
		$result = $this->db->get('premium_categories');
		if($result->num_rows > 0){
			return $result->row('category_name');	
		}
			return false;
	}
	// view sub cat name from id..
	function get_sub_category_name($sub_cateid){
		$this->db->connection_check();
		$this->db->where('sun_category_id',$sub_cateid);
		$result = $this->db->get('sub_categories');
		if($result->num_rows > 0){
			return $result->result('sub_category_name');	
		}
			return false;
	}
	
	// add new category
	function addsubcategory(){
		$this->db->connection_check();
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('sub_categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$max_val = $get->sort_order;
		}
		$maxval = $max_val + 1;
		$seo_url  = $this->admin_model->seoUrl($this->input->post('sub_category_name'));
		
		$data = array(
		'sub_category_name'=>$this->input->post('sub_category_name'),
		'cate_id'=>$this->input->post('cate_id'),
		'meta_keyword'=>$this->input->post('meta_keyword'),
		'meta_description'=>$this->input->post('meta_description'),
		'sort_order'=>1,
		'category_status'=>$this->input->post('category_status'),
		'sub_category_url'=> $seo_url
		);
		
		$this->db->insert('sub_categories',$data);
		return true;
	}		
	
	// add new premium category
	function addpremiumsubcategory(){
	/*print_r($this->input->post());
	exit;*/
		$this->db->connection_check();
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('premium_sub_categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$max_val = $get->sort_order;
		}
		$maxval = $max_val + 1;
		$seo_url  = $this->admin_model->seoUrl($this->input->post('sub_category_name'));
		
		$data = array(
		'sub_category_name'=>$this->input->post('sub_category_name'),
		'cate_id'=>$this->input->post('cate_id'),
		'meta_keyword'=>$this->input->post('meta_keyword'),
		'meta_description'=>$this->input->post('meta_description'),
		'sort_order'=>1,
		'category_status'=>$this->input->post('category_status'),
		'sub_category_url'=> $seo_url
		);
		
		$this->db->insert('premium_sub_categories',$data);
		return true;
	}		
	
	
	// edit category
	function get_subcategory($category_id){
		$this->db->connection_check();
		$this->db->where('sun_category_id',$category_id);
        $query = $this->db->get('sub_categories');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
           return $query->result();
        }
        return false;	
	}
	
	// edit category
	function get_premium_subcategory($category_id){
		$this->db->connection_check();
		$this->db->where('sun_category_id',$category_id);
        $query = $this->db->get('premium_sub_categories');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
           return $query->result();
        }
        return false;	
	}
	
	//update category
	function update_subcategory(){
			$this->db->connection_check();
	$seo_url  = $this->admin_model->seoUrl($this->input->post('sub_category_name'));
		$data = array(
			'cate_id'=>$this->input->post('cate_id'),
			'sub_category_name'=>$this->input->post('sub_category_name'),
			'meta_keyword'=>$this->input->post('meta_keyword'),
			'meta_description'=>$this->input->post('meta_description'),
			'category_status'=>$this->input->post('category_status'),
			'sub_category_url'=>$seo_url
		);
		$id = $this->input->post('sun_category_id');
		$this->db->where('sun_category_id',$id);
		$upd = $this->db->update('sub_categories',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}
	}
	
	//update category
	function update_permiumsubcategory(){
	$this->db->connection_check();
	$seo_url  = $this->admin_model->seoUrl($this->input->post('sub_category_name'));
		$data = array(
			'cate_id'=>$this->input->post('cate_id'),
			'sub_category_name'=>$this->input->post('sub_category_name'),
			'meta_keyword'=>$this->input->post('meta_keyword'),
			'meta_description'=>$this->input->post('meta_description'),
			'category_status'=>$this->input->post('category_status'),
			'sub_category_url'=>$seo_url
		);
		$id = $this->input->post('sun_category_id');
		$this->db->where('sun_category_id',$id);
		$upd = $this->db->update('premium_sub_categories',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	// delete category
	function deletesubcategory($id){
		$this->db->connection_check();
		// get order of category which is to be deleted.
		$start_order = $this->db->get_where('sub_categories',array('sun_category_id'=>$id))->row('sort_order');
		
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('sub_categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$end_order = $get->sort_order;
		}
		$this->db->delete('sub_categories',array('sun_category_id' => $id));
		$newval = $start_order;
		for($inc=$start_order; $inc<=$end_order;$inc++){
			$newval = $newval + 1;
			
			$data = array('sort_order'=>$inc);
			$this->db->where('sort_order',$newval);
			$this->db->update('sub_categories',$data);
		}
		return true;
	}
	
	// delete category
	function deletepremiumsubcategory($id){
		$this->db->connection_check();
		// get order of category which is to be deleted.
		$start_order = $this->db->get_where('premium_sub_categories',array('sun_category_id'=>$id))->row('sort_order');
		
		$this->db->select_max('sort_order');
		$get_max = $this->db->get('sub_categories');
		$gets = $get_max->result();
		foreach($gets as $get){
			$end_order = $get->sort_order;
		}
		$this->db->delete('premium_sub_categories',array('sun_category_id' => $id));
		$newval = $start_order;
		for($inc=$start_order; $inc<=$end_order;$inc++){
			$newval = $newval + 1;
			
			$data = array('sort_order'=>$inc);
			$this->db->where('sort_order',$newval);
			$this->db->update('premium_sub_categories',$data);
		}
		return true;
	}
	
	
	function seoUrl($string) 
	{
		$this->db->connection_check();
		//Lower case everything
		$string = strtolower($string);
		/*New code for spl char replace 22-11-16*/
		$arrstr = array('&'=>'e','ã'=>'a','á'=>'a','à'=>'a','â'=>'a','é'=>'e','ê'=>'e','ó'=>'o','ō'=>'o','ô'=>'o','í'=>'i','î'=>'i','ú'=>'u','ü'=>'u');
		foreach($arrstr as $key=> $newstring)
		{
			$string = str_replace($key, $newstring, $string); 
		}
		/*end 22-11-16*/
		//Make alphanumeric (removes all other characters)
		$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
		//Clean up multiple dashes or whitespaces
		$string = preg_replace("/[\s-]+/", " ", $string);
		//Convert whitespaces and underscore to dash
		$string = preg_replace("/[\s_]/", "-", $string);
		/*$string = base64_encode($string);*/
		return $string;
	}
	
	function get_updated_categories($cateid,$maincateid)
	{
		$this->db->connection_check();
		$this->db->where('category_id',$maincateid);
		$this->db->where('store_id',$cateid);
        $query = $this->db->get('tbl_store_sub_cate');
        if($query->num_rows >= 1)
		{
           return $query->result();
        }
        return false;	
	}
	
	function get_premium_updated_categories($cateid,$maincateid)
	{
		$this->db->connection_check();
		$this->db->where('category_id',$maincateid);
		$this->db->where('store_id',$cateid);
        $query = $this->db->get('tbl_premium_sub_cate');
        if($query->num_rows >= 1)
		{
           return $query->result();
        }
        return false;	
	}
	
	// view all affiliates
	function site_affiliates(){
		$this->db->connection_check();
		$this->db->order_by('affiliate_id','desc');
		$result = $this->db->get('providers');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	// add new affiliates
	// add new affiliates
	function site_addaffiliate(){
		$this->db->connection_check();
		$data = array(
		'affiliate_name'=>$this->input->post('affiliate_name'),
		'affiliate_type'=>$this->input->post('affiliate_type'),
		'affiliate_status'=>$this->input->post('affiliate_status'),
		'affiliate_param'=>$this->input->post('affiliate_param'),
		'affiliate_traking_param'=>$this->input->post('affiliate_traking_param')		
		);
		$this->db->insert('providers',$data);		
		return true;
	}

	// New code for add bank details page//
	function bank_details()
	{
		$this->db->connection_check();
		$this->db->order_by('bankid','desc');
		$result = $this->db->get('tbl_banknames');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}

	function get_bankdetails($id){
	$this->db->connection_check();
	$this->db->where('bankid',$id);
	$result = $this->db->get('tbl_banknames');
		if($result->num_rows > 0){
			return $result->result();		
		}	
	}

	function add_bankdetails(){
		$this->db->connection_check();
		$data = array(
		'bank_name'=>$this->input->post('bank_name'),
		'bank_id'  =>$this->input->post('bank_num_id'),
		);
		$this->db->insert('tbl_banknames',$data);		
		return true;
	}

	function update_bankdetails(){
		$this->db->connection_check();
		
		$bank_id = $this->input->post('bank_id');
		$data = array(
			'bank_name'=>$this->input->post('bank_name'),
			'bank_id'  =>$this->input->post('bank_num_id'),	
		);
		
		$this->db->where('bankid',$bank_id);
		$updation = $this->db->update('tbl_banknames',$data);
		return true;	
	}
	function delete_bankdetails($id)
	{	
		$this->db->connection_check();
		$this->db->delete('tbl_banknames',array('bankid' => $id));
		return true;	
	}

	function delete_multi_bank_details()
	 {
		 $this->db->connection_check();
		  $sort_order = $this->input->post('chkbox');
		 foreach($sort_order as $key=>$val)
		 {
			 $id = $key;		
				$this->db->delete('tbl_banknames',array('id' => $id));			
		 }
		 return true;
	 }
	 function stores()
	 {
	 	$this->db->connection_check();
		$data = array(
		'store_name'=>$this->input->post('store_name'),
		);
		$this->db->insert('tbl_stores',$data);		
		return true;
	 }

	//End//
	
	// view affiliate
	function site_get_affiliate($id){
	$this->db->connection_check();
	$this->db->where('affiliate_id',$id);
	$result = $this->db->get('providers');
		if($result->num_rows > 0){
			return $result->result();		
		}	
	}
	
	
	function site_get_store($id){
	$this->db->connection_check();
	$this->db->where('affiliate_id',$id);
	$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->row();		
		}	
	}
	
	
	
	
	
	// update affiliate
	function site_updateaffiliate(){
		$this->db->connection_check();
		
		$affiliate_id = $this->input->post('affiliate_id');
		$data = array(
			'affiliate_name'=>$this->input->post('affiliate_name'),
			'affiliate_type'=>$this->input->post('affiliate_type'),
			'affiliate_status'=>$this->input->post('affiliate_status'),
			'affiliate_traking_param'=>$this->input->post('affiliate_traking_param'),
			'affiliate_param'=>$this->input->post('affiliate_param')
		
		);
		
		$this->db->where('affiliate_id',$affiliate_id);
		$updation = $this->db->update('providers',$data);
		return true;	
	}
	
	// delete affiliate
	function site_deleteaffiliate($id)
	{	
		$this->db->connection_check();
		$this->db->delete('providers',array('affiliate_id' => $id));
		return true;	
	}
	
	/************ Dec 11th *************/
	
	// view all cashbcak..
	function get_all_missing_cashback($store_name){
		$this->db->connection_check();
		$this->db->order_by('cashback_id','desc');
		//new//
		$this->db->where_in('missing_reason',array('Missing Cashback','Missing Approval'));
		if($store_name)
		{
			$this->db->like('retailer_name',$store_name);		
		}
		//$user_query = $this->db->get('cashback');
		//End//
		//old//
		$user_query = $this->db->get('missing_cashback');
		
		if($user_query->num_rows > 0)
        {
            return $user_query->result();
        }
		else
		{
			return false;
		}
	}

	//Pilaventhiran 07/05/2016 START
	function get_all_missing_approval(){
		$this->db->connection_check();
		$this->db->order_by('cashback_id','desc');
		//new//
		$this->db->where('missing_reason','Missing Approval');
		//$user_query = $this->db->get('cashback');
		//End//
		//old//
		$user_query = $this->db->get('missing_cashback');
		
		if($user_query->num_rows > 0)
        {
            return $user_query->result();
        }
		else
		{
			return false;
		}
	}
	//Pilaventhiran 07/05/2016 END
	
	function view_missing_cb($cbid)
	{
		$this->db->connection_check();
		$this->db->where('cashback_id',$cbid);        
        $query = $this->db->get('missing_cashback');
        if($query->num_rows >= 1)
		{
          return  $row = $query->row();
           // return $query->result();
        }
        return false;
	}
	
	
	
	// update user status..
	function missiing_cashback_update()
	{
		$name = $this->db->query("select * from admin")->row();
		$site_name  = $name->site_name;
				
		$this->db->connection_check();
		$curr_status = $this->input->post('status');	
		$cashback_id = $this->input->post('cashback_id');
		$username = $this->input->post('username');
		$us_email = $this->input->post('us_email');	
		$ticket_id = $this->input->post('ticket_id');
		$retailer_name = $this->input->post('retailer_name');
		$cancel_reason = $this->input->post('cancel_reason');

		$Cashback_Return_Amount = $this->input->post('Cashback_Return_Amount');
		$user_id = $this->input->post('user_id');
		switch($curr_status)
		{
			case 0:	//Success
					$mode_1 	   = "<a href='".base_url()."loja-nao-avisou-compra'>Aprovado</a>";
					$current_msg   = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> 
					Novidades! A loja acabou de nos confirmar que eles realmente não nos informaram da sua compra. Mas agora já está tudo resolvido! Acabou de pingar R$ '.$Cashback_Return_Amount.' na sua conta.
					</span>';
					$userbalance 	   = $this->user_balance($user_id);
					$new_balnce 	   = $userbalance+$Cashback_Return_Amount;
					$mode_1 		   = "Credited";
					$transation_reason = 'Cashback';
					$details_id 	   = $this->input->post('cashback_id');
					$this->update_users_balance($cashback_id,$user_id,$Cashback_Return_Amount,$mode_1,$new_balnce,$transation_reason,$details_id,'missing_cashback');				
					//$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> Dear '.$username.',<br><br>Thank you for sending us the details of your transaction. Your Missing Cashback Ticket: '.$ticket_id.' has been Completed Successfully. Your Cashback Amount Added into your Account.<br><br>Please let us know if you have any further queries. Thaks For your business.<br><br> Current Status: Completed<br><br> Warm regards,<br> '.$site_name.' Team</span>';
					$current_msg   	   = $Cashback_Return_Amount;
					$cancel_reason 	   = $Cashback_Return_Amount;
			break;
			case 1: //Cancel
					$mode_1 	   = "<a href='".base_url()."loja-nao-avisou-compra'>Cancelado</a>";
					$redirect_link = "<a href='".base_url()."recomendacoes-evitar-cancelamento'>recomentacoes para evitar cancelamento</a>";
					$current_msg   = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> 
					A loja acabou de nos atualizar sobre o seu caso e, infelizmente, eles não aprovaram o seu pedido. A justificativa que nos deram foi: '.$cancel_reason.'. Para evitar problemas futuros leia as Regras e exceções da loja e siga os '.$redirect_link.'"
					</span>';
			break;
			case 2: //Send to retailer
					$mode_1 	 = "<a href='".base_url()."loja-nao-avisou-compra'>Enviado para a loja</a>";
					$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">
					Acabamos de enviar a sua reclamação para a loja. O processo de conferência é manual e pode levar até 40 dias úteis para que eles nos respondam. (A gente sabe que isso é muito demorado e é um saco esperar tanto, mas infelizmente não depende de nós . E esse é um “prazo máximo” pode ser que leve bem menos que isso).
					</span>';
			break;
			case 3: //Pending
					/*$userbalance 	   = $this->user_balance($user_id);
					$new_balnce 	   = $userbalance+$Cashback_Return_Amount;
					$mode_1 		   = "Credited";
					$transation_reason = 'Cashback';
					$details_id 	   = $this->input->post('cashback_id');
					$this->update_users_balance($cashback_id,$user_id,$Cashback_Return_Amount,$mode_1,$new_balnce,$transation_reason,$details_id,'missing_cashback');				
					//$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> Dear '.$username.',<br><br>Thank you for sending us the details of your transaction. Your Missing Cashback Ticket: '.$ticket_id.' has been Completed Successfully. Your Cashback Amount Added into your Account.<br><br>Please let us know if you have any further queries. Thaks For your business.<br><br> Current Status: Completed<br><br> Warm regards,<br> '.$site_name.' Team</span>';
					$current_msg   	   = $Cashback_Return_Amount;
					$cancel_reason 	   = $Cashback_Return_Amount;*/
			break;
			case 4: //Complete ticket only
					$mode_1 	 = "<a href='".base_url()."loja-nao-avisou-compra'>complete_ticket_only</a>";
					$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">
					Complete Ticket only.
					</span>';

					/*$userbalance 	   = $this->user_balance($user_id);
					$new_balnce 	   = $userbalance+$Cashback_Return_Amount;
					$mode_1 		   = "Credited";
					$transation_reason = 'Cashback';
					$details_id 	   = $this->input->post('cashback_id');
					$this->update_users_balance($cashback_id,$user_id,$Cashback_Return_Amount,$mode_1,$new_balnce,$transation_reason,$details_id,'missing_cashback');				
					//$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> Dear '.$username.',<br><br>Thank you for sending us the details of your transaction. Your Missing Cashback Ticket: '.$ticket_id.' has been Completed Successfully. Your Cashback Amount Added into your Account.<br><br>Please let us know if you have any further queries. Thaks For your business.<br><br> Current Status: Completed<br><br> Warm regards,<br> '.$site_name.' Team</span>';
					$current_msg   	   = $Cashback_Return_Amount;
					$cancel_reason 	   = $Cashback_Return_Amount;*/
			break;
			
		}
		
		$support_ticket = $this->db->query("SELECT * from tbl_users where user_id=$user_id")->row('support_tickets');

		$data = array(
			'status'=>$this->input->post('status'),
			'status_update_date'=>$this->input->post('status_update_date'),
			'cancel_msg'=>$cancel_reason,
			'current_msg'=>$current_msg,
		);	
		$cashback_id = $this->input->post('cashback_id');
		$this->db->where('cashback_id',$cashback_id);
		$upd = $this->db->update('missing_cashback',$data);
		if($upd)
		{
			if($curr_status!=3)
			{
				if($support_ticket == 1)
				{
					$mail_temp 		= $this->db->query("select * from tbl_mailtemplates where mail_id='14'")->row();
					$fe_cont 		= $mail_temp->email_template;
					$name 			= $this->db->query("select * from admin")->row();
					//$subject 		= "Your Missing Ticket Reply";
					$subject 	    = $mail_temp->email_subject;
					$admin_emailid  = $name->admin_email;
					$site_logo 		= $name->site_logo;
					$site_name  	= $name->site_name;
					$contact_number = $name->contact_number;
					$servername 	= base_url();
					$nows 			= date('Y-m-d');	
					$this->load->library('email');
					//$see_status_missing = "<a href='".base_url()."loja_nao_avisou_compra'>status da solicitação</a>";
					$unsuburl     = base_url().'un-subscribe/missing_cashback/'.$user_id;
	           		$myaccount    = base_url().'minha-conta';
					
					$gd_api=array(
						'###ADMINNO###'=>$contact_number,
						'###EMAIL###'=>$username,
						'###DATE###'=>$nows,
						'###MESSAGE###'=>$current_msg,
						'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
						'###SITENAME###' =>$site_name,
						'###MISSING_CASHBACK_STATUS###'=>$mode_1,
						//'###SEE_STATUS_MISSING###'=>$see_status_missing,
						'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
	                	'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
						);
							   
					$gd_message=strtr($fe_cont,$gd_api);
					
					$config = Array(
					 'mailtype'  => 'html',
					  'charset'   => 'utf-8',
					  );

					$list = array($us_email);
					
					$this->email->initialize($config);
					$this->email->set_newline("\r\n");
					$this->email->from($admin_emailid,$site_name.'!');
					$this->email->to($list);
					$this->email->subject($subject);
					$this->email->message($gd_message);
					$this->email->send();
					$this->email->print_debugger();
				}	
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	
	// delete user details..
	function delete_missing_cashback($id)
	{
		$this->db->connection_check();
		$this->db->delete('missing_cashback',array('cashback_id' => $id));  
		$this->db->delete('transation_details',array('details_id' => $id));     
		return true;
	}
	
	function update_users_balance($cashback_id,$userid,$trans_amount,$mode,$newbalnce,$transation_reason,$details_id=null,$table=null)
	{
		//echo $cashback_id; echo "<br>";
		//echo $details_id; exit
		$this->db->connection_check();
		$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$userid))->row('balance');
		if($prev_userbal == '')
		{
			$prev_userbal = 0;	
		}
		$data = array(		
		'balance' => $newbalnce);
		$this->db->where('user_id',$userid);
		$update_qry = $this->db->update('tbl_users',$data);
		if($update_qry)
		{
			
			$now = date('Y-m-d H:i:s');
			// Transation
			
			$this->db->select_max('trans_id');
				$result = $this->db->get('transation_details')->row();  
				$trans_id = $result->trans_id;
				$trans_id = $trans_id+1;
				$n9 = '5236555';
				$n12 = $n9 + $trans_id; 
			$data = array(		
			'transation_amount' => $trans_amount,
			'user_id' => $userid,
			'transation_date' => $now,
			'transation_reason' => $transation_reason,
			'mode' => $mode,
			'transation_id'=>$n12,
			'details_id'=>$details_id,
			'table'=>$table,
			'transation_status ' => 'Pending');
			$this->db->insert('transation_details',$data);
			$user_deta = $this->view_user($userid);
			//$name = $this->db->query("select * from admin")->row();
			if($user_deta->refer!=0)
			{
				/*$refer = $user_deta->refer;
				
				//New code for refferal category type//
					$categorytype = $user_deta->referral_category_type;

					$name   = $this->db->query("select * from referral_settings where ref_id='$categoryone'")->row();
					$status = $name->ref_by_rate;
					if($status == 1)
					{	
						$caspe = $name->ref_cashback_rate;
					}
					
					
				//End//
				//$caspe = $name->referral_cashback;
				$cal_percent =($trans_amount*$caspe)/100;
				$this->db->select_max('trans_id');
				$result = $this->db->get('transation_details')->row();  
				$trans_id = $result->trans_id;
				$trans_id = $trans_id+1;
				$n9 = '5236555';
				$n12 = $n9 + $trans_id; 
	
				$data = array(		
				'transation_amount' => $cal_percent,
				'user_id' => $userid,
				'transation_date' => $now,
				'transation_id'=>$n12,
				'transation_reason' => "Pending Referal Payment",
				'mode' => $mode,
				'details_id'=>'',
				'table'=>'',
				'transation_status ' => 'Pending');
				$this->db->insert('transation_details',$data);*/
			}
			
			if($table == 'missing_cashback')
			{
				$data = array(
				'user_id' 	        => $userid,
				'transation_id'	    => $trans_id,
				'transation_reason' => 'Missing Cashback request',
				'cashback_reason' 	=> 'Missing Cashback',
				'transation_amount'	=> $trans_amount,
				'mode'				=> 'debited',
				'transation_date'   => $now,
				'transaction_date'  => $now, 
				'transation_status' => 'Approved' 
				);
				$this->db->insert('transation_details',$data);
			}
			/*$this->db->where('cashback_id',$details_id);        
       		$querys = $this->db->get('missing_cashback')->row();
			$click_details = $this->click_history_details($querys->click_id);
			$this->db->select_max('cashback_id');
				$result = $this->db->get('cashback')->row();  
				$cashback_id = $result->cashback_id;
				$cashback_id = $cashback_id+1;
				$n9 = '666554';
				$n12 = $n9 + $cashback_id; 
				$m = '1';
			$data = array(		
			'user_id' => $userid,
			'coupon_id' => $querys->retailer_name,
			'affiliate_id' => $querys->retailer_name,
			'status' => 'Completed',
			'cashback_amount'=>$trans_amount,
			'reference_id'=>$n12,
			'referral'=>$m,
			'date_added' => $now);
			$this->db->insert('cashback',$data);*/
			
			/*new code for missing ticket details update casshback table details start 25-8-16*/
			$this->db->where('cashback_id',$details_id);        
       		$querys = $this->db->get('missing_cashback')->row();
       		$cashback_reference = $querys->cashback_reference;

       		$data = array(		
				'status'   		  => 'Completed',
				'referral' 		  => 1,
				'cashback_amount' => $trans_amount,
				//'transaction_date'=> $now
				);
			$this->db->where('reference_id',$cashback_reference);
			$update_qry = $this->db->update('cashback',$data);
			/*end 25-6-16*/


			/*New code for withdraw email notification details 9-1-17*/
			$names    	   = $this->db->query("select * from admin where admin_id=1")->row();
			$min_cash_amt1 = $names->minimum_cashback;
			$min_cash_amt  = $this->currency_format($min_cash_amt1);
			$Site_Logo     = $names->site_logo;
			$admin_emailid = $names->admin_email;
			$site_name 	   = $names->site_name;
			$user_balances = $this->view_balance($userid);
			$userdetailss  = $this->view_user($userid);
			//print_r($userdetailss);
			$with_status   = $userdetailss[0]->withdraw_mail;
			$firstname 	   = $userdetailss[0]->first_name;
			$lastname      = $userdetailss[0]->last_name;
			$email 		   = $userdetailss[0]->email;

			/*new code for username details 9-1-17*/
			if($firstname == '' && $lastname == '')
			{
				$ex_name  = explode('@', $userdetailss[0]->email);
				$username = $ex_name[0]; 
			}
			else
			{
				$username = $firstname.''.$lastname;
			}	
			/*End 9-1-17*/
			
			//echo "hai";exit;
			//echo $with_status; exit;
			if($prev_userbal < $min_cash_amt)
			{
				if($with_status == 1)
				{	//echo "hai";
					if($user_balances>=$min_cash_amt)
					{	//echo "hai1"; exit;
						$unsuburl	= base_url().'un-subscribe/withdraw/'.$userid;
						$myaccount  = base_url().'resgate';
						$obj_temp   = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
						if($obj_temp->num_rows>0)
						{
							$mail_temp  = $obj_temp->row(); 
							$fe_cont    = $mail_temp->email_template;	
							$subject  	= $mail_temp->email_subject;		
							$servername = base_url();
							$nows 		= date('Y-m-d');	
							$this->load->library('email');
							$gd_api=array(
								
								'###NAME###'=>$username,
								'###AMOUNT###'=>str_replace('.', ',', $user_balances),
								'###REQUEST_WITHDRAW###'=>str_replace('.', ',', $min_cash_amt),
								'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
								'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
								'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
								);
											   
							$gd_message=strtr($fe_cont,$gd_api);
							$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
							);
							//echo $admin_emailid; echo "<br>";
							//echo $us_email;exit;
							//echo "<pre>"; print_r($list = array($us_email)); exit;
							$this->email->set_newline("\r\n");
							$this->email->initialize($config);
							$this->email->from($admin_emailid,$site_name.'!');
							$this->email->to($email);
							$this->email->subject($subject);
							$this->email->message($gd_message);
							$this->email->send();
							$this->email->print_debugger();
							//echo $user_email; echo "<br>";
						    //echo $gd_message;
						    // exit;
						}
					}
				}
			}	
			/*end 9-1-17*/
			return true;
		}
		else 
		{ 
			return false;
		}	
	}
	
	function user_balance($user_id=null)
	{
		$this->db->connection_check();
		if($user_id!="")
		{
			$this->db->where('user_id',$user_id);
			$allfaqs = $this->db->get('tbl_users');
			return $allfaqs->row("balance");
		}
		else
		{
			return 0;
		}
		
	}	
	
	function click_history_details($click_id)
	{
		$this->db->connection_check();
		$this->db->where('click_id',$click_id);
		$all = $this->db->get('click_history');
		if($all->num_rows > 0){
			return $all->row();
		}
		return false;
	}
	/************ Dec 11th *************/
	
	function bulk_stores($bulkcoupon)
	{
		$this->db->connection_check(); 
		$coupon_type = '';
		$this->load->library('CSVReader');
		$main_url = 'uploads/stores/'.$bulkcoupon;
	 	$result =   $this->csvreader->parse_file($main_url);
	
		if(count($result)!=0)
		{
			foreach($result as $res)
			{
				$new_random = mt_rand(0,99999);
				$affiliate_name = $res['affiliate_name'];
				$seo_url  = $this->admin_model->seoUrl($affiliate_name);
				/*New code for encode affilaite_url details 9-11-16*/
				/*$seo_url  = base64_encode($seo_url);*/
				/*End 9-11-16*/
				$affiliate_logo_1 = $res['affiliate_logo'];
				
				//$file = 'http://media.themalaysianinsider.com/assets/uploads/articles/flipkart-logo-072914.jpg.jpg';
				// Open the file to get existing content
				$data = file_get_contents($affiliate_logo_1);
				
				$affiliate_logo = $new_random.'.jpg';
				
				//New code for AWS 24-9-16//
				//$filepath = 'uploads/affiliates/'.$affiliate_logo;
				//$tmp 	  = $data;
				//$this->load->library('S3');
				//$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
				//$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ);
				//End//


				// New file
				$new = 'uploads/affiliates/'.$affiliate_logo;
				// Write the contents back to a new file
				file_put_contents($new, $data);				
				$affiliate_desc = $this->db->escape_str($res['affiliate_desc']);
				$cashback_percentage = $res['cashback_percentage'];
				$site_url = $res['site_url'];
				
				/*$website_url = $res['website_url'];*/
				$meta_keyword = $this->db->escape_str($res['meta_keyword']);
				$meta_description = $this->db->escape_str($res['meta_description']);
				$affiliate_cashback_type = $this->db->escape_str($res['affiliate_cashback_type']);				
				$featured = $res['featured'];
				$now = date('Y-m-d H:i:s');
				$store_of_week = $res['store_of_week'];				
				//Pilaventhiran 14/05/2016 START
				$logo_url = $res['logo_url'];
				
				$retailer_ban_url = $res['retailer_ban_url'];
				$sidebar_image_url = $res['sidebar_image_url'];
				$terms_and_conditions = $res['terms_and_conditions'];
				$how_to_get_this_offer = $res['how_to_get_this_offer'];
				$notify_desk = $res['notify_desk'];
				$old_cashback = $res['old_cashback'];
				$redir_notify = $res['redir_notify'];
				$name_extra_param = $res['name_extra_param'];
				$content_extra_param = $res['content_extra_param'];
				$cashback_percent_android = $res['cashback_percent_android'];
				$cashback_content_android = $res['cashback_content_android'];
				$content_extra_param_android = $res['content_extra_param_android'];
				//Pilaventhiran 14/05/2016 END
				$this->db->where('affiliate_name',$affiliate_name);
				$result = $this->db->get('affiliates');
				
				if($result->num_rows == 0)
				{
					$results = $this->db->query("INSERT INTO `affiliates` (`affiliate_name`, `affiliate_url`, `affiliate_logo`, `affiliate_desc`, `affiliate_status`, `cashback_percentage`, `logo_url`, `meta_keyword`, `meta_description`, `featured`, `affiliate_cashback_type`,`store_of_week`, `date_added`, `retailer_ban_url`, `sidebar_image_url`, `terms_and_conditions`, `how_to_get_this_offer`, `notify_desk`, `old_cashback`, `redir_notify`, `name_extra_param`, `content_extra_param`, `cashback_percent_android`, `cashback_content_android`, `content_extra_param_android`) VALUES ('$affiliate_name', '$seo_url', '$affiliate_logo', '$affiliate_desc', '1','$cashback_percentage', '$logo_url', '$meta_keyword', '$meta_description', '$featured', '$affiliate_cashback_type','$store_of_week','$now','$retailer_ban_url','$sidebar_image_url','$terms_and_conditions','$how_to_get_this_offer','$notify_desk','$old_cashback','$redir_notify','$name_extra_param','$content_extra_param','$cashback_percent_android','$cashback_content_android','$content_extra_param_android');");
				}				
				
			}
		}
		
		return true;
	}
	
	
	function reports_upload($bulkcoupon){ 
		$this->db->connection_check();
		$coupon_type = '';
		$this->load->library('CSVReader');
		$main_url = 'uploads/reports/'.$bulkcoupon;
	 	$result =   $this->csvreader->parse_file($main_url);
		//$name = $this->db->query("select * from admin")->row();
		//$ref_cashbcak_percent =  $name->referral_cashback;

		if(count($result)!=0)
		{	

			
		 
			$s =1;
			foreach($result as $res)
			{
				$pay_out_amount    = $res['pay_out_amount'];
				$sale_amount       = $res['sale_amount'];
				$offer_provider    = $res['offer_provider'];
				$transaction_id    = $res['transaction_id'];
				$user_tracking_id  = $res['user_tracking_id'];
				//$transaction_id1   = rand(10000,99999);
	  		    //$newtransaction_id = md5($transaction_id1);
	  		    //echo $newtransaction_id; exit;
				$store_details = $this->get_offer_provider_cashback($offer_provider);
			
				
				
				$get_userid = decode_userid($user_tracking_id);
			
				if($store_details)
				{
				
					if($store_details->cashback_percentage)
					{
						$affiliate_cashback_type = $store_details->affiliate_cashback_type;
					if($store_details->affiliate_cashback_type=='Percentage')//Percent
						{
							$is_cashback = 1;
							
							$cashback_percentage = $store_details->cashback_percentage;
							$cashback_calc = ($sale_amount*$cashback_percentage)/100;
							$cashback_amount = number_format($cashback_calc, 2);
							$check_ref = $this->check_ref_user($get_userid);
							$ref_cashback_amount = 0;
							if($check_ref>0)					
							{	
								/*old code
								$ref_id  = $check_ref;
								$ref_cashback_percent = $ref_cashbcak_percent;
								$ref_cashback_amount = number_format((($cashback_amount*$ref_cashbcak_percent)/100), 2);
								*/
								//New code for referral details//
								$ref_id  = $check_ref;
								$return = $this->check_active_user($ref_id);
								if($return)
								{
									foreach($return as $newreturn)
									{
										$category_type = $newreturn->referral_category_type; 
										 	
										$referrals 	  		  = $this->db->query("select * from referral_settings where ref_id='$category_type'")->row();	
										$status 			  = $referrals->ref_by_rate;
										if($status == 1)
										{
											$ref_cashback_percent = $referrals->ref_cashback_rate;
											$ref_cashback_amount  = $ref_cashback_percent;
										}	 
										 
									}
								}

								//End//
							}
							$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
						}
						else //Flat
						{
							$is_cashback = 1;
							$cashback_percentage = $store_details->cashback_percentage;
							//$cashback_calc = ($sale_amount*$cashback_percentage)/100;
							$cashback_amount = number_format($cashback_percentage, 2);
							$check_ref = $this->check_ref_user($get_userid);
							$ref_cashback_amount = 0;
							if($check_ref>0)					
							{
								/*old code
								$ref_id  = $check_ref;
								$ref_cashback_percent = $ref_cashbcak_percent;
								$ref_cashback_amount = number_format((($cashback_amount*$ref_cashbcak_percent)/100), 2);
								*/

								//New code for referral details//
								$ref_id  = $check_ref;
								$return  = $this->check_active_user($ref_id);

								if($return)
								{
									foreach($return as $newreturn)
									{
										$category_type = $newreturn->referral_category_type;  	
										$referrals 	  		  = $this->db->query("select * from referral_settings where ref_id='$category_type'")->row();	
										$status 			  = $referrals->ref_by_rate;
										if($status == 1)
										{
											$ref_cashback_percent = $referrals->ref_cashback_rate;
											$ref_cashback_amount  = $ref_cashback_percent;
										} 
									}
								}
								//End//
							}

							$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
						}
					}
					else 
					{
						$is_cashback = 0;
						$cashback_percentage = 0;
						$cashback_amount=0;
						$ref_id = 0;
						$ref_cashback_percent= 0;
						$ref_cashback_amount =0;
						$total_Cashback_paid= 0;
						$affiliate_cashback_type ='';
					}
				}
				else
				{
					
						$is_cashback = 0;
						$cashback_percentage = 0;
						$cashback_amount=0;
						$ref_id = 0;
						$ref_cashback_percent= 0;
						$ref_cashback_amount =0;
						$total_Cashback_paid= 0;
						$affiliate_cashback_type='';
				}
				$date = $res['date'];
				$now = date('Y-m-d H:i:s');
				$last_updated = $now;
				
				$results = $this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`, `is_cashback`, `cashback_percentage`, `affiliate_cashback_type`,`cashback_amount`, `ref_id`, `ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`) VALUES ('$offer_provider', '$date', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '$last_updated', '$is_cashback', '$cashback_percentage', '$affiliate_cashback_type','$cashback_amount', '$ref_id','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','$now');");
				//,`report_update_id`,'$newtransaction_id'
				
				$insert_id = $this->db->insert_id();
				
				if($is_cashback!=0)
				{
					$update_user_bal = $this->update_user_bal($get_userid,$cashback_amount);
					$now = date('Y-m-d H:i:s');
					$transation_reason = "Cashback";
					$mode = "Credited";
					$this->db->select_max('trans_id');
				$result = $this->db->get('transation_details')->row();  
				$trans_id = $result->trans_id;
				$trans_id = $trans_id+1;
				$n9 = '5236555';
				$n12 = $n9 + $trans_id; 
					$data = array(		
						'transation_amount' => $cashback_amount,
						'user_id' => $get_userid,
						'transation_date' => $now,
						'transation_reason' => $transation_reason,
						'mode' => $mode,
                        'cashback_transaction' =>$sale_amount,
                        'cashback_reason' => $offer_provider,
						'details_id'=>$insert_id,
						'transation_id'=>$n12,
						'table'=>'tbl_report',
						'report_update-id'=>$newtransaction_id,
						'transation_status ' => 'Pending');
					$this->db->insert('transation_details',$data);
				
					if($ref_cashback_amount!=0)
					{
						/*$this->db->select_max('trans_id');
						$result = $this->db->get('transation_details')->row();  
						$trans_id = $result->trans_id;
						$trans_id = $trans_id+1;
						$n9 = '5236555';
						$n12 = $n9 + $trans_id; 
						$update_user_bal = $this->update_user_bal($ref_id,$ref_cashback_amount);
						$data = array(		
							'transation_amount' => $ref_cashback_amount,
							'user_id' => $ref_id,
							'transation_date' => $now,
							'transation_reason' => 'Pending Referal Payment',
							'mode' => $mode,
							'transation_id'=>$n12,
							'details_id'=>'',
							'table'=>'',
							'transation_status ' => 'Pending');						
						$this->db->insert('transation_details',$data);*/
					}
					
					$this->db->select_max('cashback_id');
				$result = $this->db->get('cashback')->row();  
				$cashback_id = $result->cashback_id;
				$cashback_id = $cashback_id+1;
				$n9 = '666554';
				$n12 = $n9 + $cashback_id; 
					
					$data = array(		
					'user_id' => $get_userid,
					'coupon_id' => $offer_provider,
					'affiliate_id' => $offer_provider,
					'status' => 'Completed',
					'cashback_amount'=>$cashback_amount,
					'transaction_amount' => $sale_amount,
					'reference_id'=>$n12,
					//'report_update-id'=>$newtransaction_id,
					'date_added' => $now);
					$this->db->insert('cashback',$data);
				}
		
				$s++;
			}
		}
		
		return true;
	}
	
	function get_offer_provider_cashback($storename)
	{
		$this->db->connection_check();
		$this->db->like('affiliate_name', $storename);
		$this->db->limit(1,0);
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->result();
			return $result->row();
		}
		return false;
	}
//SATz //	
	function check_ref_user($ref_user)
	{
		$this->db->connection_check();
		$this->db->where('user_id',$ref_user);        
		$result = $this->db->get('tbl_users');
		if($result->num_rows > 0){
			return $result->row('refer');

		}
		return false;
	}

//SATz //	
	function update_user_bal($user_id,$newamount)
	{		
				$this->db->connection_check();	
				$old_bal = $this->user_balance($user_id);
				$new_bal = $old_bal+$newamount;
				$data = array(		
				'balance' => $new_bal);
				$this->db->where('user_id',$user_id);
				$update_qry = $this->db->update('tbl_users',$data);
	}
	
	function reports()
	{
		$this->db->connection_check();
		$this->db->order_by("report_id", "desc");
		$result = $this->db->get('tbl_report');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	function transaction_id_reports($transaction_id)
	{
		$this->db->connection_check();
		//$this->db->order_by("report_id", "desc");
		$con=$this->db->where('transaction_id',$transaction_id);
		$result = $this->db->get('tbl_report');
		if($result->num_rows > 0){
			return true;
		}
		return false;
	}
	
	
	function view_report($report_id)
	{
		$this->db->connection_check();
		$this->db->where('report_id',$report_id);        
        $query = $this->db->get('tbl_report');
        if($query->num_rows >= 1)
		{
          return $row = $query->row();
        }
        return false;
	}

	//New code for Pending cashback details//
	function report_export()
	{
		$this->db->connection_check();
		$this->db->order_by('affiliate_id','desc');
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	} 

	
	// view user email	
	function user_name($user_id)
	{
		$this->db->connection_check();	
		$result = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('first_name');
		return $result;	
	}

	/*New code for username details 17-10-16*/
	// view user email	
	function user_names($user_id)
	{
		$this->db->connection_check();	
		$result = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row();
		return $result;	
	}
	/*End*/
		
	
	function transactions()
	{
		$this->db->connection_check();
		//$this->db->select("*");
		//$this->db->order_by("trans_id", "desc");
		//$this->db->from("transation_details");
		//$result = $this->db->get();
		$result = $this->db->query("SELECT * from `transation_details` order by trans_id desc"); //`trans_id`,`transation_id`,`user_id`,`report_update_id`,`transation_reason`,`transation_amount`,`transation_date`,`table`,`details_id`,`new_txn_id`,`ref_user_tracking_id`,`transation_status`	
		if($result->num_rows > 0)
		{
			return $result->result();
		}
		return false;
	}
		function get_transaction1($id){
			$this->db->connection_check();
			$this->db->where("trans_id", $id);
			$this->db->from("transation_details");
			$result = $this->db->get();
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	
	
	//adding cms..
	function addblog($logo)
	{
		$now = date('Y-m-d H:i:s');
		$data = array(
		'cms_heading' => $this->input->post('page_title'),
		'cms_metatitle' => $this->input->post('meta_title'),
		'cms_metakey' => $this->input->post('meta_keyword'),
		'cms_metadesc' => $this->input->post('meta_description'),
		'cms_content' => $this->input->post('cms_content'),
		'affiliate_logo' => $logo,
		'blog_time' =>  $now,
		'cms_status' => $this->input->post('cms_status')
		);
		
		$this->db->insert('tbl_blog',$data);
		return true;
	}
	// get all cms
	function get_allblog(){
		
		$cms_query = $this->db->get('tbl_blog');
		if($cms_query->num_rows > 0)
        {
            $row = $cms_query->row();
            return $cms_query->result();
        }
		else
		{
			return false;		
		}
	}
	
	// get particular cms
	function get_blogcontent($id){
	
		$this->db->where('cms_id',$id);        
        $query = $this->db->get('tbl_blog');
        if($query->num_rows >= 1)
		{
           $row = $query->row();			
            return $query->result();			
        }      
        return false;		
	}
	
	
	//update cms ..
	function updateblog($img){
	$now = date('Y-m-d H:i:s');
		$data = array(
			'cms_heading' => $this->input->post('page_title'),
			'cms_metatitle' => $this->input->post('meta_title'),
			'cms_metakey' => $this->input->post('meta_keyword'),
			'cms_metadesc' => $this->input->post('meta_description'),
			'cms_content' => $this->input->post('cms_content'),
			'cms_status' =>  $this->input->post('cms_status'),
			'blog_time' =>  $now,
			'affiliate_logo' =>  $img
		);
		$id =  $this->input->post('cms_id');
		$this->db->where('cms_id',$id);
		$upd = $this->db->update('tbl_blog',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}	
	}
	
	
	// delete cms..
	function deleteblog($id){
	
		$this->db->delete('tbl_blog',array('cms_id' => $id));
		return true;
	
	}
	
	function get_allcomments($blogid){
		$this->db->where('bid',$blogid);
		$cms_query = $this->db->get('tbl_bloguser_comments');
		if($cms_query->num_rows > 0)
        {
            $row = $cms_query->row();
            return $cms_query->result();
        }
		else
		{
			return false;		
		}
	}
	
	function delete_comments($id){
	
		$this->db->delete('tbl_bloguser_comments',array('cid' => $id));
		return true;
	
	}
	
	function status_change_comments($changes_id)
	{
		$this->db->where('cid',$changes_id);
		$cms_query = $this->db->get('tbl_bloguser_comments')->row();
		if($cms_query->status=='active')
		{
			$st = 'deactive';
		}
		else
		{
			$st = 'active';
		}
		$data = array(
			'status' =>  $st
		);
		$this->db->where('cid',$changes_id);
		$upd = $this->db->update('tbl_bloguser_comments',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}	
	}
	
	function add_comments(){
		$now = date('Y-m-d H:i:s');
		$data = array(
		'bid'=>$this->input->post('blog_id'),
		'user_id'=>'Admin',
		'comments'=>$this->input->post('comments'),
		'created_date'=>$now,
		'c_date'=>$now,
		'status'=>'active'	
		);
		$this->db->insert('tbl_bloguser_comments',$data);		
		return true;
	}
	
	function add_manual_credit($id)
	{	
		//echo "hai";
		$userid 	  = $this->input->post('user_id'); 
		$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$userid))->row('balance');
		if($prev_userbal == '')
		{
			$prev_userbal = 0;	
		}
		if($id == '1')
		{
			if($this->input->post('status') == 'Approved')
			{
				$user_bale = $this->view_balance($this->input->post('user_id'));
				$newbalnce= $user_bale+$this->input->post('transation_amount');
				$data = array(		
				'balance' => $newbalnce);
				$this->db->where('user_id',$this->input->post('user_id'));
				$update_qry = $this->db->update('tbl_users',$data);
			}
			$this->db->select_max('trans_id');
			$result = $this->db->get('transation_details')->row();  
			$trans_id = $result->trans_id;
			$trans_id = $trans_id+1;
			$n9 = '5236555';
			$n12 = $n9 + $trans_id; 
			$now = date('Y-m-d');
			$data = array(
			'user_id'=>$this->input->post('user_id'),
			'transation_reason'=>$this->input->post('transation_reason'),
			'transation_amount'=>$this->input->post('transation_amount'),
			'transation_date'=>$now,
			'transaction_date'=>$now,
			'transation_id'=>$n12,
			'mode'=>'Credited',
			'transation_status'=>$this->input->post('status')	
			);
			$this->db->insert('transation_details',$data);	
			if($this->input->post('transation_reason')=="Cashback")
			{
				$refer_user = $this->view_user($this->input->post('user_id'));
				if($refer_user)
				{
					foreach($refer_user as $single)
					{
						$referral_user = $single->refer;
					}
					
					$referral_user_det = $this->view_user($referral_user);
					if($referral_user_det)
					{
						foreach($referral_user_det as $single1)
						{
							$referral_balance = $single1->balance;
							$referral_id 	  = $single1->user_id;
							
							//new code for referral cashback amount details//

							$category_type 	  = $single1->referral_category_type;
							$referrals = $this->db->query("select * from referral_settings where ref_id='$category_type'")->row();	
							$status    = $referrals->ref_by_rate;
							if($status == 1)
							{
								$caspe     = $referrals->ref_cashback_rate;
							}
							//End//
						}
						
						//$name = $this->db->query("select * from admin")->row();
						//$caspe = $name->referral_cashback;
						
						//New code hide for 13-9-16
						/*$tr_amt = $this->input->post('transation_amount');
						//$cal_percent =($tr_amt*$caspe)/100;
						//echo $cal_percent; exit;
						$this->db->select_max('trans_id');
						$result = $this->db->get('transation_details')->row();  
						$trans_id = $result->trans_id;
						$trans_id = $trans_id+1;
						$n9 = '5236555';
						$n12 = $n9 + $trans_id; 
						$data = array(		
						'transation_amount' => $cal_percent,
						'user_id' => $referral_id,
						'transation_date' => $now,
						'transation_reason' => "Referal Payment",
						'mode'=>'Credited',
						'transation_id'=>$n12,
						'details_id'=>'',
						'table'=>'',
						'transation_status ' => $this->input->post('status'));
						$this->db->insert('transation_details',$data);
						if($this->input->post('status') == 'Approved'){
						$this->db->where('user_id',$referral_id);
						$this->db->update('tbl_users',array('balance'=>$referral_balance+$cal_percent));*/

						//End 13-9-16
					} 
				}
			}		
			//return true;
		}
		else if($id =='0')
		{
			$trans_id = $this->input->post('trans_id');
			if($this->input->post('transation_status') != 'Approved' || $this->input->post('transation_status') != 'Paid')
			{
				if($this->input->post('status') == 'Approved')
				{
					$user_bale = $this->view_balance($this->input->post('user_id'));
					$newbalnce = $user_bale+$this->input->post('transation_amount');
					$data 	   = array(		
					'balance'  => $newbalnce);
					$this->db->where('user_id',$this->input->post('user_id'));
					$update_qry= $this->db->update('tbl_users',$data);
				}
			}
			if($this->input->post('transation_status') == 'Approved' || $this->input->post('transation_status') == 'Paid')
			{
				if($this->input->post('status') == 'Canceled' || $this->input->post('status') == 'Pending')
				{
					$user_bale = $this->view_balance($this->input->post('user_id'));
					$newbalnce = $user_bale-$this->input->post('transation_amount');
					$data 	   = array(		
					'balance'  => $newbalnce);
					$this->db->where('user_id',$this->input->post('user_id'));
					$update_qry= $this->db->update('tbl_users',$data);
				}
			}
			$data = array(
			'transation_status'=>$this->input->post('status')
			);
			$this->db->where('trans_id',$trans_id);
			$this->db->update('transation_details',$data);		
			//return true;
		}

		$names    	   = $this->db->query("select * from admin where admin_id=1")->row();
		$min_cash_amt1 = $names->minimum_cashback;
		$min_cash_amt  = $this->currency_format($min_cash_amt1);
		$Site_Logo     = $names->site_logo;
		$admin_emailid = $names->admin_email;
		$site_name 	   = $names->site_name;
		$user_balances = $this->view_balance($this->input->post('user_id'));
		$userdetailss  = $this->view_user($this->input->post('user_id'));
		 
		$with_status   = $userdetailss[0]->withdraw_mail;
		$firstname 	   = $userdetailss[0]->first_name;
		$lastname      = $userdetailss[0]->last_name;
		$email 		   = $userdetailss[0]->email;

		/*new code for username details 9-1-17*/
		if($firstname == '' && $lastname == '')
		{
			$ex_name  = explode('@', $userdetailss[0]->email);
			$username = $ex_name[0]; 
		}
		else
		{
			$username = $firstname.''.$lastname;
		}	
		/*End 9-1-17*/

		if($prev_userbal < $min_cash_amt)
		{	
			  
			if($with_status == 1)
			{	 
				if($user_balances>=$min_cash_amt)
				{	
					//echo "hai1"; exit;
					$unsuburl	= base_url().'un-subscribe/withdraw/'.$this->input->post('user_id');
					$myaccount  = base_url().'resgate';
					$obj_temp   = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
					if($obj_temp->num_rows>0)
					{
						$mail_temp  = $obj_temp->row(); 
						$fe_cont    = $mail_temp->email_template;	
						$subject  	= $mail_temp->email_subject;		
						$servername = base_url();
						$nows 		= date('Y-m-d');	
						$this->load->library('email');
						$gd_api=array(
							
							'###NAME###'=>$username,
							'###AMOUNT###'=>str_replace('.', ',', $user_balances),
							'###REQUEST_WITHDRAW###'=>str_replace('.', ',', $min_cash_amt),
							'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
							);
										   
						$gd_message=strtr($fe_cont,$gd_api);
						$config = Array(
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
						);

						$this->email->set_newline("\r\n");
						$this->email->initialize($config);
						$this->email->from($admin_emailid,$site_name.'!');
						$this->email->to($email);
						$this->email->subject($subject);
						$this->email->message($gd_message);
						$this->email->send();
						$this->email->print_debugger();
					}
				}
			}
		}	
		return true;
	}
	
	function count_coupons($catename=null)
	{
		//$count_coupons = $this->db->query("SELECT count(*) as counting FROM `coupons` where offer_name like '%$catename%'");
		$count_coupons = $this->db->query("SELECT count(*) as counting FROM `coupons` where offer_name='".$catename."' AND coupon_status='completed'");
		if($count_coupons->num_rows > 0)
        {
            return $count_coupons->row();
        }
		else
		{
			return false;
		}
	}
	
	function count_clicks($catid)
	{
		$count_coupons = $this->db->query("SELECT count(*) as counting FROM `click_history` where affiliate_id='$catid'");
		if($count_coupons->num_rows > 0)
        {
            return $count_coupons->row();
        }
		else
		{
			return false;
		}
	}
	
	
/*New changes 9-5-16*/
// add new shopping coupon..
function add_shoppingcoupon($img,$img_type)
 {
	//print_r($_POST); exit;
	$this->db->connection_check();
	   // premium_coupon_feature
	   	$new_features_type = '';
		$premium_coupon_feature = $this->input->post('premium_coupon_feature'); 
		$start_date = $this->input->post('start_date');
		if($start_date!=""){
			$start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
		}
		else {
			$start_date = date('Y-m-d');
		}
		$expiry_date = date('Y-m-d',strtotime($this->input->post('expiry_date')));
		$stcat = $this->input->post('categorys_list');
		
		if($this->input->post('categorys_list'))
		{
			$store_categorys =implode(",",$this->input->post('categorys_list')); 
		}
		else
		{
			$store_categorys ='';
		}
		
		if($premium_coupon_feature)
		{
			$exp_couponfeatures =implode(",",$premium_coupon_feature);  
		}
		else
		{
			$exp_couponfeatures ='';  
		}
		
		$store_name    = $this->input->post('store_name');  
		$extra_param   = $this->input->post('extra_param');  
		$features_type = $this->input->post('premium_coupon_feature'); 


		if(count($features_type) > 0)
		{
			$new_features_type =implode(",",$features_type);  
		}
		 
		$codes=$this->input->post('code');  
		  
		$seo_url  = $this->admin_model->newseoUrl($this->input->post('offer_name'));
		
		$data = array(
			 
			'offer_name'=>$this->input->post('offer_name'),
			'coupon_image'=>$img,
			'description'=>$this->input->post('description'),
			'about'=>$this->input->post('about'),
			'nutshel'=>$this->input->post('nutshel'),
			'fine_print'=>$this->input->post('fine_print'),
			'company'=>$this->input->post('company'),
			'location'=>$this->input->post('location'),
			'user_max' => $this->input->post('user_max'),
			'seo_url'=>$seo_url,
				
			'category'=>$store_categorys,
			'type'=>$this->input->post('type'),
			 // 'title'=>$this->input->post('title'),
			// 'code'=>$this->input->post('code'),
			'long_description' => $this->input->post('long_description'),
			'offer_page'=>$this->input->post('web_url'),
			'amount'=>$this->input->post('amount'),
			 	'coupon_code'=>$codes,  
			'remain_coupon_code'=>$codes,   
			'status'=>"1",    
			'start_date'=>$start_date,
			'expiry_date'=>$expiry_date,

			/*New code 9-5-16*/
			'price'=>$this->input->post('org_price'),
			'store_name'=>$store_name,
			'tracking'=>$extra_param,
			'features'=>$new_features_type,
			/*end code 9-5-16*/
			'img_type' => $img_type,
			'extra_param_url' => $this->input->post('extra_tracking_param')
		); 
		
		
		$this->db->insert('shopping_coupons',$data);
		$new_id = $this->db->insert_id();
		
		/*$codes = $this->input->post('code');
		foreach($codes as $val){
			
			$data = array(
				'shoppingcoupon_id'=>$new_id,
				'status'=>'1',
				'code'=>$val
			);
			
			
			$this->db->insert('shoppingcodes',$data);		
		} */
		
		/* 
		foreach($stcat as $maincat)
		 {
				$var="size_".$maincat;
				$subcat = $this->input->post($var);
				foreach($subcat as $subcategory)
				{
					$data = array(
					'category_id'=>$maincat,
					'sub_category_id'=>$subcategory,
					'store_id'=>$new_id
					);
					$this->db->insert('tbl_premium_sub_cate',$data);
			}
		} 
		
		*/ 
		 
		 
		return true;
 }
/*New changes are accure 11-5-16*/

function update_shoppingcoupon($img) 
{
	$this->db->connection_check();
		/*print_r($this->input->post());
		exit;*/
		$premium_coupon_feature = $this->input->post('premium_coupon_feature'); 
		if($premium_coupon_feature)
		{
			$exp_couponfeatures =implode(",",$premium_coupon_feature); 
		}
		else
		{
			$exp_couponfeatures ='';
		}
			
		if($this->input->post('start_date')){
			$start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
		}
		else 
		{
			$start_date = date('Y-m-d');
		}
		$stcat = $this->input->post('categorys_list');
		if($this->input->post('categorys_list'))
		{
			$store_categorys =implode(",",$this->input->post('categorys_list'));
		}
		else
		{
			$store_categorys ='';
		}
		
		$expiry_date 		= date('Y-m-d',strtotime($this->input->post('expiry_date')));	
		$shoppingcoupon_id  = $this->input->post('shoppingcoupon_id'); 
		$codes 			    = $this->input->post('code');
		$seo_url  	        = $this->admin_model->newseoUrl($this->input->post('offer_name'));
		$data = array(
			 
			'offer_name'	   	 =>$this->input->post('offer_name'), 
			'seo_url'		   	 =>$seo_url, 			
			'coupon_image'     	 =>$img,
			'description'	   	 =>$this->input->post('description'),
			'about'			   	 =>$this->input->post('about'),
			'nutshel'		   	 =>$this->input->post('nutshel'),
			'fine_print'	   	 =>$this->input->post('fine_print'),
			'company'		   	 =>$this->input->post('company'),
			'location'		   	 =>$this->input->post('location'),
			'category'	 	   	 =>$store_categorys,
			'type'			   	 =>$this->input->post('type'),	
			'amount'		   	 =>$this->input->post('amount'),
			'offer_page'	   	 =>$this->input->post('web_url'),  
			'start_date'	   	 =>$start_date,
			'long_description' 	 =>$this->input->post('long_description'),
			'date_added'       	 =>$start_date,
			'user_max' 		   	 => $this->input->post('user_max'),
			'expiry_date'      	 =>$expiry_date, 
			'coupon_code'        =>$codes,  
			'remain_coupon_code' =>$codes, 
			'price'				 =>$this->input->post('org_price'),
			'store_name'		 =>$this->input->post('store_name'),
			'tracking'  	  	 =>$this->input->post('extra_param'),
			'features'		     =>$exp_couponfeatures,
			'img_type' 			 =>$this->input->post('img_types'),
			'extra_param_url' 	 =>$this->input->post('extra_tracking_param')
		);
		
		$this->db->where('shoppingcoupon_id',$shoppingcoupon_id);
		$updation = $this->db->update('shopping_coupons',$data);
   
		/*
		$codes = $this->input->post('code');
		$shoppingcode_id = $this->input->post('shoppingcode_id');
		if($codes)
		{
			foreach($codes as $key=>$val){
				
				$data = array(
					'code'=>$val
				);
				$this->db->where('shoppingcode_id',$shoppingcode_id[$key]);
				$updation = $this->db->update('shoppingcodes',$data);
			}  
		} */  
		  
		// this is for inserting new coupons..
		/* 	$new_codes = $this->input->post('codes');
			if($new_codes!=""){
				foreach($new_codes as $val){
			
					$data = array(
						'shoppingcoupon_id'=>$shoppingcoupon_id,
						'status'=>'1',
						'code'=>$val
					);
					$this->db->insert('shoppingcodes',$data); 
				}
			} */
			//print_r($this->input->post());  
			 
		/* 	foreach($stcat as $maincat)
			{
				
				$var="size_".$maincat;
				$subcat = $this->input->post($var);
				foreach($subcat as $subcategory)
				{
					$data = array(
					'category_id'=>$maincat,
					'sub_category_id'=>$subcategory,
					'store_id'=>$shoppingcoupon_id
					); 
					$this->db->insert('tbl_premium_sub_cate',$data);
			}
		}		 */  
		
	//	exit; 
		
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;
		}
 }	
function reviews(){
	$this->db->connection_check();
	/* $this->db->order_by('id','desc');
	 $result = $this->db->get('revi ews');*/
	 
	 $result = $this->db->query("SELECT * FROM (reviews) where coupon_id IN
							(
								SELECT  shoppingcoupon_id 
									FROM
										`shopping_coupons`
									
							) ORDER BY `id` desc");
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	 
}
 function get_name($cis,$uid)
 {
	 $this->db->connection_check();
	 //echo $uid;die;
	 $this->db->where('user_id',$uid);
	 $result = $this->db->get('tbl_users');
	//echo $this->db->last_query();die;
		if($result->num_rows > 0){
			$uresult = $result->row();
			$uname = $uresult->first_name;
		}else{
			$uname = '';
		}
	
	 $this->db->where('shoppingcoupon_id',$cis);
	
		$result = $this->db->get('shopping_coupons');
		if($result->num_rows > 0){
			$uresult = $result->row();
			$cname = $uresult->offer_name;
			$seo_url = $uresult->seo_url;
		}else{
			$cname = '';
			$seo_url = '';
		}
		return $uname.",".$cname.",".$seo_url;
 }
	
	function changestatus($id,$status)
	{
		$this->db->connection_check();
		if($status==1) $var=0;else $var=1;
			$data = array(
			'approve'=>$var,
			
			);
			$this->db->where('id',$id);
			$updation = $this->db->update('reviews',$data);	
	}
	
	function orders()
	{
		$this->db->connection_check();
		$this->db->order_by('id','desc');
		$this->db->where('status','Paid');
		$result = $this->db->get('premium_order');
		if($result->num_rows > 0){
			return $result->result();
		}
			return false;
	}
	 
	 function sort_categorys_new()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('sort_arr');
		 foreach($sort_order as $key=>$val)
		 {
			  $data = array(
				'sort_order'=>$val			
				);
			$this->db->where('category_id',$key);
			$updation = $this->db->update('categories',$data);	
		 }
		 return true;
			
	 }
	 
	 function sort_premium_categorys_new()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('sort_arr');
		 foreach($sort_order as $key=>$val)
		 {
			  $data = array(
				'sort_order'=>$val			
				);
			$this->db->where('category_id',$key);
			$updation = $this->db->update('premium_categories',$data);	
		 }
		 return true;
			
	 }
	 
	 
	 function multi_delete_user()
	 {
		 $this->db->connection_check();
		 if($this->input->post('chkbox'))
		 {
			  $sort_order = $this->input->post('chkbox');
			  foreach($sort_order as $key=>$val)
			  {
				   $this->db->where('user_id',$key);
				   $query = $this->db->get('tbl_users');
				   $email = $query->row('email');
					
					$data = array(
						'admin_status' => 'deleted',
						'status' =>  '0'
					);
					$this->db->where('user_id',$key);
					$upd = $this->db->update('tbl_users',$data);
					$this->db->delete('referrals',array('referral_email' => $email));   
			  }
		 	return true;
		 }
			
	 }
	 
	 function sort_categorys_new_delete()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
		 foreach($sort_order as $key=>$val)
		 {
			 $id = $key;
		
			// get order of category which is to be deleted.
			$start_order = $this->db->get_where('categories',array('category_id'=>$id))->row('sort_order');
			
			$this->db->select_max('sort_order');
			$get_max = $this->db->get('categories');
			$gets = $get_max->result();
			foreach($gets as $get){
				$end_order = $get->sort_order;
			}
	
			$this->db->delete('categories',array('category_id' => $id));
			$newval = $start_order;
			for($inc=$start_order; $inc<=$end_order;$inc++){
				$newval = $newval + 1;
				
				$data = array('sort_order'=>$inc);
				$this->db->where('sort_order',$newval);
				$this->db->update('categories',$data);
			}
			
		 }
		 return true;
			
	 }
	 
	 
	 
	 
	  function sort_sub_categorys_new()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('sort_arr');
		 foreach($sort_order as $key=>$val)
		 {
			  $data = array(
				'sort_order'=>$val			
				);
			$this->db->where('sun_category_id',$key);
			$updation = $this->db->update('sub_categories',$data);	
		 }
		 return true;
			
	 }
	 
	  function sort_sub_categorys_new_delete()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
		 foreach($sort_order as $key=>$val)
		 {
			 $id = $key;
		
			// get order of category which is to be deleted.
			$start_order = $this->db->get_where('sub_categories',array('sun_category_id'=>$id))->row('sort_order');
			
			$this->db->select_max('sort_order');
			$get_max = $this->db->get('sub_categories');
			$gets = $get_max->result();
			foreach($gets as $get){
				$end_order = $get->sort_order;
			}
	
			$this->db->delete('sub_categories',array('sun_category_id' => $id));
			$newval = $start_order;
			for($inc=$start_order; $inc<=$end_order;$inc++){
				$newval = $newval + 1;
				
				$data = array('sort_order'=>$inc);
				$this->db->where('sort_order',$newval);
				$this->db->update('sub_categories',$data);
			}
			
		 }
		 return true;
			
	 }
	 		
	 function sort_premium_categorys_new_delete()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
		 foreach($sort_order as $key=>$val)
		 {
			 $id = $key;		
				$start_order = $this->db->get_where('premium_categories',array('category_id'=>$id))->row('sort_order');		
				$this->db->select_max('sort_order');
				$get_max = $this->db->get('premium_categories');
				$gets = $get_max->result();
				foreach($gets as $get){
					$end_order = $get->sort_order;
				}
		
				$this->db->delete('premium_categories',array('category_id' => $id));
				$newval = $start_order;
				for($inc=$start_order; $inc<=$end_order;$inc++){
					$newval = $newval + 1;
					
					$data = array('sort_order'=>$inc);
					$this->db->where('sort_order',$newval);
					$this->db->update('premium_categories',$data);
				}			
			
		 }
		 return true;
			
	 }
	 
	 function delete_multi_site_affiliate()
	 {
		 $this->db->connection_check();
		  $sort_order = $this->input->post('chkbox');
		 foreach($sort_order as $key=>$val)
		 {
			 $id = $key;		
				$this->db->delete('providers',array('affiliate_id' => $id));			
		 }
		 return true;
	 }
	 
	 
	 function sort_affiliates()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('sort_arr');
		 foreach($sort_order as $key=>$val)
		 {
			  $data = array(
				'sort_order'=>$val			
				);
			$this->db->where('affiliate_id',$key);
			$updation = $this->db->update('affiliates',$data);	
		 }
		 return true;
	 }
	 
	 function sort_affiliates_delete()
	 {
		 $this->db->connection_check();
		  $sort_order = $this->input->post('chkbox');
		 foreach($sort_order as $key=>$val)
		 {
				 $id = $key;
				 $this->db->delete('affiliates',array('affiliate_id' => $id));
				$this->db->delete('tbl_store_sub_cate',array('store_id' => $id));
				
				$this->db->delete('click_history',array('affiliate_id' => $id));	
					
		 }
		  return true;
		
	 }
	 
	 function click_history_bulk_delete()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
			 foreach($sort_order as $key=>$val)
			 {
					 $id = $key;
					 $this->db->delete('click_history',array('click_id'=>$id));
			 }
		return true;	
	 }
	 
	 function cashback_details_bulk_delete()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
			 foreach($sort_order as $key=>$val)
			 {
					 $id = $key;
					 $this->db->delete('category_cashback',array('cbid'=>$id));
			 }
		return true;	
	 }
	 
	 
	  function coupons_bulk_delete()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
			 foreach($sort_order as $key=>$val)
			 {
					 $delete_id = $key;
					 $this->db->delete('coupons',array('coupon_id'=>$delete_id));
	
			 }
		return true;	
	 }
	 
	 
	 function bulk_shopping_delete()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
			foreach($sort_order as $key=>$val)
			 {
					 $delete_id = $key;
					$this->db->delete('shopping_coupons',array('shoppingcoupon_id'=>$delete_id)); 
			 }
		return true;
	 }
	 
	 function delete_bulk_orders()
	 {
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
			foreach($sort_order as $key=>$val)
			 {
					 $delete_id = $key;
					$this->db->delete('premium_order',array('id'=>$delete_id)); 
			 }
		return true;
	 }
	 
	 function bulk_reviews_delete()
	 {
		 $this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
			foreach($sort_order as $key=>$val)
			 {
					 $delete_id = $key;
					$this->db->delete('reviews',array('id'=>$delete_id)); 
			 }
		return true; 
	 }
	 
	 function reports_bulk_delete()
	 {
		 $this->db->connection_check();
			$sort_order = $this->input->post('chkbox');
			foreach($sort_order as $key=>$val)
			 {
					 $delete_id = $key;
					$this->db->delete('tbl_report',array('report_id'=>$delete_id)); 
			 }
		return true;  
	 }
	 
	 
	function delete_bulk_records($sort_order,$table,$feald)
	{		 
			$this->db->connection_check();
			foreach($sort_order as $key=>$val)
			{
			    $delete_id = $key;
			    /*New code for update multiple users balance details 27-5-16.*/
			 	$cb_r  = $this->db->get_where('cashback',array('cashback_id'=>$delete_id))->row();   
				if($cb_r)
				{
					$txn_id  = $cb_r->txn_id;
				    $amount  = $cb_r->cashback_amount;
				    $user_id = $cb_r->user_id;
				}
				
				if($cb_r->status == "Completed")
				{
					//echo "hai"; exit;
					/*New code for update a user balance details 27-5-16.*/
					$total_amt 	   = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('balance');
					$remain_amount = ($total_amt - $amount);  
					$data = array(		
						'balance' => $remain_amount);
								
					$this->db->where('user_id',$user_id);
					$update_qry = $this->db->update('tbl_users',$data);
					/*End 27-5-16.*/
				}
			 	/*End*/
				$this->db->delete($table,array($feald=>$delete_id));
				$this->db->delete('transation_details',array('trans_id'=>$txn_id)); 
			}
			return true;  
	}
	 
	 
	//forgetpassword
	function forgetpassword(){
		$this->db->connection_check();
		$email = $this->input->post('forget_email');
		
		$user_email = $this->db->get_where('admin', array('admin_id' =>'1'))->row('email_notify');
		
		if($user_email!=$email){
			return false;
		}
		//send email 
			$this->load->library('email');
			
			$this->db->where('admin_id',1);
			$admin_det = $this->db->get('admin');
			if($admin_det->num_rows >0) 
			{    
				 $admin = $admin_det->row();
				 $admin_email = $admin->admin_email;
				 $site_name = $admin->site_name;
				 $admin_no = $admin->contact_number;
				 $admin_password = $admin->admin_password;
				   $site_logo = $name->site_logo;
			}			
			$date =date('Y-m-d');
			
			$this->db->where('mail_id',6);
			$mail_template = $this->db->get('tbl_mailtemplates');
			if($mail_template->num_rows >0) 
			{        
			   $fetch = $mail_template->row();
			   $subject = $fetch->email_subject;  
			   $templete = $fetch->email_template;
			    $config = Array(
				  //'protocol' => 'smtp',
				 // 'smtp_host' => 'ssl://smtp.googlemail.com',
				 // 'smtp_port' => 465,
				 // 'smtp_user' => 'vivek.developer@osiztechnologies.com',
				 // 'smtp_pass' => 'iamnotlosero',
				  'mailtype'  => 'html',
				  'charset'   => 'utf-8',
				  );
     			// $this->email->initialize($config);        
     			$this->email->set_newline("\r\n");
     			$this->email->initialize($config);
			   	$this->email->from($admin_email,$site_name.'!');
			   	$this->email->to($user_email);
			   	$this->email->subject($subject);
				
			    $data = array(
					'###PASSWORD###'=>$admin_password,
					'###COMPANYLOGO###'=>'<img alt="" src='.base_url()."/uploads/adminpro/".$site_logo.' />',
					'###SITENAME###'=>$site_name,
					'###ADMINNO###'=>$admin_no,
					'###DATE###'=>$date
			    );
			   $content_pop=strtr($templete,$data);
			   $this->email->message($content_pop);
			   $this->email->send(); 
				return 'success';
			}
			
	}
	
function multi_delete_banners()
{
	$this->db->connection_check();
		 if($this->input->post('chkbox'))
		 {
			  $sort_order = $this->input->post('chkbox');
			  foreach($sort_order as $key=>$val)
			  {
				  $this->db->delete('tbl_banners',array('banner_id' => $key));
			  }
		 	return true;
		 }
}
function payment_settings()
{
		$this->db->connection_check();
		$merchant_key = $this->input->post('merchant_key');
		$merchant_salt = $this->input->post('merchant_salt');
		$merchant_id = $this->input->post('merchant_id');
		$payment_mode = $this->input->post('payment_mode');
		//$data['payment_mode'] = $details->payment_mode;			
			$data = array(
			'merchant_key'=>$merchant_key,
			'merchant_salt'=>$merchant_salt,
			'merchant_id'=>$merchant_id,
			'payment_mode'=>$payment_mode
			);
			$this->db->where('admin_id',1);	
			$this->db->update('admin',$data);
			return true;
}
function category_cashback($cateid)
{
		$this->db->connection_check();
		$this->db->where('store_categorys !=', '');
		$this->db->where('affiliate_id',$cateid);	
		$all = $this->db->get('affiliates');
		if($all->num_rows > 0)
		{
			$results=  $all->row();
			$store_categorys = $results->store_categorys;
			//echo "SELECT * FROM `categories` where FIND_IN_SET(`category_id`,'$store_categorys')";
			$quers = $this->db->query("SELECT * FROM `categories` where FIND_IN_SET(`category_id`,'$store_categorys')");
			if($quers->num_rows > 0)
			{
				$fetch = $quers->result();
				return $fetch;
			//$catelist = explode(',',$store_categorys);
			}
			else
			{
				return false;
			}
						
		}
		else
		{
			return false;
		}
}
function cashback_details_category($catid,$storeid)
{
	$this->db->connection_check();
	$this->db->where('category_id',$catid);	
	$this->db->where('store_id',$storeid);
	$all = $this->db->get('category_cashback');
	if($all->num_rows > 0)
	{
		$results=  $all->row();
		return $results;
	}
	else
	{
		return false;
	}
	
}
	function update_catecashback_ins($caskbackid=null)
	{
		$this->db->connection_check();
		if($this->input->post('action')=='new')
		{
			$data = array(			
			'store_id' => $this->input->post('store_id'),
			'cashback_type' => $this->input->post('cashback_type'),
			'cashback' => $this->input->post('cashback'),
			'cashback_details' => $this->input->post('cashback_details'),
			'status' => $this->input->post('status')		
			);
			$this->db->insert('category_cashback',$data);
			return true;
		}
		else
		{
			$data = array(			
			'store_id' => $this->input->post('store_id'),
			'cashback_type' => $this->input->post('cashback_type'),
			'cashback' => $this->input->post('cashback'),
			'cashback_details' => $this->input->post('cashback_details'),
			'status' => $this->input->post('status')		
			);
			
			$cbid = $this->input->post('caskbackid');
			$this->db->where('cbid',$cbid);
			$updation = $this->db->update('category_cashback',$data);
			return true;
		}
		
	}
	function upload_reports($bulkcoupon)
	{	
		$this->db->connection_check();
		$coupon_type = '';
		$this->load->library('CSVReader');	
		$main_url 	 = 'uploads/reports/'.$bulkcoupon;	
		$result   	 = $this->csvreader->parse_file($main_url);
		 
		if(count($result)!=0)	
		{
			$s = 1;
			foreach($result as $res)	
			{ 
				//echo "hai"; exit;
				//SATz report duplication identifier 
				//$all = 0;
				$transaction_id   = $res['transaction_id'];
				//$this->db->where('transaction_id',$transaction_id);
				//$all = $this->db->get('tbl_report')->num_rows();
				$this->db->where('report_update_id',$transaction_id);
				$all = $this->db->get('cashback')->num_rows();
				
				if($all == 0)
				{	
					
					//$dt 			  = new DateTime($res['date']);
					$datess 		  = date('Y-m-d H:i:s', strtotime($res['date']));
					$pay_out_amount   = $res['pay_out_amount'];	
					$sale_amount 	  = $res['sale_amount'];	
					$offer_provider   = $res['offer_provider'];				
					$cashback 		  = $res['cashback'];	
					$user_tracking_id = $res['user_tracking_id'];		
	                $plataform 		  = $res['plataform'];
					$type_cb 		  = $res['type_cb'];
					$calc_cb 		  = $res['calc_cb'];
					$commission 	  = $res['commission'];

					/*New code for store id details 4-3-17*/
					$pingou_store_id  = $res['pingou_store_id'];					
					/*End 4-3-17*/

					//SATz//

					$store_details 	  = $this->get_offer_provider_cashback($offer_provider);
				 	$get_userid 	  = decode_userid($user_tracking_id); 
					$cashback_amount  = $cashback;
					$check_ref 		  = $this->check_ref_user($get_userid);

					/*New code for user referral category type details 25-10-16*/
					$ref_cat_type  = $this->db->query("SELECT * from `tbl_users` where user_id='$get_userid'")->row('ref_user_cat_type'); 
					/*End 25-10-16*/
										
					if($transaction_id !='')
					{
						$newtransaction_id   = $transaction_id;
					}
					else
					{
						$newtransaction_id   = rand(1000,9999);	
					}	
					
					$getuser = $this->view_user($get_userid);
				
				 
					$new_txn_id 		 = rand(1000,9999);
					$ref_cashback_amount = 0;
					$ref_id 			 = 0;
					$referred 			 = 0;
					$txn_id_new 		 = 0;

					if($check_ref > 0)		
					{
						$ref_id  = $check_ref;
						$return  = $this->check_active_user($ref_id);
						//New code for referral amount bu categorywise// 
						$now  	 = date('Y-m-d');
						$n9   	 = '666554';
						$n12  	 = $n9 + $ref_id; 	
						$mode 	 = "Credited";	
						
						if($return)
						{	
							//$new_txn_id = rand(1000,9999);
							$referred = 1;
							$i = 1;
							foreach($return as $newreturn)
							{
								 
								$category_type        = $ref_cat_type;
								//$category_type 	  = $newreturn->referral_category_type; 
								$referrals 	  		  = $this->db->query("select * from referral_settings where ref_id ='$category_type'")->row();	
								$ref_by_percentage    = $referrals->ref_by_percentage;
								$cashback_percentage  = $referrals->ref_cashback;
								$dayscount		      = $referrals->valid_months;
								$ref_by_rate          = $referrals->ref_by_rate;
								$ref_cashback_amount  = $referrals->ref_cashback_rate;
								$bonus_by_ref_rate    = $referrals->bonus_by_ref_rate;

								//Code for (1** Type--Refferal by Percentage) 30-3-16//
								if($ref_by_percentage == 1)
								{	
									foreach($getuser as $newusers)
									{
										$reg_date     = date('Y-m-d', strtotime($newusers->date_added));
										$exp_date     = date('Y-m-d', strtotime("+$dayscount day", strtotime($reg_date)));
										$new_expdate  = strtotime($exp_date);
										$current_date = date("Y-m-d");
										$tday_date    = strtotime($current_date);
										$mailids 	  = $newusers->email;

										if(($new_expdate) >= ($tday_date))
										{	

											$new_cashback_amount = round((($cashback_amount)*($cashback_percentage)/100),2);  
											//$new_cashback_amount = $this->admin_model->currency_format($new_cashback_amount);
											
											$data = array(			
											'transation_amount' => $new_cashback_amount,	
											'user_id' => $ref_id,	
											'transation_date' => $now,
											'transation_id'=>$n12,	
											'transation_reason' => 'Referral Cashback amount',	
											'mode' => $mode,
											'transaction_date' => $datess,
											'details_id'=>'',	
											'table'=>'',	
											'transation_status ' => 'Pending',
											'new_txn_id'=> $new_txn_id,
											'report_update_id'=>$newtransaction_id,
											'ref_user_tracking_id'=>$get_userid
											);

											$this->db->insert('transation_details',$data);
											$txn_id_new = $this->db->insert_id();

											$referal_mail = $newreturn->referral_mail;
											$user_email   = $newreturn->email;
											$first_name   = $newreturn->first_name;
											$last_name 	  = $newreturn->last_name;								

											if($first_name == '' && $last_name == '')
											{
												$ex_name   = explode('@', $user_email);
												$user_name = $ex_name[0]; 
											}
											else
											{
												$user_name = $first_name.' '.$last_name;
											}

											//New mail code for Pending referral Cashback Mail 29-3-17//
											$this->db->where('admin_id',1);
											$admin_det = $this->db->get('admin');
											if($admin_det->num_rows >0) 
											{    
												$admin 		 = $admin_det->row();
												$admin_email = $admin->admin_email;
												$site_name   = $admin->site_name;
												$admin_no 	 = $admin->contact_number;
												$site_logo 	 = $admin->site_logo;
											}
											$date =date('Y-m-d');
											if($referal_mail == 1)
											{	
												$this->db->where('mail_id',20);
												$mail_template = $this->db->get('tbl_mailtemplates');
												if($mail_template->num_rows >0) 
												{
													$fetch = $mail_template->row();
													$subject = $fetch->email_subject;
													$templete = $fetch->email_template;
													$url = base_url().'my_earnings/';
													$unsuburls	 = base_url().'un-subscribe/referral/'.$ref_id;
											   		$myaccount    = base_url().'minha-conta';
													
													$this->load->library('email');
													$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
													);
															
													$sub_data = array(
													'###SITENAME###'=>$site_name
													);
													
													$subject_new = strtr($subject,$sub_data);
													// $this->email->initialize($config);
													$this->email->set_newline("\r\n");
													$this->email->initialize($config);
													$this->email->from($admin_email,$site_name.'!');
													$this->email->to($user_email);
													$this->email->subject($subject_new);
													$datas = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>date('y-m-d'),
													'###AMOUNT###'=>str_replace('.', ',', $new_cashback_amount),
													'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
													);
													$content_pop=strtr($templete,$datas);
													$this->email->message($content_pop);
													$this->email->send();  
												}
											}	
											//End 29-3-17//

										}
										else
										{
											$data = array(
												'status' => 'Inativa',
											);
											$this->db->where('referral_email',$mailids);
											$updates = $this->db->update('referrals',$data);	 
										}
									}
								}	
								$i++;	 
							} 
						}
						//End//
					}

					//echo $s;
					$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
					
					//main contents
					$is_cashback = 1;	
					$cashback_percentage = 0;	
					$cashback_amount=$cashback;	
					$ref_id = $ref_id;	
					$ref_cashback_percent = $ref_cashback_amount;	
					$ref_cashback_amount  = $ref_cashback_amount;	
					$total_Cashback_paid  = $total_Cashback_paid;	
					$affiliate_cashback_type='';
					//main contents
					$date = date('Y-m-d H:i:s', strtotime($res['date']));	
					$now = date('Y-m-d H:i:s');	
					$last_updated = $now;

					$this->db->where('transaction_id',$transaction_id);
					$report_all = $this->db->get('tbl_report')->num_rows();
					
					if($report_all == 0)
					{
						$results   = $this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`, `is_cashback`, `cashback_percentage`, `affiliate_cashback_type`,`cashback_amount`, `ref_id`, `ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`) VALUES ('$offer_provider', '$date', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '$last_updated', '$is_cashback', '$cashback_percentage', '$affiliate_cashback_type','$cashback_amount', '$ref_id','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','$now','$newtransaction_id');");	
						$insert_id = $this->db->insert_id();
					}

					if($is_cashback!=0)	
					{	
						// $update_user_bal = $this->update_user_bal($get_userid,$cashback_amount);	
						$now = date('Y-m-d');	
						// $transation_reason = "Pending Cashback";	
						$mode = "Credited";	

						//New code hide referral amount earining for referral 9-9-16
						/*if($ref_cashback_amount!=0)	
						{
							$refer_details = $this->db->query("select * from tbl_users where user_id=$get_userid")->row();
							$refer_status  = $refer_details->referral_amt;
							if($refer_status == 0)
							{	
								$this->db->select_max('trans_id');
								$result = $this->db->get('transation_details')->row();  
								$trans_id = $result->trans_id;
							 
								$trans_id = $trans_id+1;
								$n9 = '5236555';
								$n12 = $n9 + $trans_id; 
								//$update_user_bal = $this->update_user_bal($ref_id,$ref_cashback_amount);	
								
								$this->db->where('transation_id',$transaction_id);
								$trans_all = $this->db->get('transation_details')->num_rows();
								if($trans_all == 0)
								{

									$data = array(			
										'transation_amount' => $ref_cashback_amount,	
										'user_id' => $ref_id,
										'transation_id' => $newtransaction_id,	
										'transation_date' => $now,
										//'transation_id'=>$n12,	
										'transation_reason' => 'Pending Referal Payment',	
										'mode' => $mode,
										'transaction_date' => $datess,
										'details_id'=>'',	
										'table'=>'',
										'new_txn_id'=> '',	
										'transation_status ' => 'Pending',
										'report_update_id'=>$newtransaction_id
										);
									//echo print_r($data); exit;
									$this->db->insert('transation_details',$data);
									$txn_id_new = $this->db->insert_id();
								}	
								//User table update referral status//
								$data = array(		
								'referral_amt' => 1);
								$this->db->where('user_id',$get_userid);
								$update_qry = $this->db->update('tbl_users',$data);	
								//End//
							}	
						}*/
						//End 9-9-16

						$this->db->select_max('cashback_id');
						$result 	 = $this->db->get('cashback')->row();  
						$cashback_id = $result->cashback_id;
						$cashback_id = $cashback_id+1;
						$n9 = '666554';
						$n12 = $n9 + $cashback_id; 

						//print_r($get_userid);exit;
						$this->db->where('report_update_id',$newtransaction_id);
						$cash_all = $this->db->get('cashback')->num_rows();
						if($cash_all == 0)
						{
							$data = array(
							'user_id' => $get_userid,	
							'coupon_id' => $offer_provider,	
							'affiliate_id' => $offer_provider,	
							'status' => 'Pending',	
							'cashback_amount'=>$cashback_amount,	
							'date_added' => $now,
							'referral' => $referred,
							'transaction_amount' => $sale_amount,
							'transaction_date' => $datess,
							'report_update_id'=>$newtransaction_id,
							'reference_id'=>$n12,
							'new_txn_id'=>$new_txn_id,
							'txn_id' => $txn_id_new,
							'plataform' =>$plataform,
							'type_cb' => $type_cb,
							'calc_cb' => $calc_cb,
							'commission' => $commission,
							'pingou_store_id'=>$pingou_store_id
							);	
							$this->db->insert('cashback',$data);	


							/* mail for pending cashback */
							$user_detail = $this->view_user($get_userid);
							if($user_detail)
							{
								foreach($user_detail as $user_detail_single)
								{
									$referral_balance = $user_detail_single->balance;
									$user_email 	  = $user_detail_single->email;
									//$user_name  	  = $user_detail_single->first_name.' '.$user_detail_single->last_name;
									$accbalance 	  = $user_detail_single->cashback_mail;
								

									$first_name   = $user_detail_single->first_name;
									$last_name 	  = $user_detail_single->last_name;								

									if($first_name == '' && $last_name == '')
									{
										$ex_name   = explode('@', $user_email);
										$user_name = $ex_name[0]; 
									}
									else
									{
										$user_name = $first_name.' '.$last_name;
									}
								}
							}
							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin 		 = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name 	 = $admin->site_name;
								$admin_no 	 = $admin->contact_number;
								$site_logo   = $admin->site_logo;
							}
							$date = date('Y-m-d');

							if($accbalance == 1)
							{
								$this->db->where('mail_id',10);
								$mail_template = $this->db->get('tbl_mailtemplates');
								
								if($mail_template->num_rows >0) 
								{
								    $fetch     = $mail_template->row();
								    $subject   = $fetch->email_subject;
								    $templete  = $fetch->email_template;
								    //$url     = base_url().'cashback/my_earnings/';
								    $unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
								    $myaccount = base_url().'minha-conta';
								   
									$this->load->library('email');
									
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);
									
									$sub_data = array(
										'###SITENAME###'=>$site_name
									);
									$subject_new = strtr($subject,$sub_data);
									
									$this->email->set_newline("\r\n");
									$this->email->initialize($config);
									$this->email->from($admin_email,$site_name.'!');
									$this->email->to($user_email);
									$this->email->subject($subject_new);
								   
									$data = array(
										'###NAME###'=>$user_name,
										'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###AMOUNT###'=>str_replace('.', ',', $cashback_amount),
										'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
										'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
								    );
								   
								    $content_pop=strtr($templete,$data);
								   	$this->email->message($content_pop);
								   	$this->email->send();  
								}
							}
							/* mail for pending cashback */	
						}			
					}
					$s++;
				}	
		 	}//exit;
		}
		//echo "454";exit;
		return true;
	}
	function check_active_user($user_id){
		
		$this->db->connection_check();
		$this->db->where('user_id',$user_id);
		$this->db->where('status','1');
		$this->db->where('admin_status','');
		$ret = $this->db->get('tbl_users');
		 
		if($ret->num_rows > 0){
			//return 1;
			return $ret->result();
		}
			return false;
		//return 0;
	}
	
	function ads(){
	$this->db->connection_check();
	$this->db->order_by('ads_id','ASC');
		$result = $this->db->get('ads');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	function get_ads($ads_id)
	{
		$this->db->connection_check();
		$result = $this->db->get_where('ads',array('ads_id'=>$ads_id))->row();
		return $result;
	}
	
	function updateads($img)
	{
		$this->db->connection_check();
		$data = array(
			'ads_url' => $this->input->post('ads_url'),
			'ads_position' => $this->input->post('ads_position'),
			'ads_image' =>  $img
		);
		
		$id =  $this->input->post('ads_id');
		$this->db->where('ads_id',$id);
		$upd = $this->db->update('ads',$data);
		if($upd){
			return true;
		}
		else{
			return false;
		}	
	}
	
	function contacts()
	{
		$this->db->connection_check();
		$this->db->order_by('id','desc');
		$result = $this->db->get('tbl_contact');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	 }
	 
	 
	 function deletecontact($id)
	 {
		 $this->db->connection_check();	
		$this->db->connection_check();$this->db->delete('tbl_contact',array('id' => $id));
		return true;
	
	}
	
	function multi_delete_contacts()
	{
			$this->db->connection_check();
			 if($this->input->post('chkbox'))
			 {
				  $sort_order = $this->input->post('chkbox');
				  foreach($sort_order as $key=>$val)
				  {
					  $this->db->delete('tbl_contact',array('id' => $key));
				  }
				return true;
			 }
	}
	function select_table($tab_conti)
	{
		$this->db->connection_check();
		if($tab_conti=='active')
		{
			 $selqry="SELECT * FROM shopping_coupons  WHERE expiry_date >='".date('Y-m-d')."' order by shoppingcoupon_id desc";  
			 $result=$this->db->query("$selqry"); 
				if($result->num_rows > 0)
				{		
					return $result->result();
				}
		}
		else
		{
			 $selqry="SELECT * FROM shopping_coupons  WHERE expiry_date <='".date('Y-m-d')."' order by shoppingcoupon_id desc";  
			 $result=$this->db->query("$selqry"); 
				if($result->num_rows > 0)
				{		
					return $result->result();
				}
		}
		
		
	}
	//New code for Report Export page//
	//SATz //
	function users_bank_details()
	{
		$this->db->connection_check();
		//$this->db->order_by('bank_id','desc');
		$bank_array = array(
		'account_holder !='=> "",
		'bank_name != '=> "",
		'branch_name != '=> "",
		'account_number !='=> "",
		'ifsc_code !='=> "",
			);
		$this->db->where($bank_array);
		$result = $this->db->get('tbl_users');
		if($result->num_rows > 0){
			return $result->result();
			//return true;
		}
		return false;
	}
	

	function pending_report()
	{
		$store_name  = $this->input->post('store_name');
		$start_date1 = $this->input->post('start_date');
		$end_date1 	 = $this->input->post('end_date');
		$bank_name 	 = $this->input->post('bank_name');
		$start_date  = date("Y-m-d",strtotime($start_date1));
		$end_date    = date("Y-m-d",strtotime($end_date1));
		$user_id 	 = $this->input->post('user_id');
        $sep_user    = explode(',',$user_id);
        $type 		 = $this->input->post('type');
        $store_status= $this->input->post('status');
        $user 		 = $user_id;

        
        if(isset($_POST['savexls']))
		{	
			//New code for XLS downloader//
			$filename   = "export_report-".$t_date.".xls";
			$this->load->dbutil();
			$this->load->helper('download');
			$delimiter  = ",";
			$newline    = "\r\n";
			$enclosure  = '"';
			//End//
		}
		else
		{
			//New code for csv downloader//
			$filename   = "export_report-".$t_date.".csv";
			$this->load->dbutil();
			$this->load->helper('download');
			$delimiter  = ",";
			$newline    = "\r\n";
			$enclosure  = '"';
			//End//
		}
        
         
        //Pending cashback menu//
        if($type == 'pending_cashback')
		{
			$i=0;         	
            
            /*New code for Cashback details with status 11-7-16*/

			/*Store name details 11-7-16*/
       		if(isset($store_name))
       		{
        			if($store_name == "All")
        			{
        				$storename = "`coupon_id`!='' ";
        			}
        			else
        			{
        				$storename = "`coupon_id` = '$store_name'";	
        			}
       		}
        	/*End Store details 11-7-16*/

        	/*Store Status details 11-7-16*/
        	if(isset($store_status))
        	{
        			if($store_status == "All")
        			{
        				$storestatus = "AND `status`!='' ";
        			}
        			else
        			{
        				if($storename == '')
            			{
            				$storestatus = "AND `status`!=''";	
            			}
            			else
            			{
            				$storestatus = "AND `status` = '$store_status'";	
            			}	
        			}
        	}
        	/*End Status details 11-7-16*/

        	/*User id details 11-7-16*/
       		if(isset($user_id))
       		{
        			if($user_id == "")
        			{
        				$userid = "";
        			}
        			else
        			{
        				if(($storename == '') && ($storestatus ==''))
            			{
            				$userid = "AND `user_id` =''";	
            			}
            			else
            			{
            				$userid = "AND `user_id` IN($user_id)";	
            			}	
        			}
       		}
        	/*End User id details 11-7-16*/

        	/*Add Date field details 11-7-16*/
       		if(isset($start_date1))
        	{
            		if($start_date1=="")
            		{
            			$new_start_date = "";
            		}
            		else
            		{
            			if(($storename == '') && ($storestatus == '') && ($userid == '')) 
            			{
            				$new_start_date = "AND `date_added` BETWEEN  '$start_date'";	
            			}
            			else
            			{
            				$new_start_date = "AND `date_added` BETWEEN  '$start_date'";	
            			}
            				
            		}
        	}
            /*End Add Date field details 11-7-16*/

            /*Close Date field details 11-7-16*/
        	if(isset($end_date1))
      		{
            		if($end_date1=="")
            		{
            			$new_end_date = "";
            		}
            		 
            		else
            		{
            			if(($storename == '') && ($storestatus == '') && ($userid == '') && ($new_start_date == ''))
            			{
            				 		
            				$new_end_date = "AND '$end_date'";
 						}
            			else
            			{	
            				$new_end_date = "AND '$end_date'";
            							
            			}		
            		}
        	}
        	/*End Close Date field details 11-7-16*/

        	if(($storename == '') && ($storestatus == '') && ($userid == '') && ($new_start_date == '') && ($new_end_date == ''))
        	{
            		/*$selqry ="SELECT `cashback_id`,`user_id`,`coupon_id`,`transaction_amount`,`status`,`date_added` FROM cashback";*/
            		$selqry ="SELECT * FROM cashback";
            		//echo $selqry; exit;
        	}
        	else
        	{
            		$selqry ="SELECT * FROM cashback  where $storename $storestatus $userid $new_start_date $new_end_date";
            		//echo $selqry; exit;
        	}	

			$result =$this->db->query("$selqry");
			//echo $this->db->last_query(); die;
			ob_end_clean();
			$data = $this->dbutil->csv_from_result($result, $delimiter, $newline, $enclosure);
				force_download($filename, $data);	
		}
		//END//

		//Missing cashback Menu //
		if($type == 'missing_cashback')
		{
        	$status      = $this->input->post('status');
            //foreach($sep_user as $user)
			//{	
				if(($store_name =="All") && ($status =="All") && ($user =='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM missing_cashback"; 	 
				}
				 
				if(($store_name =="All") && ($status !="All") && ($user !='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM missing_cashback where status ='$status' AND user_id IN ($user)"; 	 
				}
				if(($store_name =="All") && ($status =="All") && ($user =='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM missing_cashback where status ='$status' AND trans_date BETWEEN  '$start_date' AND '$end_date'"; 	 
				}

				if(($store_name =="All") && ($status !="All") && ($user =='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM missing_cashback where status ='$status'"; 	 
				}
				if(($store_name =="All") && ($status !="All") && ($user =='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM missing_cashback where status ='$status' AND trans_date BETWEEN  '$start_date' AND '$end_date'"; 	 
				}
				if(($store_name =="All") && ($status !="All") && ($user !='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM missing_cashback where status ='$status' AND user_id IN ($user) AND trans_date BETWEEN  '$start_date' AND '$end_date'"; 	 
				}
				if(($store_name =="All") && ($status =="All") && ($user !='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM missing_cashback where user_id IN ($user)"; 	 
				}
				if(($store_name =="All") && ($status =="All") && ($start_date1!='') && ($end_date1!='') && ($user ==''))
				{
					$selqry ="SELECT * FROM missing_cashback where trans_date BETWEEN  '$start_date' AND '$end_date'";
				}


				if(($store_name !="All") && ($status =="All") && ($user !='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM missing_cashback where retailer_name= '$store_name' AND user_id IN ($user)";
				}
				if(($store_name !="All") && ($status =="All") && ($user =='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM missing_cashback where retailer_name= '$store_name' AND trans_date BETWEEN  '$start_date' AND '$end_date'";
				}
				if(($store_name !="All") && ($status =="All") && ($user !='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM missing_cashback where retailer_name= '$store_name' AND user_id IN ($user) AND trans_date BETWEEN  '$start_date' AND '$end_date'";
				}
				if(($store_name !="All") && ($status !="All") && ($user =='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM missing_cashback where retailer_name= '$store_name' AND status = '$status' AND trans_date BETWEEN  '$start_date' AND '$end_date'";
				}



				if(($store_name !="All") && ($status =="All") && ($start_date1=='') && ($end_date1=='') && ($user ==''))
				{
					$selqry ="SELECT * FROM missing_cashback where retailer_name= '$store_name'";
				}
				if(($store_name !="All") && ($status !="All") && ($start_date1=='') && ($end_date1=='') && ($user ==''))
				{
					$selqry ="SELECT * FROM missing_cashback where retailer_name='$store_name' AND status = '$status'";
				}
				if(($store_name !="All") && ($status !="All") && ($start_date1=='') && ($end_date1=='') && ($user !=''))
				{
					$selqry ="SELECT * FROM missing_cashback where user_id IN ($user) AND retailer_name='$store_name' AND status = '$status'";
				}
				if(($store_name !="All") && ($status !="All") && ($start_date1!='') && ($end_date1!='') && ($user !=''))
				{
					$selqry ="SELECT * FROM missing_cashback where user_id IN ($user) AND retailer_name='$store_name' AND status = '$status' AND trans_date BETWEEN  '$start_date' AND '$end_date'";
				}
				if(($store_name =="All") && ($status =="All") && ($start_date1!='') && ($end_date1!='') && ($user !=''))
				{
					$selqry ="SELECT * FROM missing_cashback where user_id IN ($user) AND trans_date BETWEEN  '$start_date' AND '$end_date'";
				}
				 
				$result =$this->db->query("$selqry");
				ob_end_clean();
				$data = $this->dbutil->csv_from_result($result, $delimiter, $newline, $enclosure);
				force_download($filename, $data);
				/*$result =$this->db->query("$selqry");
				if($result->num_rows > 0)
				{		
					return $result->result();
				}*/
				//$i++;
			//}	
		}	
		//END

		//Withdraw Menu //
		if($type == 'withdraws')
		{
        	//echo "<pre>"; print_r($_POST); 
        	$status    = $this->input->post('status');
        	//New code for withdraw page add abankname details 7-5-16//
        	$bank_name = $this->input->post('bank_name');
        	//End//


        	//New code functionalities for withdraw export page 7-5-16//
        	
        	/*Status details 7-5-16*/

        		if(isset($status))
        		{
        			if($status == "All")
        			{
        				$new_status = "`status`!='' ";
        			}
        			else
        			{
        				$new_status = "`status` = '$status'";	
        			}
        		}

        	/*End Status details 7-5-16*/

        	/*Bank name details 7-5-16*/

        		if(isset($bank_name))
            	{
            		if($bank_name=="All")
            		{
            			$new_bank_name = "AND `bank_name`!=''";
            		}
            		else
            		{
            			if($new_status == '')
            			{
            				$new_bank_name = "AND `bank_name`!=''";	
            			}
            			else
            			{
            				$new_bank_name = "AND `bank_name` = '$bank_name'";	
            			}
            				
            		}
            	}

        	/*End bank name details 7-5-16*/

        	/*Add Date field details 7-5-16*/

        		if(isset($start_date1))
            	{
            		if($start_date1=="")
            		{
            			$new_start_date = "";
            		}
            		else
            		{
            			if(($new_status == '') && ($new_bank_name == ''))
            			{
            				$new_start_date = "`date_added` = '$start_date'";	
            			}
            			else
            			{
            				$new_start_date = "AND `date_added` = '$start_date'";	
            			}
            				
            		}
            	}
            /*End Add Date field details 7-5-16*/

            /*Close Date field details 7-5-16*/

            	if(isset($end_date1))
        		{
            		if($end_date1=="")
            		{
            			$new_end_date = "";
            		}
            		 
            		else
            		{
            			if(($new_status == '') && ($new_bank_name == '') && ($new_start_date == ''))
            			{
            				 		
            				$new_end_date = "`closing_date` = '$end_date'";
 						}
            			else
            			{	
            				$new_end_date = "AND `closing_date` = '$end_date'";
            							
            			}		
            		}
            	}

            /*End Close Date field details 7-5-16*/

        		if(($new_status == '') && ($new_bank_name == '') && ($new_start_date == '') && ($new_end_date == ''))
            	{
            		$selqry ="SELECT * FROM withdraw";
            	}
            	else
            	{
            		$selqry ="SELECT * FROM withdraw  where $new_status $new_bank_name $new_start_date $new_end_date";
            	}	
				 
				$result =$this->db->query("$selqry");
				ob_end_clean();
				$data = $this->dbutil->csv_from_result($result, $delimiter, $newline, $enclosure);
				force_download($filename, utf8_decode($data)); 		 
		}	
		//END

		//Payments Menu //
		if($type == 'payments')
		{
        	$status       = $this->input->post('status');
        	$trans_reason = $this->input->post('trans_reason');
        	$mode 		  = $this->input->post('mode');

        	//foreach($sep_user as $user)
			//{
				if(($status =="all") && ($mode =="all") && ($trans_reason =='') && ($start_date1=='') && ($end_date1=='') && ($user =='')) 
				{
					 $selqry ="SELECT * FROM  transation_details";
				}
				if(($status =="all") && ($mode =="all") && ($user !='') && ($trans_reason =='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM  transation_details where user_id IN ($user)";
				}
				if(($status =="all") && ($mode =="all") && ($user =='') && ($trans_reason !='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM  transation_details where transation_reason ='$trans_reason'";
				}
				if(($status =="all") && ($mode =="all") && ($user =='') && ($trans_reason =='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM  transation_details where transation_date BETWEEN '$start_date' AND '$end_date'";
				}
				if(($status =="all") && ($mode =="all") && ($user !='') && ($trans_reason =='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM  transation_details where user_id IN ($user) AND transation_date BETWEEN '$start_date' AND '$end_date'";
				}
				if(($status =="all") && ($mode =="all") && ($user =='') && ($trans_reason !='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM  transation_details where transation_reason ='$trans_reason' AND transation_date BETWEEN '$start_date' AND '$end_date'";
				}
				if(($status =="all") && ($mode =="all") && ($user !='') && ($trans_reason !='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM  transation_details where user_id IN ($user) AND transation_reason ='$trans_reason'";
				}
				if(($status =="all") && ($mode =="all") && ($user !='') && ($trans_reason !='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM  transation_details where user_id IN ($user) AND transation_reason ='$trans_reason' AND transation_date BETWEEN '$start_date' AND '$end_date'";
				}
				if(($status =="all") && ($mode !="all") && ($user !='') && ($trans_reason !='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM  transation_details where mode ='$mode' AND transation_reason ='$trans_reason' AND transation_date BETWEEN '$start_date' AND '$end_date'";
				}


				if(($status =="all") && ($mode !="all") && ($user !='') && ($trans_reason =='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM  transation_details where user_id IN ($user) AND mode = '$mode' AND user_id = '$user'";
				}
				if(($status =="all") && ($mode !="all") && ($user =='') && ($trans_reason !='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM  transation_details where mode ='$mode' AND transation_reason ='$trans_reason'";
				}
				if(($status =="all") && ($mode !="all") && ($trans_reason =='') && ($start_date1=='') && ($end_date1=='') && ($user ==''))
				{
					 $selqry ="SELECT * FROM  transation_details where mode = '$mode'"; 	 
				}
				if(($status =="all") && ($mode !="all") && ($trans_reason =='') && ($start_date1!='') && ($end_date1!='') && ($user ==''))
				{
					 $selqry ="SELECT * FROM  transation_details where mode = '$mode' AND transation_date BETWEEN '$start_date' AND '$end_date' "; 	 
				}
				if(($status =="all") && ($mode !="all") && ($trans_reason =='') && ($start_date1!='') && ($end_date1!='') && ($user !=''))
				{
					 $selqry ="SELECT * FROM  transation_details where mode = '$mode' AND transation_date BETWEEN '$start_date' AND '$end_date' AND user_id IN ($user)"; 	 
				}
				if(($status =="all") && ($mode !="all") && ($trans_reason !='') && ($start_date1!='') && ($end_date1!='') && ($user ==''))
				{
					 $selqry ="SELECT * FROM  transation_details where mode = '$mode' AND transation_date BETWEEN '$start_date' AND '$end_date' AND transation_reason ='$trans_reason'"; 	 
				}
				if(($status =="all") && ($mode !="all") && ($trans_reason !='') && ($start_date1!='') && ($end_date1!='') && ($user !=''))
				{
					 $selqry ="SELECT * FROM  transation_details where mode = '$mode' AND transation_date BETWEEN '$start_date' AND '$end_date' AND transation_reason ='$trans_reason' AND user_id IN ($user)"; 	 
				}

				if(($status !="all") && ($mode =="all") && ($trans_reason !='') && ($start_date1=='') && ($end_date1=='') && ($user ==''))
				{
					 $selqry ="SELECT * FROM  transation_details where transation_status ='$status' AND transation_reason ='$trans_reason'"; 	 
				}



				if(($status !="all") && ($mode =="all") && ($user !='') && ($trans_reason =='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM  transation_details where user_id = '$user' AND transation_status ='$status' AND user_id IN ($user)";
				}
				if(($status !="all") && ($mode =="all") && ($user =='') && ($trans_reason =='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM  transation_details where transation_status ='$status'";
				}

				//if(($status !="all") && ($mode =="all") && ($user =='') && ($trans_reason !='') && ($start_date1=='') && ($end_date1==''))
				//{
				//	$selqry ="SELECT * FROM  transation_details where transation_reason ='$trans_reason' AND transation_status ='$status' AND user_id IN ($user)";
				//}
				if(($status !="all") && ($mode =="all") && ($user =='') && ($trans_reason =='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM  transation_details where transation_status ='$status' AND transation_date BETWEEN '$start_date' AND '$end_date'";
				}

				if(($status !="all") && ($mode =="all") && ($user !='') && ($trans_reason =='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM  transation_details where transation_status ='$status' AND transation_date BETWEEN '$start_date' AND '$end_date' AND user_id IN ($user)";
				}
				if(($status !="all") && ($mode =="all") && ($user =='') && ($trans_reason !='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM  transation_details where transation_status ='$status' AND transation_date BETWEEN '$start_date' AND '$end_date' AND transation_reason ='$trans_reason'";
				}
				if(($status !="all") && ($mode =="all") && ($user !='') && ($trans_reason !='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM  transation_details where transation_status ='$status' AND transation_date BETWEEN '$start_date' AND '$end_date' AND transation_reason ='$trans_reason' AND user_id IN ($user)";
				}
				

				if(($status !="all") && ($mode !="all") && ($trans_reason !='') && ($start_date1=='') && ($end_date1=='') && ($user ==''))
				{
					 $selqry ="SELECT * FROM  transation_details where mode = '$mode' AND transation_reason ='$trans_reason' AND transation_status ='$status'"; 	 
				}
				if(($status !="all") && ($mode =="all") && ($trans_reason !='') && ($start_date1=='') && ($end_date1=='') && ($user !=''))
				{
					 $selqry ="SELECT * FROM  transation_details where mode = '$mode' AND transation_reason ='$trans_reason' AND transation_status ='$status'"; 	 
				}
				if(($status !="all") && ($mode !="all") && ($trans_reason =='') && ($start_date1!='') && ($end_date1!='') && ($user !=''))
				{
					 $selqry ="SELECT * FROM  transation_details where mode = '$mode' AND transation_status ='$status' AND transation_date BETWEEN '$start_date' AND '$end_date' AND user_id IN ($user)"; 	 
				}
				if(($status !="all") && ($mode !="all") && ($trans_reason !='') && ($start_date1!='') && ($end_date1!='') && ($user ==''))
				{
					 $selqry ="SELECT * FROM  transation_details where mode = '$mode' AND transation_reason ='$trans_reason' AND transation_status ='$status' AND transation_date BETWEEN '$start_date' AND '$end_date'"; 	 
				}


				if(($status !="all") && ($mode !="all") && ($trans_reason =='') && ($start_date1!='') && ($end_date1!='') && ($user ==''))
				{
					 $selqry ="SELECT * FROM  transation_details where mode = '$mode' AND transation_status ='$status' AND transation_date BETWEEN '$start_date' AND '$end_date' "; 	 
				}
				if(($status !="all") && ($mode !="all") && ($trans_reason =='') && ($start_date1=='') && ($end_date1=='') && ($user ==''))
				{
					$selqry ="SELECT * FROM  transation_details where mode = '$mode' AND transation_status ='$status'";
				}
				if(($status !="all") && ($mode !="all") && ($user !='') && ($trans_reason =='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM  transation_details where mode = '$mode' AND transation_status ='$status' AND user_id IN ($user)";
				}
				if(($status !="all") && ($mode !="all") && ($user !='') && ($trans_reason !='') && ($start_date1=='') && ($end_date1==''))
				{
					$selqry ="SELECT * FROM  transation_details where mode = '$mode' AND transation_status ='$status' AND user_id IN ($user) AND transation_reason ='$trans_reason'";
				}
				if(($status !="all") && ($mode !="all") && ($user !='') && ($trans_reason !='') && ($start_date1!='') && ($end_date1!=''))
				{
					$selqry ="SELECT * FROM  transation_details where mode = '$mode' AND transation_status ='$status' AND user_id IN ($user) AND transation_reason ='$trans_reason' AND transation_date BETWEEN '$start_date' AND '$end_date'";
				}

				$result =$this->db->query("$selqry");
				ob_end_clean();
				$data = $this->dbutil->csv_from_result($result, $delimiter, $newline, $enclosure);
				force_download($filename, $data);
				/*$result =$this->db->query("$selqry");
				if($result->num_rows > 0)
				{		
					return $result->result();
				}*/
				//$i++;
			//}		 	
		}	
		//END

		//Subscribers Menu //
		if($type == 'subscribers')
		{
        	
        	$status       = $this->input->post('status');

				if(($status =="all") && ($start_date1 == "") && ($end_date1 == ""))
				{
					$selqry ="SELECT * FROM  subscribers";  	 
				}
				 
				if(($status!="all") && ($start_date1 == "") && ($end_date1 == ""))
				{
					$selqry ="SELECT * FROM  subscribers where subscriber_status = $status";  	 
				}
				if(($status!="all") && ($start_date1!= "") && ($end_date1!= ""))
				{	 
					$selqry ="SELECT * FROM  subscribers  where subscriber_status = $status AND date_subscribed BETWEEN  '$start_date' AND '$end_date'";
				}
				if(($status=="all") && ($start_date1!= "") && ($end_date1!= ""))
				{	 
					$selqry ="SELECT * FROM  subscribers  where date_subscribed BETWEEN  '$start_date' AND '$end_date'";
				}

				$result =$this->db->query("$selqry");
				ob_end_clean();
				$data = $this->dbutil->csv_from_result($result, $delimiter, $newline, $enclosure);
				force_download($filename, $data);
				
				/*if($result->num_rows > 0)
				{		
					return $result->result();
				}*/			 
		}	
		//END

		//New code for userdetails menu 6-5-16//
		//User details Menu //
		if($type == 'users')
		{
        	
        	$acc_status    = $this->input->post('acc_status');
        	$category_type = $this->input->post('category_type');
        	$login_app     = $this->input->post('login_app');
        	$bal_status    = $this->input->post('bal_status');
        	$news_status   = $this->input->post('news_status');
        	$ref_status    = $this->input->post('ref_status');
        	$bank_status   = $this->input->post('bank_status');


        	//echo "<pre>";print_r($_POST); exit;
        	/*Account status details 6-5-16*/
        	if(isset($acc_status))
        	{
        		if($acc_status == "All")
        		{
        			$new_acc_status = "";
        		}
        		else
        		{
        			$new_acc_status = " `status` = $acc_status";	
        		}
        	}
        	
        	/*Caetgory type details 6-5-16*/
        	if(isset($category_type))
        	{
        		if($category_type=="All")
        		{
        			$new_category_type = "";
        		}
        		else
        		{
        			if($new_acc_status == '')
        			{
        				$new_category_type = "`referral_category_type` = $category_type";	
        			}
        			else
        			{
        				$new_category_type = "AND `referral_category_type` = $category_type";	
        			}
        				
        		}
        	}

        	/*App login details 6-5-16*/
        	if(isset($login_app))
        	{
        		if($login_app=="All")
        		{
        			$new_login_app = "";
        		}
        		else
        		{
        			if(($new_acc_status == '') && ($new_category_type == ''))
        			{
        				$new_login_app = "`app_login` = $login_app";	
        			}
        			else
        			{
        				$new_login_app = "AND `app_login` = $login_app";	
        			}
        				
        		}
        	}

        	/*User account balance details 6-5-16*/
        	if(isset($bal_status))
        	{
        		if($bal_status=="All")
        		{
        			$new_bal_status = "";
        		}
        		 
        		else
        		{


        			if(($new_acc_status == '') && ($new_category_type == '') && ($new_login_app == ''))
        			{
        				if($bal_status=="2")
        				{
        					$new_bal_status = "`balance` != 0";
        				}
        				else
        				{ 
        					
        					$new_bal_status = "`balance` = $bal_status";
							}
        			}
        			else
        			{	
        				if($bal_status=="2")
        				{
        					$new_bal_status = "AND `balance` != 0";
        				}
        				else
        				{
        					$new_bal_status = "AND `balance` = $bal_status";
        				}			
        			}
        				
        		}
        	}

        	/*New letter email notification details 6-5-16*/
        	if(isset($news_status))
        	{
        		if($news_status=="All")
        		{
        			$new_news_status = "";
        		}
        		else
        		{
        			if(($new_acc_status == '') && ($new_category_type == '') && ($new_login_app == '') && ($new_bal_status == ''))
        			{
        				$new_news_status = "`newsletter_mail` = $news_status";	
        			}
        			else
        			{
        				$new_news_status = "AND `newsletter_mail` = $news_status";	
        			}
        				
        		}
        	}

        	/*Referral details 6-5-16*/
        	if(isset($ref_status))
        	{
        		if($ref_status=="All")
        		{
        			$new_ref_status = "";
        		}
        		else
        		{
        			if(($new_acc_status == '') && ($new_category_type == '') && ($new_login_app == '') && ($new_bal_status == '') && ($new_news_status == ''))
        			{
        				if($ref_status == '1')
        				{
        					$new_ref_status = "`refer`!=0";	
        				}
        				if($ref_status == '0')
        				{
        					$new_ref_status = "`refer`=$ref_status";
        				}	
        					
        			}
        			else
        			{
        				if($ref_status == '1')
        				{
        					$new_ref_status = "AND `refer`!=0";	
        				}
        				if($ref_status == '0')
        				{
        					$new_ref_status = "AND `refer`=$ref_status";
        				}		
        			}
        				
        		}
        	}

        	/*Bank Information details 6-5-16*/
        	if(isset($bank_status))
        	{
        		if($bank_status=="All")
        		{
        			$new_bank_status = "";
        		}
        		else
        		{
        			if(($new_acc_status == '') && ($new_category_type == '') && ($new_login_app == '') && ($new_bal_status == '') && ($new_news_status == '') && ($new_ref_status == ''))
        			{
        				if($bank_status=="1")
        				{
        					$new_bank_status = "`bank_name` !=''";
        				}
        				else
        				{ 
        					
        					$new_bank_status = "`bank_name` = ''";
							}	
        			}
        			else
        			{	
        				if($bank_status=="1")
        				{
        					$new_bank_status = " AND `bank_name` !=''";
        				}
        				else
        				{ 
        					$new_bank_status = "AND `bank_name` = ''";	
        				}
        			}
        				
        		}
        	}

        	if(($new_acc_status == '') && ($new_category_type == '') && ($new_login_app == '') && ($new_bal_status == '') && ($new_news_status == '') && ($new_ref_status == '') && ($new_bank_status == ''))
        	{
        		$selqry ="SELECT `user_id`,`first_name`,`last_name`,`email`,`street`,`streetnumber`,`sex`,`city`,`state`,`country`,`contact_no`,`celular_no`,`zipcode`,`complemento`,`bairro`,`reg_type`,`status`,`random_code`,`account_holder`,`bank_name`,`branch_name`,`account_number`,`ifsc_code`,`refer`,`balance`,`date_added`,`admin_status`,`cashback_mail`,`withdraw_mail`,`referral_mail`,`newsletter_mail`,`acbalance_mail`,`support_tickets`,`bonus_benefit`,`referral_category_type`,`referral_amt`,`app_login`,`profile` FROM tbl_users";
        	}
        	else
        	{
        		$selqry ="SELECT `user_id`,`first_name`,`last_name`,`email`,`street`,`streetnumber`,`sex`,`city`,`state`,`country`,`contact_no`,`celular_no`,`zipcode`,`complemento`,`bairro`,`reg_type`,`status`,`random_code`,`account_holder`,`bank_name`,`branch_name`,`account_number`,`ifsc_code`,`refer`,`balance`,`date_added`,`admin_status`,`cashback_mail`,`withdraw_mail`,`referral_mail`,`newsletter_mail`,`acbalance_mail`,`support_tickets`,`bonus_benefit`,`referral_category_type`,`referral_amt`,`app_login`,`profile` FROM tbl_users  where $new_acc_status $new_category_type $new_login_app $new_bal_status $new_news_status $new_ref_status $new_bank_status";
        	}	
        	
        	if(isset($_POST['userdetail']))
        	{
        		$filename   = "export_useremail-".$t_date.".xls";

        		if(($new_acc_status == '') && ($new_category_type == '') && ($new_login_app == '') && ($new_bal_status == '') && ($new_news_status == '') && ($new_ref_status == '') && ($new_bank_status == ''))
        		{
        			$selqry ="SELECT `email` from tbl_users";
        		}
        		else
        		{
        			$selqry ="SELECT `email` FROM tbl_users  where $new_acc_status $new_category_type $new_login_app $new_bal_status $new_news_status $new_ref_status $new_bank_status";
        		}
        		
        	}


			$result =$this->db->query("$selqry");
			ob_end_clean();
			$data = $this->dbutil->csv_from_result($result, $delimiter, $newline, $enclosure);
			force_download($filename, $data);				 
		}	
		//END 
		//End user details 6-5-16//	
		
	}

	//END//

	///New code for report update page//

	function update_reports($updatereport,$type)
	{
		 
		$this->db->connection_check();
		$this->load->library('email');
		$coupon_type = '';	
		$this->load->library('CSVReader');	
		$main_url = 'uploads/updatereports/'.$updatereport;	
		$result   =  $this->csvreader->parse_file($main_url);	
		$name = $this->db->query("select * from admin")->row();	
		//$ref_cashbcak_percent =  $name->referral_cashback;	
		if(count($result)!=0)	
		{
			$s =1;		
			if($type == 'pending_cashback')
			{ 
				 
				foreach($result as $res)	
				{	
					$status 		  = $res['status'];	
					$report_update_id = $res['report_update_id'];
				    $newtransaction_id= $res['report_update_id'];

				    $cash_status 	   = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('status');
					$user_id     	   = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('user_id'); 
					$cashback_amount   = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('cashback_amount');;
					$transaction_date  = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('transaction_date');;
		            $txn_id 		   = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('txn_id');;

		            if($newtransaction_id == '')
					{
						$newid             = rand(1000,9999);
						$newtransaction_id = md5($newid);
					}

					$user = $this->view_user($user_id);

					$prev_userbal 	  = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('balance');
					if($prev_userbal == '')
					{
						$prev_userbal = 0;	
					}
					if($status == 'Completed')
					{
						/*New code for status verfification 7-9-16*/
						if($cash_status !='Completed')
						{
							/*End*/
							if($user)
							{
								foreach($user as $single)
								{
									$balance    = $single->balance;
									$user_email = $single->email;
									$first_name = $single->first_name;
									$last_name  = $single->last_name;


									if($first_name == '' && $last_name == '')
									{
										$ex_name   = explode('@', $user_email);
										$user_name = $ex_name[0]; 
									}
									else
									{
										$user_name = $first_name.' '.$last_name;
									}	
								}

								/*Update User table(Balance) and Update cashback table(Approve cashback) */
								$this->db->where('user_id',$user_id);
								$this->db->update('tbl_users',array('balance'=>$balance+$cashback_amount));

								$data = array(	
								'status'=>$status
								);
								$this->db->where('report_update_id',$report_update_id);
								$updation = $this->db->update('cashback',$data);
								/*Update End*/

								/*Add a Cashback credited details into transaction table */
								$ins_data = array('user_id'=>$user_id,'transation_id'=>$txn_id,'transation_amount'=>$cashback_amount,'mode'=>'Credited','transaction_date'=>$transaction_date,'transation_date'=>date('Y-m-d'),'transation_status'=>'Paid','transation_reason'=>'Cashback','report_update_id'=>$newtransaction_id); //$newtransaction_id
								$this->db->insert('transation_details',$ins_data);
								/*End*/

								//Cashback Credited Mail Notification//
							 	$this->db->where('admin_id',1);
								$admin_det = $this->db->get('admin');
								if($admin_det->num_rows >0) 
								{    
									$admin 		 = $admin_det->row();
									$admin_email = $admin->admin_email;
									$site_name 	 = $admin->site_name;
									$admin_no 	 = $admin->contact_number;
									$site_logo 	 = $admin->site_logo;
								}
								
								$date =date('Y-m-d');
								
								if($single->cashback_mail == 1)
								{
									$this->db->where('mail_id',8);
									$mail_template = $this->db->get('tbl_mailtemplates');
									if($mail_template->num_rows >0) 
									{
									   $fetch 		 = $mail_template->row();
									   $subject 	 = $fetch->email_subject;
									   $templete 	 = $fetch->email_template;
									   $url 		 = base_url().'my_earnings/';
									   $unsuburl	 = base_url().'un-subscribe/cashback/'.$user_id;
									   $myaccount    = base_url().'minha-conta';
									   
										$this->load->library('email');
										
										$config = Array(
											'mailtype'  => 'html',
											'charset'   => 'utf-8',
										);
										
										$sub_data = array(
											'###SITENAME###'=>$site_name
										);
										$subject_new = strtr($subject,$sub_data);
										
										// $this->email->initialize($config);
										 $this->email->set_newline("\r\n");
										   $this->email->initialize($config);
										   $this->email->from($admin_email,$site_name.'!');
										   $this->email->to($user_email);
										   $this->email->subject($subject_new);
									   
										$data = array(
											'###NAME###'=>$user_name,
											'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
											'###SITENAME###'=>$site_name,
											'###ADMINNO###'=>$admin_no,
											'###DATE###'=>$date,
											'###AMOUNT###'=>str_replace('.', ',', bcdiv($cashback_amount,1,2)),
											'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
											'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
									    );
									   
									   $content_pop=strtr($templete,$data);
									   $this->email->message($content_pop);
									   $this->email->send();  
									}
								}	
								//mail for pending cashback //


								/*New code for referral mail and user cashback balance details 30-8-16*/
								
								//$user  = $this->view_user($user_id);
								//$newid = rand(1000,9999);
								//$newtransaction_id = md5($newid);
								//if($user)
								//{
									/*foreach($user as $single)
									{
										$balance = $single->balance;
										$user_email = $single->email;
										$user_name = $single->first_name.' '.$single->last_name;	
									}*/

									
									$this->db->where('report_update_id',$report_update_id);
									$cashbacks = $this->db->get('cashback');
									$cashback_data = $cashbacks->row();

									//New hide for referral details 9-9-16
									if($cashback_data->referral!=0)
									{
										$this->db->where('trans_id',$cashback_data->txn_id);
										$txn = $this->db->get('transation_details');
										$txn_detail = $txn->row();

										if($txn_detail)
										{
											$txn_id 	 	   = $txn_detail->trans_id;
											$ref_user_id 	   = $txn_detail->user_id;
											$transation_amount = $txn_detail->transation_amount;
											$refer_user 	   = $this->view_user($ref_user_id);
											
											if($refer_user)
											{
												foreach($refer_user as $single)
												{
													$referral_balance = $single->balance;
													$user_email 	  = $single->email;
													$first_name 	  = $single->first_name;
													$last_name 		  = $single->last_name;

													if($first_name == '' && $last_name == '')
													{
														$ex_name   = explode('@', $user_email);
														$user_name = $ex_name[0]; 
													}
													else
													{
														$user_name = $first_name.' '.$last_name;
													}
												}

												//$date =date('Y-m-d');

												//Update refer user balance details in User table and transaction table(approve referral cashback amount)//
												$this->db->where('user_id',$ref_user_id);
												$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amount));
												 

												$data = array('transation_status'=>'Approved','transation_reason'=>'Referal Payment'); //'transaction_date'=>$date,
												$this->db->where('transation_reason','Pending Referal Payment');
												$this->db->where('trans_id',$txn_id);
												$this->db->update('transation_details',$data);

												/* mail for Approve cashback amt mail notifications */
												if($single->referral_mail == 1)
												{	
													
													$this->db->where('mail_id',9);
													$mail_template = $this->db->get('tbl_mailtemplates');
													if($mail_template->num_rows >0) 
													{
													   $fetch 	  = $mail_template->row();
													   $subject   = $fetch->email_subject;
													   $templete  = $fetch->email_template;
													   $url 	  = base_url().'cashback/my_earnings/';
													   $unsuburls = base_url().'cashback/un-subscribe/referral/'.$ref_user_id;
										   			   $myaccount = base_url().'cashback/minha_conta';
													   
														$this->load->library('email');
														
														$config = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
														);
														
														$sub_data = array(
															'###SITENAME###'=>$site_name
														);
														$subject_new = strtr($subject,$sub_data);
														
														// $this->email->initialize($config);
														 $this->email->set_newline("\r\n");
														   $this->email->initialize($config);
														   $this->email->from($admin_email,$site_name.'!');
														   $this->email->to($user_email);
														   $this->email->subject($subject_new);
													   
														$datas = array(
															'###NAME###'=>$user_name,
															'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
															'###SITENAME###'=>$site_name,
															'###ADMINNO###'=>$admin_no,
															'###DATE###'=>$date,
															'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amount,1,2)),
															'###ULINK###'=>'<a href='.$unsuburls.'>'.$unsuburls.'</a>',
															'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
														);
													   
													   $content_pop=strtr($templete,$datas);
													  //echo $subject_new;  echo $content_pop;exit;
													   $this->email->message($content_pop);
													   $this->email->send();  
													}
												}	
												/* Mail for Approve referral cashback amount mail End*/
											}
										}
									}
									//End 9-9-16

									$this->db->where('admin_id',1);
									$Admin_Details_Query    = $this->db->get('admin');
									$Admin_Details 		    = $Admin_Details_Query->row();
									$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
									$Site_Logo 				= $Admin_Details->site_logo;
									$admin_emailid 			= $Admin_Details->admin_email;

									if($ref_user_id!='')
									{

										$this->db->where('report_update_id',$report_update_id);
										$cashbacks = $this->db->get('cashback');
										$cashback_data = $cashbacks->row();

										if($cashback_data->referral!=0)
										{
											$this->db->where('new_txn_id',$cashback_data->new_txn_id);
											$txn = $this->db->get('transation_details');
											$txn_detail = $txn->row();
											
											if($txn_detail)
											{
												$new_txn_ids = $txn_detail->new_txn_id;
										
												if($single->referral_category_type != '')
												{ 	
													$referrals    	    = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
													$ref_by_percentage  = $referrals->ref_by_percentage;
													$ref_by_rate 		= $referrals->ref_by_rate;
													$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

													//3** Bonus by Refferal Rate type//
													if($bonus_by_ref_rate == 1)
													{
														$n9  = '333445';
														$n12 = $n9 + $ref_user_id;
														$now = date('Y-m-d H:i:s');	
														$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Referal Payment' AND user_id=$ref_user_id"; 
												 		$query=$this->db->query("$selqry");
														$numrows = $query->num_rows();
														if($numrows > 0)
														{
															$fetch = $query->row();
															$usercount = $fetch->userid;
															$referrals      = $this->db->query("select * from referral_settings where ref_id='$single->referral_category_type'")->row();	
															$bonus_amount   = $referrals->ref_cashback_rate_bonus;
															$friends_count  = $referrals->friends_count;
															if($usercount == $friends_count)
															{	
																if($bonus_amount!='')
																{	
																	if($single->referral_category_type == 1)
																	{ 
																	 	$types = 'One';
																	}
																	if($single->referral_category_type == 2)
																	{ 
																	 	$types = 'Two';
																	}
																	if($single->referral_category_type == 3)
																	{ 
																	 	$types = 'Three';
																	}
																	if($single->referral_category_type == 4)
																	{ 
																	 	$types = 'Four';
																	}
																	if($single->referral_category_type == 5)
																	{ 
																	 	$types = 'Five';
																	}
																	if($single->referral_category_type == 6)
																	{ 
																	 	$types = 'Six';
																	}
																	if($single->referral_category_type == 7)
																	{ 
																	 	$types = 'Seven';
																	}
																	if($single->referral_category_type == 8)
																	{ 
																	 	$types = 'Eight';
																	}
																	if($single->referral_category_type == 9)
																	{ 
																	 	$types = 'Nine';
																	}
																	if($single->referral_category_type == 10)
																	{ 
																	 	$types = 'Ten';
																	}

																	$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category '.$types.' User' AND `user_id`=$ref_user_id"; 
													 				$query1=$this->db->query("$selqry");
													 				$newnumrows = $query1->num_rows();
																	if($newnumrows > 0)
																	{
																		$fetch = $query1->row();
																		$users_count = $fetch->userid;
																		if($users_count == 0)	
																		{	
																			$data = array(			
																			'transation_amount' => $bonus_amount,	
																			'user_id' => $ref_user_id,	
																			'transation_date' => $now,
																			'transaction_date' => $now,
																			'transation_id'=>$n12,	
																			'transation_reason' => 'Referral Bonus for Category '.$types.' User',	
																			'mode' => 'Credited',
																			'details_id'=>'',	
																			'table'=>'',	
																			'new_txn_id'=>0,
																			'transation_status ' => 'Approved',
																			'report_update_id'=>$newtransaction_id
																			);	
																			$this->db->insert('transation_details',$data);
																		}	
																	}
																}	
															}
														} 
													}
													//3** Bonus by Refferal Rate type End//

													//1** Refferal by Percentage type Start//
													if($ref_by_percentage == 1)
													{
														$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); /*,'transaction_date'=>$date*/
														$this->db->where('new_txn_id',$new_txn_ids);
														$this->db->update('transation_details',$data);
													}
													//1** Refferal by Percentage type End//	
												}	
											}
										}

										/*New code for withdraw notification 1-4-17*/
										$ref_prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$ref_user_id))->row('balance');
										if($ref_prev_userbal == '')
										{
											$ref_prev_userbal = 0;	
										}


										//$ref_Total_Amount = ($ref_balance+$ref_cashback_amount);
										$ref_Total_Amount = ($referral_balance+$transation_amount);
										$ref_User_details = $this->admin_model->view_user($ref_user_id);
										$ref_us_email 	  = $ref_User_details[0]->email;
										$ref_with_status  = $ref_User_details[0]->withdraw_mail;
										$ref_unsuburl	  = base_url().'un-subscribe/withdraw/'.$ref_user_id;
										$ref_myaccount    = base_url().'resgate';
										$ref_firstname 	  = $ref_User_details[0]->first_name;
										$ref_lastname  	  = $ref_User_details[0]->last_name;

										if($ref_firstname == '' && $ref_lastname == '')
										{
											$ex_name  	  = explode('@', $ref_User_details[0]->email);
											$ref_username = $ex_name[0]; 
										}
										else
										{
											$ref_username = $ref_firstname.''.$ref_lastname;
										}	
										/*End 9-1-17*/
										if($ref_prev_userbal < $Admin_Minimum_Cashback)
										{
											if($ref_with_status == 1)
											{
												if($ref_Total_Amount>=$Admin_Minimum_Cashback)
												{
													$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
													if($obj_temp->num_rows>0)
													{
														$mail_temp  = $obj_temp->row(); 
														$fe_cont    = $mail_temp->email_template;	
														$subject  	= $mail_temp->email_subject;	
														$servername = base_url();
														$nows 		= date('Y-m-d');	
														$this->load->library('email');
														$gd_api=array(
															
															'###NAME###'=>$ref_username,
															'###AMOUNT###'=>str_replace('.', ',', $ref_Total_Amount),
															'###REQUEST_WITHDRAW###'=>str_replace('.', ',', $Admin_Minimum_Cashback),
															'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
															'###ULINK###'=>'<a href='.$ref_unsuburl.'>'.$ref_unsuburl.'</a>',
															'###MYACLINK###'=>'<a href='.$ref_myaccount.'>'.$ref_myaccount.'</a>'
															);
																		   
														$gd_message=strtr($fe_cont,$gd_api);
														$config = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
														);

														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_emailid,$site_name.'!');
														$this->email->to($ref_us_email);
														$this->email->subject($subject);
														$this->email->message($gd_message);
														$this->email->send();
														$this->email->print_debugger();
													}
												}
											}
										}
										/*end 1-4-17*/		
									}

									/*New code for withdraw email notification mail 9-1-17*/
									$this->db->where('admin_id',1);
									$Admin_Details_Query    = $this->db->get('admin');
									$Admin_Details 		    = $Admin_Details_Query->row();
									$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
									$Total_Amount 			= ($balance+$cashback_amount);
									$Site_Logo 				= $Admin_Details->site_logo;
									//echo "userid".$user_id; echo "<br>";
									$User_details 			= $this->admin_model->view_user($user_id);
									$admin_emailid 			= $Admin_Details->admin_email;
									//echo "<pre>"; print_r($User_details); exit;
									$us_email 				= $User_details[0]->email;
									$with_status   			= $User_details[0]->withdraw_mail;
									//$username 			= $User_details[0]->first_name.''.$User_details[0]->last_name;
									$unsuburl	 			= base_url().'un-subscribe/withdraw/'.$user_id;
									$myaccount    			= base_url().'resgate';
									//echo $Total_Amount; echo "<br>";
									//echo  $Admin_Minimum_Cashback;
									
									$firstname = $User_details[0]->first_name;
									$lastname  = $User_details[0]->last_name;

									if($firstname == '' && $lastname == '')
									{
										$ex_name  = explode('@', $User_details[0]->email);
										$username = $ex_name[0]; 
									}
									else
									{
										$username = $firstname.''.$lastname;
									}	
									/*End 9-1-17*/
									
									if($prev_userbal < $Admin_Minimum_Cashback)
									{
										if($with_status == 1)
										{
											if($Total_Amount>=$Admin_Minimum_Cashback)
											{
												$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
												if($obj_temp->num_rows>0)
												{
													$mail_temp  = $obj_temp->row(); 
													$fe_cont    = $mail_temp->email_template;	
													$subject  	= $mail_temp->email_subject;		
													$servername = base_url();
													$nows 		= date('Y-m-d');	
													$this->load->library('email');
													$gd_api=array(
														
														'###NAME###'=>$username,
														'###AMOUNT###'=>str_replace('.', ',', $Total_Amount),
														'###REQUEST_WITHDRAW###'=>str_replace('.', ',', $Admin_Minimum_Cashback),
														'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
														'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
														'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
														);
																	   
													$gd_message=strtr($fe_cont,$gd_api);
													$config = Array(
														'mailtype'  => 'html',
														'charset'   => 'utf-8',
													);
													//echo $admin_emailid; echo "<br>";
													//echo $us_email;exit;
													//echo "<pre>"; print_r($list = array($us_email)); exit;
													$this->email->set_newline("\r\n");
													$this->email->initialize($config);
													$this->email->from($admin_emailid,$site_name.'!');
													$this->email->to($us_email);
													$this->email->subject($subject);
													$this->email->message($gd_message);
													$this->email->send();
													$this->email->print_debugger();
													//echo $user_email; echo "<br>";
												    //echo $gd_message;
												   // exit;
												}
											}
										}	
									}	
									/*End 9-1-17*/
								//}
								/*End 30-8-16*/
							}	
						}	
					}
					
					if($status == 'Pending')
					{	
						
						$user_id      = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('user_id');
						$userdetails  = $this->view_user($user_id);

						$getuser_mail = $userdetails[0]->email;
						$first_name   = $userdetails[0]->first_name;
						$last_name    = $userdetails[0]->last_name;
						
						if($first_name == '' && $last_name == '')
						{
							$user_name  = explode('@',$getuser_mail);
							$username   = $user_name[0];
						}
						else
						{
							$username  = $first_name." ".$last_name;
						}

						$cashback_status = $userdetails[0]->cashback_mail;
						$refer_user  	 = $userdetails[0]->refer;
						
						//$cashback_amount = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('cashback_amount');

						$cashback_amount = $this->db->query("SELECT * from cashback where `report_update_id`='$report_update_id'")->row('cashback_amount');

						
						/*New code for status verfification 7-9-16*/
						if($cash_status !='Pending') 
						{
							
							/*New code for update user balance details 24-3-17*/
							$user_bale 		= $this->view_balance($user_id);
							$newbalnce 		= $user_bale - $cashback_amount;
								
							$data = array(		
							'balance' => $newbalnce);
							$this->db->where('user_id',$user_id);
							$update_qry = $this->db->update('tbl_users',$data);
							/*end 24-3-17*/


							$data = array(	
							'status'=>$status
							);
							$this->db->where('report_update_id',$report_update_id);
							$updation 	 = $this->db->update('cashback',$data);

							$data = array(	
							'transation_status'=>$status
							);
						
							$this->db->where('report_update_id',$report_update_id);
							$updation = $this->db->update('transation_details',$data);

							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name = $admin->site_name;
								$admin_no = $admin->contact_number;
								$site_logo = $admin->site_logo;
							}
							
							$date =date('Y-m-d');
							/* mail for pending cashback */
							if($cashback_status == 1)
							{

								$this->db->where('mail_id',10);
								$mail_template = $this->db->get('tbl_mailtemplates');
								if($mail_template->num_rows >0) 
								{
								   $fetch = $mail_template->row();
								   $subject = $fetch->email_subject;
								   $templete = $fetch->email_template;
								   $unsuburl	 = base_url().'cashback/un-subscribe/cashback/'.$user_id;
								   $myaccount    = base_url().'cashback/minha-conta';
								   // $url = base_url().'cashback/my_earnings/';
								   
									$this->load->library('email');
									
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);
									
									$sub_data = array(
										'###SITENAME###'=>$site_name
									);
									$subject_new = strtr($subject,$sub_data);
									
									// $this->email->initialize($config);
									$this->email->set_newline("\r\n");
								    $this->email->initialize($config);
								    $this->email->from($admin_email,$site_name.'!');
								    $this->email->to($getuser_mail);
								    $this->email->subject($subject_new);
								   
									$data = array(
										'###NAME###'=>$username,
										'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###AMOUNT###'=>str_replace('.', ',', $cashback_amount),
										'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
										'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
								    );
								   
								   $content_pop=strtr($templete,$data);
								   // echo $content_pop; echo $subject_new;
								   $this->email->message($content_pop);
								   $this->email->send();  
								
								}
							}	
							/* mail for pending cashback */
						}	
					}

					if($status == 'Canceled')
					{	
						
						$user_id      = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('user_id');
						$userdetails  = $this->view_user($user_id);

						$getuser_mail = $userdetails[0]->email;
						$first_name   = $userdetails[0]->first_name;
						$last_name    = $userdetails[0]->last_name;
						
						if($first_name == '' && $last_name == '')
						{
							$user_name  = explode('@',$getuser_mail);
							$username   = $user_name[0];
						}
						else
						{
							$username  = $first_name." ".$last_name;
						}

						$cashback_status = $userdetails[0]->cashback_mail;
						$refer_user  	 = $userdetails[0]->refer;

						//$cashback_amount = $this->db->get_where('cashback',array('report_update_id'=>$report_update_id))->row('cashback_amount');

						$cashback_amount = $this->db->query("SELECT * from cashback where `report_update_id`='$report_update_id'")->row('cashback_amount');


						$ref_email       = $this->db->get_where('tbl_users',array('user_id'=>$refer_user))->row('email');
						$reffer_status   = $this->db->get_where('tbl_users',array('user_id'=>$refer_user))->row('referral_mail');
						$ref_first_name  = $this->db->get_where('tbl_users',array('user_id'=>$refer_user))->row('first_name');
						$ref_last_name   = $this->db->get_where('tbl_users',array('user_id'=>$refer_user))->row('last_name');

						$amount          = $this->db->query("SELECT * from `transation_details` where `report_update_id`='$report_update_id'")->row('transation_amount');
						 
						if($ref_first_name == '' && $ref_last_name == '')
						{
							$user_name    = explode('@',$ref_email);
							$ref_username = $user_name[0];
						}
						else
						{
							$ref_username = $ref_first_name." ".$ref_last_name;
						}
						
						/*New code for status verfification 7-9-16*/
						if($cash_status !='Canceled') 
						{
							if($cash_status =='Completed' && $status == 'Canceled')
							{
								/*New code for update user balance details 24-3-17*/
								$user_bale 		= $this->view_balance($user_id);
								$newbalnce 		= $user_bale - $cashback_amount;
									
								$data = array(		
								'balance' => $newbalnce);
								$this->db->where('user_id',$user_id);
								$update_qry = $this->db->update('tbl_users',$data);
								/*end 24-3-17*/
							}

							$data = array(	
							'status'=>$status
							);
							$this->db->where('report_update_id',$report_update_id);
							$updation 	 = $this->db->update('cashback',$data);

							$data = array(	
							'transation_status'=>$status
							);
						
							$this->db->where('report_update_id',$report_update_id);
							$updation = $this->db->update('transation_details',$data);
							
							/* mail for pending cashback */
							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin       = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name 	 = $admin->site_name;
								$admin_no 	 = $admin->contact_number;
								$site_logo 	 = $admin->site_logo;
							}
							
							$date =date('Y-m-d');
							
							if($cashback_status == 1)
							{

								$this->db->where('mail_id',11);
								$mail_template = $this->db->get('tbl_mailtemplates');
								if($mail_template->num_rows >0) 
								{
								   $fetch = $mail_template->row();
								   $subject = $fetch->email_subject;
								   $templete = $fetch->email_template;
								   $unsuburl	 = base_url().'cashback/un-subscribe/cashback/'.$user_id;
								   $myaccount    = base_url().'cashback/minha-conta';
								   //$url 		 = base_url().'cashback/my_earnings/';
								   
									//$this->load->library('email');
									
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);
									
									$sub_data = array(
										'###SITENAME###'=>$site_name
									);
									$subject_new = strtr($subject,$sub_data);
									
									// $this->email->initialize($config);
									 $this->email->set_newline("\r\n");
									   $this->email->initialize($config);
									   $this->email->from($admin_email,$site_name.'!');
									   $this->email->to($getuser_mail);
									   $this->email->subject($subject_new);
								   
									$data = array(
										'###NAME###'=>$username,
										'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###AMOUNT###'=>str_replace('.', ',', $cashback_amount),
										'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
										'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
								    );
								   
								   $content_pop=strtr($templete,$data);
								   // echo $content_pop; echo $subject_new;
								   $this->email->message($content_pop);
								   $this->email->send();  
								
								}
							}	
							/* mail for pending cashback */
							
							/*Mail for cancelled referral amount for reffered user mail 29-8-16*/
							if($refer_user!=0)
							{
								if($updation)
								{
									$unsuburl  = base_url().'un-subscribe/referral/'.$refer_user;
								    $myaccount = base_url().'minha-conta';

									$this->db->where('admin_id',1);
									$admin_det = $this->db->get('admin');
									if($admin_det->num_rows >0) 
									{    
										$admin 		 = $admin_det->row();
										$admin_email = $admin->admin_email;
										$site_name 	 = $admin->site_name;
										$admin_no 	 = $admin->contact_number;
										$site_logo 	 = $admin->site_logo;
									}

									if($reffer_status == 1)
									{
										$this->db->where('mail_id',19);
										$mail_template = $this->db->get('tbl_mailtemplates');
										if($mail_template->num_rows >0) 
										{
										   $fetch = $mail_template->row();
										   $subject = $fetch->email_subject;
										   $templete = $fetch->email_template;
											
											$config = Array(
												'mailtype'  => 'html',
												'charset'   => 'utf-8',
											);
											
											 
											// $this->email->initialize($config);
											$this->email->set_newline("\r\n");
											$this->email->initialize($config);
											$this->email->from($admin_email,$site_name.'!');
											$this->email->to($ref_email);
											$this->email->subject($subject);
										   
											$data = array(
												'###ADMINNO###'=>$admin_no,
												'###EMAIL###'=>$ref_username,
												'###DATE###'=>$date,
												'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
												'###SITENAME###' =>$site_name,
												'###STATUS###'=>$status,
												'###AMOUNT###'=>str_replace('.', ',', $amount),
												'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
												'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
										    );
										   
										   $content_pop=strtr($templete,$data);
										   //echo $content_pop;exit;
										   $this->email->message($content_pop);
										   $this->email->send();  
										}
									}
									
								}
								/*end*/
							}
						}	
					}
				} 
			}	
			if($type == 'missing_cashback')
			{ 	
				//echo "hai";exit;
				foreach($result as $res)	
				{	
					$cashback_id  	  = $res['cashback_id'];		
					$status 		  = $res['status'];
					$ad_info          = $res['additional_information'];
					$nows 			  = date('Y-m-d');
					
					$miss_cash_status = $this->db->get_where('missing_cashback',array('cashback_id'=>$cashback_id))->row('status');

					if($status == 0) //0 Means Success status
					{
						if($miss_cash_status !=0)
						{
							$data = array(
										'status'=>$status,
										'status_update_date'=>date('Y-m-d'),
									);
							$this->db->where('cashback_id',$cashback_id);
							$updation = $this->db->update('missing_cashback',$data);

							if($updation)
							{
								$this->db->where('cashback_id',$cashback_id);
								$osiz_miss_cash = $this->db->get('missing_cashback');
								if($osiz_miss_cash)
								{
									$osiz_miss_cash_destails = $osiz_miss_cash->row();

									$cash_reason = $osiz_miss_cash_destails->missing_reason; 
									$cash_refer  = $osiz_miss_cash_destails->cashback_reference;

									$user_id     = $osiz_miss_cash_destails->user_id;
									$cash_amt    = $osiz_miss_cash_destails->transation_amount;
									$trans_ids   = rand(1000,9999);
									$trans_id    = md5($trans_ids);

									$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('balance');
									if($prev_userbal == '')
									{
										$prev_userbal = 0;	
									}

									$osiz_user_details = $this->view_user($user_id);
									foreach($osiz_user_details as $osiz_new_userdetails)
									{
										$balance    	= $osiz_new_userdetails->balance;
										$user_email 	= $osiz_new_userdetails->email;
										$user_name  	= $osiz_new_userdetails->first_name.' '.$single->last_name;	
										//$cashback_mail  = $osiz_new_userdetails->cashback_mail;
										$cashback_mail  = $osiz_new_userdetails->support_tickets;
									}

									/*New code for missing approval ticket details 31-8-16*/
									if($cash_reason === 'Missing Approval')
									{
										
										$data = array(
											'status'=>'Completed',
											);

										$this->db->where('reference_id',$cash_refer);
										$updation = $this->db->update('cashback',$data);

										$this->db->where('reference_id',$cash_refer);
										$osiz_cash_details = $this->db->get('cashback');

										if($osiz_cash_details)
										{
											$osiz_ref_details = $osiz_cash_details->row();
											$cashback_amount  = $osiz_ref_details->cashback_amount;


											$this->db->where('user_id',$user_id);
											$this->db->update('tbl_users',array('balance'=>$balance+$cashback_amount));
											
											if($cashback_mail !=0)
											{
												/*Mail cofig details 30-8-16*/
												$name 			 = $this->db->query("select * from admin")->row();
												
												$admin_emailid 	 = $name->admin_email;
												$site_logo 		 = $name->site_logo;
												$site_name  	 = $name->site_name;
												$contact_number  = $name->contact_number;
												$current_msg1 	 = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Your missing Approval ticket request has sucessfully Completed. </span>';
												$img_urls 		 = "<img src=".base_url()."uploads/adminpro/".$site_logo.">";

												$mail_temp 	  	 = $this->db->query("select * from tbl_mailtemplates where mail_id='14'")->row();
												$fe_cont 	  	 = $mail_temp->email_template;
												$subject 		 = $mail_temp->email_subject;	
												$nows 		  	 = date('Y-m-d');	
												$statuss      	 = $status;
												$list 	  	  	 = $user_email;
												$see_miss_status = "<a href='".base_url()."extrato'>status da solicitação</a>";
												$unsuburl     	 = base_url().'un-subscribe/missing_approval/'.$user_id;
           										$myaccount    	 = base_url().'minha-conta';

												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
													);
												
												$this->email->initialize($config);
												$this->email->set_newline("\r\n");
												$this->email->from($admin_emailid,$site_name.'!');
												$this->email->to($list);
												$this->email->subject($subject);
												
												$gd_api=array(
												
													'###ADMINNO###'=>$contact_number,
													'###EMAIL###'=>$user_email,
													'###DATE###'=>$nows,
													'###MESSAGE###'=>$current_msg1,
													'###COMPANYLOGO###'=>$img_urls,
													'###SITENAME###' =>$site_name,
													'###MISSING_CASHBACK_STATUS###'=>'Completed',
													'###SEE_STATUS_MISSING###'=>$see_miss_status,
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
                									'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
													);

												$gd_message=strtr($fe_cont,$gd_api);
												//echo $gd_message; exit;
												$this->email->message($gd_message);
												$this->email->send();
												$this->email->print_debugger();
												/*end 30-8-16*/
											}

											/*New code for withdraw email notification mail 9-1-17*/
											$this->db->where('admin_id',1);
											$Admin_Details_Query    = $this->db->get('admin');
											$Admin_Details 		    = $Admin_Details_Query->row();
											$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
											//$Total_Amount 			= ($balance+$cashback_amount);
											$Site_Logo 				= $Admin_Details->site_logo;
											$User_details 			= $this->admin_model->view_user($user_id);
											$admin_emailid 			= $Admin_Details->admin_email;
											$us_email 				= $User_details[0]->email;
											$with_status   			= $User_details[0]->withdraw_mail;
											$unsuburl	 			= base_url().'un-subscribe/withdraw/'.$user_id;
											$myaccount    			= base_url().'resgate';
											$firstname 				= $User_details[0]->first_name;
											$lastname  				= $User_details[0]->last_name;
											$Total_Amount 			= $User_details[0]->balance;

											if($firstname == '' && $lastname == '')
											{
												$ex_name  = explode('@', $User_details[0]->email);
												$username = $ex_name[0]; 
											}
											else
											{
												$username = $firstname.''.$lastname;
											}	
											/*End 9-1-17*/
											
											if($prev_userbal < $Admin_Minimum_Cashback)
											{
												if($with_status == 1)
												{
													if($Total_Amount>=$Admin_Minimum_Cashback)
													{
														$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
														if($obj_temp->num_rows>0)
														{
															$mail_temp  = $obj_temp->row(); 
															$fe_cont    = $mail_temp->email_template;	
															$subject  	= $mail_temp->email_subject;		
															$servername = base_url();
															$nows 		= date('Y-m-d');	
															$this->load->library('email');
															$gd_api=array(
																'###NAME###'=>$username,
																'###AMOUNT###'=>str_replace('.', ',', $Total_Amount),
																'###REQUEST_WITHDRAW###'=>str_replace('.', ',', $Admin_Minimum_Cashback),
																'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
																'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
																);
																			   
															$gd_message=strtr($fe_cont,$gd_api);
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);

															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_emailid,$site_name.'!');
															$this->email->to($us_email);
															$this->email->subject($subject);
															$this->email->message($gd_message);
															$this->email->send();
															$this->email->print_debugger();
														}
													}
												}
											}		
											/*End 9-1-17*/
										}
									}
									/*End 31-8-16*/
									else
									{

										$data = array(
											'user_id' 	        => $user_id,
											'transation_id'	    => $trans_id,
											'transation_reason' => 'Missing Cashback request',
											'cashback_reason' 	=> 'Missing Cashback',
											'transation_amount'	=> $cash_amt,
											'mode'				=> 'debited',
											'transation_date'   => $nows,
											'transaction_date'  => $nows, 
											'transation_status' => 'Approved' 
											);
										
										$this->db->insert('transation_details',$data);
										$this->db->where('user_id',$user_id);
										$this->db->update('tbl_users',array('balance'=>$balance+$cash_amt));

										if($cashback_mail !=0)
										{
											/*Mail config details 30-8-16*/
											$name 			 = $this->db->query("select * from admin")->row();
											//$subject 		 = "Your Missing Cashback Reply";
											$admin_emailid 	 = $name->admin_email;
											$site_logo 		 = $name->site_logo;
											$site_name  	 = $name->site_name;
											$contact_number  = $name->contact_number;
											$current_msg1 	 = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Your missing cashback ticket request has sucessfully Completed. </span>';
											$img_urls 		 = "<img src=".base_url()."uploads/adminpro/".$site_logo.">";


											$mail_temp 	  	 = $this->db->query("select * from tbl_mailtemplates where mail_id='14'")->row();
											$fe_cont 	  	 = $mail_temp->email_template;	
											$subject 		 = $mail_temp->email_subject;
											$nows 		  	 = date('Y-m-d');	
											$statuss      	 = $status;
											$list 	  	  	 = $user_email;
											$see_miss_status = "<a href='".base_url()."loja-cancelou-minha-compra'>status da solicitação</a>";
											$unsuburl     	 = base_url().'un-subscribe/missing_cashback/'.$user_id;
           									$myaccount       = base_url().'minha-conta';

											$config = Array(
												'mailtype'  => 'html',
												'charset'   => 'utf-8',
												);
											
											$this->email->initialize($config);
											$this->email->set_newline("\r\n");
											$this->email->from($admin_emailid,$site_name.'!');
											$this->email->to($list);
											$this->email->subject($subject);
											
											$gd_api=array(
											
												'###ADMINNO###'=>$contact_number,
												'###EMAIL###'=>$user_email,
												'###DATE###'=>$nows,
												'###MESSAGE###'=>$current_msg1,
												'###COMPANYLOGO###'=>$img_urls,
												'###SITENAME###' =>$site_name,
												'###MISSING_CASHBACK_STATUS###'=>'Completed',
												'###SEE_STATUS_MISSING###'=>$see_miss_status,
												'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
                								'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
												);

											$gd_message=strtr($fe_cont,$gd_api);
											//echo $gd_message; exit;
											$this->email->message($gd_message);
											$this->email->send();
											$this->email->print_debugger();
											/*end 30-8-16*/
										}

										/*New code for withdraw email notification mail 9-1-17*/
										$this->db->where('admin_id',1);
										$Admin_Details_Query    = $this->db->get('admin');
										$Admin_Details 		    = $Admin_Details_Query->row();
										$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
										//$Total_Amount 		= ($balance+$cashback_amount);
										$Site_Logo 				= $Admin_Details->site_logo;
										$User_details 			= $this->admin_model->view_user($user_id);
										$admin_emailid 			= $Admin_Details->admin_email;
										$us_email 				= $User_details[0]->email;
										$with_status   			= $User_details[0]->withdraw_mail;
										$unsuburl	 			= base_url().'un-subscribe/withdraw/'.$user_id;
										$myaccount    			= base_url().'resgate';
										$firstname 				= $User_details[0]->first_name;
										$lastname  				= $User_details[0]->last_name;
										$Total_Amount 			= $User_details[0]->balance;

										if($firstname == '' && $lastname == '')
										{
											$ex_name  = explode('@', $User_details[0]->email);
											$username = $ex_name[0]; 
										}
										else
										{
											$username = $firstname.''.$lastname;
										}	
										
										if($prev_userbal < $Admin_Minimum_Cashback)
										{
											if($with_status == 1)
											{
												if($Total_Amount>=$Admin_Minimum_Cashback)
												{
													$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
													if($obj_temp->num_rows>0)
													{
														$mail_temp  = $obj_temp->row(); 
														$fe_cont    = $mail_temp->email_template;	
														$subject  	= $mail_temp->email_subject;		
														$servername = base_url();
														$nows 		= date('Y-m-d');	
														$this->load->library('email');
														$gd_api=array(
															'###NAME###'=>$username,
															'###AMOUNT###'=>str_replace('.', ',', $Total_Amount),
															'###REQUEST_WITHDRAW###'=>str_replace('.', ',', $Admin_Minimum_Cashback),
															'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
															'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
															'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
															);
																		   
														$gd_message=strtr($fe_cont,$gd_api);
														$config = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
														);

														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_emailid,$site_name.'!');
														$this->email->to($us_email);
														$this->email->subject($subject);
														$this->email->message($gd_message);
														$this->email->send();
														$this->email->print_debugger();
													}
												}
											}	
										}	
										/*End 9-1-17*/
									}
								}
							}
						}	
					}	

					if($status == 1) //1 Means Cancelled status
					{
						if($miss_cash_status!=1)
						{	

							$data = array(
								'status'=>$status,
								'current_msg'=>$ad_info,
								'cancel_msg'=>$ad_info,
								'status_update_date'=>date('Y-m-d'),
							);
							$this->db->where('cashback_id',$cashback_id);
							$updation1 = $this->db->update('missing_cashback',$data);

							if($updation1)
							{
								$this->db->where('cashback_id',$cashback_id);
								$osiz_miss_cash = $this->db->get('missing_cashback');
								if($osiz_miss_cash)
								{
									$osiz_miss_cash_destails = $osiz_miss_cash->row();

									$user_id    = $osiz_miss_cash_destails->user_id;
									/*$cash_amt   = $ad_info;
									$trans_ids  = rand(1000,9999);
									$trans_id   = md5($trans_ids);

									$data = array(
										'user_id' 	        => $user_id,
										'transation_id'	    => $trans_id,
										'transation_reason' => 'Missing Cashback request',
										'cashback_reason' 	=> 'Missing Cashback',
										'transation_amount'	=> $cash_amt,
										'mode'				=> 'Credited',
										'transation_date'   => $nows,
										'transaction_date'  => $nows, 
										'transation_status' => 'Canceled' 
										);
										$this->db->insert('transation_details',$data);*/

									$osiz_user_details = $this->view_user($user_id);
									foreach($osiz_user_details as $osiz_new_userdetails)
									{
										$balance    	= $osiz_new_userdetails->balance;
										$user_email 	= $osiz_new_userdetails->email;
										$user_name  	= $osiz_new_userdetails->first_name.' '.$single->last_name;	
										//$cashback_mail  = $osiz_new_userdetails->cashback_mail;
										$cashback_mail  = $osiz_new_userdetails->support_tickets;
									}
									
									//$this->db->where('user_id',$user_id);
									//$this->db->update('tbl_users',array('balance'=>$balance+$cash_amt));

									if($cashback_mail !=0)
									{
										/*Mail cofig details 30-8-16*/
										$name 			 = $this->db->query("select * from admin")->row();
										//$subject 		 = "Your Missing Cashback Reply";
										$admin_emailid 	 = $name->admin_email;
										$site_logo 		 = $name->site_logo;
										$site_name  	 = $name->site_name;
										$contact_number  = $name->contact_number;
										$current_msg1 	 = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Your missing cashback ticket request has Cancelled by Admin. </span>';
										$img_urls 		 = "<img src=".base_url()."uploads/adminpro/".$site_logo.">";


										$mail_temp 	  	 = $this->db->query("select * from tbl_mailtemplates where mail_id='14'")->row();
										$fe_cont 	  	 = $mail_temp->email_template;	
										$subject 		 = $mail_temp->email_subject;
										$nows 		  	 = date('Y-m-d');	
										$statuss      	 = $status;
										$list 	  	  	 = $user_email;
										$see_miss_status = "<a href='".base_url()."loja-cancelou-minha-compra'>status da solicitação</a>";
										$unsuburl     	 = base_url().'un-subscribe/missing_cashabck/'.$user_id;
           								$myaccount    	 = base_url().'minha-conta';

										$config = Array(
											'mailtype'  => 'html',
											'charset'   => 'utf-8',
											);
										
										$this->email->initialize($config);
										$this->email->set_newline("\r\n");
										$this->email->from($admin_emailid,$site_name.'!');
										$this->email->to($list);
										$this->email->subject($subject);
										
										$gd_api=array(
										
											'###ADMINNO###'=>$contact_number,
											'###EMAIL###'=>$user_email,
											'###DATE###'=>$nows,
											'###MESSAGE###'=>$current_msg1,
											'###COMPANYLOGO###'=>$img_urls,
											'###SITENAME###' =>$site_name,
											'###MISSING_CASHBACK_STATUS###'=>'Cancelled',
											'###SEE_STATUS_MISSING###'=>$see_miss_status,
											'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
                							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
											);

										$gd_message=strtr($fe_cont,$gd_api);
										//echo $gd_message; exit;
										$this->email->message($gd_message);
										$this->email->send();
										$this->email->print_debugger();
										/*end 30-8-16*/
									}
								}
							}
						}	
					}

					if($status == 2) //2 Means Send to retailer status
					{
						if($miss_cash_status !=2)
						{
							$data = array(
								'status'=>$status,
								'status_update_date'=>date('Y-m-d'),
							);
							$this->db->where('cashback_id',$cashback_id);
							$updation2 = $this->db->update('missing_cashback',$data);
						 
							if($updation2)
							{
								
								$this->db->where('cashback_id',$cashback_id);
								$osiz_miss_cash = $this->db->get('missing_cashback');
								if($osiz_miss_cash)
								{
									$osiz_miss_cash_destails = $osiz_miss_cash->row();

									$user_id    = $osiz_miss_cash_destails->user_id;

									$osiz_user_details = $this->view_user($user_id);
									foreach($osiz_user_details as $osiz_new_userdetails)
									{
										$balance    	= $osiz_new_userdetails->balance;
										$user_email 	= $osiz_new_userdetails->email;
										$user_name  	= $osiz_new_userdetails->first_name.' '.$single->last_name;	
										//$cashback_mail  = $osiz_new_userdetails->cashback_mail;
										$cashback_mail  = $osiz_new_userdetails->support_tickets;
									}

									if($cashback_mail !=0)
									{
										/*Mail cofig details 30-8-16*/
										$name 			 = $this->db->query("select * from admin")->row();
										//$subject 		 = "Your Missing Cashback Reply";
										$admin_emailid 	 = $name->admin_email;
										$site_logo 		 = $name->site_logo;
										$site_name  	 = $name->site_name;
										$contact_number  = $name->contact_number;
										$current_msg1 	 = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Your missing cashback ticket request has Successfully Send to Retailer. </span>';
										$img_urls 		 = "<img src=".base_url()."uploads/adminpro/".$site_logo.">";
										$mail_temp 	  	 = $this->db->query("select * from tbl_mailtemplates where mail_id='14'")->row();
										$fe_cont 	  	 = $mail_temp->email_template;	
										$subject 		 = $mail_temp->email_subject;
										$nows 		  	 = date('Y-m-d');	
										$statuss      	 = $status;
										$list 	  	  	 = $user_email;
										$see_miss_status = "<a href='".base_url()."loja-cancelou-minha-compra'>status da solicitação</a>";
										$unsuburl  		 = base_url().'un-subscribe/missing_cashabck/'.$user_id;
           								$myaccount    	 = base_url().'minha-conta';

										$config = Array(
											'mailtype'  => 'html',
											'charset'   => 'utf-8',
										);
										
										$this->email->initialize($config);
										$this->email->set_newline("\r\n");
										$this->email->from($admin_emailid,$site_name.'!');
										$this->email->to($list);
										$this->email->subject($subject);
										
										$gd_api=array(
										
											'###ADMINNO###'=>$contact_number,
											'###EMAIL###'=>$user_email,
											'###DATE###'=>$nows,
											'###MESSAGE###'=>$current_msg1,
											'###COMPANYLOGO###'=>$img_urls,
											'###SITENAME###' =>$site_name,
											'###MISSING_CASHBACK_STATUS###'=>'Send to retailer',
											'###SEE_STATUS_MISSING###'=>$see_miss_status,
											'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
                							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
											);

										$gd_message=strtr($fe_cont,$gd_api);
										//echo $gd_message; exit;
										$this->email->message($gd_message);
										$this->email->send();
										$this->email->print_debugger();
										/*end 30-8-16*/
									}
								}	
							}
						}	
					}

					if($status == 4) //4 Means Complete Ticket Only status
					{
						if($miss_cash_status !=0)
						{
							$data = array(
										'status'=>$status,
										'status_update_date'=>date('Y-m-d'),
									);
							$this->db->where('cashback_id',$cashback_id);
							$updation = $this->db->update('missing_cashback',$data);

							if($updation)
							{
								$this->db->where('cashback_id',$cashback_id);
								$osiz_miss_cash = $this->db->get('missing_cashback');
								if($osiz_miss_cash)
								{
									$osiz_miss_cash_destails = $osiz_miss_cash->row();

									$cash_reason = $osiz_miss_cash_destails->missing_reason; 
									$cash_refer  = $osiz_miss_cash_destails->cashback_reference;

									$user_id     = $osiz_miss_cash_destails->user_id;
									$cash_amt    = $osiz_miss_cash_destails->transation_amount;
									$trans_ids   = rand(1000,9999);
									$trans_id    = md5($trans_ids);

									$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('balance');
									if($prev_userbal == '')
									{
										$prev_userbal = 0;	
									}

									$osiz_user_details = $this->view_user($user_id);
									foreach($osiz_user_details as $osiz_new_userdetails)
									{
										$balance    	= $osiz_new_userdetails->balance;
										$user_email 	= $osiz_new_userdetails->email;
										$user_name  	= $osiz_new_userdetails->first_name.' '.$single->last_name;	
										//$cashback_mail  = $osiz_new_userdetails->cashback_mail;
										$cashback_mail  = $osiz_new_userdetails->support_tickets;
									}

									/*New code for missing approval ticket details 31-8-16*/
									if($cash_reason === 'Missing Approval')
									{
										
										$data = array(
											'status'=>'Completed',
											);

										$this->db->where('reference_id',$cash_refer);
										$updation = $this->db->update('cashback',$data);

										$this->db->where('reference_id',$cash_refer);
										$osiz_cash_details = $this->db->get('cashback');

										if($osiz_cash_details)
										{
											$osiz_ref_details = $osiz_cash_details->row();
											$cashback_amount  = $osiz_ref_details->cashback_amount;


											/*$this->db->where('user_id',$user_id);
											$this->db->update('tbl_users',array('balance'=>$balance+$cashback_amount));*/
											
											if($cashback_mail !=0)
											{
												/*Mail cofig details 30-8-16*/
												$name 			 = $this->db->query("select * from admin")->row();
												
												$admin_emailid 	 = $name->admin_email;
												$site_logo 		 = $name->site_logo;
												$site_name  	 = $name->site_name;
												$contact_number  = $name->contact_number;
												$current_msg1 	 = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Your missing Approval ticket request has sucessfully Completed. </span>';
												$img_urls 		 = "<img src=".base_url()."uploads/adminpro/".$site_logo.">";

												$mail_temp 	  	 = $this->db->query("select * from tbl_mailtemplates where mail_id='14'")->row();
												$fe_cont 	  	 = $mail_temp->email_template;
												$subject 		 = $mail_temp->email_subject;	
												$nows 		  	 = date('Y-m-d');	
												$statuss      	 = $status;
												$list 	  	  	 = $user_email;
												$see_miss_status = "<a href='".base_url()."extrato'>status da solicitação</a>";
												$unsuburl     	 = base_url().'un-subscribe/missing_approval/'.$user_id;
           										$myaccount    	 = base_url().'minha-conta';

												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
													);
												
												$this->email->initialize($config);
												$this->email->set_newline("\r\n");
												$this->email->from($admin_emailid,$site_name.'!');
												$this->email->to($list);
												$this->email->subject($subject);
												
												$gd_api=array(
												
													'###ADMINNO###'=>$contact_number,
													'###EMAIL###'=>$user_email,
													'###DATE###'=>$nows,
													'###MESSAGE###'=>$current_msg1,
													'###COMPANYLOGO###'=>$img_urls,
													'###SITENAME###' =>$site_name,
													'###MISSING_CASHBACK_STATUS###'=>'Complete Ticket only',
													'###SEE_STATUS_MISSING###'=>$see_miss_status,
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
                									'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
													);

												$gd_message=strtr($fe_cont,$gd_api);
												//echo $gd_message; exit;
												$this->email->message($gd_message);
												$this->email->send();
												$this->email->print_debugger();
												/*end 30-8-16*/
											}

											/*New code for withdraw email notification mail 9-1-17*/
											$this->db->where('admin_id',1);
											$Admin_Details_Query    = $this->db->get('admin');
											$Admin_Details 		    = $Admin_Details_Query->row();
											$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
											//$Total_Amount 			= ($balance+$cashback_amount);
											$Site_Logo 				= $Admin_Details->site_logo;
											$User_details 			= $this->admin_model->view_user($user_id);
											$admin_emailid 			= $Admin_Details->admin_email;
											$us_email 				= $User_details[0]->email;
											$with_status   			= $User_details[0]->withdraw_mail;
											$unsuburl	 			= base_url().'un-subscribe/withdraw/'.$user_id;
											$myaccount    			= base_url().'resgate';
											$firstname 				= $User_details[0]->first_name;
											$lastname  				= $User_details[0]->last_name;
											$Total_Amount 			= $User_details[0]->balance;

											if($firstname == '' && $lastname == '')
											{
												$ex_name  = explode('@', $User_details[0]->email);
												$username = $ex_name[0]; 
											}
											else
											{
												$username = $firstname.''.$lastname;
											}	
											/*End 9-1-17*/
											
											if($prev_userbal < $Admin_Minimum_Cashback)
											{
												if($with_status == 1)
												{
													if($Total_Amount>=$Admin_Minimum_Cashback)
													{
														$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
														if($obj_temp->num_rows>0)
														{
															$mail_temp  = $obj_temp->row(); 
															$fe_cont    = $mail_temp->email_template;	
															$subject  	= $mail_temp->email_subject;		
															$servername = base_url();
															$nows 		= date('Y-m-d');	
															$this->load->library('email');
															$gd_api=array(
																'###NAME###'=>$username,
																'###AMOUNT###'=>str_replace('.', ',', $Total_Amount),
																'###REQUEST_WITHDRAW###'=>str_replace('.', ',', $Admin_Minimum_Cashback),
																'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
																'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
																'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
																);
																			   
															$gd_message=strtr($fe_cont,$gd_api);
															$config = Array(
																'mailtype'  => 'html',
																'charset'   => 'utf-8',
															);

															$this->email->set_newline("\r\n");
															$this->email->initialize($config);
															$this->email->from($admin_emailid,$site_name.'!');
															$this->email->to($us_email);
															$this->email->subject($subject);
															$this->email->message($gd_message);
															$this->email->send();
															$this->email->print_debugger();
														}
													}
												}
											}		
											/*End 9-1-17*/
										}
									}
									/*End 31-8-16*/
									else
									{

										$data = array(
											'user_id' 	        => $user_id,
											'transation_id'	    => $trans_id,
											'transation_reason' => 'Missing Cashback request',
											'cashback_reason' 	=> 'Missing Cashback',
											'transation_amount'	=> $cash_amt,
											'mode'				=> 'debited',
											'transation_date'   => $nows,
											'transaction_date'  => $nows, 
											'transation_status' => 'Approved' 
											);
										
										$this->db->insert('transation_details',$data);
										//$this->db->where('user_id',$user_id);
										//$this->db->update('tbl_users',array('balance'=>$balance+$cash_amt));

										if($cashback_mail !=0)
										{
											/*Mail config details 30-8-16*/
											$name 			 = $this->db->query("select * from admin")->row();
											//$subject 		 = "Your Missing Cashback Reply";
											$admin_emailid 	 = $name->admin_email;
											$site_logo 		 = $name->site_logo;
											$site_name  	 = $name->site_name;
											$contact_number  = $name->contact_number;
											$current_msg1 	 = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Your missing cashback ticket request has sucessfully Completed. </span>';
											$img_urls 		 = "<img src=".base_url()."uploads/adminpro/".$site_logo.">";


											$mail_temp 	  	 = $this->db->query("select * from tbl_mailtemplates where mail_id='14'")->row();
											$fe_cont 	  	 = $mail_temp->email_template;	
											$subject 		 = $mail_temp->email_subject;
											$nows 		  	 = date('Y-m-d');	
											$statuss      	 = $status;
											$list 	  	  	 = $user_email;
											$see_miss_status = "<a href='".base_url()."loja-cancelou-minha-compra'>status da solicitação</a>";
											$unsuburl     	 = base_url().'un-subscribe/missing_cashback/'.$user_id;
           									$myaccount       = base_url().'minha-conta';

											$config = Array(
												'mailtype'  => 'html',
												'charset'   => 'utf-8',
												);
											
											$this->email->initialize($config);
											$this->email->set_newline("\r\n");
											$this->email->from($admin_emailid,$site_name.'!');
											$this->email->to($list);
											$this->email->subject($subject);
											
											$gd_api=array(
											
												'###ADMINNO###'=>$contact_number,
												'###EMAIL###'=>$user_email,
												'###DATE###'=>$nows,
												'###MESSAGE###'=>$current_msg1,
												'###COMPANYLOGO###'=>$img_urls,
												'###SITENAME###' =>$site_name,
												'###MISSING_CASHBACK_STATUS###'=>'Complete ticket only',
												'###SEE_STATUS_MISSING###'=>$see_miss_status,
												'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
                								'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
												);

											$gd_message=strtr($fe_cont,$gd_api);
											//echo $gd_message; exit;
											$this->email->message($gd_message);
											$this->email->send();
											$this->email->print_debugger();
											/*end 30-8-16*/
										}

										/*New code for withdraw email notification mail 9-1-17*/
										$this->db->where('admin_id',1);
										$Admin_Details_Query    = $this->db->get('admin');
										$Admin_Details 		    = $Admin_Details_Query->row();
										$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
										//$Total_Amount 		= ($balance+$cashback_amount);
										$Site_Logo 				= $Admin_Details->site_logo;
										$User_details 			= $this->admin_model->view_user($user_id);
										$admin_emailid 			= $Admin_Details->admin_email;
										$us_email 				= $User_details[0]->email;
										$with_status   			= $User_details[0]->withdraw_mail;
										$unsuburl	 			= base_url().'un-subscribe/withdraw/'.$user_id;
										$myaccount    			= base_url().'resgate';
										$firstname 				= $User_details[0]->first_name;
										$lastname  				= $User_details[0]->last_name;
										$Total_Amount 			= $User_details[0]->balance;

										if($firstname == '' && $lastname == '')
										{
											$ex_name  = explode('@', $User_details[0]->email);
											$username = $ex_name[0]; 
										}
										else
										{
											$username = $firstname.''.$lastname;
										}	
										
										if($prev_userbal < $Admin_Minimum_Cashback)
										{
											if($with_status == 1)
											{
												if($Total_Amount>=$Admin_Minimum_Cashback)
												{
													$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
													if($obj_temp->num_rows>0)
													{
														$mail_temp  = $obj_temp->row(); 
														$fe_cont    = $mail_temp->email_template;	
														$subject  	= $mail_temp->email_subject;		
														$servername = base_url();
														$nows 		= date('Y-m-d');	
														$this->load->library('email');
														$gd_api=array(
															'###NAME###'=>$username,
															'###AMOUNT###'=>str_replace('.', ',', $Total_Amount),
															'###REQUEST_WITHDRAW###'=>str_replace('.', ',', $Admin_Minimum_Cashback),
															'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
															'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
															'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
															);
																		   
														$gd_message=strtr($fe_cont,$gd_api);
														$config = Array(
															'mailtype'  => 'html',
															'charset'   => 'utf-8',
														);

														$this->email->set_newline("\r\n");
														$this->email->initialize($config);
														$this->email->from($admin_emailid,$site_name.'!');
														$this->email->to($us_email);
														$this->email->subject($subject);
														$this->email->message($gd_message);
														$this->email->send();
														$this->email->print_debugger();
													}
												}
											}	
										}	
										/*End 9-1-17*/
									}
								}
							}
						}	
					}	
				}	
			}

			if($type == 'withdraws')
			{ 	
				foreach($result as $res)	
				{	
					/*$user_id 		  = $res['user_id'];	
					$requested_amount = $res['requested_amount'];*/

					$withdraw_id  	  = $res['withdraw_id'];		
					$status 		  = $res['status'];
					$date 			  = date('Y-m-d');
					
					$withdraw_details = $this->db->query("SELECT * from `withdraw` where `withdraw_id`=$withdraw_id")->row();
					$user_id 		  = $withdraw_details->user_id; 
					$withdraw_status  = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('withdraw_mail');
					$with_status      = $withdraw_details->status; 
					$requested_amount = $withdraw_details->requested_amount;
					 
					/*New code for withdraw processed mail response for users start 29-8-16*/
					
					$name 			 = $this->db->query("select * from admin")->row();
					//$subject 		 = "Your Withdraw Ticket Reply";
					$admin_emailid 	 = $name->admin_email;
					$site_logo 		 = $name->site_logo;
					$site_name  	 = $name->site_name;
					$contact_number  = $name->contact_number;
					$DADOS_BANCARIOS = '<a href='.base_url().'>DADOS_BANCARIOS</a>';
					$ulink 			 = base_url().'un-subscribe/withdraw/'.$user_id;


					if($status == "Requested")
					{
						 
						if($with_status == 'Processing')
						{
							$data = array(
							'status'=>$status					
							);
							$this->db->where('withdraw_id',$withdraw_id);
							$updation = $this->db->update('withdraw',$data);

							/*if($withdraw_status !=0)
							{
								$mail_temp 	  = $this->db->query("select * from tbl_mailtemplates where mail_id='15'")->row();
								$fe_cont 	  = $mail_temp->email_template;	
								$subject 	  = $mail_temp->email_subject;
								$servername   = base_url();
								$nows 		  = date('Y-m-d');	
								
								$statuss      = $status;
								//echo "hai";
								$User_details = $this->admin_model->view_user($user_id);
								$us_email 	  = $User_details[0]->email;
								$list 	  	  = $us_email;

								$config = Array(
									'mailtype'  => 'html',
									'charset'   => 'utf-8',
									);
								
								$this->email->initialize($config);
								$this->email->set_newline("\r\n");
								$this->email->from($admin_emailid,$site_name.'!');
								$this->email->to($list);
								$this->email->subject($subject);
								
								$gd_api=array(
								
									'###ADMINNO###'=>$contact_number,
									'###EMAIL###'=>$us_email,
									'###DATE###'=>$nows,
									'###MESSAGE###'=>$current_msg,
									'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
									'###SITENAME###' =>$site_name,
									'###WITHDRAW_STATUS###'=>$statuss,
									'###ULINK###'=>$ulink
									);

								$gd_message=strtr($fe_cont,$gd_api);
								//echo $gd_message; exit;
								$this->email->message($gd_message);
								$this->email->send();
								$this->email->print_debugger();	
							}*/
						}	
					}

					if($status == "Processing")
					{
						$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">
						Só para avisar que já estamos processando o seu resgate. Já já vai pingar dinheiro na sua conta, mas a gente avisa por email assim que o pagamento for realizado.</span>';
						
						if($with_status == 'Requested')
						{
							$data = array(
							'status'=>$status					
							);
							$this->db->where('withdraw_id',$withdraw_id);
							$updation = $this->db->update('withdraw',$data);


							if($withdraw_status !=0)
							{
								$mail_temp 	  = $this->db->query("select * from tbl_mailtemplates where mail_id='15'")->row();
								$fe_cont 	  = $mail_temp->email_template;	
								$subject 	  = $mail_temp->email_subject;
								$servername   = base_url();
								$nows 		  = date('Y-m-d');	
								
								$statuss      = '<a href='.base_url().'resgate>Processando</a>';
								//echo "hai";
								$User_details = $this->admin_model->view_user($user_id);
								$us_email 	  = $User_details[0]->email;
								$list 	  	  = $us_email;

								$config = Array(
									'mailtype'  => 'html',
									'charset'   => 'utf-8',
									);
								
								$this->email->initialize($config);
								$this->email->set_newline("\r\n");
								$this->email->from($admin_emailid,$site_name.'!');
								$this->email->to($list);
								$this->email->subject($subject);
								
								$gd_api=array(
								
									'###ADMINNO###'=>$contact_number,
									'###EMAIL###'=>$us_email,
									'###DATE###'=>$nows,
									'###MESSAGE###'=>$current_msg,
									'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
									'###SITENAME###' =>$site_name,
									'###WITHDRAW_STATUS###'=>$statuss,
									'###ULINK###'=>$ulink
									);

								$gd_message=strtr($fe_cont,$gd_api);
								//echo $gd_message; exit;
								$this->email->message($gd_message);
								$this->email->send();
								$this->email->print_debugger();	
							}
						}	
					}
					
					if($status == "Completed")
					{
						if($with_status !="Completed")
						{
							if($with_status !="Cancelled")
							{
								$data = array('closing_date'=>$date,'status'=>$status);
								$this->db->where('withdraw_id',$withdraw_id);
								$updation1 = $this->db->update('withdraw',$data);

								/*$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> Seu resgate foi realizado com sucesso! Acabou de pingar R$ '.str_replace('.', ',', $requested_amount).' na sua conta bancária. ”
								where '.$requested_amount.' is the amount of the withdraw realized</span>';*/

								$with_statuss = '<a href='.base_url().'resgate>Pagamento realizado</a>';
								$current_msg  = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">
								Seu resgate foi realizado com sucesso! Acabou de pingar R$ '.str_replace('.', ',', $requested_amount).' na sua conta bancária. ”
								</span>';

								if($withdraw_status !=0)
								{
									$mail_temp 	  = $this->db->query("select * from tbl_mailtemplates where mail_id='15'")->row();
									$fe_cont 	  = $mail_temp->email_template;
									$subject 	  = $mail_temp->email_subject;	
									$servername   = base_url();
									$nows 		  = date('Y-m-d');	
									
									$statuss      = $status;
									//echo "hai";
									$User_details = $this->admin_model->view_user($user_id);
									$us_email 	  = $User_details[0]->email;
									$list 	  	  = $us_email;
									$user_name 	  = $User_details[0]->first_name;

									if($user_name == '')
									{
										$username  = explode('@',$us_email);
										$user_name = $username[0];
									}

									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
										);
									
									$this->email->initialize($config);
									$this->email->set_newline("\r\n");
									$this->email->from($admin_emailid,$site_name.'!');
									$this->email->to($list);
									$this->email->subject($subject);
									
									$gd_api=array(
									
										'###ADMINNO###'=>$contact_number,
										'###EMAIL###'=>$user_name,
										'###DATE###'=>$nows,
										'###MESSAGE###'=>$current_msg,
										'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
										'###SITENAME###' =>$site_name,
										'###WITHDRAW_STATUS###'=>$with_statuss,
										'###ULINK###'=>$ulink
										);

									$gd_message=strtr($fe_cont,$gd_api);
									//echo $gd_message; exit;
									$this->email->message($gd_message);
									$this->email->send();
									$this->email->print_debugger();	
								}
							}
						}
					}

					if($status == "Cancelled")
					{
						/*if($with_status !="Cancelled")
						{*/
							if(($with_status =="Processing") || ($with_status =="Requested"))
							{
								$data = array('closing_date'=>$date,'status'=>$status);
								$this->db->where('withdraw_id',$withdraw_id);
								$updation1 = $this->db->update('withdraw',$data);

								//$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Ôpa, tentamos fazer o seu pagamento mas o banco não aceitou a operação porquê os dados estavam errados. O valor do seu resgate foi creditado novamente na sua conta. Pedimos que atualize os '.$DADOS_BANCARIOS.'  e qualquer problema entre em contato com a gente no '.$contact_number.'.</span>';
								
								/*User balance update details 29-8-16 start*/
								$withdraw_id_amount    = $this->get_requested_amount($withdraw_id);
					            $user_id_amount 	   = $this->get_user_amount($user_id);
					            $cancell_update_amount = $user_id_amount + $withdraw_id_amount;

					            $data=array('balance' => $cancell_update_amount);
					            $this->db->where('user_id',$user_id);
							    $updations = $this->db->update('tbl_users',$data);
							    /*End*/ 

							    $with_statuss = '<a href='.base_url().'resgate>Cancelado</a>';
								$contato      = '<a href='.base_url().'contato>Contato</a>';
								$current_msg  = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Ôpa, tentamos fazer o seu pagamento mas o banco não aceitou a operação porquê os dados estavam errados. O valor do seu resgate foi creditado novamente na sua conta. Pedimos que atualize os '.$DADOS_BANCARIOS.'  e qualquer problema entre em '.$contato.' com a gente.</span>';


							    if($withdraw_status !=0)
								{
									$mail_temp 	  = $this->db->query("select * from tbl_mailtemplates where mail_id='15'")->row();
									$fe_cont 	  = $mail_temp->email_template;	
									$subject 	  = $mail_temp->email_subject;
									$servername   = base_url();
									$nows 		  = date('Y-m-d');	
									
									$statuss      = '<a href='.base_url().'resgate>Cancelado</a>';
									//echo "hai";
									$User_details = $this->admin_model->view_user($user_id);
									$us_email 	  = $User_details[0]->email;
									$list 	  	  = $us_email;
									$user_name 	  = $User_details[0]->first_name;

									if($user_name == '')
									{
										$username  = explode('@',$us_email);
										$user_name = $username[0];
									}

												
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
										);
									
									$this->email->initialize($config);
									$this->email->set_newline("\r\n");
									$this->email->from($admin_emailid,$site_name.'!');
									$this->email->to($list);
									$this->email->subject($subject);
									
									$gd_api=array(

										'###ADMINNO###'=>$contact_number,
										'###EMAIL###'=>$user_name,
										'###DATE###'=>$nows,
										'###MESSAGE###'=>$current_msg,
										'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
										'###SITENAME###' =>$site_name,
										'###WITHDRAW_STATUS###'=>$with_statuss,
										'###ULINK###'=>$ulink
										);

									$gd_message=strtr($fe_cont,$gd_api);
									//echo $gd_message; exit;
									$this->email->message($gd_message);
									$this->email->send();
									$this->email->print_debugger();	
								}
							}	
						}
					}
					/*End 26-8-16*/
				//}
				return true;	
			}
			if($type == 'payments')
			{ 	
				foreach($result as $res)	
				{	
					$user_id 		  = $res['user_id'];	
					$trans_id  	  	  = $res['trans_id'];	
					$trans_reason	  = $res['transation_reason'];	
					$trans_status 	  = $res['transation_status'];
					$report_update_id = $res['report_update_id'];
					 	  
					$data = array(
									
									//'trans_id'=>$trans_id,
									'user_id'=>$user_id,
									'transation_reason'=>$trans_reason,
									'transation_status'=>$trans_status
								);

					$this->db->where('report_update_id',$report_update_id);
					//print_r($data); exit;
					$updation = $this->db->update('transation_details',$data);
				}			
			}
			if($type == 'subscribers')
			{ 	
				foreach($result as $res)	
				{	
					$user_email  = $res['user_email'];	
					$news_status = $res['news_status'];	
					
					//tbl_users table update					 	  
					$data = array(
						'newsletter_mail'=>$news_status,
					);

					$this->db->where('email',$user_email);
					$updation = $this->db->update('tbl_users',$data);

					//Subscribers table update
					$data = array(
						'subscriber_status'=>$news_status,
					);

					$this->db->where('subscriber_email',$user_email);
					$updation = $this->db->update('subscribers',$data);					

				}			
			}
			if($type == 'users')
			{ 	
				foreach($result as $res)	
				{	
					$user_email   = $res['user_email'];	
					$ref_category = $res['new_reff_category'];	
					 	  
					$data = array(
						'referral_category_type'=>$ref_category,
					);

					$this->db->where('email',$user_email);
					$updation = $this->db->update('tbl_users',$data);
				}			
			}
			$s++;
		}	
		return true;
	}
	//End//
	function download_free_coupons()
	{
			$this->db->connection_check();
			 $selqry="SELECT * FROM  coupons  order by coupon_id desc";  
			 $result=$this->db->query("$selqry"); 
				if($result->num_rows > 0)
				{		
					return $result->result();
				}
	}
	function cashback_details_cb($cateid)
	{
		$this->db->connection_check();
		$this->db->order_by('cbid ','desc');
		$this->db->where('store_id',$cateid);
			$result = $this->db->get('category_cashback');
			
			if($result->num_rows > 0){
				return $result->result();
			}
			return false;
			
	}
	function cashback_details_byid($cashbackid)
	{
		$this->db->connection_check();
		$this->db->where('cbid',$cashbackid);
			$result = $this->db->get('category_cashback');
			
			if($result->num_rows > 0){
				return $result->row();
			}
			return false;
			
	}
	function get_typehead_citys_list($query)
	{
		$this->db->connection_check();
		$this->db->like('city_name', $query);	
		$query = $this->db->get('citys');
		if($query->num_rows >= 1)
		{
		   return $query->result();
		}
		return false;
	}
	//
	function show_list(){
		$this->db->connection_check();
		$this->db->where('click_id','203');
		$query = $this->db->get('click_history');
		//print_r($query->row());exit;
		return $query->result();
	}
	/*Seetha 24/10/15 */
	function pending_cashback(){
		$this->db->connection_check();
		$this->db->where('status','Pending');
		$this->db->order_by('cashback_id','desc');
		$result = $this->db->get('cashback');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	function approve_cashback($cashback_id)
	{

		$this->db->connection_check();		
		$this->db->where('cashback_id',$cashback_id);
		$cb = $this->db->get('cashback');
		$user_id ='';
		if($cb)
		{
			
			$cb_r 			   = $cb->row();
			$user_id 		   = $cb_r->user_id;
			$cashback_amount   = $cb_r->cashback_amount;
			$transaction_date  = $cb_r->transaction_date;
            $txn_id 		   = $cb_r->txn_id;
            $newtransaction_id = $cb_r->report_update_id;
		

			if($newtransaction_id == '')
			{
				$newid             = rand(1000,9999);
				$newtransaction_id = md5($newid);
			}

			$user 		  = $this->view_user($user_id);
			$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('balance');
			
			if($prev_userbal == '')
			{
				$prev_userbal = 0;	
			}
			if($user)
			{
				foreach($user as $single)
				{
					$balance    = $single->balance;
					$user_email = $single->email;
					$first_name = $single->first_name;
					$last_name  = $single->last_name;

					if($first_name == '' && $last_name == '')
					{
						$ex_name   = explode('@', $user_email);
						$user_name = $ex_name[0]; 
					}
					else
					{
						$user_name = $first_name.' '.$last_name;
					}	
				}
				
				/*Update User table(Balance) and Update cashback table(Approve cashback) */
				$this->db->where('user_id',$user_id);
				$this->db->update('tbl_users',array('balance'=>$balance+$cashback_amount));
				
				//New code hide 17-9-16 ,'report_update_id'=>$newtransaction_id//
				$data = array('status'=>'Completed');   
				$this->db->where('cashback_id',$cashback_id);
				$this->db->update('cashback',$data);
				/*Update End*/

				/*Add a Cashback credited details into transaction table */
				$ins_data = array('user_id'=>$user_id,'transation_id'=>$txn_id,'transation_amount'=>$cashback_amount,'mode'=>'Credited','transaction_date'=>$transaction_date,'transation_date'=>date('Y-m-d'),'transation_status'=>'Paid','transation_reason'=>'Cashback','report_update_id'=>$newtransaction_id); //New hide 17-9-16 ,'report_update_id'=>$newtransaction_id//
				$this->db->insert('transation_details',$ins_data);
				/*End*/
				

				//Cashback Credited Mail Notification//
				$this->db->where('admin_id',1);
				$admin_det = $this->db->get('admin');
				if($admin_det->num_rows >0) 
				{    
					$admin 		 = $admin_det->row();
					$admin_email = $admin->admin_email;
					$site_name 	 = $admin->site_name;
					$admin_no 	 = $admin->contact_number;
					$site_logo 	 = $admin->site_logo;
				}
				
				$date =date('Y-m-d');
				
				if($single->cashback_mail == 1)
				{	

					$this->db->where('mail_id',8);
					$mail_template = $this->db->get('tbl_mailtemplates');
					if($mail_template->num_rows >0) 
					{
					   	$fetch     = $mail_template->row();
					    $subject   = $fetch->email_subject;
					    $templete  = $fetch->email_template;
					    $url 	   = base_url().'my_earnings/';
					    $unsuburl  = base_url().'un-subscribe/cashback/'.$user_id;
					    $myaccount = base_url().'minha-conta';
					   
						$this->load->library('email');
						
						$config = Array(
							'mailtype' => 'html',
							'charset'  => 'utf-8',
						);
						
						$sub_data = array(
							'###SITENAME###'=>$site_name
						);
						$subject_new = strtr($subject,$sub_data);
						
						// $this->email->initialize($config);
						$this->email->set_newline("\r\n");
						$this->email->initialize($config);
						$this->email->from($admin_email, $site_name.'!');
						$this->email->to($user_email);
						$this->email->subject($subject_new);
					   
						$data = array(
							'###NAME###'=>$user_name,
							'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
							'###SITENAME###'=>$site_name,
							'###ADMINNO###'=>$admin_no,
							'###DATE###'=>$date,
							'###AMOUNT###'=>str_replace('.', ',', bcdiv($cashback_amount,1,2)),
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

					    );
					   
					   $content_pop=strtr($templete,$data);
					   $this->email->message($content_pop);
					   $this->email->send();  
					}
				}	
				//End Cashback credited Mail notifications//


				/*approve pending referral cashback amt*/
				$this->db->where('cashback_id',$cashback_id);
				$cashbacks 	   = $this->db->get('cashback');
				$cashback_data = $cashbacks->row();
				if($cashback_data->referral!=0)
				{
					$this->db->where('trans_id',$cashback_data->txn_id);
					$txn 		= $this->db->get('transation_details');
					$txn_detail = $txn->row();
					
					if($txn_detail)
					{
						$txn_id 	 	   = $txn_detail->trans_id;
						$ref_user_id 	   = $txn_detail->user_id;
						$transation_amount = $txn_detail->transation_amount;
						$refer_user 	   = $this->view_user($ref_user_id);
						
						if($refer_user)
						{
							foreach($refer_user as $single)
							{
								$referral_balance = $single->balance;
								$user_email 	  = $single->email;
								$first_name 	  = $single->first_name;
								$last_name 		  = $single->last_name;

								if($first_name == '' && $last_name == '')
								{
									$ex_name   = explode('@', $user_email);
									$user_name = $ex_name[0]; 
								}
								else
								{
									$user_name = $first_name.' '.$last_name;
								}
							}

							//Update refer user balance details in User table and transaction table(approve referral cashback amount)//
							$this->db->where('user_id',$ref_user_id);
							$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amount));
							
							$data = array('transation_status'=>'Approved','transation_reason'=>'Referal Payment'); //'transaction_date'=>$date,
							$this->db->where('transation_reason','Pending Referal Payment');
							$this->db->where('trans_id',$txn_id);
							$this->db->update('transation_details',$data);
							  

							/* mail for Approve cashback amt mail notifications */
							if($single->referral_mail == 1)
							{	
								$this->db->where('mail_id',9);
								$mail_template = $this->db->get('tbl_mailtemplates');
								if($mail_template->num_rows >0) 
								{
								   $fetch 	 = $mail_template->row();
								   $subject  = $fetch->email_subject;
								   $templete = $fetch->email_template;
								   $url 	 = base_url().'my_earnings/';
								   $unsuburl = base_url().'un-subscribe/referral/'.$ref_user_id;
					   			   $myaccount= base_url().'minha-conta';
								   
									$this->load->library('email');
									
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);
									
									$sub_data = array(
										'###SITENAME###'=>$site_name
									);
									$subject_new = strtr($subject,$sub_data);
									
									// $this->email->initialize($config);
									 $this->email->set_newline("\r\n");
									   $this->email->initialize($config);
									   $this->email->from($admin_email,$site_name.'!');
									   $this->email->to($user_email);
									   $this->email->subject($subject_new);
								   
									$data = array(
										'###NAME###'=>$user_name,
										'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amount,1,2)),
										'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
										'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
									);
								   
								   $content_pop=strtr($templete,$data);
								   // echo $subject_new;  echo $content_pop;exit;
								   $this->email->message($content_pop);
								   $this->email->send();  
								}
							}	
							/* Mail for Approve referral cashback amount mail End*/
						}
					}
				}
				
				$this->db->where('admin_id',1);
				$Admin_Details_Query    = $this->db->get('admin');
				$Admin_Details 		    = $Admin_Details_Query->row();
				$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
				$Site_Logo 				= $Admin_Details->site_logo;
				$admin_emailid 			= $Admin_Details->admin_email;

				if($ref_user_id!='')
				{	
					$this->db->where('cashback_id',$cashback_id);
					$cashbacks 	   = $this->db->get('cashback');
					$cashback_data = $cashbacks->row();

					if($cashback_data->referral!=0)
					{
						$this->db->where('new_txn_id',$cashback_data->new_txn_id);
						$txn = $this->db->get('transation_details');
						$txn_detail = $txn->row();
						
						if($txn_detail)
						{
							$new_txn_ids = $txn_detail->new_txn_id;
							if($single->referral_category_type == 1)
							{	
								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryone'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

								//3** Bonus by Refferal Rate type//
								if($bonus_by_ref_rate == 1)
								{
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Referal Payment' AND user_id=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{
										$fetch = $query->row();
										$usercount = $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categoryone'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;
										if($usercount == $friends_count)
										{	
										if($bonus_amount!='')
										{	 
											$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category One User' AND `user_id`=$ref_user_id"; 
							 				$query1=$this->db->query("$selqry");
							 				$newnumrows = $query1->num_rows();
											if($newnumrows > 0)
											{
												$fetch = $query1->row();
												$users_count = $fetch->userid;
												if($users_count == 0)	
												{	
													$data = array(			
													'transation_amount' => $bonus_amount,	
													'user_id' => $ref_user_id,	
													'transation_date' => $now,
													'transaction_date' => $now,
													'transation_id'=>$n12,	
													'transation_reason' => 'Referral Bonus for Category One User',	
													'mode' => 'Credited',
													'details_id'=>'',	
													'table'=>'',	
													'new_txn_id'=>0,
													'transation_status ' => 'Approved',
													'report_update_id'=>$newtransaction_id
													);	
													$this->db->insert('transation_details',$data);
												}	
											}
										}	
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//
								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{

									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}	
							if($single->referral_category_type == 2)
							{	
							 	
								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categorytwo'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

								//3** Bonus by Refferal Rate type//
								if($bonus_by_ref_rate == 1)
								{
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$newtransaction_id   = rand(1000,9999);
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{
										$fetch 			= $query->row(); 
										$usercount 		= $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categorytwo'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;	
										if($usercount == $friends_count)
										{
											if($bonus_amount!='')
											{
												$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category Two User' AND `user_id`=$ref_user_id"; 
								 				$query1=$this->db->query("$selqry");
								 				$newnumrows = $query1->num_rows();
												if($newnumrows > 0)
												{
													$fetch = $query1->row();
													$users_count = $fetch->userid;
													if($users_count == 0)	
													{
														$data = array(			
														'transation_amount' => $bonus_amount,	
														'user_id' => $ref_user_id,	
														'transation_date' => $now,
														'transaction_date' => $now,
														'transation_id'=>$n12,	
														'transation_reason' => 'Referral Bonus for Category Two User',	
														'mode' => 'Credited',
														'details_id'=>'',	
														'table'=>'',	
														'new_txn_id'=>0,
														'transation_status ' => 'Approved',
														'report_update_id'=>$newtransaction_id
														);
														$this->db->insert('transation_details',$data);
													}
												}
											}		
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//
								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{
									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}
							if($single->referral_category_type == 3)
							{	
							 
								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

								//3** Bonus by Refferal Rate type//

								if($bonus_by_ref_rate == 1)
								{
									
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$newtransaction_id   = rand(1000,9999);
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{
										$fetch = $query->row();
										$usercount = $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;
											
										if($usercount == $friends_count)
										{
											if($bonus_amount!='')
											{
												$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category Three User' AND `user_id`=$ref_user_id"; 
								 				$query1=$this->db->query("$selqry");
								 				$newnumrows = $query1->num_rows();
												if($newnumrows > 0)
												{
													$fetch = $query1->row();
													$users_count = $fetch->userid;
													if($users_count == 0)	
													{
														
														$data = array(			
														'transation_amount' => $bonus_amount,	
														'user_id' => $ref_user_id,	
														'transation_date' => $now,
														'transaction_date' => $now,
														'transation_id'=>$n12,	
														'transation_reason' => 'Referral Bonus for Category Three User',	
														'mode' => 'Credited',
														'details_id'=>'',	
														'table'=>'',	
														'new_txn_id'=>0,
														'transation_status ' => 'Approved',
														'report_update_id'=>$newtransaction_id
														);
													 	$this->db->insert('transation_details',$data);
													}
												}
											}		
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//

								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{

									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
									//$this->db->where('transation_reason','Referral Cashback amount');
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}
							if($single->referral_category_type == 4)
							{   
								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryfour'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;
								$bonus_amount   	= $referrals->ref_cashback_rate_bonus;
								$friends_count  	= $referrals->friends_count;

								//3** Bonus by Refferal Rate type//

								if($bonus_by_ref_rate == 1)
								{
									
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$newtransaction_id   = rand(1000,9999);
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referal Payment' AND `user_id`=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{

										$fetch 			= $query->row();
										$usercount 		= $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;

										if($usercount == $friends_count)
										{
											if($bonus_amount!='')
											{
												$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category Four User' AND `user_id`=$ref_user_id"; 
								 				$query1=$this->db->query("$selqry");
								 				$newnumrows = $query1->num_rows();
												if($newnumrows > 0)
												{
													$fetch = $query1->row();
													$users_count = $fetch->userid;
													if($users_count == 0)	
													{
											
														$data = array(			
														'transation_amount' => $bonus_amount,	
														'user_id' => $ref_user_id,	
														'transation_date' => $now,
														'transaction_date' => $now,
														'transation_id'=>$n12,	
														'transation_reason' => 'Referral Bonus for Category Four User',	
														'mode' => 'Credited',
														'details_id'=>'',	
														'table'=>'',	
														'new_txn_id'=>0,
														'transation_status ' => 'Approved',
														'report_update_id'=>$newtransaction_id
														);
												 
														$this->db->insert('transation_details',$data);
													}
												}
											}		
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//

								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{

									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
									//$this->db->where('transation_reason','Referral Cashback amount');
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}
							if($single->referral_category_type == 5)
							{	

								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryfive'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

								//3** Bonus by Refferal Rate type//

								if($bonus_by_ref_rate == 1)
								{
									
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$newtransaction_id   = rand(1000,9999);
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{
										$fetch = $query->row();
										$usercount = $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categoryfive'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;
											
										if($usercount == $friends_count)
										{
											if($bonus_amount!='')
											{
												$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category five User' AND `user_id`=$ref_user_id"; 
								 				$query1=$this->db->query("$selqry");
								 				$newnumrows = $query1->num_rows();
												if($newnumrows > 0)
												{
													$fetch = $query1->row();
													$users_count = $fetch->userid;
													if($users_count == 0)	
													{
													
														$data = array(			
														'transation_amount' => $bonus_amount,	
														'user_id' => $ref_user_id,	
														'transation_date' => $now,
														'transaction_date' => $now,
														'transation_id'=>$n12,	
														'transation_reason' => 'Referral Bonus for Category Five User',	
														'mode' => 'Credited',
														'details_id'=>'',	
														'table'=>'',	
														'new_txn_id'=>0,
														'transation_status ' => 'Approved',
														'report_update_id'=>$newtransaction_id
														);
														$this->db->insert('transation_details',$data);
													}
												}
											}		
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//

								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{

									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
									//$this->db->where('transation_reason','Referral Cashback amount');
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}
							if($single->referral_category_type == 6)
							{	

								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categorysix'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

								//3** Bonus by Refferal Rate type//

								if($bonus_by_ref_rate == 1)
								{
									
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$newtransaction_id   = rand(1000,9999);
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{
										$fetch = $query->row();
										$usercount = $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categorysix'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;
											
										if($usercount == $friends_count)
										{
											if($bonus_amount!='')
											{
												$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category six User' AND `user_id`=$ref_user_id"; 
								 				$query1=$this->db->query("$selqry");
								 				$newnumrows = $query1->num_rows();
												if($newnumrows > 0)
												{
													$fetch = $query1->row();
													$users_count = $fetch->userid;
													if($users_count == 0)	
													{
													
														$data = array(			
														'transation_amount' => $bonus_amount,	
														'user_id' => $ref_user_id,	
														'transation_date' => $now,
														'transaction_date' => $now,
														'transation_id'=>$n12,	
														'transation_reason' => 'Referral Bonus for Category Six User',	
														'mode' => 'Credited',
														'details_id'=>'',	
														'table'=>'',	
														'new_txn_id'=>0,
														'transation_status ' => 'Approved',
														'report_update_id'=>$newtransaction_id
														);
														$this->db->insert('transation_details',$data);
													}
												}
											}		
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//

								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{

									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
									//$this->db->where('transation_reason','Referral Cashback amount');
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}
							if($single->referral_category_type == 7)
							{	

								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryseven'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

								//3** Bonus by Refferal Rate type//

								if($bonus_by_ref_rate == 1)
								{
									
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$newtransaction_id   = rand(1000,9999);
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{
										$fetch = $query->row();
										$usercount = $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categoryseven'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;
											
										if($usercount == $friends_count)
										{
											if($bonus_amount!='')
											{
												$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category seven User' AND `user_id`=$ref_user_id"; 
								 				$query1=$this->db->query("$selqry");
								 				$newnumrows = $query1->num_rows();
												if($newnumrows > 0)
												{
													$fetch = $query1->row();
													$users_count = $fetch->userid;
													if($users_count == 0)	
													{
													
														$data = array(			
														'transation_amount' => $bonus_amount,	
														'user_id' => $ref_user_id,	
														'transation_date' => $now,
														'transaction_date' => $now,
														'transation_id'=>$n12,	
														'transation_reason' => 'Referral Bonus for Category Seven User',	
														'mode' => 'Credited',
														'details_id'=>'',	
														'table'=>'',	
														'new_txn_id'=>0,
														'transation_status ' => 'Approved',
														'report_update_id'=>$newtransaction_id
														);
														$this->db->insert('transation_details',$data);
													}
												}
											}		
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//

								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{

									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
									//$this->db->where('transation_reason','Referral Cashback amount');
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}
							if($single->referral_category_type == 8)
							{	

								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryeight'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

								//3** Bonus by Refferal Rate type//

								if($bonus_by_ref_rate == 1)
								{
									
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$newtransaction_id   = rand(1000,9999);
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{
										$fetch = $query->row();
										$usercount = $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categoryeight'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;
											
										if($usercount == $friends_count)
										{
											if($bonus_amount!='')
											{
												$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category eight User' AND `user_id`=$ref_user_id"; 
								 				$query1=$this->db->query("$selqry");
								 				$newnumrows = $query1->num_rows();
												if($newnumrows > 0)
												{
													$fetch = $query1->row();
													$users_count = $fetch->userid;
													if($users_count == 0)	
													{
													
														$data = array(			
														'transation_amount' => $bonus_amount,	
														'user_id' => $ref_user_id,	
														'transation_date' => $now,
														'transaction_date' => $now,
														'transation_id'=>$n12,	
														'transation_reason' => 'Referral Bonus for Category Eight User',	
														'mode' => 'Credited',
														'details_id'=>'',	
														'table'=>'',	
														'new_txn_id'=>0,
														'transation_status ' => 'Approved',
														'report_update_id'=>$newtransaction_id
														);
														$this->db->insert('transation_details',$data);
													}
												}
											}		
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//

								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{

									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
									//$this->db->where('transation_reason','Referral Cashback amount');
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}
							if($single->referral_category_type == 9)
							{	

								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categorynine'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

								//3** Bonus by Refferal Rate type//

								if($bonus_by_ref_rate == 1)
								{
									
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$newtransaction_id   = rand(1000,9999);
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{
										$fetch = $query->row();
										$usercount = $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categorynine'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;
											
										if($usercount == $friends_count)
										{
											if($bonus_amount!='')
											{
												$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category nine User' AND `user_id`=$ref_user_id"; 
								 				$query1=$this->db->query("$selqry");
								 				$newnumrows = $query1->num_rows();
												if($newnumrows > 0)
												{
													$fetch = $query1->row();
													$users_count = $fetch->userid;
													if($users_count == 0)	
													{
													
														$data = array(			
														'transation_amount' => $bonus_amount,	
														'user_id' => $ref_user_id,	
														'transation_date' => $now,
														'transaction_date' => $now,
														'transation_id'=>$n12,	
														'transation_reason' => 'Referral Bonus for Category Nine User',	
														'mode' => 'Credited',
														'details_id'=>'',	
														'table'=>'',	
														'new_txn_id'=>0,
														'transation_status ' => 'Approved',
														'report_update_id'=>$newtransaction_id
														);
														$this->db->insert('transation_details',$data);
													}
												}
											}		
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//

								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{

									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
									//$this->db->where('transation_reason','Referral Cashback amount');
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}
							if($single->referral_category_type == 10)
							{	

								$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryten'")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

								//3** Bonus by Refferal Rate type//

								if($bonus_by_ref_rate == 1)
								{
									
									$n9  = '333445';
									$n12 = $n9 + $ref_user_id;
									$now = date('Y-m-d H:i:s');	
									$newtransaction_id   = rand(1000,9999);
									$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
							 		$query=$this->db->query("$selqry");
									$numrows = $query->num_rows();
									if($numrows > 0)
									{
										$fetch = $query->row();
										$usercount = $fetch->userid;
										$referrals      = $this->db->query("select * from referral_settings where category_type='categoryten'")->row();	
										$bonus_amount   = $referrals->ref_cashback_rate_bonus;
										$friends_count  = $referrals->friends_count;
											
										if($usercount == $friends_count)
										{
											if($bonus_amount!='')
											{
												$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category ten User' AND `user_id`=$ref_user_id"; 
								 				$query1=$this->db->query("$selqry");
								 				$newnumrows = $query1->num_rows();
												if($newnumrows > 0)
												{
													$fetch = $query1->row();
													$users_count = $fetch->userid;
													if($users_count == 0)	
													{
													
														$data = array(			
														'transation_amount' => $bonus_amount,	
														'user_id' => $ref_user_id,	
														'transation_date' => $now,
														'transaction_date' => $now,
														'transation_id'=>$n12,	
														'transation_reason' => 'Referral Bonus for Category Ten User',	
														'mode' => 'Credited',
														'details_id'=>'',	
														'table'=>'',	
														'new_txn_id'=>0,
														'transation_status ' => 'Approved',
														'report_update_id'=>$newtransaction_id
														);
														$this->db->insert('transation_details',$data);
													}
												}
											}		
										}
									} 
								}
								//3** Bonus by Refferal Rate type End//

								//1** Refferal by Percentage type Start//
								if($ref_by_percentage == 1)
								{

									$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
									//$this->db->where('transation_reason','Referral Cashback amount');
									$this->db->where('new_txn_id',$new_txn_ids);
									$this->db->update('transation_details',$data);
								}
								//1** Refferal by Percentage type End//	
							}
						}
					}

					/*New code for withdraw notification 1-4-17*/
					$ref_prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$ref_user_id))->row('balance');
					if($ref_prev_userbal == '')
					{
						$ref_prev_userbal = 0;	
					}


					//$ref_Total_Amount = ($ref_balance+$ref_cashback_amount);
					$ref_Total_Amount = ($referral_balance+$transation_amount);
					$ref_User_details = $this->admin_model->view_user($ref_user_id);
					$ref_us_email 	  = $ref_User_details[0]->email;
					$ref_with_status  = $ref_User_details[0]->withdraw_mail;
					$ref_unsuburl	  = base_url().'un-subscribe/withdraw/'.$ref_user_id;
					$ref_myaccount    = base_url().'resgate';
					$ref_firstname 	  = $ref_User_details[0]->first_name;
					$ref_lastname  	  = $ref_User_details[0]->last_name;

					if($ref_firstname == '' && $ref_lastname == '')
					{
						$ex_name  	  = explode('@', $ref_User_details[0]->email);
						$ref_username = $ex_name[0]; 
					}
					else
					{
						$ref_username = $ref_firstname.''.$ref_lastname;
					}	
					/*End 9-1-17*/
					if($ref_prev_userbal < $Admin_Minimum_Cashback)
					{
						if($ref_with_status == 1)
						{
							if($ref_Total_Amount>=$Admin_Minimum_Cashback)
							{
								$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
								if($obj_temp->num_rows>0)
								{
									$mail_temp  = $obj_temp->row(); 
									$fe_cont    = $mail_temp->email_template;	
									$subject  	= $mail_temp->email_subject;	
									$servername = base_url();
									$nows 		= date('Y-m-d');	
									$this->load->library('email');
									$gd_api=array(
										
										'###NAME###'=>$ref_username,
										'###AMOUNT###'=>str_replace('.', ',', $ref_Total_Amount),
										'###REQUEST_WITHDRAW###'=>str_replace('.', ',', $Admin_Minimum_Cashback),
										'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
										'###ULINK###'=>'<a href='.$ref_unsuburl.'>'.$ref_unsuburl.'</a>',
										'###MYACLINK###'=>'<a href='.$ref_myaccount.'>'.$ref_myaccount.'</a>'
										);
													   
									$gd_message=strtr($fe_cont,$gd_api);
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);

									$this->email->set_newline("\r\n");
									$this->email->initialize($config);
									$this->email->from($admin_emailid,$site_name.'!');
									$this->email->to($ref_us_email);
									$this->email->subject($subject);
									$this->email->message($gd_message);
									$this->email->send();
									$this->email->print_debugger();
								}
							}
						}
					}
					/*end 1-4-17*/
				}
				
				//Pilaventhiran 04/05/2016 START
				$Total_Amount = ($balance+$cashback_amount);
				$User_details = $this->admin_model->view_user($user_id);
				$us_email 	  = $User_details[0]->email;
				$with_status  = $User_details[0]->withdraw_mail;
				$unsuburl	  = base_url().'un-subscribe/withdraw/'.$user_id;
				$myaccount    = base_url().'resgate';
				$firstname 	  = $User_details[0]->first_name;
				$lastname  	  = $User_details[0]->last_name;

				if($firstname == '' && $lastname == '')
				{
					$ex_name  = explode('@', $User_details[0]->email);
					$username = $ex_name[0]; 
				}
				else
				{
					$username = $firstname.''.$lastname;
				}	
				/*End 9-1-17*/
				if($prev_userbal < $Admin_Minimum_Cashback)
				{
					if($with_status == 1)
					{
						if($Total_Amount>=$Admin_Minimum_Cashback)
						{
							$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
							if($obj_temp->num_rows>0)
							{
								$mail_temp  = $obj_temp->row(); 
								$fe_cont    = $mail_temp->email_template;	
								$subject  	= $mail_temp->email_subject;	
								$servername = base_url();
								$nows 		= date('Y-m-d');	
								$this->load->library('email');
								$gd_api=array(
									
									'###NAME###'=>$username,
									'###AMOUNT###'=>str_replace('.', ',', $Total_Amount),
									'###REQUEST_WITHDRAW###'=>str_replace('.', ',', $Admin_Minimum_Cashback),
									'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
									'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
									'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
									);
												   
								$gd_message=strtr($fe_cont,$gd_api);
								$config = Array(
									'mailtype'  => 'html',
									'charset'   => 'utf-8',
								);

								$this->email->set_newline("\r\n");
								$this->email->initialize($config);
								$this->email->from($admin_emailid,$site_name.'!');
								$this->email->to($us_email);
								$this->email->subject($subject);
								$this->email->message($gd_message);
								$this->email->send();
								$this->email->print_debugger();
							}
						}
					}
				}		
				//Pilaventhiran 04/05/2016 END 
			}
			return true;
		}
		return false;
	}

	//New code for Approve multiple cashback records 6/4/16//

	function approve_multi_cashbacks()
	{
		
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		$a1=0;
		foreach($sort_order as $key=>$val)
		{
			$a1++;
			$cashback_id = $key;	
			$this->db->where('cashback_id',$cashback_id);
			$cb = $this->db->get('cashback');
			//echo "<pre>";print_r($cb);
			$user_id ='';
			if($cb)
			{
				$cb_r 			  	= $cb->row();
				$user_id 		  	= $cb_r->user_id;
				$cashback_amount  	= $cb_r->cashback_amount;
				$transaction_date 	= $cb_r->transaction_date;
	            $txn_id 		  	= $cb_r->txn_id;
				$newtransaction_id  = $cb_r->report_update_id;

				if($newtransaction_id == '')
				{
					$newid             = rand(1000,9999);
					$newtransaction_id = md5($newid);
				}

				$user = $this->view_user($user_id);

				/*New code for report update id details 27-12-16*/
				$report_update_ids  = $cb_r->report_update_id;
				/*End 27-12-16*/

				$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$user_id))->row('balance');
				if($prev_userbal == '')
				{
					$prev_userbal = 0;	
				}
				
				if($user)
				{
					foreach($user as $single)
					{
						$balance 	= $single->balance;
						$user_email = $single->email;
						$first_name = $single->first_name;
						$last_name  = $single->last_name;

						if($first_name == '' && $last_name == '')
						{
							$ex_name   = explode('@', $user_email);
							$user_name = $ex_name[0]; 
						}
						else
						{
							$user_name = $first_name.' '.$last_name;
						}	
					}

					/*Update User table(Balance) and Update cashback table(Approve cashback) */
					$this->db->where('user_id',$user_id);
					$this->db->update('tbl_users',array('balance'=>$balance+$cashback_amount));
					
					$data = array('status'=>'Completed'); 
					$this->db->where('cashback_id',$cashback_id);
					$this->db->update('cashback',$data);

					/*Add a Cashback credited details into transaction table */
					$ins_data = array('user_id'=>$user_id,'transation_id'=>$txn_id,'transation_amount'=>$cashback_amount,'mode'=>'Credited','transation_date'=>date('Y-m-d'),'transation_status'=>'Paid','transation_reason'=>'Cashback','report_update_id'=>$newtransaction_id);
					$this->db->insert('transation_details',$ins_data);
					 
					
					//Cashback Credited Mail Notification//
					$this->db->where('admin_id',1);
					$admin_det = $this->db->get('admin');
					if($admin_det->num_rows >0) 
					{    
						$admin 		 = $admin_det->row();
						$admin_email = $admin->admin_email;
						$site_name   = $admin->site_name;
						$admin_no    = $admin->contact_number;
						$site_logo   = $admin->site_logo;
					}
					
					$date =date('Y-m-d');
					
					//Check a condition for email notification//
					if($single->cashback_mail == 1)
					{	
						$this->db->where('mail_id',8);
						$mail_template = $this->db->get('tbl_mailtemplates');
						if($mail_template->num_rows >0) 
						{
						   $fetch 	  = $mail_template->row();
						   $subject   = $fetch->email_subject;
						   $templete  = $fetch->email_template;
						   $url 	  = base_url().'my_earnings/';
						   $unsuburl  = base_url().'un-subscribe/cashback/'.$user_id;
						   $myaccount = base_url().'minha-conta';
						   
							$this->load->library('email');
							
							$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
							);
							
							$sub_data = array(
								'###SITENAME###'=>$site_name
							);
							$subject_new = strtr($subject,$sub_data);
							
							// $this->email->initialize($config);
							 $this->email->set_newline("\r\n");
							   $this->email->initialize($config);
							   $this->email->from($admin_email,$site_name.'!');
							   $this->email->to($user_email);
							   $this->email->subject($subject_new);
						   
							$data = array(
								'###NAME###'=>$user_name,
								'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
								'###SITENAME###'=>$site_name,
								'###ADMINNO###'=>$admin_no,
								'###DATE###'=>$date,
								'###AMOUNT###'=>str_replace('.', ',', bcdiv($cashback_amount,1,2)),
								'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
								'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

						    );
						   
						   $content_pop=strtr($templete,$data);
						   $this->email->message($content_pop);
						   $this->email->send();  
						}
					}	
					//End Cashback credited Mail notifications//


					/*Code for Approve a pending referral cashback payment 13-10-16*/

					/*approve pending referral */
					$this->db->where('cashback_id',$cashback_id);
					$cashbacks 		= $this->db->get('cashback');
					$cashback_data  = $cashbacks->row();

					if($cashback_data->referral!=0)
					{
						$this->db->where('trans_id',$cashback_data->txn_id);
						$txn 		= $this->db->get('transation_details');
						$txn_detail = $txn->row();

						if($txn_detail)
						{
							$txn_id 		   = $txn_detail->trans_id;
							$ref_user_id 	   = $txn_detail->user_id;
							$transation_amount = $txn_detail->transation_amount;
							$refer_user 	   = $this->view_user($ref_user_id);
							 
							if($refer_user)
							{
								foreach($refer_user as $single)
								{
									$referral_balance = $single->balance;
									$user_email 	  = $single->email;
									$first_name 	  = $single->first_name;
									$last_name 		  = $single->last_name;

									if($first_name == '' && $last_name == '')
									{
										$ex_name   = explode('@', $user_email);
										$user_name = $ex_name[0]; 
									}
									else
									{
										$user_name = $first_name.' '.$last_name;
									}
								}

								//Update refer user balance details in User table and transaction table(approve referral cashback amount)//
								$this->db->where('user_id',$ref_user_id);
								$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amount));
								
								$data = array('transation_status'=>'Approved','transation_reason'=>'Referal Payment'); //'transaction_date'=>$date,
								$this->db->where('transation_reason','Pending Referal Payment');
								$this->db->where('trans_id',$txn_id);
								$this->db->update('transation_details',$data);

								/* mail for Approve cashback amt mail notifications */

								if($single->referral_mail == 1)
								{	
									$this->db->where('mail_id',9);
									$mail_template = $this->db->get('tbl_mailtemplates');
									if($mail_template->num_rows >0) 
									{
									   $fetch 	  = $mail_template->row();
									   $subject   = $fetch->email_subject;
									   $templete  = $fetch->email_template;
									   $url 	  = base_url().'my_earnings/';
									   $unsuburl  = base_url().'un-subscribe/referral/'.$ref_user_id;
						   			   $myaccount = base_url().'minha-conta';
									   
										$this->load->library('email');
										
										$config = Array(
											'mailtype'  => 'html',
											'charset'   => 'utf-8',
										);
										
										$sub_data = array(
											'###SITENAME###'=>$site_name
										);
										$subject_new = strtr($subject,$sub_data);
										
										// $this->email->initialize($config);
										$this->email->set_newline("\r\n");
										$this->email->initialize($config);
										$this->email->from($admin_email,$site_name.'!');
										$this->email->to($user_email);
										$this->email->subject($subject_new);
									   
										$data = array(
											'###NAME###'=>$user_name,
											'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
											'###SITENAME###'=>$site_name,
											'###ADMINNO###'=>$admin_no,
											'###DATE###'=>$date,
											'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amount,1,2)),
											'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
											'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
										);
									   
									   $content_pop=strtr($templete,$data);
									   $this->email->message($content_pop);
									   $this->email->send();  
									}
								}	
								/* Mail for Approve referral cashback amount mail End*/
							}
						}
					}

					$this->db->where('admin_id',1);
					$Admin_Details_Query    = $this->db->get('admin');
					$Admin_Details 		    = $Admin_Details_Query->row();
					$Admin_Minimum_Cashback = $Admin_Details->minimum_cashback;
					$Site_Logo 				= $Admin_Details->site_logo;
					$admin_emailid 			= $Admin_Details->admin_email;

					if($ref_user_id!='')
					{	
						$this->db->where('cashback_id',$cashback_id);
						$cashbacks = $this->db->get('cashback');
						$cashback_data = $cashbacks->row();
						if($cashback_data->referral!=0)
						{
							$this->db->where('new_txn_id',$cashback_data->new_txn_id);
							$txn = $this->db->get('transation_details');
							$txn_detail = $txn->row();
							
							if($txn_detail)
							{
								$new_txn_ids  = $txn_detail->new_txn_id;
								 
								$referrals    	    = $this->db->query("select * from referral_settings where ref_id=$single->referral_category_type")->row();	
								$ref_by_percentage  = $referrals->ref_by_percentage;
								$ref_by_rate 		= $referrals->ref_by_rate;
								$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

								//Old code 7-4-17
								/*if($single->referral_category_type == 1)
								{	
									$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryone'")->row();	
									$ref_by_percentage  = $referrals->ref_by_percentage;
									$ref_by_rate 		= $referrals->ref_by_rate;
									$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

									//1** Refferal by Percentage type Start//
									if($ref_by_percentage == 1)
									{

										$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
										//$this->db->where('transation_reason','Referral Cashback amount');
										$this->db->where('new_txn_id',$new_txn_ids);
										$this->db->update('transation_details',$data);
										$this->db->where('user_id',$ref_user_id);
										$this->db->update('tbl_users',array('balance'=>$referral_balance+$ref_cash_amt));
									}
									//1** Refferal by Percentage type End//	
								}*/	
								//End 7-4-17

								if($single->referral_category_type == 1)
								{	
									$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryone'")->row();	
									$ref_by_percentage  = $referrals->ref_by_percentage;
									$ref_by_rate 		= $referrals->ref_by_rate;
									$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

									//3** Bonus by Refferal Rate type//
									if($bonus_by_ref_rate == 1)
									{
										$n9  = '333445';
										$n12 = $n9 + $ref_user_id;
										$now = date('Y-m-d H:i:s');	
										$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Referal Payment' AND user_id=$ref_user_id"; 
								 		$query=$this->db->query("$selqry");
										$numrows = $query->num_rows();
										if($numrows > 0)
										{
											$fetch = $query->row();
											$usercount = $fetch->userid;
											$referrals      = $this->db->query("select * from referral_settings where category_type='categoryone'")->row();	
											$bonus_amount   = $referrals->ref_cashback_rate_bonus;
											$friends_count  = $referrals->friends_count;
											if($usercount == $friends_count)
											{	
											if($bonus_amount!='')
											{	 
												$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category One User' AND `user_id`=$ref_user_id"; 
								 				$query1=$this->db->query("$selqry");
								 				$newnumrows = $query1->num_rows();
												if($newnumrows > 0)
												{
													$fetch = $query1->row();
													$users_count = $fetch->userid;
													if($users_count == 0)	
													{	
														$data = array(			
														'transation_amount' => $bonus_amount,	
														'user_id' => $ref_user_id,	
														'transation_date' => $now,
														'transaction_date' => $now,
														'transation_id'=>$n12,	
														'transation_reason' => 'Referral Bonus for Category One User',	
														'mode' => 'Credited',
														'details_id'=>'',	
														'table'=>'',	
														'new_txn_id'=>0,
														'transation_status ' => 'Approved',
														'report_update_id'=>$newtransaction_id
														);	
														$this->db->insert('transation_details',$data);
													}	
												}
											}	
											}
										} 
									}
									//3** Bonus by Refferal Rate type End//
									//1** Refferal by Percentage type Start//
									if($ref_by_percentage == 1)
									{

										$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
										$this->db->where('new_txn_id',$new_txn_ids);
										$this->db->update('transation_details',$data);
									}
									//1** Refferal by Percentage type End//	
								}	
								if($single->referral_category_type == 2)
								{	
								 	
									$referrals    	    = $this->db->query("select * from referral_settings where category_type='categorytwo'")->row();	
									$ref_by_percentage  = $referrals->ref_by_percentage;
									$ref_by_rate 		= $referrals->ref_by_rate;
									$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

									//3** Bonus by Refferal Rate type//
									if($bonus_by_ref_rate == 1)
									{
										$n9  = '333445';
										$n12 = $n9 + $ref_user_id;
										$now = date('Y-m-d H:i:s');	
										$newtransaction_id   = rand(1000,9999);
										$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
								 		$query=$this->db->query("$selqry");
										$numrows = $query->num_rows();
										if($numrows > 0)
										{
											$fetch 			= $query->row(); 
											$usercount 		= $fetch->userid;
											$referrals      = $this->db->query("select * from referral_settings where category_type='categorytwo'")->row();	
											$bonus_amount   = $referrals->ref_cashback_rate_bonus;
											$friends_count  = $referrals->friends_count;	
											if($usercount == $friends_count)
											{
												if($bonus_amount!='')
												{
													$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category Two User' AND `user_id`=$ref_user_id"; 
									 				$query1=$this->db->query("$selqry");
									 				$newnumrows = $query1->num_rows();
													if($newnumrows > 0)
													{
														$fetch = $query1->row();
														$users_count = $fetch->userid;
														if($users_count == 0)	
														{
															$data = array(			
															'transation_amount' => $bonus_amount,	
															'user_id' => $ref_user_id,	
															'transation_date' => $now,
															'transaction_date' => $now,
															'transation_id'=>$n12,	
															'transation_reason' => 'Referral Bonus for Category Two User',	
															'mode' => 'Credited',
															'details_id'=>'',	
															'table'=>'',	
															'new_txn_id'=>0,
															'transation_status ' => 'Approved',
															'report_update_id'=>$newtransaction_id
															);
															$this->db->insert('transation_details',$data);
														}
													}
												}		
											}
										} 
									}
									//3** Bonus by Refferal Rate type End//
									//1** Refferal by Percentage type Start//
									if($ref_by_percentage == 1)
									{
										$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
										$this->db->where('new_txn_id',$new_txn_ids);
										$this->db->update('transation_details',$data);
									}
									//1** Refferal by Percentage type End//	
								}
								if($single->referral_category_type == 3)
								{	
								 
									$referrals    	    = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
									$ref_by_percentage  = $referrals->ref_by_percentage;
									$ref_by_rate 		= $referrals->ref_by_rate;
									$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

									//3** Bonus by Refferal Rate type//

									if($bonus_by_ref_rate == 1)
									{
										
										$n9  = '333445';
										$n12 = $n9 + $ref_user_id;
										$now = date('Y-m-d H:i:s');	
										$newtransaction_id   = rand(1000,9999);
										$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
								 		$query=$this->db->query("$selqry");
										$numrows = $query->num_rows();
										if($numrows > 0)
										{
											$fetch = $query->row();
											$usercount = $fetch->userid;
											$referrals      = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
											$bonus_amount   = $referrals->ref_cashback_rate_bonus;
											$friends_count  = $referrals->friends_count;
												
											if($usercount == $friends_count)
											{
												if($bonus_amount!='')
												{
													$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category Three User' AND `user_id`=$ref_user_id"; 
									 				$query1=$this->db->query("$selqry");
									 				$newnumrows = $query1->num_rows();
													if($newnumrows > 0)
													{
														$fetch = $query1->row();
														$users_count = $fetch->userid;
														if($users_count == 0)	
														{
															
															$data = array(			
															'transation_amount' => $bonus_amount,	
															'user_id' => $ref_user_id,	
															'transation_date' => $now,
															'transaction_date' => $now,
															'transation_id'=>$n12,	
															'transation_reason' => 'Referral Bonus for Category Three User',	
															'mode' => 'Credited',
															'details_id'=>'',	
															'table'=>'',	
															'new_txn_id'=>0,
															'transation_status ' => 'Approved',
															'report_update_id'=>$newtransaction_id
															);
														 	$this->db->insert('transation_details',$data);
														}
													}
												}		
											}
										} 
									}
									//3** Bonus by Refferal Rate type End//

									//1** Refferal by Percentage type Start//
									if($ref_by_percentage == 1)
									{

										$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
										//$this->db->where('transation_reason','Referral Cashback amount');
										$this->db->where('new_txn_id',$new_txn_ids);
										$this->db->update('transation_details',$data);
									}
									//1** Refferal by Percentage type End//	
								}
								if($single->referral_category_type == 4)
								{   
									$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryfour'")->row();	
									$ref_by_percentage  = $referrals->ref_by_percentage;
									$ref_by_rate 		= $referrals->ref_by_rate;
									$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;
									$bonus_amount   	= $referrals->ref_cashback_rate_bonus;
									$friends_count  	= $referrals->friends_count;

									//3** Bonus by Refferal Rate type//

									if($bonus_by_ref_rate == 1)
									{
										
										$n9  = '333445';
										$n12 = $n9 + $ref_user_id;
										$now = date('Y-m-d H:i:s');	
										$newtransaction_id   = rand(1000,9999);
										$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referal Payment' AND `user_id`=$ref_user_id"; 
								 		$query=$this->db->query("$selqry");
										$numrows = $query->num_rows();
										if($numrows > 0)
										{

											$fetch 			= $query->row();
											$usercount 		= $fetch->userid;
											$referrals      = $this->db->query("select * from referral_settings where category_type='categorythree'")->row();	
											$bonus_amount   = $referrals->ref_cashback_rate_bonus;
											$friends_count  = $referrals->friends_count;

											if($usercount == $friends_count)
											{
												if($bonus_amount!='')
												{
													$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category Four User' AND `user_id`=$ref_user_id"; 
									 				$query1=$this->db->query("$selqry");
									 				$newnumrows = $query1->num_rows();
													if($newnumrows > 0)
													{
														$fetch = $query1->row();
														$users_count = $fetch->userid;
														if($users_count == 0)	
														{
												
															$data = array(			
															'transation_amount' => $bonus_amount,	
															'user_id' => $ref_user_id,	
															'transation_date' => $now,
															'transaction_date' => $now,
															'transation_id'=>$n12,	
															'transation_reason' => 'Referral Bonus for Category Four User',	
															'mode' => 'Credited',
															'details_id'=>'',	
															'table'=>'',	
															'new_txn_id'=>0,
															'transation_status ' => 'Approved',
															'report_update_id'=>$newtransaction_id
															);
													 
															$this->db->insert('transation_details',$data);
														}
													}
												}		
											}
										} 
									}
									//3** Bonus by Refferal Rate type End//

									//1** Refferal by Percentage type Start//
									if($ref_by_percentage == 1)
									{

										$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
										//$this->db->where('transation_reason','Referral Cashback amount');
										$this->db->where('new_txn_id',$new_txn_ids);
										$this->db->update('transation_details',$data);
									}
									//1** Refferal by Percentage type End//	
								}
								if($single->referral_category_type == 5)
								{	

									$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryfive'")->row();	
									$ref_by_percentage  = $referrals->ref_by_percentage;
									$ref_by_rate 		= $referrals->ref_by_rate;
									$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

									//3** Bonus by Refferal Rate type//

									if($bonus_by_ref_rate == 1)
									{
										
										$n9  = '333445';
										$n12 = $n9 + $ref_user_id;
										$now = date('Y-m-d H:i:s');	
										$newtransaction_id   = rand(1000,9999);
										$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
								 		$query=$this->db->query("$selqry");
										$numrows = $query->num_rows();
										if($numrows > 0)
										{
											$fetch = $query->row();
											$usercount = $fetch->userid;
											$referrals      = $this->db->query("select * from referral_settings where category_type='categoryfive'")->row();	
											$bonus_amount   = $referrals->ref_cashback_rate_bonus;
											$friends_count  = $referrals->friends_count;
												
											if($usercount == $friends_count)
											{
												if($bonus_amount!='')
												{
													$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category five User' AND `user_id`=$ref_user_id"; 
									 				$query1=$this->db->query("$selqry");
									 				$newnumrows = $query1->num_rows();
													if($newnumrows > 0)
													{
														$fetch = $query1->row();
														$users_count = $fetch->userid;
														if($users_count == 0)	
														{
														
															$data = array(			
															'transation_amount' => $bonus_amount,	
															'user_id' => $ref_user_id,	
															'transation_date' => $now,
															'transaction_date' => $now,
															'transation_id'=>$n12,	
															'transation_reason' => 'Referral Bonus for Category Five User',	
															'mode' => 'Credited',
															'details_id'=>'',	
															'table'=>'',	
															'new_txn_id'=>0,
															'transation_status ' => 'Approved',
															'report_update_id'=>$newtransaction_id
															);
															$this->db->insert('transation_details',$data);
														}
													}
												}		
											}
										} 
									}
									//3** Bonus by Refferal Rate type End//

									//1** Refferal by Percentage type Start//
									if($ref_by_percentage == 1)
									{

										$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
										//$this->db->where('transation_reason','Referral Cashback amount');
										$this->db->where('new_txn_id',$new_txn_ids);
										$this->db->update('transation_details',$data);
									}
									//1** Refferal by Percentage type End//	
								}
								if($single->referral_category_type == 6)
								{	

									$referrals    	    = $this->db->query("select * from referral_settings where category_type='categorysix'")->row();	
									$ref_by_percentage  = $referrals->ref_by_percentage;
									$ref_by_rate 		= $referrals->ref_by_rate;
									$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

									//3** Bonus by Refferal Rate type//

									if($bonus_by_ref_rate == 1)
									{
										
										$n9  = '333445';
										$n12 = $n9 + $ref_user_id;
										$now = date('Y-m-d H:i:s');	
										$newtransaction_id   = rand(1000,9999);
										$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
								 		$query=$this->db->query("$selqry");
										$numrows = $query->num_rows();
										if($numrows > 0)
										{
											$fetch = $query->row();
											$usercount = $fetch->userid;
											$referrals      = $this->db->query("select * from referral_settings where category_type='categorysix'")->row();	
											$bonus_amount   = $referrals->ref_cashback_rate_bonus;
											$friends_count  = $referrals->friends_count;
												
											if($usercount == $friends_count)
											{
												if($bonus_amount!='')
												{
													$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category six User' AND `user_id`=$ref_user_id"; 
									 				$query1=$this->db->query("$selqry");
									 				$newnumrows = $query1->num_rows();
													if($newnumrows > 0)
													{
														$fetch = $query1->row();
														$users_count = $fetch->userid;
														if($users_count == 0)	
														{
														
															$data = array(			
															'transation_amount' => $bonus_amount,	
															'user_id' => $ref_user_id,	
															'transation_date' => $now,
															'transaction_date' => $now,
															'transation_id'=>$n12,	
															'transation_reason' => 'Referral Bonus for Category Six User',	
															'mode' => 'Credited',
															'details_id'=>'',	
															'table'=>'',	
															'new_txn_id'=>0,
															'transation_status ' => 'Approved',
															'report_update_id'=>$newtransaction_id
															);
															$this->db->insert('transation_details',$data);
														}
													}
												}		
											}
										} 
									}
									//3** Bonus by Refferal Rate type End//

									//1** Refferal by Percentage type Start//
									if($ref_by_percentage == 1)
									{

										$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
										//$this->db->where('transation_reason','Referral Cashback amount');
										$this->db->where('new_txn_id',$new_txn_ids);
										$this->db->update('transation_details',$data);
									}
									//1** Refferal by Percentage type End//	
								}
								if($single->referral_category_type == 7)
								{	

									$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryseven'")->row();	
									$ref_by_percentage  = $referrals->ref_by_percentage;
									$ref_by_rate 		= $referrals->ref_by_rate;
									$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

									//3** Bonus by Refferal Rate type//

									if($bonus_by_ref_rate == 1)
									{
										
										$n9  = '333445';
										$n12 = $n9 + $ref_user_id;
										$now = date('Y-m-d H:i:s');	
										$newtransaction_id   = rand(1000,9999);
										$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
								 		$query=$this->db->query("$selqry");
										$numrows = $query->num_rows();
										if($numrows > 0)
										{
											$fetch = $query->row();
											$usercount = $fetch->userid;
											$referrals      = $this->db->query("select * from referral_settings where category_type='categoryseven'")->row();	
											$bonus_amount   = $referrals->ref_cashback_rate_bonus;
											$friends_count  = $referrals->friends_count;
												
											if($usercount == $friends_count)
											{
												if($bonus_amount!='')
												{
													$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category seven User' AND `user_id`=$ref_user_id"; 
									 				$query1=$this->db->query("$selqry");
									 				$newnumrows = $query1->num_rows();
													if($newnumrows > 0)
													{
														$fetch = $query1->row();
														$users_count = $fetch->userid;
														if($users_count == 0)	
														{
														
															$data = array(			
															'transation_amount' => $bonus_amount,	
															'user_id' => $ref_user_id,	
															'transation_date' => $now,
															'transaction_date' => $now,
															'transation_id'=>$n12,	
															'transation_reason' => 'Referral Bonus for Category Seven User',	
															'mode' => 'Credited',
															'details_id'=>'',	
															'table'=>'',	
															'new_txn_id'=>0,
															'transation_status ' => 'Approved',
															'report_update_id'=>$newtransaction_id
															);
															$this->db->insert('transation_details',$data);
														}
													}
												}		
											}
										} 
									}
									//3** Bonus by Refferal Rate type End//

									//1** Refferal by Percentage type Start//
									if($ref_by_percentage == 1)
									{

										$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
										//$this->db->where('transation_reason','Referral Cashback amount');
										$this->db->where('new_txn_id',$new_txn_ids);
										$this->db->update('transation_details',$data);
									}
									//1** Refferal by Percentage type End//	
								}
								if($single->referral_category_type == 8)
								{	

									$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryeight'")->row();	
									$ref_by_percentage  = $referrals->ref_by_percentage;
									$ref_by_rate 		= $referrals->ref_by_rate;
									$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

									//3** Bonus by Refferal Rate type//

									if($bonus_by_ref_rate == 1)
									{
										
										$n9  = '333445';
										$n12 = $n9 + $ref_user_id;
										$now = date('Y-m-d H:i:s');	
										$newtransaction_id   = rand(1000,9999);
										$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
								 		$query=$this->db->query("$selqry");
										$numrows = $query->num_rows();
										if($numrows > 0)
										{
											$fetch = $query->row();
											$usercount = $fetch->userid;
											$referrals      = $this->db->query("select * from referral_settings where category_type='categoryeight'")->row();	
											$bonus_amount   = $referrals->ref_cashback_rate_bonus;
											$friends_count  = $referrals->friends_count;
												
											if($usercount == $friends_count)
											{
												if($bonus_amount!='')
												{
													$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category eight User' AND `user_id`=$ref_user_id"; 
									 				$query1=$this->db->query("$selqry");
									 				$newnumrows = $query1->num_rows();
													if($newnumrows > 0)
													{
														$fetch = $query1->row();
														$users_count = $fetch->userid;
														if($users_count == 0)	
														{
														
															$data = array(			
															'transation_amount' => $bonus_amount,	
															'user_id' => $ref_user_id,	
															'transation_date' => $now,
															'transaction_date' => $now,
															'transation_id'=>$n12,	
															'transation_reason' => 'Referral Bonus for Category Eight User',	
															'mode' => 'Credited',
															'details_id'=>'',	
															'table'=>'',	
															'new_txn_id'=>0,
															'transation_status ' => 'Approved',
															'report_update_id'=>$newtransaction_id
															);
															$this->db->insert('transation_details',$data);
														}
													}
												}		
											}
										} 
									}
									//3** Bonus by Refferal Rate type End//

									//1** Refferal by Percentage type Start//
									if($ref_by_percentage == 1)
									{

										$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
										//$this->db->where('transation_reason','Referral Cashback amount');
										$this->db->where('new_txn_id',$new_txn_ids);
										$this->db->update('transation_details',$data);
									}
									//1** Refferal by Percentage type End//	
								}
								if($single->referral_category_type == 9)
								{	

									$referrals    	    = $this->db->query("select * from referral_settings where category_type='categorynine'")->row();	
									$ref_by_percentage  = $referrals->ref_by_percentage;
									$ref_by_rate 		= $referrals->ref_by_rate;
									$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

									//3** Bonus by Refferal Rate type//

									if($bonus_by_ref_rate == 1)
									{
										
										$n9  = '333445';
										$n12 = $n9 + $ref_user_id;
										$now = date('Y-m-d H:i:s');	
										$newtransaction_id   = rand(1000,9999);
										$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
								 		$query=$this->db->query("$selqry");
										$numrows = $query->num_rows();
										if($numrows > 0)
										{
											$fetch = $query->row();
											$usercount = $fetch->userid;
											$referrals      = $this->db->query("select * from referral_settings where category_type='categorynine'")->row();	
											$bonus_amount   = $referrals->ref_cashback_rate_bonus;
											$friends_count  = $referrals->friends_count;
												
											if($usercount == $friends_count)
											{
												if($bonus_amount!='')
												{
													$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category nine User' AND `user_id`=$ref_user_id"; 
									 				$query1=$this->db->query("$selqry");
									 				$newnumrows = $query1->num_rows();
													if($newnumrows > 0)
													{
														$fetch = $query1->row();
														$users_count = $fetch->userid;
														if($users_count == 0)	
														{
														
															$data = array(			
															'transation_amount' => $bonus_amount,	
															'user_id' => $ref_user_id,	
															'transation_date' => $now,
															'transaction_date' => $now,
															'transation_id'=>$n12,	
															'transation_reason' => 'Referral Bonus for Category Nine User',	
															'mode' => 'Credited',
															'details_id'=>'',	
															'table'=>'',	
															'new_txn_id'=>0,
															'transation_status ' => 'Approved',
															'report_update_id'=>$newtransaction_id
															);
															$this->db->insert('transation_details',$data);
														}
													}
												}		
											}
										} 
									}
									//3** Bonus by Refferal Rate type End//

									//1** Refferal by Percentage type Start//
									if($ref_by_percentage == 1)
									{

										$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
										//$this->db->where('transation_reason','Referral Cashback amount');
										$this->db->where('new_txn_id',$new_txn_ids);
										$this->db->update('transation_details',$data);
									}
									//1** Refferal by Percentage type End//	
								}
								if($single->referral_category_type == 10)
								{	

									$referrals    	    = $this->db->query("select * from referral_settings where category_type='categoryten'")->row();	
									$ref_by_percentage  = $referrals->ref_by_percentage;
									$ref_by_rate 		= $referrals->ref_by_rate;
									$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;

									//3** Bonus by Refferal Rate type//

									if($bonus_by_ref_rate == 1)
									{
										
										$n9  = '333445';
										$n12 = $n9 + $ref_user_id;
										$now = date('Y-m-d H:i:s');	
										$newtransaction_id   = rand(1000,9999);
										$selqry="SELECT COUNT(user_id) as userid FROM transation_details where transation_reason='Referal Payment' AND user_id=$ref_user_id"; 
								 		$query=$this->db->query("$selqry");
										$numrows = $query->num_rows();
										if($numrows > 0)
										{
											$fetch = $query->row();
											$usercount = $fetch->userid;
											$referrals      = $this->db->query("select * from referral_settings where category_type='categoryten'")->row();	
											$bonus_amount   = $referrals->ref_cashback_rate_bonus;
											$friends_count  = $referrals->friends_count;
												
											if($usercount == $friends_count)
											{
												if($bonus_amount!='')
												{
													$selqry="SELECT COUNT(user_id) as userid FROM transation_details where `transation_reason`='Referral Bonus for Category ten User' AND `user_id`=$ref_user_id"; 
									 				$query1=$this->db->query("$selqry");
									 				$newnumrows = $query1->num_rows();
													if($newnumrows > 0)
													{
														$fetch = $query1->row();
														$users_count = $fetch->userid;
														if($users_count == 0)	
														{
														
															$data = array(			
															'transation_amount' => $bonus_amount,	
															'user_id' => $ref_user_id,	
															'transation_date' => $now,
															'transaction_date' => $now,
															'transation_id'=>$n12,	
															'transation_reason' => 'Referral Bonus for Category Ten User',	
															'mode' => 'Credited',
															'details_id'=>'',	
															'table'=>'',	
															'new_txn_id'=>0,
															'transation_status ' => 'Approved',
															'report_update_id'=>$newtransaction_id
															);
															$this->db->insert('transation_details',$data);
														}
													}
												}		
											}
										} 
									}
									//3** Bonus by Refferal Rate type End//

									//1** Refferal by Percentage type Start//
									if($ref_by_percentage == 1)
									{

										$data = array('transation_status'=>'Approved','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
										//$this->db->where('transation_reason','Referral Cashback amount');
										$this->db->where('new_txn_id',$new_txn_ids);
										$this->db->update('transation_details',$data);
									}
									//1** Refferal by Percentage type End//	
								}
							}
						}

						/*New code for withdraw notification 1-4-17*/
						$ref_prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$ref_user_id))->row('balance');
						if($ref_prev_userbal == '')
						{
							$ref_prev_userbal = 0;	
						}


						//$ref_Total_Amount = ($ref_balance+$ref_cashback_amount);
						$ref_Total_Amount = ($referral_balance+$transation_amount);
						$ref_User_details = $this->admin_model->view_user($ref_user_id);
						$ref_us_email 	  = $User_details[0]->email;
						$ref_with_status  = $User_details[0]->withdraw_mail;
						$ref_unsuburl	  = base_url().'un-subscribe/withdraw/'.$ref_user_id;
						$ref_myaccount    = base_url().'resgate';
						$ref_firstname 	  = $User_details[0]->first_name;
						$ref_lastname  	  = $User_details[0]->last_name;

						if($ref_firstname == '' && $ref_lastname == '')
						{
							$ex_name  	  = explode('@', $ref_User_details[0]->email);
							$ref_username = $ex_name[0]; 
						}
						else
						{
							$ref_username = $ref_firstname.''.$ref_lastname;
						}	
						/*End 9-1-17*/
						if($ref_prev_userbal < $Admin_Minimum_Cashback)
						{
							if($ref_with_status == 1)
							{
								if($ref_Total_Amount>=$Admin_Minimum_Cashback)
								{
									$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
									if($obj_temp->num_rows>0)
									{
										$mail_temp  = $obj_temp->row(); 
										$fe_cont    = $mail_temp->email_template;	
										$subject  	= $mail_temp->email_subject;	
										$servername = base_url();
										$nows 		= date('Y-m-d');	
										$this->load->library('email');
										$gd_api=array(
											
											'###NAME###'=>$ref_username,
											'###AMOUNT###'=>str_replace('.', ',', $ref_Total_Amount),
											'###REQUEST_WITHDRAW###'=>str_replace('.', ',', $Admin_Minimum_Cashback),
											'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
											'###ULINK###'=>'<a href='.$ref_unsuburl.'>'.$ref_unsuburl.'</a>',
											'###MYACLINK###'=>'<a href='.$ref_myaccount.'>'.$ref_myaccount.'</a>'
											);
														   
										$gd_message=strtr($fe_cont,$gd_api);
										$config = Array(
											'mailtype'  => 'html',
											'charset'   => 'utf-8',
										);

										$this->email->set_newline("\r\n");
										$this->email->initialize($config);
										$this->email->from($admin_emailid,$site_name.'!');
										$this->email->to($ref_us_email);
										$this->email->subject($subject);
										$this->email->message($gd_message);
										$this->email->send();
										$this->email->print_debugger();
									}
								}
							}
						}
						/*end 1-4-17*/		
					}
					/*End*/

					/*New code for withdraw email notification mail 9-1-17*/
					$Total_Amount = ($balance+$cashback_amount);
					$User_details = $this->admin_model->view_user($user_id);
					$us_email 	  = $User_details[0]->email;
					$with_status  = $User_details[0]->withdraw_mail;
					$unsuburl	  = base_url().'un-subscribe/withdraw/'.$user_id;
					$myaccount    = base_url().'resgate';
					$firstname 	  = $User_details[0]->first_name;
					$lastname  	  = $User_details[0]->last_name;

					if($firstname == '' && $lastname == '')
					{
						$ex_name  = explode('@', $User_details[0]->email);
						$username = $ex_name[0]; 
					}
					else
					{
						$username = $firstname.''.$lastname;
					}	
					/*End 9-1-17*/
					
					if($prev_userbal < $Admin_Minimum_Cashback)
					{
						if($with_status == 1)
						{
							if($Total_Amount>=$Admin_Minimum_Cashback)
							{
								$obj_temp = $this->db->query("select * from tbl_mailtemplates where mail_id='16'");
								if($obj_temp->num_rows>0)
								{
									$mail_temp  = $obj_temp->row(); 
									$fe_cont    = $mail_temp->email_template;	
									$subject  	= $mail_temp->email_subject;	
									$servername = base_url();
									$nows 		= date('Y-m-d');	
									$this->load->library('email');
									$gd_api=array(
										
										'###NAME###'=>$username,
										'###AMOUNT###'=>str_replace('.', ',', $Total_Amount),
										'###REQUEST_WITHDRAW###'=>str_replace('.', ',', $Admin_Minimum_Cashback),
										'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$Site_Logo,
										'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
										'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
										);
													   
									$gd_message=strtr($fe_cont,$gd_api);
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);
									//echo $admin_emailid; echo "<br>";
									//echo $us_email;exit;
									//echo "<pre>"; print_r($list = array($us_email)); exit;
									$this->email->set_newline("\r\n");
									$this->email->initialize($config);
									$this->email->from($admin_emailid,$site_name.'!');
									$this->email->to($us_email);
									$this->email->subject($subject);
									$this->email->message($gd_message);
									$this->email->send();
									$this->email->print_debugger();
									//echo $user_email; echo "<br>";
								    //echo $gd_message;
								   // exit;
								}
							}
						}
					}		
					/*end 9-1-17*/
				}	 
			}
			//return true;
			//End//
		}

		if($a1!=0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//End//




	function cancel_cashback($cashback_id)
	{
		
		$this->db->connection_check();		
		$this->db->where('cashback_id',$cashback_id);
		$cb = $this->db->get('cashback');
		$user_id ='';
		if($cb)
		{
			$cb_r    		   = $cb->row();
			$user_id 		   = $cb_r->user_id;
			$cashback_amount   = $cb_r->cashback_amount;
            $transaction_date  = $cb_r->transaction_date;
            $txn_id 		   = $cb_r->txn_id;
            $newtransaction_id = $cb_r->report_update_id;
			
			if($newtransaction_id == '')
			{
				$newid             = rand(1000,9999);
				$newtransaction_id = md5($newid);
			}

		
			$user = $this->view_user($user_id);

			if($user)
			{
				foreach($user as $single)
				{
					$balance    = $single->balance;
					$user_email = $single->email;
					$first_name = $single->first_name;
					$last_name  = $single->last_name;

					if($first_name == '' && $last_name == '')
					{
						$ex_name   = explode('@', $user_email);
						$user_name = $ex_name[0]; 
					}
					else
					{
						$user_name = $first_name.' '.$last_name;
					}
				}

				 
				$data = array('status'=>'Canceled');
				$this->db->where('cashback_id',$cashback_id);
				$this->db->update('cashback',$data);
                if($txn_id)
                {
					$data2 = array('transation_status'=>'Canceled');
					$this->db->where('trans_id',$txn_id);
					$this->db->update('transation_details',$data2);
                }


				/* mail for Canceled cashback notifications*/
				$this->db->where('admin_id',1);
				$admin_det = $this->db->get('admin');
				if($admin_det->num_rows >0) 
				{    
					$admin       = $admin_det->row();
					$admin_email = $admin->admin_email;
					$site_name   = $admin->site_name;
					$admin_no    = $admin->contact_number;
					$site_logo   = $admin->site_logo;
				}
				$date =date('Y-m-d');

				if($single->cashback_mail == 1)
				{
					$this->db->where('mail_id',11);
					$mail_template = $this->db->get('tbl_mailtemplates');
					if($mail_template->num_rows >0) 
					{
					    
					   $fetch 		 = $mail_template->row();
					   $subject 	 = $fetch->email_subject;
					   $templete     = $fetch->email_template;
					   $url 		 = base_url().'my_earnings/';
					   $unsuburl	 = base_url().'un-subscribe/cashback/'.$user_id;
					   $myaccount    = base_url().'minha-conta';
					   
						$this->load->library('email');
						
						$config = Array(
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
						);
						
						$sub_data = array(
							'###SITENAME###'=>$site_name
						);
						$subject_new = strtr($subject,$sub_data);
						
						// $this->email->initialize($config);
						$this->email->set_newline("\r\n");
						$this->email->initialize($config);
						$this->email->from($admin_email,$site_name.'!');
						$this->email->to($user_email);
						$this->email->subject($subject_new);
					   
						$data = array(
							'###NAME###'=>$user_name,
							'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
							'###SITENAME###'=>$site_name,
							'###ADMINNO###'=>$admin_no,
							'###DATE###'=>$date,
							'###AMOUNT###'=>str_replace('.', ',', bcdiv($cashback_amount,1,2)),
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
					    );
					   
					   $content_pop=strtr($templete,$data);
					   $this->email->message($content_pop);
					   $this->email->send();  
					}
				} 
				/*End mail for Canceled cashback notifications*/  

				$this->db->where('cashback_id',$cashback_id);
				$cashbacks 	   = $this->db->get('cashback');
				$cashback_data = $cashbacks->row();
				
				if($cashback_data->referral!=0)
				{
					$this->db->where('trans_id',$cashback_data->txn_id);
					$txn 	     = $this->db->get('transation_details');
					$txn_detail  = $txn->row();
					$new_txn_ids = $cashback_data->new_txn_id;

					if($txn_detail)
					{
						$txn_id 	 	   = $txn_detail->trans_id;
						$ref_user_id 	   = $txn_detail->user_id;
						$transation_amount = $txn_detail->transation_amount;
						$refer_user 	   = $this->view_user($ref_user_id);
						
						if($refer_user)
						{
							foreach($refer_user as $single)
							{
								$referral_balance = $single->balance;
								$user_email 	  = $single->email;
								$first_name 	  = $single->first_name;
								$last_name 		  = $single->last_name;

								if($first_name == '' && $last_name == '')
								{
									$ex_name   = explode('@', $user_email);
									$user_name = $ex_name[0]; 
								}
								else
								{
									$user_name = $first_name.' '.$last_name;
								}
							}
							//$this->db->where('user_id',$ref_user_id);
							//$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amount));
							$data = array('transation_status'=>'Canceled','transation_reason'=>'Referal Payment');
							$this->db->where('trans_id',$txn_id);
							$this->db->update('transation_details',$data);
							
							/* mail for Canceled referral cashback amt mail notifications */
							if($single->referral_mail == 1)
							{
								$this->db->where('mail_id',12);
								$mail_template = $this->db->get('tbl_mailtemplates');
								if($mail_template->num_rows >0) 
								{
								   	$fetch 	   = $mail_template->row();
								   	$subject   = $fetch->email_subject;
								   	$templete  = $fetch->email_template;
								   	$url 	   = base_url().'my_earnings/';
								   	$unsuburl  = base_url().'un-subscribe/referral/'.$ref_user_id;
					   			   	$myaccount = base_url().'minha-conta';
								   
									$this->load->library('email');
									
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);

									$sub_data = array(
										'###SITENAME###'=>$site_name
									);

									$subject_new = strtr($subject,$sub_data);
									$this->email->set_newline("\r\n");
									$this->email->initialize($config);
									$this->email->from($admin_email,$site_name.'!');
									$this->email->to($user_email);
									$this->email->subject($subject_new);
								   
									$data = array(
										'###NAME###'=>$user_name,
										'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###AMOUNT###'=>str_replace('.', ',', bcdiv($transation_amount,1,2)),
										'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
										'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
									);
								   
								   $content_pop=strtr($templete,$data);
								   $this->email->message($content_pop);
								   $this->email->send();  
								}
							}	
							/* mail for Canceled referral cashback amt mail notifications */	 

							$data = array('transation_status'=>'Canceled','transation_reason'=>'Referral Cashback amount'); /*,'transaction_date'=>$date*/
							$this->db->where('new_txn_id',$new_txn_ids);
							$this->db->where('transation_status','Approved');
							$this->db->update('transation_details',$data);
						}
					}
				}
			}
			return true;
		}
		return false;
	}


	//New code for Cancel multiple cashback records 6/4/16//

	function cancel_multi_cashbacks()
	{
		 
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		$a1=0;
		foreach($sort_order as $key=>$val)
		{
			$a1++;
			$cashback_id = $key;	
			$this->db->where('cashback_id',$cashback_id);
			$cb = $this->db->get('cashback');
			
			$user_id ='';
			if($cb)
			{
			$cb_r    		 = $cb->row();
			$user_id 		 = $cb_r->user_id;
            $txn_id  		 = $cb_r->txn_id;
			$cashback_amount = $cb_r->cashback_amount;
		
			$user = $this->view_user($user_id);
			if($user)
			{
				foreach($user as $single)
				{
					$balance    = $single->balance;
					$user_email = $single->email;
					$user_name  = $single->first_name.' '.$single->last_name;
				}

				//$this->db->where('user_id',$user_id);
				//$this->db->update('tbl_users',array('balance'=>$balance+$cashback_amount));				
				//$ins_data = array('user_id'=>$user_id,'transation_amount'=>$cashback_amount,'mode'=>'Credited','transation_date'=>date('Y-m-d'),'transation_status'=>'Paid','transation_reason'=>'Cashback');
				//$this->db->insert('transation_details',$ins_data);
				//echo "kjfdj";exit;
				$data = array('status'=>'Canceled');
				$this->db->where('cashback_id',$cashback_id);
				$this->db->update('cashback',$data);
                if($txn_id)
                {
					$data2 = array('transation_status'=>'Canceled'); //,'transaction_date'=>date('Y-m-d')
					$this->db->where('trans_id',$txn_id);
					$this->db->update('transation_details',$data2);
                }
				/* mail for pending cashback */
				$this->db->where('admin_id',1);
				$admin_det = $this->db->get('admin');
				if($admin_det->num_rows >0) 
				{    
					$admin = $admin_det->row();
					$admin_email = $admin->admin_email;
					$site_name = $admin->site_name;
					$admin_no = $admin->contact_number;
					$site_logo = $admin->site_logo;
				}
				$date =date('Y-m-d');
				if($single->cashback_mail == 1)
				{
					$this->db->where('mail_id',11);
					$mail_template = $this->db->get('tbl_mailtemplates');
					if($mail_template->num_rows >0) 
					{
					    
					   $fetch = $mail_template->row();
					   $subject = $fetch->email_subject;
					   $templete = $fetch->email_template;
					   $url = base_url().'my_earnings/';
					   $unsuburl	 = base_url().'un-subscribe/cashback/'.$user_id;
					   $myaccount    = base_url().'minha-conta';
					   
						$this->load->library('email');
						
						$config = Array(
							'mailtype'  => 'html',
							'charset'   => 'utf-8',
						);
						
						$sub_data = array(
							'###SITENAME###'=>$site_name
						);
						$subject_new = strtr($subject,$sub_data);
						
						// $this->email->initialize($config);
						 $this->email->set_newline("\r\n");
						   $this->email->initialize($config);
						   $this->email->from($admin_email,$site_name.'!');
						   $this->email->to($user_email);
						   $this->email->subject($subject_new);
					   
						$data = array(
							'###NAME###'=>$user_name,
							'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
							'###SITENAME###'=>$site_name,
							'###ADMINNO###'=>$admin_no,
							'###DATE###'=>$date,
							'###AMOUNT###'=>str_replace('.', ',', $cashback_amount),
							'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
							'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
					    );
					   
					   $content_pop=strtr($templete,$data);
					   //echo print_r($content_pop); exit;
					   // echo $content_pop; echo $subject_new;
					   $this->email->message($content_pop);
					   $this->email->send();  
					}
				}     
				$this->db->where('cashback_id',$cashback_id);
				$cashbacks = $this->db->get('cashback');
				$cashback_data = $cashbacks->row();
				if($cashback_data->referral!=0)
				{
					$this->db->where('trans_id',$cashback_data->txn_id);
					$txn = $this->db->get('transation_details');
					$txn_detail = $txn->row();
					if($txn_detail)
					{
						$txn_id = $txn_detail->trans_id;
						$ref_user_id = $txn_detail->user_id;
						$transation_amount = $txn_detail->transation_amount;
						$refer_user = $this->view_user($ref_user_id);
						if($refer_user)
						{
							foreach($refer_user as $single)
							{
								$referral_balance = $single->balance;
								$user_email = $single->email;
								$user_name = $single->first_name.' '.$single->last_name;
							}
							//$this->db->where('user_id',$ref_user_id);
							//$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amount));
							$data = array('transation_status'=>'Canceled','transation_reason'=>'Referral Cashback amount'); //,'transaction_date'=>$date
							$this->db->where('trans_id',$txn_id);
							$this->db->update('transation_details',$data);
							// mail for pending referral //
							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name = $admin->site_name;
								$admin_no = $admin->contact_number;
								$site_logo = $admin->site_logo;
							}
							$date =date('Y-m-d');
							
							if($single->referral_mail == 1)
							{
								$this->db->where('mail_id',12);
								$mail_template = $this->db->get('tbl_mailtemplates');
								if($mail_template->num_rows >0) 
								{
								   $fetch = $mail_template->row();
								   $subject = $fetch->email_subject;
								   $templete = $fetch->email_template;
								   $url = base_url().'my_earnings/';
								   $unsuburl	 = base_url().'un-subscribe/referral/'.$ref_user_id;
					   			   $myaccount    = base_url().'minha-conta';
								   
									$this->load->library('email');
									
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);
									$sub_data = array(
										'###SITENAME###'=>$site_name
									);
									$subject_new = strtr($subject,$sub_data);
									// $this->email->initialize($config);
									 $this->email->set_newline("\r\n");
									   $this->email->initialize($config);
									   $this->email->from($admin_email,$site_name.'!');
									   $this->email->to($user_email);
									   $this->email->subject($subject_new);
								   
									$data = array(
										'###NAME###'=>$user_name,
										'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###AMOUNT###'=>str_replace('.', ',', $transation_amount),
										'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
										'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
									);
								   
								   $content_pop=strtr($templete,$data);
								  // echo $subject_new;  echo $content_pop;exit;
								   $this->email->message($content_pop);
								   $this->email->send();  
								}
							}	
							// mail for pending referral //	 
						}
					}
				}
			}
			//return true;
			}	
			/*$data = array(		
			'status' => 'Canceled');
			$this->db->where('cashback_id',$id);
			$update_qry = $this->db->update('cashback',$data);*/		
		}
		if($a1!=0)
		{
			return true;
		}else
		{
			return false;
		}
	}
	//End//





	function pending_referral(){
		$this->db->connection_check();
		$this->db->select("*");
		$this->db->from("transation_details");
		$this->db->where('transation_reason','Pending Referal Payment');
		$this->db->where('transation_status','Pending');
		$this->db->order_by("trans_id", "desc");
		$result = $this->db->get();
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	
	function approve_referral($txn_id){
		$this->db->connection_check();
		$this->db->where('trans_id',$txn_id);
		$txn = $this->db->get('transation_details');
		if($txn){
			$txn_detail = $txn->row();
			$user_id = $txn_detail->user_id;
			$transation_amount = $txn_detail->transation_amount;
			
			$refer_user = $this->view_user($user_id);
			if($refer_user){
				foreach($refer_user as $single){
					$referral_balance = $single->balance;
				}
				$this->db->where('user_id',$user_id);
				$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amount));
				
				$data = array('transation_status'=>'Approved','transation_reason'=>'Referal Payment');
				$this->db->where('trans_id',$txn_id);
				$this->db->update('transation_details',$data);

				return true;
			}
		}
		return false;
	}

	function cancel_referral($txn_id){
		$this->db->connection_check();
		$this->db->where('trans_id',$txn_id);
		$txn = $this->db->get('transation_details');
		if($txn){
			$txn_detail = $txn->row();
			$user_id = $txn_detail->user_id;
			$transation_amount = $txn_detail->transation_amount;
			
			$refer_user = $this->view_user($user_id);
			if($refer_user){
				foreach($refer_user as $single){
					$referral_balance = $single->balance;
				}
				//$this->db->where('user_id',$user_id);
				//$this->db->update('tbl_users',array('balance'=>$referral_balance+$transation_amount));
				
				$data = array('transation_status'=>'Canceled','transation_reason'=>'Referal Payment');
				$this->db->where('trans_id',$txn_id);
				$this->db->update('transation_details',$data);
				return true;
			}
		}
		return false;
	}
	/*Seetha 24/10/15 */

	//New code for delete multiple Referral records 6/4/16//

	function delete_multi_referral()
	{
		 
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		foreach($sort_order as $key=>$val)
		{
			$id = $key;		
			$this->db->delete('transation_details',array('trans_id' => $id));			
		}
		return true;
	}
	//End//

	//New code for Approve multiple Referral records 6/4/16//

	function approve_multi_referral()
	{
		 
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		foreach($sort_order as $key=>$val)
		{
			$id = $key;	

			$data = array(	
			'transation_reason' => 'Referal Payment',
			'transation_status' => 'Approved');
			$this->db->where('transation_reason','Pending Referal Payment');
			$this->db->where('transation_status','Pending');
			$this->db->where('trans_id',$id);
			$update_qry = $this->db->update('transation_details',$data);		
		}
		return true;
	}
	//End//

	//New code for Cancel multiple Referral records 6/4/16//

	function cancel_multi_referral()
	{
		 
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		foreach($sort_order as $key=>$val)
		{
			$id = $key;		
			$data = array(	
			'transation_status' => 'Canceled');
			$this->db->where('transation_reason','Pending Referal Payment');
			$this->db->where('transation_status','Pending');
			$this->db->where('trans_id',$id);
			$update_qry = $this->db->update('transation_details',$data);			
		}
		return true;
	}
	//End//








//Affiliate Network API 16/3/16//

	/*function affiliate_network($name)
	{
		$this->db->connection_check();
		$this->db->order_by('coupon_id','desc');
		$this->db->where('coupon_status','pending');
		$this->db->where('api_name',$name);
		$affiliate_network = $this->db->get('coupons');
		if($affiliate_network->num_rows > 0){
			return $affiliate_network->result();		
		}
		return false;
	}*/
	// addaffiliate_list
	function add_affiliate($img)
	{
		$this->db->connection_check();
		$data = array(
			'affiliate_network' =>$this->input->post('affiliate_name'),			
			'api_key'		    =>$this->input->post('api_key'),
			'networkid'		    =>$this->input->post('networkid'),
			//'signature_id'	=>$this->input->post('signature_id'),
			'affiliate_logo'	=>$img,
			'status'			=>$this->input->post('affiliate_status')
		);	
		$this->db->insert('affiliates_list',$data);
		return true;
	}
	//edit affiliate_list
	function get_affiliate_list($id){
	$this->db->connection_check();
		$this->db->where('id',$id);
		$affiliates = $this->db->get('affiliates_list');
		if($affiliates->num_rows > 0){
			return $affiliates->result();
		}
		return false;
	}
	// update affiliate_list
	function update_affiliate($img)
	{
		$this->db->connection_check();
		$affiliate_id = $this->input->post('affiliate_id');
		$data = array(
		//'affiliate_network' => $this->input->post('affiliate_name'),
		'api_key'           => $this->input->post('api_key'),
		'networkid' 		=> $this->input->post('networkid'),
		//'signature_id'    => $this->input->post('signature_id'),
		'affiliate_logo'    => $img,
		'status'			=> $this->input->post('affiliate_status')		
	);
	$this->db->where('id',$affiliate_id);
	$update = $this->db->update('affiliates_list',$data);
	if($update!="")
	{
		return true;
	}
	else 
	{ 
		return false;   
	}
	}
	//  deleteaffiliate_list
	function delete_affiliate($delete)
	{ 
		$this->db->connection_check();
		$this->db->delete('affiliates_list',array('id' => $delete));
		return true;
	}
	function delete_multi_affiliatenetworks()
	 {
		 $this->db->connection_check();
		  $sort_order = $this->input->post('chkbox');
		 foreach($sort_order as $key=>$val)
		 {
			$id = $key;		
			$this->db->delete('affiliates_list',array('id' => $id));			
		 }
		 return true;
	 }
	function import_coupons($content)
	{
		//print_r($content);exit;	
		$tracking 	 = $this->input->post('tracking_id');
		$coupon_type = '';
		
		if(count($content)!=0)
		{
			$array 	   = array();
			$duplicate = 0;
			$duplicate_promo_id = '';	
			
			foreach(($content['programItems']['programItem']) as $cont)
			{

				$new_category_id='';

				if(!empty($cont['description'])) 
				{	 
				 	
					$olddescription =  $cont['description'];
				    $description =  substr($olddescription,0,150);
				}	
				else
				{	 
					$description = '';
				}

				
				if (!empty($cont['categories'][0]['category']['$'])) 
				{
				  
					$category_name = $cont['categories'][0]['category']['$'];
				}	
				else
				{
					$category_name = '';
									
				}

				if (!empty($cont['categories'][0]['category']['@id']))
				{

					$promo_id = $cont['categories'][0]['category']['@id'];
					 
				}else					
				{						
					$promo_id = '';
				}

				if (!empty($cont['url'])) 
				{
					$offer_page =$cont['url'];
					 
				}else					
				{						
					$offer_page = '';
				}

				if (!empty($cont['startDate'])) 
				{
					$start_date =$cont['startDate'];
					 
				}else					
				{						
					$start_date = '';
				}
				 
				$image 		 = $cont['image'];
				$title 		 = substr($olddescription,0,50);
				$type 		 = 'Coupon';
			    $expiry_date = date('Y-m-d H:m:s');
				
				//Add new store name
				$this->db->where('affiliate_name',$category_name);
				$aff = $this->db->get('affiliates');
				if($aff->num_rows()==0){
					$data = array(
						'affiliate_name' => $category_name,
						'affiliate_url' => $offer_page,
						'affiliate_logo' => $image,
						'affiliate_status' => '1',
					);
					$this->db->insert('affiliates',$data);
					$new_store_id = $this->db->insert_id();
				}else{
					///echo "hai"; exit;
					$result = $aff->row();
					$new_store_id = $result->affiliate_id;
				}
				
				//$this->db->where('promo_id',$promo_id);
				//$result = $this->db->get('coupons');
				//echo $result->num_rows;
				//if($result->num_rows == 0){					
					if($category_name)
					{	//echo "hai"; exit;

						$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`,`code`,`offer_page`,`start_date`,`expiry_date`
						) VALUES ('$category_name','$title','$description','$type', '$code', '$offer_page','$start_date','$expiry_date')");
					 
					}
					else
					{
					$duplicate+=1;
					$duplicate_promo_id .= $promo_id.', ';
					}
					//echo "hai"; exit;
				unset($cont);
	 
			}
			//$i++;
			//exit;
			$array['duplicate'] = $duplicate;
			$array['promo_id'] = rtrim($duplicate_promo_id,', ');
		}
		//print_r($array);die;
		return $array;		
	}
	

//END//

//API Coupons 16/3/16//

function api_coupons($store_name=null){
		$this->db->connection_check();
		$this->db->order_by("coupon_id", "desc");
		if($store_name)
		{
			$this->db->like('offer_name', $store_name);	
		}
		$this->db->where("featured", "0");
		$result = $this->db->get('coupons');
		//echo $this->db->last_query();
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	// view coupon..	
	function api_editcoupon($coupon_id){
	$this->db->connection_check();
	$this->db->where('coupon_id',$coupon_id);
	$this->db->where('featured','0');
	$coupons = $this->db->get('coupons');
		if($coupons->num_rows > 0){
			return $coupons->result();
		}
		return false;
	}
	// update coupon details..
	function api_updatecoupon()
	{
		$this->db->connection_check();
		$start_date = $this->input->post('start_date');
		$expiry_date = date('Y-m-d',strtotime($this->input->post('expiry_date')));
		$coupon_id = $this->input->post('coupon_id');
		$data = array(
			'offer_name'=>$this->input->post('offer_name'),
			'category_name'=>$this->input->post('category_name'),
			'title'=>$this->input->post('title'),
			'description'=>$this->input->post('description'),
			'type'=>$this->input->post('type'),
			'code'=>$this->input->post('code'),
			'offer_page'=>$this->input->post('offer_page'),
			'expiry_date'=>$expiry_date,
			'start_date'=>$start_date,
			'featured'=>$this->input->post('featured'),
			'exclusive'=>$this->input->post('exclusive'),
			'Tracking'=>$this->input->post('Tracking'),
			'coupon_options'=>$this->input->post('coupon_options'),
			'cashback_description'=>$this->input->post('cashback_description')
		);
		$this->db->where('coupon_id',$coupon_id);
		$this->db->where('featured','0');
		$updation = $this->db->update('coupons',$data);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;
		}
	}		
	// delete coupon..
	function api_deletecoupon($delete_id){
		$this->db->connection_check();
		$data = array(
				'featured'=>0,
				'coupon_status'=>'deleted',
		);
		$this->db->where('coupon_id',$delete_id);
		$update = $this->db->update('coupons',$data);
		return true;	
	} 
	function api_changestatus($id,$status,$name)
	{
		$this->db->connection_check();
		if($status==1) $var=0;else $var=1;
			$data = array(
				'featured'=>$var,
				'coupon_status'=>'completed',
		);
		$this->db->where('coupon_id',$id);
		$updation = $this->db->update('coupons',$data);	
	}
	function api_coupons_bulk_delete()
	{
		 $this->db->connection_check();
		 $sort_order = $this->input->post('chkbox');
			 foreach($sort_order as $key=>$val)
			 {
					 $delete_id = $key;
					 $this->db->delete('coupons',array('coupon_id'=>$delete_id,'featured'=>'0'));
			 }
		return true;	
	}
	function api_download_free_coupons()
	{
		$this->db->connection_check();
		$selqry="SELECT * FROM  coupons  where featured='0' order by coupon_id desc";  
		$result=$this->db->query("$selqry"); 
		if($result->num_rows > 0)
		{		
			return $result->result();
		}
	}

//END//



	function userdetails($type)
	{	
		//echo "hai".$type; exit;
		$this->db->connection_check();
		$this->db->where('referral_category_type',$type);
		$result = $this->db->get('tbl_users');
		if($result->num_rows > 0)
		{		
			return $result->result();
		}
	}


	function add_user($email,$cat_type)
	{	
		$this->db->connection_check();
		
		$arrcat_type = array('categoryone'=>1,'categorytwo'=>2,'categorythree'=>3,'categoryfour'=>4,'categoryfive'=>5,'categorysix'=>6,'categoryseven'=>7,'categoryeight'=>8,'categorynine'=>9,'categoryten'=>10);
 		
 		if($cat_type=='')
		{
			$cat_type = 0;
		}
		else 
		{
			$cat_type = $arrcat_type[$cat_type];
		}
		 

		$this->db->where('email',$email);
		$query = $this->db->get('tbl_users');
   
		if($query->num_rows()>= 1)
		{ 	
			$data = array(
			'referral_category_type' => $cat_type,
			);
			$this->db->where('email',$email);
			$this->db->update('tbl_users',$data);
			return true;
		}
		 else
		 {
			return false;
		 }	 
	}

	function update_user($userid)
	{		
		$this->db->connection_check();	
		$data = array(		
		'referral_category_type' => 1);
		$this->db->where('user_id',$userid);
		$update_qry = $this->db->update('tbl_users',$data);
		
		if($update_qry){
			return true;
		}
		else{
			return false;
		}	
	}

	 
//End//	
	function cat_details($categoryid)
	{
		$this->db->connection_check();
		$this->db->order_by('affiliate_status','ASC');
		$this->db->where("FIND_IN_SET('$categoryid',store_categorys) !=", 0);
		$this->db->where('affiliate_status','1');
		$query = $this->db->get('affiliates');
		//echo $this->db->last_query();die;
		if($query->num_rows >= 1)
		{	
			return $query->result();
		}
		return false;
	}

	function add_related_stores($ids,$name)
	{	
		//echo $ids; exit;
		$this->db->connection_check();
		
		//print_r($_POST); exit;
		$date = date('Y-m-d');
		$store_name   = $this->input->post('category_name');
		$store_status = $this->input->post('status');
		$store_counts = $this->input->post('counts');	
		$category_id  = $ids;
		$cat_name 	  = $name;
		$seo_url  	  = $this->admin_model->seoUrl($cat_name);

		$selqry = $this->db->query("SELECT ref_affiliate_name from aff_related_stores where ref_affiliate_name='$store_name'")->row(); 
		$category_names = $selqry->ref_affiliate_name;

		if($this->input->post('categorys_list'))
		{
			$store_categorys =implode(",",$this->input->post('categorys_list'));
		}
		else
		{
			$store_categorys='';
		}
		if($category_names =='')
		{
			$data = array(
			'ref_affiliate_name'  		 =>$store_name,
			'ref_affiliate_url'   		 =>$seo_url, 
			'ref_affiliate_status'		 =>$store_status,
			'ref_affiliate_display_count'=>$store_counts,
			'ref_affiliate_categorys' 	 =>$store_categorys,
			'ref_related_aff_name' 		 =>$cat_name,
			'date_added'				 =>$date
			);
			$this->db->insert('aff_related_stores',$data); 
			return true;
		}
		else
		{
			$data = array(
			'ref_affiliate_name'  		 =>$store_name,
			'ref_affiliate_url'   		 =>$seo_url, 
			'ref_affiliate_status'		 =>$store_status,
			'ref_affiliate_display_count'=>$store_counts,
			'ref_affiliate_categorys' 	 =>$store_categorys,
			'ref_related_aff_name' 		 =>$cat_name,
			'date_added'				 =>$date
			);
			$this->db->where('ref_affiliate_name',$store_name);
			$this->db->update('aff_related_stores',$data); 
			return true;
		}	
	}


//SATz


	// view balance..
	function withdraw_balance($withdraw_id){
		$this->db->connection_check();
		$balace = $this->db->get_where('withdraw',array('withdraw_id'=>$withdraw_id))->row('requested_amount');
		return $balace;
	}

//Get User's detail for export all 27-04-2016
function user_details(){

$selqry ="SELECT * FROM tbl_users";

$result =$this->db->query("$selqry");
					ob_end_clean();
					$data = $this->dbutil->csv_from_result($result, $delimiter, $newline, $enclosure);
    				force_download($filename, $data);
					

}


//SATz 05 04 2016

	
			// get admin details..
			function user_information(){
			$this->db->connection_check();
			$this->db->where('uid','1');
			$query_admin = $this->db->get('user_information');
			if($query_admin->num_rows >= 0){
			//return	$row = $query_admin->row();
			return $query_admin->result();
				}
				else
				{
				return false;		
				}	
			}


				function user_information_update(){

				$this->db->connection_check();

//$this->input->post('type'); exit();

		if($this->input->post('type')=='extrato')
		{


  $data=array(
    'uid' =>1,			
    'ex_cash_pending' =>$this->input->post('ex_cash_pending'), 
    'ex_cash_cancelled' =>$this->input->post('ex_cash_cancelled'), 
    'ex_cash_approved' => $this->input->post('ex_cash_approved'),
    'ex_ref_pending' =>$this->input->post('ex_ref_pending'),
    'ex_ref_cancelled' =>$this->input->post('ex_ref_cancelled'),
    'ex_ref_approved' => $this->input->post('ex_ref_approved'),
    'ex_ref_pending_sec' =>$this->input->post('ex_ref_pending_sec'),
    'ex_ref_cancelled_sec' => $this->input->post('ex_ref_cancelled_sec'),
    'ex_ref_approved_sec' =>$this->input->post('ex_ref_approved_sec'), 
    'ex_ref_pending_third' =>$this->input->post('ex_ref_pending_third'), 
    'ex_ref_cancelled_third' =>$this->input->post('ex_ref_cancelled_third'),
    'ex_ref_approved_third' =>$this->input->post('ex_ref_approved_third'),
    'ex_missing_cash_approved' =>$this->input->post('ex_missing_cash_approved'),
    'ex_credit_pending' =>$this->input->post('ex_credit_pending'),
    'ex_credit_cancelled' =>$this->input->post('ex_credit_cancelled'),
    'ex_credit_approved' =>$this->input->post('ex_credit_approved')

);



	$this->db->update('user_information',$data); 
	return true;

			}
	

	if($this->input->post('type')=='missing_cashabck')
		{
    $data=array(
    'missing_cash_created' =>$this->input->post('missing_cash_created'),
    'missing_cash_sentretailer' =>$this->input->post('missing_cash_sentretailer'),
    'missing_cash_cancelled' =>$this->input->post('missing_cash_cancelled'),
    'missing_cash_completed' =>$this->input->post('missing_cash_completed')
);    
    $this->db->update('user_information',$data); 
	return true;


}


	if($this->input->post('type')=='resgate')
		{
			$data=array(
    'resgate_requested' =>$this->input->post('resgate_requested'),
    'resgate_processing' =>$this->input->post('resgate_processing'),
    'resgate_completed' =>$this->input->post('resgate_completed'),
    'resgate_cancelled' =>$this->input->post('resgate_cancelled')
        );
	
    $this->db->update('user_information',$data); 
	return true;
}



	
				}



//SATz

	//New code for report export page bank details 7-5-16//
	function new_bank_details()
	{
		$this->db->connection_check();
		$this->db->order_by('bankid','desc');
		$this->db->where('report_export_status',0);
		$result = $this->db->get('tbl_banknames');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	//End//


//Pilaventhiran 07/05/2016 START
    function missiing_approval_update()
	{
    	$name = $this->db->query("select * from admin")->row();
        $site_name  = $name->site_name;
	    $this->db->connection_check();
	    $curr_status = $this->input->post('status');    
	    $cashback_id = $this->input->post('cashback_id');
	    $username = $this->input->post('username');
	    $us_email = $this->input->post('us_email');    
	    $ticket_id = $this->input->post('ticket_id');
	    $retailer_name = $this->input->post('retailer_name');
	    $cancel_reason = $this->input->post('cancel_reason');
	    $Cashback_Return_Amount = $this->input->post('Cashback_Return_Amount');
	    $user_id = $this->input->post('user_id');
	    switch($curr_status)
	    {
	        case 0:

	            $userbalance = $this->user_balance($user_id);
	            $new_balnce = $userbalance+$Cashback_Return_Amount;
	            $mode = "Credited";
	            $transation_reason = 'Cashback';
	            $details_id = $this->input->post('cashback_id');
	            $this->update_users_balance($cashback_id,$user_id,$Cashback_Return_Amount,$mode,$new_balnce,$transation_reason,$details_id,'missing_approval');                
	    		//$current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> Dear '.$username.',<br><br>Thank you for sending us the details of your transaction. Your Missing Cashback Ticket: '.$ticket_id.' has been Completed Successfully. Your Cashback Amount Added into your Account.<br><br>Please let us know if you have any further queries. Thaks For your business.<br><br> Current Status: Completed<br><br> Warm regards,<br> '.$site_name.' Team</span>';
	            $current_msg ='';

	        break;
	        case 1:
		        $mode_1 	 = "Sent to retailer";
		        $current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal">Acabamos de enviar a sua reclamação para a loja. O processo de conferência é manual e pode levar até 40 dias úteis para que eles nos respondam. (A gente sabe que isso é muito demorado e é um saco esperar tanto, mas infelizmente não depende de nós . E esse é um “prazo máximo” pode ser que leve bem menos que isso).</span>';
	        break;
	        case 2:
		        $mode_1 	 = "Cancelled";
		        $current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> A loja acabou de nos atualizar sobre o seu caso e, infelizmente, eles não aprovaram o seu pedido. A justificativa que nos deram foi: '.$cancel_reason.'. Para evitar problemas futuros leia as Regras e exceções da loja e siga os ###RECOMENDACOES_CANCELAR###”</span>';
	        break;
	        case 3:
	        	$mode_1 	 = "Approved";
	            $current_msg = '<span style="font-family:Arial,Helvetica,sans-serif;color:#000000;font-size:14px;line-height:22px;font-style:normal"> 
				Novidades! A loja acabou de nos confirmar que eles realmente não nos informaram da sua compra. Mas agora já está tudo resolvido! Acabou de pingar R$ '.$Cashback_Return_Amount.' na sua conta.
				</span>';

				$data_Up_Users_balance = array(
				'balance'=>$this->input->post('Cashback_Return_Amount'),
				);    
				  
				$this->db->where('user_id',$user_id);
				$Query_Update_User_Balance = $this->db->update('tbl_users',$data);

			break;
    	}

    	$support_ticket = $this->db->query("SELECT * from tbl_users where user_id=$user_id")->row('support_tickets');

	    $data = array(
	        'status'=>$this->input->post('status'),
	        'status_update_date'=>$this->input->post('status_update_date'),
	        'cancel_msg'=>$this->input->post('cancel_msg'),
	        'current_msg'=>$current_msg,
	    );  

	    $cashback_id = $this->input->post('cashback_id');
	    $this->db->where('cashback_id',$cashback_id);
	    $upd = $this->db->update('missing_cashback',$data);
	    if($upd)
	    {
	        if($curr_status!=1)
	        {
	        	if($support_ticket == 1)
	        	{

		            $mail_temp 	    = $this->db->query("select * from tbl_mailtemplates where mail_id='14'")->row();
		            $fe_cont   	    = $mail_temp->email_template;
		            $name 	   	    = $this->db->query("select * from admin")->row();
		            //$subject        = "Your Missing Ticket Reply";
		            $subject 		= $mail_temp->email_subject;
		            $admin_emailid  = $name->admin_email;
		            $site_logo 	    = $name->site_logo;
		            $site_name  	= $name->site_name;
		            $contact_number = $name->contact_number;
		            $servername 	= base_url();
		            $nows 			= date('Y-m-d');    
		            $this->load->library('email');
		            $see_status_missing = "<a href='".base_url()."loja-cancelou-minha-compra'>status da solicitação</a>";
		            $unsuburl     = base_url().'un-subscribe/missing_approval/'.$user_id;
		            $myaccount    = base_url().'minha-conta';
		           
		            $gd_api=array(
	                    '###ADMINNO###'=>$contact_number,
	                    '###EMAIL###'=>$username,
	                    '###DATE###'=>$nows,
	                    '###MESSAGE###'=>$current_msg,
	                    '###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
	                    '###SITENAME###' =>$site_name,
	                    '###MISSING_CASHBACK_STATUS###'=>$mode_1,
	                    '###SEE_STATUS_MISSING###'=>$see_status_missing,
	                    '###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
	                    '###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

	                    );
		            
		            $gd_message=strtr($fe_cont,$gd_api);

		            $config = Array(
		             'mailtype'  => 'html',
		              'charset'   => 'utf-8',
		              );

		            $list = array($us_email);
		            $this->email->initialize($config);
		            $this->email->set_newline("\r\n");
		            $this->email->from($admin_emailid,$site_name.'!');
		            $this->email->to($list);
		            $this->email->subject($subject);
		            $this->email->message($gd_message);
		            $this->email->send();
		            $this->email->print_debugger();
		        }    
	        }
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}

function missing_approval_fetch($Cashback_Id=null)
{
    $this->db->connection_check();
    if($Cashback_Id!="")
    {    
        $this->db->where('reference_id',$Cashback_Id);
        $missing_approval_fetch = $this->db->get('cashback');
        return $missing_approval_fetch->result();
    }
    else
    {
        return 0;
    }
}

//Pilaventhiran 07/05/2016 END

//SATz Sub Admin management model

	//add sub admin details
	function add_sub_admin($admin_logo)
	{
	
		$main_access = serialize(array_filter($this->input->post('main_access')));
		$sub_access  = serialize(array_filter($this->input->post('sub_access')));

		if($_POST['perm'])
			$perm = serialize($_POST['perm']);
		else
			$perm = 'a:1:{i:0;s:1:"0";}';
		
		/*New code for session timing details 12-7-16*/
		$dates    = $this->input->post('days');
		$hours    = $this->input->post('hours');
		$minutes  = $this->input->post('minutes');
		if($dates < '10')
		{
			$dates 	 = "0".$dates;
		}
		if($hours < '10')
		{
			$hours 	 = "0".$hours;
		}
		if($minutes < '10') 
		{
			$minutes = "0".$minutes;
		}
		$new_ses_time_format = $dates.":".$hours.":".$minutes;
		//echo $new_ses_time_format; exit;
		/*End*/
		
		$data = array(
			'admin_username'=>$this->input->post('name'),
			'admin_password'=>$this->input->post('password'),
			'admin_email'   =>$this->input->post('email'),
			'admin_logo'    =>$admin_logo,
			//'gender'      =>$this->input->post('gender'),
			//'job_role'	=>$this->input->post('job_role'),
			//'city'	    =>$this->input->post('city'),
			'contact_number'=>$this->input->post('number'),
			'contact_info'  =>$this->input->post('content'),
			'status'		=>$this->input->post('status'),
			'sub_access'	=>$sub_access,
			'main_access' 	=>$main_access,
			'permission'	=>$perm,
			'role'			=>'sub',
			'ses_datetime'	=>$new_ses_time_format
		);
		//print_r($data);exit;
		$ins = $this->db->insert('admin',$data);
		if($ins!=""){ 
			return true;
		} else { 
			return false;
		}
	}
	
	// fetch sub admin detail..
	function fetch_sub_admin(){		
		$this->db->where('role','sub');
		$this->db->order_by('admin_id','desc');
		$fetch = $this->db->get('admin');
		if($fetch->num_rows>0){
			return $fetch->result();
		}
		return false;
	}
	
	// edit sub admin detail..
	function get_sub_admin($ids){		
		$this->db->where('admin_id',$ids);
		$fetch = $this->db->get('admin');
		if($fetch->num_rows>0){
			return $fetch->row();
		}
		return false;
	}
	
	// update sub admin details..
// update sub admin details..
	function update_sub_admin($admin_logo)
	{
		$main_access = serialize(array_filter($this->input->post('main_access')));
			 $sub_access = serialize(array_filter($this->input->post('sub_access')));
		if($_POST['perm'])
			$perm = serialize($_POST['perm']);
		else
			$perm = 'a:1:{i:0;s:1:"0";}';

		if($this->input->post('password')!='')
			$password = $this->input->post('password');
		else
			$password = '';

		/*New code for session timing details 12-7-16*/
		$dates    = $this->input->post('days');
		$hours    = $this->input->post('hours');
		$minutes  = $this->input->post('minutes');
		if($dates < '10')
		{
			$dates 	 = "0".$dates;
		}
		if($hours < '10')
		{
			$hours 	 = "0".$hours;
		}
		if($minutes < '10') 
		{
			$minutes = "0".$minutes;
		}
		$new_ses_time_format = $dates.":".$hours.":".$minutes;
		//echo $new_ses_time_format; exit;

		/*End*/

		$data = array(
			'admin_username'=>$this->input->post('name'),
			'admin_password'=>$password,
			'admin_email'=>$this->input->post('email'),
			'admin_logo'=>$admin_logo,
			// 'gender'=>$this->input->post('gender'),
			// 'job_role'=>$this->input->post('job_role'),
			// 'city'=>$this->input->post('city'),
			'contact_number'=>$this->input->post('number'),
			'contact_info'=>$this->input->post('content'),
			'status'=>$this->input->post('status'),
			'permission'=>$perm,
			'role'=>'sub',
            'main_access'=>$main_access,
			'sub_access'=>$sub_access,
			'ses_datetime'=> $new_ses_time_format
		);
		
		$this->db->where('admin_id',$this->input->post('admin_id'));
		$updation = $this->db->update('admin',$data);
		//echo $this->db->last_query();die;

		if($updation!=""){
			return true;
		} else { 
			return false;
		}
	}

	
	// delete sub admin..
	function delete_sub_admin($sub_admin_id){
	
		$this->db->delete('admin',array('admin_id'=>$sub_admin_id));
		return true;
	}
	
	function multi_delete_subadmin(){
		if($this->input->post('chkbox')) {
		  $sort_order = $this->input->post('chkbox');
		  foreach($sort_order as $key=>$val) {
				$this->db->where('admin_id',$key);
				$upd = $this->db->delete('admin');
		  }
			return true;
		}
	}
	
	
	function get_admin_pages(){	
		$fetch = $this->db->get('adminpages');
		if($fetch->num_rows>=1){
			return $fetch->result();
		} else {
			return false;
		}
	}
	function get_admin_pages1($id='')
	{	
	if($id!="")
	{
		$id=$id;
	}
	else
	{
		$id=0;
	}

	$this->db->where('sub_id',$id);
		$fetch = $this->db->get('admin_page_new');
		//echo $this->db->last_query();die;
		if($fetch->num_rows>=1){
			return $fetch->result();
		} else {
			return false;
		}
	}
	function get_admin_pages2($id='')
	{	
	if($id!="")
	{
		$id=$id;
	}
	else
	{
		$id=0;
	}

	$this->db->where('sub_id',$id);
		$fetch = $this->db->get('admin_page_new');
		//echo $this->db->last_query();die;
		if($fetch->num_rows>=1){
		return $fetch->result();
		} else {
		return false;
		}
	}

	//manage sub admin
	function check_sub_admin($email){
		$this->db->where('admin_email',$email);
		$res = $this->db->get('admin');
		if($res->num_rows > 0){
			return 0;	// exists.. failure..
		} else {
			return 1;	// not exists.. success..
		}
	}


	function getadmindetails_session(){
		$this->db->connection_check();
		$admin_id = $this->session->userdata('admin_id');
		$this->db->where('admin_id',$admin_id);
	    $query_admin = $this->db->get('admin');
		if($query_admin->num_rows >= 1){
		$row = $query_admin->row();
		return $query_admin->result();
		}
		else
		{
		return false;		
		}	
	}


//SATz sub admin END
function session_time_main_access(){

		       $admin_details = $this->getadmindetails();
		       if($admin_details){
			   foreach($admin_details as $details){
			   $hours=$ses_datetime_main  = $details->ses_datetime;
		       }
    		   }
		       $CI =& get_instance();
		       $CI->load->helper('my_time_convert');
		       $minutes=hoursToMinutes($hours);
		       return $session_minutes_meta=MinutesToSeconds($minutes);

} 
/* 11 05 2016 SATz sub admin END  */

	/*New code for upload premium coupons details 10-5-16*/

	function upload_coupons($bulkcoupons)
	{
		
		$this->db->connection_check();
		$coupon_type = '';
		$this->load->library('CSVReader');
		$main_url = 'uploads/premium_coupons/'.$bulkcoupons;
	 	$result =   $this->csvreader->parse_file($main_url);
		if(count($result)!=0)
		{
		
			foreach($result as $res)
			{

				$offer_name 	= $res['offer_name'];
				$image_url      = $res['image_url'];
				$location   	= $res['location'];
				$category   	= $res['category'];
				$discount_price = $res['amount'];
				$total_price 	= $res['price'];
				$start_date 	= $res['start_date'];
				$expiry_date 	= $res['expiry_date'];
				$coupon_code 	= $res['coupon_code'];
				$store_id       = $res['store_id'];
				$features_type  = $res['features_type'];
				$offer_url  	= $res['offer_page'];
				$extra_url  	= $res['extra_url'];

				$seo_url        = $this->admin_model->seoUrl($offer_name);

				$results = $this->db->query("INSERT INTO `shopping_coupons` (`seo_url`,`offer_name`,`coupon_image`, `location`, `category`, `amount`,`offer_page`, `start_date`,`date_added`, `expiry_date`, `coupon_code`,`remain_coupon_code`, `price`,`status`,`store_name`,`tracking`,`features_type`)
				VALUES ('$seo_url','$offer_name','$image_url', '$location', '$category', '$discount_price', '$offer_url','$start_date','$start_date', '$expiry_date', '$coupon_code', '$coupon_code','$total_price',1,'$store_id','$extra_url','$features_type');");
			}
		}
		
		return true;
	}

	/*End 10-5-16*/

	/*New code for seo url without replace a speical characters 12-5-16 */
	function newseoUrl($string) {
		$this->db->connection_check();
		//Lower case everything
		$string = strtolower($string);
		/*New code for spl char replace 27-1-17*/
		$arrstr = array('&'=>'e','ã'=>'a','á'=>'a','à'=>'a','â'=>'a','é'=>'e','ê'=>'e','ó'=>'o','ō'=>'o','ô'=>'o','í'=>'i','î'=>'i','ú'=>'u','ü'=>'u');
		foreach($arrstr as $key=> $newstring)
		{
			$string = str_replace($key, $newstring, $string); 
		}
		/*end 27-1-17*/
		//Make alphanumeric (removes all other characters)
		$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
		//Clean up multiple dashes or whitespaces
		$string = preg_replace("/[\s-]+/", " ", $string);
		//Convert whitespaces and underscore to dash
		$string = preg_replace("/[\s_]/", "-", $string);
		return $string;
	}
	/*End*/	

	/*get particular Admin details 12-7-16*/
	function get_particular_admindetails($admin_id)
	{
		$this->db->connection_check();
		$this->db->where('admin_id',$admin_id);
		$query_admin = $this->db->get('admin');
		if($query_admin->num_rows >= 1){
			$row = $query_admin->row();
			return $query_admin->result();
		}
		else
		{
			return false;		
		}	
	}
	/*End*/
	/*New code for session logout details 12-7-16*/
	function session_logout_timings()
	{	
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		}
		else 
		{	

			$admindetails 		 = $this->admin_model->get_particular_admindetails($admin_id);
			//echo "<pre>";print_r($admindetails); 
			if($admindetails)
			{
				foreach($admindetails as $details)
				{

					echo $current_date = date('d:h:i'); echo "<br>";
					$current_details = explode(':',$current_date);
					
					$current_date = $current_details[0];
					$current_hour = $current_details[1];
					$current_mins = $current_details[2];

					echo "current_seconds ". $current_seconds   = strtotime(''.$current_date.'day'. $current_hour. 'hour' .$current_mins. 'minutes', 0); echo "<br>";

					echo $login_time   	   = $details->login_time; echo "<br>";
					$login_ses_details = explode(':',$login_time);
					
					$login_date = $login_ses_details[0];
					$login_hour = $login_ses_details[1];
					$login_mins = $login_ses_details[2];

					echo "login_seconds ". $login_seconds     =  strtotime(''.$login_date.'day'. $login_hour. 'hour' .$login_mins. 'minutes', 0); echo "<br>";
					

					echo $session_time = $details->ses_datetime;echo "<br>";
					
					$session_details = explode(':',$session_time);
					
					$sess_date = $session_details[0];
					$sess_hour = $session_details[1];
					$sess_mins = $session_details[2];

					echo "session_seconds ". $session_seconds = strtotime(''.$sess_date.'day'. $sess_hour. 'hour' .$sess_mins. 'minutes', 0);
					echo "<br>";
					echo "New expiry second is ". $new_expiry_sec  = $login_seconds + $session_seconds; echo "<br>";

					if($current_seconds == $new_expiry_sec)
					{
						$this->session->sess_destroy();
						redirect('adminsettings/index','refresh');
					}
					return true;
				}
			}	
		}
	}
	/*End*/	

	/*New code for cashback Exclusive page 13-7-16 */
	function cashback_exclusive($id)
	{
		$this->db->connection_check();
		if($id!='')
		{
			$this->db->where('id',$id);
		}
		$this->db->order_by('id','desc');
		$result = $this->db->get('cashback_exclusive');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	/*end*/

	/*New code for add_cashback_exclusive page 13-7-16*/
	function add_cashback_exclusive()
	{
		$this->db->connection_check();
		
		$date 		= date('Y-m-d');
		$start_date = date("Y-m-d",strtotime($this->input->post('start_date')));
		$end_date   = date("Y-m-d",strtotime($this->input->post('end_date')));
		//echo $start_date;
		//echo $end_date;exit;

		$data = array(

			'link_name'		 =>$this->input->post('link_name'),
			'user_email'     =>$this->input->post('user_id'),
			'store_name'	 =>$this->input->post('affiliate_name'),
			'cashback_type'  =>$this->input->post('affiliate_cashback_type'),
			'cashback_web'	 =>$this->input->post('cashback_percentage'),
			'cashback_app'	 =>$this->input->post('cashback_percent_android'),
			'extra_param_web'=>$this->input->post('content_extra_param'),
			'extra_param_app'=>$this->input->post('content_extra_param_android'),
			'expiry_notify'	 =>$this->input->post('notify_desk'),
			/*newcode added 1-8-16*/
			'analytics_info' =>$this->input->post('analytics_info'),
			/*End*/
			'date_added'	 =>$date,
			'start_date'	 =>$start_date,
			'expirydate'	 =>$end_date
			); 
			$this->db->insert('cashback_exclusive',$data); 
			return true;
	}

	function update_cashback_exclusive($id)
	{
		$this->db->connection_check();
		
		$start_date = date("Y-m-d",strtotime($this->input->post('start_date')));
		$end_date   = date("Y-m-d",strtotime($this->input->post('end_date')));

		$data = array(
			'link_name'		 =>$this->input->post('link_name'),
			'user_email'     =>$this->input->post('user_id'),
			'store_name'	 =>$this->input->post('affiliate_name'),
			'cashback_type'  =>$this->input->post('affiliate_cashback_type'),
			'cashback_web'	 =>$this->input->post('cashback_percentage'),
			'cashback_app'	 =>$this->input->post('cashback_percent_android'),
			'extra_param_web'=>$this->input->post('content_extra_param'),
			'extra_param_app'=>$this->input->post('content_extra_param_android'),
			'expiry_notify'	 =>$this->input->post('notify_desk'),
			/*newcode added 1-8-16*/
			'analytics_info' =>$this->input->post('analytics_info'),
			/*End*/
			'start_date'	 =>$start_date,
			'expirydate'	 =>$end_date
			);
		
		$this->db->where('id',$id);
		$updation = $this->db->update('cashback_exclusive',$data);
		if($updation!="")
		{
			return true;
		}
		else
		{
			return false;
		}	

	}
	/*End*/
	function delete_cashback_exclusive($id)
	{
		$this->db->connection_check();
		$this->db->delete('cashback_exclusive',array('id' => $id));
		return true;	
	}

	/*New function for amount currency format 26-8-16*/
	function currency_format($osiz_amount)
	{
		$osiz_pos_amount 	 = strpos($osiz_amount,',');
		$osiz_pos_amount1 	 = strpos($osiz_amount,'.');
		if($osiz_pos_amount  === false && $osiz_pos_amount1 === false)
		{
		  	$osiz_amount  	 = $osiz_amount.".00";
		  	$osiz_new_amount = preg_replace('/\./', ',', $osiz_amount);
		  	
		  	if($osiz_new_amount == ',00')
			{
  				$osiz_new_amount = '0,00';
			} 
			return $osiz_new_amount;
		}
		else
		{	

			$osiz_new_amount      = $osiz_amount;
			$osiz_final_amount    = preg_replace('/\./', ',', $osiz_new_amount); 
		
		  	if($osiz_final_amount == ',00')
			{
  				$osiz_final_amount = '0,00';
			}
		  	return $osiz_final_amount;
		}
	}
	/*End*/
	
	//New code for currency format 30-3-17
	/*function formatinr($input)
    {
        //CUSTOM FUNCTION TO GENERATE ##,##,###.##
        $dec = "";
        $pos = strpos($input, ".");
        if ($pos === false){
            //no decimals   
        } else {
            //decimals
            $dec = substr(round(substr($input,$pos),2),1);
            $input = substr($input,0,$pos);
        }
        $num = substr($input,-3); //get the last 3 digits
        $input = substr($input,0, -3); //omit the last 3 digits already stored in $num
        while(strlen($input) > 0) //loop the process - further get digits 2 by 2
        {
            $num = substr($input,-2).",".$num;
            $input = substr($input,0,-2);
        }
        $osiz_amount = ($num . $dec);
        if($osiz_amount == '')
        {
        	$osiz_amount = '0,00';
        }
        return preg_replace('/\./', ',', $osiz_amount);
    }
	function currency_format($floatcurr, $curr = "INR")
	{
        
        $currencies['BRL'] = array(2,',','.');          //  Brazilian Real

        if ($curr == "INR")
        {    
            return $this->formatinr($floatcurr);
        } else {
            return number_format($floatcurr,$currencies[$curr][0],$currencies[$curr][1],$currencies[$curr][2]);
        }
    }*/
    //End 30-3-17


	/*New code for category type concept 13-9-16*/
	function category($type)
	{
		$this->db->connection_check();
		$this->db->where('ref_id',$type);
		$referral = $this->db->get('referral_settings');
		if($referral->num_rows >= 1){
			$row = $referral->row();
			return $referral->result();
		}
		else
		{
			return false;		
		}	
	}
	function updatecategory($type)
	{	
		$this->db->connection_check();
		$category_type 		= $this->input->post('cat_type');
		 	 
		$data = array(

			'ref_by_percentage'		 =>$this->input->post('refpercentage'),
			'ref_cashback'    		 =>$this->input->post('refcashback'),
			'valid_months'    		 =>$this->input->post('validmonth'),
			'ref_by_rate'     		 =>$this->input->post('refbyrate'),
			'ref_cashback_rate'		 =>$this->input->post('refcashback_rate'),
			'bonus_by_ref_rate'		 =>$this->input->post('bonus_rate'),
			'ref_cashback_rate_bonus'=>$this->input->post('refcashback_rate_bonus'),
			'cat_description'        =>$this->input->post('cat_description'),
			'friends_count'          =>$this->input->post('friends_count'),
			'content_above_btn'      =>$this->input->post('content_above'),
			'content_bellow_btn'     =>$this->input->post('content_bellow'),
			'notify_log_users'       =>$this->input->post('notify_log_user'),
			'cat_type_status'        =>$this->input->post('cat_type_status')
		);

		$this->db->where('category_type',$category_type);
		$updation = $this->db->update('referral_settings',$data);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}		
	}
	/*End category type concept 13-9-16*/
	
	/*New code for Amazon S3 config settings 20-9-16*/
	function amazon_s3_settings()
	{
		$this->db->connection_check();
		$this->db->where('s3_id','1');
		$query_admin = $this->db->get('amazon_s3_settings');
			if($query_admin->num_rows == 1){
				return $query_admin->row();
			}else{
				return false;		
			}	
	}

	function amazon_settings()
	{
		

		$this->db->connection_check();
		//echo "<pre>"; print_r($_POST); exit;
		$copy_status 	= $this->input->post('copy_status');
		$url_status 	= $this->input->post('url_status');
		$file_url 		= $this->input->post('file_url');
		$autoscan_status= $this->input->post('autoscan_status');
		$autoscan_url 	= $this->input->post('autoscan_url');
		$cache_status 	= $this->input->post('cache_status');
		$config_ecache	= $this->input->post('config_ecache');
		 	
			$data = array(
			'copy_files_status'=>$copy_status,
			'file_url_status'=>$url_status,
			'config_file_url'=>$file_url,
			'Copy_autoscan_status'=>$autoscan_status,
			'config_autoscan_url'=>$autoscan_url,
			'sys_memory_cache_status'=>$cache_status,
			'config_elasticache'=>$config_ecache
			);

		$this->db->where('s3_id',1);	
		$this->db->update('amazon_s3_settings',$data);
		return true;
	}
	/*End*/

	/*new code for imag url Amazon s3 cocnept 23-9-16*/
	function get_img_url()	
	{
		$this->db->connection_check();
		$img_url_details = $this->db->query("SELECT * FROM amazon_s3_settings WHERE s3_id = 1")->row();
		$img_url_status  = $img_url_details->file_url_status;
		if($img_url_status == 0)
		{
			$img_url = base_url();
		}
		else
		{
			$img_url = $img_url_details->config_file_url."/";
			//$img_url   = 'https://www.'.$this->config->item('bucket_name').'.s3.amazonaws.com/';
		}
		return $img_url;
	}

	/*new code for imag url Amazon s3 cocnept 26-9-16*/
	function get_css_js_url()	
	{
		//echo "hai"; exit;
		$this->db->connection_check();
		$files_url_details   = $this->db->query("SELECT * FROM amazon_s3_settings WHERE s3_id = 1")->row();
		$css_js_url_status  = $files_url_details->file_url_status;
		if($css_js_url_status == 0)
		{
			$css_js_url = base_url();
		}
		else
		{
			$css_js_url = $files_url_details->config_file_url."/";
		}
		return $css_js_url;
	}

	/*new code for update a records in layout settings page 6-10-16*/
	function updatelayout($backtype,$background_image,$background_color,$covertype,$cover_color,$cover_image,$topcashback_type,$topcashback_image,$topcashback_color,$image_topcashback,$pingou_site_img,$store_page_img)
	{
		$this->db->connection_check();
		$admin_logo = $logo;
		$posted 	= $this->input->post('username');
		$this->session->set_userdata('admin_username',$posted);
		$id = $this->input->post('admin_id');

		/*acc_page status 17-3-17*/
		$acc_page_status = $this->input->post('acc_page_status');		

		$data = array(
			'background_type'  =>$backtype,
			'background_image' =>$background_image,
			'background_color' =>$background_color,
			'storecover_type'  =>$covertype,
			'storecover_image' =>$cover_image,
			'storecover_color' =>$cover_color,
			'topcashback_type' =>$topcashback_type,
			'topcashback_image'=>$topcashback_image,
			'topcashback_color'=>$topcashback_color,
			'image_topcashback'=>$image_topcashback,
			'store_back_img_settings'=>$this->input->post('back_img_type'),
			'top_back_img_settings'  =>$this->input->post('top_back_img_type'),
			'notify_desk'=>$this->input->post('notify_desk'),
			/*new field for facebook share 14-11-16*/
			'storepage_meta_image'=>$store_page_img,
			'pingou_meta_image'	  =>$pingou_site_img,
			/*End 14-11-16*/
			//New code for account tab status 17-3-17
			'acc_page_status' => $acc_page_status,
			//End 17-3-17
			'cover_photo_status' => $this->input->post('cover_photo_status'),
			'storepage_popup_type' => $this->input->post('storepage_popup_type')
			);
		 	
			
			$this->db->where('admin_id',$id);
			$updation = $this->db->update('admin',$data);
		
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}
	}
	function updatesales_funnel()
	{
		$this->db->connection_check();
		$posted 	= $this->input->post('username');
		$this->session->set_userdata('admin_username',$posted);
		
		$data = array(
			'activate_method'	=>$this->input->post('activate_method'),
			'unlog_status'	 	=>$this->input->post('unlog_status'),
			'unlog_content'	 	=>$this->input->post('unlog_content'),
			'log_status'	 	=>$this->input->post('log_status'),
			'unlog_menu_status' =>$this->input->post('unlog_usr_status'),
			'log_menu_status'   =>$this->input->post('log_usr_status'),
			'notify_color'      =>$this->input->post('notify_color'),
			'rel_store_details' =>$this->input->post('rel_store_details')
		);
	 	
		$id = $this->input->post('admin_id');
		$this->db->where('admin_id',$id);
		$updation = $this->db->update('admin',$data);


		$data = array(
			/*'not_log_status'					 	  =>$this->input->post('not_log_usr_status'),
			'not_log_banner_url'	 			 	  =>$this->input->post('not_log_banner_url'),
			'not_log_banner_images'	 			 	  =>$not_log_banner_img,
			'not_log_refcat_type'	 			 	  =>$this->input->post('not_log_refcat_type'),*/
			'not_log_html_settings' 			 	  =>$this->input->post('not_log_htmlsettings'),
			/*'log_notusebonus_notuseapp_status'   	  =>$this->input->post('log_notbonus_notapp_status'),
			'log_notusebonus_notuseapp_bannerurl'     =>$this->input->post('log_notusebonus_notuseapp_bannerurl'),
			'log_notusebonus_notuseapp_banner_images' =>$log_notusebonus_notuseapp,
			'log_notusebonus_notuseapp_refcat_type'	  =>$this->input->post('log_notusebonus_notuseapp_refcat_type'),*/
			'log_notusebonus_notuseapp_html_settings' =>$this->input->post('log_notusebonus_notuseapp_html_settings'),
			/*'log_usebonus_notuseapp_status'	 		  =>$this->input->post('log_usebonus_notuseapp_status'),
			'log_usebonus_notuseapp_bannerurl'	 	  =>$this->input->post('log_usebonus_notuseapp_bannerurl'),
			'log_usebonus_notuseapp_banner_images'    =>$log_usebonus_notuseapp,
			'log_usebonus_notuseapp_refcat_type'   	  =>$this->input->post('log_usebonus_notuseapp_refcat_type'),*/
			'log_usebonus_notuseapp_html_settings'    =>$this->input->post('log_usebonus_notuseapp_html_settings'),
			/*'log_notusebonus_useapp_status' 		  =>$this->input->post('log_notusebonus_useapp_status'),
			'log_notusebonus_useapp_bannerurl' 		  =>$this->input->post('log_notusebonus_useapp_bannerurl'),
			'log_notusebonus_useapp_banner_images'    =>$log_notusebonus_useapp,
			'log_notusebonus_useapp_refcat_type'      =>$this->input->post('log_notusebonus_useapp_refcat_type'),*/
			'log_notusebonus_useapp_html_settings'    =>$this->input->post('log_notusebonus_useapp_html_settings'),
			/*'log_usebonus_useapp_status'   	   		  =>$this->input->post('log_usebonus_useapp_status'),
			'log_usebonus_useapp_bannerurl'     	  =>$this->input->post('log_usebonus_useapp_bannerurl'),
			'log_usebonus_useapp_banner_images'		  =>$log_usebonus_useapp,
			'log_usebonus_useapp_refcat_type' 		  =>$this->input->post('log_usebonus_useapp_refcat_type'),*/
			'log_usebonus_useapp_html_settings'       =>$this->input->post('log_usebonus_useapp_html_settings'),
			/*'log_standard_status'       			  =>$this->input->post('log_standard_status'),
			'log_standard_banner_url'     			  =>$this->input->post('log_standard_banner_url'),
			'log_standard_banner_images'       		  =>$log_standard,
			'log_standard_refact_type'				  =>$this->input->post('log_standard_refact_type'),*/
			'log_standard_html_settings'       		  =>$this->input->post('log_standard_html_settings'),
			'notlog_standard_html_settings'			  =>$this->input->post('notlog_standard_html_settings')
		);

		$sales_funnel_banner_id = $this->input->post('sales_funnel_banner_id');
		$this->db->where('sales_funnel_banner_id',$sales_funnel_banner_id);
		$updation = $this->db->update('sales_funnel',$data);

		$data = array(
			'topcashback_top_status'      =>$this->input->post('topcash_top_status'),
			'topcashback_bottom_status'   =>$this->input->post('topcash_bot_status'),
			'category_top_status'	      =>$this->input->post('cat_top_status'),
			'category_bottom_status'      =>$this->input->post('cat_bot_status'),
			'store_top_status' 		      =>$this->input->post('store_top_status'),
			'store_bottom_status'         =>$this->input->post('store_bot_status'),
			'barato_top_status'           =>$this->input->post('barato_top_status'),
			'barato_bottom_status' 	      =>$this->input->post('barato_bot_status'),
			'minhaconta_top_status'	      =>$this->input->post('minha_top_status'),
			'minhaconta_bottom_status'    =>$this->input->post('minha_bot_status'),
			'extrato_top_status'	 	  =>$this->input->post('extrato_top_status'),
			'extrato_bottom_status'	 	  =>$this->input->post('extrato_bot_status'),
			'resgate_top_status'    	  =>$this->input->post('resgate_top_status'),
			'resgate_bottom_status'   	  =>$this->input->post('resgate_bot_status'),
			'clickhistory_top_status'     =>$this->input->post('click_top_status'),
			'clickhistory_bottom_status'  =>$this->input->post('click_bot_status'),
			'categorystore_top_status' 	  =>$this->input->post('catstore_top_status'),
			'categorystore_bottom_status' =>$this->input->post('catstore_bot_status')
		);
		$sales_banner_id = $this->input->post('sales_banner_id');
		$this->db->where('sales_banner_id',$sales_banner_id);
		$updation = $this->db->update('sales_banner_status',$data);

	
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}	
	}
	/*end 6-10-16*/

	/*New code for sales funnel settings page update details 14-3-17*/
	function updatesales_funnel_settings()
	{
		$this->db->connection_check();
		$posted 	= $this->input->post('username');
		$this->session->set_userdata('admin_username',$posted);
		
		$data = array(
			'activate_method'	=>$this->input->post('activate_method'),
			'unlog_status'	 	=>$this->input->post('unlog_status'),
			'unlog_content'	 	=>$this->input->post('unlog_content'),
			'log_status'	 	=>$this->input->post('log_status'),
			'unlog_menu_status' =>$this->input->post('unlog_usr_status'),
			'log_menu_status'   =>$this->input->post('log_usr_status'),
			'notify_color'      =>$this->input->post('notify_color'),
			'rel_store_details' =>$this->input->post('rel_store_details')
		);
	 	
		$id = $this->input->post('admin_id');
		$this->db->where('admin_id',$id);
		$updation = $this->db->update('admin',$data);

		$data = array(
			'topcashback_top_status'      =>$this->input->post('topcash_top_status'),
			'topcashback_bottom_status'   =>$this->input->post('topcash_bot_status'),
			'category_top_status'	      =>$this->input->post('cat_top_status'),
			'category_bottom_status'      =>$this->input->post('cat_bot_status'),
			'store_top_status' 		      =>$this->input->post('store_top_status'),
			'store_bottom_status'         =>$this->input->post('store_bot_status'),
			'barato_top_status'           =>$this->input->post('barato_top_status'),
			'barato_bottom_status' 	      =>$this->input->post('barato_bot_status'),
			'minhaconta_top_status'	      =>$this->input->post('minha_top_status'),
			'minhaconta_bottom_status'    =>$this->input->post('minha_bot_status'),
			'extrato_top_status'	 	  =>$this->input->post('extrato_top_status'),
			'extrato_bottom_status'	 	  =>$this->input->post('extrato_bot_status'),
			'resgate_top_status'    	  =>$this->input->post('resgate_top_status'),
			'resgate_bottom_status'   	  =>$this->input->post('resgate_bot_status'),
			'clickhistory_top_status'     =>$this->input->post('click_top_status'),
			'clickhistory_bottom_status'  =>$this->input->post('click_bot_status'),
			'categorystore_top_status' 	  =>$this->input->post('catstore_top_status'),
			'categorystore_bottom_status' =>$this->input->post('catstore_bot_status')
		);
		$sales_banner_id = $this->input->post('sales_banner_id');
		$this->db->where('sales_banner_id',$sales_banner_id);
		$updation = $this->db->update('sales_banner_status',$data);

	
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}	
	}
	/*End 14-3-17*/

	/*new code for export user details 15-10-16*/
	function download_users()
	{
			$this->db->connection_check();
			 $selqry="SELECT * FROM  `tbl_users` order by user_id desc";  
			 $result=$this->db->query("$selqry"); 
				if($result->num_rows > 0)
				{		
					return $result->result();
				}
	}
	/*End 15-10-16*/

	
	// New code for view all Pretty Link 19-10-16 
	function pretty_link()
	{
		$this->db->connection_check();
		$this->db->order_by('pretty_link_id','desc');
		$result = $this->db->get('pretty_link_details');
		if($result->num_rows > 0)
		{
			return $result->result();
		}
		return false;
	}

	function add_pretty_link()
	{
		$this->db->connection_check();
		 
		$data = array
		(
			'link_name'=>$this->input->post('pretty_link_name'),
			'incoming_link'=>$this->input->post('income_url'),
			'external_link'=>$this->input->post('external_url'),
			'status'=>$this->input->post('status'),
		);
		$this->db->insert('pretty_link_details',$data);
		return true;
	}
	
	// view affiliate
	function get_pretty_link($id)
	{
		$this->db->connection_check();	
		$this->db->where('pretty_link_id',$id);
		$result = $this->db->get('pretty_link_details');
		if($result->num_rows > 0){
			return $result->result();		
		}	
	}
	
	
	// update affiliate
	function update_pretty_link()
	{
		$this->db->connection_check();
		
		$pretty_link_id = $this->input->post('pretty_link_id');

		$data = array
		(
			'link_name'=>$this->input->post('pretty_link_name'),
			'incoming_link'=>$this->input->post('income_url'),
			'external_link'=>$this->input->post('external_url'),
			'status'=>$this->input->post('status')
		);
		
		$this->db->where('pretty_link_id',$pretty_link_id);
		$updation = $this->db->update('pretty_link_details',$data);	
		if($updation)
		{
			return true;	
		}
		else
		{
			return false;
		}
	}

	// New code for delete affiliate 19-10-16
	function delete_pretty_link($id)
	{
		$this->db->connection_check();
		$this->db->delete('pretty_link_details',array('pretty_link_id' => $id));
		return true;	
	}

	function sort_prettylink_delete()
	{
		$this->db->connection_check();
		$sort_order = $this->input->post('chkbox');
		foreach($sort_order as $key=>$val)
		{	 
			$id = $key;
			
			$this->db->delete('pretty_link_details',array('pretty_link_id'=>$id));
		}
		
		return true;
	}

	/*New code 26-10-16*/
	function getsalesfunneldetails()
	{
		$this->db->connection_check();
		$this->db->order_by('sales_banner_id','1');
		$result = $this->db->get('sales_banner_status');
		if($result->num_rows > 0)
		{
			return $result->result();
		}
		return false;
	}
	/*end 26-10-16*/
	/*New code 27-10-16*/
	function getsalesfunnelbannerdetails()
	{
		$this->db->connection_check();
		$this->db->order_by('sales_funnel_banner_id','1');
		$result = $this->db->get('sales_funnel');
		if($result->num_rows > 0)
		{
			return $result->result();
		}
		return false;
	}
	/*End 27-10-16*/

	/*New code for add and delete banner details 4-11-16*/
	function add_bannerdetails($image_name,$category_type,$banner_url,$condition_type)
	{	
		$this->db->connection_check();
		$image_name = preg_replace("/\s+/", "_", $image_name);
		$data = array
		(
			'image_name'=>$image_name,
			'banner_cat_type'=>$category_type,
			'banner_img_url'=>$banner_url,
			'sales_funnel_banner_id'=>1,
			'banner_condition_details'=>$condition_type,
			'click_count'=>0,
			'banner_status'=>1
		);

		$insertion = $this->db->insert('sales_funnel_banners',$data);
		
		if($insertion)
		{
			return true;
		}
		else
		{
			return false;		 
		}
	}

	function delete_bannerdetails($banner_id)
	{
		$this->db->connection_check();
		$this->db->delete('sales_funnel_banners',array('banner_id' => $banner_id));      
		return true;
	}
	/*End 4-11-16*/

	/*New code for update banner details 5-11-16*/
	function update_bannerdetails($banner_id,$status)
	{	
		$this->db->connection_check();
		$data = array(
		'banner_status'=>$status	
		);
	
		$this->db->where('banner_id',$banner_id);
		$updated = $this->db->update('sales_funnel_banners',$data);
		if($updated!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}
	}
	/*End 5-11-16*/

	/*New code for popup update details 11-11-16*/
	function popupupdate()
	{
		$this->db->connection_check();
		$admin_id = $this->input->post('admin_id');

		//print_r($_POST);exit;

		$log_usr_status = $this->input->post('log_usr_status');

		$data = array('popup_log_users_status'=>$log_usr_status);
		$this->db->where('admin_id',$admin_id);
		$updationss = $this->db->update('admin',$data);

		$popup_id1 = $this->input->post('popup_id1');
		if($popup_id1!='')
		{
			$data = array(	
			'popup_status'     => $this->input->post('not_log_usr_status'),
			'session_count'    => $this->input->post('sessiontiming'),
			'popup_content'    => $this->input->post('not_log_popupcontent'),
			'dont_show_status' => $this->input->post('notlogusr_status')
			);
			$this->db->where('popup_id',$popup_id1);
			$updation1 = $this->db->update('popup_details',$data);	
		}
		

		$popup_id2 = $this->input->post('popup_id2');
		if($popup_id2!='')
		{
			$data = array(	
			'popup_status'     => $this->input->post('log_notusebonus_notuseapp_status'),
			'session_count'    => $this->input->post('log_notusebonus_notuseapp_session'),
			'popup_content'    => $this->input->post('log_notusebonus_notuseapp_content'),
			'dont_show_status' => $this->input->post('logusr_notusebonus_notuseapp_status')
			);
			$this->db->where('popup_id',$popup_id2);
			$updation2 = $this->db->update('popup_details',$data);	
		}
		
		$popup_id3 = $this->input->post('popup_id3');
		if($popup_id3!='')
		{
			$data = array(	
			'popup_status'     => $this->input->post('log_usebonus_notuseapp_status'),
			'session_count'    => $this->input->post('log_usebonus_notuseapp_session'),
			'popup_content'    => $this->input->post('log_usebonus_notuseapp_content'),
			'dont_show_status' => $this->input->post('logusr_usebonus_notuseapp_status')
			);
			$this->db->where('popup_id',$popup_id3);
			$updation3 = $this->db->update('popup_details',$data);	
		}
		
		$popup_id4 = $this->input->post('popup_id4');
		if($popup_id4!='')
		{
			$data = array(	
			'popup_status'     => $this->input->post('log_notbonus_useapp_status'),
			'session_count'    => $this->input->post('log_notbonus_useapp_session'),
			'popup_content'    => $this->input->post('log_notbonus_useapp_content'),
			'dont_show_status' => $this->input->post('logusr_notusebonus_useapp_status')
			);
			$this->db->where('popup_id',$popup_id4);
			$updation4 = $this->db->update('popup_details',$data);	
		}

		$popup_id5 = $this->input->post('popup_id5');
		if($popup_id5!='')
		{
			$data = array(	
			'popup_status'     => $this->input->post('log_usebonus_useapp_status'),
			'session_count'    => $this->input->post('log_usebonus_useapp_session'),
			'popup_content'    => $this->input->post('log_usebonus_useapp_content'),
			'dont_show_status' => $this->input->post('logusr_usebonus_useapp_status')
			);
			$this->db->where('popup_id',$popup_id5);
			$updation5 = $this->db->update('popup_details',$data);	
		}

		$popup_id6 = $this->input->post('popup_id6');
		if($popup_id6!='')
		{
			$data = array(	
			'popup_status'     => $this->input->post('stnd_all_log_users_status'),
			'session_count'    => $this->input->post('stnd_all_log_users_session'),
			'popup_content'    => $this->input->post('stnd_all_log_users_content'),
			'dont_show_status' => $this->input->post('alllogusr_status')
			);
			$this->db->where('popup_id',$popup_id6);
			$updation6 = $this->db->update('popup_details',$data);	
		}
		return true;
	}
	/*End 11-11-16*/
	
	/*New code for update exit popup code 15-11-16*/
	function update_exitpopup()
	{
		$this->db->connection_check();
		$popup_id = $this->input->post('popup_id');
			
		$data = array(
			'popup_subject'=>$this->input->post('popup_subject'),
			'popup_template'=>$this->input->post('popup_template'),
			'store_popup_template' => $this->input->post('store_popup_template'),
			'store_popup_template_nocash' => $this->input->post('store_popup_template_nocash'),
			'all_status' => $this->input->post('all_status'),
			'store_status' => $this->input->post('store_status')
		);
		
		$this->db->where('popup_id',$popup_id);
		$update = $this->db->update('exit_popup',$data);
		if($update!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}	
	}
	/*End 15-11-16*/
	/*New code for update session count for not log users 19-11-16*/
	function update_session_count_notlog()
	{
		$this->db->connection_check();
		$data = array(
			'session_count' => 0	
		);
		$update = $this->db->update('notlogusers_ipaddress',$data);
		if($update!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}
	}
	/*End 19-11-16*/
	/*New code for update session count dor logged users 19-11-16*/
	function update_session_count_log($bonus,$log_type)
	{
		$this->db->connection_check();
		
		$data = array(
			'popup_ses_count' => 0	
		);
		
		if(($bonus == 0) && ($log_type == 0))
		{
			$this->db->where('bonus_benefit',$bonus);
			$this->db->where('app_login',$log_type);
		}
		if(($bonus == 1) && ($log_type == 0))
		{
			$this->db->where('bonus_benefit',$bonus);
			$this->db->where('app_login',$log_type);
		}
		if(($bonus == 0) && ($log_type == 1))
		{
			$this->db->where('bonus_benefit',$bonus);
			$this->db->where('app_login',$log_type);
		}
		if(($bonus == 1) && ($log_type == 1))
		{
			$this->db->where('bonus_benefit',$bonus);
			$this->db->where('app_login',$log_type);
		}
		if(($bonus == 2) && ($log_type == 2))
		{
			/*$this->db->where('bonus_benefit',$bonus);
			$this->db->where('app_login',$log_type);*/
		}		
		$update = $this->db->update('tbl_users',$data);
		if($update!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}
	}
	/*End 19-11-16*/

	/*New code for get coupons,newusers details limit to display 23-12-16*/
	function newcoupons($draw,$start,$length,$searchtext,$coupon_name)
	{
		$str = ''; 
		$limit = '';

		$this->db->connection_check();
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);

		
		if($coupon_name)
		{
			$str .= " AND `offer_name` = '".$coupon_name."' "; 	
		}

		if($length != -1)
		{
			$limit=" LIMIT $start,$length";	
		}

		if($searchtext!='')
		{
			 
			//$arr_like  = array('coupon_id' => $searchtext,'offer_name' => $searchtext,'title' => $searchtext,'code' => $searchtext,'type' => $searchtext,'Tracking' => $searchtext,'extra_tracking_param' => $searchtext);
			$str .=   " AND (`coupon_id` like '%".$searchtext."%' OR `offer_name` like '%".$searchtext."%'
					    OR  `title` like '%".$searchtext."%'     OR `code` like '%".$searchtext."%'
					    OR  `type` like '%".$searchtext."%'      OR `Tracking` like '%".$searchtext."%'
					    OR  `extra_tracking_param` like '%".$searchtext."%')
					";   
			
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 4) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				//$arr_like['expiry_date'] = $strnewdate;
				$str .= " AND `expiry_date` like '%".$strnewdate."%' OR `start_date` like '%".$strnewdate."%'";
			}
			//$this->db->or_like($arr_like);
			$result =  $this->db->query("SELECT * FROM `coupons`  WHERE `coupon_status`='completed' $str order by coupon_id desc $limit ");
		}
		else 
		{
		 	$result =  $this->db->query("SELECT * FROM `coupons` where `coupon_status`='completed' $str order by coupon_id desc $limit ");
		}


		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}

	function newusers($draw,$start,$length,$searchtext)
	{
		$this->db->connection_check();
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);
		
		if($searchtext!='' && $searchtext !='De-Activated')
		{
			$arr_like = array('user_id' => $searchtext,'first_name' => $searchtext,'last_name' => $searchtext,'email' => $searchtext,'balance' => $searchtext,'contact_no' => $searchtext,'ifsc_code' => $searchtext,'random_code' => $searchtext,'refer' => $searchtext,'referral_category_type' => $searchtext,'referral_amt' => $searchtext,'app_login' => $searchtext,'bonus_benefit' => $searchtext,'newsletter_mail' => $searchtext);
		
			/*New code for search date format details 26-1-17*/
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 4) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$arr_like['date_added'] = $strnewdate;
			}

			$this->db->or_like($arr_like);
			/*End 26-1-17*/
		}
		if($searchtext == 'De-Activated')
		{
			$this->db->where('status','0');
		}
		$this->db->where('admin_status','');
		$this->db->order_by('user_id','desc');
		if($length != -1)
		{
			$this->db->limit($length,$start);	
		}
		$user_query = $this->db->get('tbl_users');
		
		if($user_query->num_rows > 0)
        {
            return $user_query->result();
        }
			return false;
	}
	/*End 23-12-16*/
	/*New code for get cashback details limit to display 17-12-16*/
	function newcashback($draw,$start,$length,$searchtext,$retailer_name,$ref_id)
	{
		$str = ''; 
		$limit = '';
		
		$this->db->connection_check();
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);
		

		if($retailer_name)
		{
			$str .= " AND `coupon_id` = '".$retailer_name."' "; 	
		}

		if($length != -1)
		{
			$limit=" LIMIT $start,$length";	
		}

		if($searchtext!='')
		{
			$cb_amt = str_replace(',', '.', $searchtext);

			$str .=   " AND t1.`reference_id` like '%".$searchtext."%'   OR t1.`user_id` like '%".$searchtext."%'
					    OR t2.`first_name` like '%".$searchtext."%'      OR t2.`last_name` like '%".$searchtext."%'
					    OR t2.`email` like '%".$searchtext."%'           OR t1.`affiliate_id` like '%".$searchtext."%'
						OR t1.`cashback_amount` like '%".$cb_amt."%' OR t1.`transaction_amount` like '%".$cb_amt."%' 
						OR t1.`status` like '%".$searchtext."%'          OR t1.`report_update_id` like '%".$searchtext."%'
				    	OR t1.`commission` like '%".$searchtext."%' 		OR t1.`referral` like '%".$searchtext."%'
				    	OR t1.`txn_id` like '%".$searchtext."%'			OR t1.`new_txn_id` like '%".$searchtext."%'
					";  


			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 4) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$str .= " AND t1.`date_added` like '%".$strnewdate."%' OR t1.`transaction_date` like '%".$strnewdate."%'";
			}

			if($retailer_name == 'reference' && $ref_id !='')
			{	
				$result =  $this->db->query("SELECT * FROM (`cashback` as t1 LEFT JOIN tbl_users as t2 ON t2.`user_id` = t1.`user_id`) WHERE 1 $str AND `reference_id`=$ref_id order by cashback_id desc $limit ");
			}
			else if($retailer_name == 'transaction' && $ref_id !='')
			{
				$result =  $this->db->query("SELECT * FROM (`cashback` as t1 LEFT JOIN tbl_users as t2 ON t2.`user_id` = t1.`user_id`) WHERE 1 $str AND `report_update_id`=$ref_id order by cashback_id desc $limit ");				
			}
			else
			{
				$result =  $this->db->query("SELECT * FROM (`cashback` as t1 LEFT JOIN tbl_users as t2 ON t2.`user_id` = t1.`user_id`) WHERE 1 $str order by cashback_id desc $limit ");
			}
			//echo "SELECT * FROM (`cashback` as t1 LEFT JOIN tbl_users as t2 ON t2.`user_id` = t1.`user_id`) WHERE 1 $str order by cashback_id desc $limit "; exit;
		}
		else 
		{	
			if($retailer_name == 'reference' && $ref_id !='')
			{
				
				$result =  $this->db->query("SELECT * FROM `cashback` where 1 $str OR reference_id ='".$ref_id."' order by cashback_id desc $limit ");
			}
			else if($retailer_name == 'transaction' && $ref_id !='')
			{
				
				$result =  $this->db->query("SELECT * FROM `cashback` where 1 $str OR report_update_id ='".$ref_id."' order by cashback_id desc $limit ");
			}
			else
			{
		 		$result =  $this->db->query("SELECT * FROM `cashback` where 1 $str order by cashback_id desc $limit ");
			}
		}
				//echo "SELECT * FROM `cashback` where 1 $str OR reference_id ='".$ref_id."' order by cashback_id desc $limit ";
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	/*End 17-12-16*/

	/*23-12-16*/
	function newpending_cashback($draw,$start,$length,$searchtext,$retailer_name,$ref_id)
	{
		$str 	= ''; 
		$limit  = '';
		$this->db->connection_check();
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);

		if($length != -1)
		{
			$limit=" LIMIT $start,$length";	
		}
		if($searchtext!='')
		{
			$str .=  "AND t2.`email` like '%".$searchtext."%' 			OR t2.`first_name` like '%".$searchtext."%'
						OR t2.`last_name` like '%".$searchtext."%' 	        OR t1.`affiliate_id` like '%".$searchtext."%' 
						OR t1.`user_id` like '%".$searchtext."%'   			OR t1.`report_update_id` like '%".$searchtext."%'
						OR t1.`transaction_amount` like '%".$searchtext."%' OR t1.`cashback_amount` like '%".$searchtext."%' 
						OR t1.`new_txn_id` like '%".$searchtext."%' 		OR t1.`referral` like '%".$searchtext."%' 			
						OR t1.`txn_id` like '%".$searchtext."%'
					"; 
			//New code for date format//
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 4) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$str .= "OR t1.`date_added` like '%".$strnewdate."%'";
			}
			//End dae format//
			if($retailer_name == 'transaction' && $ref_id !='')
			{
				//echo  "SELECT *,t1.`date_added` as `cash_date` FROM (`cashback` as t1 LEFT JOIN tbl_users as t2 ON t2.`user_id` = t1.`user_id`) WHERE t1.`status` = 'Pending' $str OR report_update_id ='".$ref_id."' order by cashback_id desc $limit ";
				$result =  $this->db->query("SELECT *,t1.`date_added` as `cash_date` FROM (`cashback` as t1 LEFT JOIN tbl_users as t2 ON t2.`user_id` = t1.`user_id`) WHERE t1.`status` = 'Pending' $str AND report_update_id ='".$ref_id."' order by cashback_id desc $limit ");				
			}
			else
			{
				$result =  $this->db->query("SELECT *,t1.`date_added` as `cash_date` FROM (`cashback` as t1 LEFT JOIN tbl_users as t2 ON t2.`user_id` = t1.`user_id`) WHERE t1.`status` = 'Pending' $str order by cashback_id desc $limit ");
			}
		}
		else
		{
			if($retailer_name == 'transaction' && $ref_id !='')
			{
				//secho "SELECT * ,`date_added` as `cash_date` FROM `cashback` WHERE `status` = 'Pending' $str OR report_update_id ='".$ref_id."' order by cashback_id desc $limit "; 
				$result =  $this->db->query("SELECT * ,`date_added` as `cash_date` FROM `cashback` WHERE `status` = 'Pending' $str AND report_update_id ='".$ref_id."' order by cashback_id desc $limit ");				
			}
			else
			{
				$result =  $this->db->query("SELECT *,`date_added` as `cash_date` FROM `cashback` WHERE `status` = 'Pending' $str order by cashback_id desc $limit ");	
			}
		}
			
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	/*End 23-12-16*/

	/*New data table code for withdraw page 21-1-17*/
	function newwithdraw($draw,$start,$length,$searchtext)
	{
		$str 	= ''; 
		$limit 	= '';
		$this->db->connection_check();
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);

		if($length != -1)
		{
			$limit=" LIMIT $start,$length";	
		}

		if($searchtext!='')
		{
			$str .=  "  AND t2.`email` like '%".$searchtext."%'    		  OR t2.`first_name` like '%".$searchtext."%'	
						OR t2.`last_name` like '%".$searchtext."%' 		  OR t1.`withdraw_id` like '%".$searchtext."%'
						OR t1.`user_id` like '%".$searchtext."%'   		  OR t2.`balance` like '%".$searchtext."%'
						OR t1.`requested_amount` like '%".$searchtext."%' OR t1.`status` like '%".$searchtext."%'
				    "; 
			//New code for date format//
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 4) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$str .= "OR t1.`date_added` like '%".$strnewdate."%' OR t1.`closing_date` like '%".$strnewdate."%'";
			}
			//End dae format//	    
			$result = $this->db->query("SELECT *,t1.`date_added` as `with_date`,t1.`status` as `with_status` FROM (`withdraw` as t1 LEFT JOIN tbl_users as t2 ON t2.`user_id` = t1.`user_id`) WHERE 1 $str order by withdraw_id desc $limit ");
		}
		else
		{
			$result = $this->db->query("SELECT *,`date_added` as `with_date`,`status` as `with_status` FROM `withdraw` where 1 $str order by withdraw_id desc $limit ");
		} 

		if($result->num_rows > 0)
		{
			return $result->result();
		}
		return false;
	}
	/*End 21-1-17*/

	function newtransactions($draw,$start,$length,$searchtext)
	{
		$str 	= ''; 
		$limit 	= '';
		$this->db->connection_check();
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);
		
		/*if($searchtext!='')
		{
			$this->db->or_like(array('user_id' => $searchtext,'report_update_id' => $searchtext,'transation_reason' => $searchtext,'transation_amount' => $searchtext,'transation_date' => $searchtext,'transaction_date' => $searchtext,'transation_id' => $searchtext,'table' => $searchtext,'details_id' => $searchtext,'new_txn_id' => $searchtext,'ref_user_tracking_id' => $searchtext,'transation_status' => $searchtext));
		}*/

		if($length != -1)
		{
			$limit=" LIMIT $start,$length";	
		}

		if($searchtext!='')
		{
			$str .= "   AND t2.`email` like '%".$searchtext."%' 	  	   OR t2.`first_name` like '%".$searchtext."%'
						OR t2.`last_name` like '%".$searchtext."%' 		   OR t1.`user_id` like '%".$searchtext."%'
						OR t1.`transation_reason` like '%".$searchtext."%' OR t1.`report_update_id` like '%".$searchtext."%'
						OR t1.`transation_amount` like '%".$searchtext."%' OR t1.`transation_date` like '%".$searchtext."%'
						OR t1.`transaction_date` like '%".$searchtext."%'  OR t1.`transation_id` like '%".$searchtext."%'  
						OR t1.`table` like '%".$searchtext."%' 			   OR t1.`details_id` like '%".$searchtext."%'           
						OR t1.`new_txn_id` like '%".$searchtext."%'        OR t1.`ref_user_tracking_id` like '%".$searchtext."%' 
						OR t1.`transation_status` like '%".$searchtext."%'
					"; 
			//New code for date format//
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 4) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$str .= "OR t1.`transation_date` like '%".$strnewdate."%' OR t1.`transaction_date` like '%".$strnewdate."%'";
			}
			//End dae format//
			$result =  $this->db->query("SELECT * FROM (`transation_details` as t1 LEFT JOIN tbl_users as t2 ON t2.`user_id` = t1.`user_id`) WHERE 1 $str order by trans_id desc $limit ");
		}
		else 
		{		 
			$result =  $this->db->query("SELECT * FROM (`transation_details` as t1 LEFT JOIN tbl_users as t2 ON t2.`user_id` = t1.`user_id`) WHERE 1 $str order by trans_id desc $limit ");
		}
		if($result->num_rows > 0)
		{
			return $result->result();
		}
		return false;
	}

	function newclickhistory($draw,$start,$length,$searchtext,$affiliate_name)
	{
		$this->db->connection_check();
		$str 	= ''; 
		$limit  = '';
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);
			
		if($affiliate_name)
		{
			$str .= " AND `store_url` = '".$affiliate_name."' "; 	
		}


		if($length != -1)
		{
			$limit=" LIMIT $start,$length";	
		}
		if($searchtext!='')
		{
			$str .=  "  AND t2.`email` like '%".$searchtext."%'     OR t1.`user_id` like '%".$searchtext."%'    
						OR t2.`first_name` like '%".$searchtext."%'	OR t2.`last_name` like '%".$searchtext."%'  
						OR t1.`store_name` like '%".$searchtext."%' OR t1.`ip_address` like '%".$searchtext."%' 
						OR t1.`date_added` like '%".$searchtext."%'
					"; 
			
			//New code for date format//
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 4) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$str .= "OR t1.`date_added` like '%".$strnewdate."%'";
			}
			//End dae format//

			$result =  $this->db->query("SELECT *,t1.`date_added` as `click_date` FROM (`click_history` as t1 LEFT JOIN tbl_users as t2 ON t2.`user_id` = t1.`user_id`) WHERE 1 $str order by click_id desc $limit "); //WHERE t1.`status` = 'Pending'
		}
		else
		{
		 	$result = $this->db->query("SELECT *,`date_added` as `click_date` FROM `click_history` where 1 $str order by click_id desc $limit ");
		}

		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	/*End 23-12-16*/

	/*New code for reports page 25-1-17*/
	function newreports($draw,$start,$length,$searchtext)
	{
		$str 	= ''; 
		$limit 	= '';
		$this->db->connection_check();
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);
		 

		if($searchtext!='')
		{
			$arr_like   = array('offer_provider' => $searchtext,'pay_out_amount' => $searchtext,'sale_amount' => $searchtext,'transaction_id' => $searchtext,'cashback_amount' => $searchtext);
			
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 4) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$arr_like['date'] 		  = $strnewdate;
				$arr_like['last_updated'] = $strnewdate;
			}
			$this->db->or_like($arr_like);
		}

		$this->db->order_by('report_id','desc');

		if($length != -1)
		{
			$this->db->limit($length,$start);	
		}
		$result = $this->db->get('tbl_report');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	/*End 25-1-17*/

	/*new code for API coupons details 13-2-17*/
	function affiliate_network($draw,$start,$length,$searchtext,$api_name)
	{
		 
		$this->db->connection_check();
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);
		
		if($searchtext!='')
		{
			$arr_like 	= array('offer_name' => $searchtext,'title' => $searchtext,'code' => $searchtext,'description' => $searchtext);
			
			$sstrcnt    = substr_count($searchtext, '/');
			$strnewdate = '';

			if($sstrcnt == 0)
			{
				if(is_numeric($searchtext) && strlen($searchtext) <= 2)
				{
					$strnewdate .= '-'.$searchtext;
				}
			}
			else if($sstrcnt <= 2)
			{
				$arrsertext = explode('/',$searchtext);
				if(isset($arrsertext[2]) && is_numeric($arrsertext[2]) && strlen($arrsertext[2]) == 4) 
				{
					$strnewdate .= $arrsertext[2];
				}
				if(isset($arrsertext[1]) && is_numeric($arrsertext[1]) && strlen($arrsertext[1]) == 2)
				{
					$strnewdate .= '-'.$arrsertext[1];
				}
				if(isset($arrsertext[0]) && is_numeric($arrsertext[0]) && strlen($arrsertext[0]) == 2) 
				{
					$strnewdate .= '-'.$arrsertext[0];
				}
			}
			if($strnewdate!='')
			{
				$arr_like['start_date']  = $strnewdate;
				$arr_like['expiry_date'] = $strnewdate;
			}
			$this->db->or_like($arr_like);
		}

		if($api_name)
		{
			$this->db->where('api_name',$api_name);
		}

		$this->db->where('coupon_status','pending');
		$this->db->order_by('coupon_id','desc');

		if($length != -1)
		{
			$this->db->limit($length,$start);	
		}
		$result = $this->db->get('coupons');
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}
	/*end 13-2-17*/

	/*new data table code for Affiliates details page 18-2-17*/
	function newaffiliate($draw,$start,$length,$searchtext)
	{
		$str = ''; 
		$limit = '';

		$this->db->connection_check();
		$start  = intval($start);
		$length = intval($length);
		$draw   = intval($draw);

		if($length != -1)
		{
			$limit=" LIMIT $start,$length";	
		}

		if($searchtext!='')
		{
			 
			//$arr_like  = array('coupon_id' => $searchtext,'offer_name' => $searchtext,'title' => $searchtext,'code' => $searchtext,'type' => $searchtext,'Tracking' => $searchtext,'extra_tracking_param' => $searchtext);
			$str .=   " AND `affiliate_id` like '%".$searchtext."%'  	   OR `affiliate_name` like '%".$searchtext."%'
					    OR  `affiliate_url` like '%".$searchtext."%'  	   OR `cashback_content_android` like '%".$searchtext."%'
					    OR  `old_cashback` like '%".$searchtext."%' 	   OR `store_categorys` like '%".$searchtext."%'
					    OR  `report_date` like '%".$searchtext."%'    	   OR `retailer_ban_url` like '%".$searchtext."%'
					    OR  `tracking_param` like '%".$searchtext."%' 	   OR `extra_tracking_param` like '%".$searchtext."%'
					    OR  `content_extra_param` like '%".$searchtext."%' OR `content_extra_param_android` like '%".$searchtext."%'
					";   

			$result =  $this->db->query("SELECT * FROM `affiliates`  WHERE 1 $str order by affiliate_id ASC $limit ");
		}
		else 
		{
		 	$result =  $this->db->query("SELECT * FROM `affiliates` where 1 $str order by affiliate_id ASC $limit ");
		}
		
		if($result->num_rows > 0){
			return $result->result();
		}
		return false;
	}

	/*End 18-2-17*/


	function get_affiliate_details($storename)
	{
		$this->db->connection_check();
		$this->db->like('affiliate_name', $storename);
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->result();
			return $result->row();
		}
		return false;
	}

	function un_subscribe($type,$userid)
	{
		
		//echo $type; echo $userid;exit; 
		$this->db->connection_check();
		if($type =='cashback')
		{
			//echo "hai".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'cashback_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		} 
		if($type =='myaccount')
		{
			//echo "hai1".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'acbalance_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		} 
		if($type =='referral')
		{
			//echo "hai2".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'referral_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		}
		if($type =='newsletter')
		{
			//echo "hai2".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'newsletter_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		}
		if($type =='subscribers')
		{
			//echo "hai2".$userid; exit;
			$udetails    = $this->db->query("select * from tbl_users where user_id=$userid")->row();
			$emailid     = $udetails->email;
			$news_status = $udetails->newsletter_mail;

			if($news_status == 1)
			{
				$data = array(		
				'newsletter_mail' => 0,		
				);
				$this->db->where('user_id',$userid);
				$update_qry = $this->db->update('tbl_users',$data);
			}

			$where = array('subscriber_email'=>$emailid,'subscriber_status'=>1);
			$this->db->where($where);
			$query = $this->db->get('subscribers');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'subscriber_status' => 0,
				);
				$this->db->where('subscriber_email',$emailid);	
				$this->db->update('subscribers',$data);
				$user_id = $fetch->subscriber_id;
				//$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		}
		/*New code for withdraw unsubscribe details 9-1-17*/
		if($type =='withdraw')
		{
			//echo "hai".$userid; exit;
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'withdraw_mail' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		} 
		/*End*/
		/*New code for missing cashback unsubscribe details 23-1-17*/
		if($type =='missing_cashback' || $type =='missing_approval')
		{
			$where = array('user_id'=>$userid);
			$this->db->where($where);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows >= 1) 
			{
				$data = array(		
				'support_tickets' => 0,
				);
				$this->db->where('user_id',$userid);	
				$this->db->update('tbl_users',$data);
				$user_id = $fetch->user_id;
				$user_email=$fetch->email;

				return 1;
			}  
			else
			{
				return 0;
			}
		}
		/*End*/
	}

	function update_unic_bonus()
	{
		$this->db->connection_check();
		$content_id = $this->input->post('content_id');
		//print_r($_POST);exit;	
		$data = array(
			 
			'not_logged_content' => $this->input->post('not_log_content'),
			'logged_content' 	 => $this->input->post('logged_content'),
			//'not_log_status' 	 => $this->input->post('not_log_status'),
			//'log_status' 		 => $this->input->post('log_status')	
			'cat_description' 	 => $this->input->post('cat_description')
		);
		
		$this->db->where('content_id',$content_id);
		$update = $this->db->update('ex_bonus_content',$data);

		$data = array(	
			'grid_name_1'  => $this->input->post('offer_name1'),
			'grid_name_2'  => $this->input->post('offer_name2'),
			'grid_name_3'  => $this->input->post('offer_name3'),
			'grid_name_4'  => $this->input->post('offer_name4'),
			'grid_name_5'  => $this->input->post('offer_name5'),
			'grid_name_6'  => $this->input->post('offer_name6'),
			'grid_name_7'  => $this->input->post('offer_name7'),
			'grid_name_8'  => $this->input->post('offer_name8'),
			'grid_name_9'  => $this->input->post('offer_name9'),
			'grid_name_10' => $this->input->post('offer_name10'),
			'grid_name_11' => $this->input->post('offer_name11'),
			'grid_name_12' => $this->input->post('offer_name12'),
			'grid_name_13' => $this->input->post('offer_name13'),
			'grid_name_14' => $this->input->post('offer_name14'),
			'grid_name_15' => $this->input->post('offer_name15'),
			'grid_name_16' => $this->input->post('offer_name16'),
			'grid_name_17' => $this->input->post('offer_name17'),
			'grid_name_18' => $this->input->post('offer_name18'),
			'grid_img_desc'=> $this->input->post('homepage_desc')
			);
		$this->db->where('grid_img_status',1);
		$update = $this->db->update('homepage_fav_store_grids',$data);	

		if($update!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}	
	}

	/*New code for Coupon upload via API 3-2-11.*/
	function import_apicoupons($content,$affiliate_name)
	{
		//echo "hai"; exit;
		//echo "<pre>"; print_r($content); echo "<br>"; 
		//echo $affiliate_name; exit;
		//echo "<pre>"; print_r($content['incentiveItems']['incentiveItem']['endDate']);exit;
		$tracking    = $this->input->post('tracking_id');
		$coupon_type = '';
		if(count($content)!=0)
		{
			 
			$array 	   = array();
			$duplicate = 0;
			$duplicate_promo_id = '';
					
			if($affiliate_name == 'zanox')
			{
				
				foreach($content['incentiveItems']['incentiveItem'] as $cont)
				{
					$new_category_id='';
					 
					//Coupon table details start 09-06-16.//
					$cont_offname    = $cont['admedia']['admediumItem']['program']['$'];
					//$offname       = preg_split("/[ ]/", $cont_offname);
				    $old_offer_name  = trim($cont_offname, " BR");
				    $offer_name      = $old_offer_name;
		        	//echo$offer_name= $offname[0]; echo "<br>";
					//}
					//exit;

				    $api_coupon_id   = $cont['@id'];

				    $zanox_program_id= $cont['admedia']['admediumItem']['program']['@id'];

					$title 		     = $cont['admedia']['admediumItem']['title'];
					$description   	 = $cont['admedia']['admediumItem']['description'];
					$code 		   	 = $cont['couponCode'];
					$offer_page 	 = $cont['admedia']['admediumItem']['trackingLinks']['trackingLink'][0]['ppc'];
					$start_date  	 = date('Y-m-d',strtotime($cont['startDate']));	
					/*New code for expiry date field 7-2-17*/ 
					if(isset($cont['endDate']))
					{
						$expiry_date 	 = date('Y-m-d',strtotime($cont['endDate']));
					}
					else
					{
						$ad_expiry_date = $this->db->query("SELECT * from admin where admin_id=1")->row('coupon_expiry_date');
						$expiry_date    = date('Y-m-d',strtotime($ad_expiry_date));
					}
					/*End 7-2-17*/

				//}

					$tracking 		 = $cont['admedia']['admediumItem']['trackingLinks']['trackingLink'][0]['@adspaceId'];
					
					if($cont['couponCode']!='')
		            {
		                $type ='Coupon';
		            }
		            else
		            {
		                $type ='Promotion';
		            }   	
					//Coupon table details end//


					//Categories table details start//
					$offer_category_name = $cont['admedia']['admediumItem']['category']['$'];
					//Categories table details End//
				
					//Add new category name in categories table//
					//New code hide 21-2-17//
					/*if($offer_category_name != "")
					{		
						$this->db->where('category_name',$offer_category_name);
						$cat = $this->db->get('categories');
						if($cat->num_rows()==0)
						{
							$seo_url  = $this->admin_model->seoUrl($offer_category_name);
							$data     = array(
								'category_name'   => $offer_category_name,
								'category_url'    => $seo_url,
								'category_status' => 0,						
							);
							$this->db->insert('categories',$data);					
							$new_category_id    = $this->db->insert_id();
							$new_subcategory_id = '';
						}
						else
						{
							$rst 			 	= $cat->row();
							$new_category_id 	= $rst->category_id;
							$new_subcategory_id = '';
						}					
					}*/
					//End 21-2-17//
					//End category table//

					//Add new store name in affiliates table//

					$this->db->where('zanox_pgm_id',$zanox_program_id);
					$aff = $this->db->get('affiliates');
					if($aff->num_rows()==0)
					{	
						$offer_url  = $this->admin_model->seoUrl($offer_name);
						$data = array(
							'affiliate_name'   => $offer_name,
							'affiliate_url'    => $offer_url,
							//'logo_url'	   => $offer_page,
							'zanox_pgm_id' 	   => $zanox_program_id,
							'affiliate_status' => '1',
						);
						$this->db->insert('affiliates',$data);
						$new_store_id = $this->db->insert_id();
					}
					/*else
					{
						$data = array(
							'zanox_pgm_id' => $zanox_program_id,
							);
						$this->db->where('affiliate_name',$offer_name);
						$updation = $this->db->update('affiliates',$data);
					}*/
					//End affiliates table details//

					/*new code for avoid repeatation coupons details 8-2-17*/
					$this->db->where('api_coupon_id',$api_coupon_id);
					$coupondetails = $this->db->get('coupons');
					if($coupondetails->num_rows()==0)
					{
						/*end 8-2-17*/

						$aff_name    = $this->db->query("SELECT * from affiliates where zanox_pgm_id='".$zanox_program_id."'")->row('affiliate_name');
						//echo $aff_name; exit;
						//$aff_name  = $getaff_name->affiliate_name;


						//Add coupons details in Coupon table//
						if($aff_name != "")
						{
							$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`, `code`, `offer_page`, `start_date`,`expiry_date`,`featured`,`exclusive`,`tracking`,`coupon_status`,`api_name`,`api_coupon_id`)
							VALUES ('$aff_name','$title', '$description','$type', '$code', '$offer_page', '$start_date','$expiry_date','$featured','$exclusive','$tracking','pending','$affiliate_name','$api_coupon_id')");
						}
						else
						{
							$duplicate+=1;
							$duplicate_promo_id .= $promo_id.', ';
						}
						//End coupons table details//
					}	
					 
					unset($cont);
				}
				$array['duplicate'] = $duplicate;
				$array['promo_id']  = rtrim($duplicate_promo_id,', ');
			}

			if($affiliate_name == 'cityads')
			{
				//echo "<pre>";print_r($content['data']['items']); exit;
				foreach($content['data']['items']['item'] as $cont)
				{
					//$duplicate = 1;
					$new_category_id='';

					//echo "<pre>"; print_r($cont);
					//Coupon table details start//
					$offer_name   = $cont['offer_name'];
					//echo $offer_name; echo "<br>";
					
					$title 	  	  = $cont['geo']['item']['title'];
					$description  = $cont['description'];
					//}exit;
					$code 		  = $cont['promo_code'];
					$offer_page   = str_replace('cityadspix','nfemo',$cont['url']);
					$start_date   = date('Y-m-d',strtotime($cont['start_date']));	
					//$expiry_date= $cont['active_to'];
					$tracking 	  = $cont['offer_id'];
					
					/*New code for expiry date field 7-2-17*/
					if(isset($cont['active_to']))
					{
						$expiry_date 	 = date('Y-m-d',strtotime($cont['active_to']));
					}
					else
					{
						$ad_expiry_date = $this->db->query("SELECT * from admin where admin_id=1")->row('coupon_expiry_date');
						$expiry_date    = date('d-m-Y',strtotime($ad_expiry_date));
					}
					/*End 7-2-17*/

					if($code !='')
					{
						$type = 'Coupon';	
					}
					else
					{
						$type = 'Promotion';
					}
 					//Coupon table details end//
					

					$api_coupon_id  = $cont['id'];
				    $cityads_pgm_id = $cont['offer_id'];


					//categories table details Start//	
					$offer_category_name = $cont['action_category_name'];
					//Categories table details End//	 
					
					//Add new category name in catgories table//
					//New code hide 21-2-17//
					/*if($offer_category_name!="")
					{	 		
						$this->db->where('category_name',$offer_category_name);
						$cat = $this->db->get('categories');
						if($cat->num_rows()==0)
						{
							
							$seo_url  = $this->admin_model->seoUrl($offer_category_name);
							$data     = array(
								'category_name'   => $offer_category_name,
								'category_url'    => $seo_url,
								'category_status' => 0,						
							);
							$this->db->insert('categories',$data);					
							$new_category_id    = $this->db->insert_id();
							$new_subcategory_id = '';
						}
						else
						{
							$rst 			 	= $cat->row();
							$new_category_id 	= $rst->category_id;
							$new_subcategory_id = '';
						}					
					}*/
					//End 21-2-17// 
					//Add new store name in affiliates table//
 					$aff_image  =  $cont['image'];
 					//$logo_url   =  $cont['url_frame'];
					
					$this->db->where('affiliate_name',$offer_name);
					$aff = $this->db->get('affiliates');
					if($aff->num_rows()==0)
					{	
						$offer_url  = $this->admin_model->seoUrl($offer_name);
						$data = array(
							'affiliate_name'   => $offer_name,
							'affiliate_url'    => $offer_url,
							'affiliate_logo'   => $aff_image,
							//'logo_url' 		   => $logo_url,
							'affiliate_status' => '1',
							'cityads_pgm_id'   => $cityads_pgm_id,
						);
						$this->db->insert('affiliates',$data);
						$new_store_id = $this->db->insert_id();
					}
					/*else
					{
						//$result 	  = $aff->row();
						//$new_store_id = $result->affiliate_id;
						$data = array(
							'cityads_pgm_id'   => $cityads_pgm_id,
							);
						$this->db->where('affiliate_name',$offer_name);
						$updation = $this->db->update('affiliates',$data);
					}*/
					/*new code for avoid repeatation coupons details 15-2-17*/
					$this->db->where('api_coupon_id',$api_coupon_id);
					$coupondetails = $this->db->get('coupons');
					if($coupondetails->num_rows()==0)
					{
						/*end 15-2-17*/
						//Add coupons details in Coupon table// 
						if($offer_name)
						{
							$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`, `code`, `offer_page`, `start_date`,`expiry_date`,`tracking`,`coupon_status`,`api_name`,`api_coupon_id`)
							VALUES ('$offer_name','$title', '$description','$type', '$code', '$offer_page', '$start_date','$expiry_date','$tracking','pending','$affiliate_name','$api_coupon_id')");
						}
						else
						{
							$duplicate+=1;
							$duplicate_promo_id .= $promo_id.', ';
						}
						//End coupons table details//
					}	
					unset($cont);
				}
			}
			
			if($affiliate_name == 'rakuten')
			{
				//echo "<pre>"; print_r($content['link']); exit;
				foreach($content['link'] as $cont)
				{
					//$duplicate = 1; 
					$new_category_id='';

					//Coupon table details Start 09-06-16.//
					$offer_name	 	 = $cont['advertisername'];
					$title 		     = $cont['couponrestriction'];
					$description     = $cont['offerdescription'];
					$code 		     = $cont['couponcode'];
					$offer_page 	 = $cont['clickurl'];
					$start_date  	 = date('Y-m-d',strtotime($cont['offerstartdate']));	
					//$expiry_date 	 = $cont['offerenddate'];	
					$tracking 		 = $cont['advertiserid'];
					
					/*New code for expiry date field 7-2-17*/
					if(isset($cont['offerenddate']))
					{
						$expiry_date 	 = date('Y-m-d',strtotime($cont['offerenddate']));
					}
					else
					{
						$ad_expiry_date = $this->db->query("SELECT * from admin where admin_id=1")->row('coupon_expiry_date');
						$expiry_date    = date('Y-m-d',strtotime($ad_expiry_date));
					}
					/*End 7-2-17*/


					if($code !='')
					{
						$type = 'Coupon';	
					}
					else
					{
						$type = 'Promotion';
					}
				    //Coupon table details end//
					 
				    $rakuten_pgm_id = $cont['advertiserid'];


					//categories table details Start 10-06-16.//
					$old_category_name = $cont['categories']['category'];
					if(is_array($old_category_name))
					{
						$category_name = $old_category_name[0];
					} 
					else
					{
						$category_name = $old_category_name;
					}
										
					//New code hide 21-2-17//
					/*if($category_name!="")
					{	   		
						$this->db->where('category_name',$category_name);
						$cat = $this->db->get('categories');
						if($cat->num_rows()==0)
						{	
							$seo_url  = $this->admin_model->seoUrl($category_name);
							$data     = array(
								'category_name'   => $category_name,
								'category_url'    => $seo_url,
								'category_status' => 0,						
							);
							$this->db->insert('categories',$data);					
							$new_category_id    = $this->db->insert_id();
							$new_subcategory_id = '';
						}
						else
						{
							$rst 			 	= $cat->row();
							$new_category_id 	= $rst->category_id;
							$new_subcategory_id = '';
						}					
					}*/
					//End//
					//Categories table details End//
					

					//Add new store name in affiliates table 10-06-16.//
					$this->db->where('affiliate_name',$offer_name);
					$aff = $this->db->get('affiliates');
					if($aff->num_rows()==0)
					{	
						$offer_url  = $this->admin_model->seoUrl($offer_name);
						$data = array(
							'affiliate_name'   => $offer_name,
							'affiliate_url'    => $offer_url,
							//'logo_url'		   => $offer_page,
							'affiliate_status' => '1',
							'rakuten_pgm_id'   => $rakuten_pgm_id,
						);
						$this->db->insert('affiliates',$data);
						$new_store_id = $this->db->insert_id();
					}
					/*else
					{
						$result 	  = $aff->row();
						$new_store_id = $result->affiliate_id;

						//New code for Api Pgm id update 9-2-17//
						$data = array(
								'rakuten_pgm_id'   => $rakuten_pgm_id,
								);
							$this->db->where('affiliate_name',$offer_name);
							$updation = $this->db->update('affiliates',$data);
						//End 9-2-17//	
					}*/
					
					/*new code for avoid repeatation coupons details 15-2-17*/
					$this->db->where('code',$code);
					//$this->db->where('offer_name',$offer_name);
					$coupondetails = $this->db->get('coupons');
					if($coupondetails->num_rows()==0)
					{
						/*end 15-2-17*/
						//Add coupons details in Coupon table// 
						if($offer_name !='')
						{
							$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`, `code`, `offer_page`, `start_date`,`expiry_date`,`tracking`,`coupon_status`,`api_name`,`api_coupon_id`)
							VALUES ('$offer_name','$title', '$description','$type', '$code', '$offer_page', '$start_date','$expiry_date','$tracking','pending','$affiliate_name','$api_coupon_id')");
						}
						else
						{
							$duplicate+=1;
							$duplicate_promo_id .= $promo_id.', ';
						}
						//End coupons table details//
					}	
					unset($cont);
					$ex++;
				}
			}
 
			if($affiliate_name == 'afilio')
			{
				//echo "<pre>"; print_r($content); exit;
				foreach($content as $cont)
				{
					//$duplicate = 1;
					$new_category_id='';

					//Coupon table details start// 

					$category_id   = $cont['progid'];
					$selqry        = $this->db->query("SELECT * from afilio_category_name_details where category_id='$category_id'")->row(); 
					$offer_name    = utf8_encode($selqry->category_name);
					
					if($offer_name == '')
					{
						$offer_name = utf8_encode($cont['title']);
					}
					//echo $offer_name; echo "<br>";
					//echo $offer_url; echo "<br>";
					//}
					//exit;

					//$offer_name    = $cont['title'];
					$title 		     = $cont['title']; 
					$description 	 = utf8_encode($cont['description']);
					$code        	 = $cont['code'];
					$type 		 	 = $cont['rule'];
					$offer_page 	 = $cont['url'];
					$start_date  	 = $cont['startdate'];	
					//$expiry_date 	 = $cont['enddate'];
					//coupon table details End//
					/*New code for expiry date field 7-2-17*/
					//}
					//exit;
					if(isset($cont['enddate']))
					{
						$expiry_date 	 = date('Y-m-d',strtotime($cont['enddate']));
					}
					else
					{
						$ad_expiry_date = $this->db->query("SELECT * from admin where admin_id=1")->row('coupon_expiry_date');
						$expiry_date    = date('Y-m-d',strtotime($ad_expiry_date));
					}

					$afilio_pgm_id  = $cont['progid'];
					$api_coupon_id  = $cont['id']; 

					//Add new category name//
					$category_id   = $cont['progid'];
					$selqry        = $this->db->query("SELECT category_name from afilio_category_name_details where category_id='$category_id'")->row(); 
					$category_name = $selqry->category_name;
					
					//New code hide 21-2-17// 
					/*if($category_name!="")
					{
						$this->db->where('category_name',$category_name);
						$cat = $this->db->get('categories');
						if($cat->num_rows()==0)
						{
							
							$seo_url  = $this->admin_model->seoUrl($category_name);
							$data     = array(
								'category_name'   => $category_name,
								'category_url'    => $seo_url,
								'category_status' => 0,						
							);
							$this->db->insert('categories',$data);					
							$new_category_id    = $this->db->insert_id();
							$new_subcategory_id = '';
						}
						else
						{
							$rst 			 	= $cat->row();
							$new_category_id 	= $rst->category_id;
							$new_subcategory_id = '';
						}					
					}*/
					//End//
					 
					//Add new store name//
					$this->db->where('affiliate_name',$offer_name);
					$aff = $this->db->get('affiliates');
					if($aff->num_rows()==0)
					{
						$this->db->where('affiliate_name',$offer_name);
						$aff = $this->db->get('affiliates');
						if($aff->num_rows()==0)
						{	
							$offer_url  = $this->admin_model->seoUrl($offer_name);
							$data = array(
								'affiliate_name'   => $offer_name,
								'affiliate_url'    => $offer_url,
								'affiliate_status' => '1',
								'afilio_pgm_id'    => $afilio_pgm_id,
							);
							$this->db->insert('affiliates',$data);
							$new_store_id = $this->db->insert_id();
						}
						 else
						{
							$data = array(
								'afilio_pgm_id'    => $afilio_pgm_id,
							);
							$this->db->insert('affiliates',$data);

							$result 	  = $aff->row();
							$new_store_id = $result->affiliate_id;
						} 
					}

					/*new code for avoid repeatation coupons details 17-2-17*/
					$this->db->where('api_coupon_id',$api_coupon_id);
					$coupondetails = $this->db->get('coupons');
					if($coupondetails->num_rows()==0)
					{
						/*end 17-2-17*/
						if($offer_name)
						{
							$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`, `code`, `offer_page`, `start_date`,`expiry_date`,`coupon_status`,`api_name`,`api_coupon_id`)
							VALUES ('$offer_name','$title', '$description','$type', '$code', '$offer_page', '$start_date','$expiry_date','pending','$affiliate_name','$api_coupon_id')");
							
						}
						else
						{
							$duplicate+=1;
							$duplicate_promo_id .= $promo_id.', ';
						}
					}	
					unset($cont);
				}
			}

			
		}	
		return $array;		
	}
	/*End*/

	/*New code for update api coupons page 7-2-17*/
	function update_coupon($api_name) 
	{
		$this->db->connection_check();

		$start_date  = $this->input->post('start_date');
		$start_date  = date('Y-m-d',strtotime($start_date));
		$expiry_date = $this->input->post('expiry_date');
		$coupon_id 	 = $this->input->post('coupon_id');
		
		/*New code for expiry date 10-10-16*/
		if($expiry_date == '')
		{
			$expiry_date = $this->db->query("SELECT * from `admin` where admin_id=1")->row('coupon_expiry_date');
		}
		/*end 10-10-16*/		
			
		$expiry_date = date('Y-m-d',strtotime($expiry_date));
		$url_type 	 = $this->input->post('url_type');

		if($url_type == 2)
		{
			$url_link_type = 'Standard';
		}
		else if($url_type == 3)
		{
			$url_link_type = 'Deeplink';
		}
		else
		{
			$url_link_type = 'Paste';
		}

		$type = $this->input->post('type');
		if($type == 1)
		{
			$type = 'Promotion';
		}
		else
		{
 			$type = 'Coupon';
		}	


		$data = array(
			'offer_name' =>$this->input->post('offer_name'),
			'expiry_date'=>$expiry_date,
			'start_date' =>$start_date,
			'title'      =>$this->input->post('title'),
			'description'=>$this->input->post('description'),
			'type' 		 =>$type,
			'code' 		 =>$this->input->post('code'),
			'cashback_description'=>$this->input->post('cashback_description'),
			/*New code for link details 15-10-16*/
			'url_link_type' 	  =>$url_link_type,
			'deeplink_url'  	  =>$this->input->post('deeplink_url'),
			/*end 15-10-16*/
			'offer_page'    	  =>$this->input->post('offer_page'),
			'coupon_options'	  =>$this->input->post('coupon_options'),
			'Tracking'      	  =>$this->input->post('Tracking'),
			'extra_tracking_param'=>$this->input->post('extra_tracking'),
			'coupon_status' 	  => 'completed'
		);
		$this->db->where('coupon_id',$coupon_id);
		$updation = $this->db->update('coupons',$data);
		if($updation!="")
		{
			return true;
		}
		else 
		{ 
			return false;
		}
	}
	/*End 7-2-17*/
	/*new code for get affiliates details with API id 23-2-17*/
	function get_affiliates_zanox_Id()
	{
		$this->db->connection_check();
		$result = $this->db->query("SELECT * from affiliates where `zanox_pgm_id`!='' order by affiliate_id desc");
		if($result->num_rows > 0)
		{
			return $result->result();
		}
		return false;
	}
	function get_affiliates_lomadee_Id()
	{
		$this->db->connection_check();
		$result = $this->db->query("SELECT * from affiliates where `lomadee_pgm_id`!='' order by affiliate_id desc");
		if($result->num_rows > 0)
		{
			return $result->result();
		}
		return false;
	}
	function get_affiliates_rakuten_Id()
	{
		$this->db->connection_check();
		$result = $this->db->query("SELECT * from affiliates where `rakuten_pgm_id`!='' order by affiliate_id desc");
		if($result->num_rows > 0)
		{
			return $result->result();
		}
		return false;
	}
	 
	function get_affiliates_cityads_Id()
	{
		$this->db->connection_check();
		$result = $this->db->query("SELECT * from affiliates where `cityads_pgm_id`!='' order by affiliate_id desc");
		if($result->num_rows > 0)
		{
			return $result->result();
		}
		return false;
	}
	function get_affiliates_afilio_Id()
	{
		$this->db->connection_check();
		$result = $this->db->query("SELECT Distinct afilio_pgm_id,affiliate_name from affiliates where `afilio_pgm_id`!='' order by affiliate_id desc");//Distinct(afilio_pgm_id)
		if($result->num_rows > 0)
		{
			return $result->result();
		}
		return false;
	}
	/*end*/

	/*New code for update exit popup code 21-03-17*/
	function update_first_access_popup()
	{
		$this->db->connection_check();
		$first_popup_id = $this->input->post('first_popup_id');
			
		$data = array(
			'first_popup_subject'      => $this->input->post('first_popup_subject'),
			'first_popup_template'     => $this->input->post('first_popup_template'),
			'store_firstpopup_template'=> $this->input->post('store_firstpopup_template'),
			'store_firstpopup_template_nocash' => $this->input->post('store_firstpopup_template_nocash'),
			'all_status_firstpopup'    => $this->input->post('all_status_firstpopup'),
			'store_status_firstpopup'  => $this->input->post('store_status_firstpopup')
		);
		
		$this->db->where('first_popup_id',$first_popup_id);
		$update = $this->db->update('first_access_popup',$data);
		if($update!="")
		{
			return true;
		}
		else 
		{ 
			return false;   
		}	
	}
	/*End 21-03-17*/

	//check Email 4-4-17
	function check_email($email)
	{
		$this->db->connection_check();
		$this->db->where('email',$email);
		$this->db->where('admin_status','');
		
		$qry = $this->db->get('tbl_users');
		$numrows1 = $qry->num_rows();
		if($numrows1 == 0)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}

	function update_user_details($text_value,$user_id,$selectname)
	{
		$this->db->connection_check();
		if($user_id!='')
		{
			$data = array(
				$selectname => $text_value,
			);

			$this->db->where('user_id',$user_id);
			$update_qry = $this->db->update('tbl_users',$data);
			return true;	
		}
		else
		{
			return false;
		}
	}

	function check_cpf($check_cpf) 
	{
    	
    	$cpf = $check_cpf;
	    //Check if the number was informed
	    if(empty($cpf)) 
	    {
	        return false;
	    }
	 
	    // removes possible masks in the number eg: turns  911.139.032-87   into   91113903287
	    $cpf = ereg_replace('[^0-9]', '', $cpf);
	    $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
	     
	    // check if the number informed has 11 digits
	    if (strlen($cpf) != 11) {
	        return false;
	    }
	    // check if one of sequences bellow is typed 
	    // Case positive, returns FALSE
	    else if ($cpf == '00000000000' || 
	        $cpf == '11111111111' || 
	        $cpf == '22222222222' || 
	        $cpf == '33333333333' || 
	        $cpf == '44444444444' || 
	        $cpf == '55555555555' || 
	        $cpf == '66666666666' || 
	        $cpf == '77777777777' || 
	        $cpf == '88888888888' || 
	        $cpf == '99999999999') {
	        return false;
	     // Do the math to verify if the IFSC_number is alid or not
	     } else {   
	         
	        for ($t = 9; $t < 11; $t++) {
	             
	            for ($d = 0, $c = 0; $c < $t; $c++) {
	                $d += $cpf{$c} * (($t + 1) - $c);
	            }
	            $d = ((10 * $d) % 11) % 10;
	            if ($cpf{$c} != $d) {
	                return false;
	            }
	        }
	 
	        return true;
	    }
	} 
}
?>

