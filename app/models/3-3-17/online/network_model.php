<?php
class Network_model extends CI_Model
{
	
	function Affiliate_details($aff_name)
	{
		$this->db->connection_check();
		$this->db->where('affiliate_network',$aff_name);
		$affiliates = $this->db->get('affiliates_list');
		if($affiliates->num_rows > 0){
			return $affiliates->row();
		}
		return false;
	}
	function get_offer_provider_cashback($storename)
	{
		$this->db->connection_check();
		$this->db->like('affiliate_name', $storename);
		$this->db->limit(1,0);
		$result = $this->db->get('affiliates');
		if($result->num_rows > 0){
			return $result->row();
		}
		return false;
	}
	function check_ref_user($ref_user)
	{
		$this->db->connection_check();
		$this->db->where('user_id',$ref_user);        
		$result = $this->db->get('tbl_users');
		if($result->num_rows > 0){
			return $result->row('refer');
		}
		return false;
	}
	function check_active_user($user_id)
	{
		
		$this->db->where('user_id',$user_id);
		$this->db->where('status','1');
		$this->db->where('admin_status','');
		$ret = $this->db->get('tbl_users');
		if($ret->num_rows>0){
			return 1;
		}
		return 0;
	}
	// view user details
	function view_user($userid)
	{
		$this->db->connection_check();
		$this->db->where('user_id',$userid);        
        $query = $this->db->get('tbl_users');
        if($query->num_rows >= 1)
		{
           $row = $query->row();
            return $query->result();
        }
        return false;
	}
	function get_affiliatename($category_ids)
	{

		$this->db->connection_check();
			$this->db->where('affiliate_id',$category_ids);
			$query = $this->db->get('affiliates');
			if($query->num_rows >= 1)
			{
			   $row = $query->row();
			   return $row;
			}
			return false;
	}
	/*new code for currency converter 24-6-16*/
	function currencyConverter($currency_from,$currency_to,$currency_input)
	{
	    $yql_base_url    = "http://query.yahooapis.com/v1/public/yql";
	    $yql_query 	     = 'select * from yahoo.finance.xchange where pair in ("'.$currency_from.$currency_to.'")';
	    $yql_query_url   = $yql_base_url . "?q=" . urlencode($yql_query);
	    $yql_query_url  .= "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
	    $yql_session 	 = curl_init($yql_query_url);
	    curl_setopt($yql_session, CURLOPT_RETURNTRANSFER,true);
	    $yqlexec 	     = curl_exec($yql_session);
	    $yql_json 		 =  json_decode($yqlexec,true);
	    $currency_output = (float) $currency_input*$yql_json['query']['results']['rate']['Rate'];

	    return $currency_output;
	}
	

	//Zanox tracking transcations
	function Zanox_trackingtransactions($content)
	{		
		$now = date('Y-m-d H:i:s');	
		$last_updated = $now;
		$tracking =$this->input->post('tracking_id');
		$name = $this->db->query("select * from admin")->row();	
		$ref_cashbcak_percent =  $name->referral_cashback;		
		if(count($content)!=0)
		{
			$array = array();
			$duplicate = 0;
			$duplicate_trans_id = '';				
			$sampless =0;
			
			
			$info=$_SERVER['REMOTE_ADDR'].'","'
			.$_SERVER['HTTP_USER_AGENT'].'","'
			.$_SERVER['HTTP_REFERER'].'","'
			.date("D dS M,Y h:i a").'"'."\n";
			
			$file= 'tracker_log.csv';
			$fp = fopen($file, "a");
			fputs($fp, $info);
			fclose($fp);
			
			foreach($content['saleItems']['saleItem'] as $cont)
			{			
				//Write a contents in logs file
				$myfile = fopen("trackinglogs.log", "a") or die("Unable to open file!");
				//$txt = "user id date";
				fwrite($myfile, $cont['@attributes']['id']. "\t" .$cont['reviewState']."\t" .$cont['trackingDate']. "\t" .$cont['modifiedDate']. "\t" .$cont['clickDate']. "\t" .$cont['adspace']. "\t" .$cont['admedium']. "\t" .$cont['program']. "\t" .$cont['clickId']. "\t" .$cont['clickInId']. "\t" .$cont['amount']. "\t" .$cont['commission']. "\t" .$cont['currency']. "\t" .$cont['gpps']['gpp']. "\n");
				fclose($myfile);
				
				$sampless++;				
				$store_name =$this->db->escape_str($cont['program']);
				$remove_cur = explode(' ',$store_name);
				$storenam = $remove_cur[0];
				
				$tracking1=$cont['gpps']['gpp'];
				
				$site_prefix= 'P0001';
				$pos = strpos($tracking1, $site_prefix);				
				if ($pos == false) {
					$tracking_id = '';
					continue;
				} else {
					$tracking_id = $tracking1;
				}
								
				$get_userid =  decode_userid($tracking_id);
				if($get_userid<=0 || $get_userid=='' )
				{
					continue;
				}
			
				$storeslisty = $this->get_offer_provider_cashback($storenam);
				$store_id = $storeslisty->affiliate_id;
				$cashback_percentage = $storeslisty->cashback_percentage;
				$sale_amount = $cont['amount'];
				$approved_payout = $cont['commission'];					
				if($storeslisty->affiliate_cashback_type=='Percentage')
				{						
					$cashback_amount = number_format(($sale_amount*$cashback_percentage)/100,2);
				}
				else
				{
					if($approved_payout>$cashback_percentage)
					{
						$cashback_amount = number_format(($cashback_percentage),2);
					}
				}
				
				$check_ref = $this->check_ref_user($get_userid);
				$ref_cashback_amount = 0;
				$ref_id = 0;
				$referred = 0;
				$txn_id_new = 0;
				if($check_ref>0)		
				{
					$ref_id  = $check_ref;
					$return = $this->check_active_user($ref_id);
					if($return==1)
					{
						$referred = 1;
						$ref_cashback_percent = $ref_cashbcak_percent;
						$ref_cashback_amount = number_format(($cashback_amount*$ref_cashbcak_percent)/100,2);
					}
				}
				
				$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
				
				$is_cashback = 1;	
				$affiliate_cashback_type=$storeslisty->affiliate_cashback_type;
				$transaction_id=$cont['clickId'];					
				$pay_out_amount =$cont['commission'];
				$sale_amount =$cont['amount'];
				$date = $cont['trackingDate'];
				$status = 'Approved';
				
				$this->db->where('transaction_id',$transaction_id);
				$result = $this->db->get('tbl_report');
				$now = date('Y-m-d H:i:s');	
				$last_updated = $now;
				if($result->num_rows == 0)
				{
					$results = $this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`, `is_cashback`, `cashback_percentage`, `affiliate_cashback_type`,`cashback_amount`, `ref_id`, `ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`) VALUES ('$storenam', '$date', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '$last_updated', '$is_cashback', '$cashback_percentage', '$affiliate_cashback_type','$cashback_amount', '$ref_id','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','$status');");	
					$insert_id = $this->db->insert_id();
					if($is_cashback!=0)	
					{	
						$now = date('Y-m-d H:i:s');	
						$mode = "Credited";	
						if($ref_cashback_amount!=0)	
						{
							$data = array(			
								'transation_amount' => $ref_cashback_amount,	
								'user_id' => $ref_id,	
								'transation_date' => $now,	
								'transation_reason' => 'Pending Referal Payment',	
								'mode' => $mode,	
								'details_id'=>'',	
								'table'=>'',	
								'transation_status' => 'Progress');
							$this->db->insert('transation_details',$data);
							$txn_id_new = $this->db->insert_id();
						}
	
						$data = array(
						'user_id' => $get_userid,	
						'coupon_id' => $storenam,	
						'affiliate_id' => $storenam,	
						'status' => 'Pending',	
						'cashback_amount'=>$cashback_amount,	
						'date_added' => $date,
						'referral' => $referred,
						'txn_id' => $txn_id_new
						);	
						$this->db->insert('cashback',$data);	
						
					/* mail for pending cashback */
					$user_detail = $this->view_user($get_userid);
					if($user_detail){
						foreach($user_detail as $user_detail_single){
							$referral_balance = $user_detail_single->balance;
							$user_email = $user_detail_single->email;
							$user_name = $user_detail_single->first_name.' '.$user_detail_single->last_name;
						}
					}
					$this->db->where('admin_id',1);
					$admin_det = $this->db->get('admin');
					if($admin_det->num_rows >0) 
					{    
						$admin = $admin_det->row();
						$admin_email = $admin->admin_email;
						$site_name = $admin->site_name;
						$admin_no = $admin->contact_number;
						$site_logo = $admin->site_logo;
					}
					
					$date =date('Y-m-d');
					
					$this->db->where('mail_id',12);
					$mail_template = $this->db->get('tbl_mailtemplates');
						if($mail_template->num_rows >0) 
						{
						   $fetch = $mail_template->row();
						   $subject = $fetch->email_subject;
						   $templete = $fetch->email_template;
						   // $url = base_url().'cashback/my_earnings/';
						   
							$this->load->library('email');
							
							$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
							);
							
							$sub_data = array(
								'###SITENAME###'=>$site_name
							);
							$subject_new = strtr($subject,$sub_data);
							
							// $this->email->initialize($config);
							 $this->email->set_newline("\r\n");
							   $this->email->initialize($config);
							   $this->email->from($admin_email);
							   $this->email->to($user_email);
							   $this->email->subject($subject_new);
						   
							$data = array(
								'###NAME###'=>$user_name,
								'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
								'###SITENAME###'=>$site_name,
								'###ADMINNO###'=>$admin_no,
								'###DATE###'=>$date,
								'###AMOUNT###'=>$cashback_amount
							);
						   
						   $content_pop=strtr($templete,$data);
							//$content_pop; echo $subject_new;
							
						   $this->email->message($content_pop);
						   $this->email->send();  
						}
					/* mail for pending cashback */
					}	
				}
				else{
					$duplicate+=1;
					$duplicate_trans_id .= $transaction_id.', ';
				}
			}
			$array['duplicate'] = $duplicate;
			$array['trans_id'] = rtrim($duplicate_trans_id,', ');		
		}
		//print_r($array);die;
		return $array;	
	}
	//lomadee tracking transcations
	function Lomadee_trackingtransactions($content)
	{
		
		$now = date('Y-m-d H:i:s');	
		$last_updated = $now;
		$tracking =$this->input->post('tracking_id');
		$name = $this->db->query("select * from admin")->row();	
		$ref_cashbcak_percent =  $name->referral_cashback;		
		if(count($content)!=0)
		{
			$array = array();
			$duplicate = 0;
			$duplicate_trans_id = '';				
			$sampless =0;
			
			
			$info=$_SERVER['REMOTE_ADDR'].'","'
			.$_SERVER['HTTP_USER_AGENT'].'","'
			.$_SERVER['HTTP_REFERER'].'","'
			.date("D dS M,Y h:i a").'"'."\n";
			
			$file= 'tracker_log.csv';
			$fp = fopen($file, "a");
			fputs($fp, $info);
			fclose($fp);
			
			foreach($content['item'] as $cont){			
				
				//Write a contents in logs file
				
             	$myfile = fopen("trackinglogs.log", "a") or die("Unable to open file!");
				//$txt = "user id date";
				fwrite($myfile, $cont['date']. "\t" .$cont['currency']."\t" .$cont['commission']. "\t" .$cont['associateId']. "\t" .$cont['site']['siteId']. "\t" .$cont['site']['siteName']. "\t" .$cont['advertiser']['advertiserId']. "\t" .$cont['advertiser']['advertiserName']. "\t" .$cont['events']['event']['eventId']. "\t" .$cont['events']['event']['eventName']. "\t" .$cont['events']['event']['gmv']. "\t" .$cont['application']['applicationId']. "\t" .$cont['application']['applicationName']. "\t" .$cont['transactionCode']. "\t" .$cont['transactionId']. "\t" .$cont['Product Name']. "\t" .$cont['statusId']. "\t" .$cont['statusName']. "\t" .$cont['clickDate']. "\t" .$cont['checkoutDate']. "\n");
				fclose($myfile);
				
				$sampless++;				
				$storenam =$this->db->escape_str($cont['advertiser']['advertiserName']);			
				$offerurl_id=$this->db->escape_str($cont['events']['event']['eventId']);
				$offerurl_name=$this->db->escape_str($cont['events']['event']['eventName']);
				$tracking1=$cont['associateId'];
				
				//$tracking_id= 'KDID230149';
				if($tracking1!=''){
					$tracking_id = $tracking1;
				}else{
					$tracking_id = '';
					continue;
				}
				
				$get_userid =  decode_userid($tracking_id);
				if($get_userid<=0 || $get_userid=='' )
				{
					continue;
				}
			
				$storeslisty = $this->get_offer_provider_cashback($storenam);
				$store_id = $storeslisty->affiliate_id;
				$cashback_percentage = $storeslisty->cashback_percentage;
				$sale_amount = $cont['gmv'];
				$approved_payout = $cont['commission'];					
				if($storeslisty->affiliate_cashback_type=='Percentage')
				{						
					$cashback_amount = number_format(($sale_amount*$cashback_percentage)/100,2);
				}
				else
				{
					if($approved_payout>$cashback_percentage)
					{
						$cashback_amount = number_format(($cashback_percentage),2);
					}
				}
				
				$check_ref = $this->check_ref_user($get_userid);
				$ref_cashback_amount = 0;
				$ref_id = 0;
				$referred = 0;
				$txn_id_new = 0;
				if($check_ref>0)		
				{
					$ref_id  = $check_ref;
					$return = $this->check_active_user($ref_id);
					if($return==1)
					{
						$referred = 1;
						$ref_cashback_percent = $ref_cashbcak_percent;
						$ref_cashback_amount = number_format(($cashback_amount*$ref_cashbcak_percent)/100,2);
					}
				}
				
				$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
				
				$is_cashback = 1;	
				$affiliate_cashback_type=$storeslisty->affiliate_cashback_type;
				$transaction_id=$cont['transactionId'];					
				$pay_out_amount =$cont['commission'];
				$sale_amount =$cont['gmv'];
				$date = $cont['date'];
				$offerid = $cont['events']['event']['eventId'];
				$status =  $cont['statusName'];
				
				$this->db->where('transaction_id',$transaction_id);
				$result = $this->db->get('tbl_report');
				$now = date('Y-m-d H:i:s');	
				$last_updated = $now;
				if($result->num_rows == 0)
				{
					$results = $this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`, `is_cashback`, `cashback_percentage`, `affiliate_cashback_type`,`cashback_amount`, `ref_id`, `ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`) VALUES ('$storenam', '$date', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '$last_updated', '$is_cashback', '$cashback_percentage', '$affiliate_cashback_type','$cashback_amount', '$ref_id','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','$status');");	
					$insert_id = $this->db->insert_id();
					if($is_cashback!=0)	
					{	
						$now = date('Y-m-d H:i:s');	
						$mode = "Credited";	
						if($ref_cashback_amount!=0)	
						{
							$data = array(			
								'transation_amount' => $ref_cashback_amount,	
								'user_id' => $ref_id,	
								'transation_date' => $now,	
								'transation_reason' => 'Pending Referal Payment',	
								'mode' => $mode,	
								'details_id'=>'',	
								'table'=>'',	
								'transation_status' => 'Progress');
							$this->db->insert('transation_details',$data);
							$txn_id_new = $this->db->insert_id();
						}
	
						$data = array(
						'user_id' => $get_userid,	
						'coupon_id' => $storenam,	
						'affiliate_id' => $storenam,	
						'status' => 'Pending',	
						'cashback_amount'=>$cashback_amount,	
						'date_added' => $date,
						'referral' => $referred,
						'txn_id' => $txn_id_new
						);	
						$this->db->insert('cashback',$data);	
						
					/* mail for pending cashback */
					$user_detail = $this->view_user($get_userid);
					if($user_detail){
						foreach($user_detail as $user_detail_single){
							$referral_balance = $user_detail_single->balance;
							$user_email = $user_detail_single->email;
							$user_name = $user_detail_single->first_name.' '.$user_detail_single->last_name;
						}
					}
					$this->db->where('admin_id',1);
					$admin_det = $this->db->get('admin');
					if($admin_det->num_rows >0) 
					{    
						$admin = $admin_det->row();
						$admin_email = $admin->admin_email;
						$site_name = $admin->site_name;
						$admin_no = $admin->contact_number;
						$site_logo = $admin->site_logo;
					}
					
					$date =date('Y-m-d');
					
					$this->db->where('mail_id',12);
					$mail_template = $this->db->get('tbl_mailtemplates');
						if($mail_template->num_rows >0) 
						{
						   $fetch = $mail_template->row();
						   $subject = $fetch->email_subject;
						   $templete = $fetch->email_template;
						   // $url = base_url().'cashback/my_earnings/';
						   
							$this->load->library('email');
							
							$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
							);
							
							$sub_data = array(
								'###SITENAME###'=>$site_name
							);
							$subject_new = strtr($subject,$sub_data);
							
							// $this->email->initialize($config);
							 $this->email->set_newline("\r\n");
							   $this->email->initialize($config);
							   $this->email->from($admin_email);
							   $this->email->to($user_email);
							   $this->email->subject($subject_new);
						   
							$data = array(
								'###NAME###'=>$user_name,
								'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
								'###SITENAME###'=>$site_name,
								'###ADMINNO###'=>$admin_no,
								'###DATE###'=>$date,
								'###AMOUNT###'=>$cashback_amount
							);
						   
						   $content_pop=strtr($templete,$data);
							//$content_pop; echo $subject_new;
							
						   $this->email->message($content_pop);
						   $this->email->send();  
						}
					/* mail for pending cashback */
					}	
				}
				else{
					$duplicate+=1;
					$duplicate_trans_id .= $transaction_id.', ';
				}
			}
			$array['duplicate'] = $duplicate;
			$array['trans_id'] = rtrim($duplicate_trans_id,', ');		
		}
		//print_r($array);die;
		return $array;	
	}
	//Cityads tracking transactions
	function importCityads_trackingtransactions($content)
	{
		$now = date('Y-m-d H:i:s');	
		$last_updated = $now;
		$tracking =$this->input->post('tracking_id');
		$name = $this->db->query("select * from admin")->row();	
		$ref_cashbcak_percent =  $name->referral_cashback;	
		if(count($content)!=0)
		{					
			$array = array();
			$duplicate = 0;
			$duplicate_trans_id = '';				
			$sampless =0;		
				
			
			$info=$_SERVER['REMOTE_ADDR'].'","'
			.$_SERVER['HTTP_USER_AGENT'].'","'
			.$_SERVER['HTTP_REFERER'].'","'
			.date("D dS M,Y h:i a").'"'."\n";
			
			$file= 'tracker_log.csv';
			$fp = fopen($file, "a");
			fputs($fp, $info);
			fclose($fp);
			
			foreach($content['data']['items'] as $cont)
			{	
				//Write a contents in logs file
		
				$myfile = fopen("trackinglogs.log", "a") or die("Unable to open file!");
				//$txt = "user id date";
				fwrite($myfile, $cont['offerID']. "\t" .$cont['offerName']."\t" .$cont['basketQTY']. "\t" .$cont['campaignTargetName']. "\t" .$cont['clickTime]']. "\t" .$cont['commissionOpen']. "\t" .$cont['commissionCancelled']. "\t" .$cont['commissionApproved']. "\t" .$cont['originalCurrency']. "\t" .$cont['customerType']. "\t" .$cont['conversion']. "\t" .$cont['leadDelta']. "\t" .$cont['leadTime']. "\t" .$cont['orderID']. "\t" .$cont['orderTotal']. "\t" .$cont['originalBasket']. "\t" .$cont['paymentMethod'].  "\t" .$cont['commission']. "\t" .$cont['saleDelta']. "\t" .$cont['saleTime']. "\t" .$cont['status']. "\t" .$cont['subaccount']. "\t" .$cont['subaccount2']. "\t" .$cont['subaccount3'].  "\t" .$cont['subaccount4']. "\t" .$cont['subaccount5']. "\t" .$cont['submissionID']. "\t" .$cont['wmCurrency']. "\t" .$cont['xid']. "\n");
				fclose($myfile);
				
				$sampless++;				
				$storenam =$this->db->escape_str($cont['offerName']);
				
				$tracking1=$cont['subaccount'];
				
				//$tracking_id= 'KDID230149';
				if($tracking1!=''){
					$tracking_id = $tracking1;
				}else{
					$tracking_id = '';
					continue;
				}
				
				$get_userid =  decode_userid($tracking_id);
				if($get_userid<=0 || $get_userid=='' )
				{
					continue;
				}
			
				$storeslisty = $this->get_offer_provider_cashback($storenam);
				$store_id = $storeslisty->affiliate_id;
				$cashback_percentage = $storeslisty->cashback_percentage;
				$sale_amount = $cont['commissionOpen'];
				$approved_payout = $cont['commission'];					
				if($storeslisty->affiliate_cashback_type=='Percentage')
				{						
					$cashback_amount = number_format(($sale_amount*$cashback_percentage)/100,2);
				}
				else
				{
					if($approved_payout>$cashback_percentage)
					{
						$cashback_amount = number_format(($cashback_percentage),2);
					}
				}
				
				$check_ref = $this->check_ref_user($get_userid);
				$ref_cashback_amount = 0;
				$ref_id = 0;
				$referred = 0;
				$txn_id_new = 0;
				if($check_ref>0)		
				{
					$ref_id  = $check_ref;
					$return = $this->check_active_user($ref_id);
					if($return==1)
					{
						$referred = 1;
						$ref_cashback_percent = $ref_cashbcak_percent;
						$ref_cashback_amount = number_format(($cashback_amount*$ref_cashbcak_percent)/100,2);
					}
				}
				
				$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
				
				$is_cashback = 1;	
				$affiliate_cashback_type=$storeslisty->affiliate_cashback_type;
				$transaction_id=$cont['submissionID'];					
				$pay_out_amount =$cont['commission'];
				$sale_amount =$cont['commissionOpen'];
				$date = $cont['leadTime'];
				//$offerid = $cont['Offer ID'];
				$status = $cont['status'];
				
				$this->db->where('transaction_id',$transaction_id);
				$result = $this->db->get('tbl_report');
				$now = date('Y-m-d H:i:s');	
				$last_updated = $now;
				if($result->num_rows == 0)
				{
					$results = $this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`, `is_cashback`, `cashback_percentage`, `affiliate_cashback_type`,`cashback_amount`, `ref_id`, `ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`) VALUES ('$storenam', '$date', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '$last_updated', '$is_cashback', '$cashback_percentage', '$affiliate_cashback_type','$cashback_amount', '$ref_id','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','$status');");	
					$insert_id = $this->db->insert_id();
					if($is_cashback!=0)	
					{	
						$now = date('Y-m-d H:i:s');	
						$mode = "Credited";	
						if($ref_cashback_amount!=0)	
						{
							$data = array(			
								'transation_amount' => $ref_cashback_amount,	
								'user_id' => $ref_id,	
								'transation_date' => $now,	
								'transation_reason' => 'Pending Referal Payment',	
								'mode' => $mode,	
								'details_id'=>'',	
								'table'=>'',	
								'transation_status' => 'Progress');
							$this->db->insert('transation_details',$data);
							$txn_id_new = $this->db->insert_id();
						}
	
						$data = array(
						'user_id' => $get_userid,	
						'coupon_id' => $storenam,	
						'affiliate_id' => $storenam,	
						'status' => 'Pending',	
						'cashback_amount'=>$cashback_amount,	
						'date_added' => $date,
						'referral' => $referred,
						'txn_id' => $txn_id_new
						);	
						$this->db->insert('cashback',$data);	
						
					/* mail for pending cashback */
					$user_detail = $this->view_user($get_userid);
					if($user_detail){
						foreach($user_detail as $user_detail_single){
							$referral_balance = $user_detail_single->balance;
							$user_email = $user_detail_single->email;
							$user_name = $user_detail_single->first_name.' '.$user_detail_single->last_name;
						}
					}
					$this->db->where('admin_id',1);
					$admin_det = $this->db->get('admin');
					if($admin_det->num_rows >0) 
					{    
						$admin = $admin_det->row();
						$admin_email = $admin->admin_email;
						$site_name = $admin->site_name;
						$admin_no = $admin->contact_number;
						$site_logo = $admin->site_logo;
					}
					
					$date =date('Y-m-d');
					
					$this->db->where('mail_id',12);
					$mail_template = $this->db->get('tbl_mailtemplates');
						if($mail_template->num_rows >0) 
						{
						   $fetch = $mail_template->row();
						   $subject = $fetch->email_subject;
						   $templete = $fetch->email_template;
						   // $url = base_url().'cashback/my_earnings/';
						   
							$this->load->library('email');
							
							$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
							);
							
							$sub_data = array(
								'###SITENAME###'=>$site_name
							);
							$subject_new = strtr($subject,$sub_data);
							
							// $this->email->initialize($config);
							 $this->email->set_newline("\r\n");
							   $this->email->initialize($config);
							   $this->email->from($admin_email);
							   $this->email->to($user_email);
							   $this->email->subject($subject_new);
						   
							$data = array(
								'###NAME###'=>$user_name,
								'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
								'###SITENAME###'=>$site_name,
								'###ADMINNO###'=>$admin_no,
								'###DATE###'=>$date,
								'###AMOUNT###'=>$cashback_amount
							);
						   
						   $content_pop=strtr($templete,$data);
							//$content_pop; echo $subject_new;
							
						   $this->email->message($content_pop);
						   $this->email->send();  
						}
					/* mail for pending cashback */
					}	
				}
				else{
					$duplicate+=1;
					$duplicate_trans_id .= $transaction_id.', ';
				}						
			}
			$array['duplicate'] = $duplicate;
			$array['trans_id'] = rtrim($duplicate_trans_id,', ');		
		}
		//print_r($array);die;
		return $array;	
	}
	//Afilio tracking transactions
	function importafilio_trackingtransactions($content)
	{
		
		$now = date('Y-m-d H:i:s');	
		$last_updated = $now;
		$tracking =$this->input->post('tracking_id');
		$name = $this->db->query("select * from admin")->row();	
		$ref_cashbcak_percent =  $name->referral_cashback;	
		if(count($content)!=0)
		{		
			
			$array = array();
			$duplicate = 0;
			$duplicate_trans_id = '';				
			$sampless =0;		
				
				
			$info=$_SERVER['REMOTE_ADDR'].'","'
			.$_SERVER['HTTP_USER_AGENT'].'","'
			.$_SERVER['HTTP_REFERER'].'","'
			.date("D dS M,Y h:i a").'"'."\n";
			
			$file= 'tracker_log.csv';
			$fp = fopen($file, "a");
			fputs($fp, $info);
			fclose($fp);
			
			foreach($content['list'] as $contss){			
				foreach($contss as $conts)
				{	
					foreach($conts as $cont)
					{	
						//Write a contents in logs file
				
						$myfile = fopen("trackinglogs.log", "a") or die("Unable to open file!");
						//$txt = "user id date";
						fwrite($myfile, $cont['saleid']. "\t" .$cont['status']."\t" .$cont['progid']. "\t" .$cont['order_id']. "\t" .$cont['order_price]']. "\t" .$cont['comission']. "\t" .$cont['date']. "\t" .$cont['payment']. "\t" .$cont['xtra']. "\n");
						fclose($myfile);
						
						$sampless++;				
						$storeid =$this->db->escape_str($cont['progid']);
						$storenam = $this->get_affiliatename($storeid);
				
				
						$tracking1=$cont['xtra'];
						
						//$tracking_id= 'KDID230149';
						if($tracking1!=''){
							$tracking_id = $tracking1;
						}else{
							$tracking_id = '';
							continue;
						}
						
						$get_userid =  decode_userid($tracking_id);
						if($get_userid<=0 || $get_userid=='' )
						{
							continue;
						}
					
						$storeslisty = $this->get_offer_provider_cashback($storenam->affiliate_name);
						$store_id = $storeslisty->affiliate_id;
						$cashback_percentage = $storeslisty->cashback_percentage;
						$sale_amount = $cont['order_price'];
						$approved_payout = $cont['comission'];					
						if($storeslisty->affiliate_cashback_type=='Percentage')
						{						
							$cashback_amount = number_format(($sale_amount*$cashback_percentage)/100,2);
						}
						else
						{
							if($approved_payout>$cashback_percentage)
							{
								$cashback_amount = number_format(($cashback_percentage),2);
							}
						}
						
						$check_ref = $this->check_ref_user($get_userid);
						$ref_cashback_amount = 0;
						$ref_id = 0;
						$referred = 0;
						$txn_id_new = 0;
						if($check_ref>0)		
						{
							$ref_id  = $check_ref;
							$return = $this->check_active_user($ref_id);
							if($return==1)
							{
								$referred = 1;
								$ref_cashback_percent = $ref_cashbcak_percent;
								$ref_cashback_amount = number_format(($cashback_amount*$ref_cashbcak_percent)/100,2);
							}
						}
						
						$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
						
						$is_cashback = 1;	
						$affiliate_cashback_type=$storeslisty->affiliate_cashback_type;
						$transaction_id=$cont['saleid'];					
						$pay_out_amount =$cont['order_price'];
						$sale_amount =$cont['Sales'];
						$date = $cont['date'];
						//$offerid = $cont['Offer ID'];
						$status = $cont['status'];
						
						$this->db->where('transaction_id',$transaction_id);
						$result = $this->db->get('tbl_report');
						$now = date('Y-m-d H:i:s');	
						$last_updated = $now;
						if($result->num_rows == 0)
						{
							$results = $this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`, `is_cashback`, `cashback_percentage`, `affiliate_cashback_type`,`cashback_amount`, `ref_id`, `ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`) VALUES ('$storenam', '$date', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '$last_updated', '$is_cashback', '$cashback_percentage', '$affiliate_cashback_type','$cashback_amount', '$ref_id','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','$status');");	
							$insert_id = $this->db->insert_id();
							if($is_cashback!=0)	
							{	
								$now = date('Y-m-d H:i:s');	
								$mode = "Credited";	
								if($ref_cashback_amount!=0)	
								{
									$data = array(			
										'transation_amount' => $ref_cashback_amount,	
										'user_id' => $ref_id,	
										'transation_date' => $now,	
										'transation_reason' => 'Pending Referal Payment',	
										'mode' => $mode,	
										'details_id'=>'',	
										'table'=>'',	
										'transation_status' => 'Progress');
									$this->db->insert('transation_details',$data);
									$txn_id_new = $this->db->insert_id();
								}
			
								$data = array(
								'user_id' => $get_userid,	
								'coupon_id' => $storenam,	
								'affiliate_id' => $storenam,	
								'status' => 'Pending',	
								'cashback_amount'=>$cashback_amount,	
								'date_added' => $date,
								'referral' => $referred,
								'txn_id' => $txn_id_new
								);	
								$this->db->insert('cashback',$data);	
								
							/* mail for pending cashback */
							$user_detail = $this->view_user($get_userid);
							if($user_detail){
								foreach($user_detail as $user_detail_single){
									$referral_balance = $user_detail_single->balance;
									$user_email = $user_detail_single->email;
									$user_name = $user_detail_single->first_name.' '.$user_detail_single->last_name;
								}
							}
							$this->db->where('admin_id',1);
							$admin_det = $this->db->get('admin');
							if($admin_det->num_rows >0) 
							{    
								$admin = $admin_det->row();
								$admin_email = $admin->admin_email;
								$site_name = $admin->site_name;
								$admin_no = $admin->contact_number;
								$site_logo = $admin->site_logo;
							}
							
							$date =date('Y-m-d');
							
							$this->db->where('mail_id',12);
							$mail_template = $this->db->get('tbl_mailtemplates');
								if($mail_template->num_rows >0) 
								{
								   $fetch = $mail_template->row();
								   $subject = $fetch->email_subject;
								   $templete = $fetch->email_template;
								   // $url = base_url().'cashback/my_earnings/';
								   
									$this->load->library('email');
									
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);
									
									$sub_data = array(
										'###SITENAME###'=>$site_name
									);
									$subject_new = strtr($subject,$sub_data);
									
									// $this->email->initialize($config);
									 $this->email->set_newline("\r\n");
									   $this->email->initialize($config);
									   $this->email->from($admin_email);
									   $this->email->to($user_email);
									   $this->email->subject($subject_new);
								   
									$data = array(
										'###NAME###'=>$user_name,
										'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###AMOUNT###'=>$cashback_amount
									);
								   
								   $content_pop=strtr($templete,$data);
									//$content_pop; echo $subject_new;
									
								   $this->email->message($content_pop);
								   $this->email->send();  
								}
							/* mail for pending cashback */
							}	
						}
						else{
							$duplicate+=1;
							$duplicate_trans_id .= $transaction_id.', ';
						}
					}
				}			
			}
			$array['duplicate'] = $duplicate;
			$array['trans_id'] = rtrim($duplicate_trans_id,', ');		
		}
		//print_r($array);die;
		return $array;	
	}
	//Rakuten (linkshare) tracking transactions
	function import_trackingtransactions($content)
	{
		$now = date('Y-m-d H:i:s');	
		$last_updated = $now;
		$tracking =$this->input->post('tracking_id');
		$name = $this->db->query("select * from admin")->row();	
		$ref_cashbcak_percent =  $name->referral_cashback;	
		
		if(count($content)!=0)
		{
			$array = array();
			$duplicate = 0;
			$duplicate_trans_id = '';				
			$sampless =0;
			
			
			$info=$_SERVER['REMOTE_ADDR'].'","'
			.$_SERVER['HTTP_USER_AGENT'].'","'
			.$_SERVER['HTTP_REFERER'].'","'
			.date("D dS M,Y h:i a").'"'."\n";
			
			$file= 'tracker_log.csv';
			$fp = fopen($file, "a");
			fputs($fp, $info);
			fclose($fp);
			
			foreach($content as $cont){			

				//Write a contents in logs file
				
             	$myfile = fopen("trackinglogs.log", "a") or die("Unable to open file!");
				//$txt = "user id date";
				fwrite($myfile, $cont['MID']. "\t" .$cont['Advertiser Name']."\t" .$cont['Click Through Rate (CTR)']. "\t" .$cont['Orders/Click']. "\t" .$cont['Earnings Per Click (EPC)']. "\t" .$cont['Sales']. "\t" .$cont['Baseline Commission']. "\t" .$cont['Adjusted Commission']. "\t" .$cont['Total Commission']. "\t" .$cont['Currency']. "\t" .$cont['Member ID (U1)']. "\t" .$cont['Network']. "\t" .$cont['Network ID']. "\t" .$cont['Offer ID']. "\t" .$cont['Offer Name']. "\t" .$cont['Product Name']. "\t" .$cont['Transaction Date']. "\t" .$cont['Transaction ID']. "\t" .$cont['id']. "\n");
				fclose($myfile);
				
				$sampless++;				
				$storenam =$this->db->escape_str($cont['Advertiser Name']);			
				$offerurl_id=$this->db->escape_str($cont['Offer ID']);
				$offerurl_name=$this->db->escape_str($cont['Offer Name']);
				$tracking1=$cont['Member ID (U1)'];
				
				//$tracking_id= 'KDID230149';
				if($tracking1!=''){
					$tracking_id = $tracking1;
				}else{
					$tracking_id = '';
					continue;
				}
				
				$get_userid =  decode_userid($tracking_id);
				if($get_userid<=0 || $get_userid=='' )
				{
					continue;
				}
			
				$storeslisty = $this->get_offer_provider_cashback($storenam);
				$store_id = $storeslisty->affiliate_id;
				$cashback_percentage = $storeslisty->cashback_percentage;
				$sale_amount = $cont['Sales'];
				$approved_payout = $cont['Total Commission'];					
				if($storeslisty->affiliate_cashback_type=='Percentage')
				{						
					$cashback_amount = number_format(($sale_amount*$cashback_percentage)/100,2);
				}
				else
				{
					if($approved_payout>$cashback_percentage)
					{
						$cashback_amount = number_format(($cashback_percentage),2);
					}
				}
				
				$check_ref = $this->check_ref_user($get_userid);
				$ref_cashback_amount = 0;
				$ref_id = 0;
				$referred = 0;
				$txn_id_new = 0;
				if($check_ref>0)		
				{
					$ref_id  = $check_ref;
					$return = $this->check_active_user($ref_id);
					if($return==1)
					{
						$referred = 1;
						$ref_cashback_percent = $ref_cashbcak_percent;
						$ref_cashback_amount = number_format(($cashback_amount*$ref_cashbcak_percent)/100,2);
					}
				}
				
				$total_Cashback_paid = $cashback_amount+$ref_cashback_amount;
				
				$is_cashback = 1;	
				$affiliate_cashback_type=$storeslisty->affiliate_cashback_type;
				$transaction_id=$cont['Transaction ID'];					
				$pay_out_amount =$cont['Total Commission'];
				$sale_amount =$cont['Sales'];
				$date = $cont['Transaction Date'];
				$offerid = $cont['Offer ID'];
				$status = 'Approved';
				
				$this->db->where('transaction_id',$transaction_id);
				$result = $this->db->get('tbl_report');
				$now = date('Y-m-d H:i:s');	
				$last_updated = $now;
				if($result->num_rows == 0)
				{
					$results = $this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`, `is_cashback`, `cashback_percentage`, `affiliate_cashback_type`,`cashback_amount`, `ref_id`, `ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`) VALUES ('$storenam', '$date', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '$last_updated', '$is_cashback', '$cashback_percentage', '$affiliate_cashback_type','$cashback_amount', '$ref_id','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','$status');");	
					$insert_id = $this->db->insert_id();
					if($is_cashback!=0)	
					{	
						$now = date('Y-m-d H:i:s');	
						$mode = "Credited";	
						if($ref_cashback_amount!=0)	
						{
							$data = array(			
								'transation_amount' => $ref_cashback_amount,	
								'user_id' => $ref_id,	
								'transation_date' => $now,	
								'transation_reason' => 'Pending Referal Payment',	
								'mode' => $mode,	
								'details_id'=>'',	
								'table'=>'',	
								'transation_status' => 'Progress');
							$this->db->insert('transation_details',$data);
							$txn_id_new = $this->db->insert_id();
						}
	
						$data = array(
						'user_id' => $get_userid,	
						'coupon_id' => $storenam,	
						'affiliate_id' => $storenam,	
						'status' => 'Pending',	
						'cashback_amount'=>$cashback_amount,	
						'date_added' => $date,
						'referral' => $referred,
						'txn_id' => $txn_id_new
						);	
						$this->db->insert('cashback',$data);	
						
					/* mail for pending cashback */
					$user_detail = $this->view_user($get_userid);
					if($user_detail){
						foreach($user_detail as $user_detail_single){
							$referral_balance = $user_detail_single->balance;
							$user_email = $user_detail_single->email;
							$user_name = $user_detail_single->first_name.' '.$user_detail_single->last_name;
						}
					}
					$this->db->where('admin_id',1);
					$admin_det = $this->db->get('admin');
					if($admin_det->num_rows >0) 
					{    
						$admin = $admin_det->row();
						$admin_email = $admin->admin_email;
						$site_name = $admin->site_name;
						$admin_no = $admin->contact_number;
						$site_logo = $admin->site_logo;
					}
					
					$date =date('Y-m-d');
					
					$this->db->where('mail_id',12);
					$mail_template = $this->db->get('tbl_mailtemplates');
						if($mail_template->num_rows >0) 
						{
						   $fetch = $mail_template->row();
						   $subject = $fetch->email_subject;
						   $templete = $fetch->email_template;
						   // $url = base_url().'cashback/my_earnings/';
						   
							$this->load->library('email');
							
							$config = Array(
								'mailtype'  => 'html',
								'charset'   => 'utf-8',
							);
							
							$sub_data = array(
								'###SITENAME###'=>$site_name
							);
							$subject_new = strtr($subject,$sub_data);
							
							// $this->email->initialize($config);
							 $this->email->set_newline("\r\n");
							   $this->email->initialize($config);
							   $this->email->from($admin_email);
							   $this->email->to($user_email);
							   $this->email->subject($subject_new);
						   
							$data = array(
								'###NAME###'=>$user_name,
								'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
								'###SITENAME###'=>$site_name,
								'###ADMINNO###'=>$admin_no,
								'###DATE###'=>$date,
								'###AMOUNT###'=>$cashback_amount
							);
						   
						   $content_pop=strtr($templete,$data);
							//$content_pop; echo $subject_new;
							
						   $this->email->message($content_pop);
						   $this->email->send();  
						}
					/* mail for pending cashback */
					}	
				}
				else{
					$duplicate+=1;
					$duplicate_trans_id .= $transaction_id.', ';
				}
			}
			$array['duplicate'] = $duplicate;
			$array['trans_id'] = rtrim($duplicate_trans_id,', ');		
		}
		//print_r($array);die;
		return $array;	
	}


	/*New code for Coupon upload via API 7-6-16.*/
	/*function import_apicoupons($content,$affiliate_name)
	{
		//echo "hai"; exit;
		//echo "<pre>"; print_r($content); echo "<br>";
		//echo $affiliate_name; exit;
		$coupon_type = '';
		if(count($content)!=0)
		{

			$array 	   = array();
			$duplicate = 0;
			$duplicate_promo_id = '';
					
			if($affiliate_name == 'zanox')
			{
				//echo "<pre>"; print_r($content['incentiveItems']['incentiveItem']);exit;
				foreach($content['incentiveItems']['incentiveItem'] as $cont)
				{
					
					//$duplicate = 1;
					$new_category_id='';
					 
					//Coupon table details start 09-06-16.//
					$cont_offname    = $cont['admedia']['admediumItem']['program']['$'];
					//$offname       = preg_split("/[ ]/", $cont_offname);
				    //$offer_name    = trim($cont_offname, " BR");
				    $offer_name      = $cont_offname;
		        	//echo$offer_name= $offname[0]; echo "<br>";
					//}
					//exit;
					$title 		     = $cont['admedia']['admediumItem']['title'];
					$description   	 = $cont['admedia']['admediumItem']['description'];
					$code 		   	 = $cont['couponCode'];
					$offer_page 	 = $cont['admedia']['admediumItem']['trackingLinks']['trackingLink'][0]['ppc'];
					$start_date  	 = date('Y-m-d',strtotime($cont['startDate']));	
					$expiry_date 	 = date('Y-m-d',strtotime($cont['endDate']));	
					$tracking 		 = $cont['admedia']['admediumItem']['trackingLinks']['trackingLink'][0]['@adspaceId'];
					
					if($cont['couponCode']!='')
		            {
		                $type ='Coupon';
		            }
		            else
		            {
		                $type ='Promotion';
		            }   	
					//Coupon table details end//


					//Categories table details start//
					$offer_category_name = $cont['admedia']['admediumItem']['category']['$'];
					//Categories table details End//
				
					//Add new category name in categories table//
					if($offer_category_name != "")
					{	
	                    		
						$this->db->where('category_name',$offer_category_name);
						$cat = $this->db->get('categories');
						if($cat->num_rows()==0)
						{
							
							$seo_url  = $this->admin_model->seoUrl($offer_category_name);
							$data     = array(
								'category_name'   => $offer_category_name,
								'category_url'    => $seo_url,
								'category_status' => 0,						
							);
							$this->db->insert('categories',$data);					
							$new_category_id    = $this->db->insert_id();
							$new_subcategory_id = '';
						}
						else
						{
							$rst 			 	= $cat->row();
							$new_category_id 	= $rst->category_id;
							$new_subcategory_id = '';
						}					
					}
					//End category table//

					//Add new store name in affiliates table//

						$this->db->where('affiliate_name',$offer_name);
						$aff = $this->db->get('affiliates');
						if($aff->num_rows()==0)
						{	
							$offer_url  = $this->admin_model->seoUrl($offer_name);
							$data = array(
								'affiliate_name'   => $offer_name,
								'affiliate_url'    => $offer_url,
								//'logo_url'		   => $offer_page,
								'affiliate_status' => '1',
							);
							$this->db->insert('affiliates',$data);
							$new_store_id = $this->db->insert_id();
						}
					//End affiliates table details//

					//Add coupons details in Coupon table//
					if($offer_name != "")
					{
					 
						$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`, `code`, `offer_page`, `start_date`,`expiry_date`,`featured`,`exclusive`,`tracking`)
						VALUES ('$offer_name','$title', '$description','$type', '$code', '$offer_page', '$start_date','$expiry_date','$featured','$exclusive','$tracking')");
					}
					else
					{
						$duplicate+=1;
						$duplicate_promo_id .= $promo_id.', ';
					}
					//End coupons table details//
					 
					unset($cont);
				}
				$array['duplicate'] = $duplicate;
				$array['promo_id']  = rtrim($duplicate_promo_id,', ');
			}
			
			if($affiliate_name == 'rakuten')
			{
				//echo "<pre>"; print_r($content['link']); exit;
				foreach($content['link'] as $cont)
				{
					$duplicate = 1; 
					$new_category_id='';

					//Coupon table details Start 09-06-16.//
					$offer_name	 	 = $cont['advertisername'];
					$title 		     = $cont['couponrestriction'];
					$description     = $cont['offerdescription'];
					$code 		     = $cont['couponcode'];
					$offer_page 	 = $cont['clickurl'];
					$start_date  	 = date('Y-m-d',strtotime($cont['offerstartdate']));	
					$expiry_date 	 = date('Y-m-d',strtotime($cont['offerenddate']));	
					$tracking 		 = $cont['advertiserid'];
					if($code !='')
					{
						$type = 'Coupon';	
					}
					else
					{
						$type = 'Promotion';
					}
				    //Coupon table details end//
					
					//categories table details Start 10-06-16.//
					$old_category_name = $cont['categories']['category'];
					if(is_array($old_category_name))
					{
						$category_name = $old_category_name[0];
					} 
					else
					{
						$category_name = $old_category_name;
					}
					

					if($category_name!="")
					{	   		
						$this->db->where('category_name',$category_name);
						$cat = $this->db->get('categories');
						if($cat->num_rows()==0)
						{	
							$seo_url  = $this->admin_model->seoUrl($category_name);
							$data     = array(
								'category_name'   => $category_name,
								'category_url'    => $seo_url,
								'category_status' => 0,						
							);
							$this->db->insert('categories',$data);					
							$new_category_id    = $this->db->insert_id();
							$new_subcategory_id = '';
						}
						else
						{
							$rst 			 	= $cat->row();
							$new_category_id 	= $rst->category_id;
							$new_subcategory_id = '';
						}					
					}
					//Categories table details End//
					

					//Add new store name in affiliates table 10-06-16.//
					$this->db->where('affiliate_name',$offer_name);
					$aff = $this->db->get('affiliates');
					if($aff->num_rows()==0)
					{	
						$offer_url  = $this->admin_model->seoUrl($offer_name);
						$data = array(
							'affiliate_name'   => $offer_name,
							'affiliate_url'    => $offer_url,
							//'logo_url'		   => $offer_page,
							'affiliate_status' => '1',
						);
						$this->db->insert('affiliates',$data);
						$new_store_id = $this->db->insert_id();
					}
					else
					{
						$result 	  = $aff->row();
						$new_store_id = $result->affiliate_id;
					}
					
					//Add coupons details in Coupon table// 
					if($offer_name !='')
					{
						$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`, `code`, `offer_page`, `start_date`,`expiry_date`,`tracking`)
						VALUES ('$offer_name','$title', '$description','$type', '$code', '$offer_page', '$start_date','$expiry_date','$tracking')");
					}
					else
					{
						$duplicate+=1;
						$duplicate_promo_id .= $promo_id.', ';
					}
					//End coupons table details//
					unset($cont);
				}
			}
 
			if($affiliate_name == 'cityads')
			{
				//echo "<pre>";print_r($content['data']['items']); exit;
				foreach($content['data']['items'] as $cont)
				{
					$duplicate = 1;
					$new_category_id='';

					
					//Coupon table details start//
					$offer_name  		 = utf8_decode($cont['offer_name']);
					//echo $offer_name; echo "<br>";
					//}exit;
					//$title 		 	 = $cont['geo'][0]['title'];
					$description   		 = $cont['description'];
					$code 				 = $cont['promo_code'];
					$offer_page 		 = $cont['url'];
					$start_date  		 = date('Y-m-d',strtotime($cont['start_date']));	
					$expiry_date 		 = date('Y-m-d',strtotime($cont['active_to']));
					$tracking 			 = $cont['offer_id'];
					if($code !='')
					{
						$type = 'Coupon';	
					}
					else
					{
						$type = 'Promotion';
					}
 					//Coupon table details end//
					
					//categories table details Start//	
					$offer_category_name = $cont['action_category_name'];
					//Categories table details End//	 
					
					//Add new category name in catgories table//
					if($offer_category_name!="")
					{	
	                    		
						$this->db->where('category_name',$offer_category_name);
						$cat = $this->db->get('categories');
						if($cat->num_rows()==0)
						{
							
							$seo_url  = $this->admin_model->seoUrl($offer_category_name);
							$data     = array(
								'category_name'   => $offer_category_name,
								'category_url'    => $seo_url,
								'category_status' => 0,						
							);
							$this->db->insert('categories',$data);					
							$new_category_id    = $this->db->insert_id();
							$new_subcategory_id = '';
						}
						else
						{
							$rst 			 	= $cat->row();
							$new_category_id 	= $rst->category_id;
							$new_subcategory_id = '';
						}					
					}
					 
					//Add new store name in affiliates table//
 					$aff_image  =  $cont['image'];
 					//$logo_url   =  $cont['url_frame'];
					
					$this->db->where('affiliate_name',$offer_name);
					$aff = $this->db->get('affiliates');
					if($aff->num_rows()==0)
					{	
						$offer_url  = $this->admin_model->seoUrl($offer_name);
						$data = array(
							'affiliate_name'   => $offer_name,
							'affiliate_url'    => $offer_url,
							'affiliate_logo'   => $aff_image,
							//'logo_url' 		   => $logo_url,
							'affiliate_status' => '1',
						);
						$this->db->insert('affiliates',$data);
						$new_store_id = $this->db->insert_id();
					}
					else
					{
						$result 	  = $aff->row();
						$new_store_id = $result->affiliate_id;
					}
					
					//Add coupons details in Coupon table// 
					if($offer_name)
					{
						$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`, `code`, `offer_page`, `start_date`,`expiry_date`,`tracking`)
						VALUES ('$offer_name','$title', '$description','$type', '$code', '$offer_page', '$start_date','$expiry_date','$tracking')");
					}
					else
					{
						$duplicate+=1;
						$duplicate_promo_id .= $promo_id.', ';
					}
					//End coupons table details//
					unset($cont);
				}
			}
			
			if($affiliate_name == 'afilio')
			{
				//echo "<pre>"; print_r($content); exit;
				foreach($content as $cont)
				{

					$duplicate = 1;
					$new_category_id='';

					//Coupon table details start// 
					$offer_name      = $cont['title'];
					$title 		     = $cont['title'];
					$description 	 = $cont['description'];
					$code        	 = $cont['code'];
					$type 		 	 = $cont['rule'];
					$offer_page 	 = $cont['url'];
					$start_date  	 = $cont['startdate'];	
					$expiry_date 	 = $cont['enddate'];
					//coupon table details End//

					//Add new category name//
					$category_id   = $cont['progid'];
					$selqry        = $this->db->query("SELECT category_name from afilio_category_name_details where category_id='$category_id'")->row(); 
					$category_name = $selqry->category_name;
					 
					if($category_name!="")
					{	
	                    		
						$this->db->where('category_name',$category_name);
						$cat = $this->db->get('categories');
						if($cat->num_rows()==0)
						{
							
							$seo_url  = $this->admin_model->seoUrl($category_name);
							$data     = array(
								'category_name'   => $category_name,
								'category_url'    => $seo_url,
								'category_status' => 0,						
							);
							$this->db->insert('categories',$data);					
							$new_category_id    = $this->db->insert_id();
							$new_subcategory_id = '';
						}
						else
						{
							$rst 			 	= $cat->row();
							$new_category_id 	= $rst->category_id;
							$new_subcategory_id = '';
						}					
					}
					 
					//Add new store name//
					if($offer_name != "")
					{
						$this->db->where('affiliate_name',$offer_name);
						$aff = $this->db->get('affiliates');
						if($aff->num_rows()==0)
						{	
							$offer_url  = $this->admin_model->seoUrl($offer_name);
							$data = array(
								'affiliate_name'   => $offer_name,
								'affiliate_url'    => $offer_url,
								'affiliate_status' => '1',
							);
							$this->db->insert('affiliates',$data);
							$new_store_id = $this->db->insert_id();
						}
						else
						{
							$result 	  = $aff->row();
							$new_store_id = $result->affiliate_id;
						}
					}

					 
					if($offer_name)
					{
						$this->db->query("INSERT INTO `coupons` (`offer_name`,`title`,`description`,`type`, `code`, `offer_page`, `start_date`,`expiry_date`,`featured`,`exclusive`)
						VALUES ('$offer_name','$title', '$description','$type', '$code', '$offer_page', '$start_date','$expiry_date','$featured','$exclusive')");
						
					}
					else
					{
						$duplicate+=1;
						$duplicate_promo_id .= $promo_id.', ';
					}
					unset($cont);
				}
			}
		}	
		return $array;		
	}*/
	/*End*/


	/*New code for report upload Via API 15-6-16.*/
	function import_apireports($content,$affiliate_name)
	{
		//echo "<pre>";print_r($content); exit;
		$report_type = '';
		if(count($content)!=0)
		{
			$duplicate = 0;
			$duplicate_promo_id = '';
			
			if($affiliate_name == 'zanox')
			{
				//echo "<pre>";print_r($content); exit;
				if($content['saleItems']!='')
				{

					if(is_array($content['saleItems']['saleItem'][0]))
					{
						//echo "hai";
						//echo "hai1".$content['saleItems']['saleItem']; exit;
						foreach($content['saleItems']['saleItem'] as $cont)
						{
							  
							$all=0;
							//$transaction_id = $cont['clickId'];
							$transaction_id = $cont['@attributes']['id'];
							$this->db->where('transaction_id',$transaction_id);
							$all = $this->db->get('tbl_report')->num_rows();				 	  	 
							 
							if($all == 0)
							{
								 
								$duplicate = 1;
								$user_id            = $cont['gpps']['gpp'][0];
								$val_user_id		= substr($user_id, 0, 5);
								
								if($val_user_id == 'P0001')
								{
									$get_userid     = decode_userid($user_id);	
								}
								else
								{
									$get_userid     = $user_id;
								} 
								
								$currency_type      = $cont['currency'];
								if($currency_type != 'BRL')
								{
									$amount   			= $cont['amount'];
									$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
								}	
								else
								{
									$transaction_amount = $cont['amount'];	
								}
									
								$coupon_name    = $cont['program'];
								//$offname         = preg_split("/[ ]/", $coupon_names);
				    			//$coupon_name     = trim($offname, " BR");


								$commission_amount  = $cont['commission'];
								$transaction_date   = $cont['trackingDate'];
								$status 			= $cont['reviewState'];
								
								$report_update_id   = $cont['@attributes']['id'];
								 
								
								/*$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
								VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Pending')");*/
								 
								if($transaction_amount!='')
								{
									 
									$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`cashback_amount`,`ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`) 
									VALUES ('$coupon_name', '$transaction_date', '$commission_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s H:i:s')."','$commission_amount','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','".date('Y-m-d H:i:s')."','$report_update_id')");
								 	 
									$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`cashback_amount`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`)
									VALUES ('$get_userid','$coupon_name',$commission_amount,'$coupon_name','Pending','".date('Y-m-d H:i:s')."', '$transaction_amount', '".date('Y-m-d H:i:s')."','$transaction_id','$commission_amount','$report_update_id')");	
								}
							}
							else
							{
								$duplicate = 0;
							}		
						}	
					} 
					else
					{
						//echo "hai".$content['saleItems']['saleItem']['clickId']; exit;
						$all=0;
						//$transaction_id = $content['saleItems']['saleItem']['clickId'];
						$transaction_id   = $content['saleItems']['saleItem']['@attributes']['id'];

						$this->db->where('transaction_id',$transaction_id);
						$all = $this->db->get('tbl_report')->num_rows();				 	  	 
						if($all == 0)
						{

							$duplicate = 1;
							$user_id            = $content['saleItems']['saleItem']['gpps']['gpp'][0];
							$val_user_id		= substr($user_id, 0, 5);
								
							if($val_user_id == 'P0001')
							{
								$get_userid     = decode_userid($user_id);	
							}
							else
							{
								$get_userid     = $user_id;
							} 

							$currency_type      = $content['saleItems']['saleItem']['currency'];

							if($currency_type != 'BRL')
							{
								$amount   			= $content['saleItems']['saleItem']['amount'];
								$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
							}	
							else
							{
								$transaction_amount = $content['saleItems']['saleItem']['amount'];	
							}

							//$coupon_name      = $content['saleItems']['saleItem']['program'];
							$coupon_name     	= $content['saleItems']['saleItem']['program'];
							//$offname          = preg_split("/[ ]/", $coupon_names);
				    		//$coupon_name     	= trim($offname, " BR");


							$commission_amount  = $content['saleItems']['saleItem']['commission'];
							$transaction_date   = $content['saleItems']['saleItem']['trackingDate'];
							$transaction_amount = $content['saleItems']['saleItem']['amount'];
							$report_update_id   = $content['saleItems']['saleItem']['@attributes']['id'];
							
							/*$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
							VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Pending')");*/
							
							if($transaction_amount!='')
							{
								 
								$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`cashback_amount`,`ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`) 
								VALUES ('$coupon_name', '$transaction_date', '$commission_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s H:i:s')."','$cashback_amount','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','".date('Y-m-d H:i:s')."','$report_update_id')");
							}
							if($coupon_name !='')
							{
								$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`cashback_amount`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`)
								VALUES ('$get_userid','$coupon_name',$commission_amount,'$coupon_name','Pending','".date('Y-m-d H:i:s')."', '$transaction_amount', '".date('Y-m-d H:i:s')."','$transaction_id','$commission_amount','$report_update_id')");	
							}
						}
					}
				}
				else
				{
					return 2;
				}		  
			}
			
			if($affiliate_name == 'lomadee')
			{
				
				//echo '<pre>'; print_r($content['item']); exit;
				if($content['item']!='')
				{
					if(is_array($content['item'][0]))
					{
						//echo "hai";
						foreach($content['item'] as $cont)
						{
							$all=0;
							$transaction_id   = $cont['transactionId'];
							$this->db->where('transaction_id',$transaction_id);
							$all = $this->db->get('tbl_report')->num_rows();
					 
							if($all == 0)
							{

								$duplicate = 1;
								$user_id            = $cont['associateId'];
							 	$get_userid     	= decode_userid($user_id);	
							 	 
							 	if($get_userid =='-230000')
							 	{
							 		$get_userid     = 0;
							 	}

							 	$currency_type      = $cont['currency'];
								if($currency_type != 'BRL')
								{
									$amount   			= $cont['gmv'];
									$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
								}	
								else
								{
									$transaction_amount = $cont['gmv'];
								}

								$coupon_id          = $cont['advertiser']['advertiserName'];
								$cashback_amount    = $cont['commission'];
								$old_trans_date     = $cont['checkoutDate'];
								$transaction_date   = preg_replace('/\//', '-',$old_trans_date);
								 
								
								 
								if($transaction_id !='')
								{
									$report_update_id   = $transaction_id;
								}else
								{
									$report_update_id   = rand(1000,9999);	
								}

								if($transaction_id != '')
								{
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d')."','".date('Y-m-d')."','Cashback','Credited','$report_update_id','Pending')");
								 
								}
								if($transaction_amount!='')
								{
									$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`cashback_amount`,`ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`) 
									VALUES ('$coupon_id', '$transaction_date', '$cashback_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s')."','$cashback_amount','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','".date('Y-m-d H:i:s')."','$report_update_id')");
								}
								
								
								if($coupon_id !='')
								{
									$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`)
									VALUES ('$get_userid','$coupon_id','$coupon_id','Pending','".date('Y-m-d')."', '$transaction_amount', '".date('Y-m-d')."','$transaction_id','$cashback_amount','$report_update_id')");
								}
							}	 
						}
					}	
					else
					{
						//echo "hello";
						$all = 0;
						$transaction_id   = $content['item']['transactionId'];
						$this->db->where('transaction_id',$transaction_id);
						$all = $this->db->get('tbl_report')->num_rows();
					 					 
						if($all == 0)
						{
							 
							$duplicate = 1;
							$user_id            = $content['item']['associateId'];
							$get_userid     	= decode_userid($user_id);	
							 
							if($get_userid =='-230000')
							{
								$get_userid     = 0;
							}
							$currency_type      = $content['item']['currency'];
							
							if($currency_type != 'BRL')
							{
								$amount   			= $content['item']['gmv'];
								$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
							}	
							else
							{
								$transaction_amount = $content['item']['gmv'];
							}

							$coupon_id          = $content['item']['advertiser']['advertiserName'];
							$cashback_amount    = $content['item']['commission'];
							$old_trans_date     = $content['item']['checkoutDate'];
							$transaction_date   = preg_replace('/\//', '-',$old_trans_date);
							//$transaction_amount = $content['item']['gmv'];
							//echo $transaction_amount; exit;
							$ref_cashback_percent= 0;
							$ref_cashback_amount = 0;
							$total_Cashback_paid = 0;
							if($transaction_id !='')
							{
								$report_update_id   = $transaction_id;
							}
							else
							{
								$report_update_id   = rand(1000,9999);	
							}

							if($transaction_id != '')
							{
								$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
								VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d')."','".date('Y-m-d')."','Cashback','Credited','$report_update_id','Pending')");
							}
							if($transaction_amount!='')
							{
								$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`cashback_amount`,`ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`) 
								VALUES ('$coupon_id', '$transaction_date', '$cashback_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s')."','$cashback_amount','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','".date('Y-m-d H:i:s')."','$report_update_id')");
							}
							if($coupon_id !='')
							{
								$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`)
								VALUES ('$get_userid','$coupon_id','$coupon_id','Pending','".date('Y-m-d')."', '$transaction_amount', '".date('Y-m-d')."','$transaction_id','$cashback_amount','$report_update_id')");
							}
						}
					}
				}
				else
				{
					return 2;
				}		  
			}
			
			//Pon prakesh start//
			if($affiliate_name == 'rakuten')
			{
				
				if($content!='')
				{
					//echo "hai"; exit;
					foreach($content as $cont)
					{
						
						$all=0;
						$transaction_id    = $cont['Transaction ID'];
						$this->db->where('transaction_id',$transaction_id);
						$all = $this->db->get('tbl_report')->num_rows();
			 
						if($all == 0)
						{
							$duplicate = 1;
							
							//transation_details table fields details//

							$transaction_id		 = $cont['Transaction ID'];
							//$transaction_amount	 = $cont['Sales'];
							$user_id 			 = $cont['Member ID (U1)'];
							$get_userid          = decode_userid($user_id);

							if($transaction_id !='')
							{
								$report_update_id   = $transaction_id;
							}else
							{
								$report_update_id   = rand(1000,9999);	
							}

							$currency_type      = $cont['Currency'];
							if($currency_type != 'BRL')
							{
								$amount   			= $cont['Sales'];
								$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
							}	
							else
							{
								$transaction_amount = $cont['Sales'];
							}

							//tbl_report table fields details//
							$coupon_name 		 = $cont['Product Name'];
							$old_trans_date 	 = $cont['Transaction Date'];
							$transaction_date    = preg_replace('/\//', '-',$old_trans_date);
							$pay_out_amount   	 = $cont['Total Commission'];
							$ref_cashback_percent= 0;
							$ref_cashback_amount = 0;
							$total_Cashback_paid = 0;

							//Cashback table fields details// 
							 $cashback_amount    = $cont['Total Commission'];
							
							$now 				 = date('Y-m-d H:i:s');
							$current_date  		 = date('Y-m-d',strtotime($now));
							
							
							$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
							VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d')."','".date('Y-m-d')."','Cashback','Credited','$report_update_id','Pending')");

							if($transaction_amount != '')
							{
								$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`cashback_amount`,`ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`) 
								VALUES ('$coupon_name', '$transaction_date', '$pay_out_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s')."','$pay_out_amount','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','".date('Y-m-d H:i:s')."','$report_update_id')");
							}
							if($coupon_name !='')
							{	
								$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`)
								VALUES ('$get_userid','$coupon_name','$coupon_name','Pending','".date('Y-m-d')."', '$transaction_amount', '".date('Y-m-d')."','$transaction_id','$cashback_amount','$report_update_id')");
							}	
						}
						else
						{
							$duplicate = 0;
						}	
					}
				}
				else
				{
					return 2;
				}	
			}

			//new URL changes 20-6-16//
			if($affiliate_name == 'cityads')
			{
				//echo "<pre>";print_r($content); exit;
				if($content['data']['items']!='')
				{
					foreach($content['data']['items'] as $cont)
					{
							
							$all = 0;
							$transaction_id    = $cont['orderID'];
							$this->db->where('transaction_id',$transaction_id);
							$all = $this->db->get('tbl_report')->num_rows(); 
				 
							if($all == 0)
							{
								
								$duplicate 			 = 1;
								$offer_name			 = $cont['offerName'];
								$transaction_amount  = $cont['orderTotal'];
								$commission          = $cont['commissionOpen'];
								$user_id             = $cont['subaccount'];
								$get_userid          = decode_userid($user_id);
								$report_update_id 	 = $cont['submissionID'];

								$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
								VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Pending')");
								 
								if($transaction_amount!='')
								{	 
									$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`status`,`report_update_id`) 
									VALUES ('$offer_name', '".date('Y-m-d H:i:s')."', '$transaction_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','$report_update_id')");
								}
								
								if($offer_name !='')
								{
									$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`)
									VALUES ('$get_userid','$offer_name','$offer_name','Pending','".date('Y-m-d H:i:s')."', '$transaction_amount', '".date('Y-m-d H:i:s')."','$transaction_id','$commission','$report_update_id')");	 
								}
							}
		 			}
	 			}
	 			else
	 			{
	 				return 2;
	 			}	 
			} 
			//Pon prakesh End//

			if($affiliate_name == 'afilio')
			{	
				//echo "<pre>";print_r($content['list']); exit;
				if($content['list']!='')
				{
					foreach($content['list'] as $cont)
					{
						 
						$all=0;
						$transaction_id    = $cont['saleid']['value'];
						$this->db->where('transaction_id',$transaction_id);
						$all = $this->db->get('tbl_report')->num_rows();
						 
						if($all == 0)
						{

							$duplicate = 1;
							 
							$category_id 		 = $cont['progid']['value'];	
							$selqry        		 = $this->db->query("SELECT category_name from afilio_category_name_details where category_id='$category_id'")->row(); 
							$coupon_id   		 = $selqry->category_name;
							$user_id             = $cont['xtra']['value'];	
							$get_userid          = decode_userid($user_id);
							$commission_amount   = $cont['comission']['value'];
							$transaction_date    = $cont['date']['value'];
							//$transaction_date  = preg_replace('/\//', '-',$old_trans_date);
							$transaction_amount  = $cont['order_price']['value'];
							$order_id   		 = $cont['order_id']['value'];

							if($transaction_id !='')
							{
								$report_update_id   = $transaction_id;
							}else
							{
								$report_update_id   = rand(1000,9999);	
							}


							if($transaction_amount!='')
							{
								$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`cashback_amount`,`ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`) 
								VALUES ('$coupon_id', '$transaction_date', '$cashback_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s')."','$cashback_amount','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','".date('Y-m-d H:i:s')."','$report_update_id')");
							}
							if($coupon_id !='')
							{
								$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`)
								VALUES ('$get_userid','$coupon_id','$coupon_id','Pending','".date('Y-m-d H:i:s')."', '$transaction_amount', '".date('Y-m-d H:i:s')."','$transaction_id','$cashback_amount','$report_update_id')");
							}	
						}	 
					}
				}
				else
				{
					return 2;
				}
			}
		}	
		return $duplicate;
	}
	/*End*/

	/*New code for report upload Via API 15-6-16.*/
	function update_apireports($content,$affiliate_name)
	{
		//echo "<pre>";print_r($content); exit;
		$report_type = '';
		if(count($content)!=0)
		{
			$duplicate = 0;
			$duplicate_promo_id = '';
			
			if($affiliate_name == 'zanox')
			{
				//echo "<pre>";print_r($content); exit;
				if($content['saleItems']!='')
				{	
					//echo "hai"; exit;
					if(is_array($content['saleItems']['saleItem'][0]))
					{
						//echo "hai";
						//echo "hai1".$content['saleItems']['saleItem']; exit;
						foreach($content['saleItems']['saleItem'] as $cont)
						{
							  
							$all=0;
							//$transaction_id   = $cont['clickId'];
							$transaction_id   = $cont['@attributes']['id'];
							$report_update_id = $cont['@attributes']['id'];

							//$this->db->where('transation_id',$transaction_id);
							$this->db->where('report_update_id',$report_update_id);
							$all = $this->db->get('cashback')->num_rows();				 	  	 
						//echo $all;   
						//}exit;
							if($all != 0)
							{
								 
								$duplicate = 1;
								$user_id            = $cont['gpps']['gpp'][0]; 
								$val_user_id		= substr($user_id, 0, 5); 
								
								if($val_user_id == 'P0001')
								{
									$get_userid     = decode_userid($user_id);	
								}
								else
								{
									$get_userid     = $user_id;
								} 
								  
							  
								$currency_type      = $cont['currency'];
								if($currency_type != 'BRL')
								{
									$amount   			= $cont['amount'];
									$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
								}	
								else
								{
									$transaction_amount = $cont['amount'];	
								}

								$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$get_userid))->row('balance');
								if($prev_userbal == '')
								{
									$prev_userbal = 0;	
								}

								$status = $cont['reviewState'];

								if($status == 'open')
								{
									$tstatus = 'Pending';
									$cstatus = 'Pending';
								}
								if($status == 'approved')
								{
									$tstatus = 'Approved';
									$cstatus = 'Completed';
								}
								if($status == 'rejected')
								{
									$tstatus = 'Canceled';
									$cstatus = 'Canceled';
								}
								if($status == 'confirmed')
								{
									$tstatus = 'Credited';
									$cstatus = 'Completed';
								}

								
								if($status !='')
								{
									if($tstatus == 'Approved' || $tstatus = 'Credited')
									{
										//$data = array('transation_status' => $tstatus); 
										//$this->db->where('report_update_id',$report_update_id);
										//$updation = $this->db->update('transation_details',$data);
										$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
										VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");
									}
									if($tstatus == 'Canceled')
									{
										//$data = array('transation_status' => $tstatus); 
										//$this->db->where('report_update_id',$report_update_id);
										//$updation = $this->db->update('transation_details',$data);
										$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
										VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Canceled')");
									}

									/*New code for mail notification details 1-3-17*/
									$userdetails 	 = $this->db->query("SELECT * from tbl_users where user_id=$get_userid")->row();
									$cashback_status = $userdetails->cashback_mail;
									$user_email      = $userdetails->email;
									$first_name      = $userdetails->first_name;
									$last_name       = $userdetails->last_name;
									
									if($first_name == '' && $last_name == '')
									{
										$ex_name   = explode('@', $user_email);
										$user_name = $ex_name[0]; 
									}
									else
									{
										$user_name = $first_name.''.$last_name;
									}

									$this->db->where('admin_id',1);
									$admin_det = $this->db->get('admin');
									if($admin_det->num_rows >0) 
									{    
										$admin 		 = $admin_det->row();
										$admin_email = $admin->admin_email;
										$site_name 	 = $admin->site_name;
										$admin_no	 = $admin->contact_number;
										$site_logo 	 = $admin->site_logo;
									}
									$date = date('Y-m-d');
									/*end 1-3-17*/

									$data = array('status' => $cstatus); 
									$this->db->where('report_update_id',$report_update_id);
									$updation = $this->db->update('cashback',$data);

									if($cstatus == 'Completed')
									{
										$this->db->where('user_id',$get_userid);
										$this->db->update('tbl_users',array('balance'=>$prev_userbal+$transaction_amount));
										
										/*mail for Completed cashback 1-3-17*/
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',8);
											$mail_template = $this->db->get('tbl_mailtemplates');
											if($mail_template->num_rows >0) 
											{
											   	$fetch     = $mail_template->row();
											   	$subject   = $fetch->email_subject;
											   	$templete  = $fetch->email_template;
											   	$url 	  = base_url().'my_earnings/';
											   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
											   	$myaccount = base_url().'minha_conta';
											   
											  	$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);

												$subject_new = strtr($subject,$sub_data);
												
												//$this->email->initialize($config);
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email, $site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', $transaction_amount),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

											    );
											   
											   $content_pop=strtr($templete,$data);
											   $this->email->message($content_pop);
											   $this->email->send();  
											}
										} 
										/*end 1-3-17*/
									}
									if($cstatus == 'Canceled')
									{
										/*mail for Canceled cashback 1-3-17*/
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',11);
											$mail_template = $this->db->get('tbl_mailtemplates');
											if($mail_template->num_rows >0) 
											{

											   	$fetch 	   = $mail_template->row();
											   	$subject   = $fetch->email_subject;
											   	$templete  = $fetch->email_template;
											   	$url 	   = base_url().'cashback/my_earnings/';
											   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
											   	$myaccount = base_url().'minha_conta';
											   
												$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);
												$subject_new = strtr($subject,$sub_data);
												
												//$this->email->initialize($config);
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email,$site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', $transaction_amount),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
											    );
											   
											   $content_pop=strtr($templete,$data);
											   //echo print_r($content_pop); exit;
											   // echo $content_pop; echo $subject_new;
											   $this->email->message($content_pop);
											   $this->email->send();  
											}
										} 
										/*end 1-3-17*/ 
									}
									if($cstatus == 'Pending')
									{
										/*mail for Pending cashback 1-3-17*/
										if($cashback_status == 1)
										{
											$this->db->where('mail_id',10);
											$mail_template = $this->db->get('tbl_mailtemplates');
											
											if($mail_template->num_rows >0) 
											{
											    $fetch     = $mail_template->row();
											    $subject   = $fetch->email_subject;
											    $templete  = $fetch->email_template;
											    //$url     = base_url().'cashback/my_earnings/';
											    $unsuburl  = base_url().'cashback/un-subscribe/cashback/'.$get_userid;
											    $myaccount = base_url().'cashback/minha_conta';
											   
												$this->load->library('email');
												
												$config = Array(
													'mailtype'  => 'html',
													'charset'   => 'utf-8',
												);
												
												$sub_data = array(
													'###SITENAME###'=>$site_name
												);
												$subject_new = strtr($subject,$sub_data);
												
												$this->email->set_newline("\r\n");
												$this->email->initialize($config);
												$this->email->from($admin_email,$site_name.'!');
												$this->email->to($user_email);
												$this->email->subject($subject_new);
											   
												$data = array(
													'###NAME###'=>$user_name,
													'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
													'###SITENAME###'=>$site_name,
													'###ADMINNO###'=>$admin_no,
													'###DATE###'=>$date,
													'###AMOUNT###'=>str_replace('.', ',', $cashback_amount),
													'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
													'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
											    );
											   
											    $content_pop=strtr($templete,$data);
											   	$this->email->message($content_pop);
											   	$this->email->send();  
											}
										} 
										/*end 1-3-17*/
									}
								}
							}	
						}	
					} 
					else
					{
						//echo "hai".$content['saleItems']['saleItem']['clickId']; exit;
						$all=0;
						$transaction_id   = $content['saleItems']['saleItem']['@attributes']['id'];
						$report_update_id = $content['saleItems']['saleItem']['@attributes']['id'];
						//$this->db->where('transaction_id',$transaction_id);
						//$all = $this->db->get('tbl_report')->num_rows();	
						$this->db->where('report_update_id',$report_update_id);
						$all = $this->db->get('transation_details')->num_rows();			 	  	 
						if($all != 0)
						{

							$duplicate = 1;
							 
							//$report_update_id   = $content['saleItems']['saleItem']['@attributes']['id'];
							$status 			= $content['saleItems']['saleItem']['reviewState'];
							
							$user_id            = $content['saleItems']['saleItem']['gpps']['gpp'][0]; 
							$val_user_id		= substr($user_id, 0, 5); 
								
								if($val_user_id == 'P0001')
								{
									$get_userid     = decode_userid($user_id);	
								}
								else
								{
									$get_userid     = $user_id;
								} 
								  
							  
								$currency_type      = $content['saleItems']['saleItem']['currency'];
								if($currency_type != 'BRL')
								{
									$amount   			= $content['saleItems']['saleItem']['amount'];
									$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
								}	
								else
								{
									$transaction_amount = $content['saleItems']['saleItem']['amount'];	
								}

								$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$get_userid))->row('balance');
								if($prev_userbal == '')
								{
									$prev_userbal = 0;	
								}

								$status = $content['saleItems']['saleItem']['reviewState'];

								if($status == 'open')
								{
									$tstatus = 'Pending';
									$cstatus = 'Pending';
								}
								if($status == 'approved')
								{
									$tstatus = 'Approved';
									$cstatus = 'Completed';
								}
								if($status == 'rejected')
								{
									$tstatus = 'Canceled';
									$cstatus = 'Canceled';
								}
								if($status == 'confirmed')
								{
									$tstatus = 'Credited';
									$cstatus = 'Completed';
								}



							$data = array('status' => $status); 
							$this->db->where('report_update_id',$report_update_id);
							$updation = $this->db->update('transation_details',$data);							
							
							if($status !='')
							{
								
								if($tstatus == 'Approved' || $tstatus = 'Credited')
								{
									//$data = array('transation_status' => $tstatus); 
									//$this->db->where('report_update_id',$report_update_id);
									//$updation = $this->db->update('transation_details',$data);
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Paid')");
								}

								if($tstatus == 'Canceled')
								{
									//$data = array('transation_status' => $tstatus); 
									//$this->db->where('report_update_id',$report_update_id);
									//$updation = $this->db->update('transation_details',$data);
									$this->db->query("INSERT INTO `transation_details` (`transation_id`,`transation_amount`,`user_id`,`transation_date`,`transaction_date`,`transation_reason`,`mode`,`report_update_id`,`transation_status`) 
									VALUES ('$transaction_id','$transaction_amount','$get_userid','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','Cashback','Credited','$report_update_id','Canceled')");
								}

								/*New code for mail notification details 1-3-17*/
								$userdetails 	 = $this->db->query("SELECT * from tbl_users where user_id=$get_userid")->row();
								$cashback_status = $userdetails->cashback_mail;
								$user_email      = $userdetails->email;
								$first_name      = $userdetails->first_name;
								$last_name       = $userdetails->last_name;
								
								if($first_name == '' && $last_name == '')
								{
									$ex_name   = explode('@', $user_email);
									$user_name = $ex_name[0]; 
								}
								else
								{
									$user_name = $first_name.''.$last_name;
								}

								$this->db->where('admin_id',1);
								$admin_det = $this->db->get('admin');
								if($admin_det->num_rows >0) 
								{    
									$admin 		 = $admin_det->row();
									$admin_email = $admin->admin_email;
									$site_name 	 = $admin->site_name;
									$admin_no	 = $admin->contact_number;
									$site_logo 	 = $admin->site_logo;
								}
								$date = date('Y-m-d');
								/*end 1-3-17*/

								$data = array('status' => $cstatus); 
								$this->db->where('report_update_id',$report_update_id);
								$updation = $this->db->update('cashback',$data);

								if($cstatus == 'Completed')
								{
									$this->db->where('user_id',$get_userid);
									$this->db->update('tbl_users',array('balance'=>$prev_userbal+$transaction_amount));
									
									/*mail for Completed cashback 1-3-17*/
									if($cashback_status == 1)
									{
										$this->db->where('mail_id',8);
										$mail_template = $this->db->get('tbl_mailtemplates');
										if($mail_template->num_rows >0) 
										{
										   	$fetch     = $mail_template->row();
										   	$subject   = $fetch->email_subject;
										   	$templete  = $fetch->email_template;
										   	$url 	  = base_url().'my_earnings/';
										   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
										   	$myaccount = base_url().'minha_conta';
										   
										  	$this->load->library('email');
											
											$config = Array(
												'mailtype'  => 'html',
												'charset'   => 'utf-8',
											);
											
											$sub_data = array(
												'###SITENAME###'=>$site_name
											);

											$subject_new = strtr($subject,$sub_data);
											
											//$this->email->initialize($config);
											$this->email->set_newline("\r\n");
											$this->email->initialize($config);
											$this->email->from($admin_email, $site_name.'!');
											$this->email->to($user_email);
											$this->email->subject($subject_new);
										   
											$data = array(
												'###NAME###'=>$user_name,
												'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
												'###SITENAME###'=>$site_name,
												'###ADMINNO###'=>$admin_no,
												'###DATE###'=>$date,
												'###AMOUNT###'=>str_replace('.', ',', $transaction_amount),
												'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
												'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'

										    );
										   
										   $content_pop=strtr($templete,$data);
										   $this->email->message($content_pop);
										   $this->email->send();  
										}
									} 
									/*end 1-3-17*/
								}
								if($cstatus == 'Canceled')
								{
									/*mail for Canceled cashback 1-3-17*/
									if($cashback_status == 1)
									{
										$this->db->where('mail_id',11);
										$mail_template = $this->db->get('tbl_mailtemplates');
										if($mail_template->num_rows >0) 
										{

										   	$fetch 	   = $mail_template->row();
										   	$subject   = $fetch->email_subject;
										   	$templete  = $fetch->email_template;
										   	$url 	   = base_url().'cashback/my_earnings/';
										   	$unsuburl  = base_url().'un-subscribe/cashback/'.$get_userid;
										   	$myaccount = base_url().'minha_conta';
										   
											$this->load->library('email');
											
											$config = Array(
												'mailtype'  => 'html',
												'charset'   => 'utf-8',
											);
											
											$sub_data = array(
												'###SITENAME###'=>$site_name
											);
											$subject_new = strtr($subject,$sub_data);
											
											//$this->email->initialize($config);
											$this->email->set_newline("\r\n");
											$this->email->initialize($config);
											$this->email->from($admin_email,$site_name.'!');
											$this->email->to($user_email);
											$this->email->subject($subject_new);
										   
											$data = array(
												'###NAME###'=>$user_name,
												'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
												'###SITENAME###'=>$site_name,
												'###ADMINNO###'=>$admin_no,
												'###DATE###'=>$date,
												'###AMOUNT###'=>str_replace('.', ',', $transaction_amount),
												'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
												'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
										    );
										   
										   $content_pop=strtr($templete,$data);
										   //echo print_r($content_pop); exit;
										   // echo $content_pop; echo $subject_new;
										   $this->email->message($content_pop);
										   $this->email->send();  
										}
									} 
									/*end 1-3-17*/ 
								}
								if($cstatus == 'Pending')
								{
									/*mail for Pending cashback 1-3-17*/
									if($cashback_status == 1)
									{
										$this->db->where('mail_id',10);
										$mail_template = $this->db->get('tbl_mailtemplates');
										
										if($mail_template->num_rows >0) 
										{
										    $fetch     = $mail_template->row();
										    $subject   = $fetch->email_subject;
										    $templete  = $fetch->email_template;
										    //$url     = base_url().'cashback/my_earnings/';
										    $unsuburl  = base_url().'cashback/un-subscribe/cashback/'.$get_userid;
										    $myaccount = base_url().'cashback/minha_conta';
										   
											$this->load->library('email');
											
											$config = Array(
												'mailtype'  => 'html',
												'charset'   => 'utf-8',
											);
											
											$sub_data = array(
												'###SITENAME###'=>$site_name
											);
											$subject_new = strtr($subject,$sub_data);
											
											$this->email->set_newline("\r\n");
											$this->email->initialize($config);
											$this->email->from($admin_email,$site_name.'!');
											$this->email->to($user_email);
											$this->email->subject($subject_new);
										   
											$data = array(
												'###NAME###'=>$user_name,
												'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
												'###SITENAME###'=>$site_name,
												'###ADMINNO###'=>$admin_no,
												'###DATE###'=>$date,
												'###AMOUNT###'=>str_replace('.', ',', $cashback_amount),
												'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
												'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
										    );
										   
										    $content_pop=strtr($templete,$data);
										   	$this->email->message($content_pop);
										   	$this->email->send();  
										}
									} 
									/*end 1-3-17*/
								}	
							}
						}
					}
				}		  
			}
			if($affiliate_name == 'lomadee')
			{
				//echo "<pre>";print_r($content); exit;
				if($content['saleItems']!='')
				{

					if(is_array($content['saleItems']['saleItem'][0]))
					{
						//echo "hai";
						//echo "hai1".$content['saleItems']['saleItem']; exit;
						foreach($content['saleItems']['saleItem'] as $cont)
						{
							  
							$all=0;
							//$transaction_id   = $cont['clickId'];
							$transaction_id   = $cont['@attributes']['id'];
							$report_update_id = $cont['@attributes']['id'];

							$this->db->where('transation_id',$transaction_id);
							$this->db->where('report_update_id',$report_update_id);
							$all = $this->db->get('transation_details')->num_rows();				 	  	 
							//echo $all;   
							//}exit;
							if($all != 0)
							{
								 
								$duplicate = 1;
								$user_id            = $cont['gpps']['gpp'][0]; 
								$val_user_id		= substr($user_id, 0, 5); 
								
								if($val_user_id == 'P0001')
								{
									$get_userid     = decode_userid($user_id);	
								}
								else
								{
									$get_userid     = $user_id;
								} 
								  
							  
								$currency_type      = $cont['currency'];
								if($currency_type != 'BRL')
								{
									$amount   			= $cont['amount'];
									$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
								}	
								else
								{
									$transaction_amount = $cont['amount'];	
								}

								$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$get_userid))->row('balance');
								if($prev_userbal == '')
								{
									$prev_userbal = 0;	
								}



								$status 			= $cont['reviewState'];
								$report_update_id   = $cont['@attributes']['id'];

								if($status == 'open')
								{
									$tstatus = 'Pending';
									$cstatus = 'Pending';
								}
								if($status == 'approved')
								{
									$tstatus = 'Approved';
									$cstatus = 'Completed';
								}
								if($status == 'rejected')
								{
									$tstatus = 'Canceled';
									$cstatus = 'Canceled';
								}
								if($status == 'confirmed')
								{
									$tstatus = 'Credited';
									$cstatus = 'Completed';
								}

								$data = array('transation_status' => $tstatus); 
								$this->db->where('report_update_id',$report_update_id);
								$updation = $this->db->update('transation_details',$data);
								 
								
								if($status !='')
								{
									if($cstatus == 'Completed')
									{
										$this->db->where('user_id',$get_userid);
										$this->db->update('tbl_users',array('balance'=>$prev_userbal+$transaction_amount));
									}

									$data = array('status' => $cstatus); 
									$this->db->where('report_update_id',$report_update_id);
									$updation = $this->db->update('cashback',$data);	
								}
							}
							else
							{
								$duplicate = 0;
							}		
						}	
					} 
					else
					{
						//echo "hai".$content['saleItems']['saleItem']['clickId']; exit;
						$all=0;
						$transaction_id   = $content['saleItems']['saleItem']['@attributes']['id'];
						$report_update_id = $content['saleItems']['saleItem']['@attributes']['id'];
						$this->db->where('transaction_id',$transaction_id);
						$all = $this->db->get('tbl_report')->num_rows();				 	  	 
						if($all != 0)
						{

							$duplicate = 1;
							 
							$report_update_id   = $content['saleItems']['saleItem']['@attributes']['id'];
							$status 			= $content['saleItems']['saleItem']['reviewState'];
								 

							$data = array('status' => $status); 
							$this->db->where('report_update_id',$report_update_id);
							$updation = $this->db->update('transation_details',$data);							
							
							if($status !='')
							{
								$data = array('status' => $status); 
								$this->db->where('report_update_id',$report_update_id);
								$updation = $this->db->update('cashback',$data);	
							}
						}
					}
				}
				else
				{
					return 2;
				}		  
			}
			if($affiliate_name == 'rakuten')
			{
				//echo "<pre>";print_r($content); exit;
				if($content['saleItems']!='')
				{

					if(is_array($content['saleItems']['saleItem'][0]))
					{
						//echo "hai";
						//echo "hai1".$content['saleItems']['saleItem']; exit;
						foreach($content['saleItems']['saleItem'] as $cont)
						{
							  
							$all=0;
							//$transaction_id   = $cont['clickId'];
							$transaction_id   = $cont['@attributes']['id'];
							$report_update_id = $cont['@attributes']['id'];

							$this->db->where('transation_id',$transaction_id);
							$this->db->where('report_update_id',$report_update_id);
							$all = $this->db->get('transation_details')->num_rows();				 	  	 
							//echo $all;   
							//}exit;
							if($all != 0)
							{
								 
								$duplicate = 1;
								$user_id            = $cont['gpps']['gpp'][0]; 
								$val_user_id		= substr($user_id, 0, 5); 
								
								if($val_user_id == 'P0001')
								{
									$get_userid     = decode_userid($user_id);	
								}
								else
								{
									$get_userid     = $user_id;
								} 
								  
							  
								$currency_type      = $cont['currency'];
								if($currency_type != 'BRL')
								{
									$amount   			= $cont['amount'];
									$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
								}	
								else
								{
									$transaction_amount = $cont['amount'];	
								}

								$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$get_userid))->row('balance');
								if($prev_userbal == '')
								{
									$prev_userbal = 0;	
								}



								$status 			= $cont['reviewState'];
								$report_update_id   = $cont['@attributes']['id'];

								if($status == 'open')
								{
									$tstatus = 'Pending';
									$cstatus = 'Pending';
								}
								if($status == 'approved')
								{
									$tstatus = 'Approved';
									$cstatus = 'Completed';
								}
								if($status == 'rejected')
								{
									$tstatus = 'Canceled';
									$cstatus = 'Canceled';
								}
								if($status == 'confirmed')
								{
									$tstatus = 'Credited';
									$cstatus = 'Completed';
								}

								$data = array('transation_status' => $tstatus); 
								$this->db->where('report_update_id',$report_update_id);
								$updation = $this->db->update('transation_details',$data);
								 
								
								if($status !='')
								{
									if($cstatus == 'Completed')
									{
										$this->db->where('user_id',$get_userid);
										$this->db->update('tbl_users',array('balance'=>$prev_userbal+$transaction_amount));
									}

									$data = array('status' => $cstatus); 
									$this->db->where('report_update_id',$report_update_id);
									$updation = $this->db->update('cashback',$data);	
								}
							}
							else
							{
								$duplicate = 0;
							}		
						}	
					} 
					else
					{
						//echo "hai".$content['saleItems']['saleItem']['clickId']; exit;
						$all=0;
						$transaction_id   = $content['saleItems']['saleItem']['@attributes']['id'];
						$report_update_id = $content['saleItems']['saleItem']['@attributes']['id'];
						$this->db->where('transaction_id',$transaction_id);
						$all = $this->db->get('tbl_report')->num_rows();				 	  	 
						if($all != 0)
						{

							$duplicate = 1;
							 
							$report_update_id   = $content['saleItems']['saleItem']['@attributes']['id'];
							$status 			= $content['saleItems']['saleItem']['reviewState'];
								 

							$data = array('status' => $status); 
							$this->db->where('report_update_id',$report_update_id);
							$updation = $this->db->update('transation_details',$data);							
							
							if($status !='')
							{
								$data = array('status' => $status); 
								$this->db->where('report_update_id',$report_update_id);
								$updation = $this->db->update('cashback',$data);	
							}
						}
					}
				}
				else
				{
					return 2;
				}		  
			}
			if($affiliate_name == 'cityads')
			{
				//echo "<pre>";print_r($content); exit;
				if($content['saleItems']!='')
				{

					if(is_array($content['saleItems']['saleItem'][0]))
					{
						//echo "hai";
						//echo "hai1".$content['saleItems']['saleItem']; exit;
						foreach($content['saleItems']['saleItem'] as $cont)
						{
							  
							$all=0;
							//$transaction_id   = $cont['clickId'];
							$transaction_id   = $cont['@attributes']['id'];
							$report_update_id = $cont['@attributes']['id'];

							$this->db->where('transation_id',$transaction_id);
							$this->db->where('report_update_id',$report_update_id);
							$all = $this->db->get('transation_details')->num_rows();				 	  	 
							//echo $all;   
							//}exit;
							if($all != 0)
							{
								 
								$duplicate = 1;
								$user_id            = $cont['gpps']['gpp'][0]; 
								$val_user_id		= substr($user_id, 0, 5); 
								
								if($val_user_id == 'P0001')
								{
									$get_userid     = decode_userid($user_id);	
								}
								else
								{
									$get_userid     = $user_id;
								} 
								  
							  
								$currency_type      = $cont['currency'];
								if($currency_type != 'BRL')
								{
									$amount   			= $cont['amount'];
									$transaction_amount = currencyConverter($currency_type,'BRL',$amount);
								}	
								else
								{
									$transaction_amount = $cont['amount'];	
								}

								$prev_userbal = $this->db->get_where('tbl_users',array('user_id'=>$get_userid))->row('balance');
								if($prev_userbal == '')
								{
									$prev_userbal = 0;	
								}



								$status 			= $cont['reviewState'];
								$report_update_id   = $cont['@attributes']['id'];

								if($status == 'open')
								{
									$tstatus = 'Pending';
									$cstatus = 'Pending';
								}
								if($status == 'approved')
								{
									$tstatus = 'Approved';
									$cstatus = 'Completed';
								}
								if($status == 'rejected')
								{
									$tstatus = 'Canceled';
									$cstatus = 'Canceled';
								}
								if($status == 'confirmed')
								{
									$tstatus = 'Credited';
									$cstatus = 'Completed';
								}

								$data = array('transation_status' => $tstatus); 
								$this->db->where('report_update_id',$report_update_id);
								$updation = $this->db->update('transation_details',$data);
								 
								
								if($status !='')
								{
									if($cstatus == 'Completed')
									{
										$this->db->where('user_id',$get_userid);
										$this->db->update('tbl_users',array('balance'=>$prev_userbal+$transaction_amount));
									}

									$data = array('status' => $cstatus); 
									$this->db->where('report_update_id',$report_update_id);
									$updation = $this->db->update('cashback',$data);	
								}
							}
							else
							{
								$duplicate = 0;
							}		
						}	
					} 
					else
					{
						//echo "hai".$content['saleItems']['saleItem']['clickId']; exit;
						$all=0;
						$transaction_id   = $content['saleItems']['saleItem']['@attributes']['id'];
						$report_update_id = $content['saleItems']['saleItem']['@attributes']['id'];
						$this->db->where('transaction_id',$transaction_id);
						$all = $this->db->get('tbl_report')->num_rows();				 	  	 
						if($all != 0)
						{

							$duplicate = 1;
							 
							$report_update_id   = $content['saleItems']['saleItem']['@attributes']['id'];
							$status 			= $content['saleItems']['saleItem']['reviewState'];
								 

							$data = array('status' => $status); 
							$this->db->where('report_update_id',$report_update_id);
							$updation = $this->db->update('transation_details',$data);							
							
							if($status !='')
							{
								$data = array('status' => $status); 
								$this->db->where('report_update_id',$report_update_id);
								$updation = $this->db->update('cashback',$data);	
							}
						}
					}
				}
				else
				{
					return 2;
				}		  
			}
			if($affiliate_name == 'afilio')
			{
				//echo "<pre>";print_r($content); exit;
				if($content['list']!='')
				{
					foreach($content['list'] as $cont)
					{
						 
						$all=0;
						$transaction_id    = $cont['saleid']['value'];
						$this->db->where('transaction_id',$transaction_id);
						$all = $this->db->get('tbl_report')->num_rows();
						 
						if($all == 0)
						{

							$duplicate = 1;
							 
							$category_id 		 = $cont['progid']['value'];	
							$selqry        		 = $this->db->query("SELECT category_name from afilio_category_name_details where category_id='$category_id'")->row(); 
							$coupon_id   		 = $selqry->category_name;
							$user_id             = $cont['xtra']['value'];	
							$get_userid          = decode_userid($user_id);
							$commission_amount   = $cont['comission']['value'];
							$transaction_date    = $cont['date']['value'];
							//$transaction_date  = preg_replace('/\//', '-',$old_trans_date);
							$transaction_amount  = $cont['order_price']['value'];
							$order_id   		 = $cont['order_id']['value'];

							if($transaction_id !='')
							{
								$report_update_id   = $transaction_id;
							}else
							{
								$report_update_id   = rand(1000,9999);	
							}

														
							if($transaction_amount!='')
							{
								$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`cashback_amount`,`ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`) 
								VALUES ('$coupon_id', '$transaction_date', '$cashback_amount', '$transaction_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s')."','$cashback_amount','$ref_cashback_percent','$ref_cashback_amount','$total_Cashback_paid','".date('Y-m-d H:i:s')."','$report_update_id')");
							}
							if($coupon_id !='')
							{
								$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`)
								VALUES ('$get_userid','$coupon_id','$coupon_id','Pending','".date('Y-m-d H:i:s')."', '$transaction_amount', '".date('Y-m-d H:i:s')."','$transaction_id','$cashback_amount','$report_update_id')");
							}	
						}	 
					}
				}		  
			}
		}	
		return $duplicate;
	}
	/*End*/
	
}
?>
