<?php
class Extension extends CI_Controller
{
	    public function __construct(){	
		parent::__construct();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->load->library('session');
		$this->load->library('form_validation','Utility');
		$this->load->model('extension_model');
			$this->load->model('front_model');
			session_start();
		
	 }
	 
			 function getnewdesign()
			 {
				  $user_id  = $this->session->userdata('user_id'); 

				  $data['userdetails'] = $this->extension_model->userdetails($user_id);
				  
				  if($user_id!='')
				  {
				  echo    $this->load->view('extension/getnewdesign',$data);
			      }
			      else
			      {
					echo  $this->load->view('extension/beforelogin',$data);  
				  }
			 }
			 
			 function getuserloginornot()
			 {
				  $user_id  = $this->session->userdata('user_id'); 
				  
				  echo $user_id;
			 }
			 
			 function getofferdetails()
			 {
				   $currenturl = $this->input->post('currenturl');

				   $data['getmaxcoupons'] = $this->extension_model->getcurrentcoupons($currenturl);
				   
				   $data['currenturl']  = $currenturl;
 				   
				  echo  $this->load->view('extension/getcouponsbasedonurl',$data);  
			 }
			 
			 
			 function mainfoxoffer()
			 {
				   $currenturl = $this->input->post('currenturl');
				   
				   $data['getmaxcoupons'] = $this->extension_model->getcurrentcoupons($currenturl);
				   
				  echo  $this->load->view('extension/mainfoxoffer',$data);  
			 }
			 

			 
			function get_domain($url)
			{
				$pieces = parse_url($url);
				$domain = isset($pieces['host']) ? $pieces['host'] : '';
				if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
				return $regs['domain'];
				}
				return false;
			}
			
			
				function redirecionar($storeid=null,$couponid=null,$name)
				{	
					
				$admindetails = $this->front_model->getadmindetails();
				$data['admindetails'] = $admindetails;
				$store_details = $this->front_model->get_store_details_byid($storeid);
				$data['store_details'] = $store_details;
				$store_name = $store_details->affiliate_name;
				
				 $_SESSION['activate'] = $storeid;
				 
				 		
				if($name!='')
				{
				$coupon = $this->front_model->get_coupons_from_shoppingcoupon_byid($couponid);
				}
				else
				{
				$coupon = $this->front_model->get_coupons_from_coupon_byid($couponid);
				}	
				$user_id = $this->session->userdata('user_id');
				$click_history = $this->front_model->click_history($storeid,$couponid,$user_id);
				$data['user_id'] = $user_id;
				$data['coupon'] = $coupon;
				$this->load->view('extension/visit-shop',$data);
				}	
				
				function adicionar_bonus()
				{
				$this->load->view('extension/givenunicbonus');	
				}

				function statusupdate()
				{
				    $_SESSION["status"] = "close";
				    echo  $_SESSION["status"];
				}
				
				// anand
				function set_status($name,$key)
				{
				    $_SESSION["browser_url"] = $key;
				    echo $_SESSION["browser_url"];
				}
				
				
				function getstasus()
				{
				   	if(isset($_SESSION["status"]))
					{
					   echo $_SESSION["status"];
					}
				}
				
				function statusupdateclose()
				{
					unset($_SESSION["status"]);
				}
				
				function checkactivestatus()
				{
					if(isset($_SESSION['activate']))
					{
						echo $_SESSION['activate'];
					}
				}
				
				function unsetactivestatus()
				{
					 unset($_SESSION["activate"]);
				}
			


}


?>
