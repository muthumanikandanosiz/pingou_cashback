<?php
class Networks extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->load->model('network_model');
		$this->input->session_helper();
	}
	
	/*Real Time Tracking Transactions Start by seetha mam*/
	
	//Zanox tracking transactions function
	function Zanox_trackingtransactions()
	{
		$this->input->session_helper();
		$Affiliate_details = $this->network_model->Affiliate_details('Zanox');
		$aff_name =$Affiliate_details->affiliate_network;
		$connectID =$Affiliate_details->api_key;			
		$secret_key =$Affiliate_details->networkid;			
		$flag=0;
		if($connectID!='')
		{			
			$http_verb     = 'GET';
			$date          = date ("Y-m-d");
			$uri           = '/reports/sales/date/' . $date;
			$time_stamp    = gmdate('D, d M Y H:i:s T', time());
			$nonce  	   = uniqid() . uniqid();
			$string_to_sign= mb_convert_encoding($http_verb . $uri . $time_stamp . $nonce, 'UTF-8');
			$signature 	   = base64_encode(hash_hmac('sha1', $string_to_sign, $secret_key, true));
			 
			$feeds 		   = 'https://api.zanox.com/xml/2011-03-01' . $uri . '?connectid=' . $connectID . '&date=' . $time_stamp . '&nonce=' . $nonce . '&signature=' . $signature;
			 
			$contents      = simplexml_load_file($feeds);
			$json 	  	   = json_encode($contents);
			$contents 	   = json_decode($json,TRUE);	
			//echo "sasas".'<pre>';print_r($contents); exit;			
		}			
		if($flag==0)
		{				
			$results = $this->network_model->Zanox_trackingtransactions($contents);
			//print_r($results);die;
			$msg =array();		
			if($results['duplicate'] == 0){						
				$msg['success'] = 'Reports details imported successfully.';
			}
			else if($results['duplicate']!=0){					
				$msg['success'] = 'Reports details imported successfully.';
			}					
		}
		else 
		{
			$msg['success'] = 'No data found.';
		}
		//echo json_encode($msg);				
	}
	//Lomadee tracking transactions function
	function Lomadee_trackingtransactions()
	{
		$this->input->session_helper();
		$Affiliate_details = $this->network_model->Affiliate_details('Lomadee');
		//print_r($Affiliate_details);
		$aff_name =$Affiliate_details->affiliate_network;
		$token_id =$Affiliate_details->api_key;			
		$publisher_id =$Affiliate_details->networkid;			
		$flag=0;
		if($token_id!='')
		{			
			//01-06-2016 17-06-2016	
			$username     = 'fabriciocmello@gmail.com';
			$password     = 'q8VXso';
			$start_date   = date ("dmY");
			$start_date_format   = date ("Y-m-d");
			$end_date     = date('dmY',strtotime($start_date_format ."+10 days"));
		/* 	$start_date     = date('dmY',strtotime('01-06-2016'));
			$end_date     = date('dmY',strtotime('17-06-2016')); */
			$url = "https://api.lomadee.com/services/report/transactions/".$publisher_id."/?startDate=".$start_date."&endDate=".$end_date."&eventStatus=0";
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_NOBODY, false);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type:text/xml'));
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
			curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
			curl_setopt($ch, CURLOPT_USERAGENT,
			"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_REFERER, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0); 
			$haveresults_set = curl_exec($ch);

			/*Curl to json converter*/
			$xml 	  = simplexml_load_string($haveresults_set);
			$json 	  = json_encode($xml);
			$contents = json_decode($json,TRUE);
			//echo "<pre>"; print_r($contents);exit;
		}			
		if($flag==0)
		{				
			$results = $this->network_model->Lomadee_trackingtransactions($contents);
			//print_r($results);die;
			$msg =array();		
			if($results['duplicate'] == 0){						
				$msg['success'] = 'Reports details imported successfully.';
			}
			else if($results['duplicate']!=0){					
				$msg['success'] = 'Reports details imported successfully.';
			}					
		}
		else 
		{
			$msg['success'] = 'No data found.';
		}
		//echo json_encode($msg);				
	}
	//upload linkshare report automatically
	function linkshare_trackingtransactions()
	{
		$this->input->session_helper();
		$Affiliate_details = $this->network_model->Affiliate_details('Rakuten');
		//print_r($Affiliate_details);
		$aff_name =$Affiliate_details->affiliate_network;
		$apikey =$Affiliate_details->api_key;			
		$flag=0;
		if($apikey!='')
		{				
			include('Network/linkshare.php');		
		}			
		if($flag==0)
		{				
			$results = $this->network_model->import_trackingtransactions($newArray);
			//print_r($results);die;
			$msg =array();		
			if($results['duplicate'] == 0){						
				$msg['success'] = 'Reports details imported successfully.';
			}
			else if($results['duplicate']!=0){					
				$msg['success'] = 'Reports details imported successfully.';
			}					
		}
		else 
		{
			$msg['success'] = 'No data found.';
		}
		//echo json_encode($msg);				
	}
	// Cityads tracking transactions
	function Cityads_trackingtransactions()
	{
		$this->input->session_helper();
		$Affiliate_details = $this->network_model->Affiliate_details('Cityads');
		$apikey =$Affiliate_details->api_key;	 		
		$dateStart   = date ("Y-m-d");
		$dateEnd = date ("Y-m-d", strtotime ($dateStart ."+10 days"));  
 		/* $dateEnd = '2016-06-20';
		$dateStart = '2016-06-15'; */
		$flag=0;
		if($apikey!='')
		{		
			$url = 'http://us-geo.cityads.com/api/rest/webmaster/json/orderstatistics/'.$dateStart.'/'.$dateEnd.'?remote_auth='.$apikey;
			//echo $url;exit;
			$content = json_decode(file_get_contents($url),true);	
			//echo "<pre>";print_r($content);exit;		
		}			
		if($flag==0)
		{
			$results = $this->network_model->importCityads_trackingtransactions($content);
			//print_r($results);die;
			$msg =array();		
			if($results['duplicate'] == 0){						
				$msg['success'] = 'Reports details imported successfully.';
			}
			else if($results['duplicate']!=0){					
				$msg['success'] = 'Reports details imported successfully.';
			}					
		}
		else 
		{
			$msg['success'] = 'No data found.';
		}
		//echo json_encode($msg);				
	}
	// Afilio tracking transactions
	function afilio_trackingtransactions()
	{
		$this->input->session_helper();
		$Affiliate_details = $this->network_model->Affiliate_details('Afilio');
		//print_r($Affiliate_details);
		$aff_name =$Affiliate_details->affiliate_network;
		$affid =$Affiliate_details->networkid;			
		$apikey =$Affiliate_details->api_key;	 		
		$dateStart   = date ("Y-m-d");
		$dateEnd = date ("Y-m-d", strtotime ($dateStart ."+10 days"));
 		/* $dateEnd = '2016-06-15';
		$dateStart = '2016-06-01'; */
		$flag=0;
		if($apikey!='')
		{				
			$url = 'https://v2.afilio.com.br/api/leadsale_api.php?mode=list&token='.$apikey.'&affid='.$affid.'&type=sale&dateStart='.$dateStart.'&dateEnd='.$dateEnd.'&format=JSON&dateType=transaction';
			/* $html = json_decode(file_get_contents($url),true);	 */
			$html = file_get_contents($url);			
			$cont = iconv('UTF-8', 'ISO-8859-1//TRANSLIT',$html);		
			$content = json_decode($cont,true);
			
		}			
		if($flag==0)
		{
			$results = $this->network_model->importafilio_trackingtransactions($content);
			//print_r($results);die;
			$msg =array();		
			if($results['duplicate'] == 0){						
				$msg['success'] = 'Reports details imported successfully.';
			}
			else if($results['duplicate']!=0){					
				$msg['success'] = 'Reports details imported successfully.';
			}					
		}
		else 
		{
			$msg['success'] = 'No data found.';
		}
		//echo json_encode($msg);				
	}

	/*Real Time Tracking Transactions End*/

	
	/*New code for Upload Coupons details  7-6-16.*/
	function upload_apicoupons()
	{	
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');

		if($admin_id=="")
		{
		  redirect('adminsettings/index','refresh');
		} 
		else
		{	
			if($this->input->post('uploadtype'))
			{
				$upload_type    = $this->input->post('uploadtype');
				$affiliate_name = $this->input->post('aff_type');
				
				if($upload_type == 'Coupons_upload')
				{
					if($affiliate_name!='')
					{
						
						if($affiliate_name == 'zanox')
						{

							$connection_id  = '6D2504B4C68860782BBA';
							$secret_key     = '7bf98038714c45+4b20e6aec684570/1CBef494c';
							$url 		    = 'http://api.zanox.com/json/2011-03-01/incentives/?connectid='.$connection_id.'&region=BR&adspace=2149716&incentiveType=coupons';
							$content        = json_decode(file_get_contents($url),true);
							$content_status = $content['items'];
							//echo "Zanox <pre>"; print_r($content); exit;
						}

						if($affiliate_name == 'rakuten')
						{	
							
												
							$token_id  		= '3d0ad0eda6d7a35fc31a685efbf9d19c82475bbb20929b5c060886cbae822535';
							$url   	   		= 'http://couponfeed.linksynergy.com/coupon?token='.$token_id;
							$content_url	= file_get_contents($url);
							$xml_content    = simplexml_load_string($content_url);
							$json_content   = json_encode($xml_content);
							$content        = json_decode($json_content,TRUE);
							$content_status = $content['TotalMatches'];
							//echo "<pre>"; print_r($content); exit;
							//echo $content_status; exit;
						}
						
						if($affiliate_name == 'cityads')
						{	
							
							$token_id    	= '3d7b3dd997d6e957b8924240eb6097cb7a02ed549c9c5d0523f500ea33f366e0';
							$client_id   	= '649885';
							$secret_key  	= '051cb84b51716df8bad6ad90c1083d8e'; 
							$remote_auth 	= '5049f98b30b43c185c8917fb719576dc';
							$url         	= 'http://api.cityads.com/api/rest/webmaster/xml/coupons?remote_auth='.$remote_auth;
							
							//echo $url; exit; 
							$content_url	= file_get_contents($url);
							$xml_content    = simplexml_load_string($content_url);
							//echo "<pre>"; print_r($xml_content); exit;
							$json_content   = json_encode($xml_content);
							$content        = json_decode($json_content,TRUE);
							//$content 	    = json_decode(file_get_contents($url),true);
							$content_status = $content['status'];
							//echo '<pre>';print_r($content);exit;
						} 
						
						if($affiliate_name == 'afilio')
						{	
			
							$username      = 'pingou';
							$password      = 'yhasdf@fm16';
							$token_id      = '57581974981921.99713763';
							$site_id       = '47777';
							$aff_id        = '42747';
							$url 		   = 'http://v2.afilio.com.br/api/feedproducts.php?token='.$token_id.'&mode=dl&siteid='.$site_id.'&affid='.$aff_id;
							
							/*New code for csv file to convert a response details start 18-6-16.*/
							// Arrays we'll use later
							$keys = array();
							$newArray = array();
							// Function to convert CSV into associative array
							function csvToArray($file, $delimiter) 
							{ 
							  	if (($handle = fopen($file, 'r')) !== FALSE)
							  	{ 
							    	$i = 0; 
							    	while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) 
							    	{ 
							      		for ($j = 0; $j < count($lineArray); $j++) 
							      		{ 
							        		$arr[$i][$j] = $lineArray[$j]; 
							      		} 
							      		$i++; 
							    	} 
							    	fclose($handle); 
								} 
							  	return $arr; 
							} 
							
							// Do it
							$data = csvToArray($url, ';');
							//echo "<pre>";print_r($data); exit;
							// Set number of elements (minus 1 because we shift off the first row)
							$count = count($data) - 1; 
							//Use first row for names  
							// $labels = array_shift($data);  
							//print_r($data[0]); exit;
							foreach ($data[0] as $label) 
							{ 
							  $keys[] = $label; 
							}  
							 
							// Add Ids, just in case we want them later
							$keys[] = 'id';
							
							for ($i = 1; $i < $count; $i++)
							{
							  $data[$i][] = $i;
							}
							// Bring it all together
							for ($j = 1; $j < $count; $j++) 
							{
							  $d = array_combine($keys,$data[$j]);
							  $newArray[$j] = $d;
							}

							$content = $newArray;
							 
							//echo "<pre>"; print_r($content); exit;
							$content_status= 1;
							/*End 18-6-16*/
						}
						
						//echo $content_status; exit; 
						if($content_status!=0)
						{				
							if($flag==0)
							{
								$getdatas = $this->network_model->import_apicoupons($content,$affiliate_name);
								
								if($getdatas['duplicate'] == 0)
								{
									$this->session->set_flashdata('success', 'Coupons details imported successfully.');
									redirect('networks/upload_apicoupons','refresh');
								}
								else if($getdatas['duplicate']!=0)
								{		
									$this->session->set_flashdata('error', 'New Coupons details added successfully and <span style="color:red">'.$getdatas['duplicate'].'</span> duplicate records neglected. The duplicate transactions ids are '.$getdatas['trans_id']);
									redirect('networks/upload_apicoupons','refresh');
								}					
							}
						}
						else 
						{
							$this->session->set_flashdata('error','No data found.');
							redirect('networks/upload_apicoupons','refresh');
						}	
					}	
				}
			}		
				$data['action'] = "coupons";
				$this->load->view('adminsettings/upload_apicoupons',$data);
		}	
	}
	/*End*/

	/*New code for upload report details 7-6-16*/
	function upload_apireport()
	{	

		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
		  redirect('adminsettings/index','refresh');
		} else
		{	
			
			if($this->input->post('uploadtype'))
			{
				//print_r($_POST); exit;
				$upload_type    = $this->input->post('uploadtype');
				$affiliate_name = $this->input->post('aff_type');
				
				if($upload_type == 'Report_upload')
				{
					if($affiliate_name!='')
					{
						if($affiliate_name == 'zanox')
						{
							
							$connectID     = '6BA59464117A45ADAB2D';
							$secret_key    = '7bf98038714c45+4b20e6aec684570/1CBef494c';  
							$http_verb     = 'GET';
							$date          = date('Y-m-d',strtotime($this->input->post('reportdate')));
							$uri           = '/reports/sales/date/' . $date;
							$time_stamp    = gmdate('D, d M Y H:i:s T', time());
							$nonce  	   = uniqid() . uniqid();
							$string_to_sign= mb_convert_encoding($http_verb . $uri . $time_stamp . $nonce, 'UTF-8');
							$signature 	   = base64_encode(hash_hmac('sha1', $string_to_sign, $secret_key, true));
							 
							$feeds 		   = 'https://api.zanox.com/xml/2011-03-01' . $uri . '?connectid=' . $connectID . '&date=' . $time_stamp . '&nonce=' . $nonce . '&signature=' . $signature; //.'datetype=modified_date&adspace=2149716'
							//echo $feeds; exit;
							$contents      = simplexml_load_file($feeds);
							//echo '<pre>';print_r($contents); exit;
							$json 	  	   = json_encode($contents);
							$contents 	   = json_decode($json,TRUE);	
							//echo '<pre>';print_r($contents);exit;
							$status 	   = $contents['total'];
						}

						if($affiliate_name == 'lomadee')
						{
							$username     = 'fabriciocmello@gmail.com';
							$password     = 'q8VXso';
							$start_date   = date('dmY',strtotime($this->input->post('startdate')));
							$end_date     = date('dmY',strtotime($this->input->post('enddate')));
							$publisher_id = '22679375';
							$token_id     = 'e2ZhYnJpY2lvY21lbGxvQGdtYWlsLmNvbX06e3E4Vlhzb30=';
							//$url 		  = "https://bws.buscape.com.br/api/lomadee/reportTransaction/e2ZhYnJpY2lvY21lbGxvQGdtYWlsLmNvbX06e3E4Vlhzb30=?startDate=15062016&endDate=15062016&eventStatus=0&publisherId=22679375";
							$url		  = "https://api.lomadee.com/services/report/transactions/".$publisher_id."/?startDate=".$start_date."&endDate=".$end_date."&eventStatus=0&currency=MBL";
							//echo $url; exit;
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_HEADER, false);
							curl_setopt($ch, CURLOPT_NOBODY, false);
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
							curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type:text/xml'));
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
							curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
							curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
							curl_setopt($ch, CURLOPT_USERAGENT,
							"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_REFERER, $url);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
							curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0); 
							$haveresults_set = curl_exec($ch);
							//echo "<pre>"; print_r($haveresults_set); exit;
							//Curl to json converter//
							$xml 	  = simplexml_load_string($haveresults_set);
							$json 	  = json_encode($xml);
							$contents = json_decode($json,TRUE);
							//End//
							//echo "<pre>"; print_r($contents); exit;
							//echo count($contents, COUNT_RECURSIVE);
							//echo count($contents['item']);
							//exit;
							$status  = $contents['details']['elapsedTime'];
						}

						//Pon prakesh//
						if($affiliate_name == 'rakuten')
						{
							$username   = 'facmello';
							$password   = 'yhasdf@Fm80';
							//$token_id   = '3d7b3dd997d6e957b8924240eb6097cb7a02ed549c9c5d0523f500ea33f366e0';
							$report_id  = '3326233';
							$start_date = $this->input->post('startdate');
							$end_date   = $this->input->post('enddate');
							
							header('Content-type: application/json');
							// Set your CSV feed
							$feed = 'https://ran-reporting.rakutenmarketing.com/en/reports/sales-and-activity-reports1/filters?date_range=this-month&include_summary=Y&network=8&tz=GMT&date_type=transaction&token=ZW5jcnlwdGVkYToyOntzOjU6IlRva2VuIjtzOjY0OiI2NzFiZjE1NTViYTk4ZDJhMGNkNjY5ODY5NTZhZWI4N2I2MTgyY2IxM2UwM2Y4MDBjNWU3MWM0NjljNzNhMzk4IjtzOjg6IlVzZXJUeXBlIjtzOjk6IlB1Ymxpc2hlciI7fQ%3D%3D';

							// Arrays we'll use later
							$keys = array();
							$newArray = array();
							// Function to convert CSV into associative array
							function csvToArray($file, $delimiter) 
							{ 
							  	if (($handle = fopen($file, 'r')) !== FALSE)
							  	{ 
							    	$i = 0; 
							    	while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) 
							    	{ 
							      		for ($j = 0; $j < count($lineArray); $j++) 
							      		{ 
							        		$arr[$i][$j] = $lineArray[$j]; 
							      		} 
							      		$i++; 
							    	} 
							    	fclose($handle); 
								} 
							  	return $arr; 
							} 
							
							// Do it
							$data = csvToArray($feed, ',');
							// Set number of elements (minus 1 because we shift off the first row)
							$count = count($data) - 1; 
							//Use first row for names  
							// $labels = array_shift($data);  
							foreach ($data[4] as $label) 
							{
							  $keys[] = $label;
							}
							// Add Ids, just in case we want them later
							$keys[] = 'id';
							for ($i = 5; $i < $count; $i++)
							{
							  $data[$i][] = $i;
							}
							// Bring it all together
							for ($j = 5; $j < $count; $j++) 
							{
							  $d = array_combine($keys, $data[$j]);
							  $newArray[$j] = $d;
							}

							$contents = $newArray;
							$status   = 1;
							//echo "<pre>"; print_r($contents); exit;
						}
 						//Pon prakesh//
						if($affiliate_name == 'cityads')
						{
							
							$username    = 'facmello';
							$password    = 'yhasdf@Fm80';
							$token_id    = '3d7b3dd997d6e957b8924240eb6097cb7a02ed549c9c5d0523f500ea33f366e0';
							$client_id   = '649885';
							$secret_key  = '5049f98b30b43c185c8917fb719576dc'; 
							$Remote_auth = 'ae757f7e4f1be3bcb64d67eeab39ed86';
							$start_date  = date('Y-m-d',strtotime($this->input->post('startdate')));
							$end_date    = date('Y-m-d',strtotime($this->input->post('enddate')));
							$url         = 'http://us-geo.cityads.com/api/rest/webmaster/json/orderstatistics/'.$start_date.'/'.$end_date.'?remote_auth='.$secret_key;
							//echo $url; exit; 
							//$url       = 'http://api.cityads.com/api/rest/webmaster/json/statistics-offers/action_id/'.$start_date.'/'.$end_date.'?remote_auth='.$secret_key;
							$contents  	 = json_decode(file_get_contents($url),true);
							//echo "<pre>"; print_r($contents); exit;
							$status 	 = $contents['status'];
						} 
						
						//report response details Completed//
						if($affiliate_name == 'afilio')
						{
							
							$username      = 'pingou';
							$password      = 'yhasdf@fm16';
							$token_id      = '57581974981921.99713763';
							$site_id       = '47777';
							$aff_id        = '42747';
							$start_date    = date('Y-m-d',strtotime($this->input->post('startdate')));
							$end_date      = date('Y-m-d',strtotime($this->input->post('enddate')));
							//$url 		   = 'https://v2.afilio.com.br/api/leadsale_api.php?mode=list&token='.$token_id.'&affid='.$aff_id.'&type=sale&dateStart='.$start_date.'&dateEnd='.$end_date.'&format=XML';
							$url 		   = 'https://v2.afilio.com.br/api/leadsale_api.php?mode=list&token='.$token_id.'&affid='.$aff_id.'&type=sale&dateStart='.$start_date.'&dateEnd='.$end_date.'&format=XML&dateType=transaction';
				  			
				  			$contents 	   = file_get_contents($url);
							$xml 	  	   = simplexml_load_string($contents, "SimpleXMLElement", LIBXML_NOCDATA);
							$json 	  	   = json_encode($xml);
							$contents 	   = json_decode($json,TRUE);	
							//echo "<pre>"; print_r($contents);exit;
							$status 	   = 1;
						}

						if($status != '')
						{				
							if($flag==0)
							{
								$getdatas = $this->network_model->import_apireports($contents,$affiliate_name);
								 
								if($getdatas == 1)
								{
									$this->session->set_flashdata('success', 'Report details imported successfully.');
									redirect('networks/upload_apireport','refresh');
								}
								else if($getdatas == 0)
								{	

									$this->session->set_flashdata('error', 'Report Details Already Added.');
									redirect('networks/upload_apireport','refresh');
								}
								else
								{
									$this->session->set_flashdata('error', 'Report Details Not Found.');
									redirect('networks/upload_apireport','refresh');
								}					
							}
						}
						else 
						{
							$this->session->set_flashdata('error','No data found.');
							redirect('networks/upload_apireport','refresh');
						}		
					}	
				}
			}		
				$data['action'] = "report";
				$this->load->view('adminsettings/upload_apireport',$data);
		}	
	}
	/*END*/

	/*new code for Update a Zanox API report details 24-2-17*/
	function update_api($api_name)
	{
		 
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if(($admin_id==""))
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			if($this->input->post('update_apireports'))
			{	
				//Completed 
				if($api_name == 'zanox')
				{
					//print_r($_POST); exit;
					//$report_date = date('Y-m-d',strtotime($this->input->post('reportdate')));
					$ad_space      = $this->input->post('adspace');
					$zanox_status  = $this->input->post('zanox_status');
					$date_type     = $this->input->post('date_type');
					$offer_id      = $this->input->post('offer_name');
					
					$connectID     = '6BA59464117A45ADAB2D';
					$secret_key    = '7bf98038714c45+4b20e6aec684570/1CBef494c';  
					$http_verb     = 'GET';
					$date          = date('Y-m-d',strtotime($this->input->post('reportdate')));
					$uri           = '/reports/sales/date/' . $date;
					$time_stamp    = gmdate('D, d M Y H:i:s T', time());
					$nonce  	   = uniqid() . uniqid();
					$string_to_sign= mb_convert_encoding($http_verb . $uri . $time_stamp . $nonce, 'UTF-8');
					$signature 	   = base64_encode(hash_hmac('sha1', $string_to_sign, $secret_key, true));

					
					if($offer_id != 'all')
					{
						$val .= '&program='.$offer_id.'';
					}
					 
					if($zanox_status != 'all')
					{
						$val .= '&state='.$zanox_status.'';
					}
					
					$feeds   = 'https://api.zanox.com/xml/2011-03-01'.$uri.'?datetype='.$date_type.'&adspace='.$ad_space.$val.'&connectid='.$connectID.'&date='.$time_stamp.'&nonce='.$nonce.'&signature='.$signature;
					//echo $feeds; 
					$contents      = simplexml_load_file($feeds);
					//echo '<pre>';print_r($contents); exit;
					$json 	  	   = json_encode($contents);
					$contents 	   = json_decode($json,TRUE);	
					//echo '<pre>';print_r($contents);exit;
					$status 	   = $contents['total'];
				}
				//Completed
				if($api_name == 'lomadee')
				{
					$offer_id     = $this->input->post('offer_name');
					$publisher_id = $this->input->post('adspace');
					$loma_status  = $this->input->post('lomadee_status');

					$username     = 'fabriciocmello@gmail.com';
					$password     = 'q8VXso';
					$start_date   = date('dmY',strtotime($this->input->post('report_startdate')));
					$end_date     = date('dmY',strtotime($this->input->post('report_enddate')));
					//$publisher_id = '22679375';
					$token_id     = 'e2ZhYnJpY2lvY21lbGxvQGdtYWlsLmNvbX06e3E4Vlhzb30=';
					
					/*if($offer_id != 'all')
					{
						$val = '&advertiserId='.$offer_id.'';
					}*/

					//$url = "https://api.lomadee.com/services/report/transactions/".$publisher_id."/?startDate=".$start_date.$val."&endDate=".$end_date."&eventStatus=0&currency=MBL";
					$url = "https://api.lomadee.com/services/report/transactions/".$publisher_id."/?startDate=".$start_date."&endDate=".$end_date."&eventStatus=".$loma_status."&currency=MBL"; //$val.
					//echo $url;
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_HEADER, false);
					curl_setopt($ch, CURLOPT_NOBODY, false);
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
					curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type:text/xml'));
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
					curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
					curl_setopt($ch, CURLOPT_USERAGENT,
					"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_REFERER, $url);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0); 
					$haveresults_set = curl_exec($ch);
					//echo "<pre>"; print_r($haveresults_set); exit;
					//Curl to json converter//
					$xml 	  = simplexml_load_string($haveresults_set);
					$json 	  = json_encode($xml);
					$contents = json_decode($json,TRUE);
					//End//
					//echo "<pre>"; print_r($contents); exit;
					//echo count($contents, COUNT_RECURSIVE);
					//echo count($contents['item']);
					//exit;
					$status  = $contents['details']['elapsedTime'];
				}
				//Not yet Completed
				if($api_name == 'rakuten')
				{
					 
					$ad_space      = $this->input->post('adspace');
					$zanox_status  = $this->input->post('zanox_status');
					$date_type     = $this->input->post('date_type');
					$offer_id      = $this->input->post('offer_name');

					if($offer_id != 'all')
					{
						$val .= '&program='.$offer_id.'';
					}
					 
					if($zanox_status != 'all')
					{
						$val .= '&state='.$zanox_status.'';
					}

					$username      = 'facmello';
					$password      = 'yhasdf@Fm80';
					//$token_id    = '3d7b3dd997d6e957b8924240eb6097cb7a02ed549c9c5d0523f500ea33f366e0';
					$report_id     = '3326233';
					$start_date    = $this->input->post('startdate');
					$end_date      = $this->input->post('enddate');
					
					header('Content-type: application/json');
					// Set your CSV feed
					$feed = 'https://ran-reporting.rakutenmarketing.com/en/reports/sales-and-activity-reports1/filters?date_range=this-month&include_summary=Y&network=8&tz=GMT&date_type='.$date_type.'&token=ZW5jcnlwdGVkYToyOntzOjU6IlRva2VuIjtzOjY0OiI2NzFiZjE1NTViYTk4ZDJhMGNkNjY5ODY5NTZhZWI4N2I2MTgyY2IxM2UwM2Y4MDBjNWU3MWM0NjljNzNhMzk4IjtzOjg6IlVzZXJUeXBlIjtzOjk6IlB1Ymxpc2hlciI7fQ%3D%3D'.$val;
					//echo $feed; exit;
					// Arrays we'll use later
					$keys = array();
					$newArray = array();
					// Function to convert CSV into associative array
					function csvToArray($file, $delimiter) 
					{ 
					  	if (($handle = fopen($file, 'r')) !== FALSE)
					  	{ 
					    	$i = 0; 
					    	while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) 
					    	{ 
					      		for ($j = 0; $j < count($lineArray); $j++) 
					      		{ 
					        		$arr[$i][$j] = $lineArray[$j]; 
					      		} 
					      		$i++; 
					    	} 
					    	fclose($handle); 
						} 
					  	return $arr; 
					} 
					
					// Do it
					$data = csvToArray($feed, ',');
					// Set number of elements (minus 1 because we shift off the first row)
					$count = count($data) - 1; 
					//Use first row for names  
					// $labels = array_shift($data);  
					foreach ($data[4] as $label) 
					{
					  $keys[] = $label;
					}
					// Add Ids, just in case we want them later
					$keys[] = 'id';
					for ($i = 5; $i < $count; $i++)
					{
					  $data[$i][] = $i;
					}
					// Bring it all together
					for ($j = 5; $j < $count; $j++) 
					{
					  $d = array_combine($keys, $data[$j]);
					  $newArray[$j] = $d;
					}

					$contents = $newArray;
					$status   = 1;
					//echo "<pre>"; print_r($contents); exit;
				}
				//Completed 
				if($api_name == 'cityads')
				{	
					$offer_id     = $this->input->post('offer_name');
					$date_type    = $this->input->post('date_type');
					$city_status  = $this->input->post('cityads_status');
					
					if($offer_id != 'all')
					{
						$val .= '&q='.$offer_id.'';
					}
					 
					/*if($city_status != 'all')
					{
						$val .= '&status='.$city_status.'';
					} */

					$username     = 'facmello';
					$password     = 'yhasdf@Fm80';
					$token_id     = '3d7b3dd997d6e957b8924240eb6097cb7a02ed549c9c5d0523f500ea33f366e0';
					$client_id    = '649885';
					$secret_key   = '5049f98b30b43c185c8917fb719576dc'; 
					$Remote_auth  = 'ae757f7e4f1be3bcb64d67eeab39ed86';
					$start_date   = date('Y-m-d',strtotime($this->input->post('report_startdate')));
					$end_date     = date('Y-m-d',strtotime($this->input->post('report_enddate')));
					$url          = 'http://us-geo.cityads.com/api/rest/webmaster/json/orderstatistics/'.$start_date.'/'.$end_date.'?remote_auth='.$secret_key.$val.'&date_type='.$date_type;
					//echo $url; 
					//$url       = 'http://api.cityads.com/api/rest/webmaster/json/statistics-offers/action_id/'.$start_date.'/'.$end_date.'?remote_auth='.$secret_key;
					$contents  	  = json_decode(file_get_contents($url),true);
					//echo "<pre>"; print_r($contents); exit;
					$status 	  = $contents['status'];
				} 
				
				if($api_name == 'afilio')
				{
					
					$site_id       = $this->input->post('siteid');
					$date_type     = $this->input->post('date_type');
					$offer_id  	   = $this->input->post('offer_name');
					$afilio_status = $this->input->post('afilio_status');
					$sub_status    = $this->input->post('aff_newstatus');

					if($offer_id != 'all')
					{
						$val = '&progid='.$offer_id.'';
					}
					
					 
					/*else if($afilio_status == 'completed' && $sub_status == 'pending')
					{
						$status .= '&status=aceito&payment=Unpaid';
					}
					else if($afilio_status == 'completed' && $sub_status == 'complete')
					{
						$status .= '&status=aceito&payment=paid';
					}
					else 
					{
						$status .= '&status='.$afilio_status;
					}*/
					 

					$username    = 'pingou';
					$password    = 'yhasdf@fm16';
					$token_id    = '57581974981921.99713763';
					//$site_id     = '47777';
					$aff_id      = '42747';
					$start_date  = date('Y-m-d',strtotime($this->input->post('report_startdate')));
					$end_date    = date('Y-m-d',strtotime($this->input->post('report_enddate')));
					//$url 		 = 'https://v2.afilio.com.br/api/leadsale_api.php?mode=list&token='.$token_id.'&affid='.$aff_id.'&type=sale&dateStart='.$start_date.'&dateEnd='.$end_date.'&format=XML';
		  			$url 		 = 'https://v2.afilio.com.br/api/leadsale_api.php?mode=list&token='.$token_id.'&affid='.$aff_id.'&type=sale&dateStart='.$start_date.'&dateEnd='.$end_date.$val.'&dateType='.$date_type.'&siteid='.$site_id.'&format=XML'; //$status.transaction .$status
		  			//echo $url;
		  			$contents    = file_get_contents($url);
					$xml 	     = simplexml_load_string($contents, "SimpleXMLElement", LIBXML_NOCDATA);
					$json 	     = json_encode($xml);
					$contents    = json_decode($json,TRUE);	
					//echo "<pre>"; print_r($contents);exit;
					$status 	   = 1;
				}

				if($status != '')
				{				
					if($flag==0)
					{
						$getdatas = $this->network_model->update_apireports($contents,$api_name);
						 
						if($getdatas == 1)
						{
							$this->session->set_flashdata('success', 'Report details Updated successfully.');
							redirect('networks/update_api/'.$api_name,'refresh');
						}
						/*else if($getdatas == 0)
						{	
							$this->session->set_flashdata('error', 'Report Details Not Found.');
							redirect('networks/update_api/'.$api_name,'refresh');
						}
						else
						{
							$this->session->set_flashdata('error', 'Report Details Not Found.');
							redirect('networks/update_api/'.$api_name,'refresh');
						}*/					
					}
				}
				else 
				{
					$this->session->set_flashdata('error','No data found.');
					redirect('networks/update_api/'.$api_name,'refresh');
				}
			} 
			$this->load->view('adminsettings/update_api',$data);	
			
		}
	}

	function convertCurrency($amount,$from,$to)
	{
		    $url  = "https://www.google.com/finance/converter?a=$amount&from=$from&to=$to";
		    $data = file_get_contents($url);
		    preg_match("/<span class=bld>(.*)<\/span>/",$data, $converted);
		    $converted = preg_replace("/[^0-9.]/", "", $converted[1]);
		    return round($converted, 3);
	}



	function apizanox()
	{
		
		//$date 		  = $_REQUEST['date'];
		$offer_provider	  = $_REQUEST['offer_provider'];
		$pay_out_currency = $_REQUEST['pay_out_currency'];
		$pay_out_amount	  = $_REQUEST['pay_out_amount'];
		$sale_currency    = $_REQUEST['sale_currency'];
		$sale_amount      = $_REQUEST['sale_amount'];
		$transaction_id	  = $_REQUEST['transaction_id'];
		$user_tracking_id = $_REQUEST['user_tracking_id'];
		$extra_tracking	  = $_REQUEST['extra_tracking'];

		//$extra_tracking   = '1-1-05.00-12.00-00001';

		if($extra_tracking != '')
		{
			$newtracking    = explode('-', $extra_tracking);
			$platform       = $newtracking[0];
			$cashback_type  = $newtracking[1];
			$cashback_value = $newtracking[2];
			$commission_val = $newtracking[3];
			$five_digit_num = $newtracking[4];
		}

		if($platform != 0)
		{
			//USD to BRL convertion 28-3-17//
			if($sale_currency == 'USD')
			{
				$new_sale_amount = $this->convertCurrency($sale_amount,'USD','BRL');
			}
			else if($sale_currency == 'EUR')
			{
				$new_sale_amount = $this->convertCurrency($sale_amount,'EUR','BRL');
			}
			else
			{
				$new_sale_amount = $sale_amount;
			}
			//USD to BRL convertion 28-3-17//

			if($platform == 1 || $platform == 2)
			{
				//Cashback amount calculation 28-3-17//

				if($cashback_type == 1)
				{
					$cashback_amt   = ($new_sale_amount)*($cashback_value/100); 
					$commission_amt = $commission_val;
				}

				if($cashback_type == 2)
				{
					$cashback_amt   = $cashback_value;
					$commission_amt = $commission_val;
				}

				//Cashback amount calculation 28-3-17
			}
			if($platform == 3)
			{
				//Via Cashback exclusive// 
				if($new_sale_amount > $cashback_value)
				{
					$cashback_amt   = $cashback_value;
				}
				else
				{
					$cashback_amt   = $new_sale_amount;
				}
				
				$commission_amt =  $commission_val;
			}
			
			/*if($pay_out_currency == 'USD')
			{
				$pay_out_amount = $this->convertCurrency($pay_out_amount,'USD','BRL');
			}*/


			if(strpos($offer_provider,'%20') !== false) 
			{
			    $offer_name = str_replace("%20"," ",$offer_provider);
			}
			else
			{
				$offer_name = $offer_provider; 
			}

			$offer_name 	= utf8_encode($offer_name);

			$coupon_name  	= trim($offer_name, " BR");
			$date 		  	= date("Y-m-d H:i:s", $date);

			$aff_details 	 = $this->db->query("SELECT * from affiliates where affiliate_name='$coupon_name'")->row();
			$pingou_store_id = $aff_details->affiliate_id;

			if($pingou_store_id!='')
			{
				$data = array('zanox_offer_provider' => $offer_name);
	            $this->db->where('affiliate_name',$coupon_name);
			    $updation = $this->db->update('affiliates',$data); 


				$all = 0;
				$this->db->where('transaction_id',$transaction_id);
				$all = $this->db->get('tbl_report')->num_rows();				 	  	 
				 
				if($all == 0)
				{
					$duplicate   = 1;
					$val_user_id = substr($user_tracking_id, 0, 5);

					if($val_user_id == 'P0001')
					{
						$get_userid = decode_userid($user_tracking_id);	
					}

					if($new_sale_amount!='')
					{
		 
						$this->db->query("INSERT INTO `tbl_report` (`offer_provider`, `date`, `pay_out_amount`, `sale_amount`, `transaction_id`, `user_tracking_id`, `last_updated`,`cashback_amount`,`ref_cashback_percent`,`ref_cashback_amount`,`total_Cashback_paid`, `status`,`report_update_id`,`five_digits`) 
						VALUES ('$offer_name', '".date('Y-m-d H:i:s')."', '$pay_out_amount', '$sale_amount', '$transaction_id','$get_userid', '".date('Y-m-d H:i:s H:i:s')."','$cashback_amt','$ref_cashback_percent','$ref_cashback_amount','$cashback_amt','".date('Y-m-d H:i:s')."','$transaction_id','$five_digit_num')");
					 	 
						$this->db->query("INSERT INTO `cashback` (`user_id`,`coupon_id`,`cashback_amount`,`affiliate_id`,`status`, `date_added`, `transaction_amount`, `transaction_date`,`new_txn_id`,`commission`,`report_update_id`,`pingou_store_id`,`uploaded_by`,`affiliate_network`,`five_digits`,`platform`,`type_cb`,`calc_tb`)
						VALUES ('$get_userid','$offer_name',$cashback_amt,'$offer_name','Pending','".date('Y-m-d H:i:s')."', '$new_sale_amount', '".date('Y-m-d H:i:s')."','$transaction_id','$commission_amt','$transaction_id','$pingou_store_id','realtime','zanox','$five_digit_num','$platform','$cashback_type','$cashback_value')");	
					

						$this->db->where('admin_id',1);
						$admin_det = $this->db->get('admin');
						if($admin_det->num_rows >0) 
						{    
							$admin 		 = $admin_det->row();
							$admin_email = $admin->admin_email;
							$site_name 	 = $admin->site_name;
							$admin_no 	 = $admin->contact_number;
							$site_logo 	 = $admin->site_logo;
						}
								
						$date = date('Y-m-d');
						
						$userdetails = $this->db->query("SELECT * from `tbl_users` where `user_id`=$get_userid")->row();
						
						if(count($userdetails) > 0)
						{
							$cashback_status = $userdetails->cashback_mail;
							$getuser_mail 	 = $userdetails->email;
							$first_name      = $userdetails->first_name;
							$last_name       = $userdetails->last_name;
							
							if($first_name == '' && $last_name == '')
							{
								$ex_name   = explode('@', $getuser_mail);
								$user_name = $ex_name[0]; 
							}
							else
							{
								$user_name = $first_name.''.$last_name;
							}
							/* mail for pending cashback */
							if($cashback_status == 1)
							{
								$this->db->where('mail_id',10);
								$mail_template = $this->db->get('tbl_mailtemplates');
								if($mail_template->num_rows >0) 
								{
								   	$fetch 	   = $mail_template->row();
								    $subject   = $fetch->email_subject;
								    $templete  = $fetch->email_template;
								    $unsuburl  = base_url().'cashback/un-subscribe/cashback/'.$get_userid;
								    $myaccount = base_url().'cashback/minha-conta';
								    //$url 	   = base_url().'cashback/my_earnings/';
								   
									$this->load->library('email');
									
									$config = Array(
										'mailtype'  => 'html',
										'charset'   => 'utf-8',
									);
									
									$sub_data = array(
										'###SITENAME###'=>$site_name
									);
									$subject_new = strtr($subject,$sub_data);
									
									// $this->email->initialize($config);
									$this->email->set_newline("\r\n");
								    $this->email->initialize($config);
								    $this->email->from($admin_email,$site_name.'!');
								    $this->email->to($getuser_mail);
								    $this->email->subject($subject_new);
								   
									$data = array(
										'###NAME###'=>$user_name,
										'###COMPANYLOGO###' =>base_url()."uploads/adminpro/".$site_logo,
										'###SITENAME###'=>$site_name,
										'###ADMINNO###'=>$admin_no,
										'###DATE###'=>$date,
										'###AMOUNT###'=>str_replace('.', ',', $pay_out_amount),
										'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>',
										'###MYACLINK###'=>'<a href='.$myaccount.'>'.$myaccount.'</a>'
								    );
								   
								   $content_pop=strtr($templete,$data);
								   // echo $content_pop; echo $subject_new;
								   $this->email->message($content_pop);
								   $this->email->send();  
								}
							}	
							/* mail for pending cashback */	
						}
					}
				}
			}
		}			
	}

}
?>