<?php
class Mobile extends CI_Controller
{
 	public function __construct()
 	{	
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('front_model');
		session_start();
		
		  
		/*New code for not log user ip address added into database 18-11-16*/
		$user_id = $this->session->userdata('user_id');
		if($user_id == '')
		{
			$user_ip_details = $this->front_model->set_ip_address();
		}
		/*End 18-11-16*/
		if($this->uri->segment(1) == 'cms')
		{
			//redirect('404','refresh');
			redirect('','refresh');
		}

	}
	function register()
	{
		$this->input->session_helper();
		$arr_detail 	= $_REQUEST['register'];
		$request    	= json_decode($arr_detail);  
		$user_email	    = $request->user_email;
		$password 		= $request->password;
		$new_random 	= mt_rand(1000000,99999999);
		$date 	  	    = date('Y-m-d h:i:s');
		$uni_id   	    = $this->input->post('uni_id');
		$unic_code 	    = $this->random_string(10);

		$arrcat_type   = array('3454'=>2,'8765'=>3,'2345'=>4,'1647'=>5,'6536'=>6,'7486'=>7,'8362'=>8,'9326'=>9,'2695'=>10);
	    if (array_key_exists($cat_type_url,$arrcat_type))
		{
	    	$categorytype = $arrcat_type[$cat_type_url];
	    }
	    else
	    {	
	    	$categorytype = 1;
	    }

	    if($pagename)
		{
			$new_uid = $uni_id;
		}
		else
		{
			$new_uid = $ref_id;
		}
		if($new_uid)
		{	
			$this->db->where('random_code',$new_uid);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows() > 0)
			{	
				$fetch = $query->row();
				$user_id = $fetch->user_id;
				$ref_cat_type = $fetch->referral_category_type;

			}
			else
			{
				$user_id = 0;
			}
		}
		else
		{
			$user_id = 0;
			if($ref_cat_type == '')
			{
				$ref_cat_type = 0;
			}
		}

		$userdetailss = $this->db->query("select * from tbl_users where email='$user_email'");
		
		if($userdetailss->num_rows > 0)
		{
			$res['success'] = 0;
  			$res['message'] = 'Already EmailId has been registered';
		}
		else
		{	
			$data = array(		
			'email'=>$user_email,
			'password'=>$password,
			'random_code'=>$new_random,
			'refer'=>$user_id,
			'cashback_mail'=>1,
			'withdraw_mail'=>1,
			'referral_mail'=>1,
			'acbalance_mail'=>1,
			'support_tickets'=>1,
			'newsletter_mail'=>1,
			'referral_category_type'=>$categorytype,
			'date_added'=>$date,
			'unic_bonus_code'=>$unic_code,
			'ref_user_cat_type'=>$ref_cat_type
			);
			
			$this->db->insert('tbl_users',$data,$datas);
			$insert_id = $this->db->insert_id();

			$res['success']=1;
  			$res['message']="User Details has been Inserted successfully";
  			$res['user_id'] = $insert_id;
		}
		$json = json_encode($res);
		echo $json;
		/*
		//New code for (type 2 format) user referral cashback amount for referred user (Pending status) 8-9-16//
		if($user_id !=0)
		{
			$user_deta = $this->front_model->view_user($user_id);
			if($user_deta)
			{
				//New code for refferal category type//
				$categorytype = $user_deta[0]->referral_category_type;
				
				
				$name   = $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();
				$status = $name->ref_by_rate;
				if($status == 1)
				{	
					$caspe = $name->ref_cashback_rate;
				}
				//End//
				
				if($status == 1)
				{
					$this->db->select_max('trans_id');
					$result   = $this->db->get('transation_details')->row();  
					$trans_id = $result->trans_id;
					$trans_id = $trans_id+1;
					$n9  	  = '5236555';
					$n12 	  = $n9 + $trans_id; 
					$now 	  = date('Y-m-d');
					$newid 	  = rand(1000,9999);
					$newtransaction_id = md5($newid);


					$data = array(		
					'transation_amount' => $caspe,
					'user_id' => $user_id,
					'transation_date' => $now,
					'transaction_date' => $now,
					'transation_id'=>$n12,
					'transation_reason' => "Pending Referal Payment",
					'mode' => 'Credited',
					'details_id'=>'',
					'table'=>'',
					'report_update_id'=>$newtransaction_id,
					'ref_user_tracking_id' =>$insert_id,
					'transation_status ' => 'Pending');
					$this->db->insert('transation_details',$data);
				}	
			}
		}
		//End refferal details 8-9-16//

		//New code for (type 3 format) user referral cashback amount count of referred user(Pending status) 9-9-16//
		$referrals 			= $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();	
		$ref_by_percentage  = $referrals->ref_by_percentage;
		$ref_by_rate 		= $referrals->ref_by_rate;
		$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;
		$category_names     = ucfirst($referrals->category_type);
		
		//3 Bonus by Refferal Rate type//
		if($bonus_by_ref_rate == 1)
		{
			
			$newid 	= rand(1000,9999);
			$newtransaction_id = md5($newid);

			$n9      = '333445';
			$n12     = $n9 + $user_id;
			$now     = date('Y-m-d H:i:s');	
			$selqry  = "SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Pending Referal Payment' AND user_id=$user_id"; 
			$query 	 =  $this->db->query("$selqry");
			$numrows = $query->num_rows();

			if($numrows > 0)
			{
				$fetch 			= $query->row();
				$usercount 		= $fetch->userid;
				$referrals      = $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();	
				$bonus_amount   = $referrals->ref_cashback_rate_bonus;
				$friends_count  = $referrals->friends_count;

				if($usercount == $friends_count)
				{	
					if($bonus_amount!='')
					{	 
						$referral_amt = $user_deta[0]->referral_amt;
						if($referral_amt == 0)
						{
							$data = array(			
							'transation_amount' => $bonus_amount,	
							'user_id' => $user_id,	
							'transation_date' => $now,
							'transaction_date' => $now,
							'transation_id'=>$n12,	
							'transation_reason' => 'Referral Bonus for '. ucfirst($category_names) .' User',	
							'mode' => 'Credited',
							'details_id'=>'',	
							'table'=>'',	
							'new_txn_id'=>0,
							'transation_status ' => 'Pending',
							'report_update_id'=>$newtransaction_id,
							'ref_bonus_type'=>$categorytype
							);	
							$this->db->insert('transation_details',$data);
						
							//User table update referral status//
							$data = array(		
							'referral_amt' => 1);
							$this->db->where('user_id',$user_id);
							$update_qry = $this->db->update('tbl_users',$data);	
							//End//
						}		
					}	
				}
			} 
		}
		//End referral details 9-9-16//

		//New code for activation method 18-4-16//
		$admindetailssss = $this->front_model->getadmindetails_main();
		$activate  		 = $admindetailssss->activate_method;
		//End//

		//New code for Add a subscriber users list 31-8-16//
		$this->db->where('subscriber_email',$user_email);
		$query = $this->db->get('subscribers');
		if($query->num_rows() == 0)
		{
			$date = date('Y-m-d h:m:s');
			$data = array(
			'subscriber_email' => $user_email,
			'subscriber_status' => '1',
			'date_subscribed' => $date
			);
			$this->db->insert('subscribers',$data);
		}
		//End 31-8-16//
		
		if($new_uid)
		{
			$this->db->where('random_code',$new_uid);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows() > 0)
			{	
				$fetch = $query->row();
				$user_id = $fetch->user_id;
				$email =$fetch->email;
				
				$datas = array(
				'user_id' => $user_id,
				'user_email' => $email,
				'referral_email' => $user_email,
				'status' => 'Ativa',
				'date_added' => $date
				);
				$this->db->insert('referrals',$datas);
			}
		}
		
		//New code for referral for cashback_exclusive via registered user 30-7-16//
		$user_id 		  = $this->session->userdata('user_id'); 
		$cashback_details = $this->session->userdata('link_name');	
		$cashbackdetails  = $this->front_model->cashback_exclusive_details($cashback_details);
		$Email_id         = $cashbackdetails->user_email;
		$userdetailss     = $this->db->query("select * from tbl_users where email='$Email_id'")->row();
		$newuserid        = $userdetailss->user_id;
		if($user_id=='')
		{
			if($cashback_details !='')
			{
				$datas = array(
				'user_id' => $newuserid,
				'user_email' => $Email_id,
				'referral_email' => $user_email,
				'status' => 'Ativa',
				'date_added' => $date
				);
				$this->db->insert('referrals',$datas);
			}
		}
		//End 30-7-16//
		
		//New code for mail notification//
		if($activate == 0)
		{
		
			$this->db->where('admin_id',1);
			$admin_det = $this->db->get('admin');
			if($admin_det->num_rows >0) 
			{    
				$admin 		 = $admin_det->row();
				$admin_email = $admin->admin_email;
				$site_name 	 = $admin->site_name;
				$admin_no 	 = $admin->contact_number;
				$site_logo 	 = $admin->site_logo;
			}
			
			$date =date('d/m/Y');
			$first_name = $this->input->post('first_name');
			$last_name  = $this->input->post('last_name');
			$user_email = $this->input->post('user_email');
		
			$this->db->where('mail_id',1);
			$mail_template = $this->db->get('tbl_mailtemplates');
			
			if($mail_template->num_rows >0) 
			{        
			    $fetch    	 = $mail_template->row();
			    $subject  	 = $fetch->email_subject;  
			    $templete 	 = $fetch->email_template;  
			    $regurl	 	 = base_url().'verify-account/'.$new_random;
			    $unsuburl	 = base_url().'un-subscribe-signup/'.$new_random;

			   	$this->load->library('email'); 

				$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				);
				// $this->email->initialize($config);        
	     		$this->email->set_newline("\r\n");
				$this->email->initialize($config);        
				$this->email->from($admin_email,$site_name.'!');
				$this->email->to($user_email);
				$this->email->subject($subject);
			   	$data = array(
					'###USERNAME###'=>$first_name,
					'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
					'###SITENAME###'=>$site_name,
					'###ADMINNO###'=>$admin_no,
					'###DATE###'=>$date,
					'###LINK###'=>'<a href='.$regurl.'>'.$regurl.'</a>',
					'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>'
			   	);
			   
			    $content_pop=strtr($templete,$data);	
			    $this->email->message($content_pop);
			    $this->email->send();           
			}
			return 0;
		}
		if($activate == 1)
		{
			$datas = array('status'=>1);
			$this->db->where('user_id',$insert_id);
			$this->db->where('admin_status','');
			$update_qry = $this->db->update('tbl_users',$datas);

			$this->session->set_userdata('user_id',$insert_id);
			$this->session->set_userdata('user_email',$user_email);

			$this->db->where('admin_id',1);
			$admin_det = $this->db->get('admin');
			if($admin_det->num_rows >0) 
			{    
				$admin 		 = $admin_det->row();
				$admin_email = $admin->admin_email;
				$site_name 	 = $admin->site_name;
				$admin_no 	 = $admin->contact_number;
				$site_logo 	 = $admin->site_logo;
			}

			$date =date('d/m/Y');
			$this->db->where('mail_id',18);
			$mail_template = $this->db->get('tbl_mailtemplates');
		
			if($mail_template->num_rows >0) 
			{        
			    $fetch    	 = $mail_template->row();
			    //$subject  	 = 'User Registration - Social Login'; 
			    $subject 	 = $fetch->email_subject;
			    $templete 	 = $fetch->email_template;  
			    $Content 	 = 'Thank you for registering with <a href='.base_url().'>'.$site_name.'</a>';
			   	$this->load->library('email'); 
				$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				);       
	     		$this->email->set_newline("\r\n");
				$this->email->initialize($config);        
				$this->email->from($admin_email,$site_name.'!');
				$this->email->to($user_email);
				$this->email->subject($subject);
			   	$data = array(
					'###EMAIL###'=>$first_name,
					'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
					'###SITENAME###'=>$site_name,
					'###ADMINNO###'=>$admin_no,
					'###DATE###'=>$date,
					'###CONTENT###'=>$Content
			   	);
			   
			    $content_pop=strtr($templete,$data);	
			    $this->email->message($content_pop);
			    $this->email->send();
			    return 1;   
		    }
			return 1;
		}
		*/
	}

	function login()
	{	
		$this->db->connection_check();
		$arr_detail 	= $_REQUEST['login'];
		$request    	= json_decode($arr_detail);  
		$user_email	    = $request->user_email;
		$password 		= $request->password;

		/*
		$cashback_id 	  = $this->input->post('cashback_id');
		$cashback_details = $this->input->post('cashback_details');
		$expirydate  	  = $this->input->post('expirydate');
		$cashbackweb 	  = $this->input->post('cashbackweb');
		*/
		
		$this->db->where('email',$user_email);
		$this->db->where('password',$password);
		$this->db->where('admin_status','');
	
		$query 	 = $this->db->get('tbl_users');
		$numrows = $query->num_rows();
		 
		if($numrows==1)
		{
			$fetch 		= $query->row();
			$user_id 	= $fetch->user_id;
			$user_email = $fetch->email;
			$status 	= $fetch->status;
			
			if($status =='0')
			{
				$res['success']    = 2;
  				$res['message']    = "Your Account has Currently not activated";
  				$res['user_id']    = $user_id;
  				$res['user_email'] = $user_email;
				//return 2;   //Account has deactivated
			}
			else
			{				
				/*
				//set session
				$this->session->set_userdata('user_id',$user_id);
				$this->session->set_userdata('user_email',$user_email);
				//New code for popup count session details 20-11-16
				$this->session->set_userdata('ses_popup_count',0);
				//End 20-11-16
				*/

				$res['success']    = 1;
  				$res['message']    = "Sucessfully Logged";
				$res['user_id']    = $user_id;
				$res['user_email'] = $user_email;
				//return 1; //Account has logged
			}
	    }
	    else
	    {
	    	$res['success']    = 0;
	  		$res['message']    = "Invalid Username or Password";
	  		$res['user_id']    = $user_id;
	  		$res['user_email'] = $user_email;
			//return 0; //In valid login details
	    }
		$json = json_encode($res);
		echo $json;
	}

	function forgotpassword()
	{
		$this->db->connection_check();

		$arr_detail = $_REQUEST['forgotpass'];
		$request    = json_decode($arr_detail);  
		$email	    = $request->user_email;
		//$email	= $this->input->post('email');
		
		//send email 
		$this->load->library('email');
		$this->db->where('admin_id',1);
		$admin_det = $this->db->get('admin');
		
		if($admin_det->num_rows >0) 
		{    
			$admin 	     = $admin_det->row();
			$admin_email = $admin->admin_email;
			$site_name   = $admin->site_name;
			$admin_no 	 = $admin->contact_number;
			$site_logo   = $admin->site_logo;
		}
		
		$date = date('d/m/Y');
		
		$this->db->where('email',$email);
		$this->db->where('admin_status','');
		$query = $this->db->get('tbl_users');
		if($query->num_rows >0) 
		{
			$getuser 	= $query->row();
			$user_id    = $getuser->user_id;
			$password   = $getuser->password;
			$first_name = $getuser->first_name;
			$last_name  = $getuser->last_name;
		    $user_email = $getuser->email;
			$random_id  = $getuser->random_code;

			if($first_name == '' && $last_name == '')
			{
				$ex_username = explode('@', $user_email);
				$username    = $ex_username[0]; 
			}
			else
			{
				$username = $first_name." ".$last_name;
			}

			$this->db->where('mail_id',2);
			$mail_template = $this->db->get('tbl_mailtemplates');
			if($mail_template->num_rows >0) 
			{        
			    $fetch 	  = $mail_template->row();
			    $subject  = $fetch->email_subject;  
			    $templete = $fetch->email_template;  
			  	$regurl   = base_url().'cashback/password_reset/'.insep_encode($user_id);
			    
			 	$config = Array(
				    'mailtype' => 'html',
				  	'charset'  => 'utf-8',
				);
        
     			$this->email->set_newline("\r\n");
			    $this->email->initialize($config);        
			    $this->email->from($admin_email,$site_name.'!');
			    $this->email->to($user_email);
			    $this->email->subject($subject);

			    $data = array(
					'###USERNAME###'=>$username,
					'###PASSWORD###'=>$password,
					'###SITENAME###'=>$site_name,
					'###ADMINNO###'=>$admin_no,
					'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
					'###DATE###'=>$date,
					'###LINK###'=>'<a href='.$regurl.'>'.'Click here'.'</a>'
				);
			   
			   $content_pop=strtr($templete,$data); 
			   $this->email->message($content_pop);
			   $this->email->send();	          
			}
			$res['success'] = 1;
  			$res['message'] = "Your Password details succesffully sent to your Email";
			return true;          
		}
		else
		{
			$res['success'] = 0;
  			$res['message'] = "Invalid Email Address";
			return false;                
		}   	
	}

	function check_email($email)
	{
		
		$this->db->connection_check();
		$this->db->where('email',$email);
		$this->db->where('admin_status','');
		
		$qry = $this->db->get('tbl_users');
		$numrows1 = $qry->num_rows();
		if($numrows1 == 0)
		{
			//return true;
			return 1;
		}
		else
		{
			return 0;
			//return false;
		}	
	}

	function random_string($length) 
	{
		$key = '';
		$keys = array_merge(range(0, 9), range('A', 'Z'));

		for ($i = 0; $i < $length; $i++) {
		$key .= $keys[array_rand($keys)];
		}
		return $key;
	}
}
