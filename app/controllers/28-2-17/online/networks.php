<?php
class Networks extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->load->model('network_model');
		$this->input->session_helper();
	}
	
	/*Real Time Tracking Transactions Start by seetha mam*/
	
	//Zanox tracking transactions function
	function Zanox_trackingtransactions()
	{
		$this->input->session_helper();
		$Affiliate_details = $this->network_model->Affiliate_details('Zanox');
		$aff_name =$Affiliate_details->affiliate_network;
		$connectID =$Affiliate_details->api_key;			
		$secret_key =$Affiliate_details->networkid;			
		$flag=0;
		if($connectID!='')
		{			
			$http_verb     = 'GET';
			$date          = date ("Y-m-d");
			$uri           = '/reports/sales/date/' . $date;
			$time_stamp    = gmdate('D, d M Y H:i:s T', time());
			$nonce  	   = uniqid() . uniqid();
			$string_to_sign= mb_convert_encoding($http_verb . $uri . $time_stamp . $nonce, 'UTF-8');
			$signature 	   = base64_encode(hash_hmac('sha1', $string_to_sign, $secret_key, true));
			 
			$feeds 		   = 'https://api.zanox.com/xml/2011-03-01' . $uri . '?connectid=' . $connectID . '&date=' . $time_stamp . '&nonce=' . $nonce . '&signature=' . $signature;
			 
			$contents      = simplexml_load_file($feeds);
			$json 	  	   = json_encode($contents);
			$contents 	   = json_decode($json,TRUE);	
			//echo '<pre>';print_r($contents); exit;			
		}			
		if($flag==0)
		{				
			$results = $this->network_model->Zanox_trackingtransactions($contents);
			//print_r($results);die;
			$msg =array();		
			if($results['duplicate'] == 0){						
				$msg['success'] = 'Reports details imported successfully.';
			}
			else if($results['duplicate']!=0){					
				$msg['success'] = 'Reports details imported successfully.';
			}					
		}
		else 
		{
			$msg['success'] = 'No data found.';
		}
		//echo json_encode($msg);				
	}
	//Lomadee tracking transactions function
	function Lomadee_trackingtransactions()
	{
		$this->input->session_helper();
		$Affiliate_details = $this->network_model->Affiliate_details('Lomadee');
		//print_r($Affiliate_details);
		$aff_name =$Affiliate_details->affiliate_network;
		$token_id =$Affiliate_details->api_key;			
		$publisher_id =$Affiliate_details->networkid;			
		$flag=0;
		if($token_id!='')
		{			
			//01-06-2016 17-06-2016	
			$username     = 'fabriciocmello@gmail.com';
			$password     = 'q8VXso';
			$start_date   = date ("dmY");
			$start_date_format   = date ("Y-m-d");
			$end_date     = date('dmY',strtotime($start_date_format ."+10 days"));
		/* 	$start_date     = date('dmY',strtotime('01-06-2016'));
			$end_date     = date('dmY',strtotime('17-06-2016')); */
			$url = "https://api.lomadee.com/services/report/transactions/".$publisher_id."/?startDate=".$start_date."&endDate=".$end_date."&eventStatus=0";
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_NOBODY, false);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type:text/xml'));
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
			curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
			curl_setopt($ch, CURLOPT_USERAGENT,
			"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_REFERER, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0); 
			$haveresults_set = curl_exec($ch);

			/*Curl to json converter*/
			$xml 	  = simplexml_load_string($haveresults_set);
			$json 	  = json_encode($xml);
			$contents = json_decode($json,TRUE);
			//print_r($contents);exit;
		}			
		if($flag==0)
		{				
			$results = $this->network_model->Lomadee_trackingtransactions($contents);
			//print_r($results);die;
			$msg =array();		
			if($results['duplicate'] == 0){						
				$msg['success'] = 'Reports details imported successfully.';
			}
			else if($results['duplicate']!=0){					
				$msg['success'] = 'Reports details imported successfully.';
			}					
		}
		else 
		{
			$msg['success'] = 'No data found.';
		}
		//echo json_encode($msg);				
	}
	//upload linkshare report automatically
	function linkshare_trackingtransactions()
	{
		$this->input->session_helper();
		$Affiliate_details = $this->network_model->Affiliate_details('Rakuten');
		//print_r($Affiliate_details);
		$aff_name =$Affiliate_details->affiliate_network;
		$apikey =$Affiliate_details->api_key;			
		$flag=0;
		if($apikey!='')
		{				
			include('Network/linkshare.php');		
		}			
		if($flag==0)
		{				
			$results = $this->network_model->import_trackingtransactions($newArray);
			//print_r($results);die;
			$msg =array();		
			if($results['duplicate'] == 0){						
				$msg['success'] = 'Reports details imported successfully.';
			}
			else if($results['duplicate']!=0){					
				$msg['success'] = 'Reports details imported successfully.';
			}					
		}
		else 
		{
			$msg['success'] = 'No data found.';
		}
		//echo json_encode($msg);				
	}
	// Cityads tracking transactions
	function Cityads_trackingtransactions()
	{
		$this->input->session_helper();
		$Affiliate_details = $this->network_model->Affiliate_details('Cityads');
		$apikey =$Affiliate_details->api_key;	 		
		$dateStart   = date ("Y-m-d");
		$dateEnd = date ("Y-m-d", strtotime ($dateStart ."+10 days"));  
 		/* $dateEnd = '2016-06-20';
		$dateStart = '2016-06-15'; */
		$flag=0;
		if($apikey!='')
		{		
			$url = 'http://us-geo.cityads.com/api/rest/webmaster/json/orderstatistics/'.$dateStart.'/'.$dateEnd.'?remote_auth='.$apikey;
			//echo $url;exit;
			$content = json_decode(file_get_contents($url),true);	
					
		}			
		if($flag==0)
		{
			$results = $this->network_model->importCityads_trackingtransactions($content);
			//print_r($results);die;
			$msg =array();		
			if($results['duplicate'] == 0){						
				$msg['success'] = 'Reports details imported successfully.';
			}
			else if($results['duplicate']!=0){					
				$msg['success'] = 'Reports details imported successfully.';
			}					
		}
		else 
		{
			$msg['success'] = 'No data found.';
		}
		//echo json_encode($msg);				
	}
	// Afilio tracking transactions
	function afilio_trackingtransactions()
	{
		$this->input->session_helper();
		$Affiliate_details = $this->network_model->Affiliate_details('Afilio');
		//print_r($Affiliate_details);
		$aff_name =$Affiliate_details->affiliate_network;
		$affid =$Affiliate_details->networkid;			
		$apikey =$Affiliate_details->api_key;	 		
		$dateStart   = date ("Y-m-d");
		$dateEnd = date ("Y-m-d", strtotime ($dateStart ."+10 days"));
 		/* $dateEnd = '2016-06-15';
		$dateStart = '2016-06-01'; */
		$flag=0;
		if($apikey!='')
		{				
			$url = 'https://v2.afilio.com.br/api/leadsale_api.php?mode=list&token='.$apikey.'&affid='.$affid.'&type=sale&dateStart='.$dateStart.'&dateEnd='.$dateEnd.'&format=JSON&dateType=transaction';
			/* $html = json_decode(file_get_contents($url),true);	 */
			$html = file_get_contents($url);			
			$cont = iconv('UTF-8', 'ISO-8859-1//TRANSLIT',$html);		
			$content = json_decode($cont,true);
			
		}			
		if($flag==0)
		{
			$results = $this->network_model->importafilio_trackingtransactions($content);
			//print_r($results);die;
			$msg =array();		
			if($results['duplicate'] == 0){						
				$msg['success'] = 'Reports details imported successfully.';
			}
			else if($results['duplicate']!=0){					
				$msg['success'] = 'Reports details imported successfully.';
			}					
		}
		else 
		{
			$msg['success'] = 'No data found.';
		}
		//echo json_encode($msg);				
	}

	/*Real Time Tracking Transactions End*/

	
	/*New code for Upload Coupons details  7-6-16.*/
	function upload_apicoupons()
	{	
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');

		if($admin_id=="")
		{
		  redirect('adminsettings/index','refresh');
		} 
		else
		{	
			if($this->input->post('uploadtype'))
			{
				$upload_type    = $this->input->post('uploadtype');
				$affiliate_name = $this->input->post('aff_type');
				
				if($upload_type == 'Coupons_upload')
				{
					if($affiliate_name!='')
					{
						
						if($affiliate_name == 'zanox')
						{

							$connection_id  = '6D2504B4C68860782BBA';
							$secret_key     = '7bf98038714c45+4b20e6aec684570/1CBef494c';
							$url 		    = 'http://api.zanox.com/json/2011-03-01/incentives/?connectid='.$connection_id.'&region=BR&adspace=2149716&incentiveType=coupons';
							$content        = json_decode(file_get_contents($url),true);
							$content_status = $content['items'];
							//echo "Zanox <pre>"; print_r($content); exit;
						}

						if($affiliate_name == 'rakuten')
						{	
							
												
							$token_id  		= '3d0ad0eda6d7a35fc31a685efbf9d19c82475bbb20929b5c060886cbae822535';
							$url   	   		= 'http://couponfeed.linksynergy.com/coupon?token='.$token_id;
							$content_url	= file_get_contents($url);
							$xml_content    = simplexml_load_string($content_url);
							$json_content   = json_encode($xml_content);
							$content        = json_decode($json_content,TRUE);
							$content_status = $content['TotalMatches'];
							//echo "<pre>"; print_r($content); exit;
							//echo $content_status; exit;
						}
						
						if($affiliate_name == 'cityads')
						{	
							
							$token_id    	= '3d7b3dd997d6e957b8924240eb6097cb7a02ed549c9c5d0523f500ea33f366e0';
							$client_id   	= '649885';
							$secret_key  	= '051cb84b51716df8bad6ad90c1083d8e'; 
							$remote_auth 	= '5049f98b30b43c185c8917fb719576dc';
							$url         	= 'http://api.cityads.com/api/rest/webmaster/xml/coupons?remote_auth='.$remote_auth;
							
							//echo $url; exit; 
							$content_url	= file_get_contents($url);
							$xml_content    = simplexml_load_string($content_url);
							//echo "<pre>"; print_r($xml_content); exit;
							$json_content   = json_encode($xml_content);
							$content        = json_decode($json_content,TRUE);
							//$content 	    = json_decode(file_get_contents($url),true);
							$content_status = $content['status'];
							//echo '<pre>';print_r($content);exit;
						} 
						
						if($affiliate_name == 'afilio')
						{	
			
							$username      = 'pingou';
							$password      = 'yhasdf@fm16';
							$token_id      = '57581974981921.99713763';
							$site_id       = '47777';
							$aff_id        = '42747';
							$url 		   = 'http://v2.afilio.com.br/api/feedproducts.php?token='.$token_id.'&mode=dl&siteid='.$site_id.'&affid='.$aff_id;
							
							/*New code for csv file to convert a response details start 18-6-16.*/
							// Arrays we'll use later
							$keys = array();
							$newArray = array();
							// Function to convert CSV into associative array
							function csvToArray($file, $delimiter) 
							{ 
							  	if (($handle = fopen($file, 'r')) !== FALSE)
							  	{ 
							    	$i = 0; 
							    	while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) 
							    	{ 
							      		for ($j = 0; $j < count($lineArray); $j++) 
							      		{ 
							        		$arr[$i][$j] = $lineArray[$j]; 
							      		} 
							      		$i++; 
							    	} 
							    	fclose($handle); 
								} 
							  	return $arr; 
							} 
							
							// Do it
							$data = csvToArray($url, ';');
							//echo "<pre>";print_r($data); exit;
							// Set number of elements (minus 1 because we shift off the first row)
							$count = count($data) - 1; 
							//Use first row for names  
							// $labels = array_shift($data);  
							//print_r($data[0]); exit;
							foreach ($data[0] as $label) 
							{ 
							  $keys[] = $label; 
							}  
							 
							// Add Ids, just in case we want them later
							$keys[] = 'id';
							
							for ($i = 1; $i < $count; $i++)
							{
							  $data[$i][] = $i;
							}
							// Bring it all together
							for ($j = 1; $j < $count; $j++) 
							{
							  $d = array_combine($keys,$data[$j]);
							  $newArray[$j] = $d;
							}

							$content = $newArray;
							 
							//echo "<pre>"; print_r($content); exit;
							$content_status= 1;
							/*End 18-6-16*/
						}
						
						//echo $content_status; exit; 
						if($content_status!=0)
						{				
							if($flag==0)
							{
								$getdatas = $this->network_model->import_apicoupons($content,$affiliate_name);
								
								if($getdatas['duplicate'] == 0)
								{
									$this->session->set_flashdata('success', 'Coupons details imported successfully.');
									redirect('networks/upload_apicoupons','refresh');
								}
								else if($getdatas['duplicate']!=0)
								{		
									$this->session->set_flashdata('error', 'New Coupons details added successfully and <span style="color:red">'.$getdatas['duplicate'].'</span> duplicate records neglected. The duplicate transactions ids are '.$getdatas['trans_id']);
									redirect('networks/upload_apicoupons','refresh');
								}					
							}
						}
						else 
						{
							$this->session->set_flashdata('error','No data found.');
							redirect('networks/upload_apicoupons','refresh');
						}	
					}	
				}
			}		
				$data['action'] = "coupons";
				$this->load->view('adminsettings/upload_apicoupons',$data);
		}	
	}
	/*End*/

	/*New code for upload report details 7-6-16*/
	function upload_apireport()
	{	

		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
		  redirect('adminsettings/index','refresh');
		} else
		{	
			
			if($this->input->post('uploadtype'))
			{
				//print_r($_POST); exit;
				$upload_type    = $this->input->post('uploadtype');
				$affiliate_name = $this->input->post('aff_type');
				
				if($upload_type == 'Report_upload')
				{
					if($affiliate_name!='')
					{
						//report response details Pending//
						if($affiliate_name == 'zanox')
						{
							
							$connectID     = '6BA59464117A45ADAB2D';
							$secret_key    = '7bf98038714c45+4b20e6aec684570/1CBef494c';  
							$http_verb     = 'GET';
							$date          = date('Y-m-d',strtotime($this->input->post('reportdate')));
							$uri           = '/reports/sales/date/' . $date;
							$time_stamp    = gmdate('D, d M Y H:i:s T', time());
							$nonce  	   = uniqid() . uniqid();
							$string_to_sign= mb_convert_encoding($http_verb . $uri . $time_stamp . $nonce, 'UTF-8');
							$signature 	   = base64_encode(hash_hmac('sha1', $string_to_sign, $secret_key, true));
							 
							$feeds 		   = 'https://api.zanox.com/xml/2011-03-01' . $uri . '?connectid=' . $connectID . '&date=' . $time_stamp . '&nonce=' . $nonce . '&signature=' . $signature; //.'datetype=modified_date&adspace=2149716'
							//echo $feeds; exit;
							$contents      = simplexml_load_file($feeds);
							//echo '<pre>';print_r($contents); exit;
							$json 	  	   = json_encode($contents);
							$contents 	   = json_decode($json,TRUE);	
							//echo '<pre>';print_r($contents);exit;
							$status 	   = $contents['total'];
						}

						if($affiliate_name == 'lomadee')
						{
							$username     = 'fabriciocmello@gmail.com';
							$password     = 'q8VXso';
							$start_date   = date('dmY',strtotime($this->input->post('startdate')));
							$end_date     = date('dmY',strtotime($this->input->post('enddate')));
							$publisher_id = '22679375';
							$token_id     = 'e2ZhYnJpY2lvY21lbGxvQGdtYWlsLmNvbX06e3E4Vlhzb30=';
							//$url 		  = "https://bws.buscape.com.br/api/lomadee/reportTransaction/e2ZhYnJpY2lvY21lbGxvQGdtYWlsLmNvbX06e3E4Vlhzb30=?startDate=15062016&endDate=15062016&eventStatus=0&publisherId=22679375";
							$url		  = "https://api.lomadee.com/services/report/transactions/".$publisher_id."/?startDate=".$start_date."&endDate=".$end_date."&eventStatus=0&currency=MBL";
							//echo $url; exit;
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_HEADER, false);
							curl_setopt($ch, CURLOPT_NOBODY, false);
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
							curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type:text/xml'));
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
							curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
							curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
							curl_setopt($ch, CURLOPT_USERAGENT,
							"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_REFERER, $url);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
							curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0); 
							$haveresults_set = curl_exec($ch);
							//echo "<pre>"; print_r($haveresults_set); exit;
							//Curl to json converter//
							$xml 	  = simplexml_load_string($haveresults_set);
							$json 	  = json_encode($xml);
							$contents = json_decode($json,TRUE);
							//End//
							//echo "<pre>"; print_r($contents); exit;
							//echo count($contents, COUNT_RECURSIVE);
							//echo count($contents['item']);
							//exit;
							$status  = $contents['details']['elapsedTime'];
						}

						//Pon prakesh//
						if($affiliate_name == 'rakuten')
						{	
							$username   = 'facmello';
							$password   = 'yhasdf@Fm80';
							//$token_id   = '3d7b3dd997d6e957b8924240eb6097cb7a02ed549c9c5d0523f500ea33f366e0';
							$report_id  = '3326233';
							$start_date = $this->input->post('startdate');
							$end_date   = $this->input->post('enddate');
							
							header('Content-type: application/json');
							// Set your CSV feed
							$feed = 'https://ran-reporting.rakutenmarketing.com/en/reports/sales-and-activity-reports1/filters?date_range=this-month&include_summary=Y&network=8&tz=GMT&date_type=transaction&token=ZW5jcnlwdGVkYToyOntzOjU6IlRva2VuIjtzOjY0OiI2NzFiZjE1NTViYTk4ZDJhMGNkNjY5ODY5NTZhZWI4N2I2MTgyY2IxM2UwM2Y4MDBjNWU3MWM0NjljNzNhMzk4IjtzOjg6IlVzZXJUeXBlIjtzOjk6IlB1Ymxpc2hlciI7fQ%3D%3D';

							// Arrays we'll use later
							$keys = array();
							$newArray = array();
							// Function to convert CSV into associative array
							function csvToArray($file, $delimiter) 
							{ 
							  	if (($handle = fopen($file, 'r')) !== FALSE)
							  	{ 
							    	$i = 0; 
							    	while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) 
							    	{ 
							      		for ($j = 0; $j < count($lineArray); $j++) 
							      		{ 
							        		$arr[$i][$j] = $lineArray[$j]; 
							      		} 
							      		$i++; 
							    	} 
							    	fclose($handle); 
								} 
							  	return $arr; 
							} 
							
							// Do it
							$data = csvToArray($feed, ',');
							// Set number of elements (minus 1 because we shift off the first row)
							$count = count($data) - 1; 
							//Use first row for names  
							// $labels = array_shift($data);  
							foreach ($data[4] as $label) 
							{
							  $keys[] = $label;
							}
							// Add Ids, just in case we want them later
							$keys[] = 'id';
							for ($i = 5; $i < $count; $i++)
							{
							  $data[$i][] = $i;
							}
							// Bring it all together
							for ($j = 5; $j < $count; $j++) 
							{
							  $d = array_combine($keys, $data[$j]);
							  $newArray[$j] = $d;
							}

							$contents = $newArray;
							$status   = 1;
							//echo "<pre>"; print_r($contents); exit;
						}
 						//Pon prakesh//
						if($affiliate_name == 'cityads')
						{	
							
							$username    = 'facmello';
							$password    = 'yhasdf@Fm80';
							$token_id    = '3d7b3dd997d6e957b8924240eb6097cb7a02ed549c9c5d0523f500ea33f366e0';
							$client_id   = '649885';
							$secret_key  = '5049f98b30b43c185c8917fb719576dc'; 
							$Remote_auth = 'ae757f7e4f1be3bcb64d67eeab39ed86';
							$start_date  = date('Y-m-d',strtotime($this->input->post('startdate')));
							$end_date    = date('Y-m-d',strtotime($this->input->post('enddate')));
							$url         = 'http://us-geo.cityads.com/api/rest/webmaster/json/orderstatistics/'.$start_date.'/'.$end_date.'?remote_auth='.$secret_key;
							//echo $url; exit; 
							//$url       = 'http://api.cityads.com/api/rest/webmaster/json/statistics-offers/action_id/'.$start_date.'/'.$end_date.'?remote_auth='.$secret_key;
							$contents  	 = json_decode(file_get_contents($url),true);
								//echo "<pre>"; print_r($contents); exit;
							$status 	 = $contents['status'];
						} 
						
						//report response details Completed//
						if($affiliate_name == 'afilio')
						{
							
							$username      = 'pingou';
							$password      = 'yhasdf@fm16';
							$token_id      = '57581974981921.99713763';
							$site_id       = '47777';
							$aff_id        = '42747';
							$start_date    = date('Y-m-d',strtotime($this->input->post('startdate')));
							$end_date      = date('Y-m-d',strtotime($this->input->post('enddate')));
							//$url 		   = 'https://v2.afilio.com.br/api/leadsale_api.php?mode=list&token='.$token_id.'&affid='.$aff_id.'&type=sale&dateStart='.$start_date.'&dateEnd='.$end_date.'&format=XML';
							$url 		   = 'https://v2.afilio.com.br/api/leadsale_api.php?mode=list&token='.$token_id.'&affid='.$aff_id.'&type=sale&dateStart='.$start_date.'&dateEnd='.$end_date.'&format=XML&dateType=transaction';
				  			
				  			$contents 	   = file_get_contents($url);
							$xml 	  	   = simplexml_load_string($contents, "SimpleXMLElement", LIBXML_NOCDATA);
							$json 	  	   = json_encode($xml);
							$contents 	   = json_decode($json,TRUE);	
							//echo "<pre>"; print_r($contents);exit;
							$status 	   = 1;
						}

						if($status != '')
						{				
							if($flag==0)
							{
								$getdatas = $this->network_model->import_apireports($contents,$affiliate_name);
								 
								if($getdatas == 1)
								{
									$this->session->set_flashdata('success', 'Report details imported successfully.');
									redirect('networks/upload_apireport','refresh');
								}
								else if($getdatas == 0)
								{	

									$this->session->set_flashdata('error', 'Report Details Already Added.');
									redirect('networks/upload_apireport','refresh');
								}
								else
								{
									$this->session->set_flashdata('error', 'Report Details Not Found.');
									redirect('networks/upload_apireport','refresh');
								}					
							}
						}
						else 
						{
							$this->session->set_flashdata('error','No data found.');
							redirect('networks/upload_apireport','refresh');
						}		
					}	
				}
			}		
				$data['action'] = "report";
				$this->load->view('adminsettings/upload_apireport',$data);
		}	
	}
	/*END*/

	/*new code for Update a Zanox API report details 24-2-17*/
	function update_api($api_name)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if(($admin_id==""))
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			if($this->input->post('update_apireports'))
			{	
				if($api_name == 'zanox')
				{
					//print_r($_POST); exit;
					//$report_date = date('Y-m-d',strtotime($this->input->post('reportdate')));
					$ad_space      = $this->input->post('adspace');
					$zanox_status  = $this->input->post('zanox_status');
					$date_type     = $this->input->post('date_type');
					$offer_id      = $this->input->post('offer_name');
					
					$connectID     = '6BA59464117A45ADAB2D';
					$secret_key    = '7bf98038714c45+4b20e6aec684570/1CBef494c';  
					$http_verb     = 'GET';
					$date          = date('Y-m-d',strtotime($this->input->post('reportdate')));
					$uri           = '/reports/sales/date/' . $date;
					$time_stamp    = gmdate('D, d M Y H:i:s T', time());
					$nonce  	   = uniqid() . uniqid();
					$string_to_sign= mb_convert_encoding($http_verb . $uri . $time_stamp . $nonce, 'UTF-8');
					$signature 	   = base64_encode(hash_hmac('sha1', $string_to_sign, $secret_key, true));

					
					if($offer_id != 'all')
					{
						$val .= '&program='.$offer_id.'';
					}
					 
					if($zanox_status != 'all')
					{
						$val .= '&state='.$zanox_status.'';
					}
					
					$feeds   = 'https://api.zanox.com/xml/2011-03-01'.$uri.'?datetype='.$date_type.'&adspace='.$ad_space.$val.'&connectid='.$connectID.'&date='.$time_stamp.'&nonce='.$nonce.'&signature='.$signature;
					//echo $feeds; 
					$contents      = simplexml_load_file($feeds);
					//echo '<pre>';print_r($contents); exit;
					$json 	  	   = json_encode($contents);
					$contents 	   = json_decode($json,TRUE);	
					//echo '<pre>';print_r($contents);exit;
					$status 	   = $contents['total'];
				}

				if($affiliate_name == 'lomadee')
				{
					
					$ad_space     = $this->input->post('adspace');
					$zanox_status = $this->input->post('zanox_status');
					$date_type    = $this->input->post('date_type');
					$offer_id     = $this->input->post('offer_name');

					$username     = 'fabriciocmello@gmail.com';
					$password     = 'q8VXso';
					$start_date   = date('dmY',strtotime($this->input->post('startdate')));
					$end_date     = date('dmY',strtotime($this->input->post('enddate')));
					$publisher_id = '22679375';
					$token_id     = 'e2ZhYnJpY2lvY21lbGxvQGdtYWlsLmNvbX06e3E4Vlhzb30=';
					
					if($offer_id != 'all')
					{
						$val .= '&program='.$offer_id.'';
					}
					 
					if($zanox_status != 'all')
					{
						$val .= '&state='.$zanox_status.'';
					}

					$url		  = "https://api.lomadee.com/services/report/transactions/".$publisher_id."/?startDate=".$start_date."&endDate=".$end_date."&eventStatus=0&currency=MBL".$val;
					echo $url; exit;
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_HEADER, false);
					curl_setopt($ch, CURLOPT_NOBODY, false);
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
					curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type:text/xml'));
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
					curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
					curl_setopt($ch, CURLOPT_USERAGENT,
					"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_REFERER, $url);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0); 
					$haveresults_set = curl_exec($ch);
					//echo "<pre>"; print_r($haveresults_set); exit;
					//Curl to json converter//
					$xml 	  = simplexml_load_string($haveresults_set);
					$json 	  = json_encode($xml);
					$contents = json_decode($json,TRUE);
					//End//
					echo "<pre>"; print_r($contents); exit;
					//echo count($contents, COUNT_RECURSIVE);
					//echo count($contents['item']);
					//exit;
					$status  = $contents['details']['elapsedTime'];
				}

				if($status != '')
				{				
					if($flag==0)
					{
						$getdatas = $this->network_model->update_apireports($contents,$api_name);
						 
						if($getdatas == 1)
						{
							$this->session->set_flashdata('success', 'Report details Updated successfully.');
							redirect('networks/update_api/'.$api_name,'refresh');
						}
						else if($getdatas == 0)
						{	
							$this->session->set_flashdata('error', 'Report Details Not Found.');
							redirect('networks/update_api/'.$api_name,'refresh');
						}
						else
						{
							$this->session->set_flashdata('error', 'Report Details Not Found.');
							redirect('networks/update_api/'.$api_name,'refresh');
						}					
					}
				}
				else 
				{
					$this->session->set_flashdata('error','No data found.');
					redirect('networks/update_api/'.$api_name,'refresh');
				}
			} 
			$this->load->view('adminsettings/update_api',$data);	
			
		}
	}



}
?>