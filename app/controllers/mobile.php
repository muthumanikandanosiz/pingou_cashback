<?php
class Mobile extends CI_Controller
{
 	public function __construct()
 	{	
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('front_model');
		session_start();
		
		  
		/*New code for not log user ip address added into database 18-11-16*/
		$user_id = $this->session->userdata('user_id');
		if($user_id == '')
		{
			$user_ip_details = $this->front_model->set_ip_address();
		}
		/*End 18-11-16*/
		if($this->uri->segment(1) == 'cms')
		{
			//redirect('404','refresh');
			redirect('','refresh');
		}
	}

	//Registration Process
	function register()
	{
		//$this->input->session_helper();
		$arr_detail 	= $_REQUEST['register'];
		$request    	= json_decode($arr_detail);  
		$user_email	    = $request->user_email;
		$password 		= $request->password;
		$new_random 	= mt_rand(1000000,99999999);
		$date 	  	    = date('Y-m-d h:i:s');
		$uni_id   	    = $this->input->post('uni_id');
		$unic_code 	    = $this->random_string(10);

		$arrcat_type   = array('3454'=>2,'8765'=>3,'2345'=>4,'1647'=>5,'6536'=>6,'7486'=>7,'8362'=>8,'9326'=>9,'2695'=>10);
	    if (array_key_exists($cat_type_url,$arrcat_type))
		{
	    	$categorytype = $arrcat_type[$cat_type_url];
	    }
	    else
	    {	
	    	$categorytype = 1;
	    }

	    if($pagename)
		{
			$new_uid = $uni_id;
		}
		else
		{
			$new_uid = $ref_id;
		}
		if($new_uid)
		{	
			$this->db->where('random_code',$new_uid);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows() > 0)
			{	
				$fetch = $query->row();
				$user_id = $fetch->user_id;
				$ref_cat_type = $fetch->referral_category_type;

			}
			else
			{
				$user_id = 0;
			}
		}
		else
		{
			$user_id = 0;
			if($ref_cat_type == '')
			{
				$ref_cat_type = 0;
			}
		}

		$userdetailss = $this->db->query("select * from tbl_users where email='$user_email'");
		
		if($userdetailss->num_rows > 0)
		{
			$res['success'] = 0;
  			$res['message'] = 'Already EmailId has been registered';
		}
		else
		{	
			$data = array(		
			'email'=>$user_email,
			'password'=>$password,
			'random_code'=>$new_random,
			'refer'=>$user_id,
			'cashback_mail'=>1,
			'withdraw_mail'=>1,
			'referral_mail'=>1,
			'acbalance_mail'=>1,
			'support_tickets'=>1,
			'newsletter_mail'=>1,
			'referral_category_type'=>$categorytype,
			'date_added'=>$date,
			'unic_bonus_code'=>$unic_code,
			'ref_user_cat_type'=>$ref_cat_type,
			'status'=>1,
			'app_login'=>1
			);
			
			$this->db->insert('tbl_users',$data,$datas);
			$insert_id = $this->db->insert_id();

			if($insert_id)
			{
				$res['success']  = 1;
	  			$res['message']  = "User Details has been Inserted successfully";
  				$res['user_id']  = $insert_id;	
			}
			else
			{
				$res['success'] = 2;
	  			$res['message'] = "User Details has been Not Inserted";
  				//$res['user_id'] = $insert_id;	
			}
			
		}
		$json = json_encode($res);
		echo $json;
		
		/*
		//New code for (type 2 format) user referral cashback amount for referred user (Pending status) 8-9-16//
		if($user_id !=0)
		{
			$user_deta = $this->front_model->view_user($user_id);
			if($user_deta)
			{
				//New code for refferal category type//
				$categorytype = $user_deta[0]->referral_category_type;
				
				
				$name   = $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();
				$status = $name->ref_by_rate;
				if($status == 1)
				{	
					$caspe = $name->ref_cashback_rate;
				}
				//End//
				
				if($status == 1)
				{
					$this->db->select_max('trans_id');
					$result   = $this->db->get('transation_details')->row();  
					$trans_id = $result->trans_id;
					$trans_id = $trans_id+1;
					$n9  	  = '5236555';
					$n12 	  = $n9 + $trans_id; 
					$now 	  = date('Y-m-d');
					$newid 	  = rand(1000,9999);
					$newtransaction_id = md5($newid);


					$data = array(		
					'transation_amount' => $caspe,
					'user_id' => $user_id,
					'transation_date' => $now,
					'transaction_date' => $now,
					'transation_id'=>$n12,
					'transation_reason' => "Pending Referal Payment",
					'mode' => 'Credited',
					'details_id'=>'',
					'table'=>'',
					'report_update_id'=>$newtransaction_id,
					'ref_user_tracking_id' =>$insert_id,
					'transation_status ' => 'Pending');
					$this->db->insert('transation_details',$data);
				}	
			}
		}
		//End refferal details 8-9-16//

		//New code for (type 3 format) user referral cashback amount count of referred user(Pending status) 9-9-16//
		$referrals 			= $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();	
		$ref_by_percentage  = $referrals->ref_by_percentage;
		$ref_by_rate 		= $referrals->ref_by_rate;
		$bonus_by_ref_rate  = $referrals->bonus_by_ref_rate;
		$category_names     = ucfirst($referrals->category_type);
		
		//3 Bonus by Refferal Rate type//
		if($bonus_by_ref_rate == 1)
		{
			
			$newid 	= rand(1000,9999);
			$newtransaction_id = md5($newid);

			$n9      = '333445';
			$n12     = $n9 + $user_id;
			$now     = date('Y-m-d H:i:s');	
			$selqry  = "SELECT COUNT(user_id) as userid FROM transation_details where transation_reason ='Pending Referal Payment' AND user_id=$user_id"; 
			$query 	 =  $this->db->query("$selqry");
			$numrows = $query->num_rows();

			if($numrows > 0)
			{
				$fetch 			= $query->row();
				$usercount 		= $fetch->userid;
				$referrals      = $this->db->query("select * from referral_settings where ref_id='$categorytype'")->row();	
				$bonus_amount   = $referrals->ref_cashback_rate_bonus;
				$friends_count  = $referrals->friends_count;

				if($usercount == $friends_count)
				{	
					if($bonus_amount!='')
					{	 
						$referral_amt = $user_deta[0]->referral_amt;
						if($referral_amt == 0)
						{
							$data = array(			
							'transation_amount' => $bonus_amount,	
							'user_id' => $user_id,	
							'transation_date' => $now,
							'transaction_date' => $now,
							'transation_id'=>$n12,	
							'transation_reason' => 'Referral Bonus for '. ucfirst($category_names) .' User',	
							'mode' => 'Credited',
							'details_id'=>'',	
							'table'=>'',	
							'new_txn_id'=>0,
							'transation_status ' => 'Pending',
							'report_update_id'=>$newtransaction_id,
							'ref_bonus_type'=>$categorytype
							);	
							$this->db->insert('transation_details',$data);
						
							//User table update referral status//
							$data = array(		
							'referral_amt' => 1);
							$this->db->where('user_id',$user_id);
							$update_qry = $this->db->update('tbl_users',$data);	
							//End//
						}		
					}	
				}
			} 
		}
		//End referral details 9-9-16//

		//New code for activation method 18-4-16//
		$admindetailssss = $this->front_model->getadmindetails_main();
		$activate  		 = $admindetailssss->activate_method;
		//End//

		//New code for Add a subscriber users list 31-8-16//
		$this->db->where('subscriber_email',$user_email);
		$query = $this->db->get('subscribers');
		if($query->num_rows() == 0)
		{
			$date = date('Y-m-d h:m:s');
			$data = array(
			'subscriber_email' => $user_email,
			'subscriber_status' => '1',
			'date_subscribed' => $date
			);
			$this->db->insert('subscribers',$data);
		}
		//End 31-8-16//
		
		if($new_uid)
		{
			$this->db->where('random_code',$new_uid);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows() > 0)
			{	
				$fetch = $query->row();
				$user_id = $fetch->user_id;
				$email =$fetch->email;
				
				$datas = array(
				'user_id' => $user_id,
				'user_email' => $email,
				'referral_email' => $user_email,
				'status' => 'Ativa',
				'date_added' => $date
				);
				$this->db->insert('referrals',$datas);
			}
		}
		
		//New code for referral for cashback_exclusive via registered user 30-7-16//
		$user_id 		  = $this->session->userdata('user_id'); 
		$cashback_details = $this->session->userdata('link_name');	
		$cashbackdetails  = $this->front_model->cashback_exclusive_details($cashback_details);
		$Email_id         = $cashbackdetails->user_email;
		$userdetailss     = $this->db->query("select * from tbl_users where email='$Email_id'")->row();
		$newuserid        = $userdetailss->user_id;
		if($user_id=='')
		{
			if($cashback_details !='')
			{
				$datas = array(
				'user_id' => $newuserid,
				'user_email' => $Email_id,
				'referral_email' => $user_email,
				'status' => 'Ativa',
				'date_added' => $date
				);
				$this->db->insert('referrals',$datas);
			}
		}
		//End 30-7-16//
		
		//New code for mail notification//
		if($activate == 0)
		{
		
			$this->db->where('admin_id',1);
			$admin_det = $this->db->get('admin');
			if($admin_det->num_rows >0) 
			{    
				$admin 		 = $admin_det->row();
				$admin_email = $admin->admin_email;
				$site_name 	 = $admin->site_name;
				$admin_no 	 = $admin->contact_number;
				$site_logo 	 = $admin->site_logo;
			}
			
			$date =date('d/m/Y');
			$first_name = $this->input->post('first_name');
			$last_name  = $this->input->post('last_name');
			$user_email = $this->input->post('user_email');
		
			$this->db->where('mail_id',1);
			$mail_template = $this->db->get('tbl_mailtemplates');
			
			if($mail_template->num_rows >0) 
			{        
			    $fetch    	 = $mail_template->row();
			    $subject  	 = $fetch->email_subject;  
			    $templete 	 = $fetch->email_template;  
			    $regurl	 	 = base_url().'verify-account/'.$new_random;
			    $unsuburl	 = base_url().'un-subscribe-signup/'.$new_random;

			   	$this->load->library('email'); 

				$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				);
				// $this->email->initialize($config);        
	     		$this->email->set_newline("\r\n");
				$this->email->initialize($config);        
				$this->email->from($admin_email,$site_name.'!');
				$this->email->to($user_email);
				$this->email->subject($subject);
			   	$data = array(
					'###USERNAME###'=>$first_name,
					'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
					'###SITENAME###'=>$site_name,
					'###ADMINNO###'=>$admin_no,
					'###DATE###'=>$date,
					'###LINK###'=>'<a href='.$regurl.'>'.$regurl.'</a>',
					'###ULINK###'=>'<a href='.$unsuburl.'>'.$unsuburl.'</a>'
			   	);
			   
			    $content_pop=strtr($templete,$data);	
			    $this->email->message($content_pop);
			    $this->email->send();           
			}
			return 0;
		}
		if($activate == 1)
		{
			$datas = array('status'=>1);
			$this->db->where('user_id',$insert_id);
			$this->db->where('admin_status','');
			$update_qry = $this->db->update('tbl_users',$datas);

			$this->session->set_userdata('user_id',$insert_id);
			$this->session->set_userdata('user_email',$user_email);

			$this->db->where('admin_id',1);
			$admin_det = $this->db->get('admin');
			if($admin_det->num_rows >0) 
			{    
				$admin 		 = $admin_det->row();
				$admin_email = $admin->admin_email;
				$site_name 	 = $admin->site_name;
				$admin_no 	 = $admin->contact_number;
				$site_logo 	 = $admin->site_logo;
			}

			$date =date('d/m/Y');
			$this->db->where('mail_id',18);
			$mail_template = $this->db->get('tbl_mailtemplates');
		
			if($mail_template->num_rows >0) 
			{        
			    $fetch    	 = $mail_template->row();
			    //$subject  	 = 'User Registration - Social Login'; 
			    $subject 	 = $fetch->email_subject;
			    $templete 	 = $fetch->email_template;  
			    $Content 	 = 'Thank you for registering with <a href='.base_url().'>'.$site_name.'</a>';
			   	$this->load->library('email'); 
				$config = Array(
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				);       
	     		$this->email->set_newline("\r\n");
				$this->email->initialize($config);        
				$this->email->from($admin_email,$site_name.'!');
				$this->email->to($user_email);
				$this->email->subject($subject);
			   	$data = array(
					'###EMAIL###'=>$first_name,
					'###COMPANYLOGO###' =>base_url()."/uploads/adminpro/".$site_logo,
					'###SITENAME###'=>$site_name,
					'###ADMINNO###'=>$admin_no,
					'###DATE###'=>$date,
					'###CONTENT###'=>$Content
			   	);
			   
			    $content_pop=strtr($templete,$data);	
			    $this->email->message($content_pop);
			    $this->email->send();
			    return 1;   
		    }
			return 1;
		}
		*/
	}

	//Login Process
	function login()
	{	
		$this->db->connection_check();
		$arr_detail 	= $_REQUEST['login'];
		$request    	= json_decode($arr_detail);  
		$user_email	    = $request->user_email;
		$password 		= $request->password;

		/*
		$cashback_id 	  = $this->input->post('cashback_id');
		$cashback_details = $this->input->post('cashback_details');
		$expirydate  	  = $this->input->post('expirydate');
		$cashbackweb 	  = $this->input->post('cashbackweb');
		*/
		
		$this->db->where('email',$user_email);
		$this->db->where('password',$password);
		$this->db->where('admin_status','');
	
		$query 	 = $this->db->get('tbl_users');
		$numrows = $query->num_rows();
		 
		if($numrows==1)
		{
			$fetch 		= $query->row();
			$user_id 	= $fetch->user_id;
			$user_email = $fetch->email;
			$status 	= $fetch->status;
			
			if($status =='0')
			{
				$res['success']    = 2;
  				$res['message']    = "Your Account has Currently not activated";
  				$res['user_id']    = $user_id;
  				$res['user_email'] = $user_email;
				//return 2;   //Account has deactivated
			}
			else
			{				
				/*
				//set session
				$this->session->set_userdata('user_id',$user_id);
				$this->session->set_userdata('user_email',$user_email);
				//New code for popup count session details 20-11-16
				$this->session->set_userdata('ses_popup_count',0);
				//End 20-11-16
				*/

				$res['success']    = 1;
  				$res['message']    = "Sucessfully Logged";
				$res['user_id']    = $user_id;
				$res['user_email'] = $user_email;
				//return 1; //Account has logged
			}
	    }
	    else
	    {
	    	$res['success']    = 0;
	  		$res['message']    = "Invalid Username or Password";
	  		$res['user_id']    = $user_id;
	  		$res['user_email'] = $user_email;
			//return 0; //In valid login details
	    }
		$json = json_encode($res);
		echo $json;
	}

	//Social Login Process
	function social_login()
	{
		$this->input->session_helper();

		$arr_detail 	= $_REQUEST['social'];
		$request    	= json_decode($arr_detail);  
		$user_email	    = $request->user_email;
		//$password 		= $request->password;
		$account_type   = $request->acc_type;   
		$account_id     = $request->acc_id;
		$profile_photo  = $request->acc_photo;
		$new_random 	= mt_rand(1000000,99999999);
		$date 	  	    = date('Y-m-d h:i:s');
		$uni_id   	    = $this->input->post('uni_id');
		$unic_code 	    = $this->random_string(10);

		$arrcat_type   = array('3454'=>2,'8765'=>3,'2345'=>4,'1647'=>5,'6536'=>6,'7486'=>7,'8362'=>8,'9326'=>9,'2695'=>10);
	    if (array_key_exists($cat_type_url,$arrcat_type))
		{
	    	$categorytype = $arrcat_type[$cat_type_url];
	    }
	    else
	    {	
	    	$categorytype = 1;
	    }

	    if($pagename)
		{
			$new_uid = $uni_id;
		}
		else
		{
			$new_uid = $ref_id;
		}
		if($new_uid)
		{	
			$this->db->where('random_code',$new_uid);
			$this->db->where('admin_status','');
			$query = $this->db->get('tbl_users');
			if($query->num_rows() > 0)
			{	
				$fetch = $query->row();
				$user_id = $fetch->user_id;
				$ref_cat_type = $fetch->referral_category_type;

			}
			else
			{
				$user_id = 0;
			}
		}
		else
		{
			$user_id = 0;
			if($ref_cat_type == '')
			{
				$ref_cat_type = 0;
			}
		}

		$userdetailss = "SELECT * from tbl_users where email='$user_email'";
		
		$query=$this->db->query("$userdetailss");
		$numrows = $query->num_rows();
		if($numrows==1)
		{
			$fetch      = $query->row();
			$user_id    = $fetch->user_id;
			$user_email = $fetch->email;
			$acc_id     = $fetch->acc_id;

			$data = array('profile' =>$profile_photo,'acc_type' =>$account_type,'acc_id'=>$account_id);
			$this->db->where('user_id',$user_id);
			$update_qry = $this->db->update('tbl_users',$data);	

			$res['success']    = 2;
  			$res['message']    = "Sucessfully Logged";
  			$res['user_id']    = $user_id;
  			$res['account_id'] = $acc_id;

		}
		else
		{	
			$data = array(		
			'email'=>$user_email,
			//'password'=>$password,
			'random_code'=>$new_random,
			'refer'=>$user_id,
			'cashback_mail'=>1,
			'withdraw_mail'=>1,
			'referral_mail'=>1,
			'acbalance_mail'=>1,
			'support_tickets'=>1,
			'newsletter_mail'=>1,
			'referral_category_type'=>$categorytype,
			'date_added'=>$date,
			'unic_bonus_code'=>$unic_code,
			'ref_user_cat_type'=>$ref_cat_type,
			'status'=>1,
			//New field added for social login details 19-1-17 
			'profile'=>$profile_photo,
			'acc_type'=>$account_type,
			'acc_id'=>$account_id
			//End 19-1-17
			);
			
			$this->db->insert('tbl_users',$data,$datas);
			$insert_id = $this->db->insert_id();

			$acc_id = $this->db->query("SELECT * from tbl_users where user_id=$insert_id")->row('acc_id');
			//echo $acc_id; exit;
			$res['success']    = 1;
  			$res['message']    = "Sucessfully Logged";
  			$res['user_id']    = $insert_id;
  			$res['account_id'] = $acc_id;
		}
		$json = json_encode($res);
		echo $json;
	}

	//Forgot password Process
	function forgotpassword()
	{
		$this->db->connection_check();
		$arr_detail = $_REQUEST['forgotpass'];
		$request    = json_decode($arr_detail);  
		$email	    = $request->user_email;
		//$email	= $this->input->post('email');
		

		//send email 
		$this->load->library('email');
		$this->db->where('admin_id',1);
		$admin_det = $this->db->get('admin');
		
		if($admin_det->num_rows >0) 
		{    
			$admin 	     = $admin_det->row();
			$admin_email = $admin->admin_email;
			$site_name   = $admin->site_name;
			$admin_no 	 = $admin->contact_number;
			$site_logo   = $admin->site_logo;
		}
		
		$date = date('d/m/Y');
		
		$this->db->where('email',$email);
		$this->db->where('admin_status','');
		$query = $this->db->get('tbl_users');
		if($query->num_rows >0) 
		{
			 
			$getuser 	= $query->row();
			$user_id    = $getuser->user_id;
			$password   = $getuser->password;
			$first_name = $getuser->first_name;
			$last_name  = $getuser->last_name;
		    $user_email = $getuser->email;
			$random_id  = $getuser->random_code;

			if($first_name == '' && $last_name == '')
			{
				$ex_username = explode('@', $user_email);
				$username    = $ex_username[0]; 
			}
			else
			{
				$username = $first_name." ".$last_name;
			}

			$this->db->where('mail_id',3);
			$mail_template = $this->db->get('tbl_mailtemplates');
			if($mail_template->num_rows >0) 
			{  

			    $fetch 	  = $mail_template->row();
			    $subject  = $fetch->email_subject;  
			    $templete = $fetch->email_template;  
			  	$regurl   = base_url().'cashback/password_reset/'.insep_encode($user_id);
			    
			 	$config = Array(
				    'mailtype' => 'html',
				  	'charset'  => 'utf-8',
				);
        
     			$this->email->set_newline("\r\n");
			    $this->email->initialize($config);        
			    $this->email->from($admin_email,$site_name.'!');
			    $this->email->to($user_email);
			    $this->email->subject($subject);

			    $data = array(
					'###USERNAME###'=>$username,
					'###PASSWORD###'=>$password,
					'###SITENAME###'=>$site_name,
					'###ADMINNO###'=>$admin_no,
					'###COMPANYLOGO###'=>base_url()."/uploads/adminpro/".$site_logo,
					'###DATE###'=>$date,
				);
			   
			   $content_pop=strtr($templete,$data); 
			   //print_r($content_pop);
			   $this->email->message($content_pop);
			   $this->email->send();	          
			}
			$res['success'] = 1;
  			$res['message'] = "Your Password details succesffully sent to your Email";
			//return true;          
		}
		else
		{
			$res['success'] = 0;
  			$res['message'] = "Invalid Email Address";
			//return false;                
		}
		$json = json_encode($res);
		echo $json;   	
	}

	//Store page
	function cupom_desconto()
	{
		$arr_detail 	= $_REQUEST['name'];
		$request    	= json_decode($arr_detail);  
		$name	   	    = $request->name;

		if($name == '')
		{
			$res['pagename'] = 'desconto-cupom';
			//$this->load->view('front/desconto-cupom');	
		}	
		else
		{
			$selqry   = $this->db->query("SELECT * from affiliates where affiliate_url='$name'")->row(); 
			$aff_name = $selqry->affiliate_url;
			
			$category_details = $this->front_model->get_category_details($name); // Category details
			$category_name 	  = $category_details->category_name;
			$category_url 	  = $category_details->category_url;
			$category_id 	  = $category_details->category_id;
			
			/*New code 25-5-16.*/
			//echo $selqry->store_categorys; 
			$store_categories = explode(",",$selqry->store_categorys);
			
			foreach($store_categories  as $newstore_categories)
			{
				//echo "hai".$newstore_categories; 
				$rel_store_detail 	  = $this->front_model->cat_details($newstore_categories); 
				$stores_list 	  	  = $this->front_model->get_category_details_byid($newstore_categories);
				$category_name    	  = $stores_list->category_url;
				$res['cat_name_url']  = $category_name;
				$res['cat_name'] 	  = $stores_list->category_name;


				foreach ($rel_store_detail as $newrel_store_detail)
				{
				 	$store_categories 	     = $newrel_store_detail->ref_affiliate_categorys;
				 	$store_count 			 = $newrel_store_detail->ref_affiliate_display_count;
				 	$res['rel_store_detail'] = $store_categories;
				 	$res['rel_storescount']  = $store_count;	

				}	
			}
			/*End*/
		  
			if($name == $aff_name)
			{	
			 
				$aff_id     = $selqry->affiliate_id;
				$store_url  = $this->front_model->seoUrl($name);
				
				//related store details//
				$rel_stores = $this->front_model->get_related_stores($aff_id); 
				if($rel_stores)
				{	
					$split_categories      = explode(",",$rel_stores->ref_affiliate_categorys); 
					
					foreach($split_categories  as $splitcategories)
					{
						if($aff_id == $splitcategories)
						{
							
							$store_categories 	     = $rel_stores->ref_affiliate_categorys;
							$store_count 			 = $rel_stores->ref_affiliate_display_count;
							$res['storecategories']  = $store_categories;
							$res['storescount'] 	 = $store_count;
						}
					}			
				}
				
				//print_r($_POST);

				/*New code hide 30-1-17*/
				//$all_store 			   = $this->front_model->all_store_details();
				//$data['storedetails']    = $all_store;	
				/*End 30-1-17*/

				$store_details = $this->front_model->get_store_details($name);
				if($store_details)
				{
					
					$res['store_details']   = $store_details;
					$store_name 		    = $store_details->affiliate_name;	
					$store_coupons 		    = $this->front_model->get_coupons_from_store($store_name,20);
					$expiry_coupons 		= $this->front_model->expiry_coupons_from_store($store_name,20);
					$res['store_coupons']   = $store_coupons;
					$res['expiry_coupons']  = $expiry_coupons;
					$user_id 			    = $this->session->userdata('user_id');
					$res['user_id'] 	    = $user_id;

					/*New code for meta property related details 11-11-16*/
					$admindetails 		 = $this->front_model->getadmindetails();
					$image_name 		 = base_url().'uploads/adminpro/'.$admindetails[0]->storepage_meta_image;
					$res['page_image']   = $image_name;
					/*End 11-11-16*/

					$res['pagename'] = 'cupom-desconto';
					//$this->load->view('front/cupom-desconto',$data);
				}
				else
				{
					$res['no_records'] = '1';
					$res['pagename'] = '404';
					//$this->load->view('front/404',$data);
				}
			}

			else if($name == $category_url)
			{
				if($sub_categoryurl!="")
				{
					//echo "hai"; exit;
					$sub_categorys = $this->front_model->sub_category_details($sub_categoryurl);
					$res['category_details'] = $sub_categorys;
					$res['main_category'] = $category_name;
				}
				else
				{
					//echo "hai1"; exit;
					$res['category_details'] = $category_details;
					$res['main_category'] = $category_name;
					//echo "hai".print_r($category_details);
				}
				
				$stores_list = $this->front_model->get_stores_cashback_stores_list_cate($category_id); //get stores list
				$res['stores_list'] = $stores_list;
				//print_r($stores_list);
				//$this->load->view('front/desconto',$data);
				$res['pagename']   = 'desconto';
			}
			else
			{
				$res['pagename']   = '404';
				//$this->load->view('front/404',$data);
			}	
		}
		$json = json_encode($res);
		echo $json;
	}

	//Contact Us page
	function contact_us()
	{
		$this->load->library('email');
		$this->db->connection_check();
		
		$arr_detail = $_REQUEST['contact_us'];
		$request    = json_decode($arr_detail);  
		$name  		= $request->user_name;
		$email	    = $request->user_email;
		$message    = $request->message;	


		$data = array(
		'name' => $name,
		'email' => $email,
		'message' => $message,
		);
		$this->db->insert('tbl_contact',$data);
		
		//send email 		
		
		$this->db->where('admin_id',1);
		$admin_det = $this->db->get('admin');
		
		if($admin_det->num_rows > 0) 
		{    
			$admin = $admin_det->row();
			$admin_email = $admin->admin_email;
			$site_name = $admin->site_name;
			$admin_no = $admin->contact_number;
			$site_logo = $admin->site_logo;
		}
		
		$date 	   = date('d/m/Y');	 /*ALTEREI ERA 'Y-m-d' */	
		$name 	   = $name;
		$useremail = $email;
		$message   = $message;
		$url 	   = "<img src=".base_url()."uploads/adminpro/".$site_logo.">";
		
		$this->db->where('mail_id',7);
		$mail_template = $this->db->get('tbl_mailtemplates');
		if($mail_template->num_rows > 0) 
		{        
		    $fetch 		= $mail_template->row();
		    $subject 	= $fetch->email_subject;  
		    $templete 	= $fetch->email_template;  
		    
			$data = array(
				'###CONTENT###'=>$message,
				'###COMPANYLOGO###' =>$url,
				'###SITENAME###'=>$site_name,
				'###ADMINNO###'=>$admin_no,
				'###DATE###'=>$date								
		    );
		  	 
		    $content_pop=strtr($templete,$data);		   
		   
		   	$to 	  = $admin_email;
	        $subject  = $subject;	
	        $message  = $content_pop;
	        $headers  = "From:$useremail \r\n";
	        $headers .= "MIME-Version: 1.0\r\n";
	        $headers .= "Content-type: text/html\r\n";

			mail($to,$subject,$message,$headers);
		   /*$this->email->message($content_pop);
		   $this->email->send();
		   $this->email->print_debugger();   */
              
			$res['success'] = 1;
  			$res['message'] = "Your message has been sent successfully";                
		}
		$json = json_encode($res);
		echo $json;
	}

 	//New page creation for web services 31-1-17//
 	//Refer friends page
 	function refer_friends()
 	{
 		$arr_detail = $_REQUEST['refer'];
		$request    = json_decode($arr_detail);  
		$user_id   	= $request->user_id;
		$result 	= $this->db->query("SELECT * from referrals where user_id='".$user_id."'")->result();

		if($result)
		{
			$res['success']  = 1;
	  		$res['user_id']  = $user_id;
	  		$res['result']   = $result;
		}
		$json = json_encode($res);
		echo $json;
 	}

 	//My account page
 	function minha_conta()
 	{
 		$arr_detail = $_REQUEST['myaccount'];
		$request    = json_decode($arr_detail);  
		$user_id   	= $request->user_id;

 		$bank_details = $this->db->query("SELECT * from `tbl_banknames` ORDER by bankid desc")->result();
		
		$email_notify = $this->db->query("SELECT * from `tbl_users` where `user_id`='".$user_id."'")->result();
	
		if($email_notify == '')
		{
			$res['success'] = 2;
			$res['message'] = 'Records not found';
		}
		else
		{
			$res['success'] = 1;
			$res['results'] = $email_notify;
			$res['notify']  = $bank_details;	
		}
		$json = json_encode($res);
		echo $json;	
 	}

 	//transaction details page
 	function extrato()
 	{
 		$arr_detail  = $_REQUEST['extrato'];
		$request     = json_decode($arr_detail);  
		$user_id   	 = $request->user_id;
		$transaction = $this->db->query("SELECT * from transation_details where user_id='".$user_id."'")->result();	
		$cashback 	 = $this->db->query("SELECT * from cashback where user_id='".$user_id."'")->result();
 		
 		if($transations == '' && $cashback == '')
 		{
 			$res['success'] = 2;
			$res['message'] = 'Records not found';
 		}
 		else
 		{
 			$res['success'] 	 = 1;
			$res['transaction']  = $transaction;
			$res['cashback']  	 = $cashback;
 		}
 		$json = json_encode($res);
		echo $json;
 	}

 	//Click History page
 	function saidas_para_loja()
 	{
		$arr_detail = $_REQUEST['clickhistory'];
		$request    = json_decode($arr_detail);  
		$user_id   	= $request->user_id;

		$click  	= $this->db->query("select *,c.date_added,a.report_date from click_history as c inner join affiliates as a on a.affiliate_id = c.affiliate_id where c.user_id =$user_id order by click_id desc")->result();

 		if($click == '')
 		{
 			$res['success'] = 2;
			$res['message'] = 'Records not found';
 		}
 		else
 		{
 			$res['success'] 	 = 1;
			$res['cashback']  	 = $click;
 		} 
 		$json = json_encode($res);
		echo $json;		
 	}
 	/*End 31-1-17*/

 	/*Web services for resgate page 1-2-17*/
 	//Withdraw page
 	function resgate()
 	{
 		$arr_detail  = $_REQUEST['resgate'];
		$request     = json_decode($arr_detail);  
		$user_id   	 = $request->user_id;
		$userdetail  = $this->db->query("SELECT * from `tbl_users` where `user_id`='".$user_id."'")->result();
		$withdarw    = $this->db->query("SELECT * from `withdraw` where `user_id`='".$user_id."' ORDER by withdraw_id desc")->result();
		$hoverdetail = $this->db->query("SELECT * from `user_information` where `u_id`=1")->result();

		if($userdetail == '' && $withdarw == '' && $hoverdetail == '')
		{
			$res['success']  = 2;
			$res['message']  = 'Records not found';
		}
		else 
		{
			$res['success'] 	= 1;
			$res['userdetail']  = $userdetail;
			$res['withdarw']    = $withdarw;
			$res['hoverdetail'] = $hoverdetail;

		}

		$json = json_encode($res);
		echo $json;	
 	}

 	//Missing cashback view page details
 	function loja_nao_avisou_compra()
 	{
 		$arr_detail  = $_REQUEST['missing_details'];
		$request     = json_decode($arr_detail);  
		$user_id   	 = $request->user_id;

		$miss_cash   = $this->db->query("SELECT * from `missing_cashback` where `user_id`='".$user_id."' ORDER by cashback_id DESC")->result();
		$hoverdetail = $this->db->query("SELECT * from `user_information` where `u_id`=1")->result();

		if($miss_cash == '' && $hoverdetail == '')
		{
			$res['success']  = 2;
			$res['message']  = 'Records not found';
		}
		else 
		{
			$res['success'] 	= 1;
			$res['miss_cash']   = $miss_cash;
			$res['hoverdetail'] = $hoverdetail;

		}

		$json = json_encode($res);
		echo $json;
 	}

 	//Add a missing cashback ticket request page
 	function resgatar_compra_nao_avisada()
 	{
 		$arr_detail   = $_REQUEST['missing_details'];
		$request      = json_decode($arr_detail);  
		$date 		  = date('Y-m-d');
		$user_id   	  = $request->user_id;
		$img   	 	  = $request->image;
		$store   	  = $request->store;
		$trans_refer  = $request->trans_refer;
		$ordervalue   = $request->ordervalue;	
		$hid_click_id = $request->hid_click_id;
		$coupon_used  = $request->coupon_used;
		$details   	  = $request->details;
		$phone_no     = $request->phone_no;

		$data = array(
			'user_id'			=>$user_id,
			'missing_reason'	=>'Missing Cashback',
			'attachment'		=>$img,
			'retailer_name'  	=>$store,
			'transaction_ref_id'=>$trans_refer,
			'transation_amount' =>$ordervalue,
			'click_id'			=>$hid_click_id,
			'coupon_code'		=>$coupon_used,
			'ordervalue'		=>$ordervalue,
			'cashback_details'	=>$details,
			'status'			=>3,
			'trans_date'		=>$date,
			'phone_no' 			=>$phone_no
		);
		$result = $this->db->insert('missing_cashback',$data);

		if($result)
		{
			$res['success']  = 1;
			$res['message']  = 'Success';
		}
		else 
		{
			$res['success']  = 2;
			$res['message']  = 'Not Inserted';
		}

		$json = json_encode($res);
		echo $json;
 	}

 	//Missing Approval view page details
 	function missing_approval()
 	{
 		$arr_detail  	 = $_REQUEST['add_ticket'];
		$request     	 = json_decode($arr_detail);  
		$user_id   	 	 = $request->user_id;
		$miss_approval   = $this->db->query("SELECT * from `cashback` where `user_id`='".$user_id."' AND `status`='Canceled'")->result();
		//$userdetail  	 = $this->db->query("SELECT * from `tbl_users` where `user_id`='".$user_id."'")->result();

		if($miss_approval == '')
		{
			$res['success']  = 2;
			$res['message']  = 'Records not found';
		}
		else 
		{
			$res['success'] 	  = 1;
			$res['miss_approval'] = $miss_approval;
			//$res['userdetail']  = $userdetail;
		}
		$json = json_encode($res);
		echo $json;
 	}

 	function add_missing_approval()
 	{
 		$arr_detail  	 = $_REQUEST['add_missing'];
		$request     	 = json_decode($arr_detail);  
		$user_id   	 	 = $request->user_id;
		$cashback_id 	 = $request->cashback_id;

		$userdetail  	   = $this->db->query("SELECT * from `tbl_users` where `user_id`='".$user_id."'")->result();
		$add_miss_approval = $this->db->query("SELECT * from `cashback` where `cashback_id`='".$cashback_id."'")->result();

		if($add_miss_approval == '' && $userdetail == '')
		{
			$res['success']  = 2;
			$res['message']  = 'Records not found';
		}
		else 
		{
			$res['success'] 	  	  = 1;
			$res['add_miss_approval'] = $add_miss_approval;
			$res['userdetail']    	  = $userdetail;
		}
		$json = json_encode($res);
		echo $json;
 	}

 	function missing_approval_submit()
 	{
 		$arr_detail  	 	   = $_REQUEST['miss_submit'];
		$request     	 	   = json_decode($arr_detail);  
		$user_id   	 	 	   = $request->user_id;
		$banner_image 	 	   = $request->banner_image;
		$store 				   = $request->store;
		$transaction_reference = $request->transaction_reference;
		$transaction_amount    = $request->transaction_amount;
		$hid_click_id  		   = $request->hid_click_id;
		$coupon_used		   = $request->coupon_used;
		$ordervalue			   = $request->ordervalue;
		$details 			   = $request->details;
		$date 				   = $request->date;
		$cashback_reference    = $request->cashback_reference;

		$this->db->insert('missing_cashback',$data);
		$user_details = $this->edit_account($user_id);
		$mail 		  = $user_details->email;
 	}
 	/*End 1-2-17*/

	//Check Email validation Process Not needed
	function check_email($email)
	{
		
		$this->db->connection_check();
		$this->db->where('email',$email);
		$this->db->where('admin_status','');
		
		$qry = $this->db->get('tbl_users');
		$numrows1 = $qry->num_rows();
		if($numrows1 == 0)
		{
			//return true;
			return 1;
		}
		else
		{
			return 0;
			//return false;
		}	
	}
	function random_string($length) 
	{
		$key = '';
		$keys = array_merge(range(0, 9), range('A', 'Z'));

		for ($i = 0; $i < $length; $i++) {
		$key .= $keys[array_rand($keys)];
		}
		return $key;
	}
}
