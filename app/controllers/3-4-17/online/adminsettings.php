<?php
class Adminsettings extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->input->session_helper();
	}
	
	function check_cate()
	{
		$this->input->session_helper();
		$category = $this->input->post('category_name');
		$result = $this->admin_model->check_cate($category);
		if($result)
		{
			echo 1;
		} else {
			echo 0;
		}
	}
	
	function check_sub_cate()
	{
		$this->input->session_helper();
		$category = $this->input->post('category_name');
		$maincate = $this->input->post('maincate');
		$result = $this->admin_model->check_sub_cate($category,$maincate);
		if($result)
		{
			echo 1;
		} else {
			echo 0;
		}
	}
	
	
	// admin login..
	public function index(){
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id!=""){
			redirect('adminsettings/dashboard','refresh');
		}
		$this->load->view('adminsettings/login');
	}
	
	// login check
	function logincheck()
	{
		$this->input->session_helper();
		if($this->input->post('signin')){
			$result = $this->admin_model->logincheck();	
			if(!$result){
				$data['invalid_login'] = 'Invalid username or password';
				$this->load->view('adminsettings/login',$data);
			}	
	    	else 
			{
				redirect('adminsettings/dashboard','refresh');
			}			
		}
	}
	
	// admin dashboard
	function dashboard()
	{	
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		}
		else {
			$this->load->view('adminsettings/dashboard');
		}
	}
	
	// admin settings page..
	function settings()
	{
		$this->input->session_helper();
		//echo $sess_time = $this->session->userdata('sess_time'); exit;
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		}
		else 
		{
			$main 			     = $this->session->userdata('admin_id');
			$admin_details       = $this->admin_model->getadmindetails();
			$category_details 	 = $this->db->query("SELECT * FROM referral_settings where ref_id!=''")->result();
			$data['cat_details'] = $category_details; 
			if($admin_details)
			{
				foreach($admin_details as $details)
				{
					$data['admin_id'] 		  = $details->admin_id;
					$data['admin_username']   = $details->admin_username;
					$data['admin_email'] 	  = $details->admin_email;
					$data['email_notify'] 	  = $details->email_notify;
					$data['admin_paypal'] 	  = $details->admin_paypal;
					$data['paypal_mode'] 	  = $details->paypal_mode;
					$data['admin_logo'] 	  = $details->admin_logo;
					$data['site_logo'] 		  = $details->site_logo;
					$data['site_favicon'] 	  = $details->site_favicon;
					$data['homepage_title']   = $details->homepage_title;
					$data['referral_cashback']= $details->referral_cashback;
					$data['minimum_cashback'] = $details->minimum_cashback;
					$data['site_name'] 	      = $details->site_name;
					$data['site_url'] 		  = $details->site_url;
					$data['admin_fb'] 		  = $details->admin_fb;
					$data['admin_twitter']    = $details->admin_twitter;
					$data['admin_gplus'] 	  = $details->admin_gplus;
					$data['admin_pintrust']   = $details->admin_pintrust;
					$data['admin_instagram']  = $details->admin_instagram;
					$data['contact_number']   = $details->contact_number;
					$data['contact_info'] 	  = $details->contact_info;
					$data['address'] 		  = $details->address;
					$data['enable_blog']      = $details->enable_blog;
					$data['enable_shopping']  = $details->enable_shopping;	
					$data['enable_slider']    = $details->enable_slider;						
					$data['site_mode'] 	 	  = $details->site_mode;
					$data['meta_title'] 	  = $details->meta_title; //28-11-14 -- suhirdha added section starts..
					$data['meta_keyword'] 	  = $details->meta_keyword;
					$data['meta_description'] = $details->meta_description;
					$data['google_analytics'] = $details->google_analytics;
					$data['google_key'] 	  = $details->google_key;
					$data['google_secret']    = $details->google_secret;
					$data['facebook_key']     = $details->facebook_key; //seetha 18.08.2015
					$data['facebook_secret']  = $details->facebook_secret; 
					$data['yahoo_key'] 		  = $details->yahoo_key; //seetha 18.08.2015
					$data['yahoo_secret'] 	  = $details->yahoo_secret; 
					/*new code for hotmail configuration 6-9-16*/
					$data['hotmail_key'] 	  = $details->hotmail_key;
					$data['hotmail_secret']   = $details->hotmail_secret;
					/*end*/
					$data['blog_url'] 		  = $details->blog_url;
					$data['benefit_bonus']    = $details->unic_bonus;
					
					/*New code for category type enable disable settings 3-5-16*/
					$data['cat_two_status']	  = $details->cat_two_status;
					$data['cat_three_status'] = $details->cat_three_status;
					$data['cat_four_status']  = $details->cat_four_status;
					$data['cat_five_status']  = $details->cat_five_status;
					/*End*/
					//SATz session Time set
					$data['ses_datetime']     = $details->ses_datetime;
                    //SATz session Time set
                    /*new field for default coupon expiry date 10-10-16*/
                    $data['coupon_exp_date']  = $details->coupon_expiry_date;
                    /*End*/
                    /*new field for zanox api tracking and extra tracking param 08-07-17*/
                    $data['zanox_tracking']         = $details->zanox_tracking;
                    $data['zanox_extra_tracking']   = $details->zanox_extra_tracking;
                    $data['cityads_tracking']       = $details->cityads_tracking;
                    $data['cityads_extra_tracking'] = $details->cityads_extra_tracking;
                    $data['rakuten_tracking']       = $details->rakuten_tracking;
                    $data['rakuten_extra_tracking'] = $details->rakuten_extra_tracking;
                    $data['afilio_tracking']        = $details->afilio_tracking;
                    $data['afilio_extra_tracking']  = $details->afilio_extra_tracking;
                    $data['lomadee_tracking']       = $details->lomadee_tracking;
                    $data['lomadee_extra_tracking'] = $details->lomadee_extra_tracking;
                    /*End*/
				}
				$this->load->view('adminsettings/settings',$data);
			}	
		}		
	}
	
	//admin settings
	function settingsupdate()
	{
		$this->input->session_helper();
		if($this->input->post('save'))
		{ 
			$this->security->cookie_handlers();
			$new_random 	  = mt_rand(0,99999);
			$admin_logo 	  = $_FILES['admin_logo']['name'];
			$site_logo  	  = $_FILES['site_logo']['name'];		
			$site_favicon 	  = $_FILES['site_favicon']['name'];
			$minimum_cashback = $this->input->post('minimum_cashback');
			$benefit_bonus	  = $this->input->post('benefit_bonus');

			if(($minimum_cashback < 1 ) ||(!is_numeric($minimum_cashback))) 
			{ 
				$this->session->set_flashdata('error','Minimum cashback value should be from 1 and it should be an integer.');
				redirect('adminsettings/settings','refresh');
			}
			/*New code for amazon s3 copy settings*/
			$amazon_s3_details = $this->admin_model->amazon_s3_settings();
			$copy_status       = $amazon_s3_details->copy_files_status;
			/*End*/
			if($admin_logo!="") 
			{
					
				/*New code for image name field changes 3-10-16*/
				$info 	 	 	= new SplFileInfo($admin_logo);
				$file_ex 	 	= $info->getExtension();
				$newfilename 	= str_replace('.'.$file_ex,'', $admin_logo);
				$admin_logo     = $newfilename.$new_random.'.'.$file_ex;
				/*End 3-10-16*/

				//$admin_logo = remove_space($new_random.$admin_logo);
				$config['upload_path'] ='uploads/adminpro';
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['file_name']=$admin_logo;
				
				
				//New code for AWS 24-9-16//
				if($copy_status == 1)
				{
					$filepath = 'uploads/adminpro/'.$admin_logo;
					$tmp 	  = $_FILES['admin_logo']['tmp_name'];
					$this->load->library('S3');
					$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
				}
				//End//	
				$this->load->library('upload', $config);
				$this->upload->initialize($config);	
				if($admin_logo!="" && (!$this->upload->do_upload('admin_logo')))
				{
					$admin_logoerror = $this->upload->display_errors();
				}
					if(isset($admin_logoerror))        
					{
						$this->session->set_flashdata('error',$admin_logoerror);
						redirect('adminsettings/settings','refresh');
					}
			}
			else
			{
				$admin_logo = $this->input->post('hidden_img');
			}
			
			if($site_logo!="") 
			{
				
				/*New code for image name field changes 3-10-16*/
				$info 	 	 	= new SplFileInfo($site_logo);
				$file_ex 	 	= $info->getExtension();
				$newfilename 	= str_replace('.'.$file_ex,'', $site_logo);
				$site_logo      = $newfilename.$new_random.'.'.$file_ex;
				/*End 3-10-16*/

				//$site_logo = remove_space($new_random.$site_logo);
				$config['upload_path'] 	 = 'uploads/adminpro';
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['file_name']	 = $site_logo;
				
				//New code for AWS 24-9-16//
				if($copy_status == 1)
				{
					$filepath = 'uploads/adminpro/'.$site_logo;
					$tmp 	  = $_FILES['site_logo']['tmp_name'];
					$this->load->library('S3');
					$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
				}
				//End//	
				$this->load->library('upload', $config);
				$this->upload->initialize($config);	
				if($site_logo!="" && (!$this->upload->do_upload('site_logo')))
				{
					$site_logoerror = $this->upload->display_errors();
				}
					if(isset($site_logoerror))        
					{
						$this->session->set_flashdata('error',$site_logoerror);
						redirect('adminsettings/settings','refresh');
					}
			}	
			else
			{
				$site_logo = $this->input->post('hidden_site_logo');
			}
			if($site_favicon!="") 
			{
				
				/*New code for image name field changes 3-10-16*/
				$info 	 	 	= new SplFileInfo($site_favicon);
				$file_ex 	 	= $info->getExtension();
				$newfilename 	= str_replace('.'.$file_ex,'', $site_favicon);
				$site_favicon   = $newfilename.$new_random.'.'.$file_ex;
				/*End 3-10-16*/

				//$site_favicon = remove_space($new_random.$site_favicon);
				$config['upload_path'] ='uploads/adminpro';
				$config['allowed_types'] = 'ico|gif|jpg|jpeg|png';
				$config['file_name']=$site_favicon;
		
				//New code for AWS 24-9-16//
				if($copy_status == 1)
				{
					$filepath = 'uploads/adminpro/'.$site_favicon;
					$tmp 	  = $_FILES['site_favicon']['tmp_name'];
					$this->load->library('S3');
					$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
				}
				//End//
				$this->load->library('upload', $config);
				$this->upload->initialize($config);	
				if($site_favicon!="" && (!$this->upload->do_upload('site_favicon')))
				{
					$site_faviconerror = $this->upload->display_errors();
				}
					if(isset($site_faviconerror))        
					{
						$this->session->set_flashdata('error',$site_faviconerror);
						redirect('adminsettings/settings','refresh');
					}
			}	
			else
			{
				$site_favicon = $this->input->post('hidden_site_favicon');
			}

			$updated = $this->admin_model->updatesettings($admin_logo,$site_logo,$site_favicon);
			
			if($updated)
			{
				$this->session->set_flashdata('success', 'Admin settings updated successfully. ');
				redirect('adminsettings/settings','refresh');
			}  
			else
			{
				$this->session->set_flashdata('error', 'Admin settings not updated successfully.');
				redirect('adminsettings/settings','refresh');
			} 
		}		
	}
	
	// change password..
	function change_password()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else 
		{
			$email_notify = $this->db->query("select * FROM admin where admin_id=$admin_id")->row('email_notify');
			if($this->input->post('save')){
			
			$result = $this->admin_model->update_password();
				if($result){
					$this->session->set_flashdata('success', 'Password changed successfully.');
					redirect('adminsettings/change_password','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Old Password did not match.');
					redirect('adminsettings/change_password','refresh');
				}
			}	
			$data['email_notify'] = $email_notify;
			$this->load->view('adminsettings/changepassword',$data);		
		}
		$this->security->cookie_handlers();
	}
		// view all cms content
	function cms()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$data['allcms'] = $this->admin_model->get_allcms();
			$this->load->view('adminsettings/cms',$data);
		}	
	}
	
	// add cms contents..
	function addcms()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$this->security->cookie_handlers();
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save'))
			{
				$cms_content = $this->input->post('cms_content');
				$meta_image  = $_FILES['meta_image']['name'];	
				if($cms_content=="")
				{
					$this->session->set_flashdata('error', 'Please enter CMS content.');
					// $post_title = $this->input->post('page_title');
					redirect('adminsettings/addcms','refresh');
				}
				else
				{
					/*New code for amazon s3 copy settings*/
					$amazon_s3_details = $this->admin_model->amazon_s3_settings();
					$copy_status       = $amazon_s3_details->copy_files_status;
					/*End*/

					$flag = 0;
					if($meta_image!="") 
					{
						$new_random 	= mt_rand(0,99999);
						$meta_image 	= $_FILES['meta_image']['name'];
						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($meta_image);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $meta_image);
						$meta_image     = $newfilename."-".$new_random.'.'.$file_ex;
						/*End 3-10-16*/
						//$cover_photo 			 = remove_space($new_random.$cover_photo);
						$config['upload_path']   = 'uploads/adminpro';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['file_name']     = $meta_image;
						
						//New code for AWS 24-9-16//
						if($copy_status == 1)
						{
							$filepath = 'uploads/adminpro/'.$meta_image;
							$tmp 	  = $_FILES['meta_image']['tmp_name'];
							$this->load->library('S3');
							$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
							$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
						}
						//End//

						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($meta_image!="" && (!$this->upload->do_upload('meta_image')))
						{
							$cover_photoerror = $this->upload->display_errors();
						}
						if(isset($cover_photoerror))
						{
							$flag=1;
							/*$this->session->set_flashdata('affiliate_name',$affiliate_name);
							$this->session->set_flashdata('affiliate_desc',$affiliate_desc);
							$this->session->set_flashdata('logo_url',$logo_url);
							$this->session->set_flashdata('meta_keyword',$meta_keyword);
							$this->session->set_flashdata('meta_description',$meta_description);
							$this->session->set_flashdata('cashback_percentage',$cashback_percentage);
							$this->session->set_flashdata('error',$cover_photoerror);*/
							$this->session->set_flashdata('error',$cover_photoerror);
							redirect('adminsettings/addcms','refresh');
						}
					}

					if($flag == 0)
					{
						$results = $this->admin_model->addcms($meta_image);
						if($results){
							$this->session->set_flashdata('success', ' CMS details added successfully.');
							redirect('adminsettings/cms','refresh');
						}
						else{
							$this->session->set_flashdata('error', 'Error occurred while adding CMS.');
							redirect('adminsettings/addcms','refresh');
						}
					}	
				}
			}
			$data['action']='new';
			$this->load->view('adminsettings/addcms',$data);
		}
	}
	
	// view cms content
	function editcms($cms_editid)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$get_cms = $this->admin_model->get_cmscontent($cms_editid);
			$this->security->cookie_handlers();
			foreach($get_cms as $get)
			{
				$data['cms_id'] 	   = $get->cms_id;
				$data['cms_heading']   = $get->cms_heading;
				$data['cms_metatitle'] = $get->cms_metatitle;
				$data['cms_metakey']   = $get->cms_metakey;
				$data['cms_metadesc']  = $get->cms_metadesc;
				/*New code 22-12-16*/
				$data['url_slag'] 	   = $get->cms_urlslag;
				$data['meta_type'] 	   = $get->cms_meta_type;
				$data['meta_sitename'] = $get->meta_sitename;
				$data['meta_url']      = $get->cms_meta_url;
				$data['meta_image']    = $get->cms_meta_image;
				/*End 22-12-16*/
				$data['cms_content'] = $get->cms_content;
				$data['cms_position'] = $get->cms_position;
				$data['cms_status'] = $get->cms_status;
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/addcms',$data);
		}
	}
	
	// update cms contents..	
	function updatecms()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$this->security->cookie_handlers();
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				$cms_content = $this->input->post('cms_content');
				if($cms_content=="") 
				{
					$this->session->set_flashdata('error', 'Please enter CMS content.');
					$data['action']="Edit";
					// $post_title = $this->input->post('page_title');
					redirect('adminsettings/addcms','refresh');
				}
				else 
				{
					/*New code for amazon s3 copy settings*/
					$amazon_s3_details = $this->admin_model->amazon_s3_settings();
					$copy_status       = $amazon_s3_details->copy_files_status;
					/*End*/

					$flag = 0;
					if($meta_image!="") 
					{
						$new_random 	= mt_rand(0,99999);
						$meta_image 	= $_FILES['meta_image']['name'];
						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($meta_image);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $meta_image);
						$meta_image     = $newfilename."-".$new_random.'.'.$file_ex;
						/*End 3-10-16*/
						//$cover_photo 			 = remove_space($new_random.$cover_photo);
						$config['upload_path']   = 'uploads/adminpro';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['file_name']     = $meta_image;
						
						//New code for AWS 24-9-16//
						if($copy_status == 1)
						{
							$filepath = 'uploads/adminpro/'.$meta_image;
							$tmp 	  = $_FILES['meta_image']['tmp_name'];
							$this->load->library('S3');
							$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
							$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
						}
						//End//

						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($meta_image!="" && (!$this->upload->do_upload('meta_image')))
						{
							$cover_photoerror = $this->upload->display_errors();
						}
						if(isset($cover_photoerror))
						{
							$flag=1;
							/*$this->session->set_flashdata('affiliate_name',$affiliate_name);
							$this->session->set_flashdata('affiliate_desc',$affiliate_desc);
							$this->session->set_flashdata('logo_url',$logo_url);
							$this->session->set_flashdata('meta_keyword',$meta_keyword);
							$this->session->set_flashdata('meta_description',$meta_description);
							$this->session->set_flashdata('cashback_percentage',$cashback_percentage);
							$this->session->set_flashdata('error',$cover_photoerror);*/
							$this->session->set_flashdata('error',$cover_photoerror);
							redirect('adminsettings/addcms','refresh');
						}
					}
					else
					{
						$meta_image = $this->input->post('hidden_img');
					}
					if($flag == 0)
					{
						$updated = $this->admin_model->updatecms($meta_image);
						if($updated){
							$data['action']="Edit";
							$this->session->set_flashdata('success', ' CMS details updated successfully.');
							redirect('adminsettings/cms','refresh');
						}
						else{
							$data['action']="Edit";
							$this->session->set_flashdata('error', 'Error occurred while updating CMS details.');
							redirect('adminsettings/cms','refresh');
						}
					}
				}
			}		
		}
	}
	
	// delete cms content 
	function deletecms($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deletecms($id);
			if($deletion){
				$data['action']="Edit";
				$this->session->set_flashdata('success', ' CMS details deleted successfully.');
				redirect('adminsettings/cms','refresh');
			}
			else{
				$data['action']="Edit";
				$this->session->set_flashdata('error', 'Error occurred while deleting CMS details.');
				redirect('adminsettings/cms','refresh');
			}
		}
	}
	
	// view all faqs content
	function faqs()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$this->security->cookie_handlers();
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				 if($this->input->post('chkbox'))
				 {
					
					$sort_order = $this->input->post('chkbox');					 
					 $results = $this->admin_model->delete_bulk_records($sort_order,'tbl_faq','faq_id');
				 }
				if($results){
					
					$this->session->set_flashdata('success', 'FAQ details deleted successfully.');
					redirect('adminsettings/faqs','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while updating FAQ details.');
					redirect('adminsettings/faqs','refresh');
				}
			}
			$data['allfaqs'] = $this->admin_model->get_allfaqs();
			$this->load->view('adminsettings/faq',$data);
		}	
	}
	// add faqs
	function addfaqs()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				$faq_content = $this->input->post('faq_ans');
				if($faq_content==""){
					$this->session->set_flashdata('error', 'Please enter FAQ content.');
					// $post_title = $this->input->post('page_title');
					redirect('adminsettings/addfaqs','refresh');
				}
				else{
					$results = $this->admin_model->addfaqs();
					if($results){
						$this->session->set_flashdata('success', ' FAQ details added successfully.');
						redirect('adminsettings/faqs','refresh');
					}
					else{
						$this->session->set_flashdata('error', 'Error occurred while adding FAQ.');
						redirect('adminsettings/addfaqs','refresh');
					}
				}
			}
			$data['action']='new';
			$this->security->cookie_handlers();
			$this->load->view('adminsettings/addfaq',$data);
		}
	}
	// view faq content
	function editfaq($faq_editid)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if(($admin_id=="") || ($faq_editid=="")){
			redirect('adminsettings/index','refresh');
		} else
		{
			$get_faq = $this->admin_model->get_faqcontent($faq_editid);
			foreach($get_faq as $get){
				$data['faq_id'] = $get->faq_id;
				$data['faq_qn'] = $get->faq_qn;
				$data['faq_ans'] = $get->faq_ans;
				$data['status'] = $get->status;
			}
			$data['action'] = "edit";
			$this->security->cookie_handlers();
			$this->load->view('adminsettings/addfaq',$data);
		}
	}
	// updating faq content
	function updatefaq()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				$faq_ans = $this->input->post('faq_ans');
				if($faq_ans=="") {
					$this->session->set_flashdata('error', 'Please enter FAQ content.');
					$data['action']="Edit";
					// $post_title = $this->input->post('page_title');
					redirect('adminsettings/addfaq','refresh');
				}
				else {
					$updated = $this->admin_model->updatefaq();
					if($updated){
						$data['action']="Edit";
						$this->session->set_flashdata('success', ' FAQ details updated successfully.');
						redirect('adminsettings/faqs','refresh');
					}
					else{
						$data['action']="Edit";
						$this->session->set_flashdata('error', 'Error occurred while updating FAQ details.');
						redirect('adminsettings/faqs','refresh');
					}
				}
			}		
		}	
	}	
	// delete faq content 
	function deletefaq($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deletefaq($id);
			if($deletion){
				$this->session->set_flashdata('success', ' FAQ details deleted successfully.');
				redirect('adminsettings/faqs','refresh');
			}
			else{
				$this->session->set_flashdata('error', 'Error occurred while deleting FAQ details.');
				$this->security->cookie_handlers();
				redirect('adminsettings/faqs','refresh');
			}
		}
	}
	
	// view all users..
	function users()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				if($this->input->post('exUsers'))
				{
					$results = $this->admin_model->export_users();
				}
				$results = $this->admin_model->multi_delete_user();
				if($results){
					
					$this->session->set_flashdata('success', 'Users deleted successfully.');
					redirect('adminsettings/users','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while deleted user details.');
					redirect('adminsettings/users','refresh');
				}
			}
			//$data['allusers'] = $this->admin_model->get_allusers();
			$this->load->view('adminsettings/users',$data);
		}	
	}
	
	// view particular user details..
	function view_user($userid)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if(($admin_id=="") || ($userid=="")) {
			redirect('adminsettings/index','refresh');
		} else
		{
			$data['user_detail'] = $this->admin_model->view_user($userid);
			$this->load->view('adminsettings/viewuser',$data);
		}	
	}
	
	// update user status..
	function userupdate()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="") {
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
			$updated = $this->admin_model->userupdate();
			$this->security->cookie_handlers();
				if($updated){
					$this->session->set_flashdata('success', ' User details updated successfully.');
					redirect('adminsettings/users','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while updating user details.');
					redirect('adminsettings/users','refresh');
				}
			}
		}	
	}
	
	// delete user details..
	function deleteuser($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$this->security->cookie_handlers();
			$deletion = $this->admin_model->deleteuser($id);
			if($deletion){
				$this->session->set_flashdata('success', ' User details deleted successfully.');
				redirect('adminsettings/users','refresh');
			}
			else{
				$this->session->set_flashdata('error', 'Error occurred while deleting user details.');
				redirect('adminsettings/users','refresh');
			}
		}
	}
	
	// view all categories..
	function categories()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				$results = $this->admin_model->sort_categorys_new();
				 if($this->input->post('chkbox'))
				 {
					 $results = $this->admin_model->sort_categorys_new_delete();
				 }
				
				
				if($results){
					
					$this->session->set_flashdata('success', 'Category details updated successfully.');
					redirect('adminsettings/categories','refresh');
				}
				else{
					$this->security->cookie_handlers();
					$this->session->set_flashdata('error', 'Error occurred while updating category details.');
					redirect('adminsettings/categories','refresh');
				}
			}
			$data['categories'] = $this->admin_model->categories();
			$this->load->view('adminsettings/categories',$data);
		}
	}
	
	//view Premium category
	function premium_categories()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				$results = $this->admin_model->sort_premium_categorys_new();
				if($this->input->post('chkbox'))
				 {
					 $this->security->cookie_handlers();
					 $results = $this->admin_model->sort_premium_categorys_new_delete();
				 }
				
				if($results){
					
					$this->session->set_flashdata('success', 'Category details updated successfully.');
					redirect('adminsettings/premium_categories','refresh');
				}
				else{
					;
					$this->session->set_flashdata('error', 'Error occurred while updating category details.');
					redirect('adminsettings/premium_categories','refresh');
				}
			}
			$data['categories'] = $this->admin_model->premium_categories();
			$this->load->view('adminsettings/premium_categories',$data);
		}
	}
	
	
	// add category
	function addcategory()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save'))
			{
				if($this->input->post('category_type')==0) //Normal Coupon
				{
					$this->security->cookie_handlers();
					$results = $this->admin_model->addcategory();
					if($results)
					{
						$this->session->set_flashdata('success', ' Category details added successfully.');
						redirect('adminsettings/categories','refresh');
					}
					else
					{
						$this->session->set_flashdata('error', 'Error occurred while adding category.');
						redirect('adminsettings/addcategory','refresh');
					}
				}
				else//Premium Coupon
				{
					$results = $this->admin_model->addpremiumcategory();
					if($results)
					{
						$this->session->set_flashdata('success', ' Category details added successfully.');
						redirect('adminsettings/premium_categories','refresh');
					}
					else
					{
						$this->session->set_flashdata('error', 'Error occurred while adding category.');
						redirect('adminsettings/addcategory','refresh');
					}
				}
			}
			$data['action']='new';
			$this->load->view('adminsettings/addcategory',$data);
		}
	}
		
	// edit category..
	function editcategory($cate_editid)
	{
		$this->input->session_helper();			
		$admin_id = $this->session->userdata('admin_id');
		if(($admin_id=="") || ($cate_editid=="")){
			redirect('adminsettings/index','refresh');
		} else
		{
			$this->security->cookie_handlers();
			$get_category = $this->admin_model->get_category($cate_editid);
			foreach($get_category as $get){
				$data['category_id'] = $get->category_id;
				$data['category_name'] = $get->category_name;
				$data['meta_keyword'] = $get->meta_keyword;
				$data['meta_description'] = $get->meta_description;
				$data['category_status'] = $get->category_status;

				$data['category_image']    = $get->category_img;
				$data['category_desc']   = $get->category_desc;
				$data['ofertas_description']   = $get->ofertas_description;

			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/addcategory',$data);
		}
	}
	
	// edit category..
	function editpremiumcategory($cate_editid)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if(($admin_id=="") || ($cate_editid=="")){
			redirect('adminsettings/index','refresh');
		} else
		{
			$this->security->cookie_handlers();
			$get_category = $this->admin_model->get_premium_category($cate_editid);
			foreach($get_category as $get){
				$data['category_id'] = $get->category_id;
				$data['category_name'] = $get->category_name;
				$data['meta_keyword'] = $get->meta_keyword;
				$data['meta_description'] = $get->meta_description;
				$data['category_status'] = $get->category_status;
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/addpremiumcategory',$data);
		}
	}
		
	// update category	
	function updatecategory()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$this->security->cookie_handlers();
			if($this->input->post('save')){
				$id = $this->input->post('category_id');
				$updated = $this->admin_model->update_category();
				if($updated){
					$data['action']="Edit";
					$this->session->set_flashdata('success', ' Category details updated successfully.');
					redirect('adminsettings/categories','refresh');
				}
				else{
					$data['action']="Edit";
					$this->session->set_flashdata('error', 'Error occurred while updating category details.');
					redirect('adminsettings/editcategory/'.$id,'refresh');
				}
			}		
		}	
	}
		
	function updatepremiumcategory()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$this->security->cookie_handlers();
			$id = $this->input->post('category_id');
			if($this->input->post('save')){
				$updated = $this->admin_model->update_premium_category();
				if($updated){
					$data['action']="Edit";
					$this->session->set_flashdata('success', 'Premium Category details updated successfully.');
					redirect('adminsettings/premium_categories','refresh');
				}
				else{
					$data['action']="Edit";
					$this->session->set_flashdata('error', 'Error occurred while updating category details.');
					redirect('adminsettings/editpremiumcategory/'.$id,'refresh');
				}
			}		
		}	
	}
	
	//delete category..
	function deletecategory($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$this->security->cookie_handlers();
			$deletion = $this->admin_model->deletecategory($id);
			if($deletion){
				$this->session->set_flashdata('success', ' Category deleted successfully.');
				redirect('adminsettings/categories','refresh');
			}
			else{
				$this->session->set_flashdata('error', 'Error occurred while deleting category.');
				redirect('adminsettings/categories','refresh');
			}
		}	
	}
	
	//delete premium  category..
	function delete_premium_category($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deletepremiumcategory($id);
			if($deletion){
				$this->session->set_flashdata('success', 'Premium Category deleted successfully.');
				redirect('adminsettings/premium_categories','refresh');
			}
			else{
				$this->session->set_flashdata('error', 'Error occurred while deleting category.');
				redirect('adminsettings/premium_categories','refresh');
			}
		}	
	}
	
	// view all affiliates..
	function affiliates()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		} 
		else
		{
			if($this->input->post('hidd'))
			{
				$results = $this->admin_model->sort_affiliates();
				if($this->input->post('chkbox'))
				{
					$results = $this->admin_model->sort_affiliates_delete();
				}
				if($results)
				{
					$this->session->set_flashdata('success', 'Affiliates details updated successfully.');
					redirect('adminsettings/affiliates','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while updating affiliates details.');
					redirect('adminsettings/affiliates','refresh');
				}
			}
			//$data['affiliates'] = $this->admin_model->affiliates();
			$this->load->view('adminsettings/affiliates',$data);
		}
	}
	
	// add new affiliate
	function addaffiliate()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{	
			if($this->input->post('save'))
			{
				$flag=0;
				$affiliate_name 	 = $this->input->post('affiliate_name');
				$affiliate_desc 	 = $this->input->post('affiliate_desc');
				$logo_url 			 = $this->input->post('logo_url');
				/*$site_url 		 = $this->input->post('site_url');*/
				$meta_keyword 		 = $this->input->post('meta_keyword');
				$meta_description    = $this->input->post('meta_description');
				$cashback_percentage = $this->input->post('cashback_percentage');
				
				//Pilaventhiran 03/05/2016 START
				$notify_desk 		 	  = $this->input->post('notify_desk');
				$old_cashback 		 	  = $this->input->post('old_cashback');
				$redir_notify 		 	  = $this->input->post('redir_notify');
				$name_extra_param    	  = $this->input->post('name_extra_param');
				$content_extra_param 	  = $this->input->post('content_extra_param');
				$cashback_percent_android = $this->input->post('cashback_percent_android');
				$cashback_content_android = $this->input->post('cashback_content_android');		
				$cover_photo 			  = $_FILES['cover_photo']['name'];		
				$report_date 			  = $this->input->post('report_date');	

				
				/*New code for amazon s3 copy settings*/
				$amazon_s3_details = $this->admin_model->amazon_s3_settings();
				$copy_status       = $amazon_s3_details->copy_files_status;
				/*End*/


				//Pilaventhiran 03/05/2016 END
				$affiliate_logo = $_FILES['affiliate_logo']['name'];
				$sidebar_image  = $_FILES['sidebar_image']['name'];
				if($affiliate_desc=="")
				{
					$flag=1;
					$this->session->set_flashdata('affiliate_name',$affiliate_name);
					$this->session->set_flashdata('cashback_percentage',$cashback_percentage);
					$this->session->set_flashdata('logo_url',$logo_url);
					$this->session->set_flashdata('meta_keyword',$meta_keyword);
					$this->session->set_flashdata('meta_description',$meta_description);
					$this->session->set_flashdata('error', 'Please enter Affiliate content.');
					// $post_title = $this->input->post('page_title');
					redirect('adminsettings/addaffiliate','refresh');
				}
				else if($affiliate_logo=="")
				{
					$flag=1;
					$this->session->set_flashdata('affiliate_name',$affiliate_name);
					$this->session->set_flashdata('affiliate_desc',$affiliate_desc);
					$this->session->set_flashdata('cashback_percentage',$cashback_percentage);
					$this->session->set_flashdata('logo_url',$logo_url);
					$this->session->set_flashdata('meta_keyword',$meta_keyword);
					$this->session->set_flashdata('meta_description',$meta_description);
					$this->session->set_flashdata('error', 'Please upload an image .');
					// $post_title = $this->input->post('page_title');
					redirect('adminsettings/addaffiliate','refresh');
				}	
				//Pilaventhiran 03/05/2016 START
				elseif ($cover_photo=="") 
				{
					$flag=1;
					$this->session->set_flashdata('affiliate_name',$affiliate_name);
					$this->session->set_flashdata('affiliate_desc',$affiliate_desc);
					$this->session->set_flashdata('cashback_percentage',$cashback_percentage);
					$this->session->set_flashdata('logo_url',$logo_url);
					$this->session->set_flashdata('meta_keyword',$meta_keyword);
					$this->session->set_flashdata('meta_description',$meta_description);
					$this->session->set_flashdata('error', 'Please upload a cover photo .');
					// $post_title = $this->input->post('page_title');
					redirect('adminsettings/addaffiliate','refresh');
				}		
				//Pilaventhiran 03/05/2016 END			
				else 
				{
					$flag=0;
					if($affiliate_logo!="") 
					{
						$new_random 	= mt_rand(0,99999);
						$affiliate_logo = $_FILES['affiliate_logo']['name'];
						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($affiliate_logo);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $affiliate_logo);
						$affiliate_logo = $newfilename."-".$new_random.'.'.$file_ex;
						/*End 3-10-16*/
						//$affiliate_logo 		 = remove_space($new_random.$affiliate_logo);
						$config['upload_path']   = 'uploads/affiliates';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['file_name'] 	 = $affiliate_logo;
						
						//New code for AWS 24-9-16//
						if($copy_status == 1)
						{
							$filepath 				 = 'uploads/affiliates/'.$affiliate_logo;
							$tmp 	  				 = $_FILES['affiliate_logo']['tmp_name'];
							$this->load->library('S3');
							$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
							$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
						}
						//End//
						
						$this->load->library('upload', $config);
						$this->upload->initialize($config);

						if($affiliate_logo!="" && (!$this->upload->do_upload('affiliate_logo')))
						{
							$affiliate_logoerror = $this->upload->display_errors();
						}
						if(isset($affiliate_logoerror))
						{
							$flag=1;
							$this->session->set_flashdata('affiliate_name',$affiliate_name);
							$this->session->set_flashdata('affiliate_desc',$affiliate_desc);
							$this->session->set_flashdata('logo_url',$logo_url);
							$this->session->set_flashdata('meta_keyword',$meta_keyword);
							$this->session->set_flashdata('meta_description',$meta_description);
							$this->session->set_flashdata('cashback_percentage',$cashback_percentage);
							$this->session->set_flashdata('error',$affiliate_logoerror);
							redirect('adminsettings/addaffiliate','refresh');
						}
					}

					//Pilaventhiran 03/05/2016 START
					if($cover_photo!="") 
					{
						$new_random 	= mt_rand(0,99999);
						$cover_photo 	= $_FILES['cover_photo']['name'];
						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($cover_photo);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $cover_photo);
						$cover_photo    = $newfilename."-".$new_random.'.'.$file_ex;
						/*End 3-10-16*/
						//$cover_photo 			 = remove_space($new_random.$cover_photo);
						$config['upload_path']   = 'uploads/affiliates';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['file_name']     = $cover_photo;
						
						//New code for AWS 24-9-16//
						if($copy_status == 1)
						{
							$filepath = 'uploads/affiliates/'.$cover_photo;
							$tmp 	  = $_FILES['cover_photo']['tmp_name'];
							$this->load->library('S3');
							$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
							$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
						}
						//End//

						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($cover_photo!="" && (!$this->upload->do_upload('cover_photo')))
						{
							$cover_photoerror = $this->upload->display_errors();
						}
						if(isset($cover_photoerror))
						{
							$flag=1;
							$this->session->set_flashdata('affiliate_name',$affiliate_name);
							$this->session->set_flashdata('affiliate_desc',$affiliate_desc);
							$this->session->set_flashdata('logo_url',$logo_url);
							$this->session->set_flashdata('meta_keyword',$meta_keyword);
							$this->session->set_flashdata('meta_description',$meta_description);
							$this->session->set_flashdata('cashback_percentage',$cashback_percentage);
							$this->session->set_flashdata('error',$cover_photoerror);
							redirect('adminsettings/addaffiliate','refresh');
						}
					}
					//Pilaventhiran 03/05/2016 END
					
					if($sidebar_image!="") 
					{
						$new_random = mt_rand(0,99999);
						$sidebar_image = $_FILES['sidebar_image']['name'];

						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($sidebar_image);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $sidebar_image);
						$sidebar_image  = $newfilename."-".$new_random.'.'.$file_ex;
						/*End 3-10-16*/
						//$sidebar_image = remove_space($new_random.$sidebar_image);
						$config['upload_path'] ='uploads/sidebar_image';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['file_name']=$sidebar_image;
						

						//New code for AWS 24-9-16//
						if($copy_status == 1)
						{
							$filepath = 'uploads/sidebar_image/'.$sidebar_image;
							$tmp 	  = $_FILES['sidebar_image']['tmp_name'];
							$this->load->library('S3');
							$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
							$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
						}
						//End//


						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($sidebar_image!="" && (!$this->upload->do_upload('sidebar_image')))
						{
							$sidebar_imageerror = $this->upload->display_errors();
						}
						if(isset($sidebar_imageerror))
						{
							$flag=1;
							$this->session->set_flashdata('affiliate_name',$affiliate_name);
							$this->session->set_flashdata('affiliate_desc',$affiliate_desc);
							$this->session->set_flashdata('logo_url',$logo_url);
							$this->session->set_flashdata('meta_keyword',$meta_keyword);
							$this->session->set_flashdata('meta_description',$meta_description);
							$this->session->set_flashdata('cashback_percentage',$cashback_percentage);
							$this->session->set_flashdata('error',$sidebar_imageerror);
							redirect('adminsettings/editaffiliate/'.$affiliate_id,'refresh');
						}
					}
			
					if($flag==0)
					{
						$coupon_image = $_FILES['coupon_image']['name'];
						if($coupon_image[0])
						{
											
								$flag=0;
								// start..
								$name="coupon_image";
								foreach($_FILES[$name] as $key => $val)
								{
									for($i=0;$i<count($val);$i++)
									{
										unset($_FILES['coupon_image'][$key]);
										$_FILES['coupon_image'][$i][$key]=$val[$i];
									}
								}
								$files=$_FILES;
								$all_images = "";
								for($i=0;$i<count($files[$name]);$i++)
								{
									$_FILES['coupon_image1']=$files[$name][$i];
									$coupon_image 	  = $_FILES['coupon_image1']['name'];
									$ext 			  = pathinfo($coupon_image, PATHINFO_EXTENSION);
									$file_without_ext = pathinfo($coupon_image, PATHINFO_FILENAME);
									$image_new 		  = $this->admin_model->seoUrl($file_without_ext);
									$newimage 		  = $image_new.".".$ext;
									// $this->upload->do_upload("coupon_image1");
									$random_no 		  = mt_rand(100,99999);
									$coupon_img 	  = $newimage."-".$random_no;
									$coupon_img 	  = str_replace(" ","_",$coupon_img);
									//echo $coupon_img; exit;
									$config['upload_path']   = 'uploads/store_banner';
									$config['allowed_types'] = 'gif|jpg|jpeg|png';
									$config['file_name']     = $coupon_img;
									
									//New code for AWS 24-9-16//
									if($copy_status == 1)
									{
										$filepath = 'uploads/store_banner/'.$coupon_img;
										$tmp 	  = $_FILES['coupon_image1']['tmp_name'];
										$this->load->library('S3');
										$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
										$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
									}
									//End//
									
									$this->load->library('upload', $config);
									$this->upload->initialize($config);
									if($coupon_img!="" && (!$this->upload->do_upload('coupon_image1')))
									{
										$coupon_imageerror = $this->upload->display_errors();
									}
									$shoppingcoupon_id = $this->input->post("shoppingcoupon_id");
									// $this->upload->display_errors();
									if(isset($coupon_imageerror))
									{						
										$flag=1;
										$this->session->set_flashdata('error',$coupon_imageerror);
										redirect('adminsettings/editaffiliate/'.$affiliate_id,'refresh');
									}
									$all_images.= $coupon_img.',';
								}
								$coupon_img1 = rtrim($all_images,',');
								$coupon_img =  str_replace(" ","_",$coupon_img1);		
						}
						else
						{
							$flag=0;
							$coupon_img = $this->input->post('hidden_coupon_image');
						}
				/*	echo $coupon_img;
					exit;*/		   
						if($sidebar_image!="")			   
						{
							$results = $this->admin_model->addaffiliate($affiliate_logo,$coupon_img,$sidebar_image,$cover_photo,$report_date);
						}
						else
						{
							$results = $this->admin_model->addaffiliate($affiliate_logo,$coupon_img,$sidebar_image,$cover_photo,$report_date);
						}
						if($results){
							$this->session->set_flashdata('success', ' Store details added successfully.');
							redirect('adminsettings/affiliates','refresh');
						}
						else{
							$this->session->set_flashdata('error', 'Error occurred while adding affiliate.');
							redirect('adminsettings/addaffiliate','refresh');
						}
					}
				}
			}
			$data['action']='new';
			$this->load->view('adminsettings/addaffiliate',$data);
		}
	}
	
	// edit affiliate	
	function editaffiliate($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		} 
		else
		{
			$get_affiliate = $this->admin_model->get_affiliate($id);
			foreach($get_affiliate as $get){
				$data['affiliate_id'] = $get->affiliate_id;
				$data['affiliate_name'] = $get->affiliate_name;
				$data['affiliate_logo'] = $get->affiliate_logo;
				$data['logo_url'] = $get->logo_url;
				
				/*$data['site_url'] = $get->site_url;*/
				
				$data['affiliate_desc'] = $get->affiliate_desc;
				$data['meta_keyword'] = $get->meta_keyword;
				$data['meta_description'] = $get->meta_description;
				$data['affiliate_status'] = $get->affiliate_status;
				$data['cashback_percentage'] = $get->cashback_percentage;
				$data['featured'] = $get->featured;
				$data['store_of_week'] = $get->store_of_week;
				$data['store_categorys'] = $get->store_categorys;
				$data['affiliate_cashback_type'] = $get->affiliate_cashback_type;				
				$data['coupon_image'] = $get->coupon_image;
				$data['coupon_image'] = $get->coupon_image;
				$data['retailer_ban_url'] = $get->retailer_ban_url;				
				
				$data['how_to_get_this_offer'] = $get->how_to_get_this_offer;				
				$data['terms_and_conditions'] = $get->terms_and_conditions;
				$data['sidebar_image'] = $get->sidebar_image;
				$data['sidebar_image_url'] = $get->sidebar_image_url;
				//Pilaventhiran 03/05/2016 START
				$data['notify_desk'] = $get->notify_desk;
				$data['notify_mobile'] = $get->notify_mobile;
				$data['old_cashback'] = $get->old_cashback;
				$data['redir_notify'] = $get->redir_notify;
				$data['name_extra_param'] = $get->name_extra_param;
				$data['content_extra_param'] = $get->content_extra_param;
				$data['content_extra_param_android'] = $get->content_extra_param_android;
				$data['cashback_percent_android'] = $get->cashback_percent_android;
				$data['cashback_content_android'] = $get->cashback_content_android;
				$data['cover_photo'] = $get->cover_photo;
				$data['report_date'] = $get->report_date;
				$data['related_details'] = $get->related_details;
				//Pilaventhiran 03/05/2016 END
				/*New code for Turn and go to shop button extra tracking parameter 30-7-16*/
				$data['tracking_param'] = $get->tracking_param;
				/*End*/

				/*New code for extra contents in retailers page 14-10-16*/
				$data['ex_tracking_param']     = $get->extra_tracking_param;
				$data['coupon_track_param']    = $get->coupon_track_param;
				$data['coupon_ex_track_param'] = $get->coupon_ex_track_param;
				/*End*/
				$data['url_slug'] = $get->affiliate_url;

				/*New code for Add a API Coupons program Id 9-2-17*/
				$data['zanox_pgm_id']   = $get->zanox_pgm_id;
				$data['cityads_pgm_id'] = $get->cityads_pgm_id;
				$data['lamadee_pgm_id'] = $get->lamadee_pgm_id;
				$data['rakuten_pgm_id'] = $get->rakuten_pgm_id;
				$data['afilio_pgm_id']  = $get->afilio_pgm_id;
				/*End 9-2-17*/
				$data['zanox_offer_provider']  = $get->zanox_offer_provider;
				$data['miss_cash_amt_type']    = $get->miss_cash_amt_type;

				
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/addaffiliate',$data);		
		}
	}
	
	// update affiliate
	function updateaffiliate()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} 
		else
		{
			if($this->input->post('save'))
			{
				$flag=0;
				$affiliate_name = $this->input->post('affiliate_name');
				$affiliate_desc = $this->input->post('affiliate_desc');
				$cashback_percentage = $this->input->post('cashback_percentage');
				$logo_url = $this->input->post('logo_url');

				//Pilaventhiran 03/05/2016 START
				$notify_desk = $this->input->post('notify_desk');
				$old_cashback = $this->input->post('old_cashback');
				$redir_notify = $this->input->post('redir_notify');
				$name_extra_param = $this->input->post('name_extra_param');
				$content_extra_param = $this->input->post('content_extra_param');
				$cashback_percent_android = $this->input->post('cashback_percent_android');
				$cashback_content_android = $this->input->post('cashback_content_android');
				$report_date = $this->input->post('report_date');
				$cover_photo = $_FILES['cover_photo']['name'];
				//Pilaventhiran 03/05/2016 END
				
				/*$site_url = $this->input->post('site_url');*/
				$meta_keyword = $this->input->post('meta_keyword');
				$meta_description = $this->input->post('meta_description');
				$affiliate_logo = $_FILES['affiliate_logo']['name'];
				
				$sidebar_image = $_FILES['sidebar_image']['name'];
				$affiliate_id = $this->input->post('affiliate_id');
				
				/*New code for amazon s3 copy settings 26-12-16*/
				$amazon_s3_details = $this->admin_model->amazon_s3_settings();
				$copy_status       = $amazon_s3_details->copy_files_status;
				/*End*/
				
				$flag=0;
				if($affiliate_logo!="") 
				{
					$new_random = mt_rand(0,99999);
					$affiliate_logo = $_FILES['affiliate_logo']['name'];

					/*New code for image name field changes 3-10-16*/
					$info 	 	 	= new SplFileInfo($affiliate_logo);
					$file_ex 	 	= $info->getExtension();
					$newfilename 	= str_replace('.'.$file_ex,'', $affiliate_logo);
					//$affiliate_logo = $newfilename.$new_random.'.'.$file_ex;
					$affiliate_logo = $newfilename."-".$new_random.'.'.$file_ex;
					/*End 3-10-16*/

					//$affiliate_logo = remove_space($new_random.$affiliate_logo);
					$config['upload_path'] ='uploads/affiliates';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['file_name']=$affiliate_logo;
					

					//New code for AWS 24-9-16//
					if($copy_status == 1)
					{
						$filepath = 'uploads/affiliates/'.$affiliate_logo;
						$tmp 	  = $_FILES['affiliate_logo']['tmp_name'];
						$this->load->library('S3');
						$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
						$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
					}
					//End//



					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if($affiliate_logo!="" && (!$this->upload->do_upload('affiliate_logo')))
					{
						$affiliate_logoerror = $this->upload->display_errors();
					}
					if(isset($affiliate_logoerror))
					{
						$flag=1;
						$this->session->set_flashdata('affiliate_name',$affiliate_name);
						$this->session->set_flashdata('affiliate_desc',$affiliate_desc);
						$this->session->set_flashdata('logo_url',$logo_url);
						$this->session->set_flashdata('meta_keyword',$meta_keyword);
						$this->session->set_flashdata('meta_description',$meta_description);
						$this->session->set_flashdata('cashback_percentage',$cashback_percentage);
						$this->session->set_flashdata('error',$affiliate_logoerror);
						redirect('adminsettings/editaffiliate/'.$affiliate_id,'refresh');
					}
				}
				else 
				{
					$affiliate_logo = $this->input->post('hidden_img');
				}

				//Pilaventhiran 03/05/2016 START
				if($cover_photo!="") 
				{
					$new_random = mt_rand(0,99999);
					$cover_photo = $_FILES['cover_photo']['name'];

					/*New code for image name field changes 3-10-16*/
					$info 	 	 	= new SplFileInfo($cover_photo);
					$file_ex 	 	= $info->getExtension();
					$newfilename 	= str_replace('.'.$file_ex,'', $cover_photo);
					$cover_photo    = $newfilename."-".$new_random.'.'.$file_ex;
					/*End 3-10-16*/

					//$cover_photo = remove_space($new_random.$cover_photo);
					$config['upload_path'] ='uploads/affiliates';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['file_name']=$cover_photo;
					

					//New code for AWS 24-9-16//
					if($copy_status == 1)
					{
						$filepath = 'uploads/affiliates/'.$cover_photo;
						$tmp 	  = $_FILES['cover_photo']['tmp_name'];
						$this->load->library('S3');
						$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
						$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
					}
					//End//


					$this->load->library('upload', $config);
					$this->upload->initialize($config);


					if($cover_photo!="" && (!$this->upload->do_upload('cover_photo')))
					{
						$cover_photoerror = $this->upload->display_errors();
					}
					if(isset($cover_photoerror))
					{
						$flag=1;
						$this->session->set_flashdata('affiliate_name',$affiliate_name);
						$this->session->set_flashdata('affiliate_desc',$affiliate_desc);
						$this->session->set_flashdata('logo_url',$logo_url);
						$this->session->set_flashdata('meta_keyword',$meta_keyword);
						$this->session->set_flashdata('meta_description',$meta_description);
						$this->session->set_flashdata('cashback_percentage',$cashback_percentage);
						$this->session->set_flashdata('error',$cover_photoerror);
						redirect('adminsettings/editaffiliate/'.$affiliate_id,'refresh');
					}
				}
				else 
				{
					$cover_photo = $this->input->post('hidden__cover_photo');
				}

				//Pilaventhiran 03/05/2016
				if($sidebar_image!="") 
				{
					$new_random = mt_rand(0,99999);
					$sidebar_image = $_FILES['sidebar_image']['name'];

					/*New code for image name field changes 3-10-16*/
					$info 	 	 	= new SplFileInfo($sidebar_image);
					$file_ex 	 	= $info->getExtension();
					$newfilename 	= str_replace('.'.$file_ex,'', $sidebar_image);
					$sidebar_image  = $newfilename."-".$new_random.'.'.$file_ex;
					/*End 3-10-16*/

					//$sidebar_image = remove_space($new_random.$sidebar_image);
					$config['upload_path'] ='uploads/sidebar_image';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['file_name']=$sidebar_image;
					

					//New code for AWS 24-9-16//
					if($copy_status == 1)
					{
						$filepath = 'uploads/sidebar_image/'.$sidebar_image;
						$tmp 	  = $_FILES['sidebar_image']['tmp_name'];
						$this->load->library('S3');
						$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
						$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
					}
					//End//



					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if($sidebar_image!="" && (!$this->upload->do_upload('sidebar_image')))
					{
						$sidebar_imageerror = $this->upload->display_errors();
					}
					if(isset($sidebar_imageerror))
					{
						$flag=1;
						$this->session->set_flashdata('affiliate_name',$affiliate_name);
						$this->session->set_flashdata('affiliate_desc',$affiliate_desc);
						$this->session->set_flashdata('logo_url',$logo_url);
						$this->session->set_flashdata('meta_keyword',$meta_keyword);
						$this->session->set_flashdata('meta_description',$meta_description);
						$this->session->set_flashdata('cashback_percentage',$cashback_percentage);
						$this->session->set_flashdata('error',$sidebar_imageerror);
						redirect('adminsettings/editaffiliate/'.$affiliate_id,'refresh');
					}
				}
				else 
				{
					$sidebar_image = $this->input->post('sidebar_image_hid');
				}
				if($flag==0)
				{
					$coupon_image='';
					$coupon_image = $_FILES['coupon_image']['name'];
					if($coupon_image[0])
					{				
						$flag=0;
						// start..
						$name="coupon_image";
						foreach($_FILES[$name] as $key => $val)
						{
							for($i=0;$i<count($val);$i++)
							{
								unset($_FILES['coupon_image'][$key]);
								$_FILES['coupon_image'][$i][$key]=$val[$i];
							}
						}
						$files=$_FILES;
						$all_images = "";
						for($i=0;$i<count($files[$name]);$i++)
						{
							$_FILES['coupon_image1']=$files[$name][$i];
							$coupon_image = $_FILES['coupon_image1']['name'];
							
							$ext = pathinfo($coupon_image, PATHINFO_EXTENSION);
							$file_without_ext = pathinfo($coupon_image, PATHINFO_FILENAME);;
							
							$image_new =  $this->admin_model->seoUrl($file_without_ext);
							$newimage =  $image_new.".".$ext;
			
			
							// $this->upload->do_upload("coupon_image1");
							$random_no = mt_rand(100,99999);
							//$coupon_img = $random_no.$newimage;
							$coupon_img = $newimage."-".$random_no;
							$coupon_img =  str_replace(" ","_",$coupon_img);
							$config['upload_path'] ='uploads/store_banner';
							$config['allowed_types'] = 'gif|jpg|jpeg|png';
							$config['file_name']=$coupon_img;
							

							//New code for AWS 24-9-16//
							if($copy_status == 1)
							{
								$filepath = 'uploads/store_banner/'.$coupon_img;
								$tmp 	  = $_FILES['coupon_image1']['tmp_name'];
								$this->load->library('S3');
								$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
								$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
							}
							//End//
							$this->load->library('upload', $config);
							$this->upload->initialize($config);
							if($coupon_img!="" && (!$this->upload->do_upload('coupon_image1')))
							{
								$coupon_imageerror = $this->upload->display_errors();
							}
							$shoppingcoupon_id = $this->input->post("shoppingcoupon_id");
							// $this->upload->display_errors();
							if(isset($coupon_imageerror))
							{						
								$flag=1;
								$this->session->set_flashdata('error',$coupon_imageerror);
								redirect('adminsettings/editaffiliate/'.$affiliate_id,'refresh');
							}
							$all_images.= $coupon_img.',';
						}
						$coupon_img1 = rtrim($all_images,',');
						$coupon_img =  str_replace(" ","_",$coupon_img1);
						/*echo $coupon_img;
						exit;*/
					}
					else
					{
						$flag=0;
						$coupon_img = $this->input->post('hidden_coupon_image');
					}				
					$results = $this->admin_model->updateaffiliate($affiliate_logo,$coupon_img,$sidebar_image,$cover_photo);
					
					if($results){
						$this->session->set_flashdata('success', ' Store details updated successfully.');
						redirect('adminsettings/affiliates','refresh');
					}
					else{
						$this->session->set_flashdata('error', 'Error occurred while updating store details.');
						redirect('adminsettings/addaffiliate','refresh');
					}
				}
			}	
		}
	}	
	
	// delete affiliate
	function deleteaffiliate($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deleteaffiliate($id);
			if($deletion){
				$data['action']="Edit";
				$this->session->set_flashdata('success', ' Store details deleted successfully.');
				redirect('adminsettings/affiliates','refresh');
			}
			else{
				$data['action']="Edit";
				$this->session->set_flashdata('error', 'Error occurred while deleting store details.');
				redirect('adminsettings/affiliates','refresh');
			}
		}
	}
	
	// view all banners
	function banners()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
		    if($this->input->post('hidd'))
			{	
				$results = $this->admin_model->multi_delete_banners();
				if($results)
				{		
					$this->session->set_flashdata('success', 'Banners deleted successfully.');
					redirect('adminsettings/banners','refresh');
				}
				else
				{
				$this->session->set_flashdata('error', 'Error occurred while deleted Banners details.');
				redirect('adminsettings/banners','refresh');
				}
			}
			$data['banners'] = $this->admin_model->banners();
			$this->load->view('adminsettings/banners',$data);
		}	
	}
	
	// add new banner
	function addbanner()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			if($this->input->post('save'))
			{
				$flag=0;
				$banner_name = $this->input->post('banner_name');
				$banner_image = $_FILES['banner_image']['name'];
				if($banner_image=="")
				{
					$flag=1;
					$this->session->set_flashdata('banner_name',$banner_name);
					$this->session->set_flashdata('error', 'Please upload an image .');
					redirect('adminsettings/addbanner','refresh');
				}				
				else
				{
					/*New code for amazon s3 copy settings 26-12-16*/
					$amazon_s3_details = $this->admin_model->amazon_s3_settings();
					$copy_status       = $amazon_s3_details->copy_files_status;
					/*End*/

					$flag=0;
					if($banner_image!="") 
					{
						$new_random 		     = mt_rand(0,99999);
						$banner_image 		     = $_FILES['banner_image']['name'];

						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($banner_image);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $banner_image);
						$banner_image   = $newfilename.$new_random.'.'.$file_ex;
						/*End 3-10-16*/

						//$banner_image 		     = remove_space($new_random.$banner_image);
						$config['upload_path']   = 'uploads/banners';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['file_name'] 	 = $banner_image;


						//New code for AWS 24-9-16//
						if($copy_status == 1)
						{
							$filepath = 'uploads/banners/'.$banner_image;
							$tmp 	  = $_FILES['banner_image']['tmp_name'];
							$this->load->library('S3');
							$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
							$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
						}
						//End//

						
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($banner_image!="" && (!$this->upload->do_upload('banner_image')))
						{
							$banner_imageerror = $this->upload->display_errors();
						}
						if(isset($banner_imageerror))
						{
							$flag=1;
							$this->session->set_flashdata('banner_name',$banner_name);
							$this->session->set_flashdata('error',$banner_imageerror);
							redirect('adminsettings/addbanner','refresh');
						}
					}
					if($flag==0)
					{
						$results = $this->admin_model->addbanner($banner_image);
						if($results){
							$this->session->set_flashdata('success', ' Banner details added successfully.');
							redirect('adminsettings/banners','refresh');
						}
						else{
							$this->session->set_flashdata('error', 'Error occurred while adding banner.');
							redirect('adminsettings/addbanner','refresh');
						}
					}
				}
			}	
			$data['action']='new';
			$this->load->view('adminsettings/addbanner',$data);
		}
	}	
	
	// edit banner
	function editbanner($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$get_banner = $this->admin_model->get_banner($id);
			foreach($get_banner as $get){
				$data['banner_id'] = $get->banner_id;
				$data['banner_name'] = $get->banner_heading;
				$data['banner_image'] = $get->banner_image;
				$data['banner_status'] = $get->banner_status;
				$data['banner_position'] = $get->banner_position;
				$data['banner_url'] = $get->banner_url;
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/addbanner',$data);
		}
	}
	
	//update banner
	function updatebanner()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{		
			if($this->input->post('save'))
			{
				$flag=0;
				$banner_name = $this->input->post('banner_name');
				$banner_image = $_FILES['banner_image']['name'];

				/*New code for amazon s3 copy settings 26-12-16*/
				$amazon_s3_details = $this->admin_model->amazon_s3_settings();
				$copy_status       = $amazon_s3_details->copy_files_status;
				/*End*/

				if($banner_image!="") 
				{
					$new_random 		     = mt_rand(0,99999);
					$banner_image 		     = $_FILES['banner_image']['name'];
					//$banner_image 	     = $new_random.$banner_image;

					/*New code for image name field changes 3-10-16*/
					$info 	 	 	= new SplFileInfo($banner_image);
					$file_ex 	 	= $info->getExtension();
					$newfilename 	= str_replace('.'.$file_ex,'', $banner_image);
					$banner_image 	= $newfilename.$new_random.'.'.$file_ex;
					/*End 3-10-16*/

					//$banner_image 	         = remove_space($new_random.$banner_image);
					$config['upload_path']   = 'uploads/banners';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['file_name'] 	 = $banner_image;


					//New code for AWS 24-9-16//
					if($copy_status == 1)
					{
						$filepath = 'uploads/banners/'.$banner_image;
						$tmp 	  = $_FILES['banner_image']['tmp_name'];
						$this->load->library('S3');
						$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
						$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
					}
					//End//

					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if($banner_image!="" && (!$this->upload->do_upload('banner_image')))
					{
						$banner_imageerror = $this->upload->display_errors();
					}
					if(isset($banner_imageerror))
					{
						$flag=1;
						$this->session->set_flashdata('banner_name',$banner_name);
						$this->session->set_flashdata('error',$banner_imageerror);
						redirect('adminsettings/addbanner','refresh');
					}
				}
				else 
				{
					$flag=0;
					$banner_image = $this->input->post('hidden_img');
				}
				if($flag==0)
				{
					$results = $this->admin_model->updatebanner($banner_image);
					if($results){
						$this->session->set_flashdata('success', ' Banner details updated successfully.');
						redirect('adminsettings/banners','refresh');
					}
					else{
						$this->session->set_flashdata('error', 'Error occurred while updating banner.');
						redirect('adminsettings/addbanner','refresh');
					}
				}
			}	
			$data['action']='new';
			$this->load->view('adminsettings/addbanner',$data);
		}
	}
	// delete banner
	function deletebanner($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deletebanner($id);
			if($deletion){
				$data['action']="Edit";
				$this->session->set_flashdata('success', ' Banner details deleted successfully.');
				redirect('adminsettings/banners','refresh');
			}
			else{
				$data['action']="Edit";
				$this->session->set_flashdata('error', 'Error occurred while deleting banner details.');
				redirect('adminsettings/banners','refresh');
			}
		}
	}
	
	// view all subscribers..
	function subscribers()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				 if($this->input->post('chkbox'))
				 {
					
					$sort_order = $this->input->post('chkbox');					 
					 $results = $this->admin_model->delete_bulk_records($sort_order,'subscribers','subscriber_id');
				 }	
				if($results){
					
					$this->session->set_flashdata('success', 'Subscribers details deleted successfully.');
					redirect('adminsettings/subscribers','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while updating Subscribers details.');
					redirect('adminsettings/subscribers','refresh');
				}
			}
			$data['subscribers'] = $this->admin_model->subscribers();
			$this->load->view('adminsettings/subscribers',$data);
		}	
	}
	
	
	// delete subscriber..
	function deletesubscriber($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deletesubscriber($id);
			if($deletion){
				$data['action']="Edit";
				$this->session->set_flashdata('success', ' Subscriber email deleted successfully.');
				redirect('adminsettings/subscribers','refresh');
			}
			else{
				$data['action']="Edit";
				$this->session->set_flashdata('error', 'Error occurred while deleting subscriber email.');
				redirect('adminsettings/subscribers','refresh');
			}
		}		
	}
	
	// compose new msg..
	function compose_newsletter(){
		$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{	
			$data['action'] = "new";
			$this->load->view('adminsettings/compose_email',$data);
		}
	}
	
	// send newsletter mail..
	function send_newsletter()
	{
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				$to = $this->input->post('to');
				$message = $this->input->post('message');
				$subject = $this->input->post('subject');
				if($message==""){
					$this->session->set_flashdata('subject', $subject);
					$this->session->set_flashdata('error', 'Please enter some message.');
					redirect('adminsettings/compose_newsletter','refresh');
				}
				else { 
					$results = $this->admin_model->send_mail();
					if($results) {
						$this->session->set_flashdata('success', ' Mail Sent successfully.');
						redirect('adminsettings/subscribers','refresh');
					}
					else {
						$this->session->set_flashdata('error', 'Error occurred while sending mail.');
						redirect('adminsettings/compose_newsletter','refresh');
					}
				}
			}
		} 
	}
	
	// edit email template
	function email_template($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if(($admin_id=="") || ($id=="")){
			redirect('adminsettings/index','refresh');
		} else
		{
			$get_email_template = $this->admin_model->get_email_template($id);
			foreach($get_email_template as $get){
				$data['mail_id'] = $get->mail_id;
				$data['email_subject'] = $get->email_subject;
				$data['email_template'] = $get->email_template;
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/mailtemplates',$data);
		}
	}
	
	// update email template
	function update_email_template()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				$email_subject = $this->input->post('email_subject');
				$email_template = $this->input->post('email_template');
				$mail_id = $this->input->post('mail_id');
				if($email_template==""){
					$this->session->set_flashdata('email_subject', $email_subject);
					$this->session->set_flashdata('mail_id', $mail_id);
					$this->session->set_flashdata('error', 'Please enter some message.');
					redirect('adminsettings/email_template/'.$mail_id,'refresh');
				}
				else { 
					$results = $this->admin_model->update_email_template();
					if($results) {
						$this->session->set_flashdata('success', ' Email Template updated successfully.');
						redirect('adminsettings/email_template/'.$mail_id,'refresh');
					}
					else {
						$this->session->set_flashdata('error', 'Error occurred while updating template.');
						redirect('adminsettings/email_template/'.$mail_id,'refresh');
					}
				}
			}
		}
	}
	
	// referrals..
	function referrals()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				 if($this->input->post('chkbox'))
				 {
					
					$sort_order = $this->input->post('chkbox');
								 
					 $results = $this->admin_model->delete_bulk_records($sort_order,'referrals','referral_id');
				 }
				
				
				if($results){
					
					$this->session->set_flashdata('success', 'Referrals details Deleted successfully.');
					redirect('adminsettings/referrals','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while updating Referrals details.');
					redirect('adminsettings/referrals','refresh');
				}
			}
			$data['referrals'] = $this->admin_model->referrals();
			$this->load->view('adminsettings/referrals',$data);
		}
	}
	
	// delete referral by user id..	
	function deletereferral($user_id)
	{
		$this->input->session_helper();
	// $user_id -> is a user who referred the other users..
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
		$deletion = $this->admin_model->deletereferral($user_id);
		if($deletion){
				$this->session->set_flashdata('success', ' Referral details deleted successfully.');
				redirect('adminsettings/referrals','refresh');
			}
			else{
				$data['action']="Edit";
				$this->session->set_flashdata('error', 'Error occurred while deleting referral details.');
				redirect('adminsettings/referrals','refresh');
			}
		}
	}
	
	// view all coupons..	
	function coupons($store_name=null)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}else
		{
			if($this->input->post('hidd'))
			{
				if($this->input->post('chkbox'))
				{
					$results = $this->admin_model->coupons_bulk_delete();
				}
				if($results)
				{
					$this->session->set_flashdata('success', 'Coupons details Deleted successfully.');
					redirect('adminsettings/coupons','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while deleting Coupon details.');
					redirect('adminsettings/coupons','refresh');
				}
			}
			//$data['coupons'] = $this->admin_model->coupons($store_name);
			$this->load->view('adminsettings/coupons',$data);
		}
	}	
	
	// upload bulk coupons
	function bulkcoupon(){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$flag=0;
			if($this->input->post('save')){
				$bulkcoupon = $_FILES['bulkcoupon']['name'];
				if($bulkcoupon==""){
					$flag=1;
					$this->session->set_flashdata('error', 'Please upload the file.');
					redirect('adminsettings/bulkcoupon','refresh');
				}
				else {
					$flag=0;
					if($bulkcoupon!="") {
						$new_random = mt_rand(0,99999);
						$bulkcoupon = $_FILES['bulkcoupon']['name'];

						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($bulkcoupon);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $bulkcoupon);
						$bulkcoupon     = $newfilename.$new_random.'.'.$file_ex;
						/*End 3-10-16*/

						//$bulkcoupon = remove_space($new_random.$bulkcoupon);
						$config['upload_path'] ='uploads/coupon';
						$config['allowed_types'] = '*';
						$config['file_name']=$bulkcoupon;
						
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($bulkcoupon!="" && (!$this->upload->do_upload('bulkcoupon')))
						{
							$bulkcouponerror = $this->upload->display_errors();
						}
						if(isset($bulkcouponerror))
						{
							$flag=1;
							$this->session->set_flashdata('error',$bulkcouponerror);
							redirect('adminsettings/bulkcoupon','refresh');
						}
					}
					if($flag==0){
						$results = $this->admin_model->bulkcoupon($bulkcoupon);
						if($results){
							$this->session->set_flashdata('success', ' Coupon details added successfully.');
							redirect('adminsettings/coupons','refresh');
						}
						else{
							$this->session->set_flashdata('error', 'Error occurred while adding coupon details.');
							redirect('adminsettings/bulkcoupon','refresh');
						}
					}
				}
			$result = $this->admin_model->bulkcoupon();
			if($result){
					$this->session->set_flashdata('success', ' Coupon details added successfully.');
					redirect('adminsettings/coupons','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while adding coupon details.');
					redirect('adminsettings/bulkcoupon','refresh');
				}
			}
			$data['action'] = "new";
			$this->load->view('adminsettings/bulkcoupon',$data);
		}	
	}
	
	// add single coupon
	function addcoupon()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{	
			if($this->input->post('save'))
			{
				$insert = $this->admin_model->addcoupon();
				if($insert)
				{
					$this->session->set_flashdata('success', ' Coupon details added successfully.');
					redirect('adminsettings/coupons','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while adding coupon details.');
					redirect('adminsettings/addcoupon','refresh');
				}
			}
			$data['action'] = "new";
			$this->load->view('adminsettings/addcoupon',$data);
		}	
	}
	
	// view particular coupon details..
	function editcoupon($coupon_id,$api_name)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/dashboard','refresh');
		} else 
		{
			$admindetails = $this->admin_model->get_admindetails();
			$coupons 	  = $this->admin_model->editcoupon($coupon_id);
			foreach($coupons as $get)
			{
				$data['coupon_id']     = $get->coupon_id;
				$data['offer_name']    = $get->offer_name;
				$data['title']         = $get->title;
				$data['description']   = $get->description;
				$data['type'] 		   = $get->type;
				$data['code'] 		   = $get->code;
				$data['offer_page']    = $get->offer_page;
				$data['expiry_date']   = date('d-m-Y',strtotime($get->expiry_date));
				$data['start_date']    =  date('d-m-Y',strtotime($get->start_date));
				$data['url_link_type'] = $get->url_link_type;	
				$data['deep_url'] 	   = $get->deeplink_url;	 
				$data['ex_tracking']   = $get->extra_tracking_param;
				$data['Tracking'] 	   = $get->Tracking;	
				$data['coupon_options']= $get->coupon_options;	
				$data['cashback_description'] = $get->cashback_description;	

			}
			$data['api_name']    	  = $api_name;
			$data['admindetails'] 	  = $admindetails;
			$data['action']      	  = "edit";
			$this->load->view('adminsettings/addcoupon',$data);			
		}
	}
	
	// update coupon..
	function updatecoupon()
	{
		//print_r($_POST); exit;
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save'))
			{
				$updated = $this->admin_model->updatecoupon();
				if($updated)
				{
					$data['action']="Edit";
					$this->session->set_flashdata('success', ' Coupon details updated successfully.');
					redirect('adminsettings/coupons','refresh');
				}
				else
				{
					$data['action']="Edit";
					$this->session->set_flashdata('error', 'Error occurred while updating coupon details.');
					redirect('adminsettings/coupons','refresh');
				}
			}
			
			if($this->input->post('save_coupon'))
			{
				$api_name = $this->input->post('api_name');
				$updated  = $this->admin_model->updatecoupon();
				if($updated)
				{
					$data['action']="Edit";
					$this->session->set_flashdata('success', ' Coupon details updated successfully.');
					redirect('adminsettings/affiliate_network/'.$api_name,'refresh');
				}
				else
				{
					$data['action']="Edit";
					$this->session->set_flashdata('error', 'Error occurred while updating coupon details.');
					redirect('adminsettings/affiliate_network/'.$api_name,'refresh');
				}
			}

			if($this->input->post('approve_coupon'))
			{
				$api_name = $this->input->post('api_name');
				$updated  = $this->admin_model->update_coupon($api_name);
				if($updated)
				{
					$data['action']="Edit";
					$this->session->set_flashdata('success', ' Coupon details updated successfully.');
					redirect('adminsettings/affiliate_network/'.$api_name,'refresh');
				}
				else
				{
					$data['action']="Edit";
					$this->session->set_flashdata('error', 'Error occurred while updating coupon details.');
					redirect('adminsettings/affiliate_network/'.$api_name,'refresh');
				}
			}
			 
			
		}
	}
	
	// delete coupon..
	function deletecoupon($delete_id)
	{
		$this->input->session_helper();
		
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deletecoupon($delete_id);
			if($deletion){
				$this->session->set_flashdata('success', 'Coupon deleted successfully.');
				redirect('adminsettings/coupons','refresh');
			}
			else{
				$this->session->set_flashdata('error', 'Error occurred while deleting coupon.');
				redirect('adminsettings/coupons','refresh');
			}
		}
	}
	
	//  view all shopping coupons..
	function shoppingcoupons($store_name)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				 if($this->input->post('chkbox'))
				 {
					 $results = $this->admin_model->bulk_shopping_delete();
				 }
				 	
				if($results){					
					$this->session->set_flashdata('success', 'Shopping Coupons details deteted successfully.');
					redirect('adminsettings/shoppingcoupons','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while updating Shopping Coupons details.');
					redirect('adminsettings/shoppingcoupons','refresh');
				}
			}
			$data['shoppingcoupons'] = $this->admin_model->shoppingcoupons($store_name);
			$this->load->view('adminsettings/shoppingcoupons',$data);
		}
	}
	
	//  view all shopping coupons..
	function expired_coupons($store_name)
	{	
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		} 
		else
		{
			if($this->input->post('hidd'))
			{
				 if($this->input->post('chkbox'))
				 {
					 $results = $this->admin_model->bulk_shopping_delete();
				 }
				if($results)
				{		
					$this->session->set_flashdata('success', 'Premium Shopping Coupons details deteted successfully.');
					redirect('adminsettings/expired_coupons','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while updating Shopping Coupons details.');
					redirect('adminsettings/expired_coupons','refresh');
				}
			}
			$data['shoppingcoupons'] = $this->admin_model->exp_shoppingcoupons($store_name);
			$this->load->view('adminsettings/expired_coupons',$data);
		}
	}
	
	//download coupons
	function download_coupons($status)
	{
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			$this->load->helper('csv_helper');
			$result =$this->admin_model->select_table($status);
				ob_end_clean();
				$t_date=date('Y-m-d');
					$filename="active_coupons-".$t_date.".xls";
				
				 $test=" offer Name  \t About \t In a nutshel \t The Fine print \t company \t  Location \t Coupon codes \t start_date \t expiry_date";
				   $test.="\n";
				  if(isset($result)){
					  $k=1;
					  foreach($result as $row)
					  {
						   $offer_name=$row->offer_name;
						   //$description = $row->description;
						   $about = $row->about;
						   $nutshel = $row->nutshel;
						   $fine_print = $row->fine_print;
						   $company = $row->company;
						   $location=$row->location;						   
						   $coupon_code = $row->coupon_code;
						  // $remain_coupon_code = $row->remain_coupon_code;
						   $start_date = $row->start_date;
   						   $expiry_date = $row->expiry_date;						   
						   $test.=$offer_name."\t".$about."\t".$nutshel."\t".$fine_print."\t".$company."\t".$location."\t".$coupon_code."\t".$start_date."\t".$expiry_date;
						   $test.="\n";
						  
					   } 
				   }
				 print_r($test);  
				   
				header("Content-type: application/csv");
				//header("Content-type: application/vnd.ms-word");
				//header("Content-type: text/plain");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 2");
				$this->load->helper('file');
				write_file('./backup/'.$filename, $test);
				$data = file_get_contents("./backup/".$filename);
				$urfile=$status."_coupons-".$t_date.".csv";
				$this->load->helper('download');
				force_download($urfile, $data); 
			}
	}
	
	// update shopping coupon..
	function update_shoppingcoupon()
	{
		$this->input->session_helper();
		$coupon_image = '';
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			if($this->input->post('save'))
			{
				$img_type = $this->input->post('img_types');
				/*New code for amazon s3 copy settings 26-12-16*/
				$amazon_s3_details = $this->admin_model->amazon_s3_settings();
				$copy_status       = $amazon_s3_details->copy_files_status;
				/*End*/

				
				/*if($coupon_image[0])
				{
					$flag=0;
					$random_no = mt_rand(0,99999);
					$coupon_img = $random_no.$coupon_image;
					$config['upload_path'] ='uploads/premium';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['file_name']=$coupon_img;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					if($coupon_image!="" && (!$this->upload->do_upload('coupon_image')))
					{
						$coupon_imageerror = $this->upload->display_errors();
					}
					if(isset($coupon_imageerror))
					{
						$flag=1;
						echo $this->session->set_flashdata('error',$coupon_imageerror);
						//print_r($coupon_image);
					exit;
						redirect('adminsettings/add_shoppingcoupon','refresh');
					}
				}
				*/
				if($img_type == 'url_image')
				{
					$coupon_img = $this->input->post('coupon_image');
				}
				else
				{
					$coupon_image = $_FILES['coupon_image']['name'];
					if($coupon_image[0])
					{
						$flag=0;
						// start..
						$name="coupon_image";
						foreach($_FILES[$name] as $key => $val)
						{
							for($i=0;$i<count($val);$i++)
							{
								unset($_FILES['coupon_image'][$key]);
								$_FILES['coupon_image'][$i][$key]=$val[$i];
							}
						}
						$files=$_FILES;
						$all_images = "";
						for($i=0;$i<count($files[$name]);$i++)
						{
							$_FILES['coupon_image1'] = $files[$name][$i];
							$coupon_image 			 = $_FILES['coupon_image1']['name'];
							$ext 					 = pathinfo($coupon_image, PATHINFO_EXTENSION);
							$file_without_ext 		 = pathinfo($coupon_image, PATHINFO_FILENAME);;
							$image_new 				 =  $this->admin_model->seoUrl($file_without_ext);
							$newimage 				 =  $image_new.".".$ext;
							$random_no 				 =  mt_rand(100,99999);
							$coupon_img 			 =  $random_no.$newimage;
							$config['upload_path']   =	'uploads/premium';
							$config['allowed_types'] = 'gif|jpg|jpeg|png';
							$config['file_name']	 = $coupon_img;

							//New code for AWS 26-9-16//
							if($copy_status == 1)
							{
								$filepath = 'uploads/premium/'.$coupon_img;
								$tmp 	  = $_FILES['coupon_image1']['tmp_name'];
								$this->load->library('S3');
								$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
								$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
							}
							//End//

							$this->load->library('upload', $config);
							$this->upload->initialize($config);
							if($coupon_img!="" && (!$this->upload->do_upload('coupon_image1')))
							{
								$coupon_imageerror = $this->upload->display_errors();
							}
							$shoppingcoupon_id = $this->input->post("shoppingcoupon_id");
							// $this->upload->display_errors();
							if(isset($coupon_imageerror))
							{
								$shoppingcoupon_id = $this->input->post("shoppingcoupon_id");
								$flag=1;
								$this->session->set_flashdata('error',$coupon_imageerror);
								redirect('adminsettings/edit_shoppingcoupon/'.$shoppingcoupon_id,'refresh');
							}
							$all_images.= $coupon_img.',';
						}
						$coupon_img1 = rtrim($all_images,',');
						$coupon_img =  str_replace(" ","_",$coupon_img1);
						/*echo $coupon_img;
						exit;*/
					}
					else
					{
						$flag=0;
						$coupon_img = $this->input->post('hidden_coupon_image');
					}
				}
				
				if($flag==0)
				{
					/*echo $coupon_img;
					exit;*/
					$updated = $this->admin_model->update_shoppingcoupon($coupon_img);
					if($updated){
						$data['action']="Edit";
						$this->session->set_flashdata('success', ' Shopping coupon details updated successfully.');
						redirect('adminsettings/shoppingcoupons','refresh');
					}
					else{
						$data['action']="Edit";
						$this->session->set_flashdata('error', 'Error occurred while updating shopping coupon details.');
						redirect('adminsettings/shoppingcoupons','refresh');
					}
				}	
			}
		}
	}
	
	// delete shopping coupon..
	function delete_shoppingcoupon($delete_id){
	$this->input->session_helper();
	
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->delete_shoppingcoupon($delete_id);
			if($deletion){
				$this->session->set_flashdata('success', 'Shopping Coupon deleted successfully.');
				redirect('adminsettings/shoppingcoupons','refresh');
			}
			else{
				$this->session->set_flashdata('error', 'Error occurred while deleting shopping coupon.');
				redirect('adminsettings/shoppingcoupons','refresh');
			}
		}
	}
	
	
	// delete Exp shopping coupon..
	function delete_exp_shoppingcoupon($delete_id){
	$this->input->session_helper();	
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->delete_shoppingcoupon($delete_id);
			if($deletion){
				$this->session->set_flashdata('success', 'Shopping Coupon deleted successfully.');
				redirect('adminsettings/expired_coupons','refresh');
			}
			else{
				$this->session->set_flashdata('error', 'Error occurred while deleting shopping coupon.');
				redirect('adminsettings/expired_coupons','refresh');
			}
		}
	}
	
	// view all cashback
	function cashback($name,$user_id,$retailer_name)
	{
		//echo $name; exit;
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/dashboard','refresh');
		}
		else 
		{
			if($name == 'user')
			{
				if($name !='' && $user_id!='')
				{ 
					$data['user_detail'] 	  = $this->admin_model->view_user($user_id);
					
					if($data['user_detail'])
					{
						$this->load->view('adminsettings/viewuser',$data);	
					}
					else
					{
						redirect('adminsettings/index','refresh');
					}
				}
			}
			else
			{
				//echo $retailer_name; exit;
				if($this->input->post('hidd'))   //delete multiple cashback  seetha
				{
					if($this->input->post('chkbox'))
					{
						$sort_order = $this->input->post('chkbox');					 
						$results = $this->admin_model->delete_bulk_records($sort_order,'cashback','cashback_id');
					}				
					if($results)
					{
						$this->session->set_flashdata('success', 'Cashback details deleted successfully.');
						redirect('adminsettings/cashback','refresh');
					}
					else
					{
						$this->session->set_flashdata('error', 'Error occurred while updating cashback details.');
						redirect('adminsettings/cashback','refresh');
					}
				}	
				//$data['cashbacks'] = $this->admin_model->cashback($retailer_name);
				$this->load->view('adminsettings/cashback',$data);
			}	
		}
	}


	
	// view cashback details
	function editcashback($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/dashboard','refresh');
		} else {
			$cashback = $this->admin_model->cashback_details($id);
			//print_r($cashback);die;
			if($cashback){
				foreach($cashback as $get){				
					$data['store'] 	     = $get->affiliate_id;
					$data['coupon_id']   = $get->coupon_id;
					$data['user_id']     = $get->user_id;
					$data['status']      = $get->status;
					$data['cashback_id'] = $get->cashback_id;	
					$data['cashback'] 	 = $get->cashback_amount;
					
					$username  			 = $this->admin_model->user_names($get->user_id); 
					$user_name 			 = $username->first_name." ".$username->last_name;

					$data['user_name'] 		  = $user_name;
					$data['transaction_amt']  = $get->transaction_amount;
					$data['transaction_date'] = $get->date_added;
					$data['report_update_id'] = $get->report_update_id;
					$data['update_date']  	  = $get->transaction_date;
					$data['referral']  		  = $get->referral;
					$data['txn_id']  		  = $get->txn_id;
					$data['new_txn_id'] 	  = $get->new_txn_id;
					$data['platform']  	  	  = $get->plataform;
					$data['commission']  	  = $get->commission;
					$data['calc_cb']  		  = $get->calc_cb;
					$data['type_cb'] 	 	  = $get->type_cb;




				}//die;
				$data['action']='new';
				$this->load->view('adminsettings/editcashback',$data);
			} else {
				redirect('adminsettings/cashback','refresh');
			}
		}	
	}
	
	// update cashback status..
	function updatecashback()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				$id = $this->input->post('cashback_id');
				$updation = $this->admin_model->updatecashback($id);
				if($updation){
					$this->session->set_flashdata('success', 'Cashback status updated successfully.');
					redirect('adminsettings/cashback','refresh');
				} else {
					$data['action']="new";
					$this->session->set_flashdata('error', 'Error occurred while updating cashback status.');
					redirect('adminsettings/editcashback/'.$id,'refresh');
				}
			}
		}
	}
	
	// delete cashback details..
	function deletecashback($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deletecashback($id);
			if($deletion){
				$this->session->set_flashdata('success', 'Cashback details deleted successfully.');
				redirect('adminsettings/cashback','refresh');
			}
			else {
				$this->session->set_flashdata('error', 'Error occurred while deleting cashback details.');
				redirect('adminsettings/cashback','refresh');
			}
		}
	}	
	
	// view all withdraws..	
	function withdraw($name,$user_id)
	{

		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} 
		else
		{
			if($name !='' && $user_id!='')
			{
				$data['user_detail'] = $this->admin_model->view_user($user_id);
				if($data['user_detail'])
				{
					$this->load->view('adminsettings/viewuser',$data);	
				}
				else
				{
					redirect('adminsettings/dashboard','refresh');
				}
			}
			else
			{
				//New code for multiple Process status records 6/4/16//		
				if($_POST['processwithdraw'])
				{
					$results = $this->admin_model->process_multi_withdraw();
					if($results){
						$this->session->set_flashdata('success', 'Withdraw Process status updated successfully.');
						redirect('adminsettings/withdraw','refresh');
					} else {
						$this->session->set_flashdata('error', 'Error occurred while updating withdraw details.');
						redirect('adminsettings/withdraw','refresh');
					}
				}	
				//End//

				//New code for multiple Complete status records 6/4/16//	
				if($_POST['completewithdraw'])
				{
					$results = $this->admin_model->complete_multi_withdraw();
					if($results){
						$this->session->set_flashdata('success', 'Withdraw Completd status updated successfully.');
						redirect('adminsettings/withdraw','refresh');
					} else {
						$this->session->set_flashdata('error', 'Error occurred while updating withdraw details.');
						redirect('adminsettings/withdraw','refresh');
					}
				}	
				//End//

				//New code for multiple Cancel status Withdraw records 6/4/16//	
				if($_POST['cancelwithdraw'])
				{

					$results 	 = $this->admin_model->cancel_multi_withdraw();
					if($results){
						$this->session->set_flashdata('success', 'Withdraw Canceled status updated successfully.');
						redirect('adminsettings/withdraw','refresh');
					} else {
						$this->session->set_flashdata('error', 'Error occurred while updating withdraw details.');
						redirect('adminsettings/withdraw','refresh');
					}
				}
				//End//

				if($this->input->post('hidd'))
				{
					if($this->input->post('chkbox'))
					{
						$sort_order = $this->input->post('chkbox');					 
						$results = $this->admin_model->delete_bulk_records($sort_order,'withdraw','withdraw_id');
					}				
					
					if($results)
					{
						
						$this->session->set_flashdata('success', 'Withdraw details deleted successfully.');
						redirect('adminsettings/withdraw','refresh');
					}
					else
					{
						$this->session->set_flashdata('error', 'Error occurred while updating withdraw details.');
						redirect('adminsettings/withdraw','refresh');
					}
				}
				//$data['withdraws'] = $this->admin_model->withdraw();
				$this->load->view('adminsettings/withdraw',$data);
			}	
		}
	}
	
	// change withdraw status..
	function editwithdraw($id){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
			$withdraw = $this->admin_model->editwithdraw($id);
			if($withdraw){
				foreach($withdraw as $get) { 
					$data['withdraw_id'] = $get->withdraw_id;
					$data['user_id'] = $get->user_id;
					$data['requested_amount'] = $get->requested_amount;
					$data['date_added'] = $get->date_added;
					$data['closing_date'] = $get->closing_date;
					$data['status'] = $get->status;
					/*New code 2-8-16*/
					$data['ifsc_code'] = $get->ifsc_code;
					$data['bank_id']   = $get->bank_id;
					$data['bank_name']   = $get->bank_name;
					/*end*/

				}
			} else {
				redirect('adminsettings/withdraw','refresh');
			}
			$data['action'] = 'new';
			$this->load->view('adminsettings/editwithdraw',$data);
		}
	}
	
	
	// change withdraw status..
	function updatewithdraw(){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				$id = $this->input->post('withdraw_id');
				$updation = $this->admin_model->updatewithdraw($id);
				if($updation){ 
					$this->session->set_flashdata('success', ' Withdraw status updated successfully.');
					redirect('adminsettings/withdraw','refresh');
				} else { 
					$data['action']="new";
					$this->session->set_flashdata('error', 'Error occurred while updating withdraw status.');
					redirect('adminsettings/editwithdraw/'.$id,'refresh');
				}
			}
		}
	}
	// delete withdraw..
	function deletewithdraw($id){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deletewithdraw($id);
			if($deletion){
				$this->session->set_flashdata('success', 'Withdraw details deleted successfully.');
				redirect('adminsettings/withdraw','refresh');
			}
			else {
				$this->session->set_flashdata('error', 'Error occurred while deleting withdraw details.');
				redirect('adminsettings/withdraw','refresh');
			}
		}
	}
	
	// removes code on editing premium coupon.. (through ajax..)
	function delete_shopcoupon(){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
			$ids = $this->input->post('id');
			$result = $this->admin_model->delete_shopcoupon($ids);
			if($result){
				echo 1;
			}
			else {
				echo 0;
			}
		}	
	}
	
	// change category order..
	function change_cate_order($position,$order_no,$category_id){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
			// echo $position; echo $order_no; echo $category_id; exit;
			if($position=='up'){
				$new_order = $order_no - 1;
				$result = $this->admin_model->change_cate_order($order_no,$new_order);
				if($result){
					$this->session->set_flashdata('success', 'Category order changed successfully.');
					redirect('adminsettings/categories','refresh');
				}
				else {
					$this->session->set_flashdata('error', 'Error occurred while changing category order.');
					redirect('adminsettings/categories','refresh');
				}
			}
			else if($position=='down'){
				$new_order = $order_no + 1;
				$result = $this->admin_model->change_cate_order($order_no,$new_order);
				if($result){
					$this->session->set_flashdata('success', 'Category order changed successfully.');
					redirect('adminsettings/categories','refresh');
				}
				else {
					$this->session->set_flashdata('error', 'Error occurred while changing category order.');
					redirect('adminsettings/categories','refresh');
				}
			}
		}
	}
	
	function change_premium_cate_order($position,$order_no,$category_id){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
			// echo $position; echo $order_no; echo $category_id; exit;
			if($position=='up'){
				$new_order = $order_no - 1;
				$result = $this->admin_model->change_premium_cate_order($order_no,$new_order);
				if($result){
					$this->session->set_flashdata('success', 'Premium Category order changed successfully.');
					redirect('adminsettings/premium_categories','refresh');
				}
				else {
					$this->session->set_flashdata('error', 'Error occurred while changing category order.');
					redirect('adminsettings/premium_categories','refresh');
				}
			}
			else if($position=='down'){
				$new_order = $order_no + 1;
				$result = $this->admin_model->change_premium_cate_order($order_no,$new_order);
				if($result){
					$this->session->set_flashdata('success', 'Premium Category order changed successfully.');
					redirect('adminsettings/premium_categories','refresh');
				}
				else {
					$this->session->set_flashdata('error', 'Error occurred while changing category order.');
					redirect('adminsettings/premium_categories','refresh');
				}
			}
		}
	}
	
	// get all referrals using use email.. (through ajax..)
	function all_referrals(){
	$this->input->session_helper();	
		$email = $this->input->post('email');
		$all_emails = $this->admin_model->all_referrals($email);
		$final='';
		foreach($all_emails as $get){
			$final .= '<li>'.$get->referral_email.'</li>';
		}
		echo $final;
	}

	// view all click history..
	function click_history($name,$user_id=null)
	{

		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
			if($name !='' && $user_id!='')
			{
				$data['user_detail'] = $this->admin_model->view_user($user_id);
				$this->load->view('adminsettings/viewuser',$data);
			}
			else
			{
				if($this->input->post('hidd'))
				{
					if($this->input->post('chkbox'))
					{
						$results = $this->admin_model->click_history_bulk_delete();
					}
					if($results)
					{
						$this->session->set_flashdata('success', 'Click History details Deleted successfully.');
						redirect('adminsettings/click_history','refresh');
					}
					else
					{
						$this->session->set_flashdata('error', 'Error occurred while Deleting Click History details.');
						redirect('adminsettings/click_history','refresh');
					}	
				}
				//$data['click_histories'] = $this->admin_model->click_history($user_id);
				$this->load->view('adminsettings/click_history',$data);
			}
			
		}
	}
	
	//delete click history..
	function deletehistory($id){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deletehistory($id);
			if($deletion){
				$this->session->set_flashdata('success', 'History details deleted successfully.');
				redirect('adminsettings/click_history','refresh');
			}
			else {
				$this->session->set_flashdata('error', 'Error occurred while deleting history details.');
				redirect('adminsettings/click_history','refresh');
			}
		}
	}
	
	
	//deletecashback
	function deletecashbackdetails($store_id,$id){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deletecashbackdetails($id);
			if($deletion){
				$this->session->set_flashdata('success', 'Cashback details deleted successfully.');
				redirect('adminsettings/cashback_details/'.$store_id,'refresh');
			}
			else {
				$this->session->set_flashdata('error', 'Error occurred while deleting Cashback details.');
				redirect('adminsettings/cashback_details/'.$store_id,'refresh');
			}
		}
	}
	
	// admin signing off..
	function logout() 
	{
		$this->session->sess_destroy();
		$this->session->set_flashdata('success', 'Logged Out successfully.');
		redirect('adminsettings/index','refresh');
	}
	
	// view all sub categories..
	function sub_categories($category=null)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				$results = $this->admin_model->sort_sub_categorys_new();
				 if($this->input->post('chkbox'))
				 {
					 $results = $this->admin_model->sort_sub_categorys_new_delete();
				 }
				
				
				if($results){
					
					$this->session->set_flashdata('success', 'Sub Category details updated successfully.');
					redirect('adminsettings/sub_categories/'.$category,'refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while updating Sub category details.');
					redirect('adminsettings/sub_categories/'.$category,'refresh');
				}
			}
			$data['category_id'] = $category;
			$data['categories'] = $this->admin_model->sub_categories($category);
			$this->load->view('adminsettings/sub_categories',$data);
		}
	}
	
	// view all sub categories..
	function premium_sub_categories($category=null)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$data['category_id'] = $category;
			$data['categories'] = $this->admin_model->premium_sub_categories($category);
			$this->load->view('adminsettings/premium_sub_categories',$data);
		}
	}
	
	
		// add sub category
	function add_sub_category($categoryid){
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				$results = $this->admin_model->addsubcategory();
				if($results){
					$this->session->set_flashdata('success', 'Sub Category details added successfully.');
					redirect('adminsettings/sub_categories/'.$categoryid,'refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while adding category.');
					redirect('adminsettings/add_sub_category/'.$categoryid,'refresh');
				}			
			}
			$data['categoryid']=$categoryid;
			$data['action']='new';
			$this->load->view('adminsettings/add_sub_category',$data);
		}
	}
	
	
		// add sub category
	function add_premium_sub_category($categoryid){
	$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				$results = $this->admin_model->addpremiumsubcategory();
				if($results){
					$this->session->set_flashdata('success', 'Sub Category details added successfully.');
					redirect('adminsettings/premium_sub_categories/'.$categoryid,'refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while adding category.');
					redirect('adminsettings/add_premium_sub_category/'.$categoryid,'refresh');
				}			
			}
			$data['categoryid']=$categoryid;
			$data['action']='new';
			$this->load->view('adminsettings/add_premium_sub_category',$data);
		}
	}
	
	
	
	// edit sub category..
	function editsubcategory($cate_editid){
	$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if(($admin_id=="") || ($cate_editid=="")){
			redirect('adminsettings/index','refresh');
		} else
		{
			$get_category = $this->admin_model->get_subcategory($cate_editid);
			foreach($get_category as $get){
				$data['sun_category_id'] = $get->sun_category_id;
				$data['cate_id'] = $get->cate_id;
				$data['sub_category_name'] = $get->sub_category_name;
				$data['meta_keyword'] = $get->meta_keyword;
				$data['meta_description'] = $get->meta_description;
				$data['category_status'] = $get->category_status;
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/add_sub_category',$data);
		}
	}
	
	// edit sub category..
	function editpremiumsubcategory($cate_editid){
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if(($admin_id=="") || ($cate_editid=="")){
			redirect('adminsettings/index','refresh');
		} else
		{
			$get_category = $this->admin_model->get_premium_subcategory($cate_editid);
			foreach($get_category as $get){
				$data['sun_category_id'] = $get->sun_category_id;
				$data['cate_id'] = $get->cate_id;
				$data['sub_category_name'] = $get->sub_category_name;
				$data['meta_keyword'] = $get->meta_keyword;
				$data['meta_description'] = $get->meta_description;
				$data['category_status'] = $get->category_status;
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/add_premium_sub_category',$data);
		}
	}
	
	// update sub category	
	function updatesubcategory($categoryid){
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				$updated = $this->admin_model->update_subcategory();
				if($updated){
					$data['action']="Edit";
					$this->session->set_flashdata('success', 'Sub Category details updated successfully.');
					redirect('adminsettings/sub_categories/'.$categoryid,'refresh');
				}
				else{
					$data['action']="Edit";
					$this->session->set_flashdata('error', 'Error occurred while updating category details.');
					redirect('adminsettings/sub_categories/'.$categoryid,'refresh');
				}
			}		
		}	
	}
	
	// update sub category	
	function updatepremiumsubcategory($categoryid){
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				$updated = $this->admin_model->update_permiumsubcategory();
				if($updated){
					$data['action']="Edit";
					$this->session->set_flashdata('success', 'Sub Category details updated successfully.');
					redirect('adminsettings/premium_sub_categories/'.$categoryid,'refresh');
				}
				else{
					$data['action']="Edit";
					$this->session->set_flashdata('error', 'Error occurred while updating category details.');
					redirect('adminsettings/premium_sub_categories/'.$categoryid,'refresh');
				}
			}		
		}	
	}
	
	//delete sub category..
	function deletesubcategory($id,$mainid){
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deletesubcategory($id);
			if($deletion){
				$this->session->set_flashdata('success', 'Sub Category deleted successfully.');
				redirect('adminsettings/sub_categories/'.$mainid,'refresh');
			}
			else{
				$this->session->set_flashdata('error', 'Error occurred while deleting category.');
				redirect('adminsettings/sub_categories'.$mainid,'refresh');
			}
		}	
	}
	
	//delete sub category..
	function deletepremiumsubcategory($id,$mainid){
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deletepremiumsubcategory($id);
			if($deletion){
				$this->session->set_flashdata('success', 'Sub Category deleted successfully.');
				redirect('adminsettings/premium_sub_categories/'.$mainid,'refresh');
			}
			else{
				$this->session->set_flashdata('error', 'Error occurred while deleting category.');
				redirect('adminsettings/premium_sub_categories'.$mainid,'refresh');
			}
		}	
	}
	
	
	// view all affiliates..
	function site_affiliates(){
		$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				 if($this->input->post('chkbox'))
				 {
					 $results = $this->admin_model->delete_multi_site_affiliate();
				 }
				if($results){
					
					$this->session->set_flashdata('success', 'Site Affiliates Deleted successfully.');
					redirect('adminsettings/site_affiliates','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while deleting Site Affiliates details.');
					redirect('adminsettings/site_affiliates','refresh');
				}
			}
			
			$data['affiliates'] = $this->admin_model->site_affiliates();
			$this->load->view('adminsettings/site_affiliates',$data);
		}
	}
	
	// add new affiliate
	
	function site_addaffiliate(){
		$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				$flag=0;
				
						$results = $this->admin_model->site_addaffiliate();
						if($results){
							$this->session->set_flashdata('success', ' Affiliate details added successfully.');
							redirect('adminsettings/site_affiliates','refresh');
						}
						else{
							$this->session->set_flashdata('error', 'Error occurred while adding affiliate.');
							redirect('adminsettings/site_addaffiliate','refresh');
						}
					
				}
			}
			$data['action']='new';
			$this->load->view('adminsettings/site_addaffiliate',$data);
		}
	
	// edit affiliate

	//New code for bank details page//


		function bank_details(){
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				 if($this->input->post('chkbox'))
				 {
					 $results = $this->admin_model->delete_multi_banknames();
				 }
				if($results){
					
					$this->session->set_flashdata('success', 'Bank name Deleted successfully.');
					redirect('adminsettings/bank_details','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while deleting Bank name details.');
					redirect('adminsettings/bank_details','refresh');
				}
			}
			
			$data['bankdetails'] = $this->admin_model->bank_details();
			$this->load->view('adminsettings/bank_details',$data);
		}
	}
	
	function add_bankdetails()
	{ 
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save'))
			{
				$flag=0;		
				$results = $this->admin_model->add_bankdetails();
					if($results){
						$this->session->set_flashdata('success', ' Bank details added successfully.');
						redirect('adminsettings/bank_details','refresh');
					}
					else
					{
						$this->session->set_flashdata('error', 'Error occurred while adding Bank names.');
						redirect('adminsettings/bank_details','refresh');
					}				
			}
		}
		$data['action']='new';
		$this->load->view('adminsettings/add_bankdetails',$data);
	}
	
	function edit_bankdetails($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if(($admin_id=="") || ($id==""))
		{
			redirect('adminsettings/index','refresh');
		} else
		{
			$bank_details = $this->admin_model->get_bankdetails($id);
			foreach($bank_details as $bank)
			{	
				$data['bank_id']     = $bank->bankid;
				$data['bank_num_id'] = $bank->bank_id;
				$data['bank_name']   = $bank->bank_name;
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/add_bankdetails',$data);
		}
	}

	function update_bankdetails($id){
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				//echo print_r($_POST); exit;
				$updated = $this->admin_model->update_bankdetails();
				if($updated){
					//$data['action']="Edit";
					$this->session->set_flashdata('success', 'Bank details updated successfully.');
					redirect('adminsettings/bank_details/','refresh');
				}
				else{
					$data['action']="Edit";
					$this->session->set_flashdata('error', 'Error occurred while updating bank details.');
					redirect('adminsettings/edit_bankdetails/'.$id,'refresh');
				}
			}		
		}	
	}


	function delete_bankdetails($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->delete_bankdetails($id);
			if($deletion){
				$data['action']="Edit";
				$this->session->set_flashdata('success', ' Bank details deleted successfully.');
				redirect('adminsettings/bank_details','refresh');
			}
			else{
				$data['action']="Edit";
				$this->session->set_flashdata('error', 'Error occurred while deleting Bank details.');
				redirect('adminsettings/bank_details','refresh');
			}
		}
	}

	//New code for Store details page//

	/*function stores()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{	
			
			if($this->input->post('save'))
			{	
				$this->security->cookie_handlers();
					$results = $this->admin_model->stores();
					if($results)
					{
						$this->session->set_flashdata('success', ' Store details added successfully.');
						redirect('adminsettings/stores','refresh');
					}
					else
					{
						$this->session->set_flashdata('error', 'Error occurred while adding Store.');
						redirect('adminsettings/stores','refresh');
					}
			}
			$data['action'] = "new";
			$this->load->view('adminsettings/stores',$data);	


		}
	}*/

	//End//	



	function site_editaffiliate($id){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$get_affiliate = $this->admin_model->site_get_affiliate($id);
			foreach($get_affiliate as $get){
				$data['affiliate_id'] = $get->affiliate_id;
				$data['affiliate_name'] = $get->affiliate_name;
				$data['affiliate_type'] = $get->affiliate_type;
				$data['affiliate_status'] = $get->affiliate_status;
				$data['affiliate_param'] = $get->affiliate_param;
				$data['affiliate_traking_param'] = $get->affiliate_traking_param;
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/site_addaffiliate',$data);		
		}
	}
	
	// update affiliate
	function site_updateaffiliate(){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				$flag=0;
				$affiliate_id = $this->input->post('affiliate_id');		
						$results = $this->admin_model->site_updateaffiliate();
						
						if($results){
							$this->session->set_flashdata('success', ' Affiliates details updated successfully.');
							redirect('adminsettings/site_affiliates','refresh');
						}
						else{
							$this->session->set_flashdata('error', 'Error occurred while updating store details.');
							redirect('adminsettings/site_addaffiliate','refresh');
						}
					}
		}
	}
	
	// delete affiliate
	function site_deleteaffiliate($id){
		$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->site_deleteaffiliate($id);
			if($deletion){
				$data['action']="Edit";
				$this->session->set_flashdata('success', ' Affiliates details deleted successfully.');
				redirect('adminsettings/site_affiliates','refresh');
			}
			else{
				$data['action']="Edit";
				$this->session->set_flashdata('error', 'Error occurred while deleting Affiliates details.');
				redirect('adminsettings/site_affiliates','refresh');
			}
		}
	}
	
	/************ Dec 11th *************/
	
		// Missing cashback
	function missing_cashback($name,$user_id,$store_name)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($name !='' && $user_id!='')
			{
				$data['user_detail'] = $this->admin_model->view_user($user_id);
				if($data['user_detail'])
				{
					$this->load->view('adminsettings/viewuser',$data);	
				}
				else
				{
					redirect('adminsettings/dashboard','refresh');
				}
			}
			else
			{

				if($this->input->post('hidd'))
				{
					
					if($this->input->post('chkbox'))
					{
						$sort_order = $this->input->post('chkbox');
						$results = $this->admin_model->delete_bulk_records($sort_order,'missing_cashback','cashback_id');
					}
					if($results)
					{
						
						$this->session->set_flashdata('success', 'Missing Cashback details updated successfully.');
						redirect('adminsettings/missing_cashback','refresh');
					}
					else{
						$this->session->set_flashdata('error', 'Error occurred while updating Missing Cashback details.');
						redirect('adminsettings/missing_cashback','refresh');
					}
				}
				$data['allmissing_cashbacks'] = $this->admin_model->get_all_missing_cashback($store_name);
				$this->load->view('adminsettings/missing_cashback',$data);
			}	
		}	
	}
	
	// view particular cashback details..
	function view_missing_cashback($cashbcakid){
		$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if(($admin_id=="") || ($cashbcakid=="")) {
			redirect('adminsettings/index','refresh');
		} else
		{
			$data['missing_cashback'] = $this->admin_model->view_missing_cb($cashbcakid);
			$this->load->view('adminsettings/view_missing_cashback',$data);
		}	
	}
	
	// update cashback status..
	function cashbackupdate(){
		$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="") {
			redirect('adminsettings/index','refresh');
		} else
		{
		/*	print_r($this->input->post());
			exit;*/
			if($this->input->post('save')){
			$updated = $this->admin_model->missiing_cashback_update();
				if($updated){
					$this->session->set_flashdata('success', 'Missing Cashback details updated successfully.');
					redirect('adminsettings/missing_cashback','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while updating Missing Cashback details.');
					redirect('adminsettings/missing_cashback','refresh');
				}
			}
		}	
	}
	
	// delete cashback details..
	function delete_missing_cashback($id){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->delete_missing_cashback($id);
			if($deletion){
				$this->session->set_flashdata('success', ' Missing Cashback deleted successfully.');
				redirect('adminsettings/missing_cashback','refresh');
			}
			else{
				$this->session->set_flashdata('error', 'Error occurred while deleting Missing Cashback details.');
				redirect('adminsettings/missing_cashback','refresh');
			}
		}
	}
	
	
	/************ Dec 11th *************/
	
	
	/************ Dec 12th *************/
	// upload bulk Stores
	function bulk_store(){
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$flag=0;
			if($this->input->post('save')){
				$bulkcoupon = $_FILES['bulkcoupon']['name'];
				if($bulkcoupon==""){
					$flag=1;
					$this->session->set_flashdata('error', 'Please upload the file.');
					redirect('adminsettings/bulk_store','refresh');
				}
				else {
					$flag=0;
					if($bulkcoupon!="") {
						$new_random = mt_rand(0,99999);
						$bulkcoupon = $_FILES['bulkcoupon']['name'];

						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($bulkcoupon);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $bulkcoupon);
						$bulkcoupon     = $newfilename.$new_random.'.'.$file_ex;
						/*End 3-10-16*/

						//$bulkcoupon = remove_space($new_random.$bulkcoupon);
						$config['upload_path'] ='uploads/stores';
						$config['allowed_types'] = '*';
						$config['file_name']=$bulkcoupon;
						
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($bulkcoupon!="" && (!$this->upload->do_upload('bulkcoupon')))
						{
							$bulkcouponerror = $this->upload->display_errors();
						}
						if(isset($bulkcouponerror))
						{
							$flag=1;
							$this->session->set_flashdata('error',$bulkcouponerror);
							redirect('adminsettings/bulk_store','refresh');
						}
					}
					if($flag==0){
						$results = $this->admin_model->bulk_stores($bulkcoupon);
						if($results){
							$this->session->set_flashdata('success', ' Store details added successfully.');
							redirect('adminsettings/affiliates','refresh');
						}
						else{
							$this->session->set_flashdata('error', 'Error occurred while adding Store details.');
							redirect('adminsettings/bulk_store','refresh');
						}
					}
				}
			$result = $this->admin_model->bulk_stores();
			if($result){
					$this->session->set_flashdata('success', ' Store details added successfully.');
					redirect('adminsettings/affiliates','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while adding Store details.');
					redirect('adminsettings/bulk_store','refresh');
				}
			}
			$data['action'] = "new";
			$this->load->view('adminsettings/bulk_store',$data);
		}	
	}
	
	
	function report_upload()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
		  redirect('adminsettings/index','refresh');
		} else
		{		
			$flag=0;
			if($this->input->post('save'))
			{
				$bulkcoupon = $_FILES['bulkcoupon']['name'];
				//echo $bulkcoupon;
				if($bulkcoupon=="")
				{
					$flag=1;
					$this->session->set_flashdata('error', 'Please upload the file.');
					redirect('adminsettings/report_upload','refresh');
				}
				else 
				{
					$flag=0;
					if($bulkcoupon!="") 
					{
						$new_random = mt_rand(0,99999);
						$bulkcoupon = $_FILES['bulkcoupon']['name'];
						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($bulkcoupon);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $bulkcoupon);
						$bulkcoupon     = $newfilename.$new_random.'.'.$file_ex;
						/*End 3-10-16*/
						//$bulkcoupon = remove_space($new_random.$bulkcoupon);
						$config['upload_path'] 	 = 'uploads/reports';
						$config['allowed_types'] = '*';
						$config['max_size'] 	 = '20000';
						$config['file_name']	 = $bulkcoupon;
						
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($bulkcoupon!="" && (!$this->upload->do_upload('bulkcoupon')))
						{
							$bulkcouponerror = $this->upload->display_errors();
						}
						if(isset($bulkcouponerror))
						{
							$flag=1;
							$this->session->set_flashdata('error',$bulkcouponerror);
							redirect('adminsettings/report_upload','refresh');
						}
					}					
					if($flag==0)
					{
		 
						$results = $this->admin_model->upload_reports($bulkcoupon);
						if($results)
						{
							$this->session->set_flashdata('success', ' Store details added successfully.');
							redirect('adminsettings/reports','refresh');
						}
						else
						{
							$this->session->set_flashdata('error', 'Error occurred while adding Store details.');
							redirect('adminsettings/report_upload','refresh');
						}
					}
				}
				/*$result = $this->admin_model->upload_reports();
				if($result)
				{
					$this->session->set_flashdata('success', ' Store details added successfully.');
					redirect('adminsettings/reports','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while adding Store details.');
					redirect('adminsettings/report_upload','refresh');
				}*/
			}
			$data['action'] = "new";
			$this->load->view('adminsettings/report_upload',$data);
		}	
	}
	
	function reports()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				 if($this->input->post('chkbox'))
				 {
					 $results = $this->admin_model->reports_bulk_delete();
				 }
				if($results){
					
					$this->session->set_flashdata('success', 'reports details updated successfully.');
					redirect('adminsettings/reports','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while updating reports details.');
					redirect('adminsettings/reports','refresh');
				}
			}
			//$data['coupons'] = $this->admin_model->reports();
			$this->load->view('adminsettings/reports',$data);
		}
	}
		function view_report($report_id){
			$admin_id = $this->session->userdata('admin_id');
			if(($admin_id=="") || ($report_id=="")) {
				redirect('adminsettings/index','refresh');
			} else
			{
				$data['user'] = $this->admin_model->view_report($report_id);
				$this->load->view('adminsettings/view_report',$data);
			}	
	}
	//New code for Report update and export functions//
	function report_export($name)
	{		
			$this->input->session_helper();
			$admin_id = $this->session->userdata('admin_id');
			if($admin_id==""){
			redirect('adminsettings/index','refresh');
			}
			else 
			{
				 
				$data['action'] 	 = "new";
				$data['export_type'] = $name;

				/*
				$data['pending_cashabck']  = "pending_cashabck"; 
				$data['missing_cashabck']  = "missing_cashabck";
				$data['withdraws'] 	   	   = "withdraws";
				$data['payments'] 		   = "payments";
				$data['subscribes'] 	   = "subscribes";
				//New code for user details 6-5-16//
				$data['users'] 	           = "users";
				//end//
				*/
				
				//new code changes 7-5-16//
				$data['bankdetails'] = $this->admin_model->new_bank_details();	
				//End new code changes 7-5-16//
				$data['cashback'] 	 = $this->admin_model->report_export();
				$this->load->view('adminsettings/report_export',$data);
			}	
	}
	function report_update($name)
	{
			$this->input->session_helper();
			$admin_id = $this->session->userdata('admin_id');
			if($admin_id==""){
			redirect('adminsettings/index','refresh');
			}
			else 
			{
				$data['action'] 	 = "new";
				$data['update_type'] = $name;
				/*
				$data['pending_cashabck'] = "pending_cashabck"; 
				$data['missing_cashabck'] = "missing_cashabck";
				$data['withdraws'] 	   	  = "withdraws";
				$data['payments'] 		  = "payments";
				*/
				$data['bankdetails'] = $this->admin_model->bank_details();	
				$data['cashback'] 	 = $this->admin_model->report_export();
				$this->load->view('adminsettings/report_update',$data);
			}

	}
	function export_report()
	{	
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
		redirect('adminsettings/index','refresh');
		}
		else 
		{
			
			$this->load->helper('csv_helper');

		    $store_name = $this->input->post('store_name');
			$start_date1 = $this->input->post('start_date');
			$end_date1 	 = $this->input->post('end_date');
			$starts_date = date("Y-m-d",strtotime($start_date1));
			$ends_date   = date("Y-m-d",strtotime($end_date1));
			$user_id 	 = $this->input->post('user_id');
            $sep_user 	 = explode(',',$user_id);
            $type 		 = $this->input->post('type');
			$report      = $this->admin_model->pending_report();
			$t_date      = date('Y-m-d');
		}	
	}
	

	function update_report()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
		  redirect('adminsettings/index','refresh');
		} 
		else
		{		
			$flag=0;
			if($this->input->post('save'))
			{
				$updatereport = $_FILES['reports']['name'];
				$type 		  = $this->input->post('type');
				
				if($updatereport=="")
				{
					$flag=1;
					$this->session->set_flashdata('error', 'Please upload the file.');
					redirect('adminsettings/report_update','refresh');
				}
				else 
				{
					$flag=0;
					if($updatereport!="") 
					{
						$new_report   = mt_rand(0,99999);
						$reportname   = $_FILES['reports']['name'];

						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($reportname);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $reportname);
						$updatereport   = $newfilename.$new_report.'.'.$file_ex;
						/*End 3-10-16*/

						//$updatereport = remove_space($new_report.$reportname);
						$config['upload_path'] ='uploads/updatereports';
						$config['allowed_types'] = '*';
						$config['max_size'] = '20000';
						$config['file_name']=$updatereport;
						
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($updatereport!="" && (!$this->upload->do_upload('reports')))
						{
							
							$updatereporterror = $this->upload->display_errors();
						}
						if(isset($updatereporterror))
						{	
							$flag=1;
							$this->session->set_flashdata('error',$updatereporterror);
							redirect('adminsettings/report_update','refresh');
						}
					}
					if($flag==0)
					{
						$results = $this->admin_model->update_reports($updatereport,$type);
						if($results)
						{
							if($type == 'subscribers')
							{	

								$this->session->set_flashdata('success', ' Reports details Updated successfully.');
								redirect('adminsettings/subscribers','refresh');
							}
							else if($type == 'users')
							{
								$this->session->set_flashdata('success', ' Reports details Updated successfully.');
								redirect('adminsettings/users','refresh');	
							}
							else
							{
								$this->session->set_flashdata('success', ' Reports details Updated successfully.');
								redirect('adminsettings/reports','refresh');
							}
						}
						else
						{
							$this->session->set_flashdata('error', 'Error occurred while Updating report details.');
							redirect('adminsettings/report_update','refresh');
						}
					}
				}
			}
			$data['action'] = "new";
			$this->load->view('adminsettings/report_update',$data);
		}	
	}


	//End// 
	function edittransactions($cate_editid)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if(($admin_id=="") || ($cate_editid=="")){
			redirect('adminsettings/index','refresh');
		} else
		{
			$this->security->cookie_handlers();
			$get_category = $this->admin_model->get_transaction1($cate_editid);
			foreach($get_category as $get)
			{
				$data['trans_id'] 			= $get->trans_id;
				$data['user_id'] 			= $get->user_id;
				$data['transation_reason'] 	= $get->transation_reason;
				$data['transation_mode'] 	= $get->mode;
				$data['transation_amount'] 	= $get->transation_amount;
				$data['transation_date'] 	= $get->transation_date;
				$data['transation_status'] 	= $get->transation_status;
				$data['report_update_id'] 	= $get->report_update_id;
				$data['payment_type'] 		= $get->payment_type;
				$data['transaction_date'] 	= $get->transaction_date;
				$data['transaction_id'] 	= $get->transation_id;
				$data['table'] 				= $get->table;
				$data['details_id']  		= $get->details_id;
				$data['new_txn_id'] 		= $get->new_txn_id;
				$data['ref_user_tracking']  = $get->ref_user_tracking_id;
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/manual_credit',$data);
		}
	}
	function transactions($name,$user_id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} 
		else
		{
			if($name !='' && $user_id!='')
			{
				$data['user_detail'] = $this->admin_model->view_user($user_id);
				if($data['user_detail'])
				{
					$this->load->view('adminsettings/viewuser',$data);	
				}
				else
				{
					redirect('adminsettings/dashboard','refresh');
				}
			}
			else
			{
				if($this->input->post('hidd'))
				{
					if($this->input->post('chkbox'))
					{
						$sort_order = $this->input->post('chkbox');					 
						$results = $this->admin_model->delete_bulk_records($sort_order,'transation_details','trans_id');
					}
					if($results)
					{
						$this->session->set_flashdata('success', 'Transactions details updated successfully.');
						redirect('adminsettings/transactions','refresh');
					}
					else
					{
						$this->session->set_flashdata('error', 'Error occurred while updating transactions details.');
						redirect('adminsettings/transactions','refresh');
					}
				}
				//$data['coupons'] = $this->admin_model->transactions();
				$this->load->view('adminsettings/transactions',$data);
			}	
		}
	}
	/************ Dec 12th *************/
	
	
	/************* Dec 13th ********************/
		// view all cms content
	function blog(){
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$data['allcms'] = $this->admin_model->get_allblog();
			$this->load->view('adminsettings/blog',$data);
		}	
	}
	
	// add cms contents..
	function addblog()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
		
				$logo_url = $this->input->post('logo_url');
				
			/*	$site_url = $this->input->post('site_url');*/
				$cms_content = $this->input->post('cms_content');
				$affiliate_logo = $_FILES['affiliate_logo']['name'];
				if($cms_content==""){
					$this->session->set_flashdata('error', 'Please enter BLOG content.');
					// $post_title = $this->input->post('page_title');
					redirect('adminsettings/addblog','refresh');
				}
				else{
					if($affiliate_logo!="") {
						$new_random = mt_rand(0,99999);
						$affiliate_logo = $_FILES['affiliate_logo']['name'];


						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($affiliate_logo);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $affiliate_logo);
						$affiliate_logo = $newfilename.$new_random.'.'.$file_ex;
						/*End 3-10-16*/

						//$affiliate_logo = remove_space($new_random.$affiliate_logo);
						$config['upload_path'] ='uploads/blog';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['file_name']=$affiliate_logo;
						
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($affiliate_logo!="" && (!$this->upload->do_upload('affiliate_logo')))
						{
							$affiliate_logoerror = $this->upload->display_errors();
						}
						if(isset($affiliate_logoerror))
						{
							$this->session->set_flashdata('error',$affiliate_logoerror);
							redirect('adminsettings/addaffiliate','refresh');
						}
						
					}
					$results = $this->admin_model->addblog($affiliate_logo);
					if($results){
						$this->session->set_flashdata('success', ' BLOG details added successfully.');
						redirect('adminsettings/blog','refresh');
					}
					else{
						$this->session->set_flashdata('error', 'Error occurred while adding BLOG.');
						redirect('adminsettings/addblog','refresh');
					}
				}
			}
			$data['action']='new';
			$this->load->view('adminsettings/addblog',$data);
		}
	}
	
	// view cms content
	function editblog($cms_editid)
	{
		$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$get_cms = $this->admin_model->get_blogcontent($cms_editid);
			foreach($get_cms as $get){
				$data['cms_id'] = $get->cms_id;
				$data['cms_heading'] = $get->cms_heading;
				$data['cms_metatitle'] = $get->cms_metatitle;
				$data['cms_metakey'] = $get->cms_metakey;
				$data['cms_metadesc'] = $get->cms_metadesc;
				$data['cms_content'] = $get->cms_content;
				$data['cms_status'] = $get->cms_status;
				$data['affiliate_logo'] = $get->affiliate_logo;
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/addblog',$data);
		}
	}
	
	// update cms contents..	
	function updateblog()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				
				$cms_content = $this->input->post('cms_content');
				$cms_id = $this->input->post('cms_id');
				
				if($cms_content=="") {
					$this->session->set_flashdata('error', 'Please enter BLOG content.');
					$data['action']="Edit";
					// $post_title = $this->input->post('page_title');
					redirect('adminsettings/addblog','refresh');
				}
				else {
					$affiliate_logo = $_FILES['affiliate_logo']['name'];
					if($affiliate_logo!="") {
						$new_random = mt_rand(0,99999);
						$affiliate_logo = $_FILES['affiliate_logo']['name'];

						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($affiliate_logo);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $affiliate_logo);
						$affiliate_logo = $newfilename.$new_random.'.'.$file_ex;
						/*End 3-10-16*/

						//$affiliate_logo =remove_space($new_random.$affiliate_logo);
						$config['upload_path'] ='uploads/blog';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['file_name']=$affiliate_logo;
						
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($affiliate_logo!="" && (!$this->upload->do_upload('affiliate_logo')))
						{
							$affiliate_logoerror = $this->upload->display_errors();
						}
						if(isset($affiliate_logoerror))
						{
							
							$this->session->set_flashdata('error',$affiliate_logoerror);
							redirect('adminsettings/editblog'.$cms_id,'refresh');
							
						}
					}
					else {
						$affiliate_logo = $this->input->post('hidden_img');
					}
					
					
					$updated = $this->admin_model->updateblog($affiliate_logo);
					if($updated){
						$data['action']="Edit";
						$this->session->set_flashdata('success', ' BLOG details updated successfully.');
						redirect('adminsettings/blog','refresh');
					}
					else{
						$data['action']="Edit";
						$this->session->set_flashdata('error', 'Error occurred while updating BLOG details.');
						redirect('adminsettings/blog','refresh');
					}
				}
			}		
		}
	}
	
	// delete cms content 
	function deleteblog($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deleteblog($id);
			if($deletion){
				$data['action']="Edit";
				$this->session->set_flashdata('success', ' Blog details deleted successfully.');
				redirect('adminsettings/cms','refresh');
			}
			else{
				$data['action']="Edit";
				$this->session->set_flashdata('error', 'Error occurred while deleting Blog details.');
				redirect('adminsettings/cms','refresh');
			}
		}
	}
	
	
		function get_allcomments($blog_id){
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$data['blog_id'] = $blog_id;
			$data['allcms'] = $this->admin_model->get_allcomments($blog_id);
			$this->load->view('adminsettings/comments',$data);
		}	
	}
	
	
	function status_change_comments($refid,$blogid)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			//if($this->input->post('save')){
				$flag=0;
						$results = $this->admin_model->status_change_comments($refid);
						
						if($results){
							$this->session->set_flashdata('success', ' Comment Status updated successfully.');
							redirect('adminsettings/get_allcomments/'.$blogid,'refresh');
						}
						else{
							$this->session->set_flashdata('error', 'Error occurred while updating  Comment Status .');
							redirect('adminsettings/get_allcomments/'.$blogid,'refresh');
						}
					/*}*/
		}
	}
	
	//delete premium  category..
	function delete_comments($id,$blogid)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->delete_comments($id);
			if($deletion){
				$this->session->set_flashdata('success', 'Comments deleted successfully.');
				redirect('adminsettings/get_allcomments/'.$blogid,'refresh');
			}
			else{
				$this->session->set_flashdata('error', 'Error occurred while deleting Comments.');
				redirect('adminsettings/get_allcomments/'.$blogid,'refresh');
			}
		}	
	}
	
	
	function add_comments($blog_id=null)
	{	
			$this->input->session_helper();
			$admin_id = $this->session->userdata('admin_id');
			if($admin_id==""){
				redirect('adminsettings/index','refresh');
			} else
			{
				if($this->input->post('save')){
					$comments = $this->input->post('comments');
					if($comments==""){
						$this->session->set_flashdata('error', 'Please enter Your Comment.');
						// $post_title = $this->input->post('page_title');
						redirect('adminsettings/add_comment','refresh');
					}
					else{
						$blog_id = $this->input->post('blog_id');
						$results = $this->admin_model->add_comments();
						if($results){
							$this->session->set_flashdata('success', ' Comment added successfully.');
							redirect('adminsettings/get_allcomments/'.$blog_id,'refresh');
						}
						else{
							$this->session->set_flashdata('error', 'Error occurred while adding Comment.');
							redirect('adminsettings/add_comment','refresh');
						}
					}
				}
				$data['blog_id']=$blog_id;
				$data['blog_id']=$blog_id;
				$data['action']='new';
				$this->load->view('adminsettings/add_comment',$data);
			}
			
	}
	/*****************Dec 13th *****************/
	
	/*****************Dec 15th *****************/
	function manual_credit($passing_userid=null)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
			if($admin_id==""){
				redirect('adminsettings/index','refresh');
			} else
			{
				if($this->input->post('save'))
				{	//echo $this->input->post('user_id');exit;
			$idd='1';
					$results = $this->admin_model->add_manual_credit($idd); //24/10/15
					if($results){
						$this->session->set_flashdata('success', ' Transaction has been successfully complete.');
						redirect('adminsettings/transactions','refresh');
					}
					else{
						$this->session->set_flashdata('error', 'Error occurred while Transaction.');
						redirect('adminsettings/manual_credit','refresh');
					}
				}
				if($this->input->post('saves'))
				{	//echo $this->input->post('user_id');exit;
			//echo $this->input->post('transation_amount');exit;
			$idd='0';
					$results = $this->admin_model->add_manual_credit($idd); //24/10/15
					if($results){
						$this->session->set_flashdata('success', ' Transaction has been successfully updated.');
						redirect('adminsettings/transactions','refresh');
					}
					else{
						$this->session->set_flashdata('error', 'Error occurred while Transaction.');
						redirect('adminsettings/transactions','refresh');
					}
				}
				$data['action']='new';
				$data['passing_userid']=$passing_userid;
				$this->load->view('adminsettings/manual_credit',$data);
			}
	}	
	/*****************Dec 15th *****************/
	// 5/12/2014   renuka  
	// edit particular coupon..   
 
	function edit_shoppingcoupon($coupon_id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/dashboard','refresh');
		} else {
			$coupons = $this->admin_model->edit_shoppingcoupon($coupon_id);
			//print_r($coupons);exit;
			foreach($coupons as $get){
				$image_type = $get->img_type;
				 
				$data['img_type']  = $get->img_type;
				$data['shoppingcoupon_id'] = $get->shoppingcoupon_id;
				$data['promo_id'] = $get->promo_id;
				$data['offer_id'] = $get->offer_id;
				$data['offer_name'] = $get->offer_name;
				$data['title'] = $get->title;
				$data['coupon_image'] = $get->coupon_image;
				$data['description'] = $get->description;
				$data['store_categorys'] = $get->category;
				
				$data['user_max'] = $get->user_max;
				$data['type'] = $get->type;
				$data['dis_amount'] = $get->amount;   
				$data['tot_amount'] = $get->price;  
				$data['features'] = $get->features; 
				$data['coupon_code'] = $get->coupon_code;   
				
				
				$data['about'] = $get->about;
				$data['company'] = $get->company;    
				$data['nutshel'] = $get->nutshel; 
				$data['fine_print'] = $get->fine_print;   
			
				$data['location'] = $get->location; 
				
				$data['companys'] = $get->company;   
				
				$data['long_description'] = $get->long_description;  
				
				$data['seo_url'] = $get->seo_url;        
				// $data['code'] = $get->code;  	features 	coupon_code
				$data['offer_page'] = $get->offer_page;
				$data['param_url']  = $get->tracking;
				$data['start_date'] = date('m/d/Y',strtotime($get->start_date));
				$data['expiry_date'] = date('m/d/Y',strtotime($get->expiry_date));
				$data['store_id']    = $get->store_name;
				$data['extra_param_url'] = $get->extra_param_url;

				
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/add_shoppingcoupon',$data);			
		}
	}	
 
	/*New changes 9-5-16*/
	function add_shoppingcoupon()
	{
	  	$this->input->session_helper(); 
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		}
		else
		{	
			if($this->input->post('save'))
			{
				/*New code for amazon s3 copy settings 26-12-16*/
				$amazon_s3_details = $this->admin_model->amazon_s3_settings();
				$copy_status       = $amazon_s3_details->copy_files_status;
				/*End*/

				$coupon_images = $_FILES['coupon_image']['name'];
		 		$all_images='';
				
				/*New code 9-5-16*/
				$img_url  = $this->input->post('img_url');
				/*New code 9-5-16*/

				if(($coupon_images!="") && ($img_url == ''))
				{
					$flag=0;
					$name="coupon_image";
					foreach($_FILES[$name] as $key => $val)
					{
						for($i=0;$i<count($val);$i++)
						{
							unset($_FILES['coupon_image'][$key]);
							$_FILES['coupon_image'][$i][$key]=$val[$i];
						}
					}
					$files=$_FILES;
					for($i=0;$i<count($files[$name]);$i++)
					{
						$_FILES['coupon_image1'] = $files[$name][$i];
						$coupon_image 			 = $_FILES['coupon_image1']['name'];
						$ext 					 = pathinfo($coupon_image, PATHINFO_EXTENSION);
						$file_without_ext 		 = pathinfo($coupon_image, PATHINFO_FILENAME);;
						$image_new 				 =  $this->admin_model->seoUrl($file_without_ext);
						$newimage 			  	 =  $image_new.".".$ext;
						$random_no 				 = mt_rand(100,99999);
						$coupon_img 			 = $random_no.$newimage;
						$config['upload_path'] 	 = 'uploads/premium';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['file_name']	 = $coupon_img;

						
						//New code for AWS 26-9-16//
						if($copy_status == 1)
						{
							$filepath = 'uploads/premium/'.$coupon_img;
							$tmp 	  = $_FILES['coupon_image1']['tmp_name'];
							$this->load->library('S3');
							$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
							$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
						}
						//End//


						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($coupon_img!="" && (!$this->upload->do_upload('coupon_image1')))
						{
							$coupon_imageerror = $this->upload->display_errors();
						}
						// $this->upload->display_errors();
						if(isset($coupon_imageerror))
						{
							$flag=1;
							$this->session->set_flashdata('error',$coupon_imageerror);
							redirect('adminsettings/add_shoppingcoupon','refresh');
						}
						$all_images.= $coupon_img.',';
					}
					$all_images = rtrim($all_images,',');	 
					$img_type	= "normal_image";
				}
				/*New code 9-5-16*/
				if($img_url !="")
				{
					$all_images = $img_url;
					$img_type	= "url_image";
				}	
				/*New code 9-5-16*/
			
				if($flag==0)
				{
					$insert = $this->admin_model->add_shoppingcoupon($all_images,$img_type);
					if($insert){
						$this->session->set_flashdata('success', ' Shopping Coupon details added successfully.');
						redirect('adminsettings/shoppingcoupons','refresh');
					}
					else{
						$this->session->set_flashdata('error', 'Error occurred while adding coupon details.');
						redirect('adminsettings/add_shoppingcoupon','refresh');
					}
				}
			}
			$data['action'] = "new";
			$this->load->view('adminsettings/add_shoppingcoupon',$data);
		}	
	}
	function reviews()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				 if($this->input->post('chkbox'))
				 {
					 $results = $this->admin_model->bulk_reviews_delete();
				 }
				if($results){
					
					$this->session->set_flashdata('success', 'Reviews details deleted successfully.');
					redirect('adminsettings/reviews','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while deleting reviews details.');
					redirect('adminsettings/reviews','refresh');
				}
			}
			
			$data['reviews'] = $this->admin_model->reviews();
			$this->load->view('adminsettings/reviews',$data);
		}
	}
	function change_approval($id,$status)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
			if($admin_id==""){
				redirect('adminsettings/index','refresh');
			} else
			{
				$data['reviews'] = $this->admin_model->changestatus($id,$status);
				redirect('adminsettings/reviews','refresh');
			}
	}
	function orders()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				 if($this->input->post('chkbox'))
				 {
					 $results = $this->admin_model->delete_bulk_orders();
				 }
				
				
				if($results){
					
					$this->session->set_flashdata('success', 'Orders Deleted successfully.');
					redirect('adminsettings/orders','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while updating order details.');
					redirect('adminsettings/orders','refresh');
				}
			}
			
			
			$data['orders'] = $this->admin_model->orders();
			$this->load->view('adminsettings/orders',$data);
		}
	}
	function forgetpassword()
	{
		$this->input->session_helper();	
		$admin_id = $this->session->userdata('admin_id');
		if($this->input->post('forget'))
		{
			
			$result = $this->admin_model->forgetpassword();
			if(!$result){
			$this->session->set_flashdata('error','Email address is incorrect.');
					redirect('adminsettings/forgetpassword','refresh');
			}	
	    	else 
			{
				$this->session->set_flashdata('success','An email is sent to your email address.');
				redirect('adminsettings/forgetpassword','refresh');
			}					
		}
		$this->load->view('adminsettings/forgetpassword');
		
	}
	
	function payment_settings()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		}
		else {
			
				if($this->input->post('save')){
					
				$results = $this->admin_model->payment_settings();
					if($results){
						$this->session->set_flashdata('success', ' Payment details Updated successfully.');
						redirect('adminsettings/payment_settings','refresh');
					}
					else{
						$this->session->set_flashdata('error', 'Error occurred while updating Payment details .');
						redirect('adminsettings/payment_settings','refresh');
					}
				
			
				}
			
			$admin_details = $this->admin_model->getadmindetails();
			if($admin_details){
				foreach($admin_details as $details){
					$data['merchant_key'] = $details->merchant_key;
					$data['merchant_salt'] = $details->merchant_salt;
					$data['merchant_id'] = $details->merchant_id;
					$data['payment_mode'] = $details->payment_mode;					
				}
				$this->load->view('adminsettings/payment_settings',$data);
			}			
		}		
	}
	
	function gallery()
	{
		$this->input->session_helper();
		$this->load->view('adminsettings/photo_gal');
	}
	
	function category_cashback($category_id=null)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
			if($category_id=='')
			{
				redirect('adminsettings/affiliates','refresh');
			}
			if($this->input->post('hidd'))
			{
				 if($this->input->post('chkbox'))
				 {
					 $results = $this->admin_model->click_history_bulk_delete();
				 }
				if($results){
					$this->session->set_flashdata('success', 'Category cashback details Deleted successfully.');
					redirect('adminsettings/category_cashback.php','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while Deleting Category cashback details.');
					redirect('adminsettings/category_cashback.php','refresh');
				}	
			}
		$data['categorys'] = $this->admin_model->category_cashback($category_id);
		$data['store_id'] = $category_id;
		$this->load->view('adminsettings/category_cashback',$data);
		}
	}
	
	function cashback_details($category_id=null)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
			if($category_id=='')
			{
				redirect('adminsettings/affiliates','refresh');
			}
			if($this->input->post('hidd'))
			{
				 if($this->input->post('chkbox'))
				 {
					 $results = $this->admin_model->cashback_details_bulk_delete();
				 }
				if($results){
					$this->session->set_flashdata('success', 'Cashback details Deleted successfully.');
					redirect('adminsettings/cashback_details/'.$category_id,'refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while Deleting Cashback details.');
					redirect('adminsettings/cashback_details/'.$category_id,'refresh');
				}	
			}
			$data['cashbacks'] = $this->admin_model->cashback_details_cb($category_id);
			$data['store_details'] = $this->admin_model->site_get_store($category_id);			
			$data['store_id'] = $category_id;
			$this->load->view('adminsettings/cashback_details',$data);
		}
	}
	
	function update_catecashback($store_id,$action,$caskbackid=null)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){ 
			redirect('adminsettings/index','refresh');
		} else
		{
				if($this->input->post('save')){	
				if($action=='new')
				{
					$results = $this->admin_model->update_catecashback_ins();
				}
				else
				{
					$results = $this->admin_model->update_catecashback_ins($caskbackid);
				}
					if($results){
						$this->session->set_flashdata('success', ' Payment details Updated successfully.');
						redirect('adminsettings/cashback_details/'.$store_id,'refresh');
					}
					else{
						$this->session->set_flashdata('error', 'Error occurred while updating Payment details .');
						redirect('adminsettings/cashback_details/'.$store_id,'refresh');
					}
					
				}
				else
				{
					redirect('adminsettings/affiliates','refresh');
				}
					
			
		}
	}
	
	
	// view all affiliates..
	function ads()
	{
		$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$data['ads'] = $this->admin_model->ads();
			$this->load->view('adminsettings/ads',$data);
		}
	}
	
	
	function update_ads()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$this->input->session_helper();
			if($this->input->post('save'))
			{
				/*New code for amazon s3 copy settings 26-12-16*/
				$amazon_s3_details = $this->admin_model->amazon_s3_settings();
				$copy_status       = $amazon_s3_details->copy_files_status;
				/*End*/

				$ads_id = $this->input->post('ads_id');
				$affiliate_logo = $_FILES['affiliate_logo']['name'];
					
				if($affiliate_logo!="") 
				{
					$affiliate_logo = mt_rand(0,99999);
					$affiliate_logo = $_FILES['affiliate_logo']['name'];

					/*New code for image name field changes 3-10-16*/
					$info 	 	 	= new SplFileInfo($affiliate_logo);
					$file_ex 	 	= $info->getExtension();
					$newfilename 	= str_replace('.'.$file_ex,'', $affiliate_logo);
					$affiliate_logo = $newfilename.$new_random.'.'.$file_ex;
					/*End 3-10-16*/

					//$affiliate_logo =remove_space($affiliate_logo.$affiliate_logo);
					$config['upload_path'] ='uploads/ads';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['file_name']=$affiliate_logo;	

					//New code for AWS 24-9-16//
					if($copy_status == 1)
					{
						$filepath = 'uploads/ads/'.$affiliate_logo;
						$tmp 	  = $_FILES['affiliate_logo']['tmp_name'];
						$this->load->library('S3');
						$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
						$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
					}
					//End//

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if($affiliate_logo!="" && (!$this->upload->do_upload('affiliate_logo')))
					{
						$affiliate_logoerror = $this->upload->display_errors();
					}
					
					if(isset($affiliate_logoerror))
					{
						$this->session->set_flashdata('error',$affiliate_logoerror);
						redirect('adminsettings/editads/'.$ads_id,'refresh');
					}
				}
				else {
					$affiliate_logo = $this->input->post('hidden_img');
				}
				
				$updated = $this->admin_model->updateads($affiliate_logo);
				if($updated){
					$data['action']="Edit";
					$this->session->set_flashdata('success', ' Ads details updated successfully.');
					redirect('adminsettings/editads/'.$ads_id,'refresh');
				}
				else{
					$data['action']="Edit";
					$this->session->set_flashdata('error', 'Error occurred while updating Ads details.');
					redirect('adminsettings/editads/'.$ads_id,'refresh');
				}
			}	
		}
	}
	
	function editads($ads_id)
	{
		$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$get = $this->admin_model->get_ads($ads_id);
				$data['ads_id'] = $get->ads_id;
				$data['ads_image'] = $get->ads_image;
				$data['ads_url'] = $get->ads_url;
				$data['ads_position'] = $get->ads_position;
			$data['action'] = "edit";
	
			$this->load->view('adminsettings/edit_ads',$data);
		}
	}
	
	function contacts()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		}
		else
		{
				if($this->input->post('hidd'))
				{
					$results = $this->admin_model->multi_delete_contacts();
					if($results)
					{
						$this->session->set_flashdata('success', 'Contacts deleted successfully.');
						redirect('adminsettings/contacts','refresh');
					}
					else
					{
						$this->session->set_flashdata('error', 'Error occurred while deleted Contacts details.');
						redirect('adminsettings/contacts','refresh');
					}			
				}
			$data['contacts'] = $this->admin_model->contacts();
			$this->load->view('adminsettings/contacts',$data);
		}
	}
	
	function deletecontact($id)
	{
		$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deletecontact($id);
			if($deletion){
				$data['action']="Edit";
				$this->session->set_flashdata('success', ' Contact details deleted successfully.');
				redirect('adminsettings/contacts','refresh');
			}
			else{
				$data['action']="Edit";
				$this->session->set_flashdata('error', 'Error occurred while deleting Contact details.');
				redirect('adminsettings/contacts','refresh');
			}
		}
	}
	
	function download_free_coupons($status)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			$this->load->helper('csv_helper');
			$result =$this->admin_model->download_free_coupons();
			
				$t_date=date('Y-m-d');
				$filename="coupons-".$t_date.".xls";
				
				$test=" offer Name  \t Title \t Description \t Type \t Code \t  Offer Page \t start date \t Expiry \t Featured \t Exclusive \t Tracking Extra parameter";
				   $test.="\n";
					if(isset($result)){
					  $k=1;
					  foreach($result as $row)
					  {
						   $offer_name=$row->offer_name;
						   $title=$row->title;
						   $description=$row->description;
						   $type=$row->type;
						   $code=$row->code;
						   $offer_page=$row->offer_page;
						   $start_date=$row->start_date;
						   $expiry_date=$row->expiry_date;
						   $featured=$row->featured;
						   //$description = $row->description;
						   $exclusive = $row->exclusive;
						   $Tracking = $row->Tracking;
						   					   
						   $test.=$offer_name."\t".$title."\t".$description."\t".$type."\t".$code."\t".$offer_page."\t".$start_date."\t".$expiry_date."\t".$featured."\t".$exclusive."\t".$Tracking;
						   $test.="\n";
						  
					   } 
				   }
				   print_r($test);
				   
				header("Content-type: application/csv");
				//header("Content-type: application/vnd.ms-word");
				//header("Content-type: text/plain");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 2");
				$this->load->helper('file');
				write_file('./backup/'.$filename, $test);
				$data = file_get_contents("./backup/".$filename);
				$urfile="coupons-".$t_date.".csv";
				$this->load->helper('download');
				force_download($urfile, $data); 
			}
	}
	
	
	
	// add store contents..
	function addstore_cashback($store_id)
	{
		$this->input->session_helper();
	
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} 
			$data['action']='new';
			
			$data['store_id']=$store_id;
			$data['store_details'] = $this->admin_model->site_get_store($store_id);			
			$this->load->view('adminsettings/update_catecashback',$data);
	}
	
	// view store cashback content
	function editstore_cashback($store_id,$cashbackid){
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		} 
		else
		{
			$data['store_details'] = $this->admin_model->site_get_store($store_id);			
			$cashback_details = $this->admin_model->cashback_details_byid($cashbackid);
				$data['caskbackid'] = $cashback_details->cbid;				
				$data['cashback_type'] = $cashback_details->cashback_type;
				$data['cashback'] = $cashback_details->cashback;
				$data['cashback_details'] = $cashback_details->cashback_details;
				$data['status'] = $cashback_details->status;
			$data['action'] = "edit";
			$this->load->view('adminsettings/update_catecashback',$data);
		}
	}
	
	// update cms contents..	
	function updatestore_cashback(){
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				
					if($action=='new')
					{
						$results = $this->admin_model->update_catecashback_ins();
					}
					else
					{
						$results = $this->admin_model->update_catecashback_ins($caskbackid);
					}
						if($results){
							$this->session->set_flashdata('success', ' Payment details Updated successfully.');
							redirect('adminsettings/category_cashback/'.$store_id,'refresh');
						}
						else{
							$this->session->set_flashdata('error', 'Error occurred while updating Payment details .');
							redirect('adminsettings/update_catecashback/'.$category_id.'/'.$store_id.'/'.$action,'refresh');
						}
						
			}		
		}
	}
	
	// delete cms content 
	function deletestore_cashback($id)
	{
		$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->deletecms($id);
			if($deletion){
				$data['action']="Edit";
				$this->session->set_flashdata('success', ' CMS details deleted successfully.');
				redirect('adminsettings/cms','refresh');
			}
			else{
				$data['action']="Edit";
				$this->session->set_flashdata('error', 'Error occurred while deleting CMS details.');
				redirect('adminsettings/cms','refresh');
			}
		}
	}
	
	
	function getcitys_listjson($query)
	{
		$this->input->session_helper();
		if($query)
		{
			$citys_list = $this->admin_model->get_typehead_citys_list($query);
		}
		echo json_encode($citys_list);
	}
	
	//
	function show_list(){
		
		$res['result'] = $this->admin_model->show_list();
		$this->load->view('adminsettings/show_list',$res);
			
	}
	/* seetha 24/10/2015 */
	function pending_cashback($name,$user_id,$process=null,$cashback_id=null)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$user_access=$this->session->userdata('user_access');
		if($admin_id=="")
		{
			redirect('adminsettings/dashboard','refresh');
		} 
		else 
		{	 
			if($name == 'user')
			{
				if($name !='' && $user_id!='')
				{ 
					$data['user_detail'] 	  = $this->admin_model->view_user($user_id);
					
					if($data['user_detail'])
					{
						$this->load->view('adminsettings/viewuser',$data);	
					}
					else
					{
						redirect('adminsettings/index','refresh');
					}
				}
			}
			else
			{
				//New code for multiple delete records 6/4/16//	
				if($_POST['deletecashback'])
				{

					$results 	 = $this->admin_model->delete_multi_cashbacks();
					if($results){
						$this->session->set_flashdata('success', 'Cashback deleted successfully.');
						redirect('adminsettings/pending_cashback','refresh');
					} else {
						$this->session->set_flashdata('error', 'Error occurred while deleting cashback.');
						redirect('adminsettings/pending_cashback','refresh');
					}
				}
				//End//
				//New code for multiple Approve records 6/4/16//		
				if($_POST['approvecashback'])
				{
				 	
					$results = $this->admin_model->approve_multi_cashbacks();
					if($results){
						$this->session->set_flashdata('success', 'Cashback approved successfully.');
						redirect('adminsettings/pending_cashback','refresh');
					} else {
						$this->session->set_flashdata('error', 'Error occurred while approving cashback.');
						redirect('adminsettings/pending_cashback','refresh');
					}
				}	
				//End//
				//New code for multiple Cancel records 6/4/16//	
				if($_POST['cancelcashback'])
				{
					 
					$results = $this->admin_model->cancel_multi_cashbacks();
					if($results){
						$this->session->set_flashdata('success', 'Cashback canceled successfully.');
						redirect('adminsettings/pending_cashback','refresh');
					} else {
						$this->session->set_flashdata('error', 'Error occurred while canceled cashback.');
						redirect('adminsettings/pending_cashback','refresh');
					}
				}	
				//End//

				switch($name)
				{

					case "approve":
						$results = $this->admin_model->approve_cashback($user_id);
						if($results){
							$this->session->set_flashdata('success', 'Cashback approved successfully.');
							redirect('adminsettings/pending_cashback','refresh');
						} else {
							$this->session->set_flashdata('error', 'Error occurred while approving cashback.');
							redirect('adminsettings/pending_cashback','refresh');
						}
					break;
					case "delete":
						$deletion = $this->admin_model->deletecashback($user_id);
						if($deletion){
							$this->session->set_flashdata('success', 'Cashback deleted successfully.');
							redirect('adminsettings/pending_cashback','refresh');
						} else {
							$this->session->set_flashdata('error', 'Error occurred while deleting cashback.');
							redirect('adminsettings/pending_cashback','refresh');
						}
					break;
					case "cancel":
						$deletion = $this->admin_model->cancel_cashback($user_id);
						if($deletion){
							$this->session->set_flashdata('success', 'Cashback canceled successfully.');
							redirect('adminsettings/pending_cashback','refresh');
						} else {
							$this->session->set_flashdata('error', 'Error occurred while canceled cashback.');
							redirect('adminsettings/pending_cashback','refresh');
						}
					break;
					default:
						if($this->input->post('hidd'))   //delete multiple cashback  seetha
						{	
							if($this->input->post('chkbox'))
							{
								
								$sort_order = $this->input->post('chkbox');					 
								$results = $this->admin_model->delete_bulk_records($sort_order,'cashback','cashback_id');
							}				
							
							if($results){
								
								$this->session->set_flashdata('success', 'Cashback details deleted successfully.');
								redirect('adminsettings/pending_cashback','refresh');
							}
							else{
								$this->session->set_flashdata('error', 'Error occurred while updating cashback details.');
								redirect('adminsettings/pending_cashback','refresh');
							}
						}	
						//$data['cashbacks'] = $this->admin_model->pending_cashback();
						$this->load->view('adminsettings/pending_cashback',$data);
					break;	
				}
			}	
		}	
	}

	/*New code for Pending cashback details update record details 23-3-17*/
	/*function update_pending_cashback($name,$user_id,$process=null,$cashback_id=null)
	{
		
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$user_access=$this->session->userdata('user_access');
		if($admin_id=="")
		{
			redirect('adminsettings/dashboard','refresh');
		} 
		else 
		{	 
			
			if($name == 'user')
			{
				if($name !='' && $user_id!='')
				{ 
					$data['user_detail'] 	  = $this->admin_model->view_user($user_id);
					
					if($data['user_detail'])
					{
						$this->load->view('adminsettings/viewuser',$data);	
					}
					else
					{
						redirect('adminsettings/index','refresh');
					}
				}
			}
			else
			{
				 
				//New code for multiple delete records 6/4/16//	
				if($_POST['deletecashback'])
				{

					$results 	 = $this->admin_model->delete_multi_cashbacks();
					if($results){
						$this->session->set_flashdata('success', 'Cashback deleted successfully.');
						redirect('adminsettings/pending_cashback','refresh');
					} else {
						$this->session->set_flashdata('error', 'Error occurred while deleting cashback.');
						redirect('adminsettings/pending_cashback','refresh');
					}
				}
				//End//
				//New code for multiple Approve records 6/4/16//		
				if($_POST['approvecashback'])
				{
				 	
					$results = $this->admin_model->approve_multi_cashbacks();
					if($results){
						$this->session->set_flashdata('success', 'Cashback approved successfully.');
						redirect('adminsettings/pending_cashback','refresh');
					} else {
						$this->session->set_flashdata('error', 'Error occurred while approving cashback.');
						redirect('adminsettings/pending_cashback','refresh');
					}
				}	
				//End//
				//New code for multiple Cancel records 6/4/16//	
				if($_POST['cancelcashback'])
				{
					 
					$results = $this->admin_model->cancel_multi_cashbacks();
					if($results){
						$this->session->set_flashdata('success', 'Cashback canceled successfully.');
						redirect('adminsettings/pending_cashback','refresh');
					} else {
						$this->session->set_flashdata('error', 'Error occurred while canceled cashback.');
						redirect('adminsettings/pending_cashback','refresh');
					}
				}	
				//End//
				echo $process; exit;
				switch($name)
				{

					case "approve":
						$results = $this->admin_model->approve_cashback($cashback_id);
						echo $results;
						if($results){
							$this->session->set_flashdata('success', 'Cashback approved successfully.');
							redirect('adminsettings/pending_cashback','refresh');
						} else {
							$this->session->set_flashdata('error', 'Error occurred while approving cashback.');
							redirect('adminsettings/pending_cashback','refresh');
						}
					break;
					case "delete":
						$deletion = $this->admin_model->deletecashback($cashback_id);
						if($deletion){
							$this->session->set_flashdata('success', 'Cashback deleted successfully.');
							redirect('adminsettings/pending_cashback','refresh');
						} else {
							$this->session->set_flashdata('error', 'Error occurred while deleting cashback.');
							redirect('adminsettings/pending_cashback','refresh');
						}
					break;
					case "cancel":
						$deletion = $this->admin_model->cancel_cashback($cashback_id);
						if($deletion){
							$this->session->set_flashdata('success', 'Cashback canceled successfully.');
							redirect('adminsettings/pending_cashback','refresh');
						} else {
							$this->session->set_flashdata('error', 'Error occurred while canceled cashback.');
							redirect('adminsettings/pending_cashback','refresh');
						}
					break;
					default:
						if($this->input->post('hidd'))   //delete multiple cashback  seetha
						{	
							if($this->input->post('chkbox'))
							{
								
								$sort_order = $this->input->post('chkbox');					 
								$results = $this->admin_model->delete_bulk_records($sort_order,'cashback','cashback_id');
							}				
							
							if($results){
								
								$this->session->set_flashdata('success', 'Cashback details deleted successfully.');
								redirect('adminsettings/pending_cashback','refresh');
							}
							else{
								$this->session->set_flashdata('error', 'Error occurred while updating cashback details.');
								redirect('adminsettings/pending_cashback','refresh');
							}
						}	
						//$data['cashbacks'] = $this->admin_model->pending_cashback();
						$this->load->view('adminsettings/pending_cashback',$data);
					break;	
				}
			}	
		}
	}*/
	/*end 23-3-17*/
	
	function pending_referral($name,$user_id,$process=null,$txn_id=null){
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$user_access=$this->session->userdata('user_access');
		if($admin_id==""){
			redirect('adminsettings/dashboard','refresh');
		} 
		/* else if((!(in_array('13',$user_access))) && $admin_id!=1) {
			redirect('adminsettings/index','refresh');
		}*/
		else 
		{
			if($name !='' && $user_id!='')
			{
				$data['user_detail'] = $this->admin_model->view_user($user_id);
				if($data['user_detail'])
				{
					$this->load->view('adminsettings/viewuser',$data);	
				}
				else
				{
					redirect('adminsettings/dashboard','refresh');
				}
			}
			else
			{
				//New code for multiple delete records 6/4/16//	
				if($_POST['deletereferral'])
				{
					$results 	 = $this->admin_model->delete_multi_referral();
					if($results){
						$this->session->set_flashdata('success', 'Referral deleted successfully.');
						redirect('adminsettings/pending_referral','refresh');
					} else {
						$this->session->set_flashdata('error', 'Error occurred while deleting Referral.');
						redirect('adminsettings/pending_referral','refresh');
					}
				}
				//End//

				//New code for multiple Approve records 6/4/16//		
				if($_POST['approvereferral'])
				{
				 
					$results = $this->admin_model->approve_multi_referral();
					if($results){
						$this->session->set_flashdata('success', 'Referral approved successfully.');
						redirect('adminsettings/pending_referral','refresh');
					} else {
						$this->session->set_flashdata('error', 'Error occurred while approving Referral.');
						redirect('adminsettings/pending_referral','refresh');
					}
				}	
				//End//

				//New code for multiple Cancel records 6/4/16//	
				if($_POST['cancelreferral'])
				{
					 
					$results = $this->admin_model->cancel_multi_referral();
					if($results){
						$this->session->set_flashdata('success', 'Referral canceled successfully.');
						redirect('adminsettings/pending_referral','refresh');
					} else {
						$this->session->set_flashdata('error', 'Error occurred while canceled Referral.');
						redirect('adminsettings/pending_referral','refresh');
					}
				}	
				//End//
				switch($process)
				{
					case "approve":
						$results = $this->admin_model->approve_referral($txn_id);
						if($results){
							$this->session->set_flashdata('success', 'Referral approved successfully.');
							redirect('adminsettings/pending_referral','refresh');
						} else {
							$this->session->set_flashdata('error', 'Error occurred while approving referral.');
							redirect('adminsettings/pending_referral','refresh');
						}
					break;
					case "cancel":
						$results = $this->admin_model->cancel_referral($txn_id);
						if($results){
							$this->session->set_flashdata('success', 'Referral canceled successfully.');
							redirect('adminsettings/pending_referral','refresh');
						} else {
							$this->session->set_flashdata('error', 'Error occurred while canceling referral.');
							redirect('adminsettings/pending_referral','refresh');
						}
					break;
					default:
						if($this->input->post('hidd'))
						{
							if($this->input->post('chkbox'))
							{
								$sort_order = $this->input->post('chkbox');
								$results = $this->admin_model->delete_bulk_records($sort_order,'transation_details','trans_id');
							}
							if($results){
								$this->session->set_flashdata('success', 'Referral details updated successfully.');
								redirect('adminsettings/pending_referral','refresh');
							}
							else{
								$this->session->set_flashdata('error', 'Error occurred while updating referral details.');
								redirect('adminsettings/pending_referral','refresh');
							}
						}
						$data['coupons'] = $this->admin_model->pending_referral();
						$this->load->view('adminsettings/pending_referral',$data);
					break;	
				}
			}	
		}	
	}
	/* seetha 24/10/2015 */


	//Affiliate Network API 16/3/16//

	function affiliate_network($name)
	{
		$this->input->session_helper();
		$admin_id 	 = $this->session->userdata('admin_id');
		$user_access = $this->session->userdata('user_access');
		
		if($admin_id == "")
		{
			redirect('adminsettings/index','refresh');
		} 
		else
		{
			$data['affiliate_name']	     = $name; 
			//$data['affiliate_network'] = $this->admin_model->affiliate_network($name);
			$this->load->view('adminsettings/affiliate_network',$data);
		}	
	}
	function add_affiliate()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$user_access=$this->session->userdata('user_access');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		}
		else
		{
			if($this->input->post('save'))
			{
				$flag=0;
				$affiliate_name   = $this->input->post('affiliate_name');
				$api_key       	  = $this->input->post('api_key');
				$networkid 		  = $this->input->post('networkid');
				//$signature_id   = $this->input->post('signature_id');
				$affiliate_logo   = $_FILES['affiliate_logo']['name'];
				$affiliate_status = $this->input->post('affiliate_status');

				if($affiliate_logo=="")
				{
					$flag=1;
					$this->session->set_flashdata('affiliate_logo',$affiliate_logo);
					$this->session->set_flashdata('error', 'Please upload an image .');
					redirect('adminsettings/add_affiliate','refresh');
				}				
				else 
				{
					$flag=0;
					if($affiliate_logo!="") 
					{
						$new_random = mt_rand(0,99999);
						$affiliate_logo = $_FILES['affiliate_logo']['name'];
						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($affiliate_logo);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $affiliate_logo);
						$affiliate_logo = $newfilename.$new_random.'.'.$file_ex;
						/*End 3-10-16*/

						//$affiliate_logo = remove_space($new_random.$affiliate_logo);
						$config['upload_path']   ='uploads/affiliates';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						///$config['max_width']  = '150';
						//$config['max_height']  = '93';
						$config['file_name']     = $affiliate_logo;
						
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($affiliate_logo!="" && (!$this->upload->do_upload('affiliate_logo')))
						{
							$affiliate_logoerror = $this->upload->display_errors();
						}
						if(isset($affiliate_logoerror))
						{
							$flag=1;
							$this->session->set_flashdata('affiliate_name',$affiliate_name);
							$this->session->set_flashdata('error',$affiliate_logoerror);
							redirect('adminsettings/add_affiliate','refresh');
						}
					}
					if($flag==0)
					{
						$results = $this->admin_model->add_affiliate($affiliate_logo);
						if($results){
							$this->session->set_flashdata('success', ' Affiliate Network details added successfully.');
							redirect('adminsettings/affiliate_network','refresh');
						}
						else{
							$this->session->set_flashdata('error', 'Error occurred while adding affiliate network.');
							redirect('adminsettings/add_affiliate','refresh');
						}
					}
				}
			}	
			$data['action']='new';
			$this->load->view('adminsettings/add_affiliate',$data);
		}
	}	
	
	
	// edit affiliate_list
	function edit_affiliate($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$user_access=$this->session->userdata('user_access');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			$affiliates = $this->admin_model->get_affiliate_list($id);
			foreach($affiliates as $get)
			{
				$data['affiliate_id'] 	   = $get->id;
				$data['affiliate_network'] = $get->affiliate_network;
				$data['api_key'] 	   	   = $get->api_key;
				$data['networkid'] 		   = $get->networkid;
				$data['signature_id'] 	   = $get->signature_id;
				$data['affiliate_logo']    = $get->affiliate_logo;
				$data['status'] 		   = $get->status;
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/add_affiliate',$data);
		}
	}
	
	//update affiliate_list
	function update_affiliate()
	{
		//echo "<pre>"; print_r($_POST); exit;
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$user_access=$this->session->userdata('user_access');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{		
			if($this->input->post('save'))
			{
				$flag=0;
				
				$affiliate_network = $this->input->post('affiliate_network');
				$affiliate_logo    = $this->input->post('affiliate_logo');
				$affiliate_logo    = $_FILES['affiliate_logo']['name'];
				
				if($affiliate_logo!="") 
				{
					$new_random = mt_rand(0,99999);
					$affiliate_logo = $_FILES['affiliate_logo']['name'];
					//$banner_image = $new_random.$banner_image;

					/*New code for image name field changes 3-10-16*/
					$info 	 	 	= new SplFileInfo($affiliate_logo);
					$file_ex 	 	= $info->getExtension();
					$newfilename 	= str_replace('.'.$file_ex,'', $affiliate_logo);
					$affiliate_logo = $newfilename.$new_random.'.'.$file_ex;
					/*End 3-10-16*/

					//$affiliate_logo = remove_space($new_random.$affiliate_logo);
					$config['upload_path']   = 'uploads/affiliates';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					//$config['max_width']  	 = '150';
					//$config['max_height']    = '93';
					$config['file_name']     = $affiliate_logo;
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if($affiliate_logo!="" && (!$this->upload->do_upload('affiliate_logo')))
					{
						$affiliate_logoerror = $this->upload->display_errors();
					}
					if(isset($affiliate_logoerror))
					{
						$flag=1;
						$this->session->set_flashdata('affiliate_logo',$affiliate_logo);
						$this->session->set_flashdata('error',$affiliate_logoerror);
						redirect('adminsettings/addaffiliate_list','refresh');
					}
				}
				else 
				{
					$flag=0;
					$affiliate_logo = $this->input->post('hidden_img');
				}
				if($flag==0)
				{
					$results = $this->admin_model->update_affiliate($affiliate_logo);
					if($results)
					{
						$this->session->set_flashdata('success', ' Affiliate Network details updated successfully.');
						redirect('adminsettings/affiliate_network/'.$affiliate_network,'refresh');
					}
					else
					{
						$this->session->set_flashdata('error', 'Error occurred while updating affiliate network.');
						redirect('adminsettings/addaffiliate_list','refresh');
					}
				}
			}	
			$data['action']='new';
			$this->load->view('adminsettings/addaffiliate_list',$data);
		}
	}
	// delete affiliate_list
	function delete_affiliate($id)
	{

		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$user_access=$this->session->userdata('user_access');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			$deletion = $this->admin_model->delete_affiliate($id);
			if($deletion)
			{
				//$data['action']="Edit";
				$this->session->set_flashdata('success', ' Affiliate Network details deleted successfully.');
				redirect('adminsettings/affiliate_network','refresh');
			}
			else
			{
				//$data['action']="Edit";
				$this->session->set_flashdata('error', 'Error occurred while deleting affiliate network  details.');
				redirect('adminsettings/affiliate_network','refresh');
			}
		}
	}
	function get_coupons($connection_id,$secretkey,$date,$tracking_id,$api_name)
	{	  	 
		$this->input->session_helper();
		$admin_id 	 = $this->session->userdata('admin_id');
		$user_access = $this->session->userdata('user_access');
		$api_name    = $this->input->post('api_name');

		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else if((!(in_array('6',$user_access))) && $admin_id!=1) 
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			$flag = 0;
			if($api_name == 'zanox')
			{

				$connection_id  = '6D2504B4C68860782BBA';
				$secret_key     = '7bf98038714c45+4b20e6aec684570/1CBef494c';
				$url 		    = 'http://api.zanox.com/json/2011-03-01/incentives/?connectid='.$connection_id.'&region=BR&adspace=2149716&incentiveType=coupons';
				$content        = json_decode(file_get_contents($url),true);
				$content_status = $content['items'];
				//echo "Zanox <pre>"; print_r($content); exit;
			}

			if($api_name == 'cityads')
			{
				
				$token_id    	= '3d7b3dd997d6e957b8924240eb6097cb7a02ed549c9c5d0523f500ea33f366e0';
				$client_id   	= '649885';
				$secret_key  	= '051cb84b51716df8bad6ad90c1083d8e'; 
				$remote_auth 	= '5049f98b30b43c185c8917fb719576dc';
				$url         	= 'http://api.cityads.com/api/rest/webmaster/xml/coupons?geo=32&remote_auth='.$remote_auth;
				
				//echo $url; exit; 
				$content_url	= file_get_contents($url);
				$xml_content    = simplexml_load_string($content_url);
				$json_content   = json_encode($xml_content);
				$content        = json_decode($json_content,TRUE);
				//echo "<br>"; print_r($content); exit;
				$content_status = $content['status'];
			} 

			if($api_name == 'rakuten')
			{

				$token_id  		= '3d0ad0eda6d7a35fc31a685efbf9d19c82475bbb20929b5c060886cbae822535';
				$url   	   		= 'http://couponfeed.linksynergy.com/coupon?token='.$token_id;
				$content_url	= file_get_contents($url);
				$xml_content    = simplexml_load_string($content_url);
				$json_content   = json_encode($xml_content);
				$content        = json_decode($json_content,TRUE);
				$content_status = $content['TotalMatches'];
				//echo "<pre>"; print_r($content); exit;
			}
			
			if($api_name == 'afilio')
			{

				$username      = 'pingou';
				$password      = 'yhasdf@fm16';
				$token_id      = '57581974981921.99713763';
				$site_id       = '47777';
				$aff_id        = '42747';
				$url 		   = 'http://v2.afilio.com.br/api/feedproducts.php?token='.$token_id.'&mode=dl&siteid='.$site_id.'&affid='.$aff_id; //.'&format=XML'
				 
				//'http://v2.afilio.com.br/api/feedproducts.php?token=57581974981921.99713763&mode=dl&siteid=47777&affid=42747'


				/*New code for csv file to convert a response details start 18-6-16.*/
				// Arrays we'll use later
				$keys     = array();
				$newArray = array();
				// Function to convert CSV into associative array
				function csvToArray($file, $delimiter) 
				{ 
				  	if (($handle = fopen($file, 'r')) !== FALSE)
				  	{ 
				    	$i = 0; 
				    	while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) 
				    	{ 
				      		for ($j = 0; $j < count($lineArray); $j++) 
				      		{ 
				        		$arr[$i][$j] = $lineArray[$j]; 
				      		} 
				      		$i++; 
				    	} 
				    	fclose($handle); 
					} 
				  	return $arr; 
				} 
				
				// Do it
				$data = csvToArray($url, ';');
				//echo "<pre>";print_r($data); exit;
				// Set number of elements (minus 1 because we shift off the first row)
				$count = count($data) - 1; 
				//Use first row for names  
				// $labels = array_shift($data);  
				//print_r($data[0]); exit;
				foreach ($data[0] as $label) 
				{ 
				  $keys[] = $label; 
				}  
				 
				// Add Ids, just in case we want them later
				$keys[] = 'type';
				
				for ($i = 1; $i < $count; $i++)
				{
				  $data[$i][] = $i;
				}
				// Bring it all together
				for ($j = 1; $j < $count; $j++) 
				{
				  $d = array_combine($keys,$data[$j]);
				  $newArray[$j] = $d;
				}

				$content = $newArray;
				//echo "<pre>"; print_r($content); exit;
				$content_status= 1;
				/*End 18-6-16*/
			}

			

			if($content_status!=0)
			{				
				if($flag==0)
				{
					$results = $this->admin_model->import_apicoupons($content,$api_name);
					//$results = $this->admin_model->import_coupons($content);
					//print_r($results);die;
					/* if($results){
						echo $results['duplicate'];
					} */
					$msg =array();
					if($results['duplicate'] == 0)
					{
						/* $this->session->set_flashdata('success', ' Coupon details added successfully.');
						redirect('adminsettings/affiliate_network','refresh'); */
						$msg['success'] = 'Coupon details added successfully.';
					}
					else if($results['duplicate']!=0){
						
						$msg['success'] = 'New Coupon details added successfully and <span style="color:red">'.$results['duplicate'].'</span> duplicate records neglected. The duplicate promo ids are '.$results['promo_id'];
					}
				}
			}
			else {
				$msg['success'] = 'Network ID or Token is Missing.';
			}
			echo json_encode($msg);		
		}		
	}
	//END 16/3/16//

	//API Coupons 16/3/16//

	// view all coupons..	
	function api_coupons($store_name=null){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
	$user_access=$this->session->userdata('user_access');
	
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else if((!(in_array('6',$user_access))) && $admin_id!=1) {
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				 if($this->input->post('chkbox'))
				 {
					$results = $this->admin_model->api_coupons_bulk_delete();
				 }		
				
				if($results){
					
					$this->session->set_flashdata('success', 'Coupons details Deleted successfully.');
					redirect('adminsettings/api_coupons','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while deleting Coupon details.');
					redirect('adminsettings/api_coupons','refresh');
				}
			}
			$data['api_coupons'] = $this->admin_model->api_coupons($store_name);
			$this->load->view('adminsettings/api_coupons',$data);
		}
	}	
	//view particular coupon details..
	function api_editcoupon($coupon_id){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
	$user_access=$this->session->userdata('user_access');
		if($admin_id==""){
			redirect('adminsettings/dashboard','refresh');
		} else if((!(in_array('6',$user_access))) && $admin_id!=1) {
			redirect('adminsettings/index','refresh');
		}else {
			$coupons = $this->admin_model->api_editcoupon($coupon_id);
			foreach($coupons as $get){
				$data['coupon_id'] = $get->coupon_id;
				$data['offer_name'] = $get->offer_name;
				$data['category_name'] = $get->category_name;
				$data['title'] = $get->title;
				$data['description'] = $get->description;
				$data['type'] = $get->type;
				$data['code'] = $get->code;
				$data['offer_page'] = $get->offer_page;
				$data['expiry_date'] = $get->expiry_date;
				$data['start_date'] = $get->start_date;
				$data['featured'] = $get->featured;	//28/11/14 Suhirdha added starts ...
				$data['exclusive'] = $get->exclusive;	//28/11/14 Suhirdha ends...	
				$data['Tracking'] = $get->Tracking;	
				$data['coupon_options'] = $get->coupon_options;					
				$data['cashback_description'] = $get->cashback_description;					
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/api_addcoupons',$data);			
		}
	}
	
	//update coupon..
	function api_updatecoupon(){
	$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$user_access=$this->session->userdata('user_access');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		}else if((!(in_array('6',$user_access))) && $admin_id!=1) {
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save')){
				$coupon_id = $this->input->post('coupon_id');
				$start_date = $this->input->post('start_date');
				$expiry_date =$this->input->post('expiry_date');
				if($start_date > $expiry_date)
				{	
					$this->session->set_flashdata('error', 'Please Enter valid Expiry date.');
					$data['action'] = "edit";
					redirect('adminsettings/api_editcoupon/'.$coupon_id,'refresh');
				}
				else 
				{
				
					$updated = $this->admin_model->api_updatecoupon();
					if($updated){
						$data['action']="Edit";
						$this->session->set_flashdata('success', ' Coupon details updated successfully.');
						redirect('adminsettings/api_coupons','refresh');
					}
					else{
						$data['action']="Edit";
						$this->session->set_flashdata('error', 'Error occurred while updating coupon details.');
						redirect('adminsettings/api_coupons','refresh');
					}
				}
			}
		}
	}
	
	//delete API coupon..
	function api_deletecoupon($delete_id,$api_name)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$user_access=$this->session->userdata('user_access');
		if($admin_id=="")
		{ 
			redirect('adminsettings/index','refresh');
		}
		else if((!(in_array('6',$user_access))) && $admin_id!=1) 
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			$deletion = $this->admin_model->api_deletecoupon($delete_id);
			if($deletion)
			{
				$this->session->set_flashdata('success', 'Coupon deleted successfully.');
				redirect('adminsettings/affiliate_network/'.$api_name,'refresh');
			}
			else
			{
				$this->session->set_flashdata('error', 'Error occurred while deleting coupon.');
				redirect('adminsettings/affiliate_network/'.$api_name,'refresh');
			}
		}
	}
	function api_change_approval($id,$status,$name)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$user_access=$this->session->userdata('user_access');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else if((!(in_array('6',$user_access))) && $admin_id!=1) {
			redirect('adminsettings/index','refresh');
		}else
		{
			$data['api_coupons'] = $this->admin_model->api_changestatus($id,$status,$name);
			redirect('adminsettings/affiliate_network/'.$name,'refresh');
		}
	
	}
	function api_download_free_coupons($status)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$user_access=$this->session->userdata('user_access');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}else if((!(in_array('6',$user_access))) && $admin_id!=1) {
			redirect('adminsettings/index','refresh');
		}
		else
		{
			//$this->load->helper('csv_helper');
			//$result =$this->admin_model->api_download_free_coupons();
			
				$t_date=date('Y-m-d');
				$filename="coupons-".$t_date.".xls";
			    $this->load->dbutil();
				$this->load->helper('download');
				$delimiter = ",";
				$newline = "\n";
				$query = $this->db->query("SELECT `offer_name`, `title`, `description`, `type` ,`code`,`offer_page`,`start_date`,`expiry_date`,`featured`,`exclusive`,`Tracking` FROM `coupons` ");
				$data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
				force_download($filename, $data);

				/*$test="offer Name  \t\t\t\t Title \t\t Description \t\t Type \t\t Code \t\t  Offer Page \t\t start date \t\t Expiry \t\t Featured \t\t Exclusive \t\t Tracking Extra parameter";
				   $test.="\n";
					if(isset($result)){
					  $k=1;
					  foreach($result as $row)
					  {
						   $offer_name=$row->offer_name;
						   $title=$row->title;
						   $description=stripcslashes($row->description);
						   $type=$row->type;
						   $code=$row->code;
						   $offer_page=$row->offer_page;
						   $start_date=$row->start_date;
						   $expiry_date=$row->expiry_date;
						   $featured=$row->featured;
						   //$description = $row->description;
						   $exclusive = $row->exclusive;
						   $Tracking = $row->Tracking;
						   					   
						   $test.=$offer_name."\t".$title."\t".$description."\t".$type."\t".$code."\t".$offer_page."\t".$start_date."\t".$expiry_date."\t".$featured."\t".$exclusive."\t".$Tracking;
						   $test.="\n";
						  
					   } 
				   }
				   
				   
				header("Content-type: application/csv");
				//header("Content-type: application/vnd.ms-word");
				//header("Content-type: text/plain");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 2");
				$this->load->helper('file');
				write_file('./backup/'.$filename, $test);
				$data = file_get_contents("./backup/".$filename);
				$urfile="coupons-".$t_date.".csv";
				$this->load->helper('download');
				force_download($urfile, $data); */
			}
	}

	//END 16/3/16//

	function add_user()
	{	
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			 
			$this->security->cookie_handlers();
			$email 	  = $this->input->post('mailid');
			$cat_type = $this->input->post('cat_type');
			if($email!="")
	    	{	 
	        	$vars = $this->admin_model->add_user($email,$cat_type);
				$this->security->cookie_handlers();
				echo $vars;	
	    	}
	    }	
	}
	function delete_user($userid,$type)
	{	
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$this->security->cookie_handlers();
			 
	        $updated = $this->admin_model->update_user($userid);
			if($updated)
			{
				$this->session->set_flashdata('success', 'User Details deleted successfully. ');
				redirect('adminsettings/category/'.$type,'refresh');
			}  
			else
			{
				$this->session->set_flashdata('error', 'User Details not deleted successfully.');
				redirect('adminsettings/category/'.$type,'refresh');
			}	
	    	 
	    }	
	}

	//End//
	function cat_details()
	{

		$this->input->session_helper();
		$categoryid = $this->input->post('category_id');
		$result = $this->admin_model->cat_details($categoryid);
		//echo "<pre>";print_r($result);
		if($result)
		{
			$i=0;
			foreach($result as $res)
			{
				$i++;
				$cat_name = $res->affiliate_name;
				$cat_id   = $res->affiliate_id;
				echo "<h5><input type='checkbox' name='categorys_list[]' value=$cat_id> ";
				echo $cat_name;
				echo "</h5><br>";
			}	
			echo "<input type='hidden' name='ivalue' value='".$i."'>";
			 
		} 
		else 
		{
			echo "<h5 style='color:red;'>Related stores Not found</h5>";
		}
	}

	function stores()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} 
		else
		{
			if($this->input->post('save'))
			{
				//$ivalue   = $this->input->post('ivalue');
				
				//for($i=0;$i<=$ivalue;$i++)
				//{
				//	$cat_list   = implode(",",$this->input->post('vehicle'.$i));
				//}
				// echo $cat_list;
				//print_r($_POST); exit;
				$store_id   = $this->input->post('category_name');
				$store_status = $this->input->post('status');
				$store_counts = $this->input->post('counts');
				 
				$selqry   = $this->db->query("SELECT category_name from categories where category_id='$store_id'")->row(); 
				$cat_name = $selqry->category_name;
				
				$this->security->cookie_handlers();
				$results = $this->admin_model->add_related_stores($store_id,$cat_name);
				if($results)
				{
					$this->session->set_flashdata('success', ' Related Store details added successfully.');
					redirect('adminsettings/stores','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while adding Related Store.');
					redirect('adminsettings/stores','refresh');
				}
			}	
			//$data['action'] = "new";
			$this->load->view('adminsettings/stores',$data);
		}
			
	}	


//Pilaventhiran 07/05/2016 START
	function missing_approval(){
		$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('hidd'))
			{
				
				 if($this->input->post('chkbox'))
				 {
					 $sort_order = $this->input->post('chkbox');
					 $results = $this->admin_model->delete_bulk_records($sort_order,'missing_cashback','cashback_id');
				 }
				
				
				if($results){
					
					$this->session->set_flashdata('success', 'Missing Approval details updated successfully.');
					redirect('adminsettings/missing_approval','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while updating Missing Approval details.');
					redirect('adminsettings/missing_approval','refresh');
				}
			}
			$data['allmissing_cashbacks'] = $this->admin_model->get_all_missing_approval();
			$this->load->view('adminsettings/missing_approval',$data);
		}	
	}
	

function view_missing_approval($cashbcakid){
		$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if(($admin_id=="") || ($cashbcakid=="")) {
			redirect('adminsettings/index','refresh');
		} else
		{
			$data['missing_cashback'] = $this->admin_model->view_missing_cb($cashbcakid);
			$this->load->view('adminsettings/view_missing_approval',$data);
		}	
	}

	function approvalupdate(){
		$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="") {
			redirect('adminsettings/index','refresh');
		} else
		{
		 	//echo "<pre>";print_r($_POST);
			//exit; 
			if($this->input->post('save')){
			$updated = $this->admin_model->missiing_approval_update();
				if($updated){
					$this->session->set_flashdata('success', 'Missing Approval details updated successfully.');
					redirect('adminsettings/missing_approval','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while updating Missing Approval details.');
					redirect('adminsettings/missing_approval','refresh');
				}
			}
		}	
	}

	function delete_missing_approval($id){
	$this->input->session_helper();
	$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$deletion = $this->admin_model->delete_missing_cashback($id);
			if($deletion){
				$this->session->set_flashdata('success', ' Missing Approval deleted successfully.');
				redirect('adminsettings/missing_approval','refresh');
			}
			else{
				$this->session->set_flashdata('error', 'Error occurred while deleting Missing Approval details.');
				redirect('adminsettings/missing_approval','refresh');
			}
		}
	}

	//Pilaventhiran 07/05/2016 END

				function user_information(){

							$this->input->session_helper();
							$admin_id = $this->session->userdata('admin_id');
							if($admin_id==""){
							redirect('adminsettings/index','refresh');
							}

	           $data['userinfo'] = $this->admin_model->user_information();

				$this->load->view('adminsettings/user_information',$data);
				}


//SATz user information
						function user_information_update(){

						//$in=$this->input->post();
						//print_r($in);exit();


									$this->input->session_helper();
									$admin_id = $this->session->userdata('admin_id');
									if($admin_id==""){
									redirect('adminsettings/index','refresh');
									}
						          
						             if($this->input->post('save')){

									$updated = $this->admin_model->user_information_update();
									if($updated)
									{
										$this->session->set_flashdata('success', 'User information Details updated successfully.');
										redirect('adminsettings/user_information','refresh');
									}  
									else
									{
										$this->session->set_flashdata('error', 'User Information not updated successfully.');
										redirect('adminsettings/user_information','refresh');
									}
								}	



						$this->load->view('adminsettings/user_information');
						}


	//SATz Sub Admin management controllers
	//manage sub admins
	function sub_admin($section=null,$sub_admin_id=null,$status=null)
	{
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==''){
			redirect('adminsettings/index','refresh');
		}
		else
		{ 	
			/*New code for amazon s3 copy settings 26-12-16*/
			$amazon_s3_details = $this->admin_model->amazon_s3_settings();
			$copy_status       = $amazon_s3_details->copy_files_status;
			/*End*/

			switch($section)
			{	
				case "add":
				if($this->input->post('save'))
				{
					$new_random = mt_rand(0,99999);
					$admin_logo = $_FILES['image']['name'];
					if($admin_logo!="") 
					{

						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($admin_logo);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $admin_logo);
						$admin_logo 	= $newfilename.$new_random.'.'.$file_ex;
						/*End 3-10-16*/
						
						//$admin_logo =remove_space($new_random.$admin_logo);
						//$admin_logo = format_filename($admin_logo);  // replaces the non alpha numeric characters with '_'
						$config['upload_path'] ='uploads/adminpro';
						$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp|ico';
						$config['file_name']=$admin_logo;
						
						if($copy_status == 1)
						{
							$contentType  = $_FILES['image']['type']; 
							$filepath 	  = 'uploads/adminpro/'.$admin_logo;
							$tmp 		  = $_FILES['image']['tmp_name'];
							
							//New code for AWS 26-7-16//
							$this->load->library('S3');
							$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
							$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
						}
						//End//

						$this->load->library('upload', $config);
						$this->upload->initialize($config);	
						if($admin_logo!="" && (!$this->upload->do_upload('image'))){
							$admin_logoerror = $this->upload->display_errors();
						}
						if(isset($admin_logoerror)){
							$this->session->set_flashdata('error',$admin_logoerror);
							redirect('adminsettings/sub_admin','refresh');
						}
					}
					$result = $this->admin_model->add_sub_admin($admin_logo);
					if($result){
						$this->session->set_flashdata('success', 'Sub admin account added successfully.');
						redirect('adminsettings/sub_admin','refresh');
					} else {
						$this->session->set_flashdata('error', 'Error occurred while adding the sub admin account.');
						redirect('adminsettings/sub_admin','refresh');
					}
				} else {
				$data['page_view'] = 'add';
				$this->load->view('adminsettings/sub_admin',$data);
				}
				
				break;
				case "edit":
					if($this->input->post('save'))
					{
						$new_random = mt_rand(0,99999);
						$admin_logo = $_FILES['update_image']['name'];
						if($admin_logo!=''){
							$new_random = mt_rand(0,99999);		


							/*New code for image name field changes 3-10-16*/
							$info 	 	 	= new SplFileInfo($admin_logo);
							$file_ex 	 	= $info->getExtension();
							$newfilename 	= str_replace('.'.$file_ex,'', $admin_logo);
							$admin_logo 	= $newfilename.$new_random.'.'.$file_ex;
							/*End 3-10-16*/

							$admin_logo = remove_space($new_random.$admin_logo);
							//$admin_logo = format_filename($admin_logo);  // replaces the non alpha numeric characters with '_'
							$config['upload_path'] ='uploads/adminpro';
							$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp|ico';
							$config['file_name']=$admin_logo;
							
							if($copy_status == 1)
							{
								$filepath = 'uploads/adminpro/'.$admin_logo;
								$tmp 	  = $_FILES['update_image']['tmp_name'];
								//New code for AWS 24-9-16//
								$this->load->library('S3');
								$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
								$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
							}
							//End//

							$this->load->library('upload', $config);
							$this->upload->initialize($config);	
							if($admin_logo!="" && (!$this->upload->do_upload('update_image'))){
								$admin_logoerror = $this->upload->display_errors();
							}
							if(isset($admin_logoerror)){
								$this->session->set_flashdata('error',$admin_logoerror);
								redirect('adminsettings/sub_admin','refresh');
							}
						}else {
							$admin_logo = $this->input->post('hidden_img');
						}
						$result = $this->admin_model->update_sub_admin($admin_logo);
						if($result){
							$this->session->set_flashdata('success', 'Sub admin account updated successfully.');
							redirect('adminsettings/sub_admin','refresh');
						} else {
							$this->session->set_flashdata('error', 'Error occurred while updating the sub admin account.');
							redirect('adminsettings/sub_admin','refresh');
						}
					} else {
						$details = $this->admin_model->get_sub_admin($sub_admin_id);
						if(!$details){
							// $this->session->set_flashdata('error', 'Invalid sub admin account.');
							redirect('adminsettings/sub_admin','refresh');
						} else {
							$data['sub_admin'] = $details;
							$data['page_view'] = 'edit';
							$this->load->view('adminsettings/sub_admin',$data);
						}
					}
				break;
				
				case "delete":
					$result = $this->admin_model->delete_sub_admin($sub_admin_id);
					if($result){
						$this->session->set_flashdata('success', 'Sub admin account deleted successfully.');
						redirect('adminsettings/sub_admin','refresh');
					} else {
						$this->session->set_flashdata('error', 'Error occurred while deleting the sub admin account.');
						redirect('adminsettings/sub_admin','refresh');
					}
				break;
				
				case "status":
					$result = $this->admin_model->change_sub_adminstatus($sub_admin_id,$status);
					if($result){
						if($status=='1'){
							$this->session->set_flashdata('success', 'Sub admin account activated successfully.');
							redirect('adminsettings/sub_admin','refresh');
						} else {
							$this->session->set_flashdata('success', 'Sub admin account de-activated successfully.');
							redirect('adminsettings/sub_admin','refresh');
						} 
					} else {
						$this->session->set_flashdata('error', 'Error occurred while changing the sub admin account.');
						redirect('adminsettings/sub_admin','refresh');
					}
				break;
				case "check":	// through ajax..
					$email = $this->input->post('email');
					if($email!=""){
						$result = $this->admin_model->check_sub_admin($email);
						if($result==0){
							echo 0;	// exists.. failure..
						} else {
							echo 1;	// not exists.. success..
						}
					}
				break;
				default:
					if($this->input->post('hidd')){
						$results = $this->admin_model->multi_delete_subadmin();
						if($results){		
							$this->session->set_flashdata('success', 'Users deleted successfully.');
							redirect('adminsettings/sub_admin','refresh');
						}else{
							$this->session->set_flashdata('error', 'Error occurred while deleted Users details.');
							redirect('adminsettings/sub_admin','refresh');
						}
					}else{
						$data['sub_admins'] = $this->admin_model->fetch_sub_admin();
						$data['page_view'] = 'list';
						$this->load->view('adminsettings/sub_admin',$data);
					}	
				break;
			}
		}
	}


	/*New code for upload preminum coupons vie csv file 10-5-16*/
	function upload_coupons()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
		  redirect('adminsettings/index','refresh');
		} else
		{		
			$flag=0;
			if($this->input->post('save'))
			{
				$bulkcoupon = $_FILES['bulkcoupon']['name'];
				if($bulkcoupon=="")
				{
					$flag=1;
					$this->session->set_flashdata('error', 'Please upload the file.');
					redirect('adminsettings/upload_coupons','refresh');
				}
				else 
				{
					$flag=0;
					if($bulkcoupon!="")
					{
						$new_random = mt_rand(0,99999);
						$bulkcoupon = $_FILES['bulkcoupon']['name'];

						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($bulkcoupon);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $bulkcoupon);
						$bulkcoupon 	= $newfilename.$new_random.'.'.$file_ex;
						/*End 3-10-16*/


						//$bulkcoupon = remove_space($new_random.$bulkcoupon);
						$config['upload_path'] ='uploads/premium_coupons';
						$config['allowed_types'] = '*';
						$config['max_size'] = '20000';
						$config['file_name']=$bulkcoupon;
						
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($bulkcoupon!="" && (!$this->upload->do_upload('bulkcoupon')))
						{
							$bulkcouponerror = $this->upload->display_errors();
						}
						if(isset($bulkcouponerror))
						{
							$flag=1;
							$this->session->set_flashdata('error',$bulkcouponerror);
							redirect('adminsettings/upload_coupons','refresh');
						}
					}
					
					$this->load->library('CSVReader');	
					$main_url = 'uploads/premium_coupons/'.$bulkcoupon; 	
					$result =   $this->csvreader->parse_file($main_url);
				
					if($flag==0)
					{

						$results = $this->admin_model->upload_coupons($bulkcoupon);
						if($results){
							$this->session->set_flashdata('success', ' Premium coupons details added successfully.');
							redirect('adminsettings/shoppingcoupons','refresh');
						}
						else{
							$this->session->set_flashdata('error', 'Error occurred while adding Premium coupons details.');
							redirect('adminsettings/upload_coupons','refresh');
						}
					}
				}
				$result = $this->admin_model->upload_coupons();
				if($result)
				{
					$this->session->set_flashdata('success', ' Premium coupons added successfully.');
					redirect('adminsettings/shoppingcoupons','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while adding Premium coupons.');
					redirect('adminsettings/upload_coupons','refresh');
				}
			}
			$data['action'] = "new";
			$this->load->view('adminsettings/upload_coupons',$data);
		}	
	}
	/*End 10-5-16*/

	
	/*New code for View all affiliate details 9-6-16*/
	function view_all_affiliates()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$user_access=$this->session->userdata('user_access');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else if((!(in_array('6',$user_access))) && $admin_id!=1) {
			redirect('adminsettings/index','refresh');
		}else
		{
			    if($this->input->post('hidd'))
				{	
						$results = $this->admin_model->delete_multi_affiliatenetworks();
								if($results){		
									$this->session->set_flashdata('success', 'Affiliate Network deleted successfully.');
									redirect('adminsettings/view_all_affiliates','refresh');
								}
								else
								{
								$this->session->set_flashdata('error', 'Error occurred while deleted affiliate network  details.');
								redirect('adminsettings/view_all_affiliates','refresh');
								}
				}
			$data['affiliate_network'] = $this->admin_model->affiliate_network();
			$this->load->view('adminsettings/view_all_affiliates',$data);
		}
	}
	
	/*end*/
	/*New code for Add a affiliates details 8-6-16*/

	function add_affiliates()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		$user_access=$this->session->userdata('user_access');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} 
		else
		{
			if($this->input->post('save'))
			{
				$flag=0;
				$affiliate_name   = $this->input->post('affiliate_name');
				$secret_key       = $this->input->post('secret_key');
				$connectid        = $this->input->post('connectid');
				$signature_id     = $this->input->post('signature_id');
				$affiliate_logo   = $_FILES['affiliate_logo']['name'];
				$affiliate_status = $this->input->post('affiliate_status');
				if($affiliate_logo=="")
				{
					$flag=1;
					$this->session->set_flashdata('affiliate_logo',$affiliate_logo);
					$this->session->set_flashdata('error', 'Please upload an image .');
					redirect('adminsettings/addaffiliate_list','refresh');
				}				
				else 
				{
					$flag=0;
					if($affiliate_logo!="") 
					{
						$new_random 			 = mt_rand(0,99999);
						$affiliate_logo 		 = $_FILES['affiliate_logo']['name'];

						/*New code for image name field changes 3-10-16*/
						$info 	 	 	= new SplFileInfo($affiliate_logo);
						$file_ex 	 	= $info->getExtension();
						$newfilename 	= str_replace('.'.$file_ex,'', $affiliate_logo);
						$affiliate_logo	= $newfilename.$new_random.'.'.$file_ex;
						/*End 3-10-16*/


						//$affiliate_logo 		 = remove_space($new_random.$affiliate_logo);
						$config['upload_path']   ='uploads/affiliates';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['file_name']	 = $affiliate_logo;
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						
						if($affiliate_logo!="" && (!$this->upload->do_upload('affiliate_logo')))
						{
							$affiliate_logoerror = $this->upload->display_errors();
						}
						if(isset($affiliate_logoerror))
						{
							$flag=1;
							$this->session->set_flashdata('affiliate_name',$affiliate_name);
							$this->session->set_flashdata('error',$affiliate_logoerror);
							redirect('adminsettings/add_affiliates','refresh');
						}
					}
					if($flag==0)
					{
						$results = $this->admin_model->add_affiliates($affiliate_logo);
						if($results)
						{
							$this->session->set_flashdata('success', ' Affiliate Network details added successfully.');
							redirect('adminsettings/view_all_affiliates','refresh');
						}
						else
						{
							$this->session->set_flashdata('error', 'Error occurred while adding affiliate network.');
							redirect('adminsettings/add_affiliates','refresh');
						}
					}
				}
			}	
			$data['action']='new';
			$this->load->view('adminsettings/add_affiliates',$data);
		}
	}	
	/*End*/

	
	/*New code for Upload Coupons details  7-6-16.*/
	/*function upload_apicoupons()
	{	
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');

		if($admin_id=="")
		{
		  redirect('adminsettings/index','refresh');
		} 
		else
		{	
			if($this->input->post('uploadtype'))
			{
				$upload_type    = $this->input->post('uploadtype');
				$affiliate_name = $this->input->post('aff_type');
				
				if($upload_type == 'Coupons_upload')
				{
					if($affiliate_name!='')
					{
						
						if($affiliate_name == 'zanox')
						{

							$connection_id  = '6D2504B4C68860782BBA';
							$secret_key     = '7bf98038714c45+4b20e6aec684570/1CBef494c';
							$url 		    = 'http://api.zanox.com/json/2011-03-01/incentives/?connectid='.$connection_id.'&region=BR&adspace=2149716&incentiveType=coupons';
							$content        = json_decode(file_get_contents($url),true);
							$content_status = $content['items'];
							//echo "Zanox <pre>"; print_r($content); exit;
						}

						if($affiliate_name == 'rakuten')
						{	
							
												
							$token_id  		= '3d0ad0eda6d7a35fc31a685efbf9d19c82475bbb20929b5c060886cbae822535';
							$url   	   		= 'http://couponfeed.linksynergy.com/coupon?token='.$token_id;
							$content_url	= file_get_contents($url);
							$xml_content    = simplexml_load_string($content_url);
							$json_content   = json_encode($xml_content);
							$content        = json_decode($json_content,TRUE);
							$content_status = $content['TotalMatches'];
							//echo "<pre>"; print_r($content); exit;
							//echo $content_status; exit;
						}
						
						if($affiliate_name == 'cityads')
						{	
							
							$token_id    	= '3d7b3dd997d6e957b8924240eb6097cb7a02ed549c9c5d0523f500ea33f366e0';
							$client_id   	= '649885';
							$secret_key  	= '051cb84b51716df8bad6ad90c1083d8e'; 
							$remote_auth 	= '5049f98b30b43c185c8917fb719576dc';
							$url         	= 'http://api.cityads.com/api/rest/webmaster/json/coupons?remote_auth='.$remote_auth;
							$content 	    = json_decode(file_get_contents($url),true);
							$content_status = $content['status'];
							//echo '<pre>';print_r($content);exit;
						} 
						
						if($affiliate_name == 'afilio')
						{	
			
							$username      = 'pingou';
							$password      = 'yhasdf@fm16';
							$token_id      = '57581974981921.99713763';
							$site_id       = '47777';
							$aff_id        = '42747';
							$url 		   = 'http://v2.afilio.com.br/api/feedproducts.php?token='.$token_id.'&mode=dl&siteid='.$site_id.'&affid='.$aff_id;
							
							//New code for csv file to convert a response details start 18-6-16.
							// Arrays we'll use later
							$keys = array();
							$newArray = array();
							// Function to convert CSV into associative array
							function csvToArray($file, $delimiter) 
							{ 
							  	if (($handle = fopen($file, 'r')) !== FALSE)
							  	{ 
							    	$i = 0; 
							    	while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) 
							    	{ 
							      		for ($j = 0; $j < count($lineArray); $j++) 
							      		{ 
							        		$arr[$i][$j] = $lineArray[$j]; 
							      		} 
							      		$i++; 
							    	} 
							    	fclose($handle); 
								} 
							  	return $arr; 
							} 
							
							// Do it
							$data = csvToArray($url, ';');
							//echo "<pre>";print_r($data); exit;
							// Set number of elements (minus 1 because we shift off the first row)
							$count = count($data) - 1; 
							//Use first row for names  
							// $labels = array_shift($data);  
							//print_r($data[0]); exit;
							foreach ($data[0] as $label) 
							{ 
							  $keys[] = $label; 
							}  
							 
							// Add Ids, just in case we want them later
							$keys[] = 'id';
							
							for ($i = 1; $i < $count; $i++)
							{
							  $data[$i][] = $i;
							}
							// Bring it all together
							for ($j = 1; $j < $count; $j++) 
							{
							  $d = array_combine($keys,$data[$j]);
							  $newArray[$j] = $d;
							}

							$content = $newArray;
							 
							//echo "<pre>"; print_r($content); exit;
							$content_status= 1;
							//End 18-6-16//
						}
						
						//echo $content_status; exit; 
						if($content_status!=0)
						{				
							if($flag==0)
							{
								$getdatas = $this->admin_model->import_apicoupons($content,$affiliate_name);
								
								if($getdatas['duplicate'] == 0)
								{
									$this->session->set_flashdata('success', 'Coupons details imported successfully.');
									redirect('adminsettings/upload_apicoupons','refresh');
								}
								else if($getdatas['duplicate']!=0)
								{		
									$this->session->set_flashdata('error', 'New Coupons details added successfully and <span style="color:red">'.$getdatas['duplicate'].'</span> duplicate records neglected. The duplicate transactions ids are '.$getdatas['trans_id']);
									redirect('adminsettings/upload_apicoupons','refresh');
								}					
							}
						}
						else 
						{
							$this->session->set_flashdata('error','No data found.');
							redirect('adminsettings/upload_apicoupons','refresh');
						}	
					}	
				}
			}		
				$data['action'] = "coupons";
				$this->load->view('adminsettings/upload_apicoupons',$data);
		}	
	}*/
	/*End*/

	/*New code for upload report details 7-6-16*/
	/*function upload_apireport()
	{	
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
		  redirect('adminsettings/index','refresh');
		} else
		{	
			//print_r($_POST); exit;
			if($this->input->post('uploadtype'))
			{
				$upload_type    = $this->input->post('uploadtype');
				$affiliate_name = $this->input->post('aff_type');
				
				if($upload_type == 'Report_upload')
				{
					if($affiliate_name!='')
					{
						//report response details Pending//
						if($affiliate_name == 'zanox')
						{

							$connection_id = '6BA59464117A45ADAB2D';
							$secret_key    = '7bf98038714c45+4b20e6aec684570/1CBef494c';
							$nonce		   = '7143D63A5353392FD3A11C67EC5B42A7';
							$signature     = 'AcMW31Nk1RPf3uy1IeHi73/pqjE=';
							$start_date    = $this->input->post('startdate');
							$end_date      = $this->input->post('enddate');

							//$newdate 	   = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
 							$date          = date('D, d M Y H:i:s');
							$url 		   = 'http://api.zanox.com/json/2011-03-01/reports/sales/date/2013-06-15?connectid='.$connection_id.'&nonce='.$nonce.'&signature='.$signature;
							$contents      = json_decode(file_get_contents($url),true);
							//echo "<pre>"; print_r($contents); exit; 
						}

						if($affiliate_name == 'lomadee')
						{
							$username     = 'fabriciocmello@gmail.com';
							$password     = 'q8VXso';
							$start_date   = date('dmY',strtotime($this->input->post('startdate')));
							$end_date     = date('dmY',strtotime($this->input->post('enddate')));
							$publisher_id = '22679375';
							$token_id     = 'e2ZhYnJpY2lvY21lbGxvQGdtYWlsLmNvbX06e3E4Vlhzb30=';
							//$url 		  = "https://bws.buscape.com.br/api/lomadee/reportTransaction/e2ZhYnJpY2lvY21lbGxvQGdtYWlsLmNvbX06e3E4Vlhzb30=?startDate=15062016&endDate=15062016&eventStatus=0&publisherId=22679375";
							$url		  = "https://api.lomadee.com/services/report/transactions/".$publisher_id."/?startDate=".$start_date."&endDate=".$end_date."&eventStatus=0";
							
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_HEADER, false);
							curl_setopt($ch, CURLOPT_NOBODY, false);
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
							curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type:text/xml'));
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
							curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
							curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
							curl_setopt($ch, CURLOPT_USERAGENT,
							"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_REFERER, $url);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
							curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0); 
							$haveresults_set = curl_exec($ch);

							//Curl to json converter//
							$xml 	  = simplexml_load_string($haveresults_set);
							$json 	  = json_encode($xml);
							$contents = json_decode($json,TRUE);
							//End//
							//echo "<pre>"; print_r($contents); exit;
							$status  = $contents['details']['elapsedTime'];
						}

						//Pon prakesh//
						if($affiliate_name == 'rakuten')
						{	
							$username   = 'facmello';
							$password   = 'yhasdf@Fm80';
							//$token_id   = '3d7b3dd997d6e957b8924240eb6097cb7a02ed549c9c5d0523f500ea33f366e0';
							$report_id  = '3326233';
							$start_date = $this->input->post('startdate');
							$end_date   = $this->input->post('enddate');
							
							header('Content-type: application/json');
							// Set your CSV feed
							$feed = 'https://ran-reporting.rakutenmarketing.com/en/reports/sales-and-activity-reports1/filters?date_range=this-month&include_summary=Y&network=8&tz=GMT&date_type=transaction&token=ZW5jcnlwdGVkYToyOntzOjU6IlRva2VuIjtzOjY0OiI2NzFiZjE1NTViYTk4ZDJhMGNkNjY5ODY5NTZhZWI4N2I2MTgyY2IxM2UwM2Y4MDBjNWU3MWM0NjljNzNhMzk4IjtzOjg6IlVzZXJUeXBlIjtzOjk6IlB1Ymxpc2hlciI7fQ%3D%3D';

							// Arrays we'll use later
							$keys = array();
							$newArray = array();
							// Function to convert CSV into associative array
							function csvToArray($file, $delimiter) 
							{ 
							  	if (($handle = fopen($file, 'r')) !== FALSE)
							  	{ 
							    	$i = 0; 
							    	while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) 
							    	{ 
							      		for ($j = 0; $j < count($lineArray); $j++) 
							      		{ 
							        		$arr[$i][$j] = $lineArray[$j]; 
							      		} 
							      		$i++; 
							    	} 
							    	fclose($handle); 
								} 
							  	return $arr; 
							} 
							
							// Do it
							$data = csvToArray($feed, ',');
							// Set number of elements (minus 1 because we shift off the first row)
							$count = count($data) - 1; 
							//Use first row for names  
							// $labels = array_shift($data);  
							foreach ($data[4] as $label) 
							{
							  $keys[] = $label;
							}
							// Add Ids, just in case we want them later
							$keys[] = 'id';
							for ($i = 5; $i < $count; $i++)
							{
							  $data[$i][] = $i;
							}
							// Bring it all together
							for ($j = 5; $j < $count; $j++) 
							{
							  $d = array_combine($keys, $data[$j]);
							  $newArray[$j] = $d;
							}

							$contents = $newArray;
							$status   = 1;
							//echo "<pre>"; print_r($contents); exit;
						}
 						//Pon prakesh//
						if($affiliate_name == 'cityads')
						{	
							
							$username    = 'facmello';
							$password    = 'yhasdf@Fm80';
							$token_id    = '3d7b3dd997d6e957b8924240eb6097cb7a02ed549c9c5d0523f500ea33f366e0';
							$client_id   = '649885';
							$secret_key  = '5049f98b30b43c185c8917fb719576dc'; 
							$Remote_auth = 'ae757f7e4f1be3bcb64d67eeab39ed86';
							$start_date  = date('Y-m-d',strtotime($this->input->post('startdate')));
							$end_date    = date('Y-m-d',strtotime($this->input->post('enddate')));
							$url         = 'http://us-geo.cityads.com/api/rest/webmaster/json/orderstatistics/'.$start_date.'/'.$end_date.'?remote_auth='.$secret_key;
							//echo $url; exit; 
							//$url       = 'http://api.cityads.com/api/rest/webmaster/json/statistics-offers/action_id/'.$start_date.'/'.$end_date.'?remote_auth='.$secret_key;
							$contents  	 = json_decode(file_get_contents($url),true);
							//echo "<pre>"; print_r($contents); exit;
							$status 	 = $contents['status'];
						} 
						
						//report response details Completed//
						if($affiliate_name == 'afilio')
						{
							
							$username      = 'pingou';
							$password      = 'yhasdf@fm16';
							$token_id      = '57581974981921.99713763';
							$site_id       = '47777';
							$aff_id        = '42747';
							$start_date    = date('Y-m-d',strtotime($this->input->post('startdate')));
							$end_date      = date('Y-m-d',strtotime($this->input->post('enddate')));
							//$url 		   = 'https://v2.afilio.com.br/api/leadsale_api.php?mode=list&token='.$token_id.'&affid='.$aff_id.'&type=sale&dateStart='.$start_date.'&dateEnd='.$end_date.'&format=XML';
							$url 		   = 'https://v2.afilio.com.br/api/leadsale_api.php?mode=list&token='.$token_id.'&affid='.$aff_id.'&type=sale&dateStart='.$start_date.'&dateEnd='.$end_date.'&format=XML&dateType=transaction';
				  			
				  			$contents 	   = file_get_contents($url);
							$xml 	  	   = simplexml_load_string($contents, "SimpleXMLElement", LIBXML_NOCDATA);
							$json 	  	   = json_encode($xml);
							$contents 	   = json_decode($json,TRUE);	
							$status 	   = 1;
						}

						if($status != '')
						{				
							if($flag==0)
							{
								$getdatas = $this->admin_model->import_apireports($contents,$affiliate_name);
								 
								if($getdatas == 1)
								{
									$this->session->set_flashdata('success', 'Report details imported successfully.');
									redirect('adminsettings/upload_apireport','refresh');
								}
								else if($getdatas == 0)
								{	

									$this->session->set_flashdata('error', 'Report Details Already Added.');
									redirect('adminsettings/upload_apireport','refresh');
								}					
							}
						}
						else 
						{
							$this->session->set_flashdata('error','No data found.');
							redirect('adminsettings/upload_apicoupons','refresh');
						}		
					}	
				}
			}		
				$data['action'] = "report";
				$this->load->view('adminsettings/upload_apireport',$data);
		}	
	}*/
	/*END*/
	function session_logout_timings()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		}
		else 
		{	
			$getdetails = $this->admin_model->session_logout_timings();
		}	
	}

	/*New code for add edit view cashback exclusive page 13-7-16*/
	function cashback_exclusive()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			
			$data['cas_exe'] = $this->admin_model->cashback_exclusive();
			$this->load->view('adminsettings/cashback_exclusive',$data);
		}
	}

	function add_cashback_exclusive()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="") {
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save'))
			{	
				//echo "<pre>";print_r($_POST);exit;
				$results = $this->admin_model->add_cashback_exclusive();
				if($results){
					$this->session->set_flashdata('success', ' Cashback Exclusive details successfully Added.');
					redirect('adminsettings/cashback_exclusive','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Cashback Exclusive details not added successfully.');
					redirect('adminsettings/add_cashback_exclusive','refresh');	
				}
			}
			$this->load->view('adminsettings/add_cashback_exclusive');
		}	
	}
	/*End*/
	function edit_cashback_exclusive($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		} 
		else
		{
			//echo "<pre>"; print_r($_POST); exit;
			if($this->input->post('update'))
			{
				$cashback_id  = $this->input->post('cashback_id');
				$results = $this->admin_model->update_cashback_exclusive($cashback_id);
						
				if($results)
				{
					$this->session->set_flashdata('success', ' cashback details updated successfully.');
					redirect('adminsettings/cashback_exclusive','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while updating cashback details.');
					redirect('adminsettings/cashback_exclusive','refresh');
				}
			}
			$results = $this->admin_model->cashback_exclusive($id);
			if($results)
			{
				foreach($results as $get)
				{				
					$data['cashback_id'] 	 = $get->id;
					$data['link_name'] 		 = $get->link_name;
					$data['user_email'] 	 = $get->user_email;
					$data['store_name'] 	 = $get->store_name;
					$data['cashback_type'] 	 = $get->cashback_type;
					$data['cashback_web'] 	 = $get->cashback_web;	
					$data['cashback_app'] 	 = $get->cashback_app;
					$data['extra_param_web'] = $get->extra_param_web;
					$data['extra_param_app'] = $get->extra_param_app;	
					$data['expiry_notify'] 	 = $get->expiry_notify;
					$data['date_added'] 	 = $get->date_added;
					/*new code 1-8-16*/
					$data['analytics_info']  = $get->analytics_info;
					/*End*/
					$data['start_date'] 	 = $get->start_date;	
					$data['expirydate'] 	 = $get->expirydate;	
				}
				$this->load->view('adminsettings/edit_cashback_exclusive',$data);
			}
			else
			{
				redirect('adminsettings/cashback_exclusive','refresh');
			}
		}		
	}	
	
	// delete affiliate
	function delete_cashback_exclusive($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} 
		else
		{
			$deletion = $this->admin_model->delete_cashback_exclusive($id);
			if($deletion)
			{
				$this->session->set_flashdata('success', ' cashback details deleted successfully.');
				redirect('adminsettings/cashback_exclusive','refresh');
			}
			else
			{

				$this->session->set_flashdata('error', 'Error occurred while deleting cashback details.');
				redirect('adminsettings/cashback_exclusive','refresh');
			}
		}
	}

	/*New code for category type concept 13-9-16*/
	function category($type)
	{
		 
		
		$arr_cat_type =  array('one' =>1,'two' =>2,'three' =>3,'four' =>4,'five' =>5,'six' =>6,'seven' =>7,'eight' =>8,'nine' =>9,'ten' =>10);
		$cat_type 	  =  $arr_cat_type[$type];


		$userdetails      = $this->admin_model->userdetails($cat_type); 
		$data['users'] 	  = $userdetails;
		$referral_details = $this->admin_model->category($cat_type);
			
			if($referral_details)
			{
				foreach($referral_details as $details)
				{
					$data['ref_id'] 			 	 = $details->ref_id;
					$data['ref_by_percentage']       = $details->ref_by_percentage;
					$data['ref_cashback'] 		 	 = $details->ref_cashback;
					$data['valid_months']		 	 = $details->valid_months;
					$data['ref_by_rate'] 		 	 = $details->ref_by_rate;
					$data['ref_cashback_rate']   	 = $details->ref_cashback_rate;
					$data['bonus_by_ref_rate']   	 = $details->bonus_by_ref_rate;
					$data['ref_cashback_rate_bonus'] = $details->ref_cashback_rate_bonus;
					$data['friends_count'] 			 = $details->friends_count;
					$data['cat_description'] 		 = $details->cat_description;
					$data['cat_type'] 		 		 = $details->category_type;
					$data['type']					 = $type;
					
					/*News code 22-9-16*/
					$data['content_bellow']  		 = $details->content_bellow_btn;
					$data['content_above']			 = $details->content_above_btn;
					/*end*/
					/*New code 6-10-16*/
					$data['notify_log_users']  		 = $details->notify_log_users;
					/*End*/
					/*New code for registration via category link status 19-10-16*/
					$data['cat_type_status']  		 = $details->cat_type_status;
					/*End*/
				}
				$this->load->view('adminsettings/category',$data);
			}		
	}
	function category_update($type)
	{
		 
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/dashboard','refresh');
		}
		if($this->input->post('save')){
		  
			$updated = $this->admin_model->updatecategory($type);
			if($updated)
			{
				$this->session->set_flashdata('success', 'Category' .$type.' Details updated successfully. ');
				redirect('adminsettings/category/'.$type,'refresh');
			}  
			else
			{
				$this->session->set_flashdata('error', 'Category' .$type.' not updated successfully.');
				redirect('adminsettings/category/'.$type,'refresh');
			}
		}	

		//$this->load->view('adminsettings/category');
	}
	/*End category type concept 13-9-16*/

	/*new code for amazon S3 Config settings 20-9-16*/
	function amazon_s3_settings()
	{
		
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		}
		else 
		{
			
			if($this->input->post('save'))
			{
				
				$results = $this->admin_model->amazon_settings();
				//echo $results; exit;
				if($results)
				{
					$this->session->set_flashdata('success', ' Amazon S3 settings updated successfully.');
					redirect('adminsettings/amazon_s3_settings','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while updating Amazon S3 settings.');
					redirect('adminsettings/amazon_s3_settings','refresh');
				}
			}
		
			$amazon_s3_details = $this->admin_model->amazon_s3_settings();
			
			if($amazon_s3_details)
			{
				 					
				$data['copy_files_status'] 	  	 = $amazon_s3_details->copy_files_status;
				$data['file_url_status'] 	  	 = $amazon_s3_details->file_url_status;
				$data['config_file_url'] 	  	 = $amazon_s3_details->config_file_url;
				$data['copy_autoscan_status'] 	 = $amazon_s3_details->copy_autoscan_status;	
				$data['config_autoscan_url']  	 = $amazon_s3_details->config_autoscan_url;
				$data['sys_memory_cache_status'] = $amazon_s3_details->sys_memory_cache_status;	
				$data['config_elasticache'] 	 = $amazon_s3_details->config_elasticache;

				$this->load->view('adminsettings/amazon_s3_settings',$data);
			}			
		}		
	}
	/*End*/
	/*New code for Marketing sub menu's details 6-10-16*/
	function layout()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');

		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else 
		{
			$main = $this->session->userdata('admin_id');
			
			$admin_details = $this->admin_model->getadmindetails();

			if($admin_details)
			{
				foreach($admin_details as $details)
				{
					$data['admin_id'] 		  		 = $details->admin_id;
					$data['admin_username']   		 = $details->admin_username;
					$data['admin_email'] 	  		 = $details->admin_email;
					$data['background_type']  		 = $details->background_type;
					$data['storecover_type']  		 = $details->storecover_type;
					$data['store_back_img_settings'] = $details->store_back_img_settings;
					$data['top_back_img_settings']   = $details->top_back_img_settings;
					$data['topcashback_type']  		 = $details->topcashback_type;
					$data['topcashback_image'] 		 = $details->topcashback_image;
					$data['topcashback_color'] 		 = $details->topcashback_color;
					$data['image_topcashback'] 		 = $details->image_topcashback;
					$data['background_image'] 		 = $details->background_image;
					$data['background_color'] 		 = $details->background_color;
					$data['storecover_image'] 		 = $details->storecover_image;
					$data['storecover_color'] 		 = $details->storecover_color;
					$data['notify_desk'] 	  		 = $details->notify_desk;
					$data['acc_page_status'] 	     = $details->acc_page_status;
				}
			 
				$this->load->view('adminsettings/layout',$data);
			}	
		}
	} 
	function sales_funnel()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');

		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else 
		{
			$main = $this->session->userdata('admin_id');
			$admin_details = $this->admin_model->getadmindetails();

			if($admin_details)
			{
				foreach($admin_details as $details)
				{
					$data['admin_id'] 		    = $details->admin_id;
					$data['admin_username']   	= $details->admin_username;
					$data['admin_email'] 	  	= $details->admin_email;
					$data['activate_method']  	= $details->activate_method;
					$data['unlog_status']     	= $details->unlog_status;
					$data['unlog_content'] 	  	= $details->unlog_content;
					$data['log_status'] 	  	= $details->log_status;
					$data['unlog_menu_status']	= $details->unlog_menu_status;
					$data['log_menu_status']  	= $details->log_menu_status;
					$data['notify_color'] 	  	= $details->notify_color;
                    $data['rel_store_details']  = $details->rel_store_details;
				}
				
				$sales_funnel 				= $this->admin_model->getsalesfunneldetails();
				foreach($sales_funnel as $banner)
				{

					$data['sales_banner_id']   	 = $banner->sales_banner_id;
					$data['topcashback']    	 = $banner->topcashback;
					$data['top_top_status']      = $banner->topcashback_top_status;
					$data['top_bot_status']      = $banner->topcashback_bottom_status;
					$data['category']     		 = $banner->category;
					$data['cat_top_status']      = $banner->category_top_status;
					$data['cat_bot_status']      = $banner->category_bottom_status;
					$data['store']     			 = $banner->store;
					$data['store_top_status']    = $banner->store_top_status;
					$data['store_bot_status']    = $banner->store_bottom_status;
					$data['barato']     		 = $banner->barato;
					$data['barato_top_status']   = $banner->barato_top_status;
					$data['barato_bot_status']   = $banner->barato_bottom_status;
					$data['minhaconta']     	 = $banner->minhaconta;
					$data['myacc_top_status']    = $banner->minhaconta_top_status;
					$data['myacc_bot_status']    = $banner->minhaconta_bottom_status;
					$data['extrato']     		 = $banner->extrato;
					$data['extrato_top_status']  = $banner->extrato_top_status;
					$data['extrato_bot_status']  = $banner->extrato_bottom_status;
					$data['resgate']     		 = $banner->resgate;
					$data['resgate_top_status']  = $banner->resgate_top_status;
					$data['resgate_bot_status']  = $banner->resgate_bottom_status;
					$data['clickhistory']     	 = $banner->clickhistory;
					$data['clkhis_top_status']   = $banner->clickhistory_top_status;
					$data['clkhis_bot_status']   = $banner->clickhistory_bottom_status;
					$data['categorystore']     	 = $banner->categorystore;
					$data['catstore_top_status'] = $banner->categorystore_top_status;
					$data['catstore_bot_status'] = $banner->categorystore_bottom_status;
				}

				$banner_details     = $this->admin_model->getsalesfunnelbannerdetails();
				$data['banner_details']  = $banner_details;
				

				$this->load->view('adminsettings/sales_funnel',$data);
			}	
		}	
	}

	/*New code for sales funnel settings page 14-3-17*/
	function sales_funnel_settings()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');

		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else 
		{
			$main = $this->session->userdata('admin_id');
			$admin_details = $this->admin_model->getadmindetails();

			if($admin_details)
			{
				foreach($admin_details as $details)
				{
					$data['admin_id'] 		    = $details->admin_id;
					$data['admin_username']   	= $details->admin_username;
					$data['admin_email'] 	  	= $details->admin_email;
					$data['activate_method']  	= $details->activate_method;
					$data['unlog_status']     	= $details->unlog_status;
					$data['unlog_content'] 	  	= $details->unlog_content;
					$data['log_status'] 	  	= $details->log_status;
					$data['unlog_menu_status']	= $details->unlog_menu_status;
					$data['log_menu_status']  	= $details->log_menu_status;
					$data['notify_color'] 	  	= $details->notify_color;
                    $data['rel_store_details']  = $details->rel_store_details;
				}
				
				$sales_funnel 				= $this->admin_model->getsalesfunneldetails();
				foreach($sales_funnel as $banner)
				{

					$data['sales_banner_id']   	 = $banner->sales_banner_id;
					$data['topcashback']    	 = $banner->topcashback;
					$data['top_top_status']      = $banner->topcashback_top_status;
					$data['top_bot_status']      = $banner->topcashback_bottom_status;
					$data['category']     		 = $banner->category;
					$data['cat_top_status']      = $banner->category_top_status;
					$data['cat_bot_status']      = $banner->category_bottom_status;
					$data['store']     			 = $banner->store;
					$data['store_top_status']    = $banner->store_top_status;
					$data['store_bot_status']    = $banner->store_bottom_status;
					$data['barato']     		 = $banner->barato;
					$data['barato_top_status']   = $banner->barato_top_status;
					$data['barato_bot_status']   = $banner->barato_bottom_status;
					$data['minhaconta']     	 = $banner->minhaconta;
					$data['myacc_top_status']    = $banner->minhaconta_top_status;
					$data['myacc_bot_status']    = $banner->minhaconta_bottom_status;
					$data['extrato']     		 = $banner->extrato;
					$data['extrato_top_status']  = $banner->extrato_top_status;
					$data['extrato_bot_status']  = $banner->extrato_bottom_status;
					$data['resgate']     		 = $banner->resgate;
					$data['resgate_top_status']  = $banner->resgate_top_status;
					$data['resgate_bot_status']  = $banner->resgate_bottom_status;
					$data['clickhistory']     	 = $banner->clickhistory;
					$data['clkhis_top_status']   = $banner->clickhistory_top_status;
					$data['clkhis_bot_status']   = $banner->clickhistory_bottom_status;
					$data['categorystore']     	 = $banner->categorystore;
					$data['catstore_top_status'] = $banner->categorystore_top_status;
					$data['catstore_bot_status'] = $banner->categorystore_bottom_status;
				}

				$banner_details     = $this->admin_model->getsalesfunnelbannerdetails();
				$data['banner_details']  = $banner_details;
				

				$this->load->view('adminsettings/sales_funnel_settings',$data);
			}	
		}	
	}
	/*End 14-3-17*/

	/*New code for popup content 9-11-16*/
	function popup()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else 
		{
			$main 		  = $this->session->userdata('admin_id');	
			$sales_funnel = $this->admin_model->getsalesfunneldetails();
			foreach($sales_funnel as $banner)
			{

				$data['sales_banner_id']   	 = $banner->sales_banner_id;
				$data['topcashback']    	 = $banner->topcashback;
				$data['top_top_status']      = $banner->topcashback_top_status;
				$data['top_bot_status']      = $banner->topcashback_bottom_status;
				$data['category']     		 = $banner->category;
				$data['cat_top_status']      = $banner->category_top_status;
				$data['cat_bot_status']      = $banner->category_bottom_status;
				$data['store']     			 = $banner->store;
				$data['store_top_status']    = $banner->store_top_status;
				$data['store_bot_status']    = $banner->store_bottom_status;
				$data['barato']     		 = $banner->barato;
				$data['barato_top_status']   = $banner->barato_top_status;
				$data['barato_bot_status']   = $banner->barato_bottom_status;
				$data['minhaconta']     	 = $banner->minhaconta;
				$data['myacc_top_status']    = $banner->minhaconta_top_status;
				$data['myacc_bot_status']    = $banner->minhaconta_bottom_status;
				$data['extrato']     		 = $banner->extrato;
				$data['extrato_top_status']  = $banner->extrato_top_status;
				$data['extrato_bot_status']  = $banner->extrato_bottom_status;
				$data['resgate']     		 = $banner->resgate;
				$data['resgate_top_status']  = $banner->resgate_top_status;
				$data['resgate_bot_status']  = $banner->resgate_bottom_status;
				$data['clickhistory']     	 = $banner->clickhistory;
				$data['clkhis_top_status']   = $banner->clickhistory_top_status;
				$data['clkhis_bot_status']   = $banner->clickhistory_bottom_status;
				$data['categorystore']     	 = $banner->categorystore;
				$data['catstore_top_status'] = $banner->categorystore_top_status;
				$data['catstore_bot_status'] = $banner->categorystore_bottom_status;
			}
			$banner_details     = $this->admin_model->getsalesfunnelbannerdetails();
			$data['banner_details']  = $banner_details;
			$this->load->view('adminsettings/popup',$data);
		}	
	}
	
	/*new code for Update popup details 10-11-16*/
	function popupupdate()
	{
		$this->input->session_helper();
		if($this->input->post('save'))
		{
			

			$updated = $this->admin_model->popupupdate();	
			if($updated)
			{
				$this->session->set_flashdata('success', 'Popup Funnel settings updated successfully. ');
				redirect('adminsettings/popup','refresh');
			}  
			else
			{
				$this->session->set_flashdata('error', 'Popup Funnel settings not updated successfully.');
				redirect('adminsettings/popup','refresh');
			} 	
		}		
	}
	/*End*/ 

	function layoutupdate()
	{
		$this->input->session_helper();
		if($this->input->post('save'))
		{
			$this->security->cookie_handlers();
			$new_random  	  = mt_rand(0,99999);

			$background_image  = $_FILES['background_image']['name'];
			$cover_image 	   = $_FILES['cover_image']['name'];
			$topcashback_image = $_FILES['topcashbackimage']['name'];
			//Pilaventhiran 02/05/2016
			$image_topcashback = $_FILES['imagetopcashback']['name'];
			//End//
			$background_color = $this->input->post('background_color');
			$backtype 		  = $this->input->post('backtype');
			$covertype 		  = $this->input->post('covertype');
			$cover_color 	  = $this->input->post('cover_color');
			//New code for topcashback banner image settings 264-16//
			$topcashback_type  = $this->input->post('topcashbacktype');
			$topcashback_color = $this->input->post('topcashbackcolor');
			//Pilaventhiran 02/05/2016
			$notify_desk 	   = $this->input->post('notify_desk');
			
			/*New code for amazon s3 copy settings 26-12-16*/
			$amazon_s3_details = $this->admin_model->amazon_s3_settings();
			$copy_status       = $amazon_s3_details->copy_files_status;
			/*End*/

			if($background_image!="") 
			{
				
				/*New code for image name field changes 3-10-16*/
				$info 	 	 	  = new SplFileInfo($background_image);
				$file_ex 	 	  = $info->getExtension();
				$newfilename 	  = str_replace('.'.$file_ex,'', $background_image);
				$background_image = $newfilename.$new_random.'.'.$file_ex;
				/*End 3-10-16*/

				//$background_image = remove_space($new_random.$background_image);
				$config['upload_path']   = 'uploads/adminpro';
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['file_name'] 	 = $background_image;
				
				//New code for AWS 24-9-16//
				if($copy_status == 1)
				{
					$filepath = 'uploads/adminpro/'.$background_image;
					$tmp 	  = $_FILES['background_image']['tmp_name'];
					$this->load->library('S3');
					$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years")))); //
				}
				//End//

				$this->load->library('upload', $config);
				$this->upload->initialize($config);	
				if($background_image!="" && (!$this->upload->do_upload('background_image')))
				{
					$background_imageerror = $this->upload->display_errors();
				}
				if(isset($background_imageerror))        
				{
					$this->session->set_flashdata('error',$background_imageerror);
					redirect('adminsettings/settings','refresh');
				}
			}
			else
			{
				$background_image = $this->input->post('hidden_site_background_image');
			}
			if($cover_image!="") 
			{
				/*New code for image name field changes 3-10-16*/
				$info 	 	 	= new SplFileInfo($cover_image);
				$file_ex 	 	= $info->getExtension();
				$newfilename 	= str_replace('.'.$file_ex,'', $cover_image);
				$cover_image    = $newfilename.$new_random.'.'.$file_ex;
				/*End 3-10-16*/
				//$cover_image = remove_space($new_random.$cover_image);
				$config['upload_path'] ='uploads/adminpro/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['file_name']=$cover_image;
				
				//New code for AWS 24-9-16//
				if($copy_status == 1)
				{
					$filepath = 'uploads/adminpro/'.$cover_image;
					$tmp 	  = $_FILES['cover_image']['tmp_name'];
					$this->load->library('S3');
					$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
				}
				//End//	

				$this->load->library('upload', $config);
				$this->upload->initialize($config);	
				if($cover_image!="" && (!$this->upload->do_upload('cover_image')))
				{
					$background_imageerror = $this->upload->display_errors();
				}

				//$destination_img = './uploads/adminpro/'.$cover_image;
				/*new code for reduce file size 24-11-16*/
				//$d = $this->compress($filepath,$destination_img, 50);
				/*End 24-11-16*/

				if(isset($background_imageerror))        
				{
					$this->session->set_flashdata('error',$background_imageerror);
					redirect('adminsettings/settings','refresh');
				}
			}
			else
			{
				$cover_image = $this->input->post('hidden_store_cover_image');
			}
			//New code for top cashback banner image settings 26-4-16//
			if($topcashback_image!="") 
			{
				/*New code for image name field changes 3-10-16*/
				$info 	 	 	   = new SplFileInfo($topcashback_image);
				$file_ex 	 	   = $info->getExtension();
				$newfilename 	   = str_replace('.'.$file_ex,'', $topcashback_image);
				$topcashback_image = $newfilename.$new_random.'.'.$file_ex;
				/*End 3-10-16*/
				//$topcashback_image = remove_space($new_random.$topcashback_image);
				$config['upload_path']   = 'uploads/adminpro/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['file_name']	 = $topcashback_image;
				
				//New code for AWS 24-9-16//
				if($copy_status == 1)
				{
					$filepath = 'uploads/adminpro/'.$topcashback_image;
					$tmp 	  = $_FILES['topcashbackimage']['tmp_name'];
					$this->load->library('S3');
					$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
				}
				//End//	
				$this->load->library('upload', $config);
				$this->upload->initialize($config);	
				if($topcashback_image!="" && (!$this->upload->do_upload('topcashbackimage')))
				{
					$background_imageerror = $this->upload->display_errors();
				}

				if(isset($background_imageerror))        
				{
					$this->session->set_flashdata('error',$background_imageerror);
					redirect('adminsettings/settings','refresh');
				}
			}
			else
			{
				$topcashback_image = $this->input->post('hidden_site_topcashback_image');
			}
			//End//
			//New code for top cashback banner center image settings 26-4-16//
			if($image_topcashback!="") 
			{
				
				/*New code for image name field changes 3-10-16*/
				$info 	 	 	   = new SplFileInfo($image_topcashback);
				$file_ex 	 	   = $info->getExtension();
				$newfilename 	   = str_replace('.'.$file_ex,'', $image_topcashback);
				$image_topcashback = $newfilename.$new_random.'.'.$file_ex;
				/*End 3-10-16*/
				//$image_topcashback = remove_space($new_random.$image_topcashback);
				$config['upload_path'] ='uploads/adminpro';
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['file_name']=$image_topcashback;
				
				//New code for AWS 24-9-16//
				if($copy_status == 1)
				{
					$filepath = 'uploads/adminpro/'.$image_topcashback;
					$tmp 	  = $_FILES['imagetopcashback']['tmp_name'];
					$this->load->library('S3');
					$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
				}
				//End//	
				$this->load->library('upload', $config);
				$this->upload->initialize($config);	
				if($image_topcashback!="" && (!$this->upload->do_upload('imagetopcashback')))
				{
					$image_topcashbackerror = $this->upload->display_errors();
				}
					if(isset($background_imageerror))        
					{
						$this->session->set_flashdata('error',$background_imageerror);
						redirect('adminsettings/settings','refresh');
					}
			}
			else
			{
				$image_topcashback = $this->input->post('hidden_site_image_topcashback');
			}
			//End//	
			if($background_color!='')
			{
				$background_color = $background_color;
			}
			if($cover_color!='')
			{
				$cover_color = $cover_color;
			}
			
			/*New code for facebook share image details 14-11-16*/
			$store_page_img  = $_FILES['store_img']['name'];
			$pingou_site_img = $_FILES['pingou_img']['name'];

			if($pingou_site_img!="") 
			{
				$new_random 	 = mt_rand(0,99999);
				$pingou_site_img = $_FILES['pingou_img']['name'];
				/*New code for image name field changes 3-10-16*/
				$info 	 	 	 = new SplFileInfo($pingou_site_img);
				$file_ex 	 	 = $info->getExtension();
				$newfilename 	 = str_replace('.'.$file_ex,'', $pingou_site_img);
				$pingou_site_img = $newfilename.$new_random.'.'.$file_ex;
				/*End 3-10-16*/
				//$pingou_site_img 		 = remove_space($new_random.$pingou_site_img);
				$config['upload_path']   ='uploads/adminpro';
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['file_name']	 = $pingou_site_img;
				
				//New code for AWS 13-12-16//
				if($copy_status == 1)
				{
					$filepath = 'uploads/adminpro/'.$pingou_site_img;
					$tmp 	  = $_FILES['pingou_img']['tmp_name'];
					$this->load->library('S3');
					$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
				}
				//End//
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				
				if($pingou_site_img!="" && (!$this->upload->do_upload('pingou_img')))
				{
					$pingou_site_imgerror = $this->upload->display_errors();
				}
				if(isset($pingou_site_imgerror))
				{
					$flag=1;
					$this->session->set_flashdata('error',$pingou_site_imgerror);
					redirect('adminsettings/popup','refresh');
				}
			}
			else
			{
				$pingou_site_img = $this->input->post('pingou_meta_img');
			}

			if($store_page_img!="") 
			{
				$new_random 	  = mt_rand(0,99999);
				$store_page_img   = $_FILES['store_img']['name'];
				/*New code for image name field changes 3-10-16*/
				$info 	 	 	  = new SplFileInfo($store_page_img);
				$file_ex 	 	  = $info->getExtension();
				$newfilename 	  = str_replace('.'.$file_ex,'', $store_page_img);
				$store_page_img	  =  $newfilename.$new_random.'.'.$file_ex;
				/*End 3-10-16*/
				//$store_page_img 		 = remove_space($new_random.$store_page_img);
				$config['upload_path']   ='uploads/adminpro';
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['file_name']	 = $store_page_img;
				
				//New code for AWS 13-12-16//
				if($copy_status == 1)
				{
					$filepath = 'uploads/adminpro/'.$store_page_img;
					$tmp 	  = $_FILES['store_img']['tmp_name'];
					$this->load->library('S3');
					$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
				}
				//End//
				
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				
				if($store_page_img!="" && (!$this->upload->do_upload('store_img')))
				{
					$store_page_imgerror = $this->upload->display_errors();
				}
				if(isset($store_page_imgerror))
				{
					$flag=1;
					 
					$this->session->set_flashdata('error',$store_page_imgerror);
					redirect('adminsettings/popup','refresh');
				}
			}
			else
			{
				$store_page_img = $this->input->post('store_meta_img');
			}
			/*End 14-11-16*/

			$updated = $this->admin_model->updatelayout($backtype,$background_image,$background_color,$covertype,$cover_color,$cover_image,$topcashback_type,$topcashback_image,$topcashback_color,$image_topcashback,$pingou_site_img,$store_page_img);
				
			if($updated)
			{
				$this->session->set_flashdata('success', 'Admin Layout settings updated successfully. ');
				redirect('adminsettings/layout','refresh');
			}  
			else
			{
				$this->session->set_flashdata('error', 'Admin Layout settings not updated successfully.');
				redirect('adminsettings/layout','refresh');
			} 	
		}	
	}

	function sales_funnelupdate()
	{
		$this->input->session_helper();
		if($this->input->post('save'))
		{

			$updated = $this->admin_model->updatesales_funnel();	
			if($updated)
			{
				$this->session->set_flashdata('success', 'Admin Sales Funnel settings updated successfully. ');
				redirect('adminsettings/sales_funnel','refresh');
			}  
			else
			{
				$this->session->set_flashdata('error', 'Admin Sales Funnel settings not updated successfully.');
				redirect('adminsettings/sales_funnel','refresh');
			} 	
		}		
	}
	/*end*/

	function sales_funnel_settings_update()
	{
		$this->input->session_helper();
		if($this->input->post('save'))
		{

			$updated = $this->admin_model->updatesales_funnel_settings();	
			if($updated)
			{
				$this->session->set_flashdata('success', 'Admin Sales Funnel settings updated successfully. ');
				redirect('adminsettings/sales_funnel_settings','refresh');
			}  
			else
			{
				$this->session->set_flashdata('error', 'Admin Sales Funnel settings not updated successfully.');
				redirect('adminsettings/sales_funnel_settings','refresh');
			} 	
		}		
	}

	function download_users($status)
	{
		 
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			$this->load->helper('csv_helper');
			$result =$this->admin_model->download_users();
			$t_date=date('Y-m-d');
			
			if($status == 'csv')
			{
				$filename="users-".$t_date.".csv";
			}
			else
			{
				$filename="users-".$t_date.".xls";	
			}
			
			
			$test=" Id  \t Name(Status) \t Email address \t Balance \t N.Wd \t  Clicks \t Contact No \t Cellular No \t IFSC Code \t Ref \t Cat \t R_amt \t App \t U.B \t News \t R.Code \t Signup";
			$test.="\n";
			if(isset($result))
			{
			    $k=1;
			    foreach($result as $row)
			    {
				    $total_withd_ss   = $this->db->query("SELECT COUNT(*) as counting FROM `withdraw` where user_id='$row->user_id'");
                    $total_with_count = $total_withd_ss->row('counting');
                    $total_click_ss   = $this->db->query("SELECT COUNT(*) as counting FROM `click_history` where user_id='$row->user_id'");
                    $total_click      = $total_click_ss->row('counting');

				    $user_id   	 = $row->user_id;
					$name      	 = $row->first_name."".$row->last_name;
					$email     	 = $row->email;
					$balance   	 = $row->balance;
					$contact   	 = $row->contact_no;
					$cellular  	 = $row->celular_no;
					$ifsc_code 	 = $row->ifsc_code;
					$refer     	 = $row->refer;
					$cat_type    = $row->referral_category_type;
					$referal_amt = $row->referral_amt;
					$app_login   = $row->app_login;
					$unic_bonus  = $row->bonus_benefit;
					$newsletter	 = $row->newsletter_mail;
					$refer_code  = $row->random_code;
					$signup_date = $row->date_added;
					   					   
				   $test.=$user_id."\t".$name."\t".$email."\t".$balance."\t".$total_with_count."\t".$total_click."\t".$contact."\t".$cellular.
				   "\t".$ifsc_code."\t".$refer."\t".$cat_type."\t".$referal_amt."\t".$app_login."\t".$unic_bonus."\t".$newsletter."\t".$refer_code."\t".$signup_date;
				   $test.="\n";
				  
			   } 
			}
			print_r($test);
			   
			header("Content-type: application/csv");
			//header("Content-type: application/vnd.ms-word");
			//header("Content-type: text/plain");
			header("Content-Disposition: attachment; filename=".$filename);
			header("Pragma: no-cache");
			header("Expires: 2");
			$this->load->helper('file');
			write_file('./backup/'.$filename, $test);
			$data = file_get_contents("./backup/".$filename);
			$urfile="users-".$t_date.".csv";
			$this->load->helper('download');
			force_download($urfile, $data); 
		}
	}

	//New code for view all pretty Link details.. 18-10-16
	function pretty_link()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			if($this->input->post('hidd'))
			{
				//$results = $this->admin_model->sort_pretty_link();
				if($this->input->post('chkbox'))
				{
					
					$results = $this->admin_model->sort_prettylink_delete();
				
				}
				if($results)
				{
					
					$this->session->set_flashdata('success', 'Pretty Link details updated successfully.');
					redirect('adminsettings/pretty_link','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while updating Pretty Link details.');
					redirect('adminsettings/pretty_link','refresh');
				}
			}
			
			$data['prettylink'] = $this->admin_model->pretty_link();
			$this->load->view('adminsettings/pretty_link',$data);
		}
	}

	// add new Pretty Link
	
	function add_pretty_link()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		} 
		else
		{	
			if($this->input->post('save'))
			{
				$flag=0;
				$pretty_link_name = $this->input->post('pretty_link_name');
				 
				if($pretty_link_name!='')
				{
					$results = $this->admin_model->add_pretty_link();
				
					if($results)
					{
						$this->session->set_flashdata('success', ' Pretty Link details added successfully.');
						redirect('adminsettings/pretty_link','refresh');
					}
					else
					{
						$this->session->set_flashdata('error', 'Error occurred while adding Pretty Link Details.');
						redirect('adminsettings/add_pretty_link','refresh');
					}
				}
			}	
		}
		$data['action']='new';
		$this->load->view('adminsettings/add_pretty_link',$data);
	}
	
	// edit Pretty Link	
	function edit_pretty_link($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		} 
		else
		{
			$get_pretty_link = $this->admin_model->get_pretty_link($id);

			foreach($get_pretty_link as $get)
			{
				$data['pretty_link_id'] 	= $get->pretty_link_id;
				$data['pretty_link_name'] 	= $get->link_name;
				$data['income_url'] 		= $get->incoming_link;
				$data['external_url']		= $get->external_link;
				$data['pretty_link_status'] = $get->status;
			}
			$data['action'] = "edit";
			$this->load->view('adminsettings/add_pretty_link',$data);		
		}
	}
	
	// update Pretty Link
	function update_pretty_link()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		} 
		else
		{
			if($this->input->post('save'))
			{
				
				$pretty_link_name = $this->input->post('pretty_link_name');
				 
				if($pretty_link_name!='')
				{
					$results = $this->admin_model->update_pretty_link();
					if($results)
					{
						$this->session->set_flashdata('success', ' Pretty Link details updated successfully.');
						redirect('adminsettings/pretty_link','refresh');
					}
					else
					{
						$this->session->set_flashdata('error', 'Error occurred while updating Pretty Link details.');
						redirect('adminsettings/edit_pretty_link','refresh');
					}
				}
			}	
		}
	}

	//New code for delete affiliate 18-10-16
	function delete_pretty_link($id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			$deletion = $this->admin_model->delete_pretty_link($id);
			if($deletion)
			{
				$data['action']="Edit";
				$this->session->set_flashdata('success', 'Pretty Link details deleted successfully.');
				redirect('adminsettings/pretty_link','refresh');
			}
			else
			{
				$data['action']="Edit";
				$this->session->set_flashdata('error', 'Error occurred while deleting Pretty Link details.');
				redirect('adminsettings/pretty_link','refresh');
			}
		}
	}

	/*New code for add and delete banner details 4-11-16*/
	function add_bannerdetails()
	{	
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$this->security->cookie_handlers();
			
			$image_name     = $_FILES['img_name']['name'];
			$category_type  = $_POST['cat_type'];
			$banner_url     = $_POST['bannerurl'];
			$condition_type = $_POST['condtype'];
			
			/*New code for amazon s3 copy settings 26-12-16*/
			$amazon_s3_details = $this->admin_model->amazon_s3_settings();
			$copy_status       = $amazon_s3_details->copy_files_status;
			/*End*/

			if($image_name!="")
	    	{	 
	
				/*New code for image name field changes 3-10-16*/
				$info 	 	 	= new SplFileInfo($image_name);
				$file_ex 	 	= $info->getExtension();
				$newfilename 	= str_replace('.'.$file_ex,'', $image_name);
				$image_name     = $newfilename.$new_random.'.'.$file_ex;
				/*End 3-10-16*/

				//$admin_logo = remove_space($new_random.$admin_logo);
				$config['upload_path'] ='uploads/banners';
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['file_name']=$image_name;
				
				//New code for AWS 24-9-16//
				if($copy_status == 1)
				{
					$image_name = preg_replace("/\s+/", "_", $image_name);
					$filepath = 'uploads/banners/'.$image_name;
					$tmp 	  = $_FILES['img_name']['tmp_name'];
					$this->load->library('S3');
					$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($tmp,$this->config->item('bucket_name'),$filepath, S3::ACL_PUBLIC_READ,array("Cache-Control" =>"max-age=31536000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 years"))));
				}
				//End//	


				$this->load->library('upload', $config);
				$this->upload->initialize($config);	
				if($image_name!="" && (!$this->upload->do_upload('img_name')))
				{
					$admin_logoerror = $this->upload->display_errors();
				}
					if(isset($admin_logoerror))        
					{
						$this->session->set_flashdata('error',$admin_logoerror);
						redirect('adminsettings/sales_funnel','refresh');
					}
			 	
	        	$vars = $this->admin_model->add_bannerdetails($image_name,$category_type,$banner_url,$condition_type);
				$this->security->cookie_handlers();
				echo $vars;	
	    	}
	    }	
	}
	function get_bannerdetails()
	{
		$cat_type = $_POST['cat_type'];
		$data['cat_type'] = $cat_type;
        $this->load->view('adminsettings/banner_details',$data);
	}
	function delete_bannerdetails()
	{	
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			$this->security->cookie_handlers();
			
			$banner_id  = $_POST['banner_id'];

	        $deleted = $this->admin_model->delete_bannerdetails($banner_id);
			$this->security->cookie_handlers();
			echo $deleted;	
	    	 
	    }	
	}

	function update_bannerdetails()
	{	
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		} else
		{
			$this->security->cookie_handlers();
			
			$banner_id  = $_POST['banner_id'];
			$status 	= $_POST['status'];
			
	        $updated = $this->admin_model->update_bannerdetails($banner_id,$status);
			$this->security->cookie_handlers();
			echo $updated;	
	    	 
	    }	
	}

	/*End*/
	/*New code for exit popup details 15-11-16*/
	function exit_popup()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if(($admin_id=="")){
			redirect('adminsettings/index','refresh');
		} else
		{
			$get_popup_template = $this->db->query("SELECT * from exit_popup where popup_id!=0")->result();
			 
			foreach($get_popup_template as $get){
				$data['popup_id'] 			  		 = $get->popup_id;
				$data['popup_subject'] 		  		 = $get->popup_subject;
				$data['popup_template'] 	  		 = $get->popup_template;
				$data['store_popup_template'] 		 = $get->store_popup_template;
				$data['store_popup_template_nocash'] = $get->store_popup_template_nocash;
				$data['all_status'] 		  		 = $get->all_status;
				$data['store_status'] 		  		 = $get->store_status;
			}
			 
			$this->load->view('adminsettings/exit_popup',$data);
		}
	}
	function update_exitpopup()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save'))
			{
				$popup_subject  = $this->input->post('popup_subject');
				$popup_template = $this->input->post('popup_template');
				$store_popup_template = $this->input->post('store_popup_template');
				$popup_id 		= $this->input->post('popup_id');
				
				if($popup_template=="")
				{
					$this->session->set_flashdata('popup_subject', $popup_subject);
					$this->session->set_flashdata('popup_id', $popup_id);
					$this->session->set_flashdata('error', 'Please enter some message.');
					redirect('adminsettings/exit_popup');
				}
				else 
				{ 
					$results = $this->admin_model->update_exitpopup();
					if($results) {
						$this->session->set_flashdata('success', 'Exit Popup Template updated successfully.');
						redirect('adminsettings/exit_popup');
					}
					else {
						$this->session->set_flashdata('error', 'Error occurred while updating Exit Popup template.');
						redirect('adminsettings/exit_popup');
					}
				}
			}
		}
	}
	/*End 15-11-16*/

	/*New code for update reset count for not login users in popup 19-11-16*/
	function update_session_count()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			$bonus_status = $this->input->post('bonus');
			$app_log_type = $this->input->post('log_type');
			if(($bonus_status =='') && ($app_log_type ==''))
			{
				$updated = $this->admin_model->update_session_count_notlog();	
			}
			else
			{
				$updated = $this->admin_model->update_session_count_log($bonus_status,$app_log_type);	
			}
			
			$this->security->cookie_handlers();
			echo $updated;
		}
	}
	/*End 19-11-16*/

	function compress($source, $destination, $quality) 
	{
	    $info = getimagesize($source);
	    if ($info['mime'] == 'image/jpeg') 
	        $image = imagecreatefromjpeg($source);
	    elseif ($info['mime'] == 'image/gif') 
	        $image = imagecreatefromgif($source);

	    elseif ($info['mime'] == 'image/png') 
	        $image = imagecreatefrompng($source);

	    imagejpeg($image, $destination, $quality);

	    return $destination;
	}

	/*Amazon s3 file transfer details 30-11-16*/
	function upload_fileon_s3()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/index','refresh');
		}
		else
		{
			/*CSS Files*/
			$dir_path   = FCPATH.'front/css/';
			$s3_dirpath = 'front/css/';
			$files      = scandir($dir_path);
			 
			foreach($files as $newfiles)
			{
				if ($newfiles == '.' || $newfiles == '..') 
				{
					continue;
				}
				$fullpath = $dir_path.$newfiles;
				if(!is_dir($fullpath))
				{
					//$header = array('Content-Type' => $filetype);
					$newfile_path = $s3_dirpath.$newfiles;
					$this->load->library('S3');
					//$this->s3->deleteObject($this->config->item('bucket_name'), $newfile_path);
					$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ_WRITE);
					$this->s3->putObjectFile($fullpath,$this->config->item('bucket_name'),$newfile_path,S3::ACL_PUBLIC_READ_WRITE,array("Cache-Control" =>"max-age=2592000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 months"))),"text/css");
				}	
			}

			/*JS Files*/
			$jsdir_path   = FCPATH.'front/js/';
			$jss3_dirpath = 'front/js/';
			$jsfiles      = scandir($jsdir_path);
			 
			foreach($jsfiles as $jsnewfiles)
			{
				if ($jsnewfiles == '.' || $jsnewfiles == '..') 
				{
					continue;
				}
				$jsfullpath = $jsdir_path.$jsnewfiles;
				if(!is_dir($jsfullpath))
				{
					
					$jsnewfile_path = $jss3_dirpath.$jsnewfiles;
					$this->load->library('S3');
					//$this->s3->deleteObject($this->config->item('bucket_name'), $jsnewfile_path);
					$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ_WRITE);
					$this->s3->putObjectFile($jsfullpath,$this->config->item('bucket_name'),$jsnewfile_path,S3::ACL_PUBLIC_READ_WRITE,array("Cache-Control" =>"max-age=2592000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 months"))),"text/js");
				}	
			}

			/*Minify Files*/
			$mindir_path   = FCPATH.'front/minify/';
			$mins3_dirpath = 'front/minify/';
			$minfiles      = scandir($mindir_path);
			 
			foreach($minfiles as $minnewfiles)
			{
				if ($minnewfiles == '.' || $minnewfiles == '..') 
				{
					continue;
				}
				$file_format = pathinfo($minnewfiles, PATHINFO_EXTENSION);
				if($file_format == 'css')
				{
					$type = 'text/css';
				}
				else
				{	
					$type = 'text/js';
				}
				$minfullpath = $mindir_path.$minnewfiles;
				if(!is_dir($minfullpath))
				{
					
					$minnewfile_path = $mins3_dirpath.$minnewfiles;
					$this->load->library('S3');
					//$this->s3->deleteObject($this->config->item('bucket_name'), $jsnewfile_path);
					$this->s3->putBucket($this->config->item('bucket_name'),S3::ACL_PUBLIC_READ_WRITE);
					$this->s3->putObjectFile($minfullpath,$this->config->item('bucket_name'),$minnewfile_path,S3::ACL_PUBLIC_READ_WRITE,array("Cache-Control" =>"max-age=2592000","Expires" => gmdate("D, d M Y H:i:s T", strtotime("+1 months"))),$type);
				}	
			}

			echo 1;
		}
	}
 	
	function newcoupons($store_name)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/dashboard','refresh');
		}
		else 
		{
			//$totalrecords = intval($this->input->get('totalrecords'));
	        $draw 		  = $this->input->get('draw');
	        $start 		  = $this->input->get('start');
	        $length 	  = $this->input->get('length');
	        $search 	  = $this->input->get('search');
	        $searchtext   = $search['value'];
	        $coupon_name  = $this->input->get('coupon_name');
	        $totalrecords = count($this->admin_model->coupons($searchtext,$coupon_name));
	        $results 	  = $this->admin_model->newcoupons($draw,$start,$length,$searchtext,$coupon_name);
			
			$data    	  = array();
	        $no      	  = $start + 1;
	        foreach ($results as $r) 
	        {
		        
	        	$checkbox    = '<input type="checkbox"  class="check_b chksingle" name="chkbox['.$r->coupon_id.']" />'; /**/
	        	$coupon_id   = $r->coupon_id;
	        	$offer_url   = $this->admin_model->get_affiliate_details($r->offer_name);
	        	$offer_name  = '<a href='.base_url().'cupom-desconto/'.$offer_url[0]->affiliate_url.' target="_blank">'.$r->offer_name.'</a>';          
	            $title 		 = '<a href='.$r->offer_page.' target="_blank">'.$r->title.'</a>';
	            $coupon_code = $r->code; 
	            /*if($coupon->coupon_options==1){
	            	echo 'style="background: none repeat scroll 0 0 #32c2cd; color: white;"';}
				if($coupon->coupon_options==2){
					echo 'style="background: none repeat scroll 0% 0% rgb(231, 73, 85); color: white;"';}*/
				if($coupon->coupon_options==1)
				{
					$offer  = "Featured";
				}
				if($coupon->coupon_options==2)
				{
					$offer  = "Exclusive";
				}

				$promo_id   	= $r->type; 
		        $exp_date   	= date('d/m/Y',strtotime($r->expiry_date));
              	$tracking 		= $r->Tracking;
              	$extra_tracking = $r->extra_tracking_param;
              	$edit 			= '<a href='.base_url().'adminsettings/editcoupon/'.$r->coupon_id.'><i class="icon-pencil"></i></a>';
                $delete     	= '<a class="confirm-dialog" onclick="return confirmDelete();" href='.base_url().'adminsettings/deletecoupon/'.$r->coupon_id.'><i class="icon-trash"></i></a>';

		        array_push($data,array(
		        $checkbox,
		        $no,
		        $coupon_id,
		        $offer_name,
		        $title,
		        $coupon_code,
		        $offer,
		        $promo_id,
		        $tracking,
		        $extra_tracking,
		        $exp_date,
		        $edit,
		        $delete,
		        ));
		        $no++; 
		    }

        	echo json_encode(array('draw'=>intval($draw),'recordsTotal'=>$totalrecords,'recordsFiltered'=>$totalrecords,'data' =>$data));
		}
	}
	function newusers($user_ids)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/dashboard','refresh');
		}
		else 
		{
			$totalrecords =  intval($this->input->get('totalrecords'));
	        $draw 		  =  $this->input->get('draw');
	        $start 		  =  $this->input->get('start');
	        $length 	  =  $this->input->get('length');
	        $search 	  = $this->input->get('search');
	        $searchtext    = $search['value'];
	        $totalrecords = count($this->admin_model->get_allusers($searchtext));
	        $results 	  = $this->admin_model->newusers($draw,$start,$length,$searchtext);
			$data    	  = array();
	        $no      	  = $start + 1;
	        foreach ($results as $r) 
	        {
	        	$checkbox    	= '<input type="checkbox"  class="check_b chksingle" name="chkbox['.$r->user_id.']" />';
	        	$user_id     	= $r->user_id;
	        	$user_name   	= $this->admin_model->user_names($r->user_id);          
	            if($user_name->first_name != '' && $user_name->last_name !='')
	            {
	              	$username 	= $user_name->first_name." ". $user_name->last_name;
	              	if($r->status == 0)
	              	{ $username = $username. "<br> (De-Activated)";}

	            }
	            else
	            {
	            	$ex_username = explode('@', $user_name->email);
	            	$username 	 = $ex_username[0];
	            	if($r->status == 0)
	              	{ $username = $username. "<br> (De-Activated)";}
	            }  
	            
	            
	            $email 			  = '<a href='.base_url().'adminsettings/view_user/'.$r->user_id.' target="_blank">'.$this->admin_model->user_email($r->user_id).'</a>';
				$balance 		  = $r->balance; 
		        $credit     	  = '<a href='.base_url().'adminsettings/manual_credit/'.$r->user_id.' alt="Transfer Money" ><i class="icon-money"></i></a>';
              
              	$total_withd_ss   = $this->db->query("SELECT COUNT(*) as counting FROM `withdraw` where user_id='$r->user_id'");
                $total_with_count = $total_withd_ss->row('counting');
              
              	$total_click_ss   = $this->db->query("SELECT COUNT(*) as counting FROM `click_history` where user_id='$r->user_id'");
                $total_click      = $total_click_ss->row('counting');
                $contact_no		  = $r->contact_no;
                $ifsc_code		  = $r->ifsc_code;
                $referral		  = $r->refer;
                $category_type	  = "RC".$r->referral_category_type;
                $referral_amt     = $r->referral_amt;
                $app_login		  = $r->app_login;
                $bonus_benefit    = $r->bonus_benefit;
                $newsletter_mail  = $r->newsletter_mail;
                $random_code	  = $r->random_code;
                $date_added		  = date('d/m/Y',strtotime($r->date_added));
                $delete     	  = '<a class="confirm-dialog" onclick="return confirmDelete();" href='.base_url().'adminsettings/deleteuser/'.$r->user_id.'><i class="icon-trash"></i></a>';

		        array_push($data,array(
		        //$no,
		        $checkbox,
		        $user_id,
		        $username,
		        $email,
		        $balance,
		        $credit,
		        $total_with_count,
		        $total_click,
		        $contact_no,
		        $ifsc_code,
		        $referral,
		        $category_type,
		        $referral_amt,
		        $app_login,
		        $bonus_benefit,
		        $newsletter_mail,
		        $random_code,
		        $date_added,
		        $delete,
		        ));
		        $no++; 
		    }
        	echo json_encode(array('draw'=>intval($draw),'recordsTotal'=>$totalrecords,'recordsFiltered'=>$totalrecords,'data' =>$data));
		}
	}
	/*New code for cashback page datatable modity details 17-12-16*/
	function newcashback($retailer_name,$ref_id)
	{	

		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/dashboard','refresh');
		}
		else 
		{
			 
			$totalrecords  =  intval($this->input->get('totalrecords'));
	        $draw 		   =  $this->input->get('draw');
	        $start 		   =  $this->input->get('start');
	        $length 	   =  $this->input->get('length');
	        $search 	   = $this->input->get('search');
	        $searchtext    = $search['value'];
	        $retailer_name = $this->input->get('affiliate_name');
	        $ref_id 	   = $this->input->get('ref_id');
	        //echo $retailer_name; echo "<br>"; echo $ref_id;
	        //$totalrecords = count($this->admin_model->cashback($searchtext));
	        $results 	   = $this->admin_model->newcashback($draw,$start,$length,$searchtext,$retailer_name,$ref_id);
	        $without_limit = $this->admin_model->newcashback($draw,$start,-1,$searchtext,$retailer_name,$ref_id);
	        $totalrecords  = count($without_limit);
	        
			$data    	   = array();
	        $no      	   = $start + 1;

	        //echo "<pre>"; print_r($results); exit; 
	        foreach ($results as $r) 
	        {
		        
		        $userdeails = $this->admin_model->user_names($r->user_id); 		        
	        	$ref_id     = $r->reference_id;
	        	$checkbox   = '<input type="checkbox"  class="check_b chksingle" name="chkbox['.$r->cashback_id.']" />';
	        	$user_id    = $r->user_id;
	        	//$user_name  = $this->admin_model->user_names($r->user_id);          
	            
	            if($userdeails->first_name == '' && $userdeails->last_name == '')
	            {
	              	$ex_username = explode('@', $userdeails->email); 
	            	$username    = $ex_username[0];
	            }
	            else
	            {
	            	$username = $userdeails->first_name." ". $userdeails->last_name;
	            } 
	           
	            $email = 
	            '<a href='.base_url().'adminsettings/view_user/'.$r->user_id.' target="_blank">'
	            .$userdeails->email.
	            '</a>';
		   		
		   		$store      = $r->affiliate_id;
		   		$cashback   = 'R$. '.$this->admin_model->currency_format($r->cashback_amount);
		   		$trans_amt  = 'R$. '.$this->admin_model->currency_format($r->transaction_amount);
		        $trans_date = date('d/m/Y',strtotime($r->date_added));

		        if($r->status == 'Completed' || $r->status == 'Approved')
		        {
              		$status = '<span class="label label-success">Completed</span>';
              	}
              	else if($r->status == 'Canceled')
                {
                    $status = '<span class="label label-danger">Canceled</span>';
                }
                else
                {
                	$status = '<span class="label label-info">Pending</span>';
                }

              	if($r->status == 'Pending')
              	{
              		$edit   = '<span class="label label-info">Pending</span>';
              	}
              	else
              	{
              		$edit = '<a href='.base_url().'adminsettings/editcashback/'.$r->cashback_id.'><i class="icon-pencil"></i></a>';
  				}            		
  				
  				$report_update_id = $r->report_update_id;
  				$newtrans_date 	  = date('d/m/Y',strtotime($r->transaction_date));
                $referral 		  = $r->referral;
                $commission		  = $r->commission;
                $trans_id         = $r->txn_id;
                $new_trans_id     = $r->new_txn_id;

                if($r->plataform == 1){ $platform = "Web"; } else if($r->plataform == 2) { $platform = "App";} else { $platform = 0; }
                $delete           = '<a class="confirm-dialog" onclick="return confirmDelete();" href='.base_url().'adminsettings/deletecashback/'.$r->cashback_id.'><i class="icon-trash"></i></a>';

		        array_push($data,array(
		        $checkbox,
		        $ref_id,
		        $user_id,
		        $username,
		        $email,
		        $store,
		        $cashback,
		        $trans_amt,
		        $trans_date,
		        $status,
		        $edit,
		        $report_update_id,
		        $newtrans_date,
		        $commission,
		        $referral,
		        $trans_id,
		        $new_trans_id,
		        $platform,
		        $delete,
		        ));
		        $no++; 
		    }

        	echo json_encode(array('draw'=>intval($draw),'recordsTotal'=>$totalrecords,'recordsFiltered'=>$totalrecords,'data' =>$data));
		}
	}
	/*End 17-12-16*/
	function newpending_cashback($retailer_name,$ref_id)
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/dashboard','refresh');
		}
		else 
		{
			 
	        $draw 		   =  $this->input->get('draw');
	        $start 		   =  $this->input->get('start');
	        $length 	   =  $this->input->get('length');
	        $search 	   = $this->input->get('search');
	        $searchtext    = $search['value'];
	        $retailer_name = $this->input->get('affiliate_name');
	        $ref_id 	   = $this->input->get('ref_id');
	        //$totalrecords = count($this->admin_model->pending_cashback($searchtext));
	        $results 	   = $this->admin_model->newpending_cashback($draw,$start,$length,$searchtext,$retailer_name,$ref_id);
			$without_limit = $this->admin_model->newpending_cashback($draw,$start,-1,$searchtext,$retailer_name,$ref_id);
	        $totalrecords  = count($without_limit);
			$data    	   = array();
	        $no      	   = $start + 1;

	        foreach ($results as $r) 
	        {
		        
	        	$checkbox  = '<input type="checkbox"  class="check_b chksingle" name="chkbox['.$r->cashback_id.']" />';
	        	$user_id   = $r->user_id;
	        	$user_name = $this->admin_model->user_names($r->user_id);          
	            
	            if($user_name)
	            {
	              	$username = $user_name->first_name." ". $user_name->last_name;
	            }
	            else
	            {
	            	$username = '';
	            } 
	           
	            $email = 
	            '<a href='.base_url().'adminsettings/view_user/'.$r->user_id.' target="_blank">'
	            .$this->admin_model->user_email($user_id).
	            '</a>';
		   		
		   		$store      = $r->affiliate_id;
		   		$trans_amt  = 'R$. '.$this->admin_model->currency_format($r->transaction_amount);
		   		$cashback   = 'R$. '.$this->admin_model->currency_format($r->cashback_amount);
		        $trans_date = date('d/m/Y',strtotime($r->cash_date));
		        $approve    = '<a href='.base_url().'adminsettings/pending_cashback/approve/'.$r->cashback_id.' class="confirm-dialog" onclick="return confirmDelete1();" title="Approve this cashback"><button class="btn btn-success" type="button">Approve</button></a>';
		        $cancel     = '<a href='.base_url().'adminsettings/pending_cashback/cancel/'.$r->cashback_id.' class="confirm-dialog" onclick="return confirmDelete2();" title="Cancel this cashback"><button class="btn btn-danger" type="button">Cancel</button></a>';
 
  				$report_update_id = $r->report_update_id;
 				$referral 		  = $r->referral;
 				$trans_id         = $r->txn_id;
                $new_trans_id     = $r->new_txn_id;
                $delete           = '<a class="confirm-dialog" onclick="return confirmDelete3();" href='.base_url().'adminsettings/pending_cashback/delete/'.$r->cashback_id.'><i class="icon-trash"></i></a>';

		        array_push($data,array(
		        $checkbox,
		        $no,
		        $user_id,
		        $username,
		        $email,
		        $store,
		        $trans_amt,
		        $cashback,
		        $trans_date,
		        $approve,
		        $cancel,
		        $report_update_id,
		        $referral,
		        $trans_id,
		        $new_trans_id,
		        $delete,
		        ));
		        $no++; 
		    }

        	echo json_encode(array('draw'=>intval($draw),'recordsTotal'=>$totalrecords,'recordsFiltered'=>$totalrecords,'data' =>$data));
		}
	}

	/*New datatable details fr withdraw page 21-1-17*/
	function newwithdraw()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/dashboard','refresh');
		}
		else 
		{
			//$totalrecords =  intval($this->input->get('totalrecords'));
	        $draw 		   =  $this->input->get('draw');
	        $start 		   =  $this->input->get('start');
	        $length 	   =  $this->input->get('length');
	        $search 	   = $this->input->get('search');
	        $searchtext    = $search['value'];
	        //$totalrecords = count($this->admin_model->withdraw($searchtext));
	        $results 	   = $this->admin_model->newwithdraw($draw,$start,$length,$searchtext);
	        $without_limit = $this->admin_model->newwithdraw($draw,$start,-1,$searchtext);
	        $totalrecords  = count($without_limit);
			$data    	   = array();
	        $no      	   = $start + 1;
	       
	        foreach ($results as $r) 
	        {
		        
	        	$userdeails = $this->admin_model->user_names($r->user_id);

	        	$checkbox    = '<input type="checkbox"  class="check_b chksingle" name="chkbox['.$r->withdraw_id.']" />';
	        	$withdraw_id = $r->withdraw_id;
	        	$user_id     = $r->user_id;
	        	
	        	if($userdeails->first_name == '' && $userdeails->last_name == '')
	            {
	              	$ex_username = explode('@', $userdeails->email); 
	            	$username    = $ex_username[0];
	            }
	            else
	            {
	            	$username = $userdeails->first_name." ". $userdeails->last_name;
	            } 
	           
	            $email = 
	            '<a href='.base_url().'adminsettings/view_user/'.$r->user_id.' target="_blank">'
	            .$userdeails->email.
	            '</a>'; 

				$user_balance = $userdeails->balance; 
		        $withdraw_amt = $r->requested_amount;
              	$date_added   = date('d/m/Y',strtotime($r->with_date));
              	//$closing_date = date('d-M-Y',strtotime($r->closing_date));
              	//echo $r->status;
				if(($r->with_status == 'Requested') || ($r->with_status == 'Processing'))
				{
					$closing_date = 'Open';
				}
				else 
				{
					$closing_date = date('d/m/Y',strtotime($r->closing_date));
				} 
                

                if($r->with_status == 'Requested')
		        {
              		$status = '<span class="label label-success">Requested</span>';
              	}
              	else if($r->with_status == 'Processing')
                {
                    $status = '<span class="label label-warning">Processing</span>';
                }
                else if($r->with_status == 'Completed')
                {
                	$status = '<span class="label">Completed</span>';
                }
                else
                {
                	$status = '<span class="label label-info">Cancelled</span>';
                }

                if($r->with_status == 'Completed')
	            { 
	            	$edit = '<center><span class="label">Completed</span></center>';
	            }
	            else if($r->with_status == 'Cancelled')
	            {
	            	$edit = '<center><span class="label label-info">Cancelled</span></center>';
	            }
	            else
	            {
	            	$edit  = '<a href='.base_url().'adminsettings/editwithdraw/'.$r->withdraw_id.'><i class="icon-pencil"></i>';
	            } 

                $delete       = '<a class="confirm-dialog" onclick="return confirmDelete();" href='.base_url().'adminsettings/deletewithdraw/'.$r->withdraw_id.'><i class="icon-trash"></i></a>';

		        array_push($data,array(
		        $checkbox,
		        $withdraw_id,
		        $user_id,
		        $username,
		        $email,
		        $user_balance,
		        $withdraw_amt,
		        $date_added,
		        $closing_date,
		        $status,
		        $edit,
		        $delete,
		        ));
		        $no++; 
		    }
        	echo json_encode(array('draw'=>intval($draw),'recordsTotal'=>$totalrecords,'recordsFiltered'=>$totalrecords,'data' =>$data));
		}
	}
	/*End 21-1-17*/
	function newtransactions()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/dashboard','refresh');
		}
		else 
		{
			//$totalrecords  =  intval($this->input->get('totalrecords'));
	        $draw 		   =  $this->input->get('draw');
	        $start 		   =  $this->input->get('start');
	        $length 	   =  $this->input->get('length');
	        $search 	   = $this->input->get('search');
	        $searchtext    = $search['value'];
	        //$totalrecords  = count($this->admin_model->transactions());
	        $results 	   = $this->admin_model->newtransactions($draw,$start,$length,$searchtext);
			$without_limit = $this->admin_model->newtransactions($draw,$start,-1,$searchtext);
	        $totalrecords  = count($without_limit);
			$data    	   = array();
	        $no      	   = $start + 1;
	        foreach ($results as $r) 
	        {
		        
	        	$checkbox    = '<input type="checkbox"  class="check_b chksingle" name="chkbox['.$r->trans_id.']" />';
	        	$user_id     = $r->user_id;
	        	$user_name   = $this->admin_model->user_names($r->user_id);          
	            
	            if($user_name->first_name == '' && $user_name->last_name == '')
	            {
	              	$ex_username = explode('@', $user_name->email); 
	              	$username    = $ex_username[0];
	            }
	            else
	            {
	            	$username = $user_name->first_name." ". $user_name->last_name;
	            }  
	            
	            $email  = 	'<a class="user" target="_blank" href='.base_url().'adminsettings/view_user/'.$r->user_id.'>'.
	            		    $this->admin_model->user_email($user_id).'</a>';

				
				if($r->transation_reason == 'Cashback'){ $trans_reason = 'Cashback'; } 
				else if($r->transation_reason == 'Credit Account'){ $trans_reason = 'Credit Account'; }
				else { $trans_reason = 'Referal Payment'; }

				$amount      = $this->admin_model->currency_format($r->transation_amount);
				$trans_date	 = date('d/m/Y',strtotime($r->transation_date));
				$transc_date = date('d/m/Y',strtotime($r->transaction_date));
		        $trans_id    = $r->transation_id;

		        $table       = $r->table;
		        $details_id  = $r->details_id;
		        
		        if($table == 'missing_cashback')
		        {
		        	$report_update_id = $table ."-". $details_id;
		        }
		        else
		        {
		        	$report_update_id = $r->report_update_id; 
		        }

		        $new_txn_id  = $r->new_txn_id;
		        $ref_utra_id = $r->ref_user_tracking_id;
		          
		        if($r->transation_status=='Approved' || $r->transation_status=='Paid')
				{
                    $status = '<span class="btn btn-success" >Approved</span>';
                }
                else
                {
                	$status = '<span class="btn btn-danger" >'.$r->transation_status.'</span>';
	            }

	            if($r->transation_reason != 'Cashback')
	            { 
	            	$edit  = '<a href='.base_url().'adminsettings/edittransactions/'.$r->trans_id.'><i class="icon-pencil"></i>';
	            }
	            else
	            {
	            	$edit = '<center><span class="label label-info">Cashback</span></center>';
	            } 
              	 
                //$delete     = '<a class="confirm-dialog" onclick="return confirmDelete();" href='.base_url().'adminsettings/deletehistory/'.$r->click_id.'><i class="icon-trash"></i></a>';

		        array_push($data,array(
		        $checkbox,
		        $no,
		        $user_id,
		        $username,
		        $email,
		        $trans_reason,
		        $amount,
		        $trans_date,
		        $transc_date,
		        $trans_id,
		        $report_update_id,
		        //$table,
		        //$details_id,
		        $new_txn_id,
		        $ref_utra_id,
		        $status,
		        $edit,
		        ));
		        $no++; 
		    }

        	echo json_encode(array('draw'=>intval($draw),'recordsTotal'=>$totalrecords,'recordsFiltered'=>$totalrecords,'data' =>$data));
		}
	}
	function newclickhistory($user_id)
	{	
		//echo "hai".$user_id;
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/dashboard','refresh');
		}
		else 
		{
			//$totalrecords =  intval($this->input->get('totalrecords'));
	        $draw 		    =  $this->input->get('draw');
	        $start 		    =  $this->input->get('start');
	        $length 	    =  $this->input->get('length');
	        $search 	    = $this->input->get('search');
	        $searchtext     = $search['value'];
	        $affiliate_name = $this->input->get('affiliate_name');
	        //$totalrecords = count($this->admin_model->click_history($searchtext));
	        $results 	    = $this->admin_model->newclickhistory($draw,$start,$length,$searchtext,$affiliate_name);
			$without_limit  = $this->admin_model->newclickhistory($draw,$start,-1,$searchtext,$affiliate_name);
	        $totalrecords   = count($without_limit);
			$data    	    = array();
	        $no      	    = $start + 1;
	        foreach ($results as $r) 
	        {
		        
	        	$checkbox    = '<input type="checkbox"  class="check_b chksingle" name="chkbox['.$r->click_id.']" />';
	        	$user_id     = $r->user_id;
	        	$user_name   = $this->admin_model->user_names($r->user_id);       

	            if($user_name->first_name == '' && $user_name->last_name == '')
	            {
	              	$ex_username = explode('@', $user_name->email);
	            	$username 	 = $ex_username[0];

	            }
	            else
	            {
	            	$username = $user_name->first_name." ". $user_name->last_name;
	            } 
	            
	            
	            $email = 
	            '<a href='.base_url().'adminsettings/view_user/'.$r->user_id.' target="_blank">'
	            .$this->admin_model->user_email($user_id).
	            '</a>';
	            

				$store_name = $r->store_name; 
		        $ip_address = $r->ip_address;
              	$date_added = date('d/m/Y',strtotime($r->click_date));
                $delete     = '<a class="confirm-dialog" onclick="return confirmDelete();" href='.base_url().'adminsettings/deletehistory/'.$r->click_id.'><i class="icon-trash"></i></a>';

		        array_push($data,array(
		        $checkbox,
		        $no,
		        $user_id,
		        $username,
		        $email,
		        $store_name,
		        $ip_address,
		        $date_added,
		        $delete,
		        ));
		        $no++; 
		    }

        	echo json_encode(array('draw'=>intval($draw),'recordsTotal'=>$totalrecords,'recordsFiltered'=>$totalrecords,'data' =>$data));
		}
	}

	/*New data table code for affiliate page 18-2-17*/
	function newaffiliates()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/dashboard','refresh');
		}
		else 
		{
			 
			$totalrecords = intval($this->input->get('totalrecords'));
	        $draw 		  = $this->input->get('draw');
	        $start 		  = $this->input->get('start');
	        $length 	  = $this->input->get('length');
	        $search 	  = $this->input->get('search');
	        $searchtext   = $search['value'];
	        //$api_name   = $this->input->get('api_name');
	        $results 	  = $this->admin_model->newaffiliate($draw,$start,$length,$searchtext);
			$without_limit= $this->admin_model->newaffiliate($draw,$start,-1,$searchtext);
	        $totalrecords = count($without_limit);
			$data    	  = array();
	        $no      	  = $start + 1;
	        foreach ($results as $r) 
	        {
		       	
	        	if($r->featured==1)
                {
        			$extraset = 'style="background-color:#32c2cd"';
        		}
                else if($r->store_of_week==1)
                {
        			$extraset = 'style="background-color:#a5d16c"';
        		}
                else if($r->store_of_week==1 && $r->featured==1)
                {
        			$extraset = 'style="background-color:#e74955"';
        		}
                else
                {
        			$extraset = '';
        		}
        			//echo $extraset;
                if($r->affiliate_status == 0)
                {
                	$status = '(De-active)';
                }
                else
                {
                	$status = '';
                }

                if($r->sort_order!='')
				{
					$sort_order =$r->sort_order;
				}
				else
				{
					$sort_order=0;
				}

		       	$coupon_count   = $this->admin_model->count_coupons($r->affiliate_name); 
		       	$visit_count	= $this->admin_model->count_clicks($r->affiliate_id);
		       	$checkbox   	= '<input type="checkbox"  class="check_b chksingle" name="chkbox['.$r->affiliate_id.']" />';
				$affiliate_id   = '<a target="_blank" href='.base_url().'adminsettings/cashback/'.$r->affiliate_url.'>'.$r->affiliate_id.'</a>';
				

				if(strpos($r->affiliate_logo,"http") !== false)
				{
					$affiliate_logo = '<img src='.$r->affiliate_logo.' width="78" height="50" /></center>';
				}
				else
				{
					$affiliate_logo = '<img src='.$this->admin_model->get_img_url().'uploads/affiliates/'.$r->affiliate_logo.' width="78" height="50" /></center>';
				}

				
				$affiliate_name = '<a target="_blank" href='.base_url().'cupom-desconto/'.$r->affiliate_url.'>'.$r->affiliate_name.'</a>'.$status; 	        	
				$edit   		= '<a href='.base_url().'adminsettings/editaffiliate/'.$r->affiliate_id.'><i class="icon-pencil"></i></a>';	        	  
				$coupon_counts  = '<a target="_blank" href='.base_url().'adminsettings/coupons/'.$r->affiliate_name.'>'.$coupon_count->counting.'</a>';
				$cashB  		= $r->cashback_content_android;
				$oldcashback    = $r->old_cashback;
				$visits_count   = '<a target="_blank" href='.base_url().'adminsettings/click_history/'.$r->affiliate_url.'>'.$visit_count->counting.'</a>';
				$store_category = $r->store_categorys;
				$report_date    = $r->report_date;
				$no_days		= $r->retailer_ban_url;
				//$pay_in			= $r->retailer_ban_url;
				$track_param	= $r->tracking_param;
				$extra_param    = $r->extra_tracking_param;
				$content_param  = $r->content_extra_param;
				$android_param  = $r->content_extra_param_android;

				$actions        = 	'<div class="all_des">
	                              		<a target="_blank" href='.base_url().'adminsettings/cashback_details/'.$r->affiliate_id.'><i class="icon-tasks"></i></a>
	                              		<input class="span12" type="number" size="4" value='.$sort_order.' name="sort_arr['.$r->affiliate_id.']">
	                              		<a class="confirm-dialog" onclick="return confirmDelete();" href='.base_url().'adminsettings/deleteaffiliate/'.$r->affiliate_id.'><i class="icon-trash"></i></a>
                              		</div>	
									';
		        array_push($data,array(
		        $checkbox,
		        $affiliate_id,
		       	$affiliate_logo,
		        $affiliate_name,
		        $edit,
		        $coupon_counts,
		        $cashB,
		        $oldcashback,
		        $visits_count,
		        $store_category,
		        $report_date,
		        $no_days,
		        //$pay_in,
		        $track_param,
		        $extra_param,
		        $content_param,
		        $android_param,
		        $actions
		        ));
		        $no++; 
		    }
        	echo json_encode(array('draw'=>intval($draw),'recordsTotal'=>$totalrecords,'recordsFiltered'=>$totalrecords,'data' =>$data));
		}
	}
	/*End*/

	/*New code for report page 25-1-17*/
	function newreport()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/dashboard','refresh');
		}
		else 
		{
			$totalrecords =  intval($this->input->get('totalrecords'));
	        $draw 		  =  $this->input->get('draw');
	        $start 		  =  $this->input->get('start');
	        $length 	  =  $this->input->get('length');
	        $search 	  = $this->input->get('search');
	        $searchtext    = $search['value'];
	        $totalrecords = count($this->admin_model->reports());
	        $results 	  = $this->admin_model->newreports($draw,$start,$length,$searchtext);
			$data    	  = array();
	        $no      	  = $start + 1;
	       	
	        foreach ($results as $r) 
	        {
		        
	        	$checkbox       = '<input type="checkbox"  class="check_b chksingle" name="chkbox['.$r->report_id.']" />';
				$offer_provider = $r->offer_provider; 	        	
	        	$date	 		= date('d/m/Y',strtotime($r->date)); 
				$payout_amt     = $this->admin_model->currency_format($r->pay_out_amount);
				$sale_amount    = $r->sale_amount;
				$trans_id       = $r->transaction_id;
				$cashback_amt   = $r->cashback_amount;
				$update_time    = date('d/m/Y',strtotime($r->last_updated)); 
                $view       	= '<a href='.base_url().'adminsettings/view_report/'.$r->report_id.'><i class="icon-eye-open"></i></a>';

		        array_push($data,array(
		        $checkbox,
		        $no,
		        $offer_provider,
		        $date,
		        $payout_amt,
		        $sale_amount,
		        $trans_id,
		        $cashback_amt,
		        $update_time,
		        $view,
		        ));
		        $no++; 
		    }

        	echo json_encode(array('draw'=>intval($draw),'recordsTotal'=>$totalrecords,'recordsFiltered'=>$totalrecords,'data' =>$data));
		}
	}
	/*End 25-1-17*/

	/*data table details for API coupons pages 13-2-17*/
	function newaffiliate_network($apiname)
	{
		 
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id=="")
		{
			redirect('adminsettings/dashboard','refresh');
		}
		else 
		{
			 
			$totalrecords = intval($this->input->get('totalrecords'));
	        $draw 		  = $this->input->get('draw');
	        $start 		  = $this->input->get('start');
	        $length 	  = $this->input->get('length');
	        $search 	  = $this->input->get('search');
	        $searchtext   = $search['value'];
	        $api_name     = $this->input->get('api_name');
	        $results 	  = $this->admin_model->affiliate_network($draw,$start,$length,$searchtext,$api_name);
			$without_limit= $this->admin_model->affiliate_network($draw,$start,-1,$searchtext,$api_name);
	        $totalrecords = count($without_limit);
			$data    	  = array();
	        $no      	  = $start + 1;
	        foreach ($results as $r) 
	        {
		        
				$store_name     = $r->offer_name; 	        	
	        	$start_date		= date('d/m/Y',strtotime($r->start_date));
	        	$expiry_date	= date('d/m/Y',strtotime($r->expiry_date)); 
				$title     		= $r->title;
				$link 		    = '<a target="_blank" href='.$r->offer_page.'>Link</a>';
				$code           = $r->code;
				$description    = $r->description;
                //$view       	= '<a href='.base_url().'adminsettings/view_report/'.$r->report_id.'><i class="icon-eye-open"></i></a>';
				$actions        =   '<a title="Approval" href='.base_url().'adminsettings/api_change_approval/'.$r->coupon_id.'/'.$r->coupon_status.'/'.$r->api_name.' onclick="return changeStatus();"><i class="icon-exchange"></i></a>
									<a  title="Edit"     href='.base_url().'adminsettings/editcoupon/'.$r->coupon_id.'/'.$r->api_name.'><i class="icon-pencil"></i></a>&nbsp;
									<a  title="Delete"   href='.base_url().'adminsettings/api_deletecoupon/'.$r->coupon_id.'/'.$r->api_name.' class="confirm-dialog"   onclick="return confirmDelete();" ><i class="icon-trash"></i></a>&nbsp;	
									';	

                                /*echo anchor('adminsettings/api_change_approval/'.$affiliate->coupon_id.'/'.$affiliate->coupon_status.'/'.$affiliate->api_name,'<i class="icon-exchange"></i>',$confirm);
                                 $attr =array('title'=>'Edit');
              									echo anchor('adminsettings/editcoupon/'.$affiliate->coupon_id.'/'.$affiliate->api_name,'<i class="icon-pencil"></i>',$attr);           									            
                                 $confirm = array("class"=>"confirm-dialog",'title'=>'Delete',"onclick"=>"return confirmDelete('Do you want to delete this affiliate detail?');");   
                                echo anchor('adminsettings/api_deletecoupon/'.$affiliate->coupon_id.'/'.$affiliate->api_name,'<i class="icon-trash"></i>',$confirm);  */
		        array_push($data,array(
		        $no,
		        $store_name,
		        $start_date,
		        $expiry_date,
		        $description,
		        $link,
		        $code,
		        $title,
		        $actions,
		        ));
		        $no++; 
		    }
        	echo json_encode(array('draw'=>intval($draw),'recordsTotal'=>$totalrecords,'recordsFiltered'=>$totalrecords,'data' =>$data));
		}
	}
	/*End 13-2-17*/

	/*New code for extension realted Unic Bonus page content 23-1-17*/
	function content()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if(($admin_id=="")){
			redirect('adminsettings/index','refresh');
		}else{

			$get_ex_bonus_template = $this->db->query("SELECT * from ex_bonus_content where log_status=1")->result(); 
			foreach($get_ex_bonus_template as $get)
			{
				$data['content_id'] 		 = $get->content_id;
				$data['not_log_content']  	 = $get->not_logged_content;
				$data['not_log_status'] 	 = $get->not_log_status;
				$data['logged_content'] 	 = $get->logged_content;
				$data['log_status'] 		 = $get->log_status;
			}
			$this->load->view('adminsettings/content',$data);
		}
	}
	function update_unic_bonus()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		}else{

			if($this->input->post('save'))
			{
				$not_log_template 	  = $this->input->post('not_log_content');
				$logged_content 	  = $this->input->post('logged_content');
				$content_id			  = $this->input->post('content_id');
				 
				if($not_log_template == "")
				{
					$this->session->set_flashdata('not_log_template', $not_log_template);
					$this->session->set_flashdata('logged_content', $logged_content);
					$this->session->set_flashdata('error', 'Please enter some message.');
					redirect('adminsettings/content');
				}
				else 
				{ 
					$results = $this->admin_model->update_unic_bonus();
					if($results) {
						$this->session->set_flashdata('success', 'Unci Bonus Template updated successfully.');
						redirect('adminsettings/content');
					}
					else {
						$this->session->set_flashdata('error', 'Error occurred while updating Unic Bonus template.');
						redirect('adminsettings/content');
					}
				}
			}
		}
	}
	/*End*/

	/*New code for exit popup details 15-11-16*/
	function first_access_popup()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if(($admin_id=="")){
			redirect('adminsettings/index','refresh');
		} else
		{
			$get_popup_template = $this->db->query("SELECT * from first_access_popup where first_popup_id!=0")->result();
			 
			foreach($get_popup_template as $get){
				$data['first_popup_id'] 			 = $get->first_popup_id;
				$data['first_popup_subject']   		 = $get->first_popup_subject;
				$data['first_popup_template'] 		 = $get->first_popup_template;
				$data['store_firstpopup_template']	 = $get->store_firstpopup_template;
				$data['store_firstpopup_template_nocash'] = $get->store_firstpopup_template_nocash;
				$data['all_status_firstpopup'] 		 = $get->all_status_firstpopup;
				$data['store_status_firstpopup'] 	 = $get->store_status_firstpopup;
			}
			 
			$this->load->view('adminsettings/first_access_popup',$data);
		}
	}
	function update_first_access_popup()
	{
		$this->input->session_helper();
		$admin_id = $this->session->userdata('admin_id');
		if($admin_id==""){
			redirect('adminsettings/index','refresh');
		} else
		{
			if($this->input->post('save'))
			{	
				if($this->input->post('first_popup_template') == "")
				{
					$this->session->set_flashdata('first_popup_subject', $this->input->post('first_popup_subject'));
					$this->session->set_flashdata('first_popup_id', $this->input->post('first_popup_id'));
					$this->session->set_flashdata('error', 'Please enter some message.');
					redirect('adminsettings/first_access_popup');
				}
				else 
				{ 
					$results = $this->admin_model->update_first_access_popup();
					if($results) 
					{
						$this->session->set_flashdata('success', 'First Access Popup Template updated successfully.');
						redirect('adminsettings/first_access_popup');
					}
					else 
					{
						$this->session->set_flashdata('error', 'Error occurred while updating First Access Popup template.');
						redirect('adminsettings/first_access_popup');
					}
				}
			}
		}
	}
	/*End 15-11-16*/

}
?>