<?php
class Cashback extends CI_Controller
{
 	public function __construct(){	
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model('front_model');
		session_start();
		
		  
		/*New code for not log user ip address added into database 18-11-16*/
		$user_id = $this->session->userdata('user_id');
		if($user_id == '')
		{
			$user_ip_details = $this->front_model->set_ip_address();
		}
		/*End 18-11-16*/
		if($this->uri->segment(1) == 'cms')
		{
			//redirect('404','refresh');
			redirect('','refresh');
		}

	}

	function index()
	{
		
		//echo $name;
		//echo $date = date("Y-m-d H:i:s:A");
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		$cookiehandlers = $this->security->cookie_handlers();
		$admindetails = $this->front_model->getadmindetails();
		$data['admindetails'] = $admindetails;
		$data['result'] = $this->front_model->home_slider();
		$data['featured_product']=$this->front_model->getall_fetured_products();
		$data['popular']=$this->front_model->popular_products_index();  
		$data['latest']=$this->front_model->latest_products_index();

		/*New code start*/

			//New code for cashback excusive details 21-7-16.//

		    $link_name       = $_REQUEST['pro'];
		    $fulllinkdetails = '?pro='.$_REQUEST['pro'];  
		    $split_details   = explode('-',$link_name);
		    $user_name       = $split_details['0'];
		    $store_name      = $split_details['2'];

		    $cash_query     = $this->db->query("SELECT * from cashback_exclusive where link_name='$fulllinkdetails'")->row();
		    //echo print_r($cash_query);
		    $cash_ex_id   	= $cash_query->id;
		    $storenames   	= $cash_query->store_name;
		    $link_name    	= $cash_query->link_name;
		    $expiry_dates 	= $cash_query->expirydate;
		    $cashback_web 	= $cash_query->cashback_web;
		    $analytics_info = $cash_query->analytics_info;


		    $selqry          = $this->db->query("SELECT * from affiliates where affiliate_name='$storenames'")->row();
		    $affiliate_urls  = $selqry->affiliate_url;
		    $affiliate_names = $selqry->affiliate_name;
		    

		    if($cash_ex_id!='')
		    {
		    	$this->session->set_userdata('cash_ex_id',$cash_ex_id);
		    	$this->session->set_userdata('storenames',$storenames);
		    	$this->session->set_userdata('affiliate_urls',$affiliate_urls);
		    	$this->session->set_userdata('link_name',$link_name);
		    	$this->session->set_userdata('affiliate_names',$affiliate_names);	
		    	$this->session->set_userdata('expiry_dates',$expiry_dates); 
		    	$this->session->set_userdata('cashback_web',$cashback_web);
		    	$this->session->set_userdata('analytics_info',$analytics_info); 
		    }
		    

		  if($storenames)
		  { 
		    if($storenames == $affiliate_names)
		    {
		      
		      /*new changes 16-7-16*/
		      ?>
		      <form name='fr' action='cupom-desconto/<?php echo $affiliate_urls;?>/<?php echo $analytics_info;?>' method='POST'>
		        <input type='hidden' name='linkdetails' value='<?php echo $link_name;?>'>
		        <input type='hidden' name='id' value='<?php echo $cash_ex_id;?>'>
		        <input type='hidden' name='cashback_details' value='<?php echo $affiliate_urls;?>'>
		      </form>
		      <script type='text/javascript'>
		      
		      document.fr.submit();
		      </script>
		      <?php 
		     }
		  }

		/*New code end*/




		if($this->input->post('register'))
		{	//print_r($_POST); exit;
		   $ins_qry = $this->front_model->register();
		   if($ins_qry)
			{	
				$this->session->set_flashdata('success','Users details added successfully.');
				redirect('complete','refresh');
			}
			else
			{
					$this->session->set_flashdata('error','Errors occurs while adding users details.');
					redirect('','refresh');
			} 
		} 

		$this->load->view('front/index',$data);
		$cookiehandlers = $this->security->cookie_handlers();
	}
	
	//login check (SL)
	function login()
	{
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id!="")
		{
			redirect('minha-conta','refresh');
		} else {
					$this->load->view('front/login','refresh');
		}
		$cookiehandlers = $this->security->cookie_handlers();
	}
	
	function chk_invalid()
	{ 
	 
		$this->input->session_helper();
		if($this->input->post('signin')) 
		{
			$result = $this->front_model->login();
			if($result==0)
			{	
				$data['invalid_login']='Invalid username and password1.';
				$this->load->view('front/login',$data);
			} 
			else if($result==2)
			{
				$data['invalid_login']='Your account is de-activated.';
				$this->load->view('front/login',$data);
			}
			else
			{
				redirect('minha-conta','refresh');
			}
		}
		else
		{
			redirect('login','refresh');
		}
		$cookiehandlers = $this->security->cookie_handlers();
	}
	
	//registration form_validation
	function cadastro($cat_type_url)
	{
		
		//print_r($_POST);
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id!="")
		{
			redirect('minha-conta','refresh');
		}



		$pagenames 	   = $this->uri->segment(1);
		$pagename  	   = explode('-', $pagenames);
		$cat_type_url  = $pagename[1];

		$this->session->set_userdata('cat_type',$cat_type_url);
		
		$arrcat_type   = array('3454'=>2,'8765'=>3,'2345'=>4,'1647'=>5,'6536'=>6,'7486'=>7,'8362'=>8,'9326'=>9,'2695'=>10);
		

		$fullpagename  		= $this->input->post('fullpagename');
		/*$admindetails 		= $this->front_model->getadmindetails();
		$cat_two_status 	= $admindetails[0]->cat_two_status;
		$cat_three_status 	= $admindetails[0]->cat_three_status;
		$cat_four_status 	= $admindetails[0]->cat_four_status;
		$cat_five_status 	= $admindetails[0]->cat_five_status;*/
		//$cat_type_url       = $this->input->post('categorytype');
		if (array_key_exists($cat_type_url,$arrcat_type))
		{
			$data['category_type'] = $arrcat_type[$cat_type_url];
			$cookiehandlers = $this->security->cookie_handlers();
			
			$category_status = $this->db->query("SELECT * from referral_settings where ref_id=".$arrcat_type[$cat_type_url])->row();
			if($category_status->cat_type_status == 1)
			{
				$this->load->view('front/cadastro',$data);
			}
			else
			{
				redirect(base_url(),'refresh');
			}
		}
		else
		{
		 	if($this->input->post('register'))
			{	 
			    //echo "hai";exit;
			    $ins_qry = $this->front_model->register();
			   
			    if($ins_qry == 0)
				{	 
					$this->session->set_flashdata('success','Users details added successfully.');
					redirect('complete','refresh');
				}
				if($ins_qry == 1)
				{
					$this->session->set_flashdata('success','Users details added successfully.');
					redirect($fullpagename,'refresh');	
					?>	
					<!--<script>window.location.href = '<?php echo $fullpagename; ?>';</script>-->
				<?php 
				}
			}
		 	redirect(base_url(),'refresh');
		}
		
	}

	 	
	//check email for registration
	function check_email()
	{
		
		$email = $this->input->post('email');
		$result = $this->front_model->check_email($email);
		if($result)
		{
			echo 1;
		} else {
			echo 0;
		}
		$cookiehandlers = $this->security->cookie_handlers();
	}
	
//check email for registration SATz
	function check_cpf()
	{
		
		$ifsc_code = $this->input->post('ifsc_code');
		$result = $this->front_model->check_cpf($ifsc_code);
		if($result)
		{
			echo 1;
		} else {
			echo 0;
		}
		$cookiehandlers = $this->security->cookie_handlers();
	}
	

	//minha-conta page  
	function minha_conta()        // ALRTEREI era myaccount //
	{	
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id=="")
		{
			 
			$this->load->view('front/index');
			//redirect('minha-conta','refresh');
		}	 
		else
		{
		 
		// echo $user_id;
			$bank_details = $this->front_model->bank_details();
			//$edit_qry = $this->front_model->edit_account($user_id);

			$data['notify'] = $bank_details;

			$email_notify = $this->front_model->userdetails($user_id);
			$data['results'] = $email_notify;
			$this->load->view('front/minha-conta',$data);

		}
		$cookiehandlers = $this->security->cookie_handlers();
	}

	//New code for update mail notification details//

	function notifymail_update()
	{
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id=="")
		{
			redirect('','refresh');
		}	 
		else
		{  	
			if($this->input->post('save'))
			{	
				//echo print_r($_POST); exit;
		   		$new_qry = $this->front_model->notifymail_update();
		   		//echo $ins_qry; exit;
		   		if($new_qry)
				{	
					$this->session->set_flashdata('success','Users details Updated successfully.');
					redirect('minha-conta','refresh');
				}
			}	
		}
	}
	//End//

	// update the user details
	function update_account()
	{
		//echo "<pre>";print_r($_POST); exit;
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id==""){
			redirect('','refresh');
		} else
		{
			if($this->input->post('save_changes')){
	
				$updated = $this->front_model->update_account();
				if($updated){
					$this->session->set_flashdata('success', ' Users details updated successfully.');
					redirect('minha-conta','refresh');
				}
				else{
					$this->session->set_flashdata('error', 'Error occurred while updating the user details.');
					redirect('minha-conta','refresh');
				}
			}
		}
		$cookiehandlers = $this->security->cookie_handlers();
	}
	
	//change password
	function change_password()
	{
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		//echo $user_id;
		if($user_id==""){
			redirect('','refresh');
		} else 
		{
			if($this->input->post('save')){
			$result = $this->front_model->update_password();
				if($result){
					$this->session->set_flashdata('success', 'Password changed successfully.');
					redirect('minha-conta','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Old Password did not match.');
					redirect('minha-conta','refresh');
				}
			}	
		}
		$cookiehandlers = $this->security->cookie_handlers();
	}
	
	/*//bankpayment_form
	function bankpayment()
	{
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id==""){
			redirect('index','refresh');
		} else 
		{
			if($this->input->post('save_chages'))
			{	//echo print_r($_POST); exit;
				$res = $this->front_model->bankpayment();
				if($res){
					$this->session->set_flashdata('success', 'User Bankpayment details added successfully.');
					redirect('minha-conta','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while adding the user bankpayment details.');
					redirect('minha-conta','refresh');
				}
			}
		}
	$cookiehandlers = $this->security->cookie_handlers();
	}
	*/





//bankpayment_form
	function bankpayment()
	{
		
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		//$user_id=214;

		if($user_id==""){
			redirect('','refresh');
		} else 
		{
		if($this->input->post('save_chages'))
			{	
			  $check_ifcs_code = $this->front_model->userdetails($user_id);
			
             if(!$check_ifcs_code->ifsc_code){
                    $res = $this->front_model->bankpayment();             
             }else{
             
                   $res = $this->front_model->bankpayment_ifsc();
		          /*  $this->session->set_flashdata('error', 'Error occurred while adding the user bankpayment details.');
					redirect('minha-conta','refresh');*/
			 }
				
	//	$res = $this->front_model->bankpayment();
				if($res){
					$this->session->set_flashdata('success', 'User Bankpayment details added successfully.');
					redirect('minha-conta','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while adding the user bankpayment details.');
					redirect('minha-conta','refresh');
				}
			}
		}
	$cookiehandlers = $this->security->cookie_handlers();
	}
	




	//cheque_payment
	function cheque_payment()
	{
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id==""){
			redirect('','refresh');
		} else 
		{
			if($this->input->post('Save_Changes'))
			{	
				$res = $this->front_model->cheque_payment();
				if($res){
					$this->session->set_flashdata('success', 'cheque details added successfully.');
					redirect('minha-conta','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while adding the cheque details.');
					redirect('minha-conta','refresh');
				}
			} 
		}
		$this->security->cookie_handlers();
	}
	//forget_password
	function forgetpassword()
	{
		
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id!=""){
			redirect('','refresh');
		} else
		{
			if($this->input->post('reset'))
			{			
				$res = $this->front_model->forgetpassword($user_id);
				if($res){
					$this->session->set_flashdata('success', 'Your password  details sent successfully.');
					redirect('forgetpassword','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while send the password details.');
					redirect('forgetpassword','refresh');
				}
			} 
			$this->load->view('front/forgetpassword');
		}
		$this->security->cookie_handlers();
	}
	//thanku  
	function complete()
	{
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id!="")
		{
			redirect('minha-conta','refresh');
		}
		
		$this->session->set_userdata('complete','complete');
		$complete = $this->session->userdata('complete');
		if($complete!="")
		{
			$this->load->view('front/thanku');
		}
		$this->session->unset_userdata($complete);
		$this->security->cookie_handlers();
	}
	
	//reset_password
	function reset_password($a,$b,$c) 
	{
		/*if(($a=='') || ($b=='')|| ($c==''))	
		{
			redirect('register','refresh');
		}
		else
		{*/
			$user_id = $this->session->userdata('user_id');
			if($user_id=="")
			{
				redirect('','refresh');
			}
			else 
				{
					if($this->input->post('Save'))
					{	
						$res = $this->front_model->reset_password($user_id);
						if($res){
							$this->session->set_flashdata('success', 'Your Password updated successfully.');
							redirect('reset_password','refresh');
						}
						else
						{
							$this->session->set_flashdata('error', 'Error occurred while updating your password.');
							redirect('reset_password','refresh');
						}
					}  
					$this->load->view('front/reset_password');
				} 
		/*}*/
		$this->security->cookie_handlers();
	}
 
	//logout section
	function logout(){
		

		$sessionData = $this->session->all_userdata();
		foreach($sessionData as $key =>$val)
		{
			 $this->session->unset_userdata($key);
			/*if($key!='session_id' 
		       	&& $key!='last_activity' 
		      	&& $key!='ip_address' 
		       	&& $key!='user_agent' 
		       	&& $key!='admin_id'){
		         $this->session->unset_userdata($key);
		     }*/
		}
		$this->session->sess_destroy();

		$this->session->set_flashdata('success', 'Logged Out successfully.');
		$this->security->cookie_handlers();
		redirect('','refresh');
		
	}
// End (SL)	
	
//email subscribe
	
	function email_subscribe(){
		$email = $this->input->post('email');
		$vars = $this->front_model->email_sub($email);
		$this->security->cookie_handlers();
		echo $vars;
	}
//header menu page

	function header_menu($name)
	{
		$result = $this->front_model->header_menu($name);
		$this->security->cookie_handlers();
		echo $result;
	}	
//footer menu page
	
	function cms($names)
	{	
		
		/*header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");*/
		$this->input->session_helper();	
		$user_id 		= $this->session->userdata('user_id');
		$data['users']  = $this->front_model->userdetails($user_id);
		$data['admin']  = $this->front_model->admindetails();
		$data['result'] = $this->front_model->cms_content($names);
		$data['names']  = $names;
		$this->load->view('front/cms',$data);
		$this->security->cookie_handlers();
	}
	
	function onetime_benefit()
	{	
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		
		if($user_id==""){
			redirect('','refresh');
		} 
		else
		{	 
			if($this->input->post('userid'))
			{	
				$bonus_count = $this->front_model->getunicbonuscount();

				if($user_id == $this->input->post('userid') && $bonus_count==0)	
				{	//print_r($_POST); exit;
			   		$new_qry = $this->front_model->onetime_benefit();
					$this->security->cookie_handlers();
					echo $new_qry;	
				}
			}	
		}
		
	}
	
//Refer friends
	function indique_e_ganhe()
	{		
	 	$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id==""){
			$this->load->view('front/index');
		} 
		else
		{
			$data['result'] = $this->front_model->referral_network(); /*ALTEREI inclui da função refferal_network que  nao existe mais.. comentada aqui em baixo*/
			$this->load->view('front/indique-e-ganhe',$data);  /*ALTEREI inclui da função refferal_network que  nao existe mais.. comentada aqui em baixo esse item era $this->load->view('front/indique_e_ganhe');*/
		}
	}

	
    //Invite Freiends and send mail
	function invite_mail()
	{
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id==""){
			redirect('','refresh');
		} 
		else
		{
			$result = $this->front_model->invite_mail();
			//echo $result;
		}
	}
	
	//Referral Friends Network

	/*	function referral_network()
		{
			$this->input->session_helper();
			$user_id = $this->session->userdata('user_id');
			if($user_id==""){
				redirect('cashback/index','refresh');
			} 
			else
			{
				$data['result'] = $this->front_model->referral_network();
				$this->load->view('front/referral_network',$data);
			}
		}*/
	
//load contact
	function contato()
	{
		$this->security->cookie_handlers();
		$admindetails = $this->front_model->getadmindetails();
		$data['admindetails'] = $admindetails;
		$this->load->view('front/contato',$data);
	}	

//contact form details
	function contact_form()
	{
		$this->input->session_helper();
		$result = $this->front_model->contact_form();
		if($result){
			$this->session->set_flashdata('success', 'Your message has been sent successfully, we will contact you soon.');
			redirect('contato','refresh');
		}
		else{
			$this->session->set_flashdata('error', 'Error occurred while adding CMS.');
			redirect('contato','refresh');
		}
		$this->security->cookie_handlers();
	}
	
	
	
/*********************Nathan Start*************************/
/******Nov 19 th*********/

	
	function resgate($status=null)
	{
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id=="")
		{
			$this->load->view('front/index');
		}	 
		else
		{
			$edit_qry = $this->front_model->edit_account($user_id);
			$data['results'] 	 = $edit_qry;
			$data['user_id'] 	 = $user_id;
			$data['status']  	 = $status;   /*ALTEREI INCLUÍ DO MY_PAYMENTS FUNÇÃO COMENTADA ABAIXO*/
			$data['result']  	 = $this->front_model->my_payments($user_id);   /*ALTEREI INCLUÍ DO MY_PAYMENTS FUNÇÃO COMENTADA ABAIXO*/
                       $data['hover'] = $this->front_model->hover();
			$this->load->view('front/resgate',$data);
		}
		$this->security->cookie_handlers();
	}
	
	function loja_nao_avisou_compra($action=null)
	{
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id=="")
		{
			$this->load->view('front/index');
		}	 
		else
		{
			$missing_cashback = $this->front_model->missing_cashback($user_id);
			$data['results'] = $missing_cashback;
                        $data['hover'] = $this->front_model->hover();
			$this->load->view('front/loja-nao-avisou-compra',$data);
			/*switch($action)
			{
				case "add":
				{
					echo "Add cashback Goes here";
					exit;
				}
				case "edit":
				{
					echo "sample";
					exit;
				}
				case "delete":
				{
					echo "sample";
					exit;
				}
				default:
				{
					$missing_cashback = $this->front_model->missing_cashback($user_id);
					$data['results'] = $missing_cashback;
					$this->load->view('front/loja_nao_avisou_compra',$data);
				}
			}*/
		}
		$this->security->cookie_handlers();
	}
	
	function resgatar_compra_nao_avisada($action=null)
	{
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id=="")
		{
			$this->load->view('front/index');
		}	 
		else
		{
					$user_detail 	  = $this->front_model->userdetails($user_id);
					$data['users']    = $user_detail;
					$missing_cashback = $this->front_model->missing_cashback($user_id);
					$data['results']  = $missing_cashback;
					$this->load->view('front/resgatar-compra-nao-avisada',$data);
		}
		$this->security->cookie_handlers();
	}
	
	function add_withdraw()
	{
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id==""){
			redirect('','refresh');
		} 
		else
		{
			$post_price = $this->input->post('request');
			$ifsc_code  = $this->input->post('ifsccode');
			$this->front_model->update_user_balance($user_id,$post_price,$ifsc_code);
		}
		$this->security->cookie_handlers();
	}
	
	/*function addtocart()
	{
		echo "Add to cart page goes here";
		exit;
	}*/
	
	function ofertas($categoryurl=null,$sub_categoryurl=null)
	{
		$this->input->session_helper();
		//if($categoryurl==""){
		//	redirect('','refresh');    // Need to remove this comment
		//} 
		$category_details = $this->front_model->get_category_details($categoryurl); // Category details
		if($category_details)
		{
			$category_id= $category_details->category_id;
			$category_name = $category_details->category_name;
			if($sub_categoryurl!="")
			{
				$sub_categorys = $this->front_model->sub_category_details($sub_categoryurl);
				$data['category_details'] = $sub_categorys;
				$data['main_category'] = $category_name;
			}
			else
			{
				$data['category_details'] = $category_details;
				$data['main_category'] = $category_name;
			}
			
			$stores_list = $this->front_model->get_stores_list($category_id); //get stores list
			$data['categoryurl'] = $categoryurl;
			$data['sub_categoryurl'] = $sub_categoryurl;
			$data['stores_list'] = $stores_list;
			//print_r($stores_list);
			$this->load->view('front/products',$data);
		}
		else
		{
			$data['no_records'] = '1';
			$this->load->view('front/404',$data);
		}
		 
		$this->security->cookie_handlers();
	}
	
	
	/******Nov 19 th*********/
	/******Nov 26 th*********/
	
	function old_stores($store_id)
	{
		echo "Stores Page Inprogress";
		exit;
		if($categoryurl==""){
			//redirect('cashback/index','refresh');    // Need to remove this comment
		} 
		$category_details = $this->front_model->get_category_details($categoryurl); // gategory details
		$data['category_details'] = $category_details;
		$category_id= $category_details->category_id;
		
		$stores_list = $this->front_model->get_stores_list($category_id); //get stores list
		
		
		$subcategories = $this->front_model->get_subcategories($category_id); //get all sub categories
		if($subcategories!='')
		{
			foreach($subcategories as $subcat)
			{
				$subcategory_id[] = $subcat->sun_category_id;
				$sub_category_name[] = $subcat->sub_category_name;
			}
			$sub_category_name[] = $category_details->category_name;			
		}
		$sub_category_name[] = $category_details->category_name; 
		
		print_r($coupons_list);
		$this->security->cookie_handlers();
	}
	function add_rating($id,$rating)
	{ 
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id==""){
			redirect('','refresh');
		} else
		{
			if($this->input->post('id')){
	
				$updated = $this->front_model->add_rating();
				 
			}
		}
		$cookiehandlers = $this->security->cookie_handlers();
	}
	
	
	function logincheck(){
		$this->input->session_helper();
		
		
		if($this->input->post('signin')) {


				$result = $this->front_model->login();
				if($result==0)
				{
					echo "Invalid username or password";
					
				} 
				else if($result==2)
				{
					echo "Your account is de-activated.";
				}
				else{
					echo 1;
					//redirect('minha-conta','refresh');
				}
			}
			else
			{
			redirect('login','refresh');
			}
			$this->security->cookie_handlers();
	}
	/******Nov 26 th*********/
	
	/************ Dec 1 st *************/
	function verify_account($verify_code)
	{
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id!="")
		{
			redirect('minha-conta','refresh');
		}
		
		$verify = $this->front_model->verify_account($verify_code);
		if($verify==1)
		{
			$data['verify_msg'] = $verify;
		}
		else
		{
			$data['verify_msg'] = $verify;
		}
		
		$this->load->view('front/verify-account',$data);	
		$this->security->cookie_handlers();
	}

	//New code for Un_subscribe Regiastration email notification//

	function un_subscribe_signup($verify_code)
	{
		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id!="")
		{
			redirect('','refresh');
		}
		
		$verify = $this->front_model->un_subscribe_signup($verify_code);
		if($verify==1)
		{
			$data['verify_msg'] = $verify;
		}
		else
		{
			$data['verify_msg'] = $verify;
		}
		
		$this->load->view('front/un-subscribe-signup',$data);	
		$this->security->cookie_handlers();
	}

	function un_subscribe($type,$userid)
	{	
		$admindetails 		  = $this->front_model->getadmindetails();
		$data['admindetails'] = $admindetails;

		if(($type!='') && ($userid!=''))
		{
			$verify = $this->front_model->un_subscribe($type,$userid);
			
			if($verify==1)
			{
				$data['verify_msg'] = $verify;
			}
			else
			{
				$data['verify_msg'] = $verify;
			}

			$this->session->set_flashdata('success', 'Un Subscribers request successfully Completed.');
			redirect('thankyou','refresh');
		}

		$this->load->view('front/un-subscribe',$data);	
		$this->security->cookie_handlers();
	}


	//End//
	
	function visit_shop($storeid=null,$couponid=null)
	{
		 
		$admindetails = $this->front_model->getadmindetails();
		$data['admindetails'] = $admindetails;
		$store_details = $this->front_model->get_store_details_byid($storeid);
		//echo "<pre>";print_r($store_details);
		///exit;
		if($store_details=='')
		{//echo "1";exit;
				redirect('','refresh');
		}
		$data['store_details'] = $store_details;
		$store_name = $store_details->affiliate_name;		
		$coupon = $this->front_model->get_coupons_from_coupon_byid($couponid);
		//if($coupon=='')
		//{//echo "2";exit;
		//		redirect('index','refresh');
		//}
	//echo "3";exit;
		$user_id = $this->session->userdata('user_id');
		$click_history = $this->front_model->click_history($storeid,$couponid,$user_id);
		$data['user_id'] = $user_id;
		$data['coupon'] = $coupon;
		
		$this->load->view('front/visit-shop',$data);
	}
	
	//visit Shop name change into ir_loja //
	/*New changes added 13-5-16. */
	function ir_loja($storeid=null,$couponid=null,$name)
	{	
		
		$admindetails 		   = $this->front_model->getadmindetails();
		$data['admindetails']  = $admindetails;
		$store_details 		   = $this->front_model->get_store_details_byid($storeid);
		
		$data['store_details'] = $store_details;
		$store_name 		   = $store_details->affiliate_name;
		//$_SESSION['activate']  = $storeid;
		
		// extension anand
		
		$_SESSION['activate'.$storeid]  = 'couponsactivated';
		
		$user_id  		 	   = $this->session->userdata('user_id');

		/*New changes 13-5-16.*/
		if($name!='')
		{
			$coupon 		 = $this->front_model->get_coupons_from_shoppingcoupon_byid($couponid);
			$click_history   = $this->front_model->newclick_history($storeid,$couponid,$user_id);
				
		}else{
			$coupon 		 = $this->front_model->get_coupons_from_coupon_byid($couponid);
			$click_history   = $this->front_model->click_history($storeid,$couponid,$user_id);
		}	

		$data['user_id'] = $user_id;
		$data['coupon']  = $coupon;
		$data['name']    = $name;
		$this->load->view('front/visit-shop',$data);
	}	
	//End//
	
	/************ Dec 1 st *************/
	/************ Dec 8th *************/
	
	function ajaxcall()
	{
		$getdate_old = $this->input->post('date');
		$getdate = date("Y-m-d", strtotime($getdate_old));
		$user_id = $this->session->userdata('user_id');
		$get_details = $this->front_model->get_details_ajax($getdate,$user_id);
		//print_r($get_details); exit;
		$this->output->set_header('Content-Type: application/json; charset=utf-8');
		echo json_encode($get_details);
	}
	
	function get_clicked_store_details()
	{
		$this->input->session_helper();
		$getdate_old = $this->input->post('date');
		$click_store = $this->input->post('click_store');
		$getdate 	 = date("Y/m/d", strtotime($getdate_old));
		$user_id 	 = $this->session->userdata('user_id');
		$get_user_click_details = $this->front_model->get_clicked_details_ajax($getdate, $click_store, $user_id);
		if($get_user_click_details)
		{
			foreach($get_user_click_details as $clicks)
			{
				$get_exp = $this->front_model->get_clicked_expirycheck_ajax($clicks->click_id);
				$getclickdate = date("d F Y h:i A", strtotime($clicks->date_added));
				
				?>
                        <tr>
                            <td><?php echo $getclickdate;?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $clicks->voucher_name;?></td>
                            <?php
							if($get_exp)
							{
								switch($get_exp->status)
								{
									case 0:
										$s =  'Completed';
									?>
                                    <?php
									break;
									case 1:
									$s =  'Cancelled';
									?>
                                   
                                    <?php
									break;
									case 2:
									$s =  'Sent to retailer';
									
									break;
									case 3:
									$s =  'Created';
									break;
									
								}
								?>
								 <td><a style="color: green; font-weight: bold;" href="javascript:void(0)"><?php echo $s;?></a></td>
                                 <?php
							}
							else
							{  
							?>
                            <td><a style="color: green; font-weight: bold;" href="javascript:void(0)" onclick="final_step('<?php echo $clicks->click_id?>');">Click to raise ticket</a></td>
                            <?php
							}
							?>
                        </tr>
				<?php
			}
		}
		else
		{
			echo "<tr><td align='center' colspan='6'>No Records Found</td></tr>";
		}
	}
	
	/************ Dec 8th *************/
	/************ Dec 9th *************/
	function missing_Cashback_submit()
	{	
	//echo "<pre>"; print_r($_POST); exit;	
	$this->input->session_helper();
	$admin_id = $this->session->userdata('user_id');
		if($admin_id==""){
			redirect('','refresh');
		} else
		{		
			if($this->input->post('save'))
			{	
				$flag=0;
				$ticket_attachment = $this->input->post('ticket_attachment');
				$banner_image      = $_FILES['ticket_attachment']['name'];
				if($banner_image!="")
				{
					$new_random = mt_rand(0,99999);
					$banner_image = $_FILES['ticket_attachment']['name'];
					$banner_image = remove_space($new_random.$banner_image);
					$config['upload_path'] ='uploads/missing_cashback';
					$config['allowed_types'] = '*';
					$config['file_name']=$banner_image;
						
					//print_r($config);
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
						
					if($banner_image!="" && (!$this->upload->do_upload('ticket_attachment')))
					{
						$banner_imageerror = $this->upload->display_errors();
					}
						
					if(isset($banner_imageerror))
					{
						$flag=1;
						$this->session->set_flashdata('banner_name',$ticket_attachment);
						$this->session->set_flashdata('error',$banner_imageerror);
						redirect('resgatar-compra-nao-avisada','refresh');
					}
				}
				$phone_no	   = $this->input->post('phone_no');
				$user_id 	   = $this->input->post('user_id');
				$userdetails   = $this->front_model->userdetails($user_id);
				$contact_no    = $userdetails->contact_no;
				//$celular_no    = $userdetails->celular_no;
				
				if($contact_no == '')
				{
					$data = array(		
					'contact_no' => $phone_no);
					$this->db->where('user_id',$user_id);
					$update_qry = $this->db->update('tbl_users',$data);
				}

				//print_r($userdetails);exit;

				if($flag==0)
				{
					$results = $this->front_model->missing_Cashback_submit_mod($banner_image);
					if($results)
					{
						$this->session->set_flashdata('success', 'Pronto, pedido enviado com sucesso. Agora a gente vai entrar em contato com a loja para resolver isso. Você pode acompanhar o andamento nessa página.');
						redirect('loja-nao-avisou-compra','refresh');
					}
					else
					{
						$this->session->set_flashdata('error', 'Ôpa, tivemos um erro inesperado e não foi possivel incluir seu registro. Tente novamente ou entre em contato conosco.');
						redirect('loja-nao-avisou-compra','refresh');
					}
				}
			}
			else
			{
				redirect('resgatar_compra_nao_avisada','refresh');
			}
		}
		
	
	}
	/************ Dec 9th *************/
	
	/************ Dec 12th *************/
	
	/*Rename a top-cashback to cupom 20-9-16*/

	function cupom($categoryurl=null,$sub_categoryurl=null)
	{		
		$this->input->session_helper();
		if($categoryurl=="")
		{
			
			$stores_list = $this->front_model->get_top_cashback_stores_list(); //get stores list
			$data['stores_list'] = $stores_list;
			$this->load->view('front/top-cashback-all',$data);
		} 
		else
		{
			$category_details = $this->front_model->get_category_details($categoryurl); // Category details
			if($category_details)
			{
				$category_id= $category_details->category_id;
				$category_name = $category_details->category_name;
				if($sub_categoryurl!="")
				{
					$sub_categorys = $this->front_model->sub_category_details($sub_categoryurl);
					$data['category_details'] = $sub_categorys;
					$data['main_category'] = $category_name;
				}
				else
				{
					$data['category_details'] = $category_details;
					$data['main_category'] = $category_name;
				}
				
				$stores_list = $this->front_model->get_top_cashback_stores_list_cate($category_id); //get stores list
				$data['stores_list'] = $stores_list;
				$this->load->view('front/top-cashback',$data);
			}
			else
			{
				$data['no_records'] = '1';
				$this->load->view('front/404',$data);
			}
		}
		
	}



	function top_cashback($categoryurl=null,$sub_categoryurl=null)
	{		
		$this->input->session_helper();
		if($categoryurl=="")
		{
			
			$stores_list = $this->front_model->get_top_cashback_stores_list(); //get stores list
			$data['stores_list'] = $stores_list;
			$this->load->view('front/top-cashback-all',$data);
		} 
		else
		{
			$category_details = $this->front_model->get_category_details($categoryurl); // Category details
			if($category_details)
			{
				$category_id= $category_details->category_id;
				$category_name = $category_details->category_name;
				if($sub_categoryurl!="")
				{
					$sub_categorys = $this->front_model->sub_category_details($sub_categoryurl);
					$data['category_details'] = $sub_categorys;
					$data['main_category'] = $category_name;
				}
				else
				{
					$data['category_details'] = $category_details;
					$data['main_category'] = $category_name;
				}
				
				$stores_list = $this->front_model->get_top_cashback_stores_list_cate($category_id); //get stores list
				$data['stores_list'] = $stores_list;
				$this->load->view('front/top-cashback',$data);
			}
			else
			{
				$data['no_records'] = '1';
				$this->load->view('front/no-products',$data);
			}
		}
		
	}
	
	
	/************ Dec 12th *************/
	
	/************ Dec 13th *************/
	function blog()
	{		
		$blog_details = $this->front_model->get_blog_details();
		$data['blog_details'] = $blog_details; 
			$this->load->view('front/blog',$data);
	
	}
	
	function blog_details($blog_id=null)
	{
		$this->input->session_helper();
		if($blog_id=="")
		{
			redirect('','refresh');			
		}
		
		$blog_details = $this->front_model->blog_details($blog_id);
		$block_comments = $this->front_model->blog_comments($blog_id);
		$data['block_comments'] = $block_comments; 
			$data['blog_details'] = $blog_details; 
			
			$this->load->view('front/blog_details',$data);
	}
	
	function blog_comment()
	{
		if($this->input->post('blog_commentregister'))
		{
			$blog_id = $this->input->post('blog_id');
			$ins_qry = $this->front_model->post_comments();
		   if($ins_qry)
			{	
				$this->session->set_flashdata('success','Comments added successfully.');
				redirect('blog_details/'.$blog_id,'refresh');
			}
			else
			{
					$this->session->set_flashdata('error','Errors occurs while adding Comments.');
					redirect('blog_details/'.$blog_id,'refresh');
			} 
		} 
	}
	
	
	
	
	/************ Dec 13th *************/
	

/*********************Nathan End*************************/	

/*********************Nathan End*************************/	  

 
 // 5/12/2014  renuka 
 
function produtos()
{	
 	$this->input->session_helper();
	$admindetails = $this->front_model->getadmindetails();
	$data['admindetails'] = $admindetails;
	$data['result'] = $this->front_model->home_slider();     

	/*New code fro hide old function details 17-1-17*/
    $urisegment = $this->uri->segment(3);
	$perpage    = $this->front_model->getrowsperpage();         
	//$result     = $this->front_model->getall_premiumcoupons($perpage,$urisegment); 
	/*end 17-1-17*/
	$date 	  = date('Y-m-d');
	$result   = $this->db->query("SELECT * from shopping_coupons where expiry_date > '$date'")->result();
	$data['featured_product']=$this->front_model->getall_fetured_products();
	//echo "<pre>";print_r($result); exit; 
	if(!$result)  
	{			
		$data['view']="view";  
		$data['result']=$result;   
		$data['segment']="shopping";
		$data['notfound']="No Coupons are found at this time";  
		$this->load->view('front/produtos',$data);   
	}	  
	else     
	{	 
		
		$total_rows = $this->front_model->getall_premiumcouponscount(); 
 		/*New code 9-5-16*/
 		$base="produtos";  
 		/*End code 9-5-16*/           
		$data['view']="view"; 
		$data['segment']="shopping"; 
		$this->pageconfig($total_rows,$base);          
		$data['result']=$this->front_model->getall_premiumcoupons($perpage,$urisegment); 		
		/*new code 17-1-17*/
		$result   	    = $this->db->query("SELECT * from shopping_coupons where expiry_date > '$date'")->result();
 		$data['result'] = $result;			
		/*end 17-1-17*/
		$this->load->view('front/produtos',$data);      
	}  	
}

/*New code for new page promocao 1-10-16*/
function promocao($store_name)
{	
 	$this->input->session_helper();
 	if($store_name)
 	{
	 	$squery   	  = $this->db->query("select affiliate_id from affiliates where affiliate_url='$store_name'")->row();
		$affiliate_id = $squery->affiliate_id;
	 	$pcoupons  	  = $this->db->query("select * from shopping_coupons where store_name='$affiliate_id'")->result();
	 	//echo "<pre>";print_r($pcoupons); exit;
	 	if($pcoupons)
	 	{
	 		$admindetails 		  = $this->front_model->getadmindetails();
			$data['admindetails'] = $admindetails;
			//$data['result'] 	  = $this->front_model->home_slider(); 
			//$premiumcoupons 	  = $this->front_model->get_premium_cat_coupon($category_id);
			///print_r($premiumcoupons);			 
		 	/*New code 9-5-16*/
		 	$base="produtos";  
		 	/*End code 9-5-16*/           
			$data['view']="view"; 
			$data['segment']="shopping"; 
			$this->pageconfig($total_rows,$base);          
		 	$data['result']=$pcoupons;  					
			$this->load->view('front/promocao',$data);
	 	}
	 	else
	 	{
	 		$this->load->view('front/404');
	 	}
	}
	else
	{
		$this->load->view('front/404');
	}	
 	
}
/*end*/

function ajaxsess_setpremiumcategory() 
{
    //print_r($POST); exit;
    /*if($this->input->post('catid')!=0)
	{*/
	$this->session->set_userdata("sess_cashback_premiumcatid",$this->input->post('catid'));   
	/*}*/
	//echo "sasa";
	//print_r($this->input->post())
	$this->session->set_userdata("sess_cashback_starting_price",$this->input->post('starting_price'));   
	$this->session->set_userdata("sess_cashback_end_price",$this->input->post('end_price'));   
	$this->session->set_userdata("sess_cashback_featured",$this->input->post('featured'));   
	$this->session->set_userdata("sess_cashback_popular",$this->input->post('popular'));   
	$this->session->set_userdata("sess_cashback_new",$this->input->post('new'));   
	$this->session->set_userdata("sess_cashback_es",$this->input->post('es'));   

	$ajax=1;
	$admindetails 		  	= $this->front_model->getadmindetails();
	$data['admindetails'] 	= $admindetails;
	$data['result'] 		= $this->front_model->home_slider();     
	$urisegment				= $this->uri->segment(3);
	$perpage 				= $this->front_model->getrowsperpage();         
	$result 				= $this->front_model->getall_premiumcoupons($perpage,$urisegment,$ajax); 
	//$result 		 	    = $this->db->query("select * from shopping_coupons where store_name='$affiliate_id'")->result();
	if(!$result)  
	{			
		$view="view";  
		$result=$result;   
		$segment="shopping";
		$notfound="No Coupons Are Found";         
	}	  
	else     
	{	 
		$total_rows = $this->front_model->getall_premiumcouponscount($ajax); 
 		/*New code 9-5-16*/
 		$base="produtos";  
 		/*End code 9-5-16*/           
		$view="view"; 
		$segment="shopping"; 
		$this->pageconfig($total_rows,$base);          
 		$result=$this->front_model->getall_premiumcoupons($perpage,$urisegment,$ajax);  					
	}  	
	if($result!="0")
	{ 
		?>
        <div class="row row-wrap"> 
			<?php 
			foreach($result as $fetrest)
			{ 
				    	$shoppingcoupon_id      = $fetrest->shoppingcoupon_id;
				        $db_offer_name 		    = $fetrest->offer_name;
				        $db_coupon_description  = $fetrest->description;
				        $db_coupon_image 		= $fetrest->coupon_image;
				        $db_expiry_date 		= $fetrest->expiry_date;
				        $db_cp_price	    	= $fetrest->amount;
						$exp_db_coupon_image	= explode(",",$db_coupon_image);  
						$f_dbcouponfirst_img	= $exp_db_coupon_image[0];
						$len_db_offer_name	 	= strlen($db_offer_name);  
						$len_db_coupon_desc		= strlen($db_coupon_description);  
						
						$total_price    	 	= $fetrest->price; 
                        $sales_price    	 	= $fetrest->amount;
                        /*New code for precentage details 21-7-16*/
                        $sales_price         	= ($total_price - $sales_price);
                        $discount_percentage 	= (($sales_price/$total_price)*100);
                        $discount_percentage 	= preg_replace('/\./', ',',number_format(round($discount_percentage ,2),2));
                        /*end*/

						if($len_db_offer_name>=10)
						{
							$f_dbcp_name=substr($db_offer_name,0,9)."..."; 
						}
						else
						{
							$f_dbcp_name=$db_offer_name; 
						}
					    if($len_db_coupon_desc>=54)
						{
						    $f_dbcp_desc=substr($db_coupon_description,0,54)."..."; 
						}
						else
						{
						    $f_dbcp_desc=$db_coupon_description; 
						} 
						$getremain_days=$this->front_model->find_remainingdays($db_expiry_date);  
						// print_r($getremain_days); 
						// echo "$days days $hours hours remain<br />";  
			  		    ?>  
				   		<!-- New changes url 10-5-16 -->
                        <!-- <a class="col-md-4" href="<?php echo base_url(); ?>barato/<?php echo $fetrest->seo_url; ?>">
                            <div class="product-thumb">
                                <header class="product-header">
                                    <img width="235" height="195" src="<?php echo base_url(); ?>uploads/premium/<?php echo $f_dbcouponfirst_img; ?>" alt="Image Alternative text" title="Hot mixer" />
                                </header>      
                                <div class="product-inner">
                                    <h5 class="product-title"><?php  echo $f_dbcp_name;  ?></h5>
                                    <p class="product-desciption"><?php echo $f_dbcp_desc;  ?> </p> 
                                    <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> <?php  echo $getremain_days['days']." days ".$getremain_days['hours']." h "." remaining";   ?> </span>   
                                        <ul class="product-price-list">
                                            <li><span class="btn btn-blue"><?php echo "Rs ".$db_cp_price; ?></span> 
                                            </li>
                                           <?php /*echo '<li><span class="product-old-price">$141</span>
                                            </li>
                                            <li><span class="product-save">Save 63%</span>
                                            </li>';*/ ?>
                                        </ul>
                                    </div>
                                    <?php //echo '<p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>';?>
                                </div>
                            </div> 
                        </a> -->
                        <!-- New design changes code 19-7-16 -->
                        <!-- <a href="<?php echo base_url(); ?>baratonew/<?php echo urlencode($fetrest->seo_url); ?>"> -->
                                   
                        			<!-- new code for category name included in url 30-9-16 -->
                                    <?php 
                                    $category_ids  = $fetrest->category;
                                    $category_name = $this->db->query("select * from premium_categories where category_id=$category_ids")->row('category_url'); 
                                    ?>
                                    <!-- End 30-9-16 -->


                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                    	<div class="prod-photodetblk">
                                        <?php 
                                            $image_type          = $fetrest->img_type;
                                            $db_coupon_image     = $fetrest->coupon_image;
                                            if($image_type == 'url_image')
                                            {
                                                ?>
                                                <div class="photo">
                                                    <img alt="a" class="img-responsive" style="height:292px; width:264px" src="<?php echo $db_coupon_image; ?>" alt="Image Alternative text" title="Hot mixer">
                                                </div>
                                            <?php
                                            }
                                            else
                                            {
                                                ?>
                                                <div class="photo">
                                                    <img alt="a" class="img-responsive" style="height:292px; width:264px" src="<?php echo $this->front_model->get_img_url(); ?>uploads/premium/<?php echo $db_coupon_image; ?>" alt="Image Alternative text" title="Hot mixer">
                                                </div>
                                                <?php 
                                            } 
                                            ?>
                                        <div class="info">
                                            <div class="row vers">
                                                <center>
                                                    <h4>
                                                        <a href="<?php echo base_url(); ?>barato/<?php echo $category_name;?>/<?php echo urlencode($fetrest->seo_url); ?>">
                                                            <span class="cls_sub_text">
                                                                <?php echo $f_dbcp_name; ?>
                                                                <br>
                                                                R$ <?php echo $db_cp_price; ?>, e receba <?php echo $discount_percentage; ?>% de volta
                                                            </span>
                                                        </a>
                                                    </h4>
                                                </center>
                                            </div>
                                            <div class="overlay-prod ">
                                                <div class="row vers hov">
                                                    <center>
                                                        <h4 class="">
                                                            <a href="<?php echo base_url(); ?>barato/<?php echo $category_name;?>/<?php echo urlencode($fetrest->seo_url); ?>"><?php  echo $f_dbcp_name;  ?></a>
                                                        </h4>
                                                        <p>Pague R$ <?php echo $db_cp_price; ?>, e receba <br> <?php echo $discount_percentage; ?>% de volta</p>
                                                        <span><?php  echo $getremain_days['days']." days ".$getremain_days['hours']." h "." remaining";   ?></span>
                                                        <a href="<?php echo base_url(); ?>barato/<?php echo $category_name;?>/<?php echo urlencode($fetrest->seo_url); ?>">
                                                        <button type="button" class="btn btn-blu">Details</button>
                                                        </a>
                                                    </center>
                                                </div>
                                            </div>
                                        </div>
                                    	</div>
                                	</div>
                                <!-- </a> -->
                        <!-- End -->


                        <?php 
			}
			?>
        </div>
		<?php 
	}
	else
	{
		?> 
		<div class="alert alert-error">
			<button class="close" data-dismiss="alert">x</button>
			<strong>Oops! </strong>
			<?php echo $notfound;  ?>
		</div>
		<?php 
	}
	if($result!=0)  echo $this->pagination->create_links();     
}  


function pageconfig($total_rows,$base) 
{	 
	
	$this->input->session_helper();
	$perpage = $this->front_model->getrowsperpage();
	$urisegment=$this->uri->segment(3);
	$this->load->library('pagination');
	$config['base_url'] = base_url().'/cashback/'.$base.'/';    
	$config['total_rows'] = $total_rows;   
	$config['per_page'] = $perpage;
	$config['num_links']=  2; // Number of "digit" links to show before/after the currently viewed page
	$config['full_tag_open'] = '';
	$config['full_tag_close'] = '';
	$config['cur_tag_open'] = '<span class="this-page">&nbsp; <b>';
	$config['cur_tag_close'] = '&nbsp;</b></span>'; 
	$config['first_link'] = 'First';
	$config['last_link'] = 'Last';
	$config['prev_link'] = '&laquo;';  
	$config['next_link'] = '&raquo';
	$config['last_tag_open'] = '';
	$config['last_tag_close'] = '';
	$config['next_tag_open']= '&nbsp;';
	$config['next_tag_close']= '&nbsp;';
	$config['prev_tag_open']= '&nbsp;'; 
	$this->pagination->initialize($config); 
	$data['pagination']=$this->pagination->create_links();
	  
}
/*New changes 11-5-16*/	  
function barato($cat_name,$id,$details=null)
{
	$ids = urldecode($id);
	$this->input->session_helper();
	//echo $cat_name; echo "<br>"; 
	if($cat_name !='' && $id == '' && $details == '') //
	{
		 
		$location 	  = $this->session->userdata('cityname');
	   	$squery   	  = $this->db->query("select * from premium_categories where category_url='$cat_name'")->row();
	  	$category_id  = $squery->category_id;
		if($category_id)
		{
			$cat_details = $this->front_model->get_premium_cat_coupon($category_id);
			$data['cat_details'] = $cat_details;
			//echo "<pre>"; print_r($cat_details); exit;
			$this->load->view('front/produtos',$data);
		}
		else
		{
			$this->load->view('front/404',$data);
		}
	}
	else
	{
		 
		$shop_coupons = $this->db->query("select * from shopping_coupons where seo_url='$id'")->row();
		$shop_id      = $shop_coupons->shoppingcoupon_id;
		if($shop_id == '')
		{
			$this->load->view('front/404',$data);
		}
		else
		{ 
			$admindetails 				= $this->front_model->getadmindetails();
			$data['admindetails'] 	  	= $admindetails;
			$data['details']			= $details = $this->front_model->details($ids);  
			$data['related_products']	= $this->front_model->related_products($details->category,$ids);  //new//
			$data['popular']			= $this->front_model->popular_products();  	
			$data['recently_viewd']		= $this->front_model->recently_viewed();  
			$data['recent_reviews'] 	= $this->front_model->recent_reviews($details->shoppingcoupon_id);  	
			$data['reviews']			= $this->front_model->reviews($details->shoppingcoupon_id);  
			$data['avg_rating']			= $this->front_model->avg_rating($details->shoppingcoupon_id);  	
			$this->load->view('front/barato',$data);
		}
	}	
}
		function submit_ratings()
		{
			$inserty = $this->front_model->insert_comments();
			?>
			 <li> 
							 
							  <article class="comment">
								<div class="comment-author"> <img  alt="" src="images/review-img.png"> </div>
								<div class="comment-inner">
								  <ul title="4/5 rating" class="icon-group icon-list-rating comment-review-rate">
								  <?php for($i=0;$i< $this->input->post('rating');$i++) { ?>
									<li><i class="fa fa-star"></i> </li>
								  <?php } ?>
								  </ul>
								  <span class="comment-author-name"><?php echo $this->session->userdata('user_email') ?></span>
								  <p class="comment-content"> <?php echo $this->input->post('comments') ?></p>
								</div>
							  </article>
							</li>
			<?php
		}


		function addtocart()
		{
			echo $inserty = $this->front_model->insert_coupon();
		}
		function cart_listing_page()
		{
			$this->input->session_helper();
			/*$this->session->set_userdata("user_id",'1');
			$this->session->set_userdata("user_email","saravanan@osiztechnologies.com"); */  	
			$admindetails = $this->front_model->getadmindetails();
			$data['admindetails'] = $admindetails;
			$data['cart']= $this->front_model->getuser_cart();
			$this->load->view('front/cartpage',$data);
		}
		function delete_cart()
		{
			$this->input->session_helper();
			  $query = $this->db->where('id', $this->input->post('id'));
			  $query = $this->db->delete('premium_cart');
			  
			  echo 1;
			  exit;
		}
		function check_amount()
		{
		echo	$admindetails = $this->front_model->check_amount();
		}
		function payusing_site()
		{
			echo $cal=$this->front_model->cal_amount();
		}
		function thankyou()
		{
			$this->input->session_helper();
			$admindetails = $this->front_model->getadmindetails();
			$data['admindetails'] = $admindetails;
		
			$this->load->view('front/thankyou',$data);
		}
		function PayUmoney()
		{
			echo "<pre>";
			//print_r($this->input->post());exit;
			$data['sub_tot'] =$this->input->post('sub_tot');
			$this->input->session_helper();
			$admindetails = $this->front_model->getadmindetails();
			$data['admindetails'] = $admindetails;
			$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
			$data['hash_new'] = $txnid;
			$this->session->set_userdata('sub_tot',$this->input->post('sub_tot'));
			$this->session->set_userdata('hash_new',$txnid);
			$id_id = $this->front_model->insert_umoney_order($txnid);
			$this->load->view('front/PayUmoney',$data);
		}
		function Payment_Success()
		{
		
			
			$this->input->session_helper();
			//print_r($_REQUEST);
			$mihpayid=$_REQUEST['mihpayid'];
			$TxnID=$_REQUEST['txnid'];
			$useragent = $_SERVER['HTTP_USER_AGENT'];
			$ipaddress =$_SERVER['REMOTE_ADDR']; 
			$old_id = $_REQUEST['udf1'];
			/*seetha 19.8.2015*/
			$admindetails = $this->front_model->update_pay_u_money($mihpayid,$TxnID,$useragent,$ipaddress,$old_id);
			redirect('thankyou','refresh');
		}
		function Payment_Failure()
		{
			echo 'Error try again';
			exit;
		}
		function orders()
		{
			$this->input->session_helper();
				$admindetails = $this->front_model->getadmindetails();
			   $data['admindetails']= $admindetails;
			   $data['result']= $this->front_model->get_orders();
			$this->load->view('front/orders',$data);
		}
		
		function extrato()
		{
			$this->input->session_helper();
			$user_id = $this->session->userdata('user_id');
			if($user_id=="")
			{
				$this->load->view('front/index');
				//redirect('','refresh');
			}
			else
			{
				if($this->input->post('submit')) 
				{
					 
					$transations = $this->front_model->transations_bydate($user_id);			
                                        $res = 	$this->front_model->getcashbacks_bydate($user_id);	
				}
				else
				{
					$transations= $this->front_model->gettransations($user_id);	
                                         $res = $this->front_model->getcashbacks($user_id);
				}
				$data['hover'] = $this->front_model->hover();
				 $data['result']= $transations;
                                 $data['result1']=$res;
				$this->load->view('front/extrato',$data);
			}
		}

		//New code for user History page// 

		function saidas_para_loja()
		{
			$this->input->session_helper();
			$user_id = $this->session->userdata('user_id');
			if($user_id=="")
			{
				$this->load->view('front/index');
			}
			else
			{ 	
				if($this->input->post('submit')) 
				{
					 
					$history = $this->front_model->clickhistory_bydate($user_id);			
				}
				else
				{
					$history= $this->front_model->clickhistory($user_id);
				}
				 
						
               
				 $data['result']= $history;
                                 $data['result1']=$res;
				$this->load->view('front/clickhistory',$data);
			 }
		}

		//End//

		/*function coupons(){
		$this->input->session_helper();
		$data['couponslist'] = $this->front_model->get_shopping_coupons();
		$data['categories'] = $this->front_model->category();
		$this->load->view('front/coupons',$data);
		}*/

		function store_ajax($pagenum,$store_name)
		{
			
			$this->input->session_helper();
			$store_details = $this->front_model->get_store_details($store_name);
			if($store_details)
			{
			$data['store_details'] = $store_details;
			$store_name = $store_details->affiliate_name;
			$store_coupons = $this->front_model->store_ajax($pagenum,$store_name);
			if($store_coupons)	
			{
				$data['store_coupons'] = $store_coupons;
				$data['pagenum'] = $pagenum;
				$user_id = $this->session->userdata('user_id');
				$data['user_id'] = $user_id;
				$this->load->view('front/stores-ajax',$data);
			}
			else
			{
				echo 0;
			}
			}
						
		}
		
		function password_reset($resetpass=null)
		{
				//print_r($_POST);	
				$this->input->session_helper();		
				$reset_id = insep_decode($resetpass);
				if($this->input->post('Save'))
				{	
						//print_r($_POST);exit;
						$user_id = $this->input->post('user_id');
						$res = $this->front_model->reset_password($user_id);
						if($res){
							$this->session->set_flashdata('success', 'Password updated successfully. Please <a href="'.base_url().'login">Login</a> to continue.');
							redirect('password_reset','refresh');
						}
						else
						{
							$this->session->set_flashdata('error', 'Error occurred while updating your password.');
							redirect('password_reset','refresh');
						}
				}
				else
				{
						$data['user_id'] = $reset_id;
						$this->load->view('front/password_reset',$data);
				}
		
		}

		function passwordreset()
		{
			$this->input->session_helper();		
			$reset_id = insep_decode($resetpass);
			if($this->input->post('Save'))
			{	
				//print_r($_POST);exit;
				$user_id = $this->input->post('user_id');
				$res = $this->front_model->reset_password($user_id);
				if($res)
				{
					$this->session->set_flashdata('success', 'Password updated successfully. Please <a href="#login" data-toggle="modal" id="signinclks">Sign in</a> to continue.');
					redirect('forgetpassword','refresh');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error occurred while updating your password.');
					redirect('forgetpassword','refresh');
				}
			}
		}
		
	
	function downsp()
	{
		$this->input->session_helper();
		$this->load->dbutil();		
		$backup =& $this->dbutil->backup();		
		$this->load->helper('file');
		write_file('/images/mybackup.gz', $backup);		
		$this->load->helper('download');
		force_download('mybackup.gz', $backup);
	}
	
	function getstores_listjson($query)
	{
		$this->input->session_helper();
		//$query = $this->input->post('query');
		if($query)
		{
			$stores_list = $this->front_model->get_typehead_list($query);
			//print_r($stores_list);
		}
		echo json_encode($stores_list);
	}
	
	
	function getcitys_listjson($query)
	{
		if($query)
		{
			$citys_list = $this->front_model->get_typehead_citys_list($query);
		}
		echo json_encode($citys_list);
	}
	
	function email_subscribe_shoppping(){
		$this->input->session_helper();
		$email = $this->input->post('email');
		$location = $this->input->post('location');
		$this->session->set_userdata('cityname',$location);
		$vars = $this->front_model->email_sub($email);
		$this->security->cookie_handlers();
		redirect('produtos','refresh');
	}
	
	function change_location(){
		$this->input->session_helper();
		$location = $this->input->post('location');
		$this->session->set_userdata('cityname',$location);
		$this->security->cookie_handlers();
		redirect('produtos','refresh');
	}

	//New code for search details //
	function busca($searchkey)
	{
		$this->input->session_helper();
	    if($searchkey!='')
	    {

	        $query = $this->db->query("SELECT * FROM (`affiliates`) WHERE `affiliate_status` = '1' AND `affiliate_url` like '%$searchkey%' limit 20");
	        
	        $numrows = $query->num_rows();
	        if($numrows==1)
	        {    
	            redirect('cupom-desconto/'.$searchkey);
	        }
	        else{
	            redirect('','refresh');
	        }
    	}
	}
	//End//

//New code for top cashback and categories page 29-4-16//

	function desconto_cupom()
	{
		$this->input->session_helper();
		$this->load->view('front/desconto-cupom');
	   
	} 
	function desconto($categoryurl=null)
	{
		$this->input->session_helper();
		if($categoryurl==""){
			
			$stores_list = $this->front_model->get_top_stores_list_cate();
			$data['stores_list'] = $stores_list;
			$this->load->view('front/desconto',$data);
			 
		} 
		else
		{
			//echo $categoryurl;
			$category_details = $this->front_model->get_category_details($categoryurl); // Category details
			if($category_details)
			{
				$category_id= $category_details->category_id;
				$category_name = $category_details->category_name;
				if($sub_categoryurl!="")
				{
					$sub_categorys = $this->front_model->sub_category_details($sub_categoryurl);
					$data['category_details'] = $sub_categorys;
					$data['main_category'] = $category_name;
				}
				else
				{
					$data['category_details'] = $category_details;
					$data['main_category'] = $category_name;
				}
				
				$stores_list = $this->front_model->get_stores_cashback_stores_list_cate($category_id); //get stores list
				$data['stores_list'] = $stores_list;
				//print_r($stores_list);
				$this->load->view('front/404',$data);
			}
			else
			{
				$data['no_records'] = '1';
				$this->load->view('front/404',$data);
			}
		}  
	} 
// 29-4-16 End//	
	
 /*New code for storedetails 4-5-16 */
        function storedetail()
        {
	        
	        $storename = $this->input->post('storename');
	        $this->input->session_helper();
			$user_id = $this->session->userdata('user_id');
			$store_details = $this->front_model->storedetail($storename);
			//print_r($store_details); exit;
			$this->output->set_header('Content-Type: application/json; charset=utf-8');
			echo json_encode($store_details);
        }
        /*End*/ 

        //Pilaventhiran 06/05/2016 START

    function loja_cancelou_minha_compra($action=null)
	{

		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id=="")
		{
			$this->load->view('front/index');
		}	 
		else
		{
			
					$missing_approval = $this->front_model->missing_approval($user_id);
					$data['results'] = $missing_approval;

					$this->load->view('front/loja-cancelou-minha-compra',$data);
		}
		$this->security->cookie_handlers();
	}

	function missing_approval_submit()
	{		

	$this->input->session_helper();
	$admin_id = $this->session->userdata('user_id');
		if($admin_id==""){
			redirect('','refresh');
		} else
		{		
		
			if($this->input->post('save')){

				
				$flag=0;
				$ticket_attachment = $this->input->post('ticket_attachment');
				
				 $banner_image = $_FILES['ticket_attachment']['name'];
					if($banner_image!="") {
						$new_random = mt_rand(0,99999);
						$banner_image = $_FILES['ticket_attachment']['name'];
						$banner_image = remove_space($new_random.$banner_image);
						$config['upload_path'] ='uploads/missing_cashback';
						$config['allowed_types'] = '*';
						$config['file_name']=$banner_image;
						
						//print_r($config);
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						
						if($banner_image!="" && (!$this->upload->do_upload('ticket_attachment')))
						{
							$banner_imageerror = $this->upload->display_errors();
						}
						
						if(isset($banner_imageerror))
						{
							$flag=1;
							$this->session->set_flashdata('banner_name',$ticket_attachment);
							$this->session->set_flashdata('error',$banner_imageerror);
							redirect('add-missing-approval','refresh');
						}
					}
					if($flag==0){
						$results = $this->front_model->missing_approval_submit_mod($banner_image);
						if($results){
							$this->session->set_flashdata('success', 'Pronto, pedido enviado com sucesso. Agora a gente vai entrar em contato com a loja para resolver isso. Você pode acompanhar o andamento nessa página.');
							redirect('loja-cancelou-minha-compra','refresh');
						}
						else{
							$this->session->set_flashdata('error', 'Ôpa, tivemos um erro inesperado e não foi possivel incluir seu registro. Tente novamente ou entre em contato conosco.');
							redirect('loja-cancelou-minha-compra','refresh');
						}
					}
				
			}
			else
			{
				redirect('loja-cancelou-minha-compra','refresh');
			}
		}
	
	
	}


	function add_missing_approval($cashback_id=null)
	{
		
		$Cahback_Id = base64_decode($cashback_id);
		

		$this->input->session_helper();
		$user_id = $this->session->userdata('user_id');
		if($user_id=="")
		{
			redirect('','refresh');
		}	 
		else
		{
			
					$add_missing_approval = $this->front_model->add_missing_approval($Cahback_Id);
					$data['results'] = $add_missing_approval;
					
					$this->load->view('front/add-missing-approval',$data);
		}
		$this->security->cookie_handlers();
	}
        
        //Pilaventhiran 06/05/2016 END
    
    /*New code for 404 error page 13-5-16. */
	function errorcode()
	{
		$this->input->session_helper();
		$prettyname   = $this->uri->segment(1);
		$prettyurl    = $this->db->query("SELECT * from pretty_link_details where incoming_link='$prettyname' and status=1")->row();
		$external_url = $prettyurl->external_link;
		$click_count  = $prettyurl->click_count + 1;
		$incom_link   = $prettyurl->incoming_link;
		//echo $external_url; 
		/*$results    = $this->front_model->pretty_link_details($prettyname);*/
		if($external_url)
		{
			$this->db->where('external_link', $external_url);
			$this->db->where('incoming_link',$incom_link);
			$data = array("click_count"=>$click_count);
        	$this->db->update("pretty_link_details", $data);

			redirect($external_url,'refresh');
		}
		else
		{
			$this->load->view('front/404');
		}
		//$this->load->view('front/404');
	}
	/*End*/

	/*New code for cupom-desconto function name 14-5-16. */

	function cupom_desconto($name)
	{
		
		$this->input->session_helper();
		if($name == '')
		{
			$this->load->view('front/desconto-cupom');	
		}	
		else
		{
			$selqry   = $this->db->query("SELECT * from affiliates where affiliate_url='$name'")->row(); 
			$aff_name = $selqry->affiliate_url;
			
			$category_details = $this->front_model->get_category_details($name); // Category details
			$category_name 	  = $category_details->category_name;
			$category_url 	  = $category_details->category_url;
			$category_id 	  = $category_details->category_id;
			
			/*New code 25-5-16.*/
			//echo $selqry->store_categorys; 
			$store_categories = explode(",",$selqry->store_categorys);
			
			foreach($store_categories  as $newstore_categories)
			{
				//echo "hai".$newstore_categories; 
				$rel_store_detail 	  = $this->front_model->cat_details($newstore_categories); 
				$stores_list 	  	  = $this->front_model->get_category_details_byid($newstore_categories);
				$category_name    	  = $stores_list->category_url;
				$data['cat_name_url'] = $category_name;
				$data['cat_name'] 	  = $stores_list->category_name;


				foreach ($rel_store_detail as $newrel_store_detail)
				{
				 	$store_categories 	     = $newrel_store_detail->ref_affiliate_categorys;
				 	$store_count 			 = $newrel_store_detail->ref_affiliate_display_count;
				 	$data['rel_store_detail']= $store_categories;
				 	$data['rel_storescount'] 	 = $store_count;	

				}
				
			}
			/*End*/
		  
			if($name == $aff_name)
			{	
				$this->load->library('minify');
				$aff_id     = $selqry->affiliate_id;
				$store_url  = $this->front_model->seoUrl($name);
				
				//related store details//
				$rel_stores = $this->front_model->get_related_stores($aff_id); 
				if($rel_stores)
				{	
					$split_categories      = explode(",",$rel_stores->ref_affiliate_categorys); 
					
					foreach($split_categories  as $splitcategories)
					{
						//echo $aff_id; echo "<br>"; echo $splitcategories;  
						if($aff_id == $splitcategories)
						{
							
							$store_categories 	     = $rel_stores->ref_affiliate_categorys;
							$store_count 			 = $rel_stores->ref_affiliate_display_count;
							$data['storecategories'] = $store_categories;
							$data['storescount'] 	 = $store_count;
						}
					}			
				}
				
				//print_r($_POST);

				$all_store 			   = $this->front_model->all_store_details();
				$data['storedetails']  = $all_store;	

				$store_details = $this->front_model->get_store_details($name);
				if($store_details)
				{
					
					$data['store_details']  = $store_details;
					$store_name 		    = $store_details->affiliate_name;	
					$store_coupons 		    = $this->front_model->get_coupons_from_store($store_name,20);
					$expiry_coupons 		= $this->front_model->expiry_coupons_from_store($store_name,20);
					$data['store_coupons']  = $store_coupons;
					$data['expiry_coupons'] = $expiry_coupons;
					$user_id 			    = $this->session->userdata('user_id');
					$data['user_id'] 	    = $user_id;
					//$related_details 		= $store_details->related_details;
					//$data['related_details']= $related_details;

					/*New code for meta property related details 11-11-16*/
					$admindetails 		 = $this->front_model->getadmindetails();
					$image_name 		 = base_url().'uploads/adminpro/'.$admindetails[0]->storepage_meta_image;
					//$data['page_title']  = 'Pingou! Cupom desconto';
					$data['page_image']  = $image_name;
					//$data ['page_desc']  = 'sadf sadfsd sadfsdf';
					/*End 11-11-16*/

					$this->load->view('front/cupom-desconto',$data);
				}
				else
				{
					$data['no_records'] = '1';
					$this->load->view('front/404',$data);
				}
			}

			else if($name == $category_url)
			{
				if($sub_categoryurl!="")
				{
					//echo "hai"; exit;
					$sub_categorys = $this->front_model->sub_category_details($sub_categoryurl);
					$data['category_details'] = $sub_categorys;
					$data['main_category'] = $category_name;
				}
				else
				{
					//echo "hai1"; exit;
					$data['category_details'] = $category_details;
					$data['main_category'] = $category_name;
					//echo "hai".print_r($category_details);
				}
				
				$stores_list = $this->front_model->get_stores_cashback_stores_list_cate($category_id); //get stores list
				$data['stores_list'] = $stores_list;
				//print_r($stores_list);
				$this->load->view('front/desconto',$data);
			}
			else
			{
				$this->load->view('front/404',$data);
			}	
		}
	}

	/*End*/
	
	/*New code for new design howit_works page 1-7-16.*/
	function como_funciona()
	{
		$this->security->cookie_handlers();
		$admindetails = $this->front_model->getadmindetails();
		$data['admindetails'] = $admindetails;
		$this->load->view('front/como-funciona',$data);		
	}
	/*howit_works End*/

	function rating()
	{
		$this->input->session_helper();
		$result=$this->front_model->rating(); 
		echo json_encode($result);
	}

	function generate_xml()
	{
		
		$arr_url 		  = array("minha-conta","extrato","cupom-desconto");
		$data['data_url'] = $arr_url;
        header("Content-Type: text/xml;charset=iso-8859-1");
		$this->load->view('front/samplexml',$data); 
		return 1;
	}
	 function maintenance()
	{		
		$this->input->session_helper();
		//$this->load->view('front/maintenance',$data);
		$this->load->view('front/maintenance',$data);
	} 

	function store_param_details()
	{
		$this->input->session_helper();
		$store_name = $this->input->post('storename'); 
		echo $store_ex_param_details = $this->front_model->store_param_details($store_name);
	}
	function coupon_url_details()
	{
		$this->input->session_helper();
		$store_name = $this->input->post('store_name');
		echo $store_url_details = $this->front_model->store_url_details($store_name);
	}

	function deeplink_urldetails()
	{
		$this->input->session_helper();
		$link_name   = $this->input->post('link_name');
		$connect_id  = '6BA59464117A45ADAB2D';
		$add_space   = '2149716'; 
		$program_id  = '';
		$URL 		 = $link_name;
		$LINK 		 = 'http://toolbox.zanox.com/tools/api/deeplink?connectid=' . $connect_id . '&url=' . urlencode($URL) . '&adspaceid=' . $add_space;  // . '&programid=' . $program_id
		//echo $LINK; echo "<br>";
		$deeplink 	 = file_get_contents($LINK);
		//print_r($deeplink);
		$deeplinkXML = simplexml_load_string($deeplink);
		//echo "hai" echo "<br>";
		echo $deeplinkXML->url;
	}

    function hotmail()
	{
		session_start();
		include('oauth.php'); 
		include('outlook.php'); 
               
		$contacts = OutlookService::getContacts($_SESSION['access_token'], $_SESSION['user_email']);
		$data['contacts'] =  $contacts;
		$this->load->view('front/ref_friends_hotmail',$data);
	}
	function banner_clickcount()
	{
		$this->input->session_helper();
		$banner_id     = $this->input->post('banner_id');
		$click_count   = $this->input->post('click_count');
		echo $click_countdetails = $this->front_model->banner_clickcount($banner_id,$click_count); 
	}

	/*New code for faq page 14-11-16*/
	function perguntas_frequentes()
	{
		$this->input->session_helper();	
		$user_id 		= $this->session->userdata('user_id');
		$data['users']  = $this->front_model->userdetails($user_id);
		$data['admin']  = $this->front_model->admindetails();
		$data['result'] = $this->front_model->cms_content($names);
		$data['names']  = $names;
		$this->load->view('front/perguntas-frequentes',$data);
		$this->security->cookie_handlers();
	}
	/*End 14-11-16*/
	function update_count_details()
	{
		$this->input->session_helper();
		echo $count_details = $this->front_model->update_count_details();
	}

	/*New code for logged users popup count update details 19-11-16*/
	function log_update_count_details()
	{
		$this->input->session_helper();
		$this->session->set_userdata('ses_popup_count',1);
		echo $count_details = $this->front_model->log_update_count_details();
	}
	/*End 19-11-16*/
}
?>
