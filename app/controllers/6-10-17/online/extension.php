<?php
class Extension extends CI_Controller
{
	public function __construct() {	
		parent::__construct();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->load->library('session');
		$this->load->library('form_validation','Utility');
		$this->load->model('extension_model');
		$this->load->model('front_model');
		session_start();

		if($_COOKIE['user_email']!="" && $this->session->userdata('user_id')=="")
		{
			//set session
			$this->session->set_userdata('user_id',$_COOKIE['user_id']);
			$this->session->set_userdata('user_email',$_COOKIE['user_email']);
		}
	 }
	 
			function getnewdesign()
			{
		 	  	$user_id  = $this->session->userdata('user_id'); 

			  	$data['userdetails'] = $this->extension_model->userdetails($user_id);
			  	$currenturl = $this->input->get('currenturl');
			  	$store_url = $this->input->get('store_url');

			   
			   	$result = $this->extension_model->getcurrentcoupons($store_url);
			   	if($result)
			   	{
			   		$data['store_url'] = $store_url;
			   		$data['getmaxcoupons'] = $result;
			   	}
			   
			   	if($user_id!='')
			  	{
				  	echo $this->load->view('extension/getnewdesign',$data);
		      	}
		      	else
		      	{
					echo $this->load->view('extension/beforelogin',$data);  
				}
			}
			 
			 function getuserloginornot()
			 {
				  $user_id  = $this->session->userdata('user_id'); 
				  
				  echo $user_id;
			 }
			 
			 function getofferdetails()
			 {
			   	$currenturl = $this->input->post('currenturl');

			   	$result = $this->extension_model->getcurrentcoupons($currenturl);
			   
			   	// get coupons count by stores
			   	if($result)
			   	{
			   		$count = $this->extension_model->get_store_coupons($result->affiliate_name);
			   		$data['coupon_count'] = $count;
			   	}

			   	$data['getmaxcoupons'] = $result;
			   	$data['currenturl']  = $currenturl;
				   
			  	return $this->load->view('extension/getcouponsbasedonurl',$data,TRUE);
			 }
			 
			function get_storeid()
			{
				$currenturl = $this->input->post('currenturl');

				$result = $this->extension_model->getcurrentcoupons($currenturl);
				if($result)
					echo $result->affiliate_id;
				else 
					echo 0;
			}

			function mainfoxoffer()
			{
				$currenturl = $this->input->post('currenturl');
				   
				$data['getmaxcoupons'] = $this->extension_model->getcurrentcoupons($currenturl);
				   
				return $this->load->view('extension/mainfoxoffer',$data,TRUE);  
			 }
			 

			 
			function get_domain($url)
			{
				$pieces = parse_url($url);
				$domain = isset($pieces['host']) ? $pieces['host'] : '';
				if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
				return $regs['domain'];
				}
				return false;
			}
			
			
			function redirecionar($storeid=null,$couponid=null,$name)
			{
				$storeid = $this->uri->segment(2);
				$admindetails = $this->front_model->getadmindetails();
				$data['admindetails'] = $admindetails;
				$store_details = $this->front_model->get_store_details_byid($storeid);
				$data['store_details'] = $store_details;
				$store_name = $store_details->affiliate_name;
				
				//$_SESSION['activate']  = $storeid;
				// extension anand
				unset($_SESSION['SET_ACTIVE_TAB'.$storeid]);
				//$this->session->unset_userdata('SET_ACTIVE_LINK'.$storeid);
				$_SESSION['activate'.$storeid]  = 'couponsactivated'; 
				 		
				if($name!='')
				{
				$coupon = $this->front_model->get_coupons_from_shoppingcoupon_byid($couponid);
				}
				else
				{
				$coupon = $this->front_model->get_coupons_from_coupon_byid($couponid);
				}	
				$user_id = $this->session->userdata('user_id');
				$click_history = $this->front_model->click_history($storeid,$couponid,$user_id);
				$data['user_id'] = $user_id;
				$data['coupon'] = $coupon;
				$this->load->view('extension/visit-shop',$data);
			}
				
				function adicionar_bonus()
				{
					$this->load->view('extension/givenunicbonus');	
				}

				function statusupdate()
				{
				    $_SESSION["status"] = "close";
				    echo  $_SESSION["status"];
				}
				
				// anand
				function set_session()
				{
					$name = $this->input->post('name');
					$key = $this->input->post('key');
				    $_SESSION[$name] = $key;
				    echo $_SESSION[$name];
				}
				function get_session()
				{
					$name = $this->input->post('name');
				    if(isset($_SESSION[$name]))
					{
					   echo $_SESSION[$name];
					}
				}
				
				
				function getstasus()
				{
				   	if(isset($_SESSION["status"]))
					{
					   echo $_SESSION["status"];
					}
				}
				
				function statusupdateclose()
				{
					unset($_SESSION["status"]);
				}
				
				function checkactivestatus()
				{
					$store_id = $this->input->post('store_id');

					if(isset($_SESSION['activate'.$store_id]))
					{
						echo $_SESSION['activate'.$store_id];
					}
					if(isset($_SESSION['SET_ACTIVE_TAB'.$store_id]))
					{
						echo $_SESSION['SET_ACTIVE_TAB'.$store_id];
					}
				}
				
				function unsetactivestatus()
				{
					$store_id = $this->input->post('store_id');
					if(isset($_SESSION["site_coupon".$store_id]))
					{
						unset($_SESSION['close_status'.$store_id]);
						echo 'site_coupon';
					} else {	
						unset($_SESSION["activate".$store_id]);
						unset($_SESSION['SET_ACTIVE_TAB'.$store_id]);
						unset($_SESSION['close_status'.$store_id]);
						unset($_SESSION["browser_url"]);
						$this->session->unset_userdata('SET_ACTIVE_LINK'.$store_id);
						//echo 'sdadas'.$_SESSION['SET_ACTIVE_TAB'.$store_id];
					}
					
				}

				function after_activate_coupon()
				{
					echo '<img src="https://www.pingou.gmeec.com.br/front/new/images/pingouwhitelogo.png"><p style="font-size:16px !important;font-family: arial !important; font-weight: normal !important;"> Ativado! <br><br> Conclua a compra nesta <br> aba do navegador!</p><a href="javascript:void(0);" class="cd-close-info" id="bigclose"></a>';
				}

	
	function active_tab()
	{
		$currenturl = $this->input->post('currenturl');
		$status = $this->input->post('status');

		$result = $this->extension_model->getcurrentcoupons($currenturl);
		
		// get coupons count by stores
	   	if($result)
	   	{
	   		$store_id = $result->affiliate_id;
			if($_SESSION["activate".$store_id]!="" || $status=='close') //for other deactive sites open
			{
				unset($_SESSION["activate".$store_id]);
				unset($_SESSION["browser_url"]);
				unset($_SESSION['close_status'.$store_id]);
				$_SESSION['SET_ACTIVE_TAB'.$store_id]  = 'SET_ACTIVE_TAB'; 

		   		$status = 'active';
		  	} else {
		  		$status = 'not-active';
		  	} 
		} else {
			$status = 'not-active';
		}
		
		
		// large icon content			
		$large = $this->getofferdetails();

		// small icon content   	
	   	$child = $this->mainfoxoffer();
	  	
	  	$output = array(
	        'large' => $large, // load content big icon
	        'child' => $child,
	        'status' => $status
	    );
	    echo json_encode($output);
	}	

	function check_active_session()
	{
		$currenturl = $this->input->post('currenturl');	
		
		$result = $this->extension_model->getcurrentcoupons($currenturl);
		// get coupons count by stores
	   	if($result)
	   	{
	   		$store_id = $result->affiliate_id;
	   		//echo $_SESSION["activate".$store_id];die;
			//if($_SESSION["activate".$store_id]=="" || $_SESSION['SET_ACTIVE_TAB'.$store_id] !="")
			$closed_by = '';
	   		if($_SESSION["close_status".$store_id]) { 
				$status = 'close';
				$closed_by = $_SESSION["close_status".$store_id];
			} elseif($_SESSION['SET_ACTIVE_TAB'.$store_id] !="") {
				$status = 'red';
			} elseif($_SESSION["activate".$store_id]) { 
				$status = 'green';
			} else {
				$status = 'blue';
			}
		// large icon content			
		   	$large = $this->getofferdetails();

		// small icon content   	
		   	$child = $this->mainfoxoffer();
		  	
		  	$output = array(
		        'large' => $large, // load content big icon
		        'child' => $child,
		        'status' => $status,
		        'closed_by' => $closed_by
		    );
		    echo json_encode($output);
		}
	}

	// check store available in site
	function check_store()
	{
		$currenturl = $this->input->get('currenturl');

		$result = $this->extension_model->getcurrentcoupons($currenturl);
		if($result)
			echo $result->affiliate_id;
		else 
			echo 0;
	}		

	function unset_site_coupon()
	{
		$store_id = $this->input->post('store_id');
		unset($_SESSION["site_coupon".$store_id]);
	}

	function set_close_session()
	{
		$store_id = $this->input->post('store_id');
		$closed_by = $this->input->post('closed_by');
		$_SESSION["close_status".$store_id] = $closed_by;
	}
	

	function active_tab_link()
	{
		$currenturl = $this->input->post('currenturl');	
		$result = $this->extension_model->getcurrentcoupons($currenturl);	
		// get coupons count by stores
	   	if($result)
	   	{
	   		$store_id = $result->affiliate_id;
	   		$this->session->set_userdata('SET_ACTIVE_LINK'.$store_id,'SET_ACTIVE_LINK');
	   		//echo $this->session->userdata('SET_ACTIVE_LINK'.$store_id);
			//$_SESSION['SET_ACTIVE_LINK'.$store_id]  = 'SET_ACTIVE_LINK'; 
	   	}
	}

} // End class